﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;


namespace HCB
{
    public partial class Administration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
            {
                int onlineUsers  = Convert.ToInt32(Application["OnlineUsers"].ToString());

                if (onlineUsers < 2)
                    lblOnlineUsers.Text = AntiXss.HtmlEncode(onlineUsers.ToString()) + " User online";
                else
                    lblOnlineUsers.Text = AntiXss.HtmlEncode(onlineUsers.ToString()) + " Users online";

            }
            else if (Session["UserType"] != null && Session["UserType"].ToString() == "Inco")
            {
                //trAdminTool.Visible = false;
                trUserPro.Visible = false;
                trSerReg.Visible = false;
                trIllInj.Visible = false;
                trClaimClosed.Visible = false;
                trReasonClosed.Visible = false;
                trBroker.Visible = false;
                trFeeCharge.Visible = false;
                trCorPartner.Visible = false;
                trProdType.Visible = false;
                trWaitPeriod.Visible = false;
                trEmpStatus.Visible = false;
                trBrand.Visible = false;
                trIncapacity.Visible = false;
                trTypeVisit.Visible = false;
                trTypeCall.Visible = false;
                trRehabType.Visible = false;
                trRehabPro.Visible = false;
                trManageMt.Visible = false;
                trManInfo.Visible = false;
                trAuditTrail.Visible = false;                
                trIncoUser.Visible = false;
                trOnlineUser.Visible = false;
                //trSchemeNumber.Visible = false;
                trSchemeName.Visible = false;
                trRTWDuration.Visible = false;
                trSLAExceptionReport.Visible = false;
                trManagementInfoReport.Visible = false;
                trMIFullDataReport.Visible = false;
                //trReferralForm.Visible = false;
            }
            else
            {
                //ClientScript.RegisterStartupScript(typeof(Page), "MessagePopUp",
                //"alert('ABCDEFGH'); window.location.href = 'Default.aspx';", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Permission",
                //    "alert('you dont have rights to view this page.');window.location.href ='~/Default.aspx';",true);
                //Response.Redirect("~/Accessdenied.aspx", false);
                Server.Transfer("~/Accessdenied.aspx");
            }
        }
    }
}