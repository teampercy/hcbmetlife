using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Caching;

using System.Collections;
using System.Collections.Generic;
using HCBBLL;

public class HCBSiteMapProvider : StaticSiteMapProvider
{

    private readonly object siteMapLock = new object();
    private SiteMapNode root = null;
    public const string CacheDependencyKey = "SMPSiteMapProviderCacheDependency";

    public override SiteMapNode BuildSiteMap()
    {
        // Use a lock to make this method thread-safe
        lock (siteMapLock)
        {
            // First, see if we already have constructed the
            // rootNode. If so, return it...
            if (root != null)
                return root;

            // We need to build the site map!

            // Clear out the current site map structure
            base.Clear();

            // Get the categories and products information from the database
            //ProductsBLL productsAPI = new ProductsBLL();
            //Northwind.ProductsDataTable products = productsAPI.GetProducts();


            DataSet ds = MasterListBL.GetAdminList();

            // Create the root SiteMapNode
            root = new SiteMapNode(this, "root", "~/index.aspx", "Home ");
            AddNode(root);

            // Create SiteMapNodes for the categories and products
            

            foreach (DataRow prRow in ds.Tables[0].Rows)
            {
                // Add a new category SiteMapNode, if needed
                string AdminTypeKey, AdminTypeName;

                bool createUrlForProTypeNode = true;

                AdminTypeKey = string.Concat("Admin:", prRow["TypeId"]);
                AdminTypeName = prRow["TypeName"].ToString();

                //proTypeKey = string.Concat("protype:", "1");
                //proTypeName = "Dentist";

                SiteMapNode proTypeNode = FindSiteMapNodeFromKey(AdminTypeKey);

                // Add the category SiteMapNode if it does not exist
                if (proTypeNode == null)
                {
                    string proTypeUrl = string.Empty;
                    if (createUrlForProTypeNode)
                        proTypeUrl = "~/MasterList.aspx.aspx?Type=" + prRow["Type"];
                    //proTypeUrl = "~/Dentist.aspx?protype=1";


                    proTypeNode = new SiteMapNode(this, AdminTypeKey, proTypeUrl, AdminTypeName);
                    AddNode(proTypeNode, root);
                    //AddNode(proTypeNode);

                    //string stateNodeKey = proTypeKey + ":AA";
                    //SiteMapNode stateNode = FindSiteMapNodeFromKey(stateNodeKey);
                    //if (stateNode == null)
                    //{
                    //    string stateUrl = "~/SMPSearchResult.aspx?protype=" + prRow["ProfessionalTypeId"] + "&state=AA";
                    //    stateNode = new SiteMapNode(this, stateNodeKey, stateUrl, "AA");
                    //    AddNode(stateNode, proTypeNode);
                    //}
                }


                //proTypeKey = string.Concat("protype:", "2");
                //proTypeName = "Doctor";


                //proTypeNode = FindSiteMapNodeFromKey(proTypeKey);

                //// Add the category SiteMapNode if it does not exist
                //if (proTypeNode == null)
                //{
                //    string proTypeUrl = string.Empty;
                //    if (createUrlForProTypeNode)
                //        proTypeUrl = "~/Dentist.aspx?protype=2";
                //    //proTypeUrl = "~/Dentist.aspx?protype=" + proTypeName;

                //    proTypeNode = new SiteMapNode(this, proTypeKey, proTypeUrl, proTypeName);
                //    AddNode(proTypeNode, root);
                //    //AddNode(proTypeNode);
                //}


                //// Add the State to SiteMapNode
                //string stateNodeKey = proTypeKey + ":" + prRow["state"].ToString();
                //SiteMapNode stateNode = FindSiteMapNodeFromKey(stateNodeKey);
                //if (stateNode == null)
                //{
                //    string stateUrl = "~/SMPSearchResult.aspx?protype=" + prRow["Profess`ionalTypeId"] + "&state=" + prRow["state"].ToString();
                //    stateNode = new SiteMapNode(this, stateNodeKey, stateUrl, prRow["state"].ToString());
                //    AddNode(stateNode, proTypeNode);
                //}

                proTypeNode = null;
               // stateNode = null;

                //Add user to the sitemapNode
                //string userKey = proTypeKey + ":" + prRow["user"].ToString();

                //   SiteMapNode ProNode = 


                ////string stateUrl = "~/Dentist.aspx?proType=" + proTypeName + "&state=" + state;
                ////SiteMapNode stateNode = new SiteMapNode(this, string.Concat("State:", state), stateUrl, state);
                ////AddNode(stateNode, proTypeNode);

                //string searchResultsURL = "~/SMPSearchResults.aspx?state=" + state;
                //SiteMapNode searchResultsNode = new SiteMapNode(this, string.Concat("State:", state), searchResultsURL, "Search Results...");
                //AddNode(searchResultsNode, stateNode);

            }

            // Add a "dummy" item to the cache using a SqlCacheDependency
            //// on the Products and Categories tables
            //System.Web.Caching.SqlCacheDependency proTypesTableDependency = new System.Web.Caching.SqlCacheDependency("SelectMyProfessional", "ProfessionalTypes");

            //// Create an AggregateCacheDependency
            //System.Web.Caching.AggregateCacheDependency aggregateDependencies = new System.Web.Caching.AggregateCacheDependency();
            //aggregateDependencies.Add(proTypesTableDependency);

            //// Add the item to the cache specifying a callback function
            //HttpRuntime.Cache.Insert(CacheDependencyKey, DateTime.Now, aggregateDependencies, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.Normal, new CacheItemRemovedCallback(OnSiteMapChanged));


            // Finally, return the root node
            return root;
        }
    }

    protected override SiteMapNode GetRootNodeCore()
    {
        return BuildSiteMap();
    }

    protected void OnSiteMapChanged(string key, object value, CacheItemRemovedReason reason)
    {
        lock (siteMapLock)
        {
            if (string.Compare(key, CacheDependencyKey) == 0)
            {
                // Refresh the site map
                root = null;
            }
        }
    }

    public DateTime? CachedDate
    {
        get
        {
            return HttpRuntime.Cache[CacheDependencyKey] as DateTime?;
        }
    }

    
}
