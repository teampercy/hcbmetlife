﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Assessordetails.aspx.cs" Inherits="HCB.Assessordetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="scManager" runat="server">
    </asp:ScriptManager>
    <div id="backgroundPopup1" onclick="closepopup()">
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 80%; margin: 0 auto;"
            class="MainText" id="tableMain">
            <tr>
                <td align="center">
                    <asp:Label CssClass="PageTitle" ID="lblTitle" runat="server"></asp:Label>
                    <%--<><b>Maintain Reason Cancelled</b></font>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton Style="float: right; padding: 0px 50px 0px 0px;" ID="lnkAdd" runat="server"
                        OnClientClick="return  loadPopup('#Popup')"> Add New</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upGVList" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvReasonList" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                                Width="95%" OnRowCreated="gvReasonList_RowCreated1">
                                <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" />
                                <Columns>
                                    <asp:BoundField HeaderText="Name" DataField="Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkeEdit" runat="server" OnCommand="lnk_Command" CommandArgument='<%# string.Concat(Eval("ID"),",",Eval("Name")) %>'
                                                CommandName="edt"> Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Email Id" DataField="TeamEmailID" />
                                    <asp:BoundField HeaderText="Email Id" DataField="TeamPhone No" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton Visible="true" ID="lnkeDelete" runat="server" OnCommand="lnk_Command"
                                                CommandArgument='<% #Eval("ID") %>' CommandName="del" ImageUrl="Images/Del.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="hdntype" Value="" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
            <td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                                Width="95%" OnRowCreated="gvReasonList_RowCreated1">
                                <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" />
                                <Columns>
                                    <asp:BoundField HeaderText="Name" DataField="Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkeEdit" runat="server" OnCommand="lnk_Command" CommandArgument='<%# string.Concat(Eval("ID"),",",Eval("Name")) %>'
                                                CommandName="edt"> Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Email Id" DataField="TeamEmailID" />
                                    <asp:BoundField HeaderText="Email Id" DataField="TeamPhone No" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton Visible="true" ID="lnkeDelete" runat="server" OnCommand="lnk_Command"
                                                CommandArgument='<% #Eval("ID") %>' CommandName="del" ImageUrl="Images/Del.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" Value="" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="updAssess" runat="server">
            <ContentTemplate>
                <div id="AssessPopup" class="popupContact">
                    <table>
                        <thead>
                            <tr>
                                <td colspan="2">
                                    <br />
                                </td>
                            </tr>
                        </thead>
                        <tr>
                            <td>
                                <font color="black" face="Arial" size="2">Email ID:</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAEmail" ValidationGroup="validateTxt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqftxtAEmail" runat="server" ControlToValidate="txtAEmail"
                                    ErrorMessage="Email Required" ValidationGroup="validateTxt"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RGEtxtUserName" ControlToValidate="txtAEmail"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="ErrorMessage"
                                    ValidationGroup="validateUser" runat="server" ErrorMessage="Invalid Email address">  <img src='images/Exclamation.jpg'></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <td style="text-align: left">
                                    <font color="black" face="Arial" size="2">Telephone Number:</font>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtATeleNum" runat="server" CssClass="Textbox" Width="170px" MaxLength="15">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RQFtxtATeleNum" runat="server" ControlToValidate="txtATeleNum"
                                        CssClass="ErrorMessage" ValidationGroup="validateUser" ErrorMessage="TelePhone No. Required">
                           <img src='images/Exclamation.jpg'>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegtxtTeleNum" ControlToValidate="txtATeleNum"
                                        ValidationGroup="validateUser" runat="server" ErrorMessage="Invalid Formet Required Formet '123-123-1234'"
                                        ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">
                                    <img src='images/Exclamation.jpg'>
                                    </asp:RegularExpressionValidator>
                                </td>
                            </td>
                        </tr>
                        <tr>
                            <br />
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button runat="server" ID="btnSave" ValidationGroup="validateTxt" Text="Save"
                                     CommandName="assessor" OnClick="btnSave_Click" />
                                <asp:Button ID="btncancle" runat="server" Text="Cancel" OnClientClick="return closepopup();" /><%--ValidationGroup="validateUser"--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
