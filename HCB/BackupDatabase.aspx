﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="BackupDatabase.aspx.cs" Inherits="HCB.BackupDatabase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server"> 
 
    <table id="aTable" align="center" border="0" cellpadding="0" cellspacing="0" class="MainText"
        height="200">
        <tr>
            <td align="left" colspan="2" style="width: 523px; height: 154px" valign="top">
                The database system used to power this application &nbsp;is Microsoft Access.&nbsp;
                It is important to take <strong>regular</strong> backups of the database in case
                of failure.&nbsp; Backed up databases may be used for internal reporting purposes.<br />
                <br />
                <br />
                Clicking the <strong>Backup Data</strong> button will copy data from the live database
                to the backup copy.<br />
                <br />
                Clicking <strong>Download Database</strong> will download the current backup copy
                of the database.
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="width: 523px; height: 39px" valign="top">
                <asp:Button ID="btnBackupDatabase" runat="server" CssClass="ButtonClass"   Text="Backup Data" OnClick="btn_backup"
                 ValidationGroup="Save" Width="125px" />&nbsp;
                <asp:Button ID="btnDownload" runat="server" CssClass="ButtonClass"  UseSubmitBehavior="False"
                    ValidationGroup="Save" Width="130px" Text="Download Database" 
                    onclick="btnDownload_Click"   />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="height: 85px; width: 523px;" valign="top">
                <asp:Label ID="statusLabel" runat="server" CssClass="ErrorMessage" ForeColor="Red"
                    Width="514px"></asp:Label>
                <br />
            </td>
        </tr>
    </table>
</asp:Content> 
