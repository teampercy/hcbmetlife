﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HCBBLL;
using Microsoft.Security.Application;
namespace HCB
{
    public partial class ChangePassword : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lubtRedirectTo.Visible = false;

            }
        }
        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if(Page.IsValid)
                {
                DataSet ds;
                
                Users objUser = new Users();
                string ValUsername = AntiXss.HtmlEncode(txtUsername.Text.Trim().ToString());
                string ValPass = Encryption.Encrypt(AntiXss.HtmlEncode(txtConfirmPassword.Text.Trim().ToString()));
                string strCurrPassword=Encryption.Encrypt(AntiXss.HtmlEncode(txtCurrentPassword.Text.Trim().ToString()));
                ds = objUser.UpdatePasswordExpiyDate(ValUsername, strCurrPassword, ValPass);

                int error = 0;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    error = Convert.ToInt32(ds.Tables[0].Rows[0]["Error"].ToString());
                    lblUpdateMessage.Text = AntiXss.HtmlEncode(ds.Tables[0].Rows[0]["UpdateMessage"].ToString()); 
                }

                if (error == 0)
                {
                    tblUpdateInput.Visible = false;
                    tblUpdateMessage.Visible = true;
                    lubtRedirectTo.Visible = true;
                    lblChangepassword.Text =string.Empty;
                    //lblChangepassword.Text = "Your Password have been changed successfully.";
                    //  ScriptManager.RegisterStartupScript(this, this.GetType(), "RediLogin", "alert(Password was changed succesfully);", true);
                    //if (Session["UserId"] != null || Convert.ToInt32(Session["UserId"]) > 0)
                    //{
                    //    Session.Abandon();

                    //}

                    // Response.Redirect("~/Login.aspx");
                    lblUpdateMessage.ForeColor = System.Drawing.Color.Green;

                }
                else
                {


                    tblUpdateInput.Visible = true;
                    tblUpdateMessage.Visible = true;
                    lblUpdateMessage.ForeColor = System.Drawing.Color.Red;

                }


                }
            }
            catch (Exception ex)
            {

                lblUpdateMessage.Text = AntiXss.HtmlEncode("Some Error Occured");
                Response.Write("Error in Change password: " + AntiXss.HtmlEncode(ex.Message));
            }

        }
        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}