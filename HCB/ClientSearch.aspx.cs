﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using System.Text;
namespace HCB
{
    public partial class ClientSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
                {
                    GetClaimAssessor();
                    FillClaimManager();
                    gvportletbox.Visible = false;
                }
                //else
                {

                    //ClientScript.RegisterStartupScript(typeof(Page), "MessagePopUp",
                    //"alert('ABCDEFGH'); window.location.href = 'Default.aspx';", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Permission",
                    //    "alert('you dont have rights to view this page.');window.location.href ='~/Accessdenied.aspx';",true);
                    //Response.Redirect("~/Accessdenied.aspx", false);
                }

            }
        }

        private void GetClaimAssessor()
        {
            try
            {
                Users user = new Users();
                DataSet ds = user.GetClaimAssessor();
                ddlClaimAssessor.DataSource = ds.Tables["Users"];
                ddlClaimAssessor.DataTextField = "FullName";
                ddlClaimAssessor.DataValueField = "UserID";
                ddlClaimAssessor.DataBind();
                ListItem item = new ListItem("--Select Claim Assessor--", "0");
                ddlClaimAssessor.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Claim Assessor--", "0");
                ddlClaimAssessor.Items.Insert(0, item);
            }
        }

        private void FillClaimManager()
        {
            try
            {
                Users ouser = new Users();
                DataSet ds = ouser.GetClaimConsultant();
                ddlCaseMngr.DataSource = ds.Tables["Consultant"];
                ddlCaseMngr.DataTextField = "FullName";
                ddlCaseMngr.DataValueField = "UserID";
                ddlCaseMngr.DataBind();
                ListItem item = new ListItem("--Select Case Manager--", "0");
                ddlCaseMngr.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Case Manager--", "0");
                ddlCaseMngr.Items.Insert(0, item);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtHcbRef.Text = "";
            ddlCaseMngr.SelectedIndex = 0;
            ddlClaimAssessor.SelectedIndex = 0;
            //  txtRefNum.Text = "";
            txtClaimSurname.Text = "";
            txtClaimForename.Text = "";
            txtClaimDOB.Text = "";
            txtClaimAddress.Text = "";
            RdbClaimAdd.SelectedIndex = 0;
            RdbClaimForename.SelectedIndex = 0;
            RdbClaimSurname.SelectedIndex = 0;
            //  RdbRefNumList.SelectedIndex = 0;
            gvResults.Visible = false;
            lblTitle.Visible = false;
            gvportletbox.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                gvResults.Visible = true;
                lblTitle.Visible = true;
                gvportletbox.Visible = true;
                bool IsexectSurName = false, IsexectForeName = false, IsexectAddr = false;
                if (RdbClaimSurname.Items[0].Selected) IsexectSurName = true;
                if (RdbClaimForename.Items[0].Selected) IsexectForeName = true;
                if (RdbClaimAdd.Items[0].Selected) IsexectAddr = true;
                MasterListBL MSL = new MasterListBL();
                DataSet dsLIst = MSL.GetSearchList(txtHcbRef.Text, Convert.ToInt32(ddlCaseMngr.SelectedValue), Convert.ToInt32(ddlClaimAssessor.SelectedValue), IsexectSurName, txtClaimSurname.Text, IsexectForeName, txtClaimForename.Text, txtClaimDOB.Text, IsexectAddr, txtClaimAddress.Text);
                if (Session["ds"] != null) Session["ds"] = dsLIst;
                else Session.Add("ds", dsLIst);
                BindGrid(dsLIst);
            }
            catch (Exception ex)
            { 
                ClientScript.RegisterStartupScript(this.GetType(),"","alert('" + ex.Message + "');"); 
            }
        }
       
        protected void gvResults_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "HCBRef")
            {
                Session["ClientHcbRef"] = e.CommandArgument;
                Response.Redirect("~/ClientRecord.aspx");
            }
        }
        
        private void BindGrid(DataSet ds)
        {
            ClientInfo cltinfo = new ClientInfo();
            DataTable dt = new DataTable();

            try
            {
                if (ds != null)
                {
                    dt = ds.Tables["ClientList"];
                    gvResults.DataSource = dt;
                    gvResults.DataBind();
                }
                else
                {
                    gvResults.DataSource = ds;
                    gvResults.DataBind();
                }
            }
            catch (Exception ex)
            {
                gvResults.DataSource = ds;
                gvResults.DataBind();
            }
        }
        
        protected void gvResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet dsList = new DataSet();

            try
            {
                gvResults.PageIndex = e.NewPageIndex;    
                if (Session["ds"] != null) dsList = (DataSet)Session["ds"];
                else Response.Redirect(Request.UrlReferrer.PathAndQuery, false);
                BindGrid(dsList);
            }
            catch (Exception ex)
            {
                BindGrid(dsList);
            }
        }
    }
}