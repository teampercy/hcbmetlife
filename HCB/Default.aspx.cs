﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using System.Web.Services;
using System.Web.Script.Serialization;
using HCBBLL;

using System.IO;
using SelectPdf;
using HCB.UserControls;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using iTextSharp.tool.xml;
//using iTextSharp.text.html;
//using iTextSharp.text.html.simpleparser;
//using Winnovative.WnvHtmlConvert;

namespace HCB
{
    public partial class Default : System.Web.UI.Page
    {
        static int userId;
        static string usertype;
        static ClientInfo ClientInfo = new ClientInfo();
        private bool startConversion = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserType"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                userId = (int)Session["LoggedInuserId"];
                usertype = Convert.ToString(Session["UserType"]);
            }
            
            //userId = 150;
            //usertype = "Admin";
            //Session["LoggedInUserId"] = 150;
            //Session["TermsAccepted"] = "1";
            //Session["LogUserEmailAdd"] = "priyesh@rhealtech.com";
            //Session["UserType"] = "Admin";
            //Session["LoggedInUserName"] = "Priyesh";
            //Session["LogUserActiveAcct"] = 1;
            //Session["LoggedInUserPassword"] = "Priyesh127";
            //Session["PasswordExpiryDate"] = DateTime.Now.AddDays(+30);

            //            if (Session["UserType"].ToString() == "Admin")
            //            {
            //                DisableNonAdminControls();
            //                Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
            //            }
            //            else if (Session["UserType"].ToString() == "Inco")
            //            {
            //                DisableNonIncoControls();
            //                Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
            //            }
            //            else if (Session["UserType"].ToString() == "Nurse")
            //            {
            //                DisableNonNurseControls();
            //            }

            //            hidLogUserType.Value = Session["UserType"].ToString();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Response.Redirect("~/Login.aspx");
            //    }

        }

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerEmployer()
        {

            //JavaScriptSerializer TheSerializer = new JavaScriptSerializer();
            //ClientInfo ClientInfo = new ClientInfo();
            Dictionary<string, int> ClaimPerEmployer = ClientInfo.GetTotalClaimPerEmployer(userId, usertype);
            //return TheSerializer.Serialize(ClaimPerEmployer);
            return ClaimPerEmployer;
        }

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerBroker()
        {
            //ClientInfo ClientInfo = new ClientInfo();
            Dictionary<string, int> ClaimPerBroker = ClientInfo.GetTotalClaimPerBroker(userId, usertype);
            return ClaimPerBroker;
        }

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerScheme()
        {
            //ClientInfo ClientInfo=new ClientInfo();
            Dictionary<string, int> ClaimPerScheme = ClientInfo.GetTotalClaimPerScheme(userId, usertype);
            return ClaimPerScheme;
        }

        //[WebMethod]
        //public static Dictionary<string,int> GetTotalClaimPerInjury()
        //{            
        //    ClientInfo ClientInfo = new ClientInfo();
        //    Dictionary<string, int> ClaimPerInjury = ClientInfo.GetTotalClaimPerInjury();            
        //    return ClaimPerInjury;
        //}

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerInjury()
        {
            //ClientInfo ClientInfo = new ClientInfo();
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            Dictionary<string, int> ClaimPerInjury = ClientInfo.GetTotalClaimPerInjury(userId, usertype, 0, 0, startDate, endDate);
            return ClaimPerInjury;
        }

        [WebMethod]
        public static Dictionary<string, double> GetTotalClaimPerReturnToWorkReason()
        {
            //ClientInfo ClientInfo = new ClientInfo();
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            Dictionary<string, double> ClaimPerReturnToWorkReason = ClientInfo.GetTotalClaimPerReturnToWorkReason(userId, usertype, 0, 0, startDate, endDate);
            return ClaimPerReturnToWorkReason;
        }

        [WebMethod]
        public static List<UserInformation> GetNursesLocations()
        {
            Users objUser = new Users();
            return objUser.GetNurseLocations();
        }

        [WebMethod]
        public static List<DashboardInitialChart> GetInitialChartData(InitialChartTypes ChartType,DateTime FromDate,DateTime ToDate)
        {            
            return ClientInfo.GetInitialChartData(ChartType, userId, usertype,FromDate,ToDate);
        }

        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Verifies that the control is rendered */
        //}


        //protected void btnExport_Click(object sender, EventArgs e)
        //{
        //    //string base64 = Request.Form["chart_data"];
        //    //byte[] bytes = Convert.FromBase64String(base64.Split(',')[1]);
        //    //string fileName = "Chart.pdf";
        //    //iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(bytes);
        //    //using (MemoryStream memoryStream = new MemoryStream())
        //    //{
        //    //    Document document = new Document(PageSize.A4, 88f, 88f, 10f, 10f);
        //    //    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
        //    //    document.Open();
        //    //    document.Add(image);
        //    //    document.Close();
        //    //    bytes = memoryStream.ToArray();
        //    //    memoryStream.Close();
        //    //}
        //    //Response.Clear();
        //    //Response.Buffer = true;
        //    //Response.Charset = "";
        //    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    //Response.ContentType = "application/pdf";
        //    //Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
        //    //Response.BinaryWrite(bytes);
        //    //Response.Flush();
        //    //Response.End();

        //    //HiddenField hffield = hdnBrokerChart;
        //    string[] hffield = new string[] { "hdnBrokerChart" };            
        //    byte[] bytes = new byte[] {0};
        //    String base64;
        //    iTextSharp.text.Paragraph space =new  iTextSharp.text.Paragraph("  ");
        //    ArrayList a1 = new ArrayList();
        //    PdfPTable Table =new PdfPTable(2);
        //    Table.WidthPercentage = 87;
        //    Table.DefaultCell.Padding = 16;
        //    Table.DefaultCell.Border = 0;
        //    Table.HorizontalAlignment = Element.ALIGN_LEFT;

        //    for (int i = 0; i <= hffield.Length - 1; i++)
        //    {                
        //        //base64 = Request.Form[hffield[i]];
        //        base64 = hdnBrokerChart.Value;
        //        if(base64!="")
        //        {                                        
        //            bytes = Convert.FromBase64String(base64.Split(',')[1]);
        //            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(bytes);
        //            image.ScalePercent(55.0F);
        //            a1.Add(image);
        //        }
        //    }

        //    using (MemoryStream memoryStream = new MemoryStream())
        //    {
        //        Document document = new Document(PageSize.A4, 125.0F, 88.0F, 10.0F, 10.0F);
        //        PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
        //        document.Open();

        //        foreach(iTextSharp.text.Image i in a1)
        //            {
        //            document.Add(i);
        //            document.Add(space);
        //        }

        //        document.Add(Table);
        //        document.Close();
        //        bytes = memoryStream.ToArray();
        //    }

        //    string fileName = "Chart.pdf";
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.Charset = "";
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.ContentType = "application/pdf";
        //    Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
        //    Response.BinaryWrite(bytes);
        //    Response.Flush();
        //    Response.End();

        //}

        protected void btnHtmlToPdf_Click(object sender, EventArgs e)
        {
            startConversion = true;                                                   
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (startConversion)
            {
                // read parameters from the webpage            
                //string url = HttpContext.Current.Request.Url.AbsoluteUri;

                string pdf_page_size = "A4";
                SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize),
                    pdf_page_size, true);

                //string pdf_orientation = DdlPageOrientation.SelectedValue;
                string pdf_orientation = "Portrait";
                PdfPageOrientation pdfOrientation =
                    (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    pdf_orientation, true);

                int webPageWidth = 1300;                
                int webPageHeight = 0;

                // get html of the page
                TextWriter myWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(myWriter);
                base.Render(htmlWriter);

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                //// set converter options

                //converter.Options.VisibleWebElementId = "MainContent_divMainDashboard";  //This Property works in Select.Pdf dll not in Select.HtmlToPdf dll,for both of them same namespace used          
                //converter.Options.RenderPageOnTimeout = true;
                converter.Options.MinPageLoadTime = 5;
                //converter.Options.MaxPageLoadTime = 600;
                //converter.Options.JavaScriptEnabled = true;
                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = webPageWidth;
                converter.Options.WebPageHeight = webPageHeight;
                converter.Options.ExternalLinksEnabled = false;
                //converter.Options.MarginTop = 5;
                //converter.Options.MarginBottom = 5;
                //converter.Options.MarginRight = 5;
                //converter.Options.MarginLeft = 5;
                //converter.Options.PluginsEnabled=true;

                // create a new pdf document converting the html string of the page
                PdfDocument doc = converter.ConvertHtmlString(
                    myWriter.ToString(), Request.Url.AbsoluteUri);

                // save pdf document
                doc.Save(Response, false, "Dashboard_"+DateTime.Now+".pdf");

                // close pdf document
                doc.Close();
            }
            else
            {
                // render web page in browser
                base.Render(writer);
            }
        }

    }
}