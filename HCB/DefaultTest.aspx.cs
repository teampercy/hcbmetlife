﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using System.Web.Services;
using System.Web.Script.Serialization;
using HCBBLL;

using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Winnovative.WnvHtmlConvert;
using System.Data;
using System.Data.Common;
using System.Web.UI.HtmlControls;
using System.Collections;

using SelectPdf;


namespace HCB
{
    public partial class DefaultTest : System.Web.UI.Page
    {
        static int userId;
        static string usertype;
        static ClientInfo ClientInfo = new ClientInfo();
        protected void Page_Load(object sender, EventArgs e)
        {

            //if (Session["UserType"] == null)
            //{
            //    Response.Redirect("~/Login.aspx");
            //}
            //else
            //{
            //    userId = (int)Session["LoggedInuserId"];
            //    usertype = Convert.ToString(Session["UserType"]);
            //}

            userId = 150;
            usertype = "Admin";
            Session["LoggedInUserId"] = 150;
            Session["TermsAccepted"] = "1";
            Session["LogUserEmailAdd"] = "priyesh@rhealtech.com";
            Session["UserType"] = "Admin";
            Session["LoggedInUserName"] = "Priyesh";
            Session["LogUserActiveAcct"] = 1;
            Session["LoggedInUserPassword"] = "Priyesh127";
            Session["PasswordExpiryDate"] = DateTime.Now.AddDays(+30);

            //            if (Session["UserType"].ToString() == "Admin")
            //            {
            //                DisableNonAdminControls();
            //                Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
            //            }
            //            else if (Session["UserType"].ToString() == "Inco")
            //            {
            //                DisableNonIncoControls();
            //                Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
            //            }
            //            else if (Session["UserType"].ToString() == "Nurse")
            //            {
            //                DisableNonNurseControls();
            //            }

            //            hidLogUserType.Value = Session["UserType"].ToString();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Response.Redirect("~/Login.aspx");
            //    }

        }

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerEmployer()
        {

            //JavaScriptSerializer TheSerializer = new JavaScriptSerializer();
            //ClientInfo ClientInfo = new ClientInfo();
            Dictionary<string, int> ClaimPerEmployer = ClientInfo.GetTotalClaimPerEmployer(userId, usertype);
            //return TheSerializer.Serialize(ClaimPerEmployer);
            return ClaimPerEmployer;
        }

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerBroker()
        {
            //ClientInfo ClientInfo = new ClientInfo();
            Dictionary<string, int> ClaimPerBroker = ClientInfo.GetTotalClaimPerBroker(userId, usertype);
            return ClaimPerBroker;
        }

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerScheme()
        {
            //ClientInfo ClientInfo=new ClientInfo();
            Dictionary<string, int> ClaimPerScheme = ClientInfo.GetTotalClaimPerScheme(userId, usertype);
            return ClaimPerScheme;
        }

        //[WebMethod]
        //public static Dictionary<string,int> GetTotalClaimPerInjury()
        //{            
        //    ClientInfo ClientInfo = new ClientInfo();
        //    Dictionary<string, int> ClaimPerInjury = ClientInfo.GetTotalClaimPerInjury();            
        //    return ClaimPerInjury;
        //}

        [WebMethod]
        public static Dictionary<string, int> GetTotalClaimPerInjury()
        {
            //ClientInfo ClientInfo = new ClientInfo();
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            Dictionary<string, int> ClaimPerInjury = ClientInfo.GetTotalClaimPerInjury(userId, usertype, 0, 0, startDate, endDate);
            return ClaimPerInjury;
        }

        [WebMethod]
        public static Dictionary<string, double> GetTotalClaimPerReturnToWorkReason()
        {
            //ClientInfo ClientInfo = new ClientInfo();
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            Dictionary<string, double> ClaimPerReturnToWorkReason = ClientInfo.GetTotalClaimPerReturnToWorkReason(userId, usertype, 0, 0, startDate, endDate);
            return ClaimPerReturnToWorkReason;
        }

        [WebMethod]
        public static List<UserInformation> GetNursesLocations()
        {
            Users objUser = new Users();
            return objUser.GetNurseLocations();
        }

        [WebMethod]
        public static List<DashboardInitialChart> GetInitialChartData(InitialChartTypes ChartType, DateTime FromDate, DateTime ToDate)
        {
            return ClientInfo.GetInitialChartData(ChartType, userId, usertype, FromDate, ToDate);
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
                
        protected void btnHtmlToPdf_Click(object sender, EventArgs e)
        {
            // read parameters from the webpage
            //string url = TxtUrl.Text;
            string url = HttpContext.Current.Request.Url.AbsoluteUri;

            //string pdf_page_size = DdlPageSize.SelectedValue;
            string pdf_page_size = "A4";
            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize),
                pdf_page_size, true);

            //string pdf_orientation = DdlPageOrientation.SelectedValue;
            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1300;
            //try
            //{
            //    webPageWidth = Convert.ToInt32(TxtWidth.Text);
            //}
            //catch { }

            int webPageHeight = 0;
            //try
            //{
            //    webPageHeight = Convert.ToInt32(TxtHeight.Text);
            //}
            //catch { }

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // converter.Options.HttpCookies.Add(FormsAuthentication.FormsCookieName,Request.Cookies[FormsAuthentication.FormsCookieName].Value);
            //string LoginId = Convert.ToString(Session["LoggedInUserName"]);
            //string LoginPassword = Convert.ToString(Session["LoggedInUserPassword"]);
            //converter.Options.HttpCookies.Add(FormsAuthentication.Authenticate(LoginId, LoginPassword));

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = converter.ConvertUrl(url);

            // save pdf document
            doc.Save(Response, false, "Sample.pdf");

            // close pdf document
            doc.Close();
        }       
    }
}