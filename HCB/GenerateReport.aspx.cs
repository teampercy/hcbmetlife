﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Text;
using HCBBLL;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Winnovative.WnvHtmlConvert;



namespace HCB
{
    public partial class GenrateReport : System.Web.UI.Page
    {

        string strHeader = "", strColName = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
            {
                if (!IsPostBack)
                {
                    GetAllDropDownType();
                    lnkExport.Enabled = false;
                }
            }
            else
            {

                //ClientScript.RegisterStartupScript(typeof(Page), "MessagePopUp",
                //"alert('ABCDEFGH'); window.location.href = 'Accessdenied.aspx';", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Permission",
                //    "alert('you dont have rights to view this page.');window.location.href ='~/Accessdenied.aspx';",true);
                //Response.Redirect("~/Accessdenied.aspx", false);
                Server.Transfer("~/Accessdenied.aspx");
            }
        }
        private void GetAllDropDownType()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetTypeList();

                ddlReportName.DataSource = ds.Tables["TypeList"];
                ddlReportName.DataTextField = "Name";
                ddlReportName.DataValueField = "ID";
                ddlReportName.DataBind();
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("-- select --", "0");
                ddlReportName.Items.Insert(0, item);

            }
            catch (Exception ex)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("-- select --", "0");
                ddlReportName.Items.Insert(0, item);

            }

        }
        public void GetReports()
        {

        }

        public void LoadData(DataTable dt)
        {

            StringBuilder sb = new StringBuilder();
            //sb.Append("<table style='text-align:center' cellpadding=\"0\" border=\"2\" cellspacing=\"0\" class=\"display\" rel=\"datatable\" id=\"example\">");
            sb.Append("<table border=\"2\" class=\"table table-striped table-bordered table-hover table-header-fixed\" rel=\"datatable\" id=\"sample_2\">");
            //sb.Append("<thead> <tr style=\"background-color: #5687D2 !important; color: white;\">");
            sb.Append("<thead> <tr class=\"font-white bg-green-steel bold font-lg\">");
            //sb.Append("<td colspan='" + dt.Columns.Count + "'> " + strHeader + " </td></tr><tr style=\"background-color: #2459A7 !important; color: white;\">");
            sb.Append("<td colspan='" + dt.Columns.Count + "'> " + strHeader + " </td></tr><tr class=\"font-white bg-green-steel\">");
            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.ToLower() != "id" && dc.ColumnName.ToLower() != "typeid")
                {
                    if (dc.ColumnName == "Name")
                        sb.Append(" <th class=\"text-center\">" + strColName + "</th> ");
                    else
                        sb.Append(" <th class=\"text-center\">" + dc.ColumnName + "</th> ");
                }

            }

            sb.Append("</tr></thead>");
            sb.Append("<tbody>");
            //sb.Append(" <a name=\"plugin\"></a>");
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append(" <tr>");
                for (int i = 0; i < dr.Table.Columns.Count; i++)
                {
                    sb.Append(" <td>" + dr[i].ToString() + "</td> ");
                }
                sb.Append("</tr> ");
            }
            sb.Append("</tbody></table>");
            DivContainer.InnerHtml = sb.ToString();

        }

        protected void ddlReportName_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Itype = SetType(ddlReportName.SelectedItem.Value.Trim());
            DataSet ds = new DataSet();
            ds = MasterListBL.GetInformationReport(Itype);
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    LoadData(ds.Tables[0]);
                    lnkExport.Enabled = true;
                }
                else
                {
                    //DivContainer.InnerHtml = "<table width='80%' style='text-align:center'><tr><td> <br> <span style='color:red;'>  No record Present <span> </td></tr></table>";
                    DivContainer.InnerHtml = "<br> <span class=\"font-red bold text-center\"> No record Present <span>";
                }
            }
            else
            {
                //DivContainer.InnerHtml = "<table width='80%' style='text-align:center'><tr><td> <br> <span style='color:red;'>  No record Present <span> </td></tr></table>";
                DivContainer.InnerHtml = "<br> <span class=\"font-red bold text-center\"> No record Present <span>";
            }

            ddlExprotTo.SelectedIndex = 0;
        }

        public int SetType(string type)
        {
            Int16 Itype = 0;

            switch (type.ToString().ToLower().Trim())
            {
                case "1":
                case "servicerequired":
                    Itype = 1;
                    strHeader = "Service Name";
                    strColName = "Maintain Required Services ";
                    break;
                case "2":
                case "reasoncanceld":
                    Itype = 2;
                    strHeader = " Reason ";
                    strColName = " Maintain Reason Cancelled ";
                    break;
                case "3":
                case "emailaddress":

                    Itype = 3;
                    strHeader = " Email";
                    strColName = " Team Email Address ";
                    break;
                case "4":
                case "employmentstatus":

                    Itype = 4;
                    strHeader = " Status";
                    strColName = " Employment Status ";
                    break;
                case "5":
                case "illnessinjury":

                    Itype = 5;
                    strHeader = " Illnes/Injury Name";
                    strColName = " Illness or Injury ";
                    break;
                case "6":
                case "claimclosed":

                    Itype = 6;
                    strHeader = "Reason ";
                    strColName = " Claim Closed Reason ";
                    break;
                case "reasonclosed":
                case "7":
                    Itype = 7;
                    strHeader = " Reason ";
                    strColName = " Reason Closed ";
                    break;
                case "broker":
                case "8":
                    Itype = 8;
                    strHeader = " Broker Name";
                    break;
                case "corporatepartner":
                case "9":
                    Itype = 9;
                    strHeader = " Partner Name";
                    strColName = " Corporate Partner";
                    break;
                case "10":
                case "producttype":

                    Itype = 10;
                    strHeader = " Type";
                    strColName = " Product Type ";
                    break;

                case "11":
                case "waitingperiod":
                    Itype = 11;
                    strHeader = " Period";
                    strColName = " Waiting Period ";
                    break;

                case "12":
                case "feecharged":
                    Itype = 12;
                    strHeader = "Fees";
                    strColName = "Modify fees";
                    break;
                case "13":
                case "brand":
                    Itype = 13;
                    strHeader = "Brand";
                    strColName = "Brand Type";
                    break;
                case "14":
                case "incapacitydefination":
                    Itype = 14;
                    strHeader = "Defination";
                    strColName = "Defination Type";
                    break;
                case "15":
                case "typeofvisit":
                    Itype = 15;
                    strHeader = "Visit";
                    strColName = "Visit Type";
                    break;
                case "16":
                case "typeofcalls":
                    Itype = 16;
                    strHeader = "Calls";
                    strColName = "Calls Type";
                    break;

                case "17":
                case "fundedtreatmenttype":
                    Itype = 17;
                    strHeader = "Type";
                    strColName = "Funded Treatment Type";
                    break;
                case "18":
                case "fundedtreatmentprovider":
                    Itype = 18;
                    strHeader = "Provider";
                    strColName = "Funded Treatment Provider";
                    break;

                default:
                    Itype = 0;
                    break;
            }
            if (Session["Type"] != null)
            {
                Session["Type"] = Itype;
            }
            else
            {
                Session.Add("Type", Itype);
            }
            return Itype;
        }

        protected void lnkExport_Click(object sender, EventArgs e)
        {
            int Itype = SetType(ddlReportName.SelectedItem.Value.Trim());
            //DataSet ds = new DataSet();
            //ds = MasterListBL.GetInformationReport(Itype);
            //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            //   {
            switch (ddlExprotTo.SelectedItem.Value)
            {
                case "2":
                    //ExportToExcel(ds.Tables[0]);
                    ExportToExcel();
                    break;
                case "1":
                    //ExportToPDf(ds.Tables[0]);
                    ConvertHTMLStringToPDF(DivContainer.InnerHtml.ToString());
                    break;
            }
            // }
        }

        public void ExportToPDf(DataTable dt)
        {

            //Create a dummy GridView
            GridView GridView1 = new GridView();
            GridView1.AllowPaging = false;
            GridView1.DataSource = dt;
            GridView1.DataBind();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=DataTable.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            //DivContainer.RenderControl(hw);
            GridView1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

        }

        public void ConvertHTMLStringToPDF(string strHTML)
        {
            DataSet ds = new DataSet();
            ds = MasterListBL.GetPDFLicensekey();
            string LicenseKeyPdf = string.Empty;
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    LicenseKeyPdf = ds.Tables[0].Rows[0]["PdfLicenseKey"].ToString();
                }

            }

            string htmlString = strHTML;// strstyles + genrateHTML(strHTML);

            PdfConverter pdfConverter = new PdfConverter();// set the license key
            pdfConverter.LicenseKey = LicenseKeyPdf;
            //pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Winnovative HTML to PDF Converter";


            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);


            //========================== Generating pdf ======================================
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename= CustomeReport_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
            response.Flush();
            response.BinaryWrite(pdfBytes);
            response.Flush();
            response.End();
            // ================================== End here ==================================

        }

        public void ExportToExcel()
        {
            try
            {
                //Create a dummy GridView
                //GridView GridView1 = new GridView();
                //GridView1.AllowPaging = false;
                //GridView1.DataSource = dt;
                //GridView1.DataBind();

                Response.ContentType = "application/x-msexcel";
                Response.AddHeader("Content-Disposition", "attachment; filename=CustomeReport_" + DateTime.Now.ToString("ddMMyyyy") + ".xls");
                Response.ContentEncoding = Encoding.UTF8;
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                DivContainer.RenderControl(hw);
                Response.Write(tw.ToString());
                Response.End();

                //Response.Clear();
                //Response.Buffer = true;
                //Response.AddHeader("content-disposition", "attachment;filename=DataTable.xls");
                //Response.Charset = "";
                //Response.ContentType = "application/vnd.ms-excel";
                //StringWriter sw = new StringWriter();
                //HtmlTextWriter hw = new HtmlTextWriter(sw);
                //for (int i = 0; i < GridView1.Rows.Count; i++)
                //    {
                //    //Apply text style to each Row
                //    GridView1.Rows[i].Attributes.Add("class", "textmode");
                //    }
                //GridView1.RenderControl(hw);
                ////style to format numbers to string
                //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                //Response.Write(style);
                //Response.Output.Write(sw.ToString());
                //Response.Flush();
                //Response.End();
            }


            catch (Exception)
            {

                throw;
            }
        }

    }
}