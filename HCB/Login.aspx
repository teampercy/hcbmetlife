﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="HCB.Login" %>

<%@ Register Src="~/ChangePassword.ascx" TagName="cp" TagPrefix="cp1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta http-equiv="Cache-Control" content="no-store" />
    <meta http-equiv="Pragma" content="no-cache" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- BEGIN LOGIN -->
    <div class="login">
        <div class="content col-md-5">
            <img src="Images/FirstDataLoginSuccess.png" vspace="25" alt="Login Success" style="width: 108%; margin-top: -10px;" />
        </div>
        <div id="tblLogin" runat="server" class="content col-md-7" style="background-color:azure">
            <!-- BEGIN LOGIN FORM -->
            <%-- Change Table data into Div so we doesnot change the id of table,its impact all over in code --%>
            <div runat="server" class="login-form">
                <%--<form class="login-form" action="#">--%>
                <h3 class="form-title font-green col-md-11">Login</h3>
                <div class="form-group col-md-11">
                    <span class="font-sm">Please enter your user name (email address) and password.</span>
                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <div class="row">
                        <div class="col-md-11">
                            <label class="control-label visible-ie8 visible-ie9">Username</label>

                            <asp:TextBox ID="txtUserName" runat="server" autocomplete="off" ValidationGroup="validatelogin"
                                class="form-control form-control-solid placeholder-no-fix" placeholder="Username" name="username" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ValidationGroup="validatelogin" ID="userRequiredfieldvalidator" runat="server" ControlToValidate="txtUserName"
                                CssClass="font-red bold font-sm" ErrorMessage="User Name/Email is required.">
							<img src='images/Exclamation.jpg'></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-11">
                            <label class="control-label visible-ie8 visible-ie9">Password</label>
                            <%--<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />--%>

                            <asp:TextBox ID="txtPassword" runat="server" class="form-control form-control-solid placeholder-no-fix" ValidationGroup="validatelogin"
                                placeholder="Password" name="password" TextMode="Password" MaxLength="15"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="passwordRequiredFieldValidator" runat="server" ControlToValidate="txtPassword" ValidationGroup="validatelogin"
                                CssClass="font-red bold font-sm" ErrorMessage="Password is required." Display="Dynamic">
							<img src='images/Exclamation.jpg'></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="statusLabel" runat="server" Class="alert font-md font-red bold"></asp:Label>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="font-red bold font-sm text-left"
                        DisplayMode="List" />
                </div>

                <div class="form-actions text-center">
                    <div class="row">
                        <div class="col-md-11">
                            <%--<button type="submit" class="btn green uppercase">Login</button>--%>
                            <asp:Button ID="loginButton" ValidationGroup="validatelogin" runat="server" class="btn green uppercase"
                                Text="Login" OnClick="loginButton_Click" />
                        </div>
                    </div>
                </div>

                <%--<div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        Enter username or password
                    </div>--%>

                <%--</form>--%>
            </div>
            <!-- END LOGIN FORM -->
        </div>
        <!-- BEGIN RESET PASSWORD FORM -->
        <div id="tblChangePassword" runat="server" class="content col-md-7" style="width:500px;background-color:azure;">
            <%--<form class="login-form" action="ChangePassword.ascx" method="post">--%>
            <div class="login-form">
                <h3 class="form-title font-green">Reset Password</h3>
                <hr width="100%" />
                <cp1:cp ID="ucChangePassword" Title="te" runat="server" />
            </div>
            <%--</form>--%>
        </div>
        <!-- END RESET PASSWORD FORM -->
    </div>

</asp:Content>
