﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using HCBBLL;
using System.IO;
using iTextSharp;
using iTextSharp.text.api;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using Winnovative.WnvHtmlConvert;
using System.Globalization;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class MIAverageTimeDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                if (Session["UserType"] != null && (Session["UserType"].ToString() == "Admin" || Session["UserType"].ToString() == "Inco"))
                {

                    lnkExport.Enabled = false;

                }
                else
                {
                    //Response.Redirect("~/Accessdenied.aspx", false);
                    Server.Transfer("~/Accessdenied.aspx");
                }
            }
        }

        protected void btnGetReport_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("vgGenReport");
                if (Page.IsValid)
                {
                    DateTime startDate = Convert.ToDateTime(txtFromDate.Text);
                    DateTime endDate = Convert.ToDateTime(txtToDate.Text);

                    string date = startDate.ToString("dd/MM/yyyy");
                    string enddate = endDate.ToString("dd/MM/yyyy");

                    ViewState["date"] = date;
                    lblAvgContactTime.Text = "";
                    lblAvgCallTime.Text = "";
                    lblRepCom.Text = "";
                    lblAvgFullCycle.Text = ""; 
                    DataSet ds = MasterListBL.GetClientReportDetail(startDate, endDate);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        //tblDetail.Visible = true;
                        sample_2.Visible = true;
                        lnkExport.Visible = true;
                        lnkExport.Enabled = true;
                        //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["SchemeName"].ToString()))
                        //{
                        //    lblSchemeName.Text = ds.Tables[0].Rows[0]["SchemeName"].ToString();
                        //}
                        //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["MetLifeSchemeNumber"].ToString()))
                        //{
                        //    lblSchemeNumber.Text = ds.Tables[0].Rows[0]["MetLifeSchemeNumber"].ToString();
                        //}
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgContactTime"].ToString()))
                        {
                            lblAvgContactTime.Text = ds.Tables[0].Rows[0]["AvgContactTime"].ToString() + " Days";
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgCallTime"].ToString()))
                        {
                            lblAvgCallTime.Text = ds.Tables[0].Rows[0]["AvgCallTime"].ToString() + " Days";
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgRepComTime"].ToString()))
                        {
                            lblRepCom.Text = ds.Tables[0].Rows[0]["AvgRepComTime"].ToString() + " Days";
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgFullCycleTime"].ToString()))
                        {
                            lblAvgFullCycle.Text = ds.Tables[0].Rows[0]["AvgFullCycleTime"].ToString() + " Days";
                        }
                        lblerr.Text = AntiXss.HtmlEncode("");
                        if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgContactTime"].ToString()) && string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgCallTime"].ToString()) && string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgRepComTime"].ToString()) && string.IsNullOrEmpty(ds.Tables[0].Rows[0]["AvgFullCycleTime"].ToString()))
                        {
                            //tblDetail.Visible = false;
                            sample_2.Visible = false;
                            lnkExport.Visible = false;
                            lnkExport.Enabled = false;
                        }
                    }
                    else
                    {
                        //tblDetail.Visible = false;
                        sample_2.Visible = false;
                        lnkExport.Enabled = false;
                        lnkExport.Visible = false;
                        lblAvgContactTime.Text = "";
                        lblAvgCallTime.Text = "";
                        lblRepCom.Text = "";
                        lblAvgFullCycle.Text = "";             
                        lblerr.Text = AntiXss.HtmlEncode("No record exists");
                        
                    }
                    
                }
            }

            catch (Exception ex)
            {

            }
        }
        protected void lnkExport_Click(object sender, EventArgs e)
        {
            try
            {
                //  int Itype = SetType(ddlReportName.SelectedItem.Value.Trim());
                //if (ViewState["dsreport"] != null)
                //{
                    // DataSet ds = (DataSet)ViewState["dsreport"];
                    // if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string strStyles = "<head><style> tr.even { background-color: #F5FDF5; }</style></head>";
                        switch (ddlExprotTo.SelectedItem.Value)
                        {
                            case "2":
                                ExportToExcel(strStyles);
                                break;
                            case "1":
                                //ExportToPDf(ds.Tables[0]);
                                StringWriter tw = new StringWriter();
                                HtmlTextWriter hw = new HtmlTextWriter(tw);
                                divDetail.RenderControl(hw);
                                //Response.Write(style + tw.ToString());
                                ConvertHTMLStringToPDF(strStyles, hw.InnerWriter.ToString(), (string)ViewState["date"] + "");
                                break;
                        }
                    //}
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void ConvertHTMLStringToPDF(string strStyle, string strHTML, string strDate)
        {
            DataSet ds = new DataSet();
            ds = MasterListBL.GetPDFLicensekey();
            string LicenseKeyPdf = string.Empty;
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    LicenseKeyPdf = ds.Tables[0].Rows[0]["PdfLicenseKey"].ToString();
                }

            }
            string htmlString = strStyle + strHTML;// strstyles + genrateHTML(strHTML);
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = LicenseKeyPdf;
            //pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Winnovative HTML to PDF Converter";

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);

            //Please Write the following file checking on the top before calling all store procedures generatepdf() function

            //string PdfPath = ".pdf";
            //FileInfo fFile = new FileInfo(PdfPath);
            //If Not fFile.Exists Then
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
            //End If

            //if (!fFile.Exists)
            //  {
            // MessageBox.Show("File Not Found")
            //========================== Generating pdf ======================================
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename=" + AntiXss.HtmlEncode(strDate) + "_" + AntiXss.HtmlEncode(DateTime.Now.ToString("ddMMyyyy")) + ".pdf; size=" + pdfBytes.Length.ToString());
            response.Flush();
            response.BinaryWrite(pdfBytes);
            response.Flush();
            response.End();
            // ================================== End here ==================================

        }

        private string genrateHTML(string strtbldata)
        {
            string strHTML = "";

            strHTML = "<table border='0' width='95%' height='98%' class='Box1' style='border-collapse: collapse'";
            strHTML += "            align='center' cellspacing='0' cellpadding='0'>";
            strHTML += "           <tr align='center'>";
            strHTML += "            <td>";
            strHTML += "                 &nbsp;</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "<td align='center'>";
            strHTML += "<table border='0' cellspacing='0' cellpadding='0' width='98%' align='center'>";
            strHTML += "<tr>";
            strHTML += "<td width='70%' align='left'>";
            strHTML += "&nbsp;";
            strHTML += "Logo";
            strHTML += "</td>";
            strHTML += "<td align='Right' width='30%' valign='top'>";

            strHTML += "Heading";

            strHTML += " </td>";
            strHTML += "</tr>";
            strHTML += " </table>";
            strHTML += "</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "   <td>";
            strHTML += "<table border='0' cellspacing='3' width='100%'>";
            strHTML += "<tr>";
            strHTML += "                                    <td valign='top' align='center'>";
            strHTML += strtbldata;
            strHTML += "        </td>";
            strHTML += "                                </tr>";
            strHTML += "              </table>";
            strHTML += "</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "<td>";
            strHTML += "&nbsp;</td>";
            strHTML += "</tr>";
            strHTML += "  <tr>";
            strHTML += "      <td>";
            strHTML += "      <table cellpadding='0' cellspacing='0'>";
            strHTML += "          <tr>";
            strHTML += "              <td>";
            strHTML += "                 &nbsp;</td>";
            strHTML += "             <td class='textlab6'>";
            strHTML += "                 <u>Notes / Instructions</u>:</td>";
            strHTML += "          </tr>";
            strHTML += "      </table>";
            strHTML += "    </td>";
            strHTML += "  </tr>";
            strHTML += "   <tr>";
            strHTML += "      <td>";
            strHTML += "         <table border='0' cellspacing='0' cellpadding='0' width='85%'>";
            strHTML += "              <tr>";
            strHTML += "                 <td>";
            strHTML += "                     &nbsp;</td>";
            strHTML += "                 <td class='textlab8'>";
            strHTML += " instrunctuion";
            strHTML += "               </td>";
            strHTML += "            </tr>";
            strHTML += "           <tr>";
            strHTML += "              <td>";
            strHTML += "                  &nbsp;</td>";
            strHTML += "       </tr>";
            strHTML += "      <tr>";
            strHTML += "          <td>";
            strHTML += "            &nbsp;</td>";
            strHTML += "        <td class='textlab8'>";
            strHTML += "instruction";
            strHTML += "       </td>";
            strHTML += "    </tr>";
            strHTML += "  </table>";
            strHTML += "</td>";
            strHTML += "</tr>";
            strHTML += " <tr>";
            strHTML += "   <td>";
            strHTML += "  &nbsp;</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "  <td>";
            strHTML += "      &nbsp;</td>";
            strHTML += " </tr>";
            strHTML += "</table>'";

            return strHTML;


        }

        public void ExportToExcel(string style)
        {
            try
            {


                Response.ContentType = "application/x-msexcel";
                Response.AddHeader("Content-Disposition", "attachment; filename=ExcelFile.xls");
                Response.ContentEncoding = Encoding.Default;
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                //tblDetail.RenderControl(hw);
                sample_2.RenderControl(hw);
                Response.Write(style + tw.ToString());
                Response.End();


            }


            catch (Exception ex)
            {

                throw;
            }
        }

     
    }
}