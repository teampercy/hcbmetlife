﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="MasterList.aspx.cs" Inherits="HCB.MasterList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //var popupStatus = 0;
        var popupDivVal = '';

        function loadPopup(popupDiv) {
            //debugger;
            popupDivVal = popupDiv;

            if (popupDivVal == "#AssessPopup") {
                $('#AssessPopup').modal('show');
            }
            else {
                $('#Popup').modal('show');
            }
            GridDataTable();        //bind datatable when click on cancel button 
            //loads popup only if it is disabled

            //if (popupStatus == 0) {
            //    centerPopup(popupDiv);
            //    $("#backgroundPopup1").css({
            //        "opacity": "0.7"
            //    });

            //    $("#backgroundPopup1").fadeIn("slow");
            //    $(popupDiv).fadeIn("slow");
            //    popupStatus = 1;
            //}
            return false;
        }
        //$("#backgroundPopup1").click(function () {

        //    disablePopup(popupDivVal);
        //});
        //function disablePopup(popupdiv) {

        //    //disables popup only if it is enabled

        //    if (popupStatus == 1) {
        //        $("#backgroundPopup1").fadeOut("slow");
        //        $(popupdiv).fadeOut("slow");
        //        popupStatus = 0;
        //    }
        //}
        //function centerPopup(popupdiv) {
        //    //request data for centering

        //    var windowWidth = document.documentElement.clientWidth;
        //    var windowHeight = document.documentElement.clientHeight;
        //    var popupHeight = $(popupdiv).height();
        //    var popupWidth = $(popupdiv).width();
        //    //centering  
        //    $(popupdiv).css({
        //        "position": "absolute",
        //        "top": 200,
        //        // windowHeight / 2 - popupHeight / 2,
        //        "left": windowWidth / 2 - popupWidth / 2
        //    });
        //    //only need force for IE6  

        //    $("#backgroundPopup1").css({
        //        "height": windowHeight
        //    });
        //    return false;
        //}
        function closepopup() {
            debugger;
            if (popupDivVal == '#Popup') {
                //disablePopup('#Popup');
                $('#Popup').modal('hide');
                document.getElementById('<%= txtName.ClientID %>').value = "";
               <%--  document.getElementById('<%= txtSchemeNumber.ClientID %>').value = "";--%>
            }
            if (popupDivVal == '#AssessPopup') {
                //disablePopup('#AssessPopup');
                $('#AssessPopup').modal('hide');
                document.getElementById('<%= txtAName.ClientID %>').value = "";
                document.getElementById('<%= txtAEmail.ClientID %>').value = "";
                document.getElementById('<%= txtATeleNum.ClientID %>').value = "";
            }
            GridDataTable();        //bind datatable when click on cancel button 
            $(".modal-backdrop").remove();
           
        }
    </script>
    <script type="text/javascript">

        function PopupAddNew() {
            debugger;
            var mySessionVar = '<%= Session["Type"] %>';
            if (mySessionVar != null) {

                if (mySessionVar == 8) {
                    //loadPopup('#AssessPopup');
                    $('#lnkAdd').attr('data-target', '#AssessPopup');
                    $('#AssessPopup').modal('show');
                }
                else {
                    $('#lnkAdd').attr('data-target', '#Popup');
                    $('#Popup').modal('show');
                    //loadPopup('#Popup');
                }
            }
            return false;
        }
        function ConfirmDelete(str) {
            var v = confirm(' You sure want to delete ' + str + ' ?');
            alert(v);
            return false;
        }
    </script>

    <script type="text/javascript">
        /*Javascript for Gridview Datatable*/
        $(document).ready(function () {
            GridDataTable();                                 
        });

        function GridDataTable() {                    
            $("table.table-striped").prepend($("<thead></thead>").append($("table.table-striped").find("tr:first"))).dataTable();
            //$("#<%=Gvassessor.ClientID%>").prepend($("<thead></thead>").append($("#<%=Gvassessor.ClientID%>").find("tr:first"))).dataTable();                                  
            $('table.table-striped').DataTable();
            return false;
        }        
    </script>

    <style type="text/css">
        /*Css for Modal popup dispay in center*/
        .modal {
        }

        .vertical-alignment-helper {
            display: table;
            height: 100%;
            width: 100%;
        }

        .vertical-align-center {
            /* To center vertically */
            display: table-cell;
            vertical-align: middle;
        }

        .modal-content {
            /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
            width: inherit;
            height: inherit;
            /* To center horizontally */
            margin: 0 auto;
        }
    </style>
    <%-- <style type="text/css">
        #backgroundPopup1 {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6*/
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: #000000;
            border: 1px solid #cecece;
            z-index: 4;
        }

        .popupContact {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6 height: 200px;*/
            width: 550px;
            background: #FFFFFF;
            border: 2px solid #cecece;
            z-index: 5;
            font-size: 13px;
        }

            .popupContact tr {
                height: 40px;
                text-align: right;
                vertical-align: text-top;
            }

        .popupContactClose {
            font-size: 14px;
            line-height: 14px;
            right: 6px;
            top: 4px;
            position: absolute;
            color: #6fa5fd;
            font-weight: 700;
            display: block;
        }

        #overlay {
            position: fixed;
            z-index: 9 !important;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=80);
            opacity: 0.80;
            -moz-opacity: 0.80;
        }
    </style>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="scManager" runat="server">
    </asp:ScriptManager>
    <%--<div id="backgroundPopup1" onclick="closepopup()">
    </div>--%>
    <div class="portlet box blue" id="divNotFee" runat="server">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </div>
        </div>

        <div class="portlet-body font-md">
            <div class="form-group">

                <asp:LinkButton ID="lnkAdd" runat="server" CssClass="btn red btn-circle pull-right" data-toggle="modal"
                    OnClientClick="return PopupAddNew();"><i class="fa fa-plus"></i> Add New</asp:LinkButton>
            </div>
            <br /><br />
            <%--<div class="table-scrollable">--%>

                <asp:UpdatePanel ID="upGVList" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvReasonList" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                            OnRowCreated="gvReasonList_RowCreated1" OnRowDataBound="gvReasonList_RowDataBound" CssClass="table table-striped table-bordered table-hover">
                            <HeaderStyle CssClass="font-white bg-green-steel" />
                            
                            <Columns>
                                <asp:BoundField HeaderText="Name" DataField="Name" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center"/>
                                <asp:BoundField HeaderText="Scheme Number" DataField="RelatedValue" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center"/>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkeEdit" runat="server" CommandName="edt" OnCommand="lnk_Command" CssClass="btn btn-outline btn-circle btn-sm purple"
                                            CommandArgument='<%# string.Concat(Eval("ID"),",",Eval("Name"),",",Eval("RelatedValue")) %>'><i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--OnCommand="lnk_Command"  onrowcommand="gvReasonList_RowCommand"--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%--<asp:ImageButton Visible="true" ID="lnkeDelete" runat="server" CommandArgument='<% #Eval("ID") %>'
                                            OnClientClick="return confirm('You want to delete this record ?');" OnCommand="lnk_Command"
                                            CommandName="del" ImageUrl="Images/Del.gif" />--%>
                                        <asp:LinkButton Visible="true" ID="lnkeDelete" runat="server" CommandArgument='<% #Eval("ID") %>'
                                            OnClientClick="return confirm('You want to delete this record ?');" OnCommand="lnk_Command"
                                            CommandName="del" CssClass="btn btn-outline btn-circle dark btn-sm black">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hdntype" Value="" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="Gvassessor" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                            OnRowCreated="gvReasonList_RowCreated1" CssClass="table table-striped table-bordered table-hover">
                            <HeaderStyle CssClass="font-white bg-green-steel" />
                            <Columns>
                                <asp:BoundField HeaderText="Name" DataField="Name" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center"/>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkeEdit1" runat="server" CommandName="edt" OnCommand="lnk_Command" CssClass="btn btn-outline btn-circle btn-sm purple"
                                            CommandArgument='<%# string.Concat(Eval("ID"),",",Eval("Name"),",",Eval("TeamEmailID"),",",Eval("TeamPhoneNo")) %>'>
                                            <i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--OnCommand="lnk_Command"  onrowcommand="gvReasonList_RowCommand"--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%--<asp:ImageButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("ID") %>'
                                            OnClientClick="return confirm('You want to delete this record ?');" OnCommand="lnk_Command"
                                            CommandName="del" ImageUrl="Images/Del.gif" />--%>
                                        <asp:LinkButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("ID") %>'
                                            OnClientClick="return confirm('You want to delete this record ?');" OnCommand="lnk_Command"
                                            CommandName="del" CssClass="btn btn-outline btn-circle dark btn-sm black">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="HiddenField1" Value="" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Modal -->
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <div id="Popup" class="modal fade in" role="dialog">
                            <div class="vertical-alignment-helper">
                                <div class="modal-dialog vertical-align-center">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title font-green bold">
                                                <asp:Label ID="lblhead" runat="server"></asp:Label></h4>
                                        </div>
                                        <div class="modal-body form">
                                            <div class="form-horizontal">
                                                <div class="form-body">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="col-md-7">
                                                                    <asp:ValidationSummary ID="ValidationSummary1" CssClass="font-red bold font-sm text-left" ValidationGroup="validateATxt"
                                                                        runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <asp:Label ID="lblvalue" runat="server" class="col-md-4 control-label font-md"></asp:Label>
                                                                <div class="col-md-5">
                                                                    <asp:TextBox ID="txtName" ValidationGroup="validateTxt" CssClass="form-control" MaxLength="30"
                                                                        runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ID="rqftxtName" runat="server" ControlToValidate="txtName"
                                                                        ErrorMessage="Can not be blank" CssClass="font-red bold font-sm" ValidationGroup="validateTxt"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="RegExName" runat="server" ValidationGroup="validateTxt"
                                                                        ValidationExpression="^[a-zA-Z0-9\s.\-_']+$" ControlToValidate="txtName"
                                                                        ErrorMessage="Please enter valid data."></asp:RegularExpressionValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group" id="trSchemeNumber" runat="server">
                                                                <asp:Label ID="lblSchemeNumber" Text="Scheme Number" runat="server" class="col-md-4 control-label font-md"></asp:Label>
                                                                <div class="col-md-5">
                                                                    <asp:TextBox ID="txtSchemeNumber" ValidationGroup="validateTxt" CssClass="form-control" MaxLength="30"
                                                                        runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSchemeNumber"
                                                                        ErrorMessage="Can not be blank" CssClass="font-red bold font-sm" ValidationGroup="validateTxt"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="validateTxt"
                                                                        ValidationExpression="^[a-zA-Z0-9\s.\-_']+$" ControlToValidate="txtSchemeNumber"
                                                                        ErrorMessage="Please enter valid data."></asp:RegularExpressionValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <asp:Button runat="server" ID="btnSave" ValidationGroup="validateTxt" Text="Save" CssClass="btn btn-circle green bold" OnClick="btnSave_Click" />
                                            <asp:Button ID="btncancle" runat="server" data-dismiss="modal" Text="Cancel" OnClick="btn_Cancle" OnClientClick="return closepopup();"
                                                CssClass="btn btn-circle grey-salsa btn-outline" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Modal -->
                <asp:UpdatePanel ID="updAssess" runat="server">
                    <ContentTemplate>
                        <div id="AssessPopup" class="modal fade" role="dialog">
                            <div class="vertical-alignment-helper">
                                <div class="modal-dialog vertical-align-center">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title font-green bold">Assessor Detail</h4>
                                        </div>
                                        <div class="modal-body form">
                                            <div class="form-horizontal">
                                                <div class="form-body">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="col-md-7">
                                                                    <asp:ValidationSummary ID="validateUserSummary" CssClass="font-red bold font-sm text-left" ValidationGroup="validateATxt"
                                                                        runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label font-md">Assessor Name</label>
                                                                <div class="col-md-5">
                                                                    <asp:TextBox ID="txtAName" ValidationGroup="validateATxt" runat="server" MaxLength="30"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAName"
                                                                        Display="Dynamic" CssClass="ErrorMessage" ErrorMessage="Assessor name required"
                                                                        ValidationGroup="validateATxt"><img src='../images/Exclamation.jpg'> </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label font-md">Email ID</label>
                                                                <div class="col-md-5">
                                                                    <asp:TextBox ID="txtAEmail" ValidationGroup="validateATxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ID="rqftxtAEmail" runat="server" ControlToValidate="txtAEmail"
                                                                        Display="Dynamic" CssClass="ErrorMessage" ErrorMessage="Email Required" ValidationGroup="validateATxt"><img src='../images/Exclamation.jpg'> </asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="RGEtxtUserName" ControlToValidate="txtAEmail"
                                                                        Display="Dynamic" CssClass="ErrorMessage" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                        ValidationGroup="validateATxt" runat="server" ErrorMessage="Invalid Email address"><img src='../images/Exclamation.jpg'> 
                                                                    </asp:RegularExpressionValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label font-md">Telephone Number</label>
                                                                <div class="col-md-5">
                                                                    <asp:TextBox ID="txtATeleNum" runat="server" MaxLength="12" ValidationGroup="validateATxt"
                                                                        onkeypress="return CheckValues(event.keyCode, event.which,this);" CssClass="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ID="RQFtxtATeleNum" runat="server" ControlToValidate="txtATeleNum"
                                                                        Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateATxt" ErrorMessage="TelePhone No. Required">
                                                                <img src='../images/Exclamation.jpg'>     
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <asp:Button runat="server" ID="Button1" ValidationGroup="validateATxt" Text="Save" CssClass="btn btn-circle green"
                                                CommandName="assessor" OnClick="btnSave_Click" />
                                            <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="btn_Cancle" OnClientClick="return closepopup();"
                                                CssClass="btn btn-circle grey-salsa btn-outline" data-dismiss="modal" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            <%--</div>--%>

        </div>
    </div>

    <div class="portlet box blue" id="divFee" runat="server" style="display: none;">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblFeeTitle" runat="server" Text="Fees Structure"></asp:Label>
            </div>
        </div>
        <div class="portlet-body form font-md">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="form-group">
                        <asp:Label ID="lblMessage" runat="server" CssClass="font-red bold"></asp:Label>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblHomeVisit" runat="server" CssClass="col-md-3 col-md-offset-3 control-label" Text="Home Visit Fee: ">
                        </asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="txtHomeVisit" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblCall1Fee" runat="server" Text="Call1 Fee: " CssClass="col-md-3 col-md-offset-3 control-label">
                        </asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="txtCall1Fee" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblCall2Fee" runat="server" Text="Call2 Fee: " CssClass="col-md-3 col-md-offset-3 control-label">
                        </asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="txtCall2Fee" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblCall3Fee" runat="server" Text="Call3 Fee: " CssClass="col-md-3 col-md-offset-3 control-label">
                        </asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="txtCall3Fee" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblCallAborted" runat="server" Text="Aborted Call Fee: " CssClass="col-md-3 col-md-offset-3 control-label">
                        </asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="txtCallAborted" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="form-actions">
                        <asp:Button ID="btnSaveFee" runat="server" Text="Save" OnClick="btnSaveFee_Click" CssClass="btn btn-circle blue bold" />
                    </div>

                </div>
            </div>
        </div>
    </div>
    
</asp:Content>
