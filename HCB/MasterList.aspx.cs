﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HCBBLL;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class MasterList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
                {
                    if (Page.RouteData.Values["type"] != null)
                    {
                        string strRoute = Page.RouteData.Values["type"] + "";
                        if (strRoute.Trim() != "")
                        //if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                        {
                            //string Type = Request.QueryString["type"].ToString().Trim();
                            if (SetType(strRoute) != 0)
                            {
                                FillReasonList();
                            }
                            else
                            {
                                Response.Redirect("~/PageNotFound.aspx", false);
                            }
                        }
                        else
                        {

                        }

                    }
                    else
                    {
                        Response.Redirect("~/PageNotFound.aspx", false);
                    }
                    if (Session["Type"] != null)
                    {
                        if (Session["Type"].ToString() == "19")
                        {
                            trSchemeNumber.Visible = true;
                        }
                        else
                        {
                            trSchemeNumber.Visible = false;
                        }
                    }
                }

                else
                {

                    //ClientScript.RegisterStartupScript(typeof(Page), "MessagePopUp",
                    //"alert('ABCDEFGH'); window.location.href = 'Accessdenied.aspx';", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Permission",
                    //    "alert('you dont have rights to view this page.');window.location.href ='~/Accessdenied.aspx';",true);
                    //Response.Redirect("~/Accessdenied.aspx", false);
                    Server.Transfer("~/Accessdenied.aspx");
                }
            }
        }

        private void FillReasonList()
        {
            MasterListBL ML = new MasterListBL();
            HCBFeeCharged FeeChg = new HCBFeeCharged();
            DataSet ds = new DataSet();

            try
            {
                if (Session["Type"] != null)
                {
                    int IType = (Int16)Session["Type"];

                    
                    if (IType == 8)
                    {
                        gvReasonList.Visible = false;
                        ds = ML.GetAssessorList();
                        Gvassessor.DataSource = ds.Tables[0];
                        Gvassessor.DataBind();
                    }
                    else if (IType == 12)
                    {
                        try
                        {
                            gvReasonList.Visible = false;
                            lnkAdd.Visible = false;
                            Gvassessor.Visible = false;
                            divFee.Style.Add("display", "block");
                            divNotFee.Style.Add("display", "none");
                            ds = FeeChg.GetFeeCharged();

                            if (ds != null)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    if (ds.Tables[0].Rows[0]["HomeVisitCurrency"].ToString() != "")
                                    {
                                        txtHomeVisit.Text = ds.Tables[0].Rows[0]["HomeVisitCurrency"].ToString();
                                    }
                                    else
                                    {
                                        txtHomeVisit.Text = "";
                                    }

                                    if (ds.Tables[0].Rows[0]["Call1Fee"].ToString() != "")
                                    {
                                        txtCall1Fee.Text = ds.Tables[0].Rows[0]["Call1Fee"].ToString();
                                    }
                                    else
                                    {
                                        txtCall1Fee.Text = "";
                                    }

                                    if (ds.Tables[0].Rows[0]["Call2Fee"].ToString() != "")
                                    {
                                        txtCall2Fee.Text = ds.Tables[0].Rows[0]["Call2Fee"].ToString();
                                    }
                                    else
                                    {
                                        txtCall2Fee.Text = "";
                                    }

                                    if (ds.Tables[0].Rows[0]["Call3Fee"].ToString() != "")
                                    {
                                        txtCall3Fee.Text = ds.Tables[0].Rows[0]["Call3Fee"].ToString();
                                    }
                                    else
                                    {
                                        txtCall3Fee.Text = "";
                                    }

                                    if (ds.Tables[0].Rows[0]["CallAbortedFee"].ToString() != "")
                                    {
                                        txtCallAborted.Text = ds.Tables[0].Rows[0]["CallAbortedFee"].ToString();
                                    }
                                    else
                                    {
                                        txtCallAborted.Text = "";
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            txtHomeVisit.Text = "";
                            txtCall1Fee.Text = "";
                            txtCall2Fee.Text = "";
                            txtCall3Fee.Text = "";
                            txtCallAborted.Text = "";
                        }
                    }
                    else
                    {
                        Gvassessor.Visible = false;
                        ds = ML.GetMasterList(IType);
                        gvReasonList.DataSource = ds.Tables[0];
                        gvReasonList.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ds = null;
                gvReasonList.DataSource = ds;
                gvReasonList.DataBind();
            }
        }

        public int SetType(string type)
        {
            Int16 Itype = 0;

            switch (type.ToString().ToLower().Trim())
            {
                case "1":
                case "servicerequired":
                    Itype = 1;
                    lblTitle.Text = AntiXss.HtmlEncode("Maintain Required Services "); 
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode("Service Name");
                    lblvalue.Text = AntiXss.HtmlEncode("Service Name"); 
                    lblhead.Text = AntiXss.HtmlEncode("Maintain Required Services "); 
                    break;
                case "2":
                case "reasoncanceld":
                    Itype = 2;
                    lblTitle.Text = AntiXss.HtmlEncode(" Maintain Reason Cancelled ");
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Reason "); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Reason "); 
                    lblhead.Text =AntiXss.HtmlEncode(" Maintain Reason Cancelled "); 
                    break;
                case "3":
                case "emailaddress":

                    Itype = 3;
                    lblTitle.Text = AntiXss.HtmlEncode(" Team Email Address "); 
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Email");
                    lblvalue.Text = AntiXss.HtmlEncode(" Email"); 
                    lblhead.Text = AntiXss.HtmlEncode(" Team Email Address "); 
                    break;
                case "4":
                case "employmentstatus":

                    Itype = 4;
                    lblTitle.Text = AntiXss.HtmlEncode(" Employment Status "); 
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Status"); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Status"); 
                    lblhead.Text = AntiXss.HtmlEncode(" Employment Status "); 
                    break;
                case "5":
                case "illnessinjury":

                    Itype = 5;
                    lblTitle.Text = AntiXss.HtmlEncode(" Illness or Injury "); 
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Illness or Injury "); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Illness or Injury Name"); 
                    lblhead.Text = AntiXss.HtmlEncode(" Illness or Injury "); 
                    break;
                case "6":
                case "claimclosed":

                    Itype = 6;
                    lblTitle.Text =AntiXss.HtmlEncode(" Claim Closed Reason ");  
                    gvReasonList.Columns[0].HeaderText =AntiXss.HtmlEncode("Reason "); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Reason Closed"); 
                    lblhead.Text = AntiXss.HtmlEncode(" Claim Closed Reason "); 
                    break;
                case "reasonclosed":
                case "7":
                    Itype = 7;
                    lblTitle.Text = AntiXss.HtmlEncode(" Reason Closed "); 
                    gvReasonList.Columns[0].HeaderText =AntiXss.HtmlEncode(" Reason "); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Reason "); 
                    lblhead.Text = AntiXss.HtmlEncode(" Reason Closed "); 
                    break;
                case "broker":
                case "8":
                    Itype = 8;
                    lblTitle.Text = AntiXss.HtmlEncode(" Broker"); 
                    Gvassessor.Columns[0].HeaderText =AntiXss.HtmlEncode(" Broker Name");  
                    break;
                case "corporatepartner":
                case "9":
                    Itype = 9;
                    lblTitle.Text = AntiXss.HtmlEncode(" Corporate Partner"); 
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Partner Name"); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Partner Name"); 
                    lblhead.Text = AntiXss.HtmlEncode(" Corporate Partner");
                    break;
                case "10":
                case "producttype":

                    Itype = 10;
                    lblTitle.Text =AntiXss.HtmlEncode(" Product Type ");  
                    gvReasonList.Columns[0].HeaderText =AntiXss.HtmlEncode(" Type"); 
                    lblvalue.Text =AntiXss.HtmlEncode(" Type");  
                    lblhead.Text =AntiXss.HtmlEncode(" Product Type ");  
                    break;

                case "11":
                case "waitingperiod":
                    Itype = 11;
                    lblTitle.Text = AntiXss.HtmlEncode(" Waiting Period "); 
                    gvReasonList.Columns[0].HeaderText =AntiXss.HtmlEncode(" Period"); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Period");
                    lblhead.Text = AntiXss.HtmlEncode(" Waiting Period "); 
                    break;
                case "12":
                case "feecharged":
                    Itype = 12;
                    //lblTitle.Text = " Fees Structure ";
                    //gvReasonList.Columns[0].HeaderText = " Fees";
                    //lblvalue.Text = " Fees";
                    //lblhead.Text = " Modify Fees ";
                    break;
                case "13":
                case "brand":
                    Itype = 13;
                    lblTitle.Text = AntiXss.HtmlEncode(" Brand List "); 
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Brand");
                    lblvalue.Text = AntiXss.HtmlEncode(" Brand"); 
                    lblhead.Text =AntiXss.HtmlEncode(" Brand Type ");  
                    break;
                case "14":
                case "incapacitydefination":
                    Itype = 14;
                    lblTitle.Text = AntiXss.HtmlEncode(" Defination List "); 
                    gvReasonList.Columns[0].HeaderText =AntiXss.HtmlEncode(" Defination"); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Defination"); 
                    lblhead.Text = AntiXss.HtmlEncode(" Defination Type "); 
                    break;

                case "15":
                case "typeofvisit":
                    Itype = 15;
                    lblTitle.Text = AntiXss.HtmlEncode(" Type Of Vsit "); 
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Visit");
                    lblvalue.Text = AntiXss.HtmlEncode(" Visit"); 
                    lblhead.Text = AntiXss.HtmlEncode(" Visit Type ");
                    break;

                case "16":
                case "typeofcalls":
                    Itype = 16;
                    lblTitle.Text = AntiXss.HtmlEncode(" Type Of Calls ");
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Calls"); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Calls");
                    lblhead.Text = AntiXss.HtmlEncode(" Calls Type "); 
                    break;
                case "17":
                case "fundedtreatmenttype":
                    Itype = 17;
                    lblTitle.Text =AntiXss.HtmlEncode(" Funded Treatment Type ");
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Funded Treatment Type "); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Type");
                    lblhead.Text = AntiXss.HtmlEncode(" Funded Treatment Type ");
                    break;

                case "18":
                case "fundedtreatmentprovider":
                    Itype = 18;
                    lblTitle.Text = AntiXss.HtmlEncode(" Funded Treatment Provider ");
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Funded Treatment Provider "); 
                    lblvalue.Text = AntiXss.HtmlEncode(" Provider");
                    lblhead.Text = AntiXss.HtmlEncode(" Funded Treatment Provider "); 
                    break;               
                case "19":
                case "schemename":
                    Itype = 19;
                    lblTitle.Text = AntiXss.HtmlEncode(" Scheme Name ");
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" Scheme Name ");
                    lblvalue.Text = AntiXss.HtmlEncode(" Scheme Name");
                    lblhead.Text = AntiXss.HtmlEncode(" Scheme Name Type ");
                    break;
                case "20":
                case "schemenumber":
                    Itype = 20;
                    lblTitle.Text = AntiXss.HtmlEncode(" MetLife Scheme Number ");
                    gvReasonList.Columns[0].HeaderText = AntiXss.HtmlEncode(" MetLife Scheme Number ");
                    lblvalue.Text = AntiXss.HtmlEncode(" MetLife Scheme Number");
                    lblhead.Text = AntiXss.HtmlEncode(" MetLife Scheme Number Type ");
                    break;
                default:
                    Itype = 0;
                    break;
            }
            if (Session["Type"] != null)
            {
                Session["Type"] = Itype;
            }
            else
            {
                Session.Add("Type", Itype);
            }
            return Itype;
        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {

            lblTitle.Text = AntiXss.HtmlEncode(lblTitle.Text + "__" + e.CommandName.Trim().ToLower());
            string[] Val = e.CommandArgument.ToString().Split(',');
            if (e.CommandName.Trim().ToLower() == "edt")
            {
                if (Session["ListId"] != null)
                {

                    Session["ListId"] = Val[0];
                }
                else Session.Add("LIstID", Val[0].ToString());

                hdntype.Value = "edit";

                int IType = (Int16)Session["Type"];
                if (IType == 19)
                {
                    trSchemeNumber.Visible = true;
                }
                else
                {
                    trSchemeNumber.Visible = false;
                }               
                if (IType == 8)
                {
                    txtAName.Text = Val[1];
                    txtAEmail.Text = Val[2];
                    txtATeleNum.Text = Val[3];
                    ScriptManager.RegisterStartupScript(Gvassessor, this.GetType(), "edit", "loadPopup('#AssessPopup');", true);                    
                }
                else
                {
                    txtName.Text = Val[1].ToString();
                    txtSchemeNumber.Text = Val[2].ToString();
                    ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "edit", "loadPopup('#Popup');", true);
                    
                }
            }
            else if (e.CommandName.Trim().ToLower() == "del")
            {
                DeleteList(Convert.ToInt32(e.CommandArgument.ToString()));
                //ScriptManager.RegisterStartupScript(gvReasonList,this.GetType,"Del",
            }
        }

        protected void btn_Cancle(object sender, EventArgs e)
        {
            hdntype.Value = "new";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "new", "GridDataTable();", true);
            //ScriptManager.RegisterStartupScript(Gvassessor, this.GetType(), "new", "GridDataTable();", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            
            //ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "", "alert('Hi_" + hdntype.Value + "_" + (string)Session["ListId"] + "')", true);
            if (hdntype.Value == "edit")
            {
                //updateAmendment();
                if (Session["ListId"] != null)
                {
                    string IlistId = (string)Session["ListId"];
                    Int16 IType = 0;
                    if (Session["Type"] != null)
                    {
                        IType = (Int16)Session["Type"];
                        MasterListBL ML = new MasterListBL();
                        if (IType != 8)
                        {
                            ML.UpdateMasterList(Convert.ToInt32(IlistId), txtName.Text, IType,txtSchemeNumber.Text.Trim());
                        }
                        else
                        {
                            ML.UpdateAssessorList(Convert.ToInt32(IlistId), txtAEmail.Text, txtATeleNum.Text, txtAName.Text);
                        }
                        Session["ListId"] = null;
                        hdntype.Value = "new";
                        FillReasonList();
                        
                        if (IType != 8) {                            
                            ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "edit", "closepopup();", true);                            
                        }
                        else
                        { 
                            ScriptManager.RegisterStartupScript(Gvassessor, this.GetType(), "edit", "closepopup();", true);                            
                        }
                    }
                }
                else
                {

                    if (Session["Type"] != null && (int)Session["Type"] != 8)
                    { 
                        ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "Close", "closepopup();", true);                        
                    }
                    else
                        ScriptManager.RegisterStartupScript(Gvassessor, this.GetType(), "Close", "closepopup();", true);                    
                }
            }
            else
            {
                string IlistId = (string)Session["ListId"];
                Int16 IType = 0;
                if (Session["Type"] != null)
                {
                    IType = (Int16)Session["Type"];
                    MasterListBL ML = new MasterListBL();
                    if (IType != 8)
                    {
                        ML.InsertMasterList(txtName.Text, IType, txtSchemeNumber.Text.Trim());
                    }
                    else
                    {
                        ML.InsertAssessorList(txtAEmail.Text, txtATeleNum.Text, txtAName.Text); ;
                    }
                    FillReasonList();
                    if (IType == 8)
                        ScriptManager.RegisterStartupScript(Gvassessor, this.GetType(), "Insert", "closepopup();", true);
                    else
                    {                        
                        ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "Insert", "closepopup();", true);                        
                    }
                }
            }

        }

        public void DeleteList(int id)
        {
            MasterListBL ML = new MasterListBL();
            int Itype = 0;
            if (Session["Type"] != null)
            {
                Itype = (Int16)Session["Type"];
                if (Itype != 8)
                    ML.DeleteMasterList(id);
                else
                    ML.DeleteAssessorList(id);
                FillReasonList();
                if (Itype == 8)
                    ScriptManager.RegisterStartupScript(Gvassessor, this.GetType(), "del", "closepopup();", true);
                else
                { 
                    ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "del", "closepopup();", true);                    
                }
            }
            else
            {

            }
        }

        protected void gvReasonList_RowCreated1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                {
                    if (e.Row.FindControl("lnkeDelete") != null)
                    {
                        //ImageButton imgbtn = (ImageButton)e.Row.FindControl("lnkeDelete");
                        //imgbtn.OnClientClick = " Confirm('You want to delete this record?');";
                        //imgbtn.OnClientClick = "return ConfirmDelete(" + DataBinder.Eval(e.Row.DataItem, "Name") + ");";

                    }
                    //if (e.Row.FindControl("lnkeEdit") != null && DataBinder.Eval(e.Row.DataItem, "ID") != null)
                    //{
                    //    LinkButton lnk = (LinkButton)e.Row.FindControl("lnkeEdit");
                    //    Int16 Itype = 0;
                    //    if (Session["Type"] != null)
                    //    {
                    //        Itype = (Int16)Session["Type"];

                    //        if (Itype != 8)
                    //            lnk.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID") + "," + DataBinder.Eval(e.Row.DataItem, "Name");
                    //        else
                    //            lnk.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID") + "," + DataBinder.Eval(e.Row.DataItem, "Name") + "," + DataBinder.Eval(e.Row.DataItem, "TeamEmailID") + "," + DataBinder.Eval(e.Row.DataItem, "TeamPhone No");

                    //    }
                    //}
                }
            }
        }

        public void updateAmendment()
        {
            try
            {
                if (Session["Amdid"] != null)
                {
                    int AmdID = (int)Session["Amdid"];
                    if (Session["ClientHcbRef"] != null)
                    {
                        Session["ClientHcbRef"] = "DCP00001";
                    }
                    else
                        Session.Add("ClientHcbRef", "DCP0001");
                    int userid = (int)Session["LoggedInUserId"];
                    string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
                    MasterListBL MBL = new MasterListBL();
                    MBL.UpdateAmendment(Convert.ToInt32(Hcbref), userid, 1, AmdID);

                    // in case further update possible then remove this condtion
                    Session["Amdid"] = null;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void InsertAmendment()
        {
            if (Session["ClientHcbRef"] != null)
            {
                Session["ClientHcbRef"] = "DCP00001";
            }
            else
                Session.Add("ClientHcbRef", "DCP0001");

            int userid = (int)Session["LoggedInUserId"];
            string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
            MasterListBL MBL = new MasterListBL();
            int AmdId = MBL.InsertAmndment(Convert.ToInt32(Hcbref), userid, 0);
            if (AmdId != 0)
            {
                if (Session["Amdid"] != null)
                {
                    Session["AmdId"] = AmdId;
                }
                else Session.Add("AmdId", AmdId);
            }
        }

        protected void btnSaveFee_Click(object sender, EventArgs e)
        {
            HCBFeeCharged objFeeChg = new HCBFeeCharged();

            try
            {
                if (txtHomeVisit.Text != "")
                {
                    objFeeChg.HomeVisitCurrency = Convert.ToDouble(txtHomeVisit.Text);
                }
                else
                {
                    objFeeChg.HomeVisitCurrency = 0;
                }

                if (txtCall1Fee.Text != "")
                {
                    objFeeChg.Call1Fee = Convert.ToDouble(txtCall1Fee.Text);
                }
                else
                {
                    objFeeChg.Call1Fee = 0;
                }

                if (txtCall2Fee.Text != "")
                {
                    objFeeChg.Call2Fee = Convert.ToDouble(txtCall2Fee.Text);
                }
                else
                {
                    objFeeChg.Call2Fee = 0;
                }

                if (txtCall3Fee.Text != "")
                {
                    objFeeChg.Call3Fee = Convert.ToDouble(txtCall3Fee.Text);
                }
                else
                {
                    objFeeChg.Call3Fee = 0;
                }

                if (txtCallAborted.Text != "")
                {
                    objFeeChg.CallAbortedFee = Convert.ToDouble(txtCallAborted.Text);
                }
                else
                {
                    objFeeChg.CallAbortedFee = 0;
                }

                objFeeChg.ModifiedDate = DateTime.Now.ToString();

                if (objFeeChg.Save())
                {
                    lblMessage.Text = AntiXss.HtmlEncode("Fees are saved");
                }
                else
                {
                    lblMessage.Text = AntiXss.HtmlEncode("Fees are not saved");
                }
            }
            catch (Exception ex)
            {
                objFeeChg = null;

                ClientScript.RegisterStartupScript(this.GetType(), "Fee", "alert('Error'" + ex.Message + "');", true);
            }
        }

        protected void gvReasonList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int Itype = 0;
                if (Session["Type"] != null)
                {
                    Itype = (Int16)Session["Type"];
                    if (Itype == 19)
                    {
                        gvReasonList.Columns[1].Visible = true;
                    }
                    else
                    {
                        gvReasonList.Columns[1].Visible = false;
                    }
                }
            }
        }
        
    }
}