﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="MetLifeStaticValues.aspx.cs" Inherits="HCB.MetLifeStaticValues" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/ControlValidation.js" type="text/javascript"></script>

    <script type="text/javascript">

        function ValidateEmail() {
            debugger;
            var txtMetLifeEmail = document.getElementById('<%=txtMetLifeEmail.ClientID%>');

            if ($.trim(txtMetLifeEmail.value) != "") {
                if (!IsValidEmailId(txtMetLifeEmail)) {


                    document.getElementById('<%=lblMessage.ClientID%>').innerHTML = "Please enter valid MetLife EmailId";
                    txtMetLifeEmail.focus();
                    return false;
                }
            }

            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Manage MetLife Static Values
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="form-group">
                        <%--<div class="col-md-9">--%>
                            <asp:Label ID="lblMessage" runat="server" CssClass="font-red font-md bold"></asp:Label>
                        <%--</div>--%>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label font-md">MetLife Telephone No.</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtMetLifeTel" CssClass="form-control" runat="server" onkeypress="javascript:return numberOnly(this,event);" MaxLength="12">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label font-md">MetLife EmailId</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtMetLifeEmail" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="form-actions">
                        <%--<div class="row">
                            <div class="col-md-11">--%>
                        <asp:Button ID="btnSave" CssClass="btn btn-circle blue" runat="server" Text="ADD" OnClick="btnSave_Click" OnClientClick="javascript:return ValidateEmail();" />
                        <%--</div>
                        </div>--%>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <%--<table border="0" cellpadding="0" cellspacing="0" style="width: 730px; margin: 0 auto;"
        class="MainText" id="tableMain">
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Manage MetLife Static Values:-</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 18px;">
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>MetLife Telephone No.:</td>
            <td>
                <asp:TextBox ID="txtMetLifeTel" runat="server" Width="270px" onkeypress="javascript:return numberOnly(this,event);" MaxLength="12">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td>MetLife EmailId: </td>
            <td>
                <asp:TextBox ID="txtMetLifeEmail" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="ADD" Width="100px" OnClick="btnSave_Click" OnClientClick="javascript:return ValidateEmail();" />
            </td>
        </tr>
    </table>--%>
</asp:Content>
