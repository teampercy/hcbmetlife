﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MetlifeManagementInformationReport.aspx.cs" Inherits="HCB.MetlifeManagementInformationReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //function pageLoad(sender, args) {
        $(document).ready(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
            initTable2();
        });
        //}

        var initTable2 = function () {

            var table = $('.makeDataTable');

            var oTable = table.dataTable({

                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "No data available in table",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "_MENU_ entries",
                    "search": "Search:",
                    "zeroRecords": "No matching records found"
                },

                "order": [
                    [0, 'asc']
                ],

                "lengthMenu": [
                    [5, 10, 15, 30, -1],
                    [5, 10, 15, 30, "All"] // change per page values here
                ],
                // set the initial value
                "pageLength": 10,
            });
        }

    </script>

    <style type="text/css">
        .dataTables_length, .dataTables_filter, .dataTables_info, .pagination {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server">Metlife Management Information Report</asp:Label>
            </div>
        </div>

        <div class="portlet-body form">
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnOpenAll" CssClass="btn form-control input-medium"
                                    Style="border: 1px solid #c2cad8;" Text="ALL Cases" OnClick="btnOpenAll_Click" />
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="control-label">Export to  </label>
                                <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown" CssClass="form-control input-md input-inline">
                                    <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                                    <%--<asp:ListItem Text="PDF" Value="1"></asp:ListItem>--%>
                                    <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo"
                                    ValidationGroup="valdtdown"></asp:RequiredFieldValidator>
                                <asp:Button ID="lnkExport" ValidationGroup="valdtdown" runat="server" Text="Export" CssClass="btn blue bold"
                                    OnClick="lnkExport_Click"></asp:Button>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Scheme Name</label>
                                <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="ddlManagementScheme" CssClass="form-control input-medium"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlManagementScheme_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Broker</label>
                                <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="ddlManagementBroker" CssClass="form-control input-medium"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlManagementBroker_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Date Range</label>
                                <div class="col-md-6">
                                    <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                        <asp:TextBox runat="server" CssClass="form-control" name="from" ID="txtManagementFromDate"></asp:TextBox>
                                        <span class="input-group-addon">to </span>
                                        <asp:TextBox runat="server" CssClass="form-control" name="to" ID="txtManagementToDate"></asp:TextBox>
                                    </div>
                                    <!-- /input-group -->
                                    <span class="help-block">Select date range </span>
                                </div>
                                <div class="col-md-1">
                                    <asp:LinkButton runat="server" ID="btnManagementDateRange" CssClass="btn green" OnClick="btnManagementDateRange_Click">
                                            <i class="fa fa-check-circle"></i>
                                    </asp:LinkButton>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblerr" runat="server" CssClass="font-red bold"></asp:Label>
                    </div>

                    <div id="divReport" runat="server">
                    </div>

                </div>
            </div>
        </div>

    </div>
</asp:Content>
