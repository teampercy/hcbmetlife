﻿using HCBBLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HCB
{
    public partial class MetlifeManagementInformationReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                //GetManagementInformation();
                //btnOpenAll.Style.Add("border-color", "#3598dc");
                lnkExport.Enabled = false;
            }
        }

        protected void ddlManagementScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlManagementBroker.SelectedIndex = 0;
            ddlManagementScheme.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlManagementBroker.Style.Remove("border-color");

            GetManagementInformation();
        }

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            ddlManagementScheme.SelectedIndex = 0;
            ddlManagementBroker.SelectedIndex = 0;

            btnOpenAll.Style.Add("border-color", "#3598dc");
            ddlManagementScheme.Style.Remove("border-color");
            ddlManagementBroker.Style.Remove("border-color");

            GetManagementInformation();
        }

        protected void ddlManagementBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlManagementScheme.SelectedIndex = 0;
            ddlManagementBroker.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlManagementScheme.Style.Remove("border-color");
            GetManagementInformation();
        }

        protected void btnManagementDateRange_Click(object sender, EventArgs e)
        {
            GetManagementInformation();
        }


        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlManagementBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlManagementBroker.DataTextField = "AssessorTeamName";
                ddlManagementBroker.DataValueField = "ID";
                ddlManagementBroker.DataBind();
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Broker--", "0");
                ddlManagementBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Broker--", "0");
                ddlManagementBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlManagementScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlManagementScheme.DataTextField = "SchemeName";
                ddlManagementScheme.DataValueField = "ID";
                ddlManagementScheme.DataBind();
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Scheme Name--", "0");
                ddlManagementScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Scheme Name--", "0");
                ddlManagementScheme.Items.Insert(0, item);
            }
        }
        #endregion

        private void GetManagementInformation()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlManagementBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlManagementScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtManagementFromDate.Text != "" && txtManagementToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtManagementFromDate.Text);
                endDate = Convert.ToDateTime(txtManagementToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsMetlifeInformation = objClientInfo.GetMetlifeManagementInformation(UserId, usertype, AssessorTeamId, SchemeNameId, startDate, endDate);
            DataSet dsRearranged=new DataSet();
            DataTable dtCopy0 = dsMetlifeInformation.Tables[0].Copy();
            DataTable dtCopy2 = dsMetlifeInformation.Tables[2].Copy();
            DataTable dtCopy4 = dsMetlifeInformation.Tables[4].Copy();
            DataTable dtCopy1 = dsMetlifeInformation.Tables[1].Copy();
            DataTable dtCopy3 = dsMetlifeInformation.Tables[3].Copy();
            DataTable dtCopy5 = dsMetlifeInformation.Tables[5].Copy();
            dsRearranged.Tables.Add(dtCopy0);
            dsRearranged.Tables.Add(dtCopy2);
            dsRearranged.Tables.Add(dtCopy4);
            dsRearranged.Tables.Add(dtCopy1);
            dsRearranged.Tables.Add(dtCopy3);
            dsRearranged.Tables.Add(dtCopy5);
            
            ViewState.Add("dsreport", dsRearranged);
            if (dsRearranged != null && dsRearranged.Tables.Count > 0 && dsRearranged.Tables[1].Rows.Count > 0)
            {
                LoadDataset(dsRearranged);
                ViewState.Add("dsreport", dsRearranged);
                lnkExport.Enabled = true;
            }
            else
            {
                divReport.InnerHtml = "";
                ViewState["dsreport"] = null;
                lnkExport.Enabled = false;
            }
        }

        public void LoadDataset(DataSet ds)
        {
            #region code for dynemic report genretion
            StringBuilder sb = new StringBuilder();
            DataTable dt;
            int RowCounter=0;
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                RowCounter++;
                if (RowCounter == 1)
                {
                    if (txtManagementFromDate.Text != "" && txtManagementToDate.Text != "")
                    {
                        sb.Append("<table width=\"100%\"><tr><td><span class=\"font-lg font-green text-center bold\">Metlife Management Information Report From " + txtManagementFromDate.Text + " To " + txtManagementToDate.Text + "</span></td></tr></table><table><tr><td>&nbsp</td></tr></table>");
                    }
                    else
                    {
                        sb.Append("<table width=\"100%\"><tr><td><span class=\"font-lg font-green text-center bold\">Metlife Management Information Report </span></td></tr></table><table><tr><td>&nbsp</td></tr></table>");
                    }
                }
                if (RowCounter == 1|| RowCounter == 2 || RowCounter == 3)
                {
                    sb.Append("<div class=\"col-md-4\">");
                }
                sb.Append("<table border=\"2\" class=\"table-striped table-bordered table-hover table-header-fixed makeDataTable\" rel=\"datatable\" id=\"" + ds.Tables[i].TableName + "\">");
                sb.Append("<thead>");
                sb.Append(" <tr class=\"font-white bg-green-steel\">");
                dt = ds.Tables[i];
                foreach (DataColumn dc in dt.Columns)
                {
                    if ((i == 3 && dc.ColumnName == "ReturnToWorkReason") ||
                        (i == 4 && dc.ColumnName == "CancellationReason") ||
                        (i == 5 && dc.ColumnName == "InjuryCount")
                        )
                    {
                        // Do nothing
                    }
                    else
                    {
                        sb.Append(" <th class=\"text-center\"><u>" + dc.ColumnName + "</u></th> ");
                    }
                }
                sb.Append("</tr>");
                sb.Append("</thead>");
                sb.Append("<tbody>");

                DataTable dt2 = dt;
                if (dt2.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt2.Rows)
                    {
                        sb.Append(" <tr>");
                        foreach (DataColumn dc in dt2.Columns)
                        {
                            if ((i == 3 && dc.ColumnName == "ReturnToWorkReason") ||
                                (i == 4 && dc.ColumnName == "CancellationReason") ||
                                (i == 5 && dc.ColumnName == "InjuryCount")
                                )
                            {
                                //Do nothing
                            }
                            else
                            {
                                if (dr.ToString() != "")
                                {
                                    if (i == 5 && dc.ColumnName == "Percentage")
                                    {
                                        sb.Append(" <td>" + dr[dc.ColumnName].ToString() + " %" + "</td> ");
                                    }
                                    else
                                    {
                                        sb.Append(" <td>" + dr[dc.ColumnName].ToString() + "</td> ");
                                    }
                                }
                                else
                                {
                                    sb.Append(" <td>" + "." + "</td> ");
                                }
                            }
                        }
                        sb.Append("</tr> ");
                    }
                }
                else
                {
                    //sb.Append("<tr><td colspan=\"2\">No Data Found</td></tr> ");
                }

                sb.Append("</tbody></table>");
                sb.Append("<table><tr><td>&nbsp</td></tr></table>");
                if (RowCounter == 1 || RowCounter == 2 || RowCounter == 3)
                {
                    sb.Append("</div>");
                }
            }
            divReport.InnerHtml = sb.ToString();
            #endregion
        }

        public void TransformSecondDataTable(ref DataSet ds)
        {
            try
            {
                if (ds.Tables[1].Rows.Count == 0)
                {
                    DataRow row1 = ds.Tables[1].NewRow();
                    row1[0] = "Return to work – Full time";
                    row1[1] = "0";
                    DataRow row2 = ds.Tables[1].NewRow();
                    row2[0] = "Return to work – Part time";
                    row2[1] = "0";
                    ds.Tables[1].Rows.Add(row1);
                    ds.Tables[1].Rows.Add(row2);
                }
                if (ds.Tables[1].Rows.Count == 1)
                {
                    string txt = ds.Tables[1].Rows[0][0].ToString();
                    if (txt == "Return to work – Full time")
                    {
                        DataRow row = ds.Tables[1].NewRow();
                        row[0] = "Return to work – Part time";
                        row[1] = "0";
                        ds.Tables[1].Rows.Add(row);
                    }
                    else
                    {
                        DataRow row = ds.Tables[1].NewRow();
                        row[0] = "Return to work – Full time";
                        row[1] = "0";
                        ds.Tables[1].Rows.Add(row);
                    }
                }

            }
            catch (Exception ex)
            {

            }

        }

        protected void lnkExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["dsreport"] != null)
                {
                    {
                        string strStyles = "<head><style> tr.even { background-color: #F5FDF5; }</style></head>";
                        switch (ddlExprotTo.SelectedItem.Value)
                        {
                            case "2":
                                ExportToExcel(strStyles);
                                break;
                            //case "1":
                            //    ConvertHTMLStringToPDF(strStyles, divReport.InnerHtml, (string)ViewState["date"] + "");
                            //    break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ExportToExcel(string style)
        {
            try
            {
                Response.ContentType = "application/x-msexcel";
                Response.AddHeader("Content-Disposition", "attachment; filename=ExcelFile.xls");
                Response.ContentEncoding = Encoding.Default;
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                divReport.RenderControl(hw);
                Response.Write(style + tw.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}