﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using HCBBLL;
using System.IO;
using iTextSharp;
using iTextSharp.text.api;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using Winnovative.WnvHtmlConvert;
using System.Globalization;
using Microsoft.Security.Application;
using System.Web.Services;

namespace HCB
{
    public partial class MonthlyReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                btnGetReport.Attributes.Add("onclick", "javascript:return ValidateDate();");
                if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin" || Session["UserType"].ToString() == "Inco")
                {                    
                    lnkExport.Enabled = false;
                }
                else
                {
                    //Response.Redirect("~/Accessdenied.aspx", false);
                    Server.Transfer("~/Accessdenied.aspx");
                }
            }

        }

        public void LoadDataset(DataTable dt, string date, string endDate)
        {
            #region code for dynemic report genretion
            StringBuilder sb = new StringBuilder();

            double grandTotal = 0.0;            
            
            sb.Append("<table border=\"2\" class=\"table-striped table-bordered table-hover table-header-fixed\" rel=\"datatable\" id=\"sample_2\">");
            sb.Append("<thead>");            
            sb.Append(" <tr class=\"font-white bg-green-steel\">");            
            sb.Append("<td colspan='" + dt.Columns.Count + "' ><span class=\"font-lg font-white text-left bold\">MI CHARGE REPORT FROM " + date + " TO " + endDate + "</span></td></tr>");            
            sb.Append("<tr class=\"font-white bg-green-steel\">");

            foreach (DataColumn dc in dt.Columns)
            {
                sb.Append(" <th class=\"text-center\"><u>" + dc.ColumnName + "</u></th> ");
            }
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");
                      
            DataTable dt2=dt;
            if (dt2.Rows.Count > 0)
            {                
                foreach (DataRow dr in dt2.Rows)
                {
                    sb.Append(" <tr>");

                    foreach (DataColumn dc in dt2.Columns)
                    {
                        if (dr.ToString() != "")
                        {                           
                            if (dc.ColumnName.ToString() == "HCB#")
                            {
                                string Hcbref=dr[dc.ColumnName].ToString();
                                Hcbref = Hcbref.Substring(3);                               
                                sb.Append("<td> <a onclick=\"openReportDetail('" + dr[dc.ColumnName].ToString() + "');\" id='Hcbreport' style='cursor:pointer;text-decoration:underline;color: blue'> " + dr[dc.ColumnName].ToString() + "</a> </td> ");                                
                            }
                            else
                            {
                               sb.Append(" <td>" + dr[dc.ColumnName].ToString() + "</td> ");                               
                            }                           
                        }
                        else
                            sb.Append(" <td>" + "." + "</td> ");
                    }
                    sb.Append("</tr> ");
                }
                        
                }               

            sb.Append("</tbody></table>");           
            sb.Append("<div style='text-align:right;width:90%;font-size:small'>");          
            sb.Append("</div>");

            divReport.InnerHtml = sb.ToString();

            #endregion
        }

        protected void btnGetReport_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("vgGenReport");
                if (Page.IsValid)
                {                    
                    DateTime startDate=Convert.ToDateTime(txtFromDate.Text);
                    DateTime endDate=Convert.ToDateTime(txtToDate.Text);

                    string date = startDate.ToString("dd/MM/yyyy");
                    string enddate = endDate.ToString("dd/MM/yyyy");
                                        
                    DataSet ds = MasterListBL.GetReport(startDate,endDate);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        LoadDataset(ds.Tables[0], date, enddate);
                        ViewState["date"] = date;
                        ViewState.Add("dsreport", ds);
                        lblerr.Text = AntiXss.HtmlEncode("");
                        lnkExport.Enabled = true;
                    }
                    else
                    {
                        ViewState["dsreport"] = null;
                        divReport.InnerHtml = "";
                        lblerr.Text =AntiXss.HtmlEncode("No record exists");
                        lnkExport.Enabled = false;
                    }
                }
            }

            catch (Exception ex)
            {
                
            }
        }

        protected void lnkExport_Click(object sender, EventArgs e)
        {
            try
            {
                //  int Itype = SetType(ddlReportName.SelectedItem.Value.Trim());
                if (ViewState["dsreport"] != null)
                {
                    // DataSet ds = (DataSet)ViewState["dsreport"];
                    // if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string strStyles = "<head><style> tr.even { background-color: #F5FDF5; }</style></head>";
                        switch (ddlExprotTo.SelectedItem.Value)
                        {
                            case "2":
                                ExportToExcel(strStyles);
                                break;
                            case "1":
                                //ExportToPDf(ds.Tables[0]);
                                ConvertHTMLStringToPDF(strStyles, divReport.InnerHtml, (string)ViewState["date"] + "");
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ConvertHTMLStringToPDF(string strStyle, string strHTML, string strDate)
        {
            DataSet ds = new DataSet();
            ds = MasterListBL.GetPDFLicensekey();
            string LicenseKeyPdf = string.Empty;
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    LicenseKeyPdf = ds.Tables[0].Rows[0]["PdfLicenseKey"].ToString();
                }

            }
            //"<link type='text/css' href='../../../App_Themes/Default/Default.css' rel='Stylesheet' />"
            // Dim htmlString As String = "<style>BODY{FONT-SIZE: 8.5pt;WORD-SPACING: normal;FONT-FAMILY : arial;} TD{FONT-SIZE: 8pt;FONT-FAMILY: arial;} .Box1{BORDER-RIGHT: #42419c 1px solid;BORDER-TOP: #42419c 1px solid;BORDER-LEFT: #42419c 1px solid;BORDER-BOTTOM: #42419c 1px solid} .textlab2{FONT-WEIGHT: bold;FONT-SIZE: 12px;WORD-SPACING: normal;COLOR: #cc0000;FONT-FAMILY: arial;} .textlab6{FONT-WEIGHT: bold;FONT-SIZE: 10pt;WORD-SPACING: normal;COLOR: navy;FONT-FAMILY: arial;} .textlab7{FONT-WEIGHT: bold;FONT-SIZE: 8.5pt;WORD-SPACING: normal;COLOR: navy;FONT-FAMILY: arial;} .textlab8{FONT-WEIGHT:normal;FONT-SIZE:10pt;WORD-SPACING:normal;FONT-FAMILY:arial;} 	</style><table class='Box1' width='90%' align='center'><tr><td>" & lblLogo.Text + "</td><td> " & lblHeading.Text + "</td></tr><tr><td colspan='2'> " & lblHtml.Text + lblNotes.Text + lblInstructions.Text & "</td></tr></table>"

            string htmlString = strStyle + strHTML;// strstyles + genrateHTML(strHTML);
            //+ lblLogo.Text + lblHeading.Text + lblHtml.Text + "<table cellpadding='0' cellspacing='0'><tr><td>&nbsp;</td><td class='textlab6'><u>Notes / Instructions</u>:</td></tr></table>" + lblInstructions.Text + lblNotes.Text
            //string baseURL = ConfigurationManager.AppSettings("WebsiteUrl").ToString;
            // "https://10.1.1.61:443 " '
            // Dim selectablePDF As Boolean = radioConvertToSelectablePDF.Checked

            // Create the PDF converter. Optionally you can specify the virtual browser 
            // width as parameter. 1024 pixels is default, 0 means autodetect
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = LicenseKeyPdf;
            //pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Winnovative HTML to PDF Converter";

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);

            //Please Write the following file checking on the top before calling all store procedures generatepdf() function

            //string PdfPath = ".pdf";
            //FileInfo fFile = new FileInfo(PdfPath);
            //If Not fFile.Exists Then
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
            //End If

            //if (!fFile.Exists)
            //  {
            // MessageBox.Show("File Not Found")
            //========================== Generating pdf ======================================
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename=" + AntiXss.HtmlEncode(strDate) + "_" + AntiXss.HtmlEncode(DateTime.Now.ToString("ddMMyyyy")) + ".pdf; size=" + pdfBytes.Length.ToString());
            response.Flush();
            response.BinaryWrite(pdfBytes);
            response.Flush();
            response.End();
            // ================================== End here ==================================
            //}
            //else
            //  {
            //MessageBox.Show("File Found. File was created on: " & fFile.CreationTime)
            //System.Net.WebClient client = new System.Net.WebClient();
            //byte[] buffer = client.DownloadData(PdfPath);

            //Response.Redirect(path)


            //if ((buffer != null))
            //    {
            //    Response.ContentType = "application/pdf";
            //    Response.AddHeader("content-length", buffer.Length.ToString());
            //    Response.AddHeader("Content-Disposition", "attachment; filename=ConversionResult.pdf;");

            //    Response.BinaryWrite(buffer);

            //    }
            //}
            //Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            //response.Clear()
            //response.AddHeader("Content-Type", "binary/octet-stream")
            //response.AddHeader("Content-Disposition", "inline; filename=ConversionResult.pdf; size=" & pdfBytes.Length.ToString())
            //response.Flush()
            //response.BinaryWrite(pdfBytes)
            //response.Flush()
            //response.End()



        }

        private string genrateHTML(string strtbldata)
        {
            string strHTML = "";

            strHTML = "<table border='0' width='95%' height='98%' class='Box1' style='border-collapse: collapse'";
            strHTML += "            align='center' cellspacing='0' cellpadding='0'>";
            strHTML += "           <tr align='center'>";
            strHTML += "            <td>";
            strHTML += "                 &nbsp;</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "<td align='center'>";
            strHTML += "<table border='0' cellspacing='0' cellpadding='0' width='98%' align='center'>";
            strHTML += "<tr>";
            strHTML += "<td width='70%' align='left'>";
            strHTML += "&nbsp;";
            strHTML += "Logo";
            strHTML += "</td>";
            strHTML += "<td align='Right' width='30%' valign='top'>";

            strHTML += "Heading";

            strHTML += " </td>";
            strHTML += "</tr>";
            strHTML += " </table>";
            strHTML += "</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "   <td>";
            strHTML += "<table border='0' cellspacing='3' width='100%'>";
            strHTML += "<tr>";
            strHTML += "                                    <td valign='top' align='center'>";
            strHTML += strtbldata;
            strHTML += "        </td>";
            strHTML += "                                </tr>";
            strHTML += "              </table>";
            strHTML += "</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "<td>";
            strHTML += "&nbsp;</td>";
            strHTML += "</tr>";
            strHTML += "  <tr>";
            strHTML += "      <td>";
            strHTML += "      <table cellpadding='0' cellspacing='0'>";
            strHTML += "          <tr>";
            strHTML += "              <td>";
            strHTML += "                 &nbsp;</td>";
            strHTML += "             <td class='textlab6'>";
            strHTML += "                 <u>Notes / Instructions</u>:</td>";
            strHTML += "          </tr>";
            strHTML += "      </table>";
            strHTML += "    </td>";
            strHTML += "  </tr>";
            strHTML += "   <tr>";
            strHTML += "      <td>";
            strHTML += "         <table border='0' cellspacing='0' cellpadding='0' width='85%'>";
            strHTML += "              <tr>";
            strHTML += "                 <td>";
            strHTML += "                     &nbsp;</td>";
            strHTML += "                 <td class='textlab8'>";
            strHTML += " instrunctuion";
            strHTML += "               </td>";
            strHTML += "            </tr>";
            strHTML += "           <tr>";
            strHTML += "              <td>";
            strHTML += "                  &nbsp;</td>";
            strHTML += "       </tr>";
            strHTML += "      <tr>";
            strHTML += "          <td>";
            strHTML += "            &nbsp;</td>";
            strHTML += "        <td class='textlab8'>";
            strHTML += "instruction";
            strHTML += "       </td>";
            strHTML += "    </tr>";
            strHTML += "  </table>";
            strHTML += "</td>";
            strHTML += "</tr>";
            strHTML += " <tr>";
            strHTML += "   <td>";
            strHTML += "  &nbsp;</td>";
            strHTML += "</tr>";
            strHTML += "<tr>";
            strHTML += "  <td>";
            strHTML += "      &nbsp;</td>";
            strHTML += " </tr>";
            strHTML += "</table>'";

            return strHTML;


        }

        public void ExportToExcel(string style)
        {
            try
            {

                //Create a dummy GridView

                //GridView GridView1 = new GridView();

                //GridView1.AllowPaging = false;

                //GridView1.DataSource = dt;
                //GridView1.DataBind();

                Response.ContentType = "application/x-msexcel";
                Response.AddHeader("Content-Disposition", "attachment; filename=ExcelFile.xls");
                Response.ContentEncoding = Encoding.Default;
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                divReport.RenderControl(hw);
                Response.Write(style + tw.ToString());
                Response.End();

                //Response.Clear();
                //Response.Buffer = true;
                //Response.AddHeader("content-disposition", "attachment;filename=DataTable.xls");
                //Response.Charset = "";
                //Response.ContentType = "application/vnd.ms-excel";
                //StringWriter sw = new StringWriter();
                //HtmlTextWriter hw = new HtmlTextWriter(sw);
                //for (int i = 0; i < GridView1.Rows.Count; i++)
                //    {
                //    //Apply text style to each Row
                //    GridView1.Rows[i].Attributes.Add("class", "textmode");
                //    }
                //GridView1.RenderControl(hw);
                ////style to format numbers to string
                //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                //Response.Write(style);
                //Response.Output.Write(sw.ToString());
                //Response.Flush();
                //Response.End();
            }


            catch (Exception ex)
            {

                throw;
            }
        }

        public void ExportToPDf(DataTable dt)
        {


            //Create a dummy GridView

            try
            {
                GridView GridView1 = new GridView();
                GridView1.AllowPaging = false;
                GridView1.DataSource = dt;
                GridView1.DataBind();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=DataTable.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                divReport.RenderControl(hw);
                // GridView1.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
                Response.Clear();
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [WebMethod(EnableSession = true)]
        public static void SetUrl(string val)
        {
            HttpContext.Current.Session["ClientHcbRef"] = val;            
        }
    }
}
