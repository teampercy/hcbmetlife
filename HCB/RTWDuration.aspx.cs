﻿using HCBBLL;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HCB
{
    public partial class RTWDuration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                CalculateMonthlyAverageRTWDuration();
                btnOpenAll.Style.Add("border-color", "#3598dc");
            }
        }

        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlRTWDurationBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlRTWDurationBroker.DataTextField = "AssessorTeamName";
                ddlRTWDurationBroker.DataValueField = "ID";
                ddlRTWDurationBroker.DataBind();
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Broker--", "0");
                ddlRTWDurationBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Broker--", "0");
                ddlRTWDurationBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlRTWDurationScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlRTWDurationScheme.DataTextField = "SchemeName";
                ddlRTWDurationScheme.DataValueField = "ID";
                ddlRTWDurationScheme.DataBind();
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Scheme Name--", "0");
                ddlRTWDurationScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Scheme Name--", "0");
                ddlRTWDurationScheme.Items.Insert(0, item);
            }
        }
        #endregion

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            ddlRTWDurationScheme.SelectedIndex = 0;
            ddlRTWDurationBroker.SelectedIndex = 0;

            btnOpenAll.Style.Add("border-color", "#3598dc");
            ddlRTWDurationScheme.Style.Remove("border-color");
            ddlRTWDurationBroker.Style.Remove("border-color");

            CalculateMonthlyAverageRTWDuration();
        }

        protected void ddlRTWDurationScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRTWDurationBroker.SelectedIndex = 0;
            ddlRTWDurationScheme.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlRTWDurationBroker.Style.Remove("border-color");

            CalculateMonthlyAverageRTWDuration();
        }

        protected void ddlRTWDurationBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRTWDurationScheme.SelectedIndex = 0;
            ddlRTWDurationBroker.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlRTWDurationScheme.Style.Remove("border-color");

            CalculateMonthlyAverageRTWDuration();
        }

        protected void btnRTWDurationDateRange_Click(object sender, EventArgs e)
        {
            CalculateMonthlyAverageRTWDuration();
        }

        private void CalculateMonthlyAverageRTWDuration()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlRTWDurationBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlRTWDurationScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtRTWDurationFromDate.Text != "" && txtRTWDurationToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtRTWDurationFromDate.Text);
                endDate = Convert.ToDateTime(txtRTWDurationToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsMonthlyAverageRTWDuration = objClientInfo.GetDashboardAverageRTWDuration(UserId, usertype, AssessorTeamId, SchemeNameId, startDate, endDate);
            if (dsMonthlyAverageRTWDuration != null && dsMonthlyAverageRTWDuration.Tables.Count > 0 && dsMonthlyAverageRTWDuration.Tables[1].Rows.Count > 0)
            {
                LoadDataset(dsMonthlyAverageRTWDuration.Tables[1]);
                lblerr.Text = AntiXss.HtmlEncode("");
            }
            else
            {
                divReport.InnerHtml = "";
                lblerr.Text = AntiXss.HtmlEncode("No record exists");
            }
        }

        public void LoadDataset(DataTable dt)
        {
            #region code for dynemic report genretion
            StringBuilder sb = new StringBuilder();

            sb.Append("<table border=\"2\" class=\"table-striped table-bordered table-hover table-header-fixed\" rel=\"datatable\" id=\"sample_2\">");
            sb.Append("<thead>");
            sb.Append(" <tr class=\"font-white bg-green-steel\">");
            if (txtRTWDurationFromDate.Text != "" && txtRTWDurationToDate.Text != "")
            {
                sb.Append("<td colspan='" + dt.Columns.Count + "' ><span class=\"font-lg font-white text-left bold\">Monthly Average RTW Duration Reports From " + txtRTWDurationFromDate.Text + " To " + txtRTWDurationToDate.Text + "</span></td></tr>");
            }
            else
            {
                sb.Append("<td colspan='" + dt.Columns.Count + "' ><span class=\"font-lg font-white text-left bold\">Monthly Average RTW Duration Reports From 01/01/"+DateTime.Now.Year.ToString()+" To 31/12/"+ DateTime.Now.Year.ToString()+ "</span></td></tr>");
            }
            sb.Append("<tr class=\"font-white bg-green-steel\">");

            foreach (DataColumn dc in dt.Columns)
            {
                sb.Append(" <th class=\"text-center\"><u>" + dc.ColumnName + "</u></th> ");
            }
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            DataTable dt2 = dt;
            if (dt2.Rows.Count > 0)
            {
                foreach (DataRow dr in dt2.Rows)
                {
                    sb.Append(" <tr>");
                    foreach (DataColumn dc in dt2.Columns)
                    {
                        if (dr.ToString() != "")
                        {
                            sb.Append(" <td>" + dr[dc.ColumnName].ToString() + "</td> ");
                        }
                        else
                            sb.Append(" <td>" + "." + "</td> ");
                    }
                    sb.Append("</tr> ");
                }
            }

            sb.Append("</tbody></table>");
            sb.Append("<div style='text-align:right;width:90%;font-size:small'>");
            sb.Append("</div>");

            divReport.InnerHtml = sb.ToString();

            #endregion
        }
    }
}