﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ReasonCancelList.aspx.cs" Inherits="HCB.ReasonCancelList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var popupStatus = 0;
        var popupDivVal = '';
        function loadPopup(popupDiv) {
            //debugger;
            popupDivVal = popupDiv;

            //loads popup only if it is disabled

            if (popupStatus == 0) {
                centerPopup(popupDiv);
                $("#backgroundPopup1").css({
                    "opacity": "0.7"
                });

                $("#backgroundPopup1").fadeIn("slow");
                $(popupDiv).fadeIn("slow");
                popupStatus = 1;
            }
            return false;
        }
        //        $("#backgroundPopup1").click(function () {

        //            disablePopup(popupDivVal);
        //        });
        function disablePopup(popupdiv) {

            //disables popup only if it is enabled

            if (popupStatus == 1) {
                $("#backgroundPopup1").fadeOut("slow");
                $(popupdiv).fadeOut("slow");
                popupStatus = 0;
            }
        }
        function centerPopup(popupdiv) {
            //request data for centering

            var windowWidth = document.documentElement.clientWidth;
            var windowHeight = document.documentElement.clientHeight;
            var popupHeight = $(popupdiv).height();
            var popupWidth = $(popupdiv).width();
            //centering  
            $(popupdiv).css({
                "position": "absolute",
                "top": windowHeight / 2 - popupHeight / 2,
                "left": windowWidth / 2 - popupWidth / 2
            });
            //only need force for IE6  

            $("#backgroundPopup1").css({
                "height": windowHeight
            });
            return false;
        }
        function closepopup() {
            if (popupDivVal == '#Popup') {
                disablePopup('#Popup');
                document.getElementById('<%= txtName.ClientID %>').value = "";
            }
        }
    </script>
    <style type="text/css">
        #backgroundPopup1
        {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6*/
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: #000000;
            border: 1px solid #cecece;
            z-index: 4;
        }
        .popupContact
        {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6 height: 200px;*/
            width: 550px;
            background: #FFFFFF;
            border: 2px solid #cecece;
            z-index: 5;
            font-size: 13px;
        }
        
        .popupContactClose
        {
            font-size: 14px;
            line-height: 14px;
            right: 6px;
            top: 4px;
            position: absolute;
            color: #6fa5fd;
            font-weight: 700;
            display: block;
        }
        
        #overlay
        {
            position: fixed;
            z-index: 9 !important;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=80);
            opacity: 0.80;
            -moz-opacity: 0.80;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="scManager" runat="server">
    </asp:ScriptManager>
    <div id="backgroundPopup1" onclick="closepopup()">
    </div>
    <div>
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <div id="Popup" class="popupContact">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtName" ValidationGroup="validateTxt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqftxtName" runat="server" ControlToValidate="txtName"
                                    ErrorMessage="Reason cancled required" ValidationGroup="validateTxt"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="btnSave" ValidationGroup="validateTxt" Text="Save"
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btncancle" runat="server" Text="Cancel" OnClientClick="return closepopup();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                        <font color="black" face="Arial" size="3"><b>Maintain Reason Cancelled</b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton Style="float: right; padding: 0px 50px 0px 0px;" ID="lnkAdd" runat="server"
                        OnClientClick="return  loadPopup('#Popup')"> Add New</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upGVList" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvReasonList" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                                Width="95%">
                                <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" />
                                <Columns>
                                    <asp:BoundField HeaderText="Service Name" DataField="Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkeEdit" runat="server" OnCommand="lnk_Command" CommandArgument='<%# string.Concat(Eval("ID"),",",Eval("Name")) %>'
                                                CommandName="edt"> Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton Visible="true" ID="lnkeDelete" runat="server" OnClientClick="return confirm('Confirm delete?')"
                                                OnCommand="lnk_Command" CommandArgument='<% #Eval("ID") %>' CommandName="del"
                                                ImageUrl="Images/Del.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="hdntype" Value="" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>
