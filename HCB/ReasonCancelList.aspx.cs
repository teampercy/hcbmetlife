﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HCBBLL;

namespace HCB
{
    public partial class ReasonCancelList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Page.IsPostBack)
                {
                    FillReasonList();
                }

            }
        }

        private void FillReasonList()
        {
            MasterList reason = new MasterList();
            DataSet ds = reason.GetMasterList(2);
            gvReasonList.DataSource = ds.Tables["Master"];
            gvReasonList.DataBind();
        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            string[] Val =  e.CommandArgument.ToString().Split(',');
            if (e.CommandName.Trim().ToLower() == "edt")
            {
                if (Session["ListId"] != null)
                {
                   
                    Session["ListId"] =Val[0];
                }
                else Session.Add("LIstID",Val[0].ToString());

                txtName.Text = Val[1].ToString();
                hdntype.Value = "edit";
                ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "edit", "loadPopup('#Popup')", true);
            }
            else if (e.CommandName.Trim().ToLower() == "del")
            {
                DeleteList(Convert.ToInt32(e.CommandArgument.ToString()));
                //ScriptManager.RegisterStartupScript(gvReasonList,this.GetType,"Del",
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hdntype.Value == "edit")
            {
                if (Session["ListId"] != null)
                {
                    string IlistId = (string)Session["ListId"];
                    MasterList reason = new MasterList();
                    reason.UpdateMasterList(Convert.ToInt32(IlistId), txtName.Text, 2);
                    Session["ListId"] = null;
                    hdntype.Value = "new";
                    FillReasonList();
                    ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "edit", "closepopup()", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "edit", "closepopup()", true);
                }
            }
            else
            {
                MasterList reason = new MasterList();
                reason.InsertMasterList(txtName.Text, 2);
            }
        }

        public void DeleteList(int id)
        {
            MasterList reason = new MasterList();
            reason.DeleteMasterList(id);
        }


    }
}