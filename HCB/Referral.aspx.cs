﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using Microsoft.Security.Application;
using SelectPdf;
using System.IO;

namespace HCB
{
    public partial class Referral : System.Web.UI.Page
    {
        #region Variable Declaration
        int id;
        private bool startConversion = false;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserType"] != null)
            {
                if (Request.QueryString["ReferralFormId"] != null) //this is for edit
                {
                    id = Convert.ToInt32(Request.QueryString["ReferralFormId"]);
                }
                else
                    id = 0;

                if (!Page.IsPostBack)
                {
                    divPartTimeDetails.Visible = false;
                    LoadDetails();
                    lblMsg.Text = AntiXss.HtmlEncode("");
                }
            }
            else
            {
                Server.Transfer("~/Accessdenied.aspx");
            }
        }

        #region Load the Details of Form when ReferralFormId is not Zero       
        public void LoadDetails()
        {
            ReferralForm objReferralForm = new ReferralForm(id);
            bool IsRecordExist = objReferralForm.Load();
            if (IsRecordExist)
            {
                txtReferralName.Text = Convert.ToString(objReferralForm.ReferralName);
                txtReferralJobTitle.Text = Convert.ToString(objReferralForm.ReferralJobTitle);
                txtReferralCompanyName.Text = Convert.ToString(objReferralForm.ReferralCompanyName);
                txtReferralAddress.Text = Convert.ToString(objReferralForm.ReferralAddress);
                txtReferralTelephone.Text = Convert.ToString(objReferralForm.ReferralTelephone);
                txtReferralEmail.Text = Convert.ToString(objReferralForm.ReferralEmail);

                if (objReferralForm.DateOfReferral != "")
                {
                    if (objReferralForm.DateOfReferral.ToString() == "01/01/1900")
                    {
                        txtDateReferral.Text = "";
                    }
                    else if (objReferralForm.DateOfReferral.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        txtDateReferral.Text = "";
                    }
                    else
                    {
                        txtDateReferral.Text = string.Format("{0:dd/MM/yyy}", Convert.ToDateTime(objReferralForm.DateOfReferral));
                    }
                }
                else
                {
                    txtDateReferral.Text = "";
                }

                txtEmployeeName.Text = Convert.ToString(objReferralForm.EmployeeName);
                txtEmployeeAddress.Text = Convert.ToString(objReferralForm.EmployeeAddress);

                if (objReferralForm.DateOfBirth != "")
                {
                    if (objReferralForm.DateOfBirth.ToString() == "01/01/1900")
                    {
                        txtDateOfBirth.Text = "";
                    }
                    else if (objReferralForm.DateOfBirth.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        txtDateOfBirth.Text = "";
                    }
                    else
                    {
                        txtDateOfBirth.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(objReferralForm.DateOfBirth));
                    }
                }
                else
                {
                    txtDateOfBirth.Text = "";
                }

                txtEmployeeEmail.Text = Convert.ToString(objReferralForm.EmployeeEmail);
                txtEmployeeJobTitleAndLocation.Text = Convert.ToString(objReferralForm.EmployeeJobTitleAndLocation);
                txtEmployeeTelephone.Text = Convert.ToString(objReferralForm.EmployeeTelephone);
                txtLineManagerNameAndJobTitle.Text = Convert.ToString(objReferralForm.LineManagerNameAndJobTitle);
                txtLineManagerTelephoneOrEmail.Text = Convert.ToString(objReferralForm.LineManagerTelephoneOrEmail);
                RdbIsEmployee.SelectedValue = Convert.ToString(objReferralForm.IsEmployee);
                RdbEmployeeContract.SelectedValue = Convert.ToString(objReferralForm.EmployeeContract);

                if (RdbEmployeeContract.SelectedIndex == 1) //When Selected value is Part-time
                {
                    divPartTimeDetails.Visible = true;
                }
                else
                {
                    divPartTimeDetails.Visible = false;
                }

                txtPartTimeFromMonday.Text = Convert.ToString(objReferralForm.PartTimeFromMonday);
                txtPartTimeToMonday.Text = Convert.ToString(objReferralForm.PartTimeToMonday);
                txtPartTimeFromTuesday.Text = Convert.ToString(objReferralForm.PartTimeFromTuesday);
                txtPartTimeToTuesday.Text = Convert.ToString(objReferralForm.PartTimeToTuesday);
                txtPartTimeFromWednesday.Text = Convert.ToString(objReferralForm.PartTimeFromWednesday);
                txtPartTimeToWednesday.Text = Convert.ToString(objReferralForm.PartTimeToWednesday);
                txtPartTimeFromThursday.Text = Convert.ToString(objReferralForm.PartTimeFromThursday);
                txtPartTimeToThursday.Text = Convert.ToString(objReferralForm.PartTimeToThursday);
                txtPartTimeFromFriday.Text = Convert.ToString(objReferralForm.PartTimeFromFriday);
                txtPartTimeToFriday.Text = Convert.ToString(objReferralForm.PartTimeToFriday);
                RdbEmployeeAware.SelectedValue = Convert.ToString(objReferralForm.EmployeeAware);
                RdbConsentGiven.SelectedValue = Convert.ToString(objReferralForm.ConsentGiven);
                txtBroadOutline.Text = Convert.ToString(objReferralForm.BroadOutline);

                if (objReferralForm.DateOfIllness != "")
                {
                    if (objReferralForm.DateOfIllness.ToString() == "01/01/1900")
                    {
                        txtDateOfIllness.Text = "";
                    }
                    else if (objReferralForm.DateOfIllness.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        txtDateOfIllness.Text = "";
                    }
                    else
                    {
                        txtDateOfIllness.Text = string.Format("{0:dd/MM/yyy}", Convert.ToDateTime(objReferralForm.DateOfIllness));
                    }
                }
                else
                {
                    txtDateOfIllness.Text = "";
                }

                txtOtherInformation.Text = Convert.ToString(objReferralForm.OtherInformation);
                txtOtherAdvice.Text = Convert.ToString(objReferralForm.OtherAdvice);
                txtAddRelevantInformation.Text = Convert.ToString(objReferralForm.AddRelevantInformation);
                txtDetailsOfPreviousSickness.Text = Convert.ToString(objReferralForm.DetailsOfPreviousSickness);
                RdbEmployeeMedicalInsurance.SelectedValue = Convert.ToString(objReferralForm.EmployeeMedicalInsurance);
            }
        }
        #endregion

        //Clear all fields from Referral Form Page
        protected void btnClear_Click(object sender, EventArgs e)
        {
            //Text Boxes
            txtReferralName.Text = "";
            txtReferralJobTitle.Text = "";
            txtReferralCompanyName.Text = "";
            txtReferralAddress.Text = "";
            txtReferralTelephone.Text = "";
            txtReferralEmail.Text = "";
            txtDateReferral.Text = "";
            txtEmployeeName.Text = "";
            txtEmployeeAddress.Text = "";
            txtDateOfBirth.Text = "";
            txtEmployeeEmail.Text = "";
            txtEmployeeJobTitleAndLocation.Text = "";
            txtEmployeeTelephone.Text = "";
            txtLineManagerNameAndJobTitle.Text = "";
            txtLineManagerTelephoneOrEmail.Text = "";
            txtPartTimeFromMonday.Text = "";
            txtPartTimeToMonday.Text = "";
            txtPartTimeFromTuesday.Text = "";
            txtPartTimeToTuesday.Text = "";
            txtPartTimeFromWednesday.Text = "";
            txtPartTimeToWednesday.Text = "";
            txtPartTimeFromThursday.Text = "";
            txtPartTimeToThursday.Text = "";
            txtPartTimeFromFriday.Text = "";
            txtPartTimeToFriday.Text = "";
            txtBroadOutline.Text = "";
            txtDateOfIllness.Text = "";
            txtOtherInformation.Text = "";
            txtOtherAdvice.Text = "";
            txtAddRelevantInformation.Text = "";
            txtDetailsOfPreviousSickness.Text = "";

            //Radio Button List
            RdbIsEmployee.SelectedIndex = 0;
            RdbEmployeeContract.SelectedIndex = 0;
            RdbEmployeeAware.SelectedIndex = 0;
            RdbConsentGiven.SelectedIndex = 0;
            RdbEmployeeMedicalInsurance.SelectedIndex = 0;
        }

        #region Save form
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ReferralForm objReferral = new ReferralForm(id);

            objReferral.ReferralName = txtReferralName.Text;
            objReferral.ReferralJobTitle = txtReferralJobTitle.Text;
            objReferral.ReferralCompanyName = txtReferralCompanyName.Text;
            objReferral.ReferralAddress = txtReferralAddress.Text;
            objReferral.ReferralTelephone = txtReferralTelephone.Text;
            objReferral.ReferralEmail = txtReferralEmail.Text;

            if (txtDateReferral.Text != "")
            {
                if (txtDateReferral.Text != "00/00/0000" || txtDateReferral.Text != "01/01/1900")
                {
                    objReferral.DateOfReferral = string.Format("{0:dd/MMM/yyy}", Convert.ToDateTime(txtDateReferral.Text));
                }
                else
                {
                    objReferral.DateOfReferral = "";
                }
            }
            else
            {
                objReferral.DateOfReferral = "";
            }

            objReferral.EmployeeName = txtEmployeeName.Text;
            objReferral.EmployeeAddress = txtEmployeeAddress.Text;

            if (txtDateOfBirth.Text != "")
            {
                if (txtDateOfBirth.Text != "00/00/0000" || txtDateOfBirth.Text != "01/01/1900")
                {
                    objReferral.DateOfBirth = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateOfBirth.Text));
                }
                else
                {
                    objReferral.DateOfBirth = "";
                }
            }
            else
            {
                objReferral.DateOfBirth = "";
            }

            objReferral.EmployeeEmail = txtEmployeeEmail.Text;
            objReferral.EmployeeJobTitleAndLocation = txtEmployeeJobTitleAndLocation.Text;
            objReferral.EmployeeTelephone = txtEmployeeTelephone.Text;
            objReferral.LineManagerNameAndJobTitle = txtLineManagerNameAndJobTitle.Text;
            objReferral.LineManagerTelephoneOrEmail = txtLineManagerTelephoneOrEmail.Text;
            objReferral.IsEmployee = RdbIsEmployee.SelectedValue;
            objReferral.EmployeeContract = RdbEmployeeContract.SelectedValue;
            objReferral.PartTimeFromMonday = txtPartTimeFromMonday.Text;
            objReferral.PartTimeToMonday = txtPartTimeToMonday.Text;
            objReferral.PartTimeFromTuesday = txtPartTimeFromTuesday.Text;
            objReferral.PartTimeToTuesday = txtPartTimeToTuesday.Text;
            objReferral.PartTimeFromWednesday = txtPartTimeFromWednesday.Text;
            objReferral.PartTimeToWednesday = txtPartTimeToWednesday.Text;
            objReferral.PartTimeFromThursday = txtPartTimeFromThursday.Text;
            objReferral.PartTimeToThursday = txtPartTimeToThursday.Text;
            objReferral.PartTimeFromFriday = txtPartTimeFromFriday.Text;
            objReferral.PartTimeToFriday = txtPartTimeToFriday.Text;
            objReferral.EmployeeAware = RdbEmployeeAware.SelectedValue;
            objReferral.ConsentGiven = RdbConsentGiven.SelectedValue;
            objReferral.BroadOutline = txtBroadOutline.Text;

            if (txtDateOfIllness.Text != "")
            {
                if (txtDateOfIllness.Text != "00/00/0000" || txtDateOfIllness.Text != "01/01/1900")
                {
                    objReferral.DateOfIllness = string.Format("{0:dd/MMM/yyy}", Convert.ToDateTime(txtDateOfIllness.Text));
                }
                else
                {
                    objReferral.DateOfIllness = "";
                }
            }
            else
            {
                objReferral.DateOfIllness = "";
            }

            objReferral.OtherInformation = txtOtherInformation.Text;
            objReferral.OtherAdvice = txtOtherAdvice.Text;
            objReferral.AddRelevantInformation = txtAddRelevantInformation.Text;
            objReferral.DetailsOfPreviousSickness = txtDetailsOfPreviousSickness.Text;
            objReferral.EmployeeMedicalInsurance = RdbEmployeeMedicalInsurance.SelectedValue;

            objReferral.Save();
            if (objReferral.ReferralFormID != null)
            {
                lblMsg.Text = AntiXss.HtmlEncode("Referral Form Saved Successfully.");
                //ScriptManager.RegisterStartupScript(this,this.GetType(),"Alert", "alert('Referral Form Saved Successfully.');", true);               
                startConversion = true;         //This is used to Export form in PDF                
            }
            else
            {
                lblMsg.Text = AntiXss.HtmlEncode("Referral Form not Saved Successfully.");
                //ScriptManager.RegisterStartupScript(this, this.GetType(),"Alert", "alert('Referral Form not Saved Successfully.');", true);
            }
        }


        protected override void Render(HtmlTextWriter writer)        
        {
            
            if (startConversion)
            {
                // read parameters from the webpage            
                string url = HttpContext.Current.Request.Url.AbsoluteUri;

                string pdf_page_size = "A4";
                SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize),
                    pdf_page_size, true);

                //string pdf_orientation = DdlPageOrientation.SelectedValue;
                string pdf_orientation = "Portrait";
                PdfPageOrientation pdfOrientation =
                    (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    pdf_orientation, true);

                int webPageWidth = 1300;
                int webPageHeight = 0;

                // get html of the page
                TextWriter myWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(myWriter);
                base.Render(htmlWriter);

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                //// set converter options

                //converter.Options.VisibleWebElementId = "MainContent_divMainDashboard";  //This Property works in Select.Pdf dll not in Select.HtmlToPdf dll,for both of them same namespace used          
                //converter.Options.RenderPageOnTimeout = true;
                converter.Options.MinPageLoadTime = 2;
                //converter.Options.MaxPageLoadTime = 600;
                //converter.Options.JavaScriptEnabled = true;
                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = webPageWidth;
                converter.Options.WebPageHeight = webPageHeight;
                converter.Options.ExternalLinksEnabled = false;
                //converter.Options.MarginTop = 5;
                //converter.Options.MarginBottom = 5;
                //converter.Options.MarginRight = 5;
                //converter.Options.MarginLeft = 5;
                //converter.Options.PluginsEnabled=true;

                // create a new pdf document converting the html string of the page
                PdfDocument doc = converter.ConvertHtmlString(
                    myWriter.ToString(), Request.Url.AbsoluteUri);

                // save pdf document
                doc.Save(Response, false, "ReferralForm_" + DateTime.Now + ".pdf");


                // close pdf document
                doc.Close();
            }
            else
            {
                // render web page in browser
                base.Render(writer);                
            }
        }
        #endregion

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ReferralFormList.aspx");
        }

        #region Set Employee Contract Radio Button selection
        protected void RdbEmployeeContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RdbEmployeeContract.SelectedIndex == 1)    //When Selecting Part-time Radio button
            {
                divPartTimeDetails.Visible = true;
            }
            else
            {
                divPartTimeDetails.Visible = false;

                //Set to blank data when Fulltime Radio button is selected 
                txtPartTimeFromMonday.Text = "";
                txtPartTimeToMonday.Text = "";
                txtPartTimeFromTuesday.Text = "";
                txtPartTimeToTuesday.Text = "";
                txtPartTimeFromWednesday.Text = "";
                txtPartTimeToWednesday.Text = "";
                txtPartTimeFromThursday.Text = "";
                txtPartTimeToThursday.Text = "";
                txtPartTimeFromFriday.Text = "";
                txtPartTimeToFriday.Text = "";
            }
        }
        #endregion
    }
}