﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB
{
    public partial class ReferralFormList : System.Web.UI.Page
    {
        ReferralForm objReferralForm = new ReferralForm();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Setting Hard coded Session Value for Demo Use         
            //Session["LoggedInUserId"] = 150;
            //Session["TermsAccepted"] = "1";
            //Session["LogUserEmailAdd"] = "priyesh@rhealtech.com";
            //Session["UserType"] = "Admin";
            //Session["LoggedInUserName"] = "Priyesh";
            //Session["LogUserActiveAcct"] = 1;
            //Session["LoggedInUserPassword"] = "Priyesh127";
            //Session["PasswordExpiryDate"] = DateTime.Now.AddDays(+30);

            if (!Page.IsPostBack)
            {
                if (Session["UserType"] != null)
                {
                    GetList();
                }
                else
                {
                    Server.Transfer("~/Accessdenied.aspx");
                }                
            }
        }

        public void GetList()
        {
            DataSet ds = objReferralForm.GetList();
            if(ds!=null && ds.Tables.Count > 0)
            {
                gvReferralForm.DataSource = ds;
                gvReferralForm.DataBind();
            }
            else
            {
                gvReferralForm.DataSource = null;
                gvReferralForm.DataBind();
            }
        }

        protected void lnkAddReferralForm_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Referral.aspx");
        }

        #region Edit & Delete Referral Form
        protected void EditReferralForm(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);                                                
                Response.Redirect("~/Referral.aspx?ReferralFormId=" + index);
            }
            else if (e.CommandName == "Delete")
            {
                int index = Convert.ToInt32(e.CommandArgument);                
                objReferralForm.ReferralFormID= index;
                objReferralForm.Delete();
                this.GetList();
            }
        }

        protected void DeleteReferralForm(object sender, GridViewDeleteEventArgs e)
        {

        }
        #endregion
    }
}