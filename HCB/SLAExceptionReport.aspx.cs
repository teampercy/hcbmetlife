﻿using HCBBLL;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Winnovative.WnvHtmlConvert;

namespace HCB
{
    public partial class SLAExceptionReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lnkExport.Enabled = false;                
            }
        }

        protected void btnSLADateRange_Click(object sender, EventArgs e)
        {
            GetSLAExceptionReport();
        }

        private void GetSLAExceptionReport()
        {
            ClientInfo objClientInfo = new ClientInfo();
            try
            {
                Page.Validate("vgGenReport");
                if (Page.IsValid)
                {
                    DateTime startDate, endDate;
                    if (txtSLAFromDate.Text != "" && txtSLAToDate.Text != "")
                    {
                        startDate = Convert.ToDateTime(txtSLAFromDate.Text);
                        endDate = Convert.ToDateTime(txtSLAToDate.Text);
                    }
                    else
                    {
                        startDate = DateTime.MinValue;
                        endDate = DateTime.MinValue;
                    }

                    DataSet dsSLAExceptionData = objClientInfo.GetSLAExceptionData(startDate, endDate);
                    string date = startDate.ToString("dd/MM/yyyy");
                    if (dsSLAExceptionData != null && dsSLAExceptionData.Tables.Count > 0 && dsSLAExceptionData.Tables[0].Rows.Count > 0)
                    {
                        LoadDataset(dsSLAExceptionData.Tables[0]);
                        ViewState["date"] = date;
                        ViewState.Add("dsreport", dsSLAExceptionData);
                        lblerr.Text = AntiXss.HtmlEncode("");
                        lnkExport.Enabled = true;
                    }
                    else
                    {
                        ViewState["dsreport"] = null;
                        divReport.InnerHtml = "";
                        lblerr.Text = AntiXss.HtmlEncode("No record exists");
                        lnkExport.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void LoadDataset(DataTable dt)
        {
            #region code for dynemic report genretion
            StringBuilder sb = new StringBuilder();

            sb.Append("<table border=\"2\" class=\"table-striped table-bordered table-hover table-header-fixed\" rel=\"datatable\" id=\"sample_2\">");
            sb.Append("<thead>");
            sb.Append(" <tr class=\"font-white bg-green-steel\">");
            if (txtSLAFromDate.Text != "" && txtSLAToDate.Text != "")
            {
                sb.Append("<td colspan='" + dt.Columns.Count + "' ><span class=\"font-lg font-white text-left bold\">SLA Exception Report From " + txtSLAFromDate.Text + " To " + txtSLAToDate.Text + "</span></td></tr>");
            }
            else
            {
                sb.Append("<td colspan='" + dt.Columns.Count + "' ><span class=\"font-lg font-white text-left bold\">SLA Exception Report From 01/01/" + DateTime.Now.Year.ToString() + " To 31/12/" + DateTime.Now.Year.ToString() + "</span></td></tr>");
            }
            sb.Append("<tr class=\"font-white bg-green-steel\">");

            foreach (DataColumn dc in dt.Columns)
            {
                sb.Append(" <th class=\"text-center\"><u>" + dc.ColumnName + "</u></th> ");
            }
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            DataTable dt2 = dt;
            if (dt2.Rows.Count > 0)
            {
                foreach (DataRow dr in dt2.Rows)
                {
                    sb.Append(" <tr>");
                    foreach (DataColumn dc in dt2.Columns)
                    {
                        if (dr.ToString() != "")
                        {
                            if (dc.ColumnName.ToString() == "HCB#")
                            {
                                string Hcbref = dr[dc.ColumnName].ToString();
                                Hcbref = Hcbref.Substring(3);
                                sb.Append("<td> <a onclick=\"openReportDetail('" + dr[dc.ColumnName].ToString() + "');\" id='Hcbreport' style='cursor:pointer;text-decoration:underline;color: blue'> " + dr[dc.ColumnName].ToString() + "</a> </td> ");                                
                            }
                            else
                            {
                                sb.Append(" <td>" + dr[dc.ColumnName].ToString() + "</td> ");
                            }
                        }
                        else
                            sb.Append(" <td>" + "." + "</td> ");
                    }
                    sb.Append("</tr> ");
                }
            }

            sb.Append("</tbody></table>");
            sb.Append("<div style='text-align:right;width:90%;font-size:small'>");
            sb.Append("</div>");

            divReport.InnerHtml = sb.ToString();

            #endregion
        }

        protected void lnkExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["dsreport"] != null)
                {
                    {
                        string strStyles = "<head><style> tr.even { background-color: #F5FDF5; }</style></head>";
                        switch (ddlExprotTo.SelectedItem.Value)
                        {
                            case "2":
                                ExportToExcel(strStyles);
                                break;
                            case "1":
                                ConvertHTMLStringToPDF(strStyles, divReport.InnerHtml, (string)ViewState["date"] + "");
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ConvertHTMLStringToPDF(string strStyle, string strHTML, string strDate)
        {
            DataSet ds = new DataSet();
            ds = MasterListBL.GetPDFLicensekey();
            string LicenseKeyPdf = string.Empty;
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    LicenseKeyPdf = ds.Tables[0].Rows[0]["PdfLicenseKey"].ToString();
                }
            }

            string htmlString = strStyle + strHTML;// strstyles + genrateHTML(strHTML);

            // Create the PDF converter. Optionally you can specify the virtual browser 
            // width as parameter. 1024 pixels is default, 0 means autodetect
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = LicenseKeyPdf;

            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Winnovative HTML to PDF Converter";

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);

            //========================== Generating pdf ======================================
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename=" + AntiXss.HtmlEncode(strDate) + "_" + AntiXss.HtmlEncode(DateTime.Now.ToString("ddMMyyyy")) + ".pdf; size=" + pdfBytes.Length.ToString());
            response.Flush();
            response.BinaryWrite(pdfBytes);
            response.Flush();
            response.End();
            // ================================== End here ==================================                                                       
        }

        public void ExportToExcel(string style)
        {
            try
            {
                Response.ContentType = "application/x-msexcel";
                Response.AddHeader("Content-Disposition", "attachment; filename=ExcelFile.xls");
                Response.ContentEncoding = Encoding.Default;
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                divReport.RenderControl(hw);
                Response.Write(style + tw.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}