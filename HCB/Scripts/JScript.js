﻿function DisableSubmitButton(SubmitButton) {
	//if asp.NET clientside validation exists then check if form
	//is validated and exit false if fail.
	if (typeof(Page_ClientValidate) == 'function') {
		if (!Page_ClientValidate()) return false
	};
		
	SubmitButton.value='Submitted';
	SubmitButton.disabled=true;
	return true;		
}

