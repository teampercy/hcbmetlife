﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB
{
    public partial class ServiceReqList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindList();
            }
        }

        private void BindList()
        {
            ServiceRequired srvReq = new ServiceRequired();
            DataSet ds = srvReq.GetServices();
            lstService.DataSource = ds.Tables["Service"];
            lstService.DataTextField = "Service";
            lstService.DataValueField = "ServiceID";
            lstService.DataBind();
        }
    }
}