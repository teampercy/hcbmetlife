﻿using HCBBLL;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HCB
{
    public partial class TestInitiaContactChart : System.Web.UI.Page
    {
        static int userId;
        static string usertype;
        static ClientInfo ClientInfo = new ClientInfo();
        private bool startConversion = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = 150;
            usertype = "Admin";
            Session["LoggedInUserId"] = 150;
            Session["TermsAccepted"] = "1";
            Session["LogUserEmailAdd"] = "priyesh@rhealtech.com";
            Session["UserType"] = "Admin";
            Session["LoggedInUserName"] = "Priyesh";
            Session["LogUserActiveAcct"] = 1;
            Session["LoggedInUserPassword"] = "Priyesh127";
            Session["PasswordExpiryDate"] = DateTime.Now.AddDays(+30);
        }

        [WebMethod]
        public static List<DashboardInitialChart> GetInitialChartData(InitialChartTypes ChartType, DateTime FromDate, DateTime ToDate)
        {
            return ClientInfo.GetInitialChartData(ChartType, userId, usertype, FromDate, ToDate);
        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            startConversion = true;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (startConversion)
            {
                // read parameters from the webpage                           
                string pdf_page_size = "A4";
                SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize),
                    pdf_page_size, true);

                //string pdf_orientation = DdlPageOrientation.SelectedValue;
                string pdf_orientation = "Portrait";
                PdfPageOrientation pdfOrientation =
                    (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    pdf_orientation, true);

                int webPageWidth = 1300;
                int webPageHeight = 0;

                // get html of the page
                TextWriter myWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(myWriter);
                base.Render(htmlWriter);

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                //// set converter options                               
                converter.Options.MinPageLoadTime = 5;
                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = webPageWidth;
                converter.Options.WebPageHeight = webPageHeight;
                converter.Options.ExternalLinksEnabled = false;

                // create a new pdf document converting the html string of the page
                PdfDocument doc = converter.ConvertHtmlString(
                    myWriter.ToString(), Request.Url.AbsoluteUri);

                // save pdf document
                doc.Save(Response, false, "Dashboard_" + DateTime.Now + ".pdf");

                // close pdf document
                doc.Close();
            }
            else
            {
                // render web page in browser
                base.Render(writer);
            }
        }
    }
}