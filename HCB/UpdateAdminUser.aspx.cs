﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class UpdateAdminUser : System.Web.UI.Page
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserType"] != null && Session["UserType"].ToString().ToLower() == "admin")
            {
                if (!Page.IsPostBack)
                {
                    FillNominatedNurse();
                    FillLoadUser();
                    GetClaimAssessor();
                    lblMsg.Text = AntiXss.HtmlEncode("");
                    hdnUserId.Value = "";
                    //ddlLoadUser.SelectedValue = "1";
                    BindData();
                    //btnDelete.Enabled = false;

                    if (Request.QueryString != null)
                    {
                        if (Request.QueryString["id"] != null)
                        {
                            if (Request.QueryString["id"].ToString() != "")
                            {
                                if (Request.QueryString["id"].ToString() == "2")
                                {
                                    trLoadUser.Visible = false;
                                    btnDelete.Enabled = false;
                                    btnCreate.Enabled = false;
                                    lblMsg.Style.Add("display", "block");
                                    lblMsg.Text = AntiXss.HtmlEncode("You are logged in for the first time. Please enter a new Password and Save to continue.");
                                    DisableMenuLinks();
                                }
                                else
                                {
                                    trLoadUser.Visible = true;
                                    btnDelete.Enabled = true;
                                    btnCreate.Enabled = true;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //Response.Redirect("~/Accessdenied.aspx", false);
                Server.Transfer("~/Accessdenied.aspx");
            }
        }

        protected void ddlLoadUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BindData();
                ddlLoadUser.SelectedIndex = 0;
                lblMsg.Text = AntiXss.HtmlEncode("");
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Clear();
            btnDelete.Enabled = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Users users = new Users();
                int UId = 0;
                if (txtUserName.Text != "")
                {
                    users.EmailAdd = txtUserName.Text;
                }

                if (txtName.Text != "")
                {
                    users.Name = txtName.Text;
                }

                if (txtPassword.Text != "")
                {
                    //users.password = txtPassword.Text;
                    //Added to encrypt password
                    users.password = Encryption.Encrypt(txtPassword.Text);
                }

                if (txtAddress.Text != "")
                {
                    users.Address = txtAddress.Text;
                }

                if (txtCity.Text != "")
                {
                    users.City = txtCity.Text;
                }

                if (txtTeleNum.Text != "")
                {
                    users.PhoneNum = txtTeleNum.Text;
                }

                if (ddlAcctStatus.SelectedItem.Text == "Enabled")
                {
                    users.AccountActive = 1;
                }
                else if (ddlAcctStatus.SelectedItem.Text == "Disabled")
                {
                    users.AccountActive = 0;
                }

                if (ddlUserType.SelectedItem.Text != "")
                {
                    users.UserType = ddlUserType.SelectedItem.Text;
                }

                if (ddlNominatedNurse.SelectedIndex > 0)
                {
                    users.NominatedNurseId = Convert.ToInt32(ddlNominatedNurse.SelectedValue);
                }

                if (ddlInsCompName.SelectedIndex > 0)
                {
                    users.IncoId = Convert.ToInt32(ddlInsCompName.SelectedValue);
                }
                if (txtUserName.Text != "")
                {
                    users.EmailAdd = txtUserName.Text;
                }

                if (txtName.Text != "")
                {
                    users.Name = txtName.Text;
                }        
               
                users.PasswordExpiryDate = DateTime.Now.AddDays(60);
                //if (DateTime.Now > Convert.ToDateTime(ViewState["PasswordExpiryDate"]))
                //{
                //    if (ViewState["oldPassword"] != null)
                //    {
                //        string oldPass = (string)ViewState["oldPassword"];
                //        oldPass = Encryption.Decrypt((string)ViewState["oldPassword"]);
                //        if (oldPass == txtPassword.Text.Trim())
                //        {
                //            if (ViewState["PasswordExpiryDate"] != null)
                //            {
                //                if (users.PasswordExpiryDate > DateTime.Now)
                //                {
                //                    users.PasswordExpiryDate = (DateTime)ViewState["PasswordExpiryDate"];
                //                }
                //                else
                //                {
                //                    lblMsg.Text = "You cannot enter same password again.";
                //                    return;
                //                }
                //            }
                //            else
                //                users.PasswordExpiryDate = DateTime.Now.AddDays(90);
                //        }
                //        else
                //            users.PasswordExpiryDate = DateTime.Now.AddDays(90);
                //    }
                //    else
                //    {
                //        users.PasswordExpiryDate = DateTime.Now.AddDays(90);
                //    }
                //}
                //else
                //{
                //    users.password = Encryption.Encrypt(txtPassword.Text);
                //    users.PasswordExpiryDate = Convert.ToDateTime(ViewState["PasswordExpiryDate"]);
                //}

                if (hdnUserId.Value != "" && hdnUserId.Value != "0" && hdnUserId.Value != "-1")
                {
                    if (ViewState["AccountActive"] != null)
                    {
                        if (ViewState["AccountActive"].ToString() == ddlAcctStatus.SelectedValue.ToString())
                        {
                            if (ViewState["LastLoginDate"] != null)
                            {
                                if (ViewState["LastLoginDate"].ToString() == "")
                                {
                                    users.LastLogin = "";
                                }
                                else
                                {
                                    users.LastLogin = ViewState["LastLoginDate"].ToString();
                                }
                            }
                            else
                            {
                                users.LastLogin = DateTime.Now.ToString();
                            }

                            users.AccountActive = Convert.ToInt32(ViewState["AccountActive"]);
                        }
                        else if (ddlAcctStatus.SelectedValue == "1")
                        {
                            users.LastLogin = DateTime.Now.ToString();
                            users.AccountActive = Convert.ToInt32(ddlAcctStatus.SelectedValue);
                        }
                    }

                }
                else
                {
                    if (ddlAcctStatus.SelectedIndex > 0)
                    {
                        users.AccountActive = Convert.ToInt32(ddlAcctStatus.SelectedValue);
                    }
                }

                if (hdnUserId.Value != "" && hdnUserId.Value != "0" && hdnUserId.Value != "-1")
                {
                    try
                    {
                        DataSet ds = new DataSet();
                        ds = users.usp_PasswordCheckAlreadyExist(Convert.ToInt32(hdnUserId.Value.ToString()), users.password);
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows[0]["ErrorMessage"].ToString() == "1")
                                {
                                    users.UserId = Convert.ToInt32(hdnUserId.Value);
                                    users.FaildAttempt = 0;
                                    UId = users.Save();
                                    if (UId != 0)
                                    {
                                        lblMsg.Text = AntiXss.HtmlEncode("Users details are Updated.");
                                        Session["PasswordExpiryDate"] = users.PasswordExpiryDate;
                                        Response.Redirect("~/Default.aspx", false);

                                    }
                                    else
                                    {
                                        lblMsg.Text = AntiXss.HtmlEncode("Users details are Not Updated.");
                                    }
                                }
                                else if (ds.Tables[0].Rows[0]["ErrorMessage"].ToString() == "2")
                                {
                                lblMsg.Text = AntiXss.HtmlEncode("Passwords must not be changed more frequently than once a day");
                                }
                                else if (ds.Tables[0].Rows[0]["ErrorMessage"].ToString() == "0")
                                {
                                    lblMsg.Text = AntiXss.HtmlEncode("New password cannot be one of the 12 previously used passwords.");
                                }
                            }
                            //else
                            //{
                            //    lblMsg.Text = AntiXss.HtmlEncode("New password cannot be one of the 12 previously used passwords.");
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMsg.Text = AntiXss.HtmlEncode("Users details are Not Updated.");
                    }

                    btnDelete.Enabled = true;
                }
                else
                {
                    try
                    {
                        users.UserId = 0;
                        users.FaildAttempt = 0;
                        users.TermsAccepted = 1;
                        //users.PasswordExpiryDate = DateTime.Now.AddDays(-1);
                        UId = users.Save();

                        if (UId != 0)
                        {
                            hdnUserId.Value = UId.ToString();
                            if (UId == -1)
                                lblMsg.Text = AntiXss.HtmlEncode("Email already registered");
                            else
                            {
                                lblMsg.Text = AntiXss.HtmlEncode("Users details are Saved.");
                                InsertAmendment();
                            }


                            FillLoadUser();
                        }
                        else
                        {
                            lblMsg.Text = AntiXss.HtmlEncode("Users details are Not Saved.");
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMsg.Text = AntiXss.HtmlEncode("Users details are Not Saved.");
                    }

                    btnDelete.Enabled = true;
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Users usrDel = new Users();
                bool DelUser;
                if (hdnUserId.Value != "")
                {
                    usrDel.UserId = Convert.ToInt32(hdnUserId.Value);

                    DelUser = usrDel.Delete();
                    if (DelUser)
                    {
                        FillLoadUser();
                        Clear();
                        lblMsg.Text = AntiXss.HtmlEncode("User details are deleted!");
                    }
                    else
                    {
                        lblMsg.Text = AntiXss.HtmlEncode("User details are not deleted!");
                    }
                }
            }
            catch (Exception ex)
            {
                lblMsg.Text = AntiXss.HtmlEncode("User details are not deleted! ");
            }
        }

        #endregion

        #region Methods

        private void FillNominatedNurse()
        {
            try
            {
                DataSet ds = Users.GetSClaimConsultant();
                Users ouser = new Users();
                //DataSet ds = ouser.GetClaimConsultant();
                ddlNominatedNurse.DataSource = ds.Tables["Consultant"];
                ddlNominatedNurse.DataTextField = "FullName";
                ddlNominatedNurse.DataValueField = "UserID";
                ddlNominatedNurse.DataBind();
                ListItem item = new ListItem("--Select Nurse--", "0");
                ddlNominatedNurse.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Nurse--", "0");
                ddlNominatedNurse.Items.Insert(0, item);
            }
        }

        private void FillLoadUser()
        {
            try
            {
                Users ouser = new Users();
                DataSet ds = ouser.GetUsers();
                ddlLoadUser.DataSource = ds.Tables["Users"];
                ddlLoadUser.DataTextField = "FullName";
                ddlLoadUser.DataValueField = "UserID";
                ddlLoadUser.DataBind();
                ListItem item = new ListItem("--Select to Load User--", "0");
                ddlLoadUser.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select to Load User--", "0");
                ddlLoadUser.Items.Insert(0, item);
            }
        }

        private void GetClaimAssessor()
        {
            try
            {
                Users user = new Users();
                DataSet ds = user.GetClaimAssessor();
                ddlInsCompName.DataSource = ds.Tables["Users"];
                ddlInsCompName.DataTextField = "FullName";
                ddlInsCompName.DataValueField = "UserID";
                ddlInsCompName.DataBind();
                ListItem item = new ListItem("--Select Insurance Company--", "0");
                ddlInsCompName.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Insurance Company--", "0");
                ddlInsCompName.Items.Insert(0, item);
            }
        }

        private void BindData()
        {

            Users usr = new Users();

            if (ddlLoadUser.SelectedValue == "0")
            {
                if (Session["LoggedInUserId"] != null)
                {
                    if (Session["LoggedInUserId"].ToString() != "")
                    {
                        usr.UserId = Convert.ToInt32(Session["LoggedInUserId"]);
                        hdnUserId.Value = Session["LoggedInUserId"].ToString();
                    }
                    else
                    {
                        if (ddlLoadUser.SelectedValue != "0")
                        {
                            usr.UserId = Convert.ToInt32(ddlLoadUser.SelectedValue);
                            hdnUserId.Value = ddlLoadUser.SelectedValue;
                        }
                        else
                        {
                            usr.UserId = 0;
                        }
                    }
                }
                else
                {
                    if (ddlLoadUser.SelectedValue != "0")
                    {
                        usr.UserId = Convert.ToInt32(ddlLoadUser.SelectedValue);
                        hdnUserId.Value = ddlLoadUser.SelectedValue;
                    }
                    else
                    {
                        usr.UserId = 0;
                    }
                }
            }
            else
            {
                usr.UserId = Convert.ToInt32(ddlLoadUser.SelectedValue);
                hdnUserId.Value = ddlLoadUser.SelectedValue;
            }

            DataSet ds = usr.GetUserDet();

            if (ds != null)
            {
                DataTable dt = ds.Tables["UserDet"];

                try
                {
                    txtUserName.Text = usr.EmailAdd;
                }
                catch (Exception ex)
                {
                    txtUserName.Text = "";
                }

                try
                {
                    txtName.Text = usr.Name;
                }
                catch (Exception ex)
                {
                    txtName.Text = "";
                }

                try
                {
                   // txtPassword.Text = usr.password;
                    string UserPwd = Encryption.Decrypt(usr.password);
                    txtPassword.Attributes.Add("value", UserPwd);
                }
                catch (Exception ex)
                {
                    txtPassword.Text = "";
                }

                try
                {
                    txtAddress.Text = usr.Address;
                }
                catch (Exception ex)
                {
                    txtAddress.Text = "";
                }

                try
                {
                    txtCity.Text = usr.City;
                }
                catch(Exception ex)
                {
                    txtCity.Text = "";
                }

                try
                {
                    txtTeleNum.Text = usr.PhoneNum;
                }
                catch (Exception ex)
                {
                    txtTeleNum.Text = "";
                }
                if (ViewState["oldPassword"] != null)

                    ViewState["oldPassword"] = usr.password;
                else
                    ViewState.Add("oldPassword", usr.password);

                //if (usr.PasswordExpiryDate > DateTime.Now)
                // {
                if (ViewState["PasswordExpiryDate"] != null)
                    ViewState["PasswordExpiryDate"] = usr.PasswordExpiryDate;
                else
                    ViewState.Add("PasswordExpiryDate", usr.PasswordExpiryDate);
                // }

                if (ViewState["LastLoginDate"] != null)
                {
                    ViewState["LastLoginDate"] = usr.LastLogin;
                }
                else
                {
                    ViewState.Add("LastLoginDate", usr.LastLogin);
                }

                try
                {
                    if (dt.Rows[0]["AccountActive"].ToString() == "1")
                    {
                        if (ViewState["AccountActive"] != null)
                        {
                            ViewState["AccountActive"] = dt.Rows[0]["AccountActive"].ToString();
                        }
                        else
                        {
                            ViewState.Add("AccountActive", dt.Rows[0]["AccountActive"].ToString());
                        }

                        ddlAcctStatus.SelectedIndex = ddlAcctStatus.Items.IndexOf(ddlAcctStatus.Items.FindByValue("1"));
                        //ddlAcctStatus.Items.FindByValue("1").Selected = true;
                    }
                    else if (dt.Rows[0]["AccountActive"].ToString() == "0")
                    {
                        if (ViewState["AccountActive"] != null)
                        {
                            ViewState["AccountActive"] = dt.Rows[0]["AccountActive"].ToString();
                        }
                        else
                        {
                            ViewState.Add("AccountActive", dt.Rows[0]["AccountActive"].ToString());
                        }

                        ddlAcctStatus.SelectedIndex = ddlAcctStatus.Items.IndexOf(ddlAcctStatus.Items.FindByValue("2"));
                    }
                    else
                    {
                        ddlAcctStatus.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {
                    ddlAcctStatus.SelectedValue = "0";
                }

                try
                {
                    if (dt.Rows[0]["UserType"].ToString() != "")
                    {
                        ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(ddlUserType.Items.FindByText(usr.UserType));
                        // ddlUserType.Items.FindByText(usr.UserType).Selected = true;
                    }
                    else
                    {
                        ddlUserType.SelectedValue = "0";
                    }
                }
                catch (Exception ex)
                {
                    ddlUserType.SelectedValue = "0";
                }

                try
                {
                    if (dt.Rows[0]["NominatedNurseID"].ToString() != "")
                    {
                        ddlNominatedNurse.SelectedIndex = ddlNominatedNurse.Items.IndexOf(ddlNominatedNurse.Items.FindByValue(usr.NominatedNurseId.ToString()));
                        //ddlNominatedNurse.Items.FindByValue(usr.NominatedNurseId.ToString()).Selected = true;
                    }
                    else
                    {
                        ddlNominatedNurse.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {
                    ddlNominatedNurse.SelectedIndex = 0;
                }
            }

            btnDelete.Enabled = true;
        }

        private void Clear()
        {
            txtAddress.Text = "";
            txtCity.Text = "";
            txtName.Text = "";
            txtPassword.Text = "";
            txtPassword.Attributes.Add("value", "");
            txtTeleNum.Text = "";
            txtUserName.Text = "";
            ddlAcctStatus.SelectedIndex = 0;
            ddlLoadUser.SelectedIndex = 0;
            ddlNominatedNurse.SelectedIndex = 0;
            ddlUserType.SelectedIndex = 0;
            ddlInsCompName.SelectedIndex = 0;
            hdnUserId.Value = "";
            lblMsg.Text = AntiXss.HtmlEncode("");
            hdnUserId.Value = "";
        }

        public void InsertAmendment()
        {
            
            int userid = (int)Session["LoggedInUserId"];
            //string Hcbref = Session["ClientHcbRef"].ToString();
            MasterListBL MBL = new MasterListBL();
            int AmdId = MBL.InsertAmndment(0, userid, 2);
            //if (AmdId != 0)
            //{
            //    if (Session["Amdid"] != null)
            //    {
            //        Session["AmdId"] = AmdId;
            //    }
            //    else Session.Add("AmdId", AmdId);
            //}
        }

        private void DisableMenuLinks()
        {
            HyperLink hypViewOpen = (HyperLink)this.Page.Master.FindControl("hpViewOpen");
            if (hypViewOpen != null)
            {
                hypViewOpen.Enabled = false;
                hypViewOpen.Style.Add("text-decoration", "none");
                hypViewOpen.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypViewClose = (HyperLink)this.Page.Master.FindControl("hpViewClose");
            if (hypViewClose != null)
            {
                hypViewOpen.Enabled = false;
                hypViewOpen.Style.Add("text-decoration", "none");
                hypViewOpen.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypSearch = (HyperLink)this.Page.Master.FindControl("hpSearch");
            if (hypSearch != null)
            {
                hypSearch.Enabled = false;
                hypSearch.Style.Add("text-decoration", "none");
                hypSearch.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypCreate = (HyperLink)this.Page.Master.FindControl("hpCreate");
            if (hypCreate != null)
            {
                hypCreate.Enabled = false;
                hypCreate.Style.Add("text-decoration", "none");
                hypCreate.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypAuditTrail = (HyperLink)this.Page.Master.FindControl("hpAuditTrail");
            if (hypAuditTrail != null)
            {
                hypAuditTrail.Enabled = false;
                hypAuditTrail.Style.Add("text-decoration", "none");
                hypAuditTrail.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypSiteMap = (HyperLink)this.Page.Master.FindControl("hpSitemap");
            if (hypSiteMap != null)
            {
                hypSiteMap.Enabled = false;
                hypSiteMap.Style.Add("text-decoration", "none");
                hypSiteMap.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypAdmin = (HyperLink)this.Page.Master.FindControl("hpAdministration");
            if (hypAdmin != null)
            {
                hypAdmin.Enabled = false;
                hypAdmin.Style.Add("text-decoration", "none");
                hypAdmin.ForeColor = System.Drawing.Color.LightGray;
            }
        }
       
        #endregion        
    }
}