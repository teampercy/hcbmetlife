﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClaimPerBrokerChart.ascx.cs" Inherits="HCB.UserControls.ClaimPerBrokerChart" %>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption ">
            <span class="caption-subject font-dark bold uppercase">Broker</span>
            <span class="caption-helper">Top 5 Brokers</span>
        </div>
        <%--<asp:Button runat="server" ID="btnExport" OnClick="btnExport_Click"/>--%>
    </div>
    <div class="portlet-body">
        <div id="ClaimPerBrokerChart" style="width: 100%; height: 300px">
        </div>
    </div>
</div>

<script type="text/javascript">
    function DrawClaimPerBrokerChart() {
        $.ajax({
            url: "Default.aspx/GetTotalClaimPerBroker",
            type: "POST",
            contentType: "application/json;charset=utf-8",
            success: function (res) {                
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Broker');
                data.addColumn('number', 'Claims');
                $.each(res.d, function (BrokerName, TotalCases) {
                    data.addRow([BrokerName, TotalCases]);
                });

                var options = {
                    is3D: true,
                    //legend: "bottom",
                    //pieSliceText: 'value',
                    title: "Total Cases per Broker"
                };
                var chart = new google.visualization.PieChart($('#ClaimPerBrokerChart')[0]);
                chart.draw(data, options);

                //var image = '<img src="' + chart.getImageURI() + '">';
                //$("[id*=btnExport]").click(function () {
                //    $("input[name=chart_data]").val(chart.getImageURI());
                //});
            },
            failure: function (response) {
                alert("Unable to display Broker chart");
            }
        });
    }
</script>

