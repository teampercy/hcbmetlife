﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClaimPerSchemeChart.ascx.cs" Inherits="HCB.UserControls.ClaimPerSchemeChart" %>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption ">
            <span class="caption-subject font-dark bold uppercase">Scheme</span>
            <span class="caption-helper">Top 5 Schemes</span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="ClaimPerSchemeChart" style="width: 100%; height: 300px">
        </div>
    </div>
</div>

<script type="text/javascript">
    function DrawClaimPerSchemeChart() {
        $.ajax({
            url: "Default.aspx/GetTotalClaimPerScheme",
            type: "POST",
            contentType: "application/json;charset=utf-8",
            success: function (res) {
                
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Scheme');
                data.addColumn('number', 'Claims');
                $.each(res.d, function (SchemeName, TotalCases) {
                    data.addRow([SchemeName, TotalCases]);
                });

                var options = {
                    is3D: true,
                    //legend: "bottom",
                    //pieSliceText: 'value',
                    title: "Total Cases per Scheme"
                };
                var chart = new google.visualization.PieChart($('#ClaimPerSchemeChart')[0]);
                chart.draw(data, options);
            },
            failure: function (response) {
                alert("Unable to display Scheme chart");
            }
        });
    }
</script>
