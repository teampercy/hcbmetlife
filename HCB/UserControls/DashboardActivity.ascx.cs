﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB.UserControls
{
    public partial class DashboardActivity : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                LoadBrokerCases();
            }
        }

        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlDashboardBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlDashboardBroker.DataTextField = "AssessorTeamName";
                ddlDashboardBroker.DataValueField = "ID";
                ddlDashboardBroker.DataBind();
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardBroker.Items.Insert(0, item);
            }
        }

        protected void ddlDashboardBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadBrokerCases();
            }
            catch (Exception ex)
            {

            }
        }

        private void LoadBrokerCases()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlDashboardBroker.SelectedValue);

            DateTime startDate, endDate;
            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtFromDate.Text);
                endDate = Convert.ToDateTime(txtToDate.Text);                
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue; 
            }

            DataSet dsOpenCases = objClientInfo.GetDashboradAssessorCasesList(UserId, usertype, true, AssessorTeamId,startDate,endDate);
            if (dsOpenCases == null)
            {
                btnOpen.Text = "Open Cases (0)";
            }
            else
            {
                btnOpen.Text = "Open Cases (" + dsOpenCases.Tables["DashboardBrokerCasesList"].Rows.Count + ")";
            }            

            DataSet dsClosedCases = objClientInfo.GetDashboradAssessorCasesList(UserId, usertype, false, AssessorTeamId,startDate,endDate);
            if (dsClosedCases == null)
            {
                btnClose.Text = "Closed Cases (0)";
            }
            else
            {
                btnClose.Text = "Closed Cases (" + dsClosedCases.Tables["DashboardBrokerCasesList"].Rows.Count + ")";
            }

            //Stored Dataset value in viewstate for using on Button click event
            ViewState["vwOpenCases"] = dsOpenCases;
            ViewState["vwClosedCases"] = dsClosedCases;
        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "HCBRef")
            {
                if (Session["NewHCBRef"] != null)
                {
                    if (Session["NewHCBRef"].ToString() != "")
                    {
                        Session["NewHCBRef"] = null;
                    }
                }
                Session["ClientHcbRef"] = e.CommandArgument;
                if (Session["ds"] != null) Session["ds"] = null;
                Response.Redirect("~/ClientRecord.aspx");
            }
        }

        protected void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                lblPopupHeader.Text = "Open Cases";
                DataSet dsOpenCases = (DataSet)ViewState["vwOpenCases"];
                BindGrid(dsOpenCases);                

                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenCases", "DashboardPopup('#divBrokerCases');", true);
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                lblPopupHeader.Text = "Closed Cases";
                DataSet dsClosedCases = (DataSet)ViewState["vwClosedCases"];
                BindGrid(dsClosedCases);                

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosedCases", "DashboardPopup('#divBrokerCases');", true);
            }
            catch (Exception ex)
            {

            }
        }

        private void BindGrid(DataSet ds)
        {
            DataTable dt = new DataTable();
            if (ds != null)
            {
                dt = ds.Tables["DashboardBrokerCasesList"];
                try
                {
                    rptResults.DataSource = dt;
                    rptResults.DataBind();
                }
                catch (Exception ex)
                {
                    rptResults.DataSource = ds;
                    rptResults.DataBind();
                }
            }
            else
            {
                rptResults.DataSource = null;
                rptResults.DataBind();
            }
        }

        protected void btnDateRange_Click(object sender, EventArgs e)
        {
            LoadBrokerCases();
        }

        //protected void txtFromDate_TextChanged(object sender, EventArgs e)
        //{
        //    LoadBrokerCases();
        //}        
    }
}