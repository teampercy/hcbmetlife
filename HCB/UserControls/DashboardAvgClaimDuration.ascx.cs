﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB.UserControls
{
    public partial class DashboardAvgClaimDuration : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                CalculateClaimantAverageClaimDuration();
                btnOpenAll.Style.Add("border-color", "#3598dc");
            }
        }

        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlDashboardClaimDurationBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlDashboardClaimDurationBroker.DataTextField = "AssessorTeamName";
                ddlDashboardClaimDurationBroker.DataValueField = "ID";
                ddlDashboardClaimDurationBroker.DataBind();
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardClaimDurationBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardClaimDurationBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlDashboardClaimDurationScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlDashboardClaimDurationScheme.DataTextField = "SchemeName";
                ddlDashboardClaimDurationScheme.DataValueField = "ID";
                ddlDashboardClaimDurationScheme.DataBind();
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardClaimDurationScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardClaimDurationScheme.Items.Insert(0, item);
            }
        }
        #endregion

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            ddlDashboardClaimDurationScheme.SelectedIndex = 0;
            ddlDashboardClaimDurationBroker.SelectedIndex = 0;

            btnOpenAll.Style.Add("border-color", "#3598dc");
            ddlDashboardClaimDurationScheme.Style.Remove("border-color");
            ddlDashboardClaimDurationBroker.Style.Remove("border-color");

            CalculateClaimantAverageClaimDuration();
        }

        protected void ddlDashboardClaimDurationScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardClaimDurationBroker.SelectedIndex = 0;
            ddlDashboardClaimDurationScheme.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardClaimDurationBroker.Style.Remove("border-color");

            CalculateClaimantAverageClaimDuration();
        }

        protected void ddlDashboardClaimDurationBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardClaimDurationScheme.SelectedIndex = 0;
            ddlDashboardClaimDurationBroker.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardClaimDurationScheme.Style.Remove("border-color");

            CalculateClaimantAverageClaimDuration();
        }

        protected void btnClaimDurationDateRange_Click(object sender, EventArgs e)
        {
            CalculateClaimantAverageClaimDuration();
        }

        private void CalculateClaimantAverageClaimDuration()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlDashboardClaimDurationBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlDashboardClaimDurationScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtClaimDurationFromDate.Text != "" && txtClaimDurationToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtClaimDurationFromDate.Text);
                endDate = Convert.ToDateTime(txtClaimDurationToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsAverageClaimDuration = objClientInfo.GetDashboardAverageClaimDuration(UserId, usertype, AssessorTeamId, SchemeNameId, startDate, endDate);
            if (dsAverageClaimDuration.Tables["DashboardAverageClaimDuration"].Rows[0]["AvgClaimDuration"].ToString() != null && dsAverageClaimDuration.Tables["DashboardAverageClaimDuration"].Rows[0]["AvgClaimDuration"].ToString() != "")
            {
                lblAvgClaimDurationValue.Text = dsAverageClaimDuration.Tables["DashboardAverageClaimDuration"].Rows[0]["AvgClaimDuration"].ToString(); //Print the Average Claim Duration Value
            }
            else
            {
                lblAvgClaimDurationValue.Text = "0";
            }
        }
    }
}