﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardAvgRTWDuration.ascx.cs" Inherits="HCB.UserControls.DashboardAvgRTWDuration" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Average RTW Duration</span>
                </div>                
            </div>

            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnOpenAll" CssClass="btn form-control input-medium"
                                        style="border:1px solid #c2cad8;" Text="ALL Cases" OnClick="btnOpenAll_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Scheme Name</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardRTWDurationScheme" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardRTWDurationScheme_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Broker</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardRTWDurationBroker" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardRTWDurationBroker_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Date Range</label>
                                    <div class="col-md-6">
                                        <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                            <asp:TextBox runat="server" CssClass="form-control" name="from" ID="txtRTWDurationFromDate"></asp:TextBox>
                                            <span class="input-group-addon">to </span>
                                            <asp:TextBox runat="server" CssClass="form-control" name="to" ID="txtRTWDurationToDate"></asp:TextBox>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">Select date range </span>
                                    </div>
                                    <div class="col-md-1">                                        
                                        <asp:LinkButton runat="server" ID="btnRTWDurationDateRange" CssClass="btn green" OnClick="btnRTWDurationDateRange_Click">
                                            <i class="fa fa-check-circle"></i>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="pricing-content-1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="price-column-container border-active">
                                        <div class="price-table-head bg-green">
                                            <h2 class="no-margin">Average RTW Duration</h2>
                                        </div>
                                        <div class="arrow-down border-top-green"></div>
                                        <div class="price-table-pricing">
                                            <h3>
                                                <sup class="price-sign">#</sup>
                                                <asp:Label runat="server" ID="lblAvgRTWDurationValue"></asp:Label>
                                            </h3>
                                            <p>Days</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>    

<script type="text/javascript">
    function pageLoad(sender, args) {
        $(document).ready(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
    }
</script>