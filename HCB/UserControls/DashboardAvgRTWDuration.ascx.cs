﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
//using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;
//using Winnovative.WnvHtmlConvert;
//using System.Data.Common;
//using System.Web.UI.HtmlControls;

namespace HCB.UserControls
{
    public partial class DashboardAvgRTWDuration : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                CalculateAverageRTWDuration();
                btnOpenAll.Style.Add("border-color", "#3598dc");
            }
        }

        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlDashboardRTWDurationBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlDashboardRTWDurationBroker.DataTextField = "AssessorTeamName";
                ddlDashboardRTWDurationBroker.DataValueField = "ID";
                ddlDashboardRTWDurationBroker.DataBind();
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Broker--", "0");
                ddlDashboardRTWDurationBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Broker--", "0");
                ddlDashboardRTWDurationBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlDashboardRTWDurationScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlDashboardRTWDurationScheme.DataTextField = "SchemeName";
                ddlDashboardRTWDurationScheme.DataValueField = "ID";
                ddlDashboardRTWDurationScheme.DataBind();
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Scheme Name--", "0");
                ddlDashboardRTWDurationScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("--Select Scheme Name--", "0");
                ddlDashboardRTWDurationScheme.Items.Insert(0, item);
            }
        }
        #endregion

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            ddlDashboardRTWDurationScheme.SelectedIndex = 0;
            ddlDashboardRTWDurationBroker.SelectedIndex = 0;

            btnOpenAll.Style.Add("border-color", "#3598dc");
            ddlDashboardRTWDurationScheme.Style.Remove("border-color");
            ddlDashboardRTWDurationBroker.Style.Remove("border-color");

            CalculateAverageRTWDuration();
        }

        protected void ddlDashboardRTWDurationScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardRTWDurationBroker.SelectedIndex = 0;
            ddlDashboardRTWDurationScheme.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardRTWDurationBroker.Style.Remove("border-color");

            CalculateAverageRTWDuration();
        }

        protected void ddlDashboardRTWDurationBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardRTWDurationScheme.SelectedIndex = 0;
            ddlDashboardRTWDurationBroker.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardRTWDurationScheme.Style.Remove("border-color");

            CalculateAverageRTWDuration();
        }

        protected void btnRTWDurationDateRange_Click(object sender, EventArgs e)
        {
            CalculateAverageRTWDuration();
        }

        private void CalculateAverageRTWDuration()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlDashboardRTWDurationBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlDashboardRTWDurationScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtRTWDurationFromDate.Text != "" && txtRTWDurationToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtRTWDurationFromDate.Text);
                endDate = Convert.ToDateTime(txtRTWDurationToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsAverageRTWDuration = objClientInfo.GetDashboardAverageRTWDuration(UserId, usertype, AssessorTeamId, SchemeNameId, startDate, endDate);
            if (dsAverageRTWDuration.Tables["DashboardAverageRTWDuration"].Rows[0]["AvgRTWDuration"].ToString() != null && dsAverageRTWDuration.Tables["DashboardAverageRTWDuration"].Rows[0]["AvgRTWDuration"].ToString() != "")
            {
                lblAvgRTWDurationValue.Text = dsAverageRTWDuration.Tables["DashboardAverageRTWDuration"].Rows[0]["AvgRTWDuration"].ToString(); //Print the Average RTW Duration Value
            }
            else
            {
                lblAvgRTWDurationValue.Text = "0";
            }
        }
                    
    }
}