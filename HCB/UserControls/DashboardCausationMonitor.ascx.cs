﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB.UserControls
{
    public partial class DashboardCausationMonitor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                BindGrid();
                btnOpenAll.Style.Add("border-color", "#3598dc");
            }
        }

        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlDashboardInjuryBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlDashboardInjuryBroker.DataTextField = "AssessorTeamName";
                ddlDashboardInjuryBroker.DataValueField = "ID";
                ddlDashboardInjuryBroker.DataBind();
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardInjuryBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardInjuryBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlDashboardInjuryScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlDashboardInjuryScheme.DataTextField = "SchemeName";
                ddlDashboardInjuryScheme.DataValueField = "ID";
                ddlDashboardInjuryScheme.DataBind();
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardInjuryScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardInjuryScheme.Items.Insert(0, item);
            }
        }
        #endregion

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            ddlDashboardInjuryScheme.SelectedIndex = 0;
            ddlDashboardInjuryBroker.SelectedIndex = 0;

            btnOpenAll.Style.Add("border-color", "#3598dc");
            ddlDashboardInjuryScheme.Style.Remove("border-color");
            ddlDashboardInjuryBroker.Style.Remove("border-color");

            BindGrid();
        }        

        protected void ddlDashboardInjuryScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardInjuryBroker.SelectedIndex = 0;
            
            ddlDashboardInjuryScheme.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardInjuryBroker.Style.Remove("border-color");

            BindGrid();
        }

        protected void ddlDashboardInjuryBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardInjuryScheme.SelectedIndex = 0;
            
            ddlDashboardInjuryBroker.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardInjuryScheme.Style.Remove("border-color");

            BindGrid();
        }

        private void BindGrid()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlDashboardInjuryBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlDashboardInjuryScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtInjuryFromDate.Text != "" && txtInjuryToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtInjuryFromDate.Text);
                endDate = Convert.ToDateTime(txtInjuryToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsInjuryPercent = objClientInfo.GetDashboardIllnessInjuryPercentage(UserId, usertype, AssessorTeamId, SchemeNameId, startDate, endDate);
            DataTable dt = new DataTable();
            if(dsInjuryPercent!=null)
            {
                dt = dsInjuryPercent.Tables["DashboardIllnessInjuryPercentage"];
                gvInjuryPercent.DataSource = dt;
                gvInjuryPercent.DataBind();
            }
            else
            {
                gvInjuryPercent.DataSource = null;
                gvInjuryPercent.DataBind();
            }
        }

        protected void btnDateRange_Click(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}