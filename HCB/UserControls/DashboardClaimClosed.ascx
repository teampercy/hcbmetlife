﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardClaimClosed.ascx.cs" Inherits="HCB.UserControls.DashboardClaimClosed" %>
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Closed Case Analysis</span>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnOpenAll" CssClass="btn form-control input-medium"
                                         style="border:1px solid #c2cad8;" Text="ALL Cases" OnClick="btnOpenAll_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Scheme Name</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardCaseClosedScheme" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardCaseClosedScheme_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Broker</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardCaseClosedBroker" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardCaseClosedBroker_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Date Range</label>
                                    <div class="col-md-6">
                                        <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                            <asp:TextBox runat="server" CssClass="form-control" name="from" ID="txtCaseClosedFromDate"></asp:TextBox>
                                            <span class="input-group-addon">to </span>
                                            <asp:TextBox runat="server" CssClass="form-control" name="to" ID="txtCaseClosedToDate"></asp:TextBox>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">Select date range </span>
                                    </div>
                                    <div class="col-md-1">                                        
                                        <asp:LinkButton runat="server" ID="btnCaseClosedDateRange" CssClass="btn green" OnClick="btnCaseClosedDateRange_Click">
                                            <i class="fa fa-check-circle"></i>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>                        

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-warning">
                                    <asp:GridView runat="server" ID="gvClosedCasePercent" AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered">
                                        <Columns>
                                            <asp:BoundField HeaderText="Closed Case Reason" DataField="ClaimClosedReason" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left" />
                                            <asp:BoundField HeaderText="Percentage" DataField="ClaimClosedReasonPercentage" DataFormatString="{0} %" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" />
                                        </Columns>
                                        <HeaderStyle CssClass="font-white bg-green-steel" />
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblEmptyRecordText" runat="server" CssClass="font-md font-red bold"> No records Found </asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    function pageLoad(sender, args) {
        $(document).ready(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
    }
</script>