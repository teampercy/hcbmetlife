﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB.UserControls
{
    public partial class DashboardClaimClosed : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                BindGrid();
                btnOpenAll.Style.Add("border-color", "#3598dc");
            }
        }

        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlDashboardCaseClosedBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlDashboardCaseClosedBroker.DataTextField = "AssessorTeamName";
                ddlDashboardCaseClosedBroker.DataValueField = "ID";
                ddlDashboardCaseClosedBroker.DataBind();
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardCaseClosedBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardCaseClosedBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlDashboardCaseClosedScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlDashboardCaseClosedScheme.DataTextField = "SchemeName";
                ddlDashboardCaseClosedScheme.DataValueField = "ID";
                ddlDashboardCaseClosedScheme.DataBind();
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardCaseClosedScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardCaseClosedScheme.Items.Insert(0, item);
            }
        }
        #endregion

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            ddlDashboardCaseClosedScheme.SelectedIndex = 0;
            ddlDashboardCaseClosedBroker.SelectedIndex = 0;

            btnOpenAll.Style.Add("border-color", "#3598dc");
            ddlDashboardCaseClosedScheme.Style.Remove("border-color");
            ddlDashboardCaseClosedBroker.Style.Remove("border-color");

            BindGrid();
        }

        protected void ddlDashboardCaseClosedScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardCaseClosedBroker.SelectedIndex = 0;
            ddlDashboardCaseClosedScheme.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardCaseClosedBroker.Style.Remove("border-color");

            BindGrid();
        }

        protected void ddlDashboardCaseClosedBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDashboardCaseClosedScheme.SelectedIndex = 0;
            ddlDashboardCaseClosedBroker.Style.Add("border-color", "#3598dc");
            btnOpenAll.Style.Remove("border-color");
            ddlDashboardCaseClosedScheme.Style.Remove("border-color");

            BindGrid();
        }

        protected void btnCaseClosedDateRange_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlDashboardCaseClosedBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlDashboardCaseClosedScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtCaseClosedFromDate.Text != "" && txtCaseClosedToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtCaseClosedFromDate.Text);
                endDate = Convert.ToDateTime(txtCaseClosedToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsInjuryPercent = objClientInfo.GetDashboardClosedCasePercentage(UserId, usertype, AssessorTeamId, SchemeNameId, startDate, endDate);
            DataTable dt = new DataTable();
            if (dsInjuryPercent != null)
            {
                dt = dsInjuryPercent.Tables["DashboardClosedCasePercentage"];
                gvClosedCasePercent.DataSource = dt;
                gvClosedCasePercent.DataBind();
            }
            else
            {
                gvClosedCasePercent.DataSource = null;
                gvClosedCasePercent.DataBind();
            }
        }
    }
}