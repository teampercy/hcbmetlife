﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB.UserControls
{
    public partial class DashboardClaimantAge : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                CalculateClaimantAverageAge();
                btnOpenAll.Style.Add("border-color", "#3598dc");
            }
        }

        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlDashboardAgeBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlDashboardAgeBroker.DataTextField = "AssessorTeamName";
                ddlDashboardAgeBroker.DataValueField = "ID";
                ddlDashboardAgeBroker.DataBind();
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardAgeBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardAgeBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlDashboardAgeScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlDashboardAgeScheme.DataTextField = "SchemeName";
                ddlDashboardAgeScheme.DataValueField = "ID";
                ddlDashboardAgeScheme.DataBind();
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardAgeScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardAgeScheme.Items.Insert(0, item);
            }
        }
        #endregion

        protected void ddlDashboardAgeScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDashboardAgeBroker.SelectedIndex = 0;
                ddlDashboardAgeScheme.Style.Add("border-color", "#3598dc");
                btnOpenAll.Style.Remove("border-color");
                ddlDashboardAgeBroker.Style.Remove("border-color");
                CalculateClaimantAverageAge();
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlDashboardAgeBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDashboardAgeScheme.SelectedIndex = 0;
                ddlDashboardAgeBroker.Style.Add("border-color", "#3598dc");
                btnOpenAll.Style.Remove("border-color");
                ddlDashboardAgeScheme.Style.Remove("border-color");
                CalculateClaimantAverageAge();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            try
            {
                ddlDashboardAgeScheme.SelectedIndex = 0;
                ddlDashboardAgeBroker.SelectedIndex = 0;

                btnOpenAll.Style.Add("border-color", "#3598dc");
                ddlDashboardAgeScheme.Style.Remove("border-color");
                ddlDashboardAgeBroker.Style.Remove("border-color");

                CalculateClaimantAverageAge();
            }
            catch(Exception ex)
            {

            }
        }

        private void CalculateClaimantAverageAge()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlDashboardAgeBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlDashboardAgeScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtAgeFromDate.Text != "" && txtAgeToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtAgeFromDate.Text);
                endDate = Convert.ToDateTime(txtAgeToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsAverageAge = objClientInfo.GetDashboardClaimantAverageAge(UserId, usertype, AssessorTeamId, SchemeNameId, startDate, endDate);
            if (dsAverageAge.Tables["DashboardClaimantAverageAge"].Rows[0]["AvgDOB"].ToString() != null && dsAverageAge.Tables["DashboardClaimantAverageAge"].Rows[0]["AvgDOB"].ToString() != "")
            {
                lblAvgYearValue.Text = dsAverageAge.Tables["DashboardClaimantAverageAge"].Rows[0]["AvgDOB"].ToString(); //Print the Average Age Value
            }
            else
            {
                lblAvgYearValue.Text = "0";
            }
        }

        protected void btnAgeDateRange_Click(object sender, EventArgs e)
        {
            CalculateClaimantAverageAge();
        }
    }
}