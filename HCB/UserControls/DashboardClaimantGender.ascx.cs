﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB.UserControls
{
    public partial class DashboardClaimantGender : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillDashboardAssessorTeam();
                FillDashboardSchemeList();
                CalculateClaimantGenderPercentage();
                btnOpenAll.Style.Add("border-color", "#3598dc");
            }
        }

        #region Load Broker & Scheme Name List in dropdown
        private void FillDashboardAssessorTeam()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardAssessorList(UserId, usertype);

                ddlDashboardGenderBroker.DataSource = ds.Tables["DashboardBrokerList"];
                ddlDashboardGenderBroker.DataTextField = "AssessorTeamName";
                ddlDashboardGenderBroker.DataValueField = "ID";
                ddlDashboardGenderBroker.DataBind();
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardGenderBroker.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlDashboardGenderBroker.Items.Insert(0, item);
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlDashboardGenderScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlDashboardGenderScheme.DataTextField = "SchemeName";
                ddlDashboardGenderScheme.DataValueField = "ID";
                ddlDashboardGenderScheme.DataBind();
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardGenderScheme.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardGenderScheme.Items.Insert(0, item);
            }
        }
        #endregion

        protected void btnOpenAll_Click(object sender, EventArgs e)
        {
            try
            {
                ddlDashboardGenderBroker.SelectedIndex = 0;
                ddlDashboardGenderScheme.SelectedIndex = 0;
                btnOpenAll.Style.Add("border-color", "#3598dc");
                ddlDashboardGenderScheme.Style.Remove("border-color");
                ddlDashboardGenderBroker.Style.Remove("border-color");

                CalculateClaimantGenderPercentage();
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlDashboardGenderScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDashboardGenderBroker.SelectedIndex = 0;
                ddlDashboardGenderScheme.Style.Add("border-color", "#3598dc");
                btnOpenAll.Style.Remove("border-color");
                ddlDashboardGenderBroker.Style.Remove("border-color");
                CalculateClaimantGenderPercentage();
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlDashboardGenderBroker_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDashboardGenderScheme.SelectedIndex = 0;
                ddlDashboardGenderBroker.Style.Add("border-color", "#3598dc");
                btnOpenAll.Style.Remove("border-color");
                ddlDashboardGenderScheme.Style.Remove("border-color");
                CalculateClaimantGenderPercentage();
            }
            catch (Exception ex)
            {

            }
        }

        private void CalculateClaimantGenderPercentage()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int AssessorTeamId = Convert.ToInt32(ddlDashboardGenderBroker.SelectedValue);
            int SchemeNameId = Convert.ToInt32(ddlDashboardGenderScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtGenderFromDate.Text != "" && txtGenderToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtGenderFromDate.Text);
                endDate = Convert.ToDateTime(txtGenderToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }

            DataSet dsGenderPercent = objClientInfo.GetDashboardClaimantGenderPercentage(UserId, usertype, AssessorTeamId, SchemeNameId,startDate,endDate);

            if (dsGenderPercent != null)
            {
                if (dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["GenderPercentage"].ToString() != null &&
                    dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["GenderPercentage"].ToString() != "" &&
                    dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows.Count > 1)
                {
                    lblFemalePercent.Text = dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["GenderPercentage"].ToString();
                    lblMalePercent.Text = dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[1]["GenderPercentage"].ToString();
                }
                else if (dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows.Count == 1 &&
                    dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["GenderPercentage"].ToString() != null &&
                    dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["GenderPercentage"].ToString() != "")
                {
                    if (dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["ClientGender"].ToString() == "Male")
                    {
                        lblFemalePercent.Text = "0";
                        lblMalePercent.Text = dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["GenderPercentage"].ToString();
                    }
                    else
                    {
                        lblFemalePercent.Text = dsGenderPercent.Tables["DashboardClaimantGenderPercentage"].Rows[0]["GenderPercentage"].ToString();
                        lblMalePercent.Text = "0";
                    }
                }
                else
                {
                    lblFemalePercent.Text = "0";
                    lblMalePercent.Text = "0";
                }
            }
            else
            {
                lblFemalePercent.Text = "0";
                lblMalePercent.Text = "0";
            }            
        }

        protected void btnGenderDateRange_Click(object sender, EventArgs e)
        {
            CalculateClaimantGenderPercentage();
        }
    }
}