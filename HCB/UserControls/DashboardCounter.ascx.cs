﻿using HCBBLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HCB.UserControls
{
    public partial class DashboardCounter : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //int userId = (int)Session["LoggedInuserId"];
                //string usertype = Convert.ToString(Session["UserType"]);
                //ClientInfo ClientInfo = new ClientInfo();
                //lblOpenCaseCounter.Attributes.Add("data-value", ClientInfo.GetTotalClaimCounter(userId, usertype, true).ToString());
                //lblCloseCaseCounter.Attributes.Add("data-value", ClientInfo.GetTotalClaimCounter(userId, usertype, false).ToString());
                //lblAverageClaimDurationCounter.Attributes.Add("data-value", ClientInfo.GetAverageClaimDuration(userId, usertype).ToString());
                //lblEmployersCounter.Attributes.Add("data-value", ClientInfo.GetEmployerCount(userId, usertype).ToString());
                DashboardCounterValue();
            }
        }

        #region Methods
        private void DashboardCounterValue()
        {
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = Convert.ToString(Session["UserType"]);
            ClientInfo objClientInfo = new ClientInfo();
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            //Get Open Cases Count for related User Login
            DataSet dsOpenCases = objClientInfo.GetDashboradAssessorCasesList(UserId, usertype, true, 0, startDate, endDate);
            if (dsOpenCases != null)
            {
                lblOpenCaseCounter.Text = dsOpenCases.Tables["DashboardBrokerCasesList"].Rows.Count.ToString();
            }
            else
            {
                lblOpenCaseCounter.Text = "0";                
            }

            //Get Closed Cases Count for related User Login
            DataSet dsClosedCases = objClientInfo.GetDashboradAssessorCasesList(UserId, usertype, false, 0, startDate, endDate);
            if (dsClosedCases != null)
            {
                lblCloseCaseCounter.Text = dsClosedCases.Tables["DashboardBrokerCasesList"].Rows.Count.ToString();
            }
            else
            {
                lblCloseCaseCounter.Text = "0";
            }

            //Get Total Count for Distinct Employer Name for related User Login
            if(dsOpenCases!=null & dsClosedCases!=null)
            {
                dsOpenCases.Merge(dsClosedCases);
                int EmployerCounter = dsOpenCases.Tables[0].AsEnumerable().Where(dr => !string.IsNullOrEmpty(Convert.ToString(dr["EmployerName"]))).Select(dr => dr["EmployerName"]).Distinct().Count();
                lblEmployersCounter.Text = EmployerCounter.ToString();
            }
            else if(dsOpenCases!=null)
            {
                int EmployerCounter = dsOpenCases.Tables[0].AsEnumerable().Where(dr => !string.IsNullOrEmpty(Convert.ToString(dr["EmployerName"]))).Select(dr => dr["EmployerName"]).Distinct().Count();
                lblEmployersCounter.Text = EmployerCounter.ToString();
            }
            else if(dsClosedCases!=null)
            {
                int EmployerCounter = dsClosedCases.Tables[0].AsEnumerable().Where(dr => !string.IsNullOrEmpty(Convert.ToString(dr["EmployerName"]))).Select(dr => dr["EmployerName"]).Distinct().Count();
                lblEmployersCounter.Text = EmployerCounter.ToString();
            }
            else
            {
                lblEmployersCounter.Text = "0";
            }

            //Get Average Claim Duration for related User Login
            //DataSet dsAverageClaimDuration = objClientInfo.GetDashboardAverageClaimDuration(UserId, usertype, 0, 0, startDate, endDate);
            //if (dsAverageClaimDuration.Tables["DashboardAverageClaimDuration"].Rows[0]["AvgClaimDuration"].ToString() != null &&
            //    dsAverageClaimDuration.Tables["DashboardAverageClaimDuration"].Rows[0]["AvgClaimDuration"].ToString() != "")
            //{
            //    lblAverageClaimDurationCounter.Text = dsAverageClaimDuration.Tables["DashboardAverageClaimDuration"].Rows[0]["AvgClaimDuration"].ToString();
            //}
            //else
            //{
            //    lblAverageClaimDurationCounter.Text = "0";
            //}

            //Get Average RTW Duration for related User Login
            DataSet dsAverageRTWDuration = objClientInfo.GetDashboardAverageRTWDuration(UserId, usertype, 0, 0, startDate, endDate);
            if (dsAverageRTWDuration.Tables["DashboardAverageRTWDuration"].Rows[0]["AvgRTWDuration"].ToString() != null &&
                dsAverageRTWDuration.Tables["DashboardAverageRTWDuration"].Rows[0]["AvgRTWDuration"].ToString() != "")
            {
                lblAverageClaimDurationCounter.Text = dsAverageRTWDuration.Tables["DashboardAverageRTWDuration"].Rows[0]["AvgRTWDuration"].ToString();
            }
            else
            {
                lblAverageClaimDurationCounter.Text = "0";
            }

        }

        #endregion
    }
}