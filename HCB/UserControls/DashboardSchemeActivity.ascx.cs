﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;

namespace HCB.UserControls
{
    public partial class DashboardSchemeActivity : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                FillDashboardSchemeList();
                LoadSchemeCases();
            }
        }

        private void FillDashboardSchemeList()
        {
            try
            {
                ClientInfo objClientInfo = new ClientInfo();
                int UserId = (int)Session["LoggedInuserId"];
                string usertype = "";
                if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                {
                    usertype = Session["UserType"].ToString();
                }

                DataSet ds = objClientInfo.GetDashboardSchemeList(UserId, usertype);

                ddlDashboardScheme.DataSource = ds.Tables["DashboardSchemeList"];
                ddlDashboardScheme.DataTextField = "SchemeName";
                ddlDashboardScheme.DataValueField = "ID";
                ddlDashboardScheme.DataBind();
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardScheme.Items.Insert(0, item);
            }
            catch(Exception ex)
            {
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlDashboardScheme.Items.Insert(0, item);
            }
        }

        protected void ddlDashboardScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadSchemeCases();
            }
            catch(Exception ex)
            {

            }
        }

        private void LoadSchemeCases()
        {
            ClientInfo objClientInfo = new ClientInfo();
            int UserId = (int)Session["LoggedInuserId"];
            string usertype = "";
            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
            {
                usertype = Session["UserType"].ToString();
            }
            int SchemeNameId = Convert.ToInt32(ddlDashboardScheme.SelectedValue);

            DateTime startDate, endDate;
            if (txtSchemeFromDate.Text != "" && txtSchemeToDate.Text != "")
            {
                startDate = Convert.ToDateTime(txtSchemeFromDate.Text);
                endDate = Convert.ToDateTime(txtSchemeToDate.Text);
            }
            else
            {
                startDate = DateTime.MinValue;
                endDate = DateTime.MinValue;
            }
            
            DataSet dsOpenCases = objClientInfo.GetDashboradSchemeCasesList(UserId, usertype, true, SchemeNameId, startDate, endDate);
            if (dsOpenCases == null)
            {
                btnOpen.Text = "Open Cases (0)";
            }
            else
            {
                btnOpen.Text = "Open Cases (" + dsOpenCases.Tables["DashboardSchemeCasesList"].Rows.Count + ")";
            }

            DataSet dsClosedCases = objClientInfo.GetDashboradSchemeCasesList(UserId, usertype, false, SchemeNameId, startDate, endDate);
            if (dsClosedCases == null)
            {
                btnClose.Text = "Closed Cases (0)";
            }
            else
            {
                btnClose.Text = "Closed Cases (" + dsClosedCases.Tables["DashboardSchemeCasesList"].Rows.Count + ")";
            }

            //Stored Dataset value in viewstate for using on Button click event
            ViewState["vwOpenCases"] = dsOpenCases;
            ViewState["vwClosedCases"] = dsClosedCases;
        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "HCBRef")
            {
                if (Session["NewHCBRef"] != null)
                {
                    if (Session["NewHCBRef"].ToString() != "")
                    {
                        Session["NewHCBRef"] = null;
                    }
                }
                Session["ClientHcbRef"] = e.CommandArgument;
                if (Session["ds"] != null) Session["ds"] = null;
                Response.Redirect("~/ClientRecord.aspx");
            }
        }

        protected void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                lblPopupHeader.Text = "Open Cases";
                DataSet dsOpenCases = (DataSet)ViewState["vwOpenCases"];
                BindGrid(dsOpenCases);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenCases", "DashboardPopup('#divSchemeCases');", true);                
            }
            catch(Exception ex)
            {

            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                lblPopupHeader.Text = "Closed Cases";
                DataSet dsClosedCases = (DataSet)ViewState["vwClosedCases"];
                BindGrid(dsClosedCases);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosedCases", "DashboardPopup('#divSchemeCases');", true);                
            }
            catch (Exception ex)
            {

            }
        }

        private void BindGrid(DataSet ds)
        {
            DataTable dt = new DataTable();
            if (ds != null)
            {
                dt = ds.Tables["DashboardSchemeCasesList"];
                try
                {
                    rptResults.DataSource = dt;
                    rptResults.DataBind();
                }
                catch (Exception ex)
                {
                    rptResults.DataSource = ds;
                    rptResults.DataBind();
                }
            }
            else
            {
                rptResults.DataSource = null;
                rptResults.DataBind();
            }
        }

        protected void btnSchemeDateRange_Click(object sender, EventArgs e)
        {
            LoadSchemeCases();
        }
    }
}