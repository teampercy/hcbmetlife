﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeedsCalendar.ascx.cs" Inherits="HCB.UserControls.FeedsCalendar" %>
<div class="portlet light portlet-fit  calendar">
    <div class="portlet-title">
        <div class="caption">            
            <span class="caption-subject font-dark bold uppercase">Feeds</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-sm-12">
                <div id="calendar" class="has-toolbar"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#calendar').fullCalendar({            
            fixedWeekCount: false,
            header: {
                right: 'prev,next,today,month,basicWeek,basicDay'
            }
        });       

    });
</script>
