﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralStats.ascx.cs" Inherits="HCB.UserControls.GeneralStats" %>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-cursor font-dark hide"></i>
            <span class="caption-subject font-dark bold uppercase">General Stats</span>
        </div>
        <div class="actions">
            <a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                <i class="fa fa-repeat"></i>Reload </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number transactions" data-percent="55">
                        <span>+55</span>%
                    </div>
                    <a class="title" href="javascript:;">Transactions
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="margin-bottom-10 visible-sm"></div>
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number visits" data-percent="85">
                        <span>+85</span>%
                    </div>
                    <a class="title" href="javascript:;">New Visits
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="margin-bottom-10 visible-sm"></div>
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number bounce" data-percent="46">
                        <span>-46</span>%
                    </div>
                    <a class="title" href="javascript:;">Bounce
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number transactions" data-percent="55">
                        <span>+55</span>%
                    </div>
                    <a class="title" href="javascript:;">Transactions
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="margin-bottom-10 visible-sm"></div>
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number visits" data-percent="85">
                        <span>+85</span>%
                    </div>
                    <a class="title" href="javascript:;">New Visits
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="margin-bottom-10 visible-sm"></div>
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number bounce" data-percent="46">
                        <span>-46</span>%
                    </div>
                    <a class="title" href="javascript:;">Bounce
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number transactions" data-percent="55">
                        <span>+55</span>%
                    </div>
                    <a class="title" href="javascript:;">Transactions
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="margin-bottom-10 visible-sm"></div>
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number visits" data-percent="85">
                        <span>+85</span>%
                    </div>
                    <a class="title" href="javascript:;">New Visits
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="margin-bottom-10 visible-sm"></div>
            <div class="col-md-4">
                <div class="easy-pie-chart">
                    <div class="number bounce" data-percent="46">
                        <span>-46</span>%
                    </div>
                    <a class="title" href="javascript:;">Bounce
                                                                       
                                    <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $(function () {

        $('.easy-pie-chart .number.transactions').easyPieChart({
            animate: 1000,
            size: 75,
            lineWidth: 3,
            barColor: App.getBrandColor('yellow')
        });

        $('.easy-pie-chart .number.visits').easyPieChart({
            animate: 1000,
            size: 75,
            lineWidth: 3,
            barColor: App.getBrandColor('green')
        });

        $('.easy-pie-chart .number.bounce').easyPieChart({
            animate: 1000,
            size: 75,
            lineWidth: 3,
            barColor: App.getBrandColor('red')
        });

        $('.easy-pie-chart-reload').click(function () {
            $('.easy-pie-chart .number').each(function () {
                var newValue = Math.floor(100 * Math.random());
                $(this).data('easyPieChart').update(newValue);
                $('span', this).text(newValue);
            });
        });

    });
</script>
