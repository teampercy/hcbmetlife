﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InitialVisitChart.ascx.cs" Inherits="HCB.UserControls.InitialVisitChart" %>

<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption ">
            <span class="caption-subject font-dark bold uppercase">Initial Visit</span>
            <span class="caption-helper"></span>
        </div>
    </div>
    <div class="portlet-body">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Date Range</label>
                                    <div class="col-md-6">
                                        <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                            <asp:TextBox runat="server" class="form-control" name="from" ID="txtVisitFromDate"></asp:TextBox>
                                            <span class="input-group-addon">to </span>
                                            <asp:TextBox runat="server" class="form-control" name="to" ID="txtVisitToDate"></asp:TextBox>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">Select date range </span>
                                    </div>
                                    <div class="col-md-1" style="padding-left: 5px;">
                                        <button id="btnVisitDateRange" class="btn green" onclick="DrawInitialVisitChart();">
                                            <i class="fa fa-check-circle"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="InitialVisitChart" style="width: 100%; height: 300px">
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.date-picker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
    });

    function DrawInitialVisitChart() {
        
        var FromDate, ToDate, chartTitle;
        var content = 'MainContent_InitialVisitChart_';

        if ($('#' + content + 'txtVisitFromDate').val() != "" &&
            $('#' + content + 'txtVisitToDate').val() != "") {
            var ConvertFromDate = $('#' + content + 'txtVisitFromDate').val();
            var FromDateArray = ConvertFromDate.split("/");
            FromDate = FromDateArray[1] + '/' + FromDateArray[0] + '/' + FromDateArray[2];

            var ConvertToDate = $('#' + content + 'txtVisitToDate').val();
            var ToDateArray = ConvertToDate.split("/");
            ToDate = ToDateArray[1] + '/' + ToDateArray[0] + '/' + ToDateArray[2];
            chartTitle = 'First Contact to Site/Home Visit - \n' + ConvertFromDate + ' To ' + ConvertToDate;
        }
        else {
            FromDate = new Date(new Date().getFullYear(), 0, 1);
            ToDate = new Date(new Date().getFullYear(), 11, 31);;
            chartTitle = 'First Contact to Site/Home Visit  - Year ' + new Date().getFullYear();
        }
        $.ajax({
            url: "Default.aspx/GetInitialChartData",
            type: "POST",
            data: JSON.stringify({ 'ChartType': 2, 'FromDate': FromDate, 'ToDate': ToDate }),
            contentType: "application/json;charset=utf-8",
            success: function (res) {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Month-Year');
                //data.addColumn('number', 'Duration');
                data.addColumn('number', 'SLA Timescale');
                data.addColumn('number', 'Average');
                //DurationLegendData = res.d.find(obj => obj.Legend==='Duration').ChartData;
                //SLALegendData = res.d.find(obj => obj.Legend === 'SLA').ChartData;
                SLALegendData = res.d[0].ChartData;
                //AverageLegendData = res.d.find(obj => obj.Legend === 'Average').ChartData;
                AverageLegendData = res.d[1].ChartData;

                for (var month = 1; month <= AverageLegendData.length; month++) {

                    //data.addRow([moment.months(month-1), DurationLegendData[month - 1].YaxisValue, SLALegendData[month - 1].YaxisValue, AverageLegendData[month - 1].YaxisValue]);
                    data.addRow([AverageLegendData[month - 1].XaxisValue, SLALegendData[month - 1].YaxisValue, AverageLegendData[month - 1].YaxisValue]);
                }

                var options = {
                    title:chartTitle,
                    //title: 'First Contact to Site/Home Visit  - Year ' + new Date().getFullYear(),
                    legend: { position: 'bottom' },
                    vAxis: { minValue: 0}
                };

                var chart = new google.visualization.LineChart($('#InitialVisitChart')[0]);
                chart.draw(data, options);

            },
            fail: function (response) {
                alert("Unable to display initial visit chart");
            }
        });
    }
</script>
