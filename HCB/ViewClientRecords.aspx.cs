﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using System.Text;
using Microsoft.Security.Application;
namespace HCB
{
    public partial class ViewClientRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString != null)
                {
                    if (Request.QueryString["mode"] != null)
                    {
                        if (Session["LoggedInUserId"] != null && Session["LoggedInuserId"].ToString() != "")
                        {
                            ClientInfo cltinfo = new ClientInfo();
                            DataSet ds = new DataSet();
                            int userId = (int)Session["LoggedInuserId"];
                            string usertype = "";
                            if (Session["UserType"] != null && Session["UserType"].ToString() != "")
                            {
                                usertype = Session["UserType"].ToString();
                            }
                            if (Request.QueryString["mode"].ToString() == "open")
                            {
                                lblTitle.Text =AntiXss.HtmlEncode("Client Records (Open)");
                                ds = cltinfo.GetClientList(userId, usertype, true);
                                //lblheadTitle.Text = AntiXss.HtmlEncode("OPEN CASES");                                
                            }
                            else if (Request.QueryString["mode"].ToString() == "closed")
                            {
                                lblTitle.Text = AntiXss.HtmlEncode("Client Records (Closed)");
                                ds = cltinfo.GetClientList(userId, usertype, false);
                                //lblheadTitle.Text = AntiXss.HtmlEncode("CLOSED CASES");                                
                            }
                            if (Session["ds"] != null)
                                Session["ds"] = ds;
                            else
                                Session.Add("ds", ds);

                            BindGrid(ds);
                        }
                    }
                }
            }
        }

        private void BindGrid(DataSet ds)
        {

            DataTable dt = new DataTable();
            if (ds != null)
            {
                dt = ds.Tables["Client"];
                #region code for dynamic report genration
                //StringBuilder sb =  new StringBuilder();
                //sb.Append("<table cellpadding=\"0\" border=\"2\" cellspacing=\"0\" class=\"display\" rel=\"datatable\" id=\"example\">");
                //sb.Append("<thead> <tr style=\"background-color: #2459A7 !important; color: white;\">");
                //foreach(DataColumn dc in dt.Columns)
                //{
                //    sb.Append(" <th>"+ dc.ColumnName +"</th> ");
                //}
                //sb.Append("</tr></thead>");
                //sb.Append("<tbody>");
                //sb.Append(" <a name=\"plugin\"></a>");
                //foreach(DataRow dr in dt.Rows)
                //{
                //    sb.Append(" <tr>");

                //    for(int i = 0 ;  i< dr.Table.Columns.Count ; i++)
                //    {
                //       sb.Append(" <td>" + dr[i].ToString() + "</td> ");
                //    }
                //    sb.Append("</tr> ");

                //}
                //sb.Append("</tbody>");

                //wassup.InnerHtml = sb.ToString(); 
                #endregion
                try
                {
                    rptResults.DataSource = dt;
                    rptResults.DataBind();
                }
                catch (Exception ex)
                {
                    rptResults.DataSource = ds;
                    rptResults.DataBind();
                }
            }
        }

        //private void BindClosedGrid()
        //{
        //    ClientInfo cltinfo = new ClientInfo();
        //    DataSet ds = new DataSet();



        //    if (ds != null)
        //    {
        //        DataTable dt = ds.Tables["ClientClosed"];

        //        DataColumn dc = new DataColumn();
        //        dc.ColumnName = "RefNo";
        //        dc.DataType = System.Type.GetType("System.String");
        //        dt.Columns.Add(dc);
        //        string RefNo = "";

        //        //dt.Columns.Add("RefNo", typeof(string));

        //        try
        //        {
        //            foreach (DataRow dr in dt.Rows)
        //            {

        //                if (dr["HCBReference"].ToString().Length >= 5)
        //                {
        //                    RefNo = "DCP" + dr["HCBReference"].ToString();
        //                }
        //                else if (dr["HCBReference"].ToString().Length == 4)
        //                {
        //                    RefNo = "DCP0" + dr["HCBReference"].ToString();
        //                }
        //                else if (dr["HCBReference"].ToString().Length == 3)
        //                {
        //                    RefNo = "DCP00" + dr["HCBReference"].ToString();
        //                }
        //                else if (dr["HCBReference"].ToString().Length == 2)
        //                {
        //                    RefNo = "DCP000" + dr["HCBReference"].ToString();
        //                }
        //                else if (dr["HCBReference"].ToString().Length == 1)
        //                {
        //                    RefNo = "DCP0000" + dr["HCBReference"].ToString();
        //                }

        //                if (RefNo != "")
        //                {
        //                    //dt.Columns["RefNo"].DefaultValue = RefNo;
        //                    dr["RefNo"] = RefNo;
        //                    dt.AcceptChanges();
        //                }
        //            }
        //            gvResults.DataSource = dt; //ds.Tables["ClientClosed"];
        //            gvResults.DataBind();
        //        }
        //        catch (Exception ex)
        //        {
        //            gvResults.DataSource = ds;
        //            gvResults.DataBind();
        //        }
        //    }
        //}

        #region Converted
        //protected void gvResults_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "HCBRef")
        //    {
        //        Session["ClientHcbRef"] = e.CommandArgument;
        //        if (Session["ds"] != null) Session["ds"] = null;
        //        Response.Redirect("~/ClientRecord.aspx");
        //    }
        //}
        //protected void gvResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gvResults.PageIndex = e.NewPageIndex;
        //    DataSet dsList = new DataSet();
        //    if (Session["ds"] != null) dsList = (DataSet)Session["ds"];
        //    else Response.Redirect(Request.UrlReferrer.PathAndQuery, false);
        //    BindGrid(dsList);
        //}

        #endregion

        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "HCBRef")
            {
                if (Session["NewHCBRef"] != null)
                {
                    if (Session["NewHCBRef"].ToString() != "")
                    {
                        Session["NewHCBRef"] = null;
                    }
                }
                Session["ClientHcbRef"] = e.CommandArgument;
                if (Session["ds"] != null) Session["ds"] = null;
                Response.Redirect("~/ClientRecord.aspx");
            }
        }
       
    }
}