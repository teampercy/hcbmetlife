﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using System.Text;

namespace HCB
{
    public partial class audittrail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            try
                {
                if (Session["UserType"] != null && (Session["UserType"].ToString() == "Admin") || (Session["UserType"].ToString() == "Inco"))
                {
                    MasterListBL MSL = new MasterListBL();
                    DataSet dsLIst = MasterListBL.GetAmendment(Convert.ToInt32(Session["LoggedInUserId"]),Session["UserType"].ToString());
                    //if (Session["ds"] != null) Session["ds"] = dsLIst;
                    //else Session.Add("ds", dsLIst);
                    rptQuotes.DataSource = dsLIst;
                    rptQuotes.DataBind();
                }
                else
                {

                    //ClientScript.RegisterStartupScript(typeof(Page), "MessagePopUp",
                    //"alert('ABCDEFGH'); window.location.href = 'Default.aspx';", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Permission",
                    //    "alert('you dont have rights to view this page.');window.location.href ='~/Default.aspx';",true);
                    //Response.Redirect("~/Accessdenied.aspx", false);
                    Server.Transfer("~/Accessdenied.aspx");
                }

                }
            catch (Exception ex)
                {
                Response.Redirect("~/login.aspx");
                
                }

            }
        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            Session["ClientHcbRef"] = e.CommandArgument;
            Response.Redirect("~/ClientRecord.aspx", false);
        }

    }
}