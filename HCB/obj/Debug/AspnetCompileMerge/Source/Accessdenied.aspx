﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Accessdenied.aspx.cs" Inherits="HCB.Accessdenied" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div style="text-align: center; width:80%">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/LadyImage.jpg" vspace="15" style="border: 1px lightgrey solid;" />
                </td>
                <td class="MainText" style="width: 535px;" rowspan="3" valign="top">
                    <br />
                    <br />
                    <span style="font-size: 12pt"><strong> 
                        You don't have enough rights to access this page.</strong></span><br />
                    <br />
                  
                </td>
            </tr>
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/MeetingImage.jpg" style="border: 1px lightgrey solid;" />
                </td>
            </tr>
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/HandshakeImage.jpg" style="border: 1px lightgrey solid;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
