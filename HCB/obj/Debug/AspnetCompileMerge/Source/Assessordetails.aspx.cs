﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using Microsoft.Security.Application;
namespace HCB
{
    public partial class Assessordetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AssessorID"] != null)
            {
                int IAssessorID = (int)Session["AssessorID"];

                if (IAssessorID != 0)
                //if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    //string Type = Request.QueryString["type"].ToString().Trim();
                        FillReasonList();                   
                }
                else
                {
                    Response.Redirect("PageNotFound", false);
                }
            }
            else
            {
                Response.Redirect("Administration.aspx", false);
            }
        }

        private void FillReasonList()
        {
            if (Session["AssessorID"] != null)
            {
                int IAssessorID = (Int16)Session["AssessorID"];
                MasterListBL ML = new MasterListBL();
                DataSet ds = ML.GetAssessorList();
                gvReasonList.DataSource = ds.Tables[0];
                gvReasonList.DataBind();
            }
        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            lblTitle.Text = AntiXss.HtmlEncode(lblTitle.Text + "__" + e.CommandName.Trim().ToLower());
            string[] Val = e.CommandArgument.ToString().Split(',');
            if (e.CommandName.Trim().ToLower() == "edt")
            {
                if (Session["ListId"] != null)
                {
                    Session["ListId"] = Val[0];
                }
                else Session.Add("LIstID", Val[0].ToString());

              //  txtEmail.Text = Val[1].ToString();
                hdntype.Value = "edit";
                ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "edit", "loadPopup('#Popup')", true);
            }
            else if (e.CommandName.Trim().ToLower() == "del")
            {
                DeleteList(Convert.ToInt32(e.CommandArgument.ToString()));
                //ScriptManager.RegisterStartupScript(gvReasonList,this.GetType,"Del",
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "", "alert('Hi_" + hdntype.Value + "_" + (string)Session["ListId"] + "')", true);
            if (hdntype.Value == "edit")
            {
                //updateAmendment();
                if (Session["ListId"] != null)
                {
                    string IlistId = (string)Session["ListId"];
                    Int16 IType = 0;
                    if (Session["Type"] != null)
                    {
                        IType = (Int16)Session["Type"];
                        MasterListBL ML = new MasterListBL();
                        //ML.UpdateMasterList(Convert.ToInt32(IlistId), txtName.Text, IType);
                        Session["ListId"] = null;
                        hdntype.Value = "new";
                        FillReasonList();
                        ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "edit", "closepopup()", true);

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "Close", "closepopup()", true);
                }
            }
            else
            {
                string IlistId = (string)Session["ListId"];
                Int16 IType = 0;
                if (Session["Type"] != null)
                {
                    IType = (Int16)Session["Type"];
                    MasterListBL ML = new MasterListBL();
                    //ML.InsertMasterList(txtName.Text, IType);
                    FillReasonList();
                    ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "Insert", "closepopup()", true);
                }
            }

        }

        public void DeleteList(int id)
        {
            MasterListBL ML = new MasterListBL();
            ML.DeleteMasterList(id);
            FillReasonList();
            ScriptManager.RegisterStartupScript(gvReasonList, this.GetType(), "del", "closepopup()", true);
        }

        protected void gvReasonList_RowCreated1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                {
                    if (e.Row.FindControl("lnkeDelete") != null)
                    {
                        ImageButton imgbtn = (ImageButton)e.Row.FindControl("lnkeDelete");
                        imgbtn.OnClientClick = "confirm(' You want to delete" + DataBinder.Eval(e.Row.DataItem, "Name") + " ?');";
                    }
                }
            }
        }

    }
}