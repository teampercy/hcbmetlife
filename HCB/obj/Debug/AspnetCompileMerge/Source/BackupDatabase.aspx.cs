﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using Microsoft.Security.Application;
namespace HCB
{
    public partial class BackupDatabase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
            {
            }
            else
            {

                //ClientScript.RegisterStartupScript(typeof(Page), "MessagePopUp",
                //"alert('ABCDEFGH'); window.location.href = 'Default.aspx';", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Permission",
                //    "alert('you dont have rights to view this page.');window.location.href ='~/Default.aspx';",true);
                //Response.Redirect("~/Accessdenied.aspx", false);
                Server.Transfer("~/Accessdenied.aspx");
            }
        }

        protected void btn_backup(object sender, EventArgs e)
        {
            Modifydatabse("backup");
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Modifydatabse("download");
        }


        public void Modifydatabse(string task)
        {
            try
            {
                string strBackupPath = ConfigurationManager.AppSettings["BackupPath"];
                string strConn = ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString();
                int startIndex = 0, endIndex = 0;
                if (strConn.ToLower().IndexOf("jet.oledb") > -1)
                {
                    startIndex = strConn.ToLower().IndexOf("data source=");
                    if (startIndex > -1) startIndex += 12;
                }
                else
                {
                    // if this is a SQL Server database, find the index of the "initial catalog" or "database" setting
                    startIndex = strConn.IndexOf("initial catalog=");
                    if (startIndex > -1)
                        startIndex += 16;
                }
                if (startIndex > 0)
                {
                    endIndex = strConn.IndexOf(";", startIndex);
                    strConn = strConn.Substring(startIndex + 1, endIndex - (startIndex + 2));
                }

                if (task == "backup")
                {
                    if (File.Exists(strConn))
                    {
                        if (!Directory.Exists(strBackupPath + "//" + DateTime.Now.ToString("dd-MM-yyyy")))
                        {
                            Directory.CreateDirectory(strBackupPath + "//" + DateTime.Now.ToString("dd-MM-yyyy"));
                        }
                        string strfilename = strConn.Substring((strConn.LastIndexOf('\\') + 1), (strConn.Length - (strConn.IndexOf(".mdb") + 1))) + "_" + DateTime.Now.ToString("ddMMyy") + "_" + DateTime.Now.ToString("HHMMsssfff") + ".mdb";
                        File.Copy(strConn, strBackupPath + "//" + DateTime.Now.ToString("dd-MM-yyyy") + "//" + strfilename);
                        statusLabel.Text = AntiXss.HtmlEncode("Database backup succecssfully completed");  //at '" + strBackupPath + "/" + DateTime.Now.ToString("ddMMyyyy") + "/" + strfilename + "'";
                    }
                }
                else if (task == "download")
                {
                    Response.Clear();
                    string strfilename = strConn.Substring((strConn.LastIndexOf('\\') + 1), (strConn.Length - (strConn.IndexOf(".mdb") + 1))) + ".mdb";

                    FileInfo fi = new FileInfo(strConn);
                    Response.ContentType = "application/octet-stream";
                    //Response.ContentType="application/pdf" or else
                    Response.AddHeader("Content-Disposition", "attachement;filename=" + strfilename);
                    Response.CacheControl = "no-cache";
                    Response.AddHeader("Pragma", "no-cache");
                    Response.TransmitFile(strConn);
                    Response.Flush();
                    Response.Close();
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message.Replace("'","");
                ClientScript.RegisterStartupScript(this.GetType(), "DB", "alert('" + errMsg + "');",true);
            }
        }

    }

}