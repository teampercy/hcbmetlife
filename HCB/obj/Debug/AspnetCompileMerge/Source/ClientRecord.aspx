﻿<%@ Page Title="Client Record" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ClientRecord.aspx.cs" Inherits="HCB.ClientRecord" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/Validation.js" language="javascript" type="text/javascript"></script>

    <link rel="stylesheet" href="Styles/bootstrap-3.1.1.min.css" type="text/css" />
    <link rel="stylesheet" href="Styles/bootstrap-multiselect.css" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="js/bootstrap-2.3.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

    <%--    <link href="Styles/GridView.css" rel="stylesheet" />
    <link href="Styles/Site.css" rel="stylesheet" />
    <link href="Styles/Styles.css" rel="stylesheet" />--%>
    <style type="text/css">
        .CheckboxTextSpace label
        {
            padding-left:5px;

        }
        #backgroundPopup1 {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6*/
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: #000000;
            border: 1px solid #cecece;
            z-index: 4;
        }

        .popupContact {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6 height: 200px;*/
            width: 700px;
            background: #FFFFFF;
            border: 2px solid #cecece;
            z-index: 5;
            font-size: 13px;
        }

            .popupContact tr {
                height: 40px;
                text-align: right;
                vertical-align: text-top;
            }

        .popupContactClose {
            font-size: 14px;
            line-height: 14px;
            right: 6px;
            top: 4px;
            position: absolute;
            color: #6fa5fd;
            font-weight: 700;
            display: block;
        }

        #overlay {
            position: fixed;
            z-index: 9;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=80);
            opacity: 0.80;
            -moz-opacity: 0.80;
        }
    </style>
    <%--        <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        function SetSchemeNumber() {
            var SchemeName = document.getElementById('<%=ddlSchemeName.ClientID%>');
            if (SchemeName.value != "0") {
                //alert(locationid );
                $.ajax({
                    type: "POST",
                    url: "ClientRecord.aspx/SetSchemeNumber",
                    data: '{ID: "' + SchemeName.value + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != "") {
                            document.getElementById('<%=txtSchemeNumber.ClientID%>').value = response.d;
                        }
                        else {
                            document.getElementById('<%=txtSchemeNumber.ClientID%>').value = "";
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            }
            else
            {
                document.getElementById('<%=txtSchemeNumber.ClientID%>').value = "";
            }
            return false;
        }
        $(document).ready(function () {



            $('#<%=btnUpload.ClientID%>').click(function () {

                document.getElementById('<%=hdnServiceRequired.ClientID%>').value = $('#chkSer').val();

            });

            $('#<%=btnRemoveDoc.ClientID%>').click(function () {

                document.getElementById('<%=hdnServiceRequired.ClientID%>').value = $('#chkSer').val();

            });

            var brokervalue = $('#<%=ddlAssessorTeam.ClientID %>').val();
            var deferredvalue = $('#<%=ddlWaitingPeriod.ClientID %>').val();
            var injuryvalue = $('#<%=ddlInjury.ClientID %>').val();
            var reasonvalue = $('#<%=ddlClaimClosedReason.ClientID %>').val();
            if (brokervalue == "14") {
                $('#trteamassessor').fadeIn("slow");
            }
            else {
                $('#trteamassessor').fadeOut("slow");
            }

            if (deferredvalue == "11600") {

                $('#trDeferredPeriod').fadeIn("slow");
            }
            else {

                $('#trDeferredPeriod').fadeOut("slow");
            }

            if (injuryvalue == "12588") {

                $('#trillnessinjury').fadeIn("slow");
            }
            else {

                $('#trillnessinjury').fadeOut("slow");
            }

            if (reasonvalue == "12586") {

                $('#trclaimreason').fadeIn("slow");
            }
            else {

                $('#trclaimreason').fadeOut("slow");
            }
            var roleID = '<%= Session["UserType"]%>'
            if (roleID == "Inco") {
                var x = document.getElementById("chkSer");
                x.disabled = false;
            }
            else {
                var x = document.getElementById("chkSer");
                x.disabled = true;
            }
            $('#chkSer').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 0
            });
            // $("#multiselect").multiselect('dataprovider', dataService);
            $('#btnget').click(function () {
                alert($('#chkSer').val());
            });
            debugger;
            $.ajax({
                type: "POST",
                url: "ClientRecord.aspx/BindDatatoDropdownService1",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function OnPopulateControl(response) {
                    debugger;
                    list = response.d;
                    if (list.length > 0) {
                        //$select.multiselect('enable');
                        //$("#province").empty().append('<option value="0">Please select</option>');
                        $.each(list, function () {
                            $("#chkSer").append($("<option ></option>").val(this['Value']).html(this['Text']));
                        });
                        var arrayser = new Array();
                        debugger;

                        var servicevalue = $('#<%=hdnpostbackservicerequired.ClientID%>').val();
                        if (servicevalue != "") {
                            arrayser = document.getElementById('<%=hdnpostbackservicerequired.ClientID%>').value.split(",");
                        }
                        else {
                            arrayser = document.getElementById('<%=hdnServiceRequired.ClientID%>').value.split(",");
                        }
                        i = 0;
                        var dataservicerequired = new Array();
                        for (i; i < arrayser.length; i++) {
                            //selectedarray.push(''+arrayser[i]+'');
                            //dataservicerequired.push("5");
                            //dataservicerequired.push("6");
                            dataservicerequired.push('' + arrayser[i] + '');
                            if (arrayser[i] == "11595") {
                                $('#trservicerequired').fadeIn("slow");
                            }
                            else {
                                $('#trservicerequired').fadeOut("slow");
                            }
                            debugger;
                            //dataservicerequired.push(''+6+'');
                        }
                        $("#chkSer").val(dataservicerequired);
                        //debugger;

                        //showservicerequired();

                    }
                    else {
                        $("#chkSer").empty().append('<option selected="selected" value="0">Not available<option>');
                    }
                    $("#chkSer").multiselect('rebuild'); //refresh the select here
                },
                error: function () {
                    alert("Error");
                }
            });
            // debugger;
            // showservicerequired();

        });
    </script>
    <script type="text/javascript">



        function showcommonoption(data) {

            debugger;
            var value1 = $(data).val();
            var ojectname = $(data).attr('ID');
            ojectname = ojectname.substring(12);
            if (ojectname == "ddlAssessorTeam") {



                if (value1 != null) {
                    if (value1 == "14") {

                        $('#trteamassessor').fadeIn("slow");
                    }
                    else {
                        $('#trteamassessor').fadeOut("slow");
                    }

                }
            }
            if (ojectname == "ddlWaitingPeriod") {

                if (value1 != null) {
                    if (value1 == "11600") {

                        $('#trDeferredPeriod').fadeIn("slow");
                    }
                    else {
                        $('#trDeferredPeriod').fadeOut("slow");
                    }

                }

            }
            if (ojectname == "ddlInjury") {


                if (value1 != null) {
                    if (value1 == "12588") {

                        $('#trillnessinjury').fadeIn("slow");
                    }
                    else {
                        $('#trillnessinjury').fadeOut("slow");
                    }

                }
            }
            if (ojectname == "ddlClaimClosedReason") {

                if (value1 != null) {
                    if (value1 == "12586") {

                        $('#trclaimreason').fadeIn("slow");
                    }
                    else {
                        $('#trclaimreason').fadeOut("slow");
                    }

                }
            }
        }

        function showservicerequired() {
            debugger;
            var value1 = $('#chkSer').val();


            //  var array1;
            // array1 = $('#chkSer').val().split(",");
            i = 0;
            if (value1 != null) {
                for (i; i < value1.length; i++) {

                    if (value1[i] == "11595") {
                        $('#trservicerequired').fadeIn("slow");
                    }
                    else {
                        $('#trservicerequired').fadeOut("slow");
                    }
                }
            }
            if (value1 == null) {

                $('#trservicerequired').fadeOut("slow");

            }
        }

        function SaveHomeVisit() {
            //abc
        }
        function CalculateCaseCost() {
            //alert('hi');
            if (document.getElementById('<%=txtRate.ClientID%>').value != '' && document.getElementById('<%=txtNoofHour.ClientID%>').value != '') {
                document.getElementById('<%=txtCaseMgntCost.ClientID%>').value = parseFloat(parseFloat(document.getElementById('<%=txtRate.ClientID%>').value).toFixed(2) * parseFloat(document.getElementById('<%=txtNoofHour.ClientID%>').value).toFixed(2)).toFixed(2)
            }
            else { document.getElementById('<%=txtCaseMgntCost.ClientID%>').value = ''; }
            return false;
        }
        function ValidateCaseMgnt() {
            //alert(document.getElementById('<%=ddlCaseMgntapproved.ClientID%>').value);
            if (document.getElementById('<%=ddlCaseMgntapproved.ClientID%>').value == '1') {
                if (document.getElementById('<%=txtCaseMgntFundingLimit.ClientID%>').value == '') {
                    alert('Case Management Funding Limit cannot be blank');
                    txtCaseMgntFundingLimit.focus();
                    return false;
                }
            }
            return true;
        }
        function DisplayMsgCaseMgnt() {
            alert('Case Management saved successfully.');
        }
    </script>
    <script type="text/javascript">
        var popupStatus = 0;
        var popupDivVal = '';
        function loadPopup(popupDiv) {

            popupDivVal = popupDiv;
            //alert(popupDivVal);
            //loads popup only if it is disabled

            if (popupStatus == 0) {

                centerPopup(popupDiv);
                $("#backgroundPopup1").css({
                    "opacity": "0.7"
                });

                $("#backgroundPopup1").fadeIn("slow");
                $(popupDiv).fadeIn("slow");
                popupStatus = 1;
            }

            // window.scrollTo(0, document.body.clientHeight);


            return false;
        }
        //        $("#backgroundPopup1").click(function () {

        //            disablePopup(popupDivVal);
        //        });
        function disablePopup(popupdiv) {

            //disables popup only if it is enabled

            if (popupStatus == 1) {

                $("#backgroundPopup1").fadeOut("slow");
                $(popupdiv).fadeOut("slow");
                popupStatus = 0;
            }
        }
        function centerPopup(popupdiv) {

            //request data for centering
            // alert('loadPopup');
            // alert(popupDiv);
            var windowWidth = document.documentElement.clientWidth;
            var windowHeight = document.documentElement.clientHeight;
            var popupHeight = $(popupdiv).height();
            var popupWidth = $(popupdiv).width();
            //var popupHeight = 400;
            //var popupWidth = 400;
            //centering  
            $(popupdiv).css({
                "position": "absolute",
                "top": (windowHeight / 2 - popupHeight / 2) + $(window).scrollTop(),
                "left": windowWidth / 2 - popupWidth / 2
            });
            //only need force for IE6  

            $("#backgroundPopup1").css({
                "height": windowHeight
            });
            return false;
        }
        function closepopup() {

            if (popupDivVal == '#divHomeVisit') {
                disablePopup('#divHomeVisit');

            }
            else if (popupDivVal == '#divTelephone') {
                document.getElementById('<%=hdnTelephoneActivityId.ClientID%>').value = "";
                disablePopup('#divTelephone');

            }
            else if (popupDivVal == '#divCaseManagementCost') {
                document.getElementById('<%=hdbCaseManagementCostId.ClientID%>').value = "";
                disablePopup('#divCaseManagementCost');
            }
            else if (popupDivVal == '#divRehabilitation') {
                document.getElementById('<%=hdnRehabilitationId.ClientID%>').value = "";
                disablePopup('#divRehabilitation');

            }
            else if (popupDivVal == '#divServiceRequired') {
                document.getElementById('<%=lblServiceRequired.ClientID%>').value = "";
                    disablePopup('#divServiceRequired');
                }
}
function DeleteItem() {
    //popupStatus = 0;

    document.getElementById('<%=lslFailedCallPop.ClientID%>').remove((document.getElementById('<%=lslFailedCallPop.ClientID%>').value - 1));
    return false;
}
function AddListFailedCall() {
    debugger;
    var hdnFailedCallList = document.getElementById('<%=hdnFailedCallList.ClientID%>');
    var sel = document.getElementById('<%=lslFailedCallPop.ClientID%>');
    var listLength = sel.options.length;
    for (var i = 0; i < listLength; i++) {
        hdnFailedCallList.value += sel.options[i].text + ',';
        //document.getElementByID("list2").add(new Option(sel.options[i].value));
    }

}
function AddItem() {
    // Create an Option object
    //popupStatus = 0;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = dd + '/' + mm + '/' + yyyy + ' ' + today.getHours() + ':' + today.getMinutes();

    var opt = document.createElement("option");

    //  Add an Option object to Drop Down/List Box
    opt.text = today;

    var intcount;
    intcount = document.getElementById('<%=lslFailedCallPop.ClientID%>').length;
    opt.value = intcount + 1;
    document.getElementById('<%=lslFailedCallPop.ClientID%>').options.add(opt);

    //popupStatus = 1;
    return false;
    // Assign text and value to Option object
    //opt.text = today;
    //opt.value = today;



}
    </script>
    <script type="text/javascript">
        function SetHeightDiv()
        { }
        function telephonepopupopen() {
            //alert('hi');
            popupDivVal = '';
            popupStatus = 0;

            //popupDivVal
            PopupAddNew('#divTelephone');
            return false;
        }
    </script>
    <script type="text/javascript">

        function PopupAddNew(id) {

            if (id == '#divHomeVisit') {
                document.getElementById('<%=txtDateOfAppointmentPop.ClientID%>').value = '';
                document.getElementById('<%=txtDateReportCompPop.ClientID%>').value = '';
                document.getElementById('<%=txtFeeChargedPop.ClientID%>').value = '';
                document.getElementById('<%=ddlTypeOfVisitPop.ClientID%>').value = '0';
                //alert()
                // document.getElementById('<%=ddlTypeOfVisitPop.ClientID%>').selectedValue = '0';
                // $("#ddlTypeOfVisitPop").val('0')
                document.getElementById('<%=hdnVisitId.ClientID%>').value = '0';

            }
            else if (id == '#divTelephone') {

                //$("#lslFailedCallPop").empty();

            }
            else if (id == '#divCaseManagementCost') {
                document.getElementById('<%=txtCaseMgntDate.ClientID%>').value = '';
                document.getElementById('<%=txtNoofHour.ClientID%>').value = '';
                document.getElementById('<%=txtRate.ClientID%>').value = '';
                document.getElementById('<%=txtCaseMgntCost.ClientID%>').value = '';
            }
            else if (id == '#divRehabilitation') {
                document.getElementById('<%=ddlRehabType.ClientID%>').value = '0';
                document.getElementById('<%=ddlRehabProvider.ClientID%>').value = '0';
                document.getElementById('<%=txtDateInstructed.ClientID%>').value = '';
                document.getElementById('<%=txtDateReported.ClientID%>').value = '';
                document.getElementById('<%=txtRehabFeeCharged.ClientID%>').value = '';
            }
    loadPopup(id);

    return false;
}
function ConfirmDelete(str) {
    var v = confirm(' You sure want to delete ' + str + ' ?');
    alert(v);
    return false;
}
    </script>
    <script language="javascript" type="text/javascript">
        function Call1AbortedChanged() {

            var flag;
            if (document.getElementById('<%=txtCall1Date.ClientID %>').value.length == 0 || document.getElementById('<%=txtCall1Time.ClientID %>').value.length == 0) {
                alert("Call 1 Date or Time not provided.");
                flag = false;
            }

            //return flag;
        }

        function Call2AbortedChanged() {
            var flag;

            return flag;

        }
        function Call3AbortedChanged() {
            var flag;

            return flag;

        }

        function validatesave() {
            debugger;
            var str;

            var Name = document.getElementById('<%=txtClientFName.ClientID %>');
            if (Name.value == "") {
                str = "Claimant Name is required";
                alert(str);
                Name.focus();
                return false;
            }

            var claimantdob = document.getElementById('<%=txtClientDOB.ClientID %>');
            if (Trim(claimantdob.value) == "") {
                str = "Claimant Date of Birth is required";
                alert(str);
                claimantdob.focus();
                return false;
            }
            else {
                str = validateDate(claimantdob)
                if (str == "ok")
                { }
                else {
                    alert(str);
                    claimantdob.focus();
                    return false;
                }
            }

            var PolicyNum = document.getElementById('<%=txtPolicyNumber.ClientID %>');
            if (Trim(PolicyNum.value) == "") {
                str = "Claim Reference number is required";
                alert(str);
                PolicyNum.focus();
                return false;
                //if (PolicyNum.is(':disabled') == true) {
                //    return false;
                //}
                //else {
                //    PolicyNum.focus();
                //    return false;
                //}
            }

            // debugger;
            var LogUserType = $('#<%=hidLogUserType.ClientID %>');
            if (LogUserType != null) {
                if (LogUserType.val() == "Admin") {
                    var DateNurse = $('#<%=txtDateSentToNurse.ClientID %>');
                    if (DateNurse != null) {
                        if (DateNurse.val() == "") {
                            str = "Date Sent to Nurse field cannot be blank";
                            alert(str);
                            DateNurse.focus();
                            return false;
                        }
                    }
                }
            }


            //
            debugger;
            var ICD9 = document.getElementById('<%=txtICD9Code.ClientID %>');
            var Msg = "Please enter ICD9 code in proper format i.e. 000.00,000.00 etc ";
            if (ICD9 != null) {
                if (ICD9.value != "") {
                    var result = ICD9.value.split(",");
                    if (result.length > 0) {
                        for (var j = 0; j < result.length; j++) {
                            var strnew = result[j];
                            strnew = strnew.trim();
                            var regExp = /^\d+(\.\d{0,9})?$/;
                            var Input = strnew;
                            if (!regExp.test(Input)) {
                                alert("Please enter numeric value in ICD9 code in proper format i.e. 000.00,000.00 etc ");
                                //ICD9.value = "";
                                ICD9.focus();
                                return false;
                            }
                            else {
                                if (strnew.length != 6) {
                                    alert(Msg);
                                    //ICD9.value = "";
                                    ICD9.focus();
                                    return false;
                                }
                                else {
                                    var splitnew = strnew.split(".");
                                    if (splitnew.length > 1) {
                                        if (splitnew[0].length != 3 || splitnew[1].length != 2) {
                                            alert(Msg);
                                           // ICD9.value = "";
                                            ICD9.focus();
                                            return false;
                                        }
                                    }
                                    else {
                                        alert(Msg);
                                        //ICD9.value = "";
                                        ICD9.focus();
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            //

            var EndBenef = document.getElementById('<%=txtEndBenefit.ClientID %>');
            if (EndBenef != null) {
                if (EndBenef.value != "") {
                    str = validateDate(EndBenef)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }




            var IncapDate = document.getElementById('<%=txtIncapacityDay.ClientID %>');
            debugger;
            if (IncapDate != null) {
                if (IncapDate.value != "") {
                    str = validateDate(IncapDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
                else {
                    str = "Date First Absent field cannot be blank";
                    alert(str);
                    IncapDate.focus();
                    return false;
                }
            }
            var ddlInjury = document.getElementById('<%=ddlInjury.ClientID%>');
            if (ddlInjury != null) {
                debugger;
                //alert($('option:selected', $('#ddlInjury')).index());
                if (document.getElementById('<%=ddlInjury.ClientID %>').value == "0") {
                    str = "Illness or Injury field cannot be blank";
                    alert(str);
                    ddlInjury.focus();
                    return false;
                }
            }
            var ClaimCloseDate = document.getElementById('<%=txtClaimCloseDate.ClientID %>');

            if (ClaimCloseDate != null) {
                if (ClaimCloseDate.value != "") {
                    str = validateDate(ClaimCloseDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var ApptDate = document.getElementById('<%=txtAppointmentDate.ClientID %>');
            if (ApptDate != null) {
                if (ApptDate.value != "") {
                    str = validateDate(ApptDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var ReportCompDate = document.getElementById('<%=txtDateReportComp.ClientID %>');
            if (ReportCompDate != null) {
                if (ReportCompDate.value != "") {
                    str = validateDate(ReportCompDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var Call1 = document.getElementById('<%=txtCall1Date.ClientID %>');
            if (Call1 != null) {
                if (Call1.value != "") {
                    str = validateDate(Call1)
                    if (str == "ok") {

                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }





            var RTWDate1 = document.getElementById('<%=txtRTWDateOptimum.ClientID %>');
            if (RTWDate1 != null) {
                if (RTWDate1.value != "") {
                    str = validateDate(RTWDate1)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var NxtSchDate1 = document.getElementById('<%=txtNxtCallSchd.ClientID %>');
            if (NxtSchDate1 != null) {
                if (NxtSchDate1.value != "") {
                    str = validateDate(NxtSchDate1)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }





            var NurseDate = document.getElementById('<%=txtDateSentToNurse.ClientID %>');
            if (NurseDate != null) {
                if (NurseDate.value != "") {
                    str = validateDate(NurseDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var CaseClose = document.getElementById('<%=txtCaseClosed.ClientID %>');
            if (CaseClose != null) {
                if (CaseClose.value != "") {
                    str = validateDate(CaseClose)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var claimhandlerphn = document.getElementById('<%=txtClaimHandlerTelephone.ClientID %>');
            if (Trim(claimhandlerphn.value) == "") {
                str = " Claim Handler phone number is required";
                alert(str);
                return false;
            }

            var Email = document.getElementById('<%=txtClaimHandlerEmail.ClientID %>');
            if (Trim(Email.value) == "") {
                str = "Email is required";
                alert(str);
                return false;
            }
            else {
                str = emailCheck(Email.value)
                if (str == "ok") {
                }
                else {
                    alert(str);
                    return false;
                }
            }
            var teamPhn = document.getElementById('<%=txtTeamTelephone.ClientID %>');
            if (Trim(teamPhn.value) == "") {
                str = "Assessor Team phone number is required";
                alert(str);
                return false;
            }

            var clientaddress1 = document.getElementById('<%=txtAddress1.ClientID %>');
            if (Trim(clientaddress1) == "") {
                str = "Address1 is required";
                alert(str);
                return false;
            }

            var teamEmail = document.getElementById('<%=txtTeamEmail.ClientID %>');
            if (Trim(teamEmail.value) == "") {
                str = "Email is required";
                alert(str);
                return false;
            }
            else {
                str = emailCheck(teamEmail.value)
                //                if (str == "ok") {

                //                }
                //                else {
                //                    //alert(str);
                //                    return false;
                //                }
            }
            document.getElementById('<%=hdnServiceRequired.ClientID%>').value = $('#chkSer').val();
            var servReqd = $('#chkSer').val();

            if (servReqd == null || Trim(servReqd) == "") {
                str = "Please select atleast one Service Required";
                alert(str);
                return false;
            }
            //alert(document.getElementById('<%=ddlDocuments.ClientID %>').options.length);
            if (document.getElementById('<%=ddlDocuments.ClientID %>').options.length < 2) {
                alert("YOU HAVE NOT UPLOADED THE INSTRUCTION FORM. PLEASE UPLOAD THE INSTRUCTION FORM");
                return false;
            }
            debugger;



            return true;
        }



        function PreNoteSaveCheck() {
            debugger;
            var blnTest = false
            var NoteVal = $('#<%=txtAddNote.ClientID %>').val();

            if (NoteVal == "Type your note here.  Once finished click the 'Save Note' button.")
            { blnTest = true };

            if (NoteVal == "")
            { blnTest = true };

            if (blnTest == true) {
                alert('A valid note is required before save!');
                return false;
            }
            return true;
        }

        function numberOnly(txt, e) {
            //debugger;
            var arr = '0123456789.';
            var code;
            var code1;
            if (window.event) {
                code = e.keyCode;
            }
            else {
                code = e.which;
            }

            var cha = String.fromCharCode(code);
            if (arr.indexOf(cha) == -1) {
                return false;
            }
            else if (cha == '.') {
                if (txt.value.indexOf('.') > -1)
                    return false;
            }
            if (txt.value.indexOf('.') > -1) {
                code1 = txt.value.indexOf('.');
                if (txt.value.length == code1 + 3)
                    return false;
            }
        }

        function validateDate(date) {
            debugger;
            var msg;
            var sValidChars;
            var nCounter;
            var datval = date.value;
            msg = "ok";
            sValidChars = "1234567890/";
            if (Trim(datval) != "") {

                if (datval.length != 10) {
                    msg = " Please enter in exact format (dd/mm/yyyy) e.g. 02/01/2000 in ";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }

                for (nCounter = 0; nCounter < datval.length; nCounter++) {

                    if (sValidChars.indexOf(datval.substring(nCounter, nCounter + 1)) == -1) {

                        msg = " Please enter in exact format (dd/mm/yyyy)";
                        alert(msg);
                        date.value = '';
                        date.focus();
                        nCounter = datval.length;
                        return msg;
                    }

                }
                if (datval.indexOf("/") != 2) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 3) != 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 6) < 10 && datval.indexOf("/", 6) > 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }
                debugger;

                var month;
                var day;
                var year;
                month = parseInt(TrimNumber(datval.substring(3, 5)));
                day = parseInt(TrimNumber(datval.substring(0, 2)));
                year = parseInt(datval.substring(6, 10));

                if (month > 12 || month < 1) {
                    msg = " Invalid Month";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }
                if (year < 1900 || year > 3000) {
                    msg = " Invalid year. Should be in (1900-3000)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    if (day > 31 || day < 1) {
                        msg = " Days should not be more than 31 or less than 1 for entered Month";
                        alert(msg);
                        date.value = '';
                        date.focus();
                        return msg;
                    }
                }
                else {
                    if (month == 2 && (year % 4) == 0) {
                        debugger;
                        if (day > 29 || day < 1) {
                            msg = " Days should not be more than 29 or less than 1 for entered Month";
                            alert(msg);
                            date.value = '';
                            date.focus();
                            return msg;
                        }
                    }
                    else if (month == 2 && (year % 4) != 0) {
                        if (day > 28 || day < 1) {
                            msg = " Days should not be more than 28 or less than 1 for entered Month";
                            alert(msg);
                            date.value = '';
                            date.focus();
                            return msg;
                        }
                    }
                    else {
                        if (day > 30 || day < 1) {
                            msg = " Days should not be more than 30 or less than 1 for entered Month";
                            alert(msg);
                            date.value = '';
                            date.focus();
                            return msg;
                        }
                    }
                }
                return msg;

            }
            //msg = "empty";
            return msg;
        }

        function ValidateDOB(dateid) {
            //debugger;
            var msg;
            var sValidChars;
            var nCounter;
            var datval = dateid.value;
            msg = "ok";
            sValidChars = "1234567890/";

            if (Trim(datval) != "") {
                if (datval.length != 10) {
                    msg = " Please enter in exact format (dd/mm/yyyy) e.g. 02/01/2000 in ";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                for (nCounter = 0; nCounter < datval.length; nCounter++) {

                    if (sValidChars.indexOf(datval.substring(nCounter, nCounter + 1)) == -1) {

                        msg = " Please enter in exact format (dd/mm/yyyy)";
                        alert(msg);
                        date.focus();
                        nCounter = datval.length;
                        return msg;
                    }

                }

                if (datval.indexOf("/") != 2) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 3) != 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 6) < 10 && datval.indexOf("/", 6) > 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                var month;
                var day;
                var year;
                month = parseInt(TrimNumber(datval.substring(3, 5)));
                day = parseInt(TrimNumber(datval.substring(0, 2)));
                year = parseInt(datval.substring(6, 10));

                if (month > 12 || month < 1) {
                    msg = " Invalid Month";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (year < 1900 || year > 3000) {
                    msg = " Invalid year. Should be in (1900-3000)";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    if (day > 31 || day < 1) {
                        msg = " Days should not be more than 31 or less than 1 for entered Month";
                        alert(msg);
                        date.focus();
                        return msg;
                    }
                }
                else {
                    if (month == 2 && (year % 4) == 0) {
                        if (day > 29 || day < 1) {
                            msg = " Days should not be more than 29 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else if (month == 2 && (year % 4) != 0) {
                        if (day > 28 || day < 1) {
                            msg = " Days should not be more than 28 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else {
                        if (day > 30 || day < 1) {
                            msg = " Days should not be more than 30 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                }
                //return msg;
                $('#<%=btnCheckClient.ClientID %>').click();
                //_doPostBack('btnCheck', 'OnClick');
                // return false;
            }
        }

        function CloseInsurance(id) {
            $('txtClaimRef2').val('');
            $('trInsuranceDet2').hide();
        }



        function GetFailedDet(id) {
            //debugger;
            var Faildate;
            var FailTime;
            var FailedDateTime;
            var hidFailCall;
            var Company = false;


            if (id == 1) {
                var d = new Date();

                if ($('#<%=lnkAddFailedCall.ClientID %>').is(':disabled') == false) {
                    if (d != null) {
                        var date = d.format("dd/MM/yyyy hh:mm");
                        //var Time = d.getHours() + ":" + d.getMinutes();
                        var FailDate = date;

                        //$('#<%=lstFailedCalls.ClientID %>').append("<option>" + FailDate + "</option>");

                        var fcall = $.ajax({
                            type: "POST",
                            url: "ClientRecord.aspx/InsertFailCall",
                            data: { FailCall: FailDate, id: 1 },
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data, status, xhr) {
                                Company = data.d;
                                if (Company != 0) {
                                    $('#<%=lstFailedCalls.ClientID %>').append("<option value='" + Company + "'>" + FailDate + "</option>");

                                }
                            },
                            error: function (xhr, status, error) {
                                alert("Error occured" + status);
                            }
                        });
                    }
                }
            }

            return false;
        }

        function validateFaildcall(rid) {
            //debugger;
            if (rid == 1) {
                if ($('#<%=lnkRemoveFailCall1.ClientID %>').is(':disabled') == false) {
                    if ($('#<%=lstFailedCalls.ClientID%>').attr("selectedIndex") < 0) {
                        alert('select a call to delete');
                        $('#<%=lstFailedCalls.ClientID%>').focus();
                        return false;
                    }
                }
            }

            return true;

        }

        function validateFaildcallPop(rid) {

            //debugger;
            if (rid == 1) {
                if ($('#<%=lnkFailedPop.ClientID %>').is(':disabled') == false) {
                    if ($('#<%=lslFailedCallPop.ClientID%>').attr("selectedIndex") < 0) {
                        alert('select a call to delete');
                        $('#<%=lslFailedCallPop.ClientID%>').focus();
                        return false;
                    }
                }
            }
            DeleteItem();
            popupStatus = 1;
            return false;

        }
        function RemoveFailCall(rid) {
            //debugger;
            var lstvalue;
            var HidValue;

            if (rid == 1) {
                if ($('#<%=lnkRemoveFailCall1.ClientID %>').is(':disabled') == false) {
                    lstvalue = $('#<%=lstFailedCalls.ClientID %> option:selected').val();

                    if (lstvalue != "") {

                        var Delfcall = $.ajax({
                            type: "POST",
                            url: "FailedCall.aspx/DeleteFailCall",
                            data: "{'FailID':'" + lstvalue + "'}",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data, status, xhr) {
                                Company = data.d;
                            },
                            error: function (xhr, status, error) {
                                alert("Error occured" + status);
                            }
                        });

                    }
                    $('#<%=lstFailedCalls.ClientID %> option:selected').remove();
                }
            }


            return false;
        }

        function UpdateFailCall(uid) {
            var NewVal;
            var listid;

            if (uid == 1) {
                listid = $('#<%=lstFailedCalls.ClientID %> option:selected').val();
            }
        }
        function deletedocument() {
            //debugger;
            alert('called');
            if (confirm('You sure want delete this file?'))
                return true;
            else
                return false;
        }

        function showupload() {
            //debugger;
            document.getElementById('<%= tblDocUpload.ClientID %>').style.display = "block";
            document.getElementById('<%= hidShowDoc.ClientID %>').value = "1";
            return false;
        }
        function Hideupload() {
            //debugger;
            var fileupload = document.getElementById('<%= tblDocUpload.ClientID %>');
            document.getElementById('<%= tblDocUpload.ClientID %>').style.display = "none";
            document.getElementById('<%= hidShowDoc.ClientID %>').value = "0";
            document.getElementById('<%= btnCancle.ClientID %>').checked = false;

            return false;
        }

        function ActiveButton() {
            if ($('#<%=chkSendEmail.ClientID %>').is(':checked')) {
                $('#<%=btnSaveNote.ClientID %>').attr('disabled', false);
            }
            else {
                $('#<%=btnSaveNote.ClientID %>').attr('disabled', true);
            }
        }

        function SaveNotes() {

            //debugger;
            var chkSendMail;
            var txtAddNote;
            var ddlCaseMngrSelectedIndex;
            var ddlCaseMngrSelectedValue;
            var txtClaimHandlerEmail;
            var lblNotes;

            // chkSendMail = $('#<%=chkSendEmail.ClientID %>' ).attr('checked');

            chkSendMail = $('#<%=chkSendEmail.ClientID %>').is(":checked");
            txtAddNote = $('#<%=txtAddNote.ClientID %>').val();


            ddlCaseMngrSelectedIndex = $('#<%=ddlCaseMngr.ClientID %>').get(0).selectedIndex;
            ddlCaseMngrSelectedValue = $('#<%=ddlCaseMngr.ClientID %>' + ' option:selected').val();
            txtClaimHandlerEmail = $('#<%=txtClaimHandlerEmail.ClientID %>').val();
            lblNotes = $('#<%=lblNotes.ClientID %>');
            lblNotesInnerHtml = $('#<%=lblNotes.ClientID %>').html();

            $.ajax({
                type: "POST",
                url: "ClientRecord.aspx/SaveNote",
                data: "{'InnerText':'" + lblNotesInnerHtml + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, status, xhr) {
                    //alert("true");

                    //alert("Response from method is : " + data.d);
                    if (data.d == true) {
                        $(lblNotes).append('<br/><br/>' + txtAddNote);
                    }
                    else {
                        alert("Note could not be saved.");
                    }

                    $('#<%=txtAddNote.ClientID %>').val('');
                },
                error: function (xhr, status, error) {
                    alert("Error occured :<br/> Status -" + status + "<br/> Error: " + error.toString());
                }
            });

            return false;
        }

        function DisableDateControls(dateID) {
            //debugger;
            var DateVal;

            if (dateID == 1) {
                DateVal = $('#<%=txtCall1Date.ClientID %>').val();

                if (DateVal != "") {
                    $('#<%=lstFailedCalls.ClientID %>').attr("disabled", "disabled");
                    $('#<%=lnkAddFailedCall.ClientID %>').attr("disabled", "disabled");
                    $('#<%=lnkRemoveFailCall1.ClientID %>').attr("disabled", "disabled");
                }
            }


            return false;
        }

        function CallDateFunctions(date, rid) {
            //debugger;
            validateDate(date);
            DisableDateControls(rid);
            return false;
        }

        function DisableButtons() {
            //debugger;
            $('#<%=lnkAddFailedCall.ClientID %>').attr("disabled", "disabled");
            //$('#<%=lnkAddFailedCall.ClientID %>').attr("OnClientClick").remove();
            $('#<%=lnkRemoveFailCall1.ClientID %>').attr("disabled", "disabled");
            //$('#<%=lnkRemoveFailCall1.ClientID %>').attr("OnClientClick").remove();
        }



        //   function ShowDeferredPeriodOther() {

        //      debugger;
        //    var value = ').val();
        //  if (value == "11600") {

        //    debugger;

        //  $').fadeIn("slow");

        //  }
        //  else
        //    {
        // (').fadeOut("slow");
        //   }

        //} 

    </script>
    <style type="text/css">
        .ButtonClass {
        }

        .modalBackground {
            background-color: #707070;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }

        .auto-style1 {
            width: 108px;
        }

        .auto-style2 {
            width: 158px;
        }

        .auto-style3 {
            height: 29px;
        }

        .auto-style4 {
            width: 158px;
            height: 29px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptMngr" runat="server">
    </asp:ScriptManager>
    <div style="text-align: center">
        <div id="divScroll"></div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 730px; margin: 0 auto;"
            class="MainText" id="tableMain">
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" Font-Names="Arial" Font-Size="12pt"
                        ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 95px" valign="top">
                    <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td align="center" class="SectionHeader" style="height: 19px">HCB References&nbsp; <b>(HCB Use Only)</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 58px">
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px">
                                    <tr>
                                        <td>HCB Ref:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtHCBReference" runat="server" autocomplete="off" MaxLength="50"
                                                Width="196px" CssClass="Textbox" ReadOnly="True" TabIndex="1" Enabled="false"
                                                ForeColor="DarkGray" Font-Bold="true"></asp:TextBox>
                                        </td>
                                        <td style="width: 140px;"></td>
                                        <td>Case Manager:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCaseMngr" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="2">
                                            </asp:DropDownList>
                                            <img id="img_ddlNurses" src="images/Exclamation.jpg" style="visibility: hidden" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>

                <td valign="top">
                    <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%;">
                        <tr>
                            <td align="center" class="SectionHeader" style="height: 19px">MetLife Contact Details
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 35px">
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px;">
                                    <tr>
                                        <td>Claim Assessor:&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlClaimAssessor" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="3" OnSelectedIndexChanged="ddlClaimAssessor_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList></td>


                                        <td style="width: 42px;"></td>
                                        <td>Telephone:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClaimHandlerTelephone" runat="server" MaxLength="12" Width="171px"
                                                CssClass="Textbox" TabIndex="4" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Email Address:
                                        </td>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtClaimHandlerEmail" runat="server" CssClass="Textbox" Width="98.5%"
                                                TabIndex="5"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Broker:&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAssessorTeam" runat="server" Width="177px" TabIndex="6" onchange="showcommonoption(this);">
                                                <%--OnSelectedIndexChanged="ddlAssessorTeam_SelectedIndexChanged" AutoPostBack="true">--%>

                                                <%--onchange="ShowTeamAssessorOther();" --%>
                                                <%-- AutoPostBack="true"--%>
                                                <%--OnSelectedIndexChanged="ddlAssessorTeam_SelectedIndexChanged"--%>
                                            </asp:DropDownList>
                                            <%-- <img id="img5" src="images/Exclamation.jpg" style="visibility: hidden" />--%>
                                        </td>
                                        <td style="width: 42px;"></td>
                                        <td>MetLife Claims phone:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTeamTelephone" runat="server" MaxLength="12" Width="171px" CssClass="Textbox"
                                                TabIndex="7" onkeypress="javascript:return numberOnly(this,event);" Enabled="false"></asp:TextBox>
                                        </td>

                                    </tr>

                                    <tr id="trteamassessor" style="display: none">
                                        <td>Other :
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTeamOther" runat="server" Width="171px" CssClass="Textbox"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>MetLife Claims Team Email :
                                        </td>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtTeamEmail" runat="server" CssClass="Textbox" Width="98.5%" TabIndex="8" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td align="center" class="SectionHeader">Claimant Details
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px; width: 100%;">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td style="vertical-align: middle; width: 113px;" valign="top">First Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtClientFName" runat="server" MaxLength="50" CssClass="Textbox"
                                                            TabIndex="9"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtClientFName"
                                                            ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid First Name" Display="Dynamic"
                                                            ValidationGroup="validInfo">
                                                        </asp:RegularExpressionValidator>
                                                    </td>
                                                    <td style="vertical-align: middle;" valign="top">Surname:
                                                    </td>
                                                    <td style="text-align: left; vertical-align: middle;" valign="top">
                                                        <asp:TextBox ID="txtClientSurname" runat="server" MaxLength="50" CssClass="Textbox"
                                                            TabIndex="10"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtClientSurname"
                                                            ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid SurName" Display="Dynamic"
                                                            ValidationGroup="validInfo">
                                                        </asp:RegularExpressionValidator>
                                                    </td>
                                                    <td style="text-align: right; vertical-align: middle;" valign="top">DOB:
                                                    </td>
                                                    <td style="text-align: right; vertical-align: middle;" valign="top">
                                                        <asp:TextBox ID="txtClientDOB" runat="server" MaxLength="10" CssClass="Textbox" Width="75%"
                                                            AutoPostBack="true" TabIndex="11" onchange="javascript:return ValidateDOB(this);"></asp:TextBox>
                                                        <cc1:MaskedEditExtender ID="MaskedEditExtender17" runat="server" Mask="99/99/9999"
                                                            InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtClientDOB">
                                                        </cc1:MaskedEditExtender>
                                                        <asp:Button ID="btnCheckClient" runat="server" Style="display: none" OnClick="btnCheckClient_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>


                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>Address1:
                                                    </td>
                                                    <td style="padding-left: 17px;">
                                                        <asp:TextBox ID="txtAddress1" runat="server" Style="text-align: left;" Width="180px"
                                                            CssClass="Textbox" TabIndex="12" MaxLength="200">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: right">Tel No.:
                                                    </td>
                                                    <td style="text-align: right">&nbsp;&nbsp;<asp:TextBox ID="txtClientWorkTelephone" runat="server" MaxLength="12"
                                                        Width="230px" CssClass="Textbox" TabIndex="14" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Address2:
                                                    </td>
                                                    <td style="padding-left: 17px;">
                                                        <asp:TextBox ID="txtAddress2" runat="server" Style="text-align: left;" Width="180px"
                                                            CssClass="Textbox" TabIndex="12" MaxLength="200">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: right">Mobile No.:
                                                    </td>
                                                    <td style="text-align: right">&nbsp;&nbsp;<asp:TextBox ID="txtClientMobileTelephone" runat="server" MaxLength="12"
                                                        Width="230px" CssClass="Textbox" TabIndex="15" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>City:
                                                    </td>
                                                    <td style="padding-left: 17px;">
                                                        <asp:TextBox ID="txtCity" runat="server" Style="text-align: left" Width="180px"
                                                            CssClass="Textbox" TabIndex="14" MaxLength="200">
                                                        </asp:TextBox>


                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: right;">Service Required:
                                                    </td>
                                                    <td style="text-align: left; padding-left: 16px;">&nbsp;&nbsp;

                                                            <select id="chkSer" multiple="multiple" class="multiselect" style="width: 230px" onchange="showservicerequired()">
                                                            </select>
                                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtOccupation"
                                                            ForeColor="Red" ValidationExpression="^[a-zA-Z.' ']{1,255}$" ErrorMessage="Invalid Occupation"
                                                            ValidationGroup="validInfo">
                                                        </asp:RegularExpressionValidator>
                                                        --%>
                                                    </td>
                                                </tr>

                                                <tr id="trservicerequired" style="display: none">
                                                    <td colspan="4"></td>
                                                    <td valign="top" style="text-align: right">Other :
                                                    </td>
                                                    <td valign="top" style="text-align: right">
                                                        <asp:TextBox ID="txtservicerequiredother" runat="server" Width="230px" CssClass="Textbox"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>

                                                    <td style="text-align: left;">County:
                                                    </td>
                                                    <td style="padding-left: 17px;">
                                                        <asp:TextBox ID="txtCounty" runat="server" Style="text-align: left" Width="180px"
                                                            CssClass="Textbox" TabIndex="15" MaxLength="200">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: right">Occupation:
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:TextBox ID="txtOccupation" runat="server" CssClass="TextBox" TabIndex="17" Width="230px"></asp:TextBox>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="6">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtOccupation"
                                                            ForeColor="Red" ValidationExpression="^[a-zA-Z.' ']{1,255}$" ErrorMessage="Invalid Occupation"
                                                            ValidationGroup="validInfo">
                                                        </asp:RegularExpressionValidator></td>
                                                </tr>
                                                <tr>

                                                    <td style="vertical-align: middle;" valign="top">Postcode:
                                                    </td>
                                                    <td style="padding-left: 17px;">
                                                        <asp:TextBox ID="txtClientPostCode" runat="server" CssClass="TextBox" MaxLength="10"
                                                            TabIndex="18" Width="122px"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtClientPostCode"
                                                            ForeColor="Red" ValidationExpression="^[a-zA-Z0-9' ']{1,50}$" ErrorMessage="Invalid PostCode"
                                                            ValidationGroup="validInfo">
                                                        </asp:RegularExpressionValidator>
                                                    </td>
                                                    <td>
                                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtClientPostCode"
                                                            ForeColor="Red" ValidationExpression="^[a-zA-Z0-9' ']{1,50}$" ErrorMessage="Invalid Zip Code"
                                                            ValidationGroup="validInfo">
                                                        </asp:RegularExpressionValidator>--%></td>
                                                    <td></td>
                                                    <td style="text-align: right">Employer Name:
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:TextBox ID="txtEmployerName" runat="server" CssClass="TextBox" TabIndex="19" Width="230px"></asp:TextBox>
                                                        <%--<asp:RegularExpressionValidator ID="regtxtEmployerName" runat="server" ControlToValidate="txtEmployerName"
                                                ForeColor="Red"  ErrorMessage="Invalid Occupation"
                                                ValidationGroup="validInfo">
                                            </asp:RegularExpressionValidator>--%>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td style="vertical-align: middle;" valign="top">Gender:
                                                    </td>
                                                    <td style="padding-left: 16px;">
                                                        <asp:DropDownList ID="ddlGender" runat="server" TabIndex="20" Width="125px">
                                                            <asp:ListItem Value="0" Text="-Select Gender-"></asp:ListItem>
                                                            <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                                                            <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                                        </asp:DropDownList>

                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: right">Employer Contact:
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:TextBox ID="txtEmployerContract" runat="server" CssClass="TextBox" TabIndex="21" Width="230px"></asp:TextBox>
                                                    </td>

                                                </tr>
                                                <tr>
                                                   <%-- <asp:UpdatePanel ID="upSchemeName" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>--%>
                                                    <td style="vertical-align: middle;" valign="top" width="100px">Scheme Name:
                                                    </td>
                                                    <td style="padding-left: 16px; width: 180px;">
                                                        
                                                                <asp:DropDownList ID="ddlSchemeName" runat="server" TabIndex="20" Width="180px" onchange="javascript:SetSchemeNumber();"  >
                                                                </asp:DropDownList><%--OnSelectedIndexChanged="ddlSchemeName_SelectedIndexChanged" --%>
                                                         
                                                    </td>

                                                    <td style="text-align: right" colspan="3">MetLife Scheme Number:
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:TextBox ID="txtSchemeNumber" runat="server" CssClass="TextBox" TabIndex="20" Width="230px"></asp:TextBox>
                                                        <%-- <asp:DropDownList ID="ddlMetSchemeNumber" runat="server" TabIndex="20" Width="235px">
                                                        </asp:DropDownList>--%>
                                                    </td>
                                                      <%-- </ContentTemplate>
                                                        </asp:UpdatePanel>--%>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>


                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td align="center" class="SectionHeader">MetLife Scheme Details
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px" width="100%">
                                    <tr>
                                        <td valign="top">MetLife Claim Ref.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPolicyNumber" runat="server" Width="171px" CssClass="TextboxUpper"
                                                TabIndex="21" MaxLength="25"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtPolicyNumber"
                                                ForeColor="Red" ValidationExpression="^[a-zA-Z0-9'']{1,25}$" ErrorMessage="Invalid Claim Ref. Number"
                                                ValidationGroup="validInfo">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                        <td></td>

                                        <td valign="top" class="auto-style2">Incapacity 
                                                         Definition:
                                        </td>
                                        <td valign="top">
                                            <asp:DropDownList ID="ddlIncapacityDefinition" runat="server" Width="200px" CssClass="Textbox"
                                                TabIndex="23">
                                            </asp:DropDownList>

                                        </td>
                                        <%--<td style="text-align: left" width="145px" valign="top">Claim Co.:
                                        </td>
                                        <td valign="top">
                                            <asp:DropDownList ID="ddlCorpPartner" runat="server" TabIndex="21" Width="200px">
                                            </asp:DropDownList>
                                        
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td valign="top" width="145px" class="auto-style3">Deferred Period:
                                        </td>
                                        <td class="auto-style3">
                                            <asp:DropDownList ID="ddlWaitingPeriod" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="22" onchange="showcommonoption(this);">
                                                <%-- OnSelectedIndexChanged="ddlWaitingPeriod_SelectedIndexChanged" AutoPostBack="true">--%>
                                            </asp:DropDownList>
                                            <img id="img1" src="images/Exclamation.jpg" style="visibility: hidden" />
                                        </td>
                                        <td class="auto-style3"></td>
                                        <td valign="top" class="auto-style4">End of Benefit Term:
                                        </td>
                                        <td valign="top" class="auto-style3">
                                            <asp:TextBox ID="txtEndBenefit" runat="server" CssClass="Textbox" TabIndex="25" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender28" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtEndBenefit">
                                            </cc1:MaskedEditExtender>
                                        </td>


                                    </tr>


                                    <tr id="trDeferredPeriod" style="display: none">
                                        <td valign="top">Other :
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="txtDeferredOther" runat="server" Width="171px" CssClass="Textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;" valign="top">Monthly Benefit (£):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMonthlyBenefit" runat="server" CssClass="Textbox" MaxLength="15"
                                                TabIndex="24" Width="171px" onkeypress="javascript:return numberOnly(this,event)"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <%--<td style="vertical-align: middle;" valign="top">End of Benefit Term:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEndBenefit" runat="server" CssClass="Textbox" TabIndex="25" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender28" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtEndBenefit">
                                            </cc1:MaskedEditExtender>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td colspan="5">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <%--   <tr id="trInsuranceDet2" runat="server" style="display: none">
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td class="SectionHeader">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="width: 80%; text-align: center; padding-left: 7%">
                                            <asp:Label ID="lblInsTitle2" runat="server" Text="Insurance Details 2" ForeColor="White"
                                                Font-Names="Arial" Font-Size="9pt">
                                            </asp:Label>
                                        </td>
                                        <td style="width: 20%; text-align: right">
                                            <asp:LinkButton ID="lnkCloseIns2" runat="server" Text="Remove Details" ForeColor="White"
                                                Font-Names="Arial" Font-Size="9pt" OnClientClick="javascript:return CloseInsuranceDet2()">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px">
                                    <tr>
                                        <td>Claim Ref.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClaimRef2" runat="server" Width="171px" CssClass="TextboxUpper"
                                                TabIndex="27" MaxLength="25"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtClaimRef2"
                                                ForeColor="Red" ValidationExpression="^[a-zA-Z0-9'']{1,25}$" ErrorMessage="Invalid Claim Ref. No.2"
                                                ValidationGroup="validInfo">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                        <td></td>
                                        <td>Corporate Partner:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCorpPart2" runat="server" TabIndex="28">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;" valign="top">Waiting Period:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlWaitPeriod2" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="29">
                                            </asp:DropDownList>
                                            <img id="img7" src="images/Exclamation.jpg" style="visibility: hidden" />
                                        </td>
                                        <td></td>
                                        <td style="vertical-align: middle;" valign="top">Product Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlprodType2" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="30">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;" valign="top">Monthly Benefit (£):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMnthBenefit2" runat="server" CssClass="Textbox" MaxLength="15"
                                                TabIndex="31" Width="171px" onkeypress="javascript:return numberOnly(this,event)"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td style="vertical-align: middle;" valign="top">End of Benefit Term:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEndBenefit2" runat="server" CssClass="Textbox" TabIndex="32"
                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender8" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtEndBenefit2">
                                            </cc1:MaskedEditExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnAddMoreDet2" runat="server" Text="Add New Details" CssClass="ButtonClass"
                                    OnClientClick="javascript:return OpenInsuranceDet3();" TabIndex="33" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trInsuranceGap2" runat="server" style="display: none">
                <td>&nbsp;
                </td>
            </tr>
            <tr id="trInsuranceDet3" runat="server" style="display: none">
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td class="SectionHeader">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="width: 80%; text-align: center; padding-left: 7%">Insurance Details 3
                                        </td>
                                        <td style="width: 20%; text-align: right">
                                            <asp:LinkButton ID="lnkRemoIns3" runat="server" Text="Remove Details" ForeColor="White"
                                                Font-Names="Arial" Font-Size="9pt" OnClientClick="javascript:return CloseInsuranceDet3()">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px">
                                    <tr>
                                        <td>Claim Ref.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClaimRef3" runat="server" Width="171px" CssClass="TextboxUpper"
                                                TabIndex="34" MaxLength="25"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtClaimRef3"
                                                ForeColor="Red" ValidationExpression="^[a-zA-Z0-9'']{1,25}$" ErrorMessage="Invalid Claim Ref. No.3"
                                                ValidationGroup="validInfo">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                        <td></td>
                                        <td>Corporate Partner:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCorpPart3" runat="server" TabIndex="35">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;" valign="top">Waiting Period:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlWaitPeriod3" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="36">
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                        <td style="vertical-align: middle;" valign="top">Product Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlProdType3" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="37">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;" valign="top">Monthly Benefit (£):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMnthBenefit3" runat="server" CssClass="Textbox" MaxLength="15"
                                                TabIndex="38" Width="171px" onkeypress="javascript:return numberOnly(this,event)"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td style="vertical-align: middle;" valign="top">End of Benefit Term:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEndBenefit3" runat="server" CssClass="Textbox" TabIndex="39"
                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender14" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtEndBenefit3">
                                            </cc1:MaskedEditExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnAddMoreDet3" runat="server" Text="Add New Details" CssClass="ButtonClass"
                                    OnClientClick="javascript:return OpenInsuranceDet4();" TabIndex="40" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trInsuranceGap3" runat="server" style="display: none">
                <td>&nbsp;
                </td>
            </tr>
            <tr id="trInsuranceGap4" runat="server" style="display: none">
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td class="SectionHeader">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="width: 80%; text-align: center; padding-left: 7%">Insurance Details 4
                                        </td>
                                        <td style="width: 20%; text-align: right">
                                            <asp:LinkButton ID="lnkRemDet4" runat="server" Text="Remove Details" ForeColor="White"
                                                Font-Names="Arial" Font-Size="9pt" OnClientClick="javascript:return CloseInsuranceDet4()">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px">
                                    <tr>
                                        <td>Claim Ref.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClaimRef4" runat="server" Width="171px" CssClass="TextboxUpper"
                                                TabIndex="41" MaxLength="25"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtClaimRef4"
                                                ForeColor="Red" ValidationExpression="^[a-zA-Z0-9'']{1,25}$" ErrorMessage="Invalid Claim Ref. No.4"
                                                ValidationGroup="validInfo">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                        <td></td>
                                        <td>Corporate Partner:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCorpPart4" runat="server" TabIndex="42">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;" valign="top">Waiting Period:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlWaitPeriod4" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="43">
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                        <td style="vertical-align: middle;" valign="top">Product Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlProdType4" runat="server" Width="177px" CssClass="Textbox"
                                                TabIndex="44">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;" valign="top">Monthly Benefit (£):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMnthBenefit4" runat="server" CssClass="Textbox" MaxLength="15"
                                                TabIndex="45" Width="171px" onkeypress="javascript:return numberOnly(this,event)"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td style="vertical-align: middle;" valign="top">End of Benefit Term:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEndBenefit4" runat="server" CssClass="Textbox" TabIndex="46"
                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender21" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtEndBenefit4">
                                            </cc1:MaskedEditExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trInsuranceDet4" runat="server" style="display: none">
                <td>&nbsp;
                </td>
            </tr>--%>
            <tr>
                <td style="height: 179px" valign="top">
                    <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td align="center" class="SectionHeader">Claim Details
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 142px">
                                <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px">
                                    <tr>
                                        <td colspan="5">
                                            <asp:LinkButton ID="lnkViewServiceRequired" runat="server" Text="View Service Required" OnClientClick="return PopupAddNew('#divServiceRequired');"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 153px; text-align: left;">
                                            <asp:RequiredFieldValidator ID="ReqValIncapDay" runat="server" ControlToValidate="txtIncapacityDay"
                                                Display="Dynamic" ToolTip="1st Incapacity Day is required" ForeColor="Red" SetFocusOnError="true"
                                                ValidationGroup="validInfo">*</asp:RequiredFieldValidator>
                                            Date First Absent:
                                        </td>
                                        <td style="width: 170px;">
                                            <asp:TextBox ID="txtIncapacityDay" runat="server" Width="171px" CssClass="Textbox"
                                                TabIndex="47" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtIncapacityDay">
                                            </cc1:MaskedEditExtender>
                                        </td>
                                        <td></td>
                                        <td style="width: 140px; text-align: right">
                                            <asp:RequiredFieldValidator ID="ReqValIllness" runat="server" InitialValue="0" ControlToValidate="ddlInjury"
                                                Display="Dynamic" ToolTip="Illness or Injury should be selected" ForeColor="Red"
                                                SetFocusOnError="true" ValidationGroup="validInfo">*</asp:RequiredFieldValidator>
                                            Illness or Injury:
                                        </td>
                                        <td style="width: 203px">
                                            <asp:DropDownList ID="ddlInjury" runat="server" Width="194px" CssClass="Textbox"
                                                TabIndex="48" onchange="showcommonoption(this);">
                                                <%--   OnSelectedIndexChanged="ddlInjury_SelectedIndexChanged" AutoPostBack="true">--%>
                                            </asp:DropDownList>
                                            <%--  <img id="img_ddlInjury" src="images/Exclamation.jpg" style="visibility: hidden" />--%>
                                        </td>
                                    </tr>
                                    <tr id="trillnessinjury" style="display: none">
                                        <td colspan="3"></td>
                                        <td valign="top" style="text-align: right">Other :
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="txtillnessinjuryother" runat="server" Width="171px" CssClass="Textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Disability Type:
                                        </td>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtDisabilityType" runat="server" Width="455px" CssClass="Textbox"
                                                TabIndex="49"></asp:TextBox>
                                            (per MDG)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;" valign="middle">ICD9 Code:
                                        </td>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtICD9Code" runat="server" Width="180px" CssClass="Textbox" TabIndex="50"
                                                MaxLength="40"></asp:TextBox>
                                            <%--<br />--%>
                                            &nbsp;&nbsp;<font color="red" face="Arial" size="2">(Enter ICD9 codes separated
                                                by commas i.e 000.00,000.00 etc.)</font>
                                            <%-- <asp:RegularExpressionValidator ID="regExpICDCode" runat="server" ControlToValidate="txtICD9Code"
                                                ValidationExpression="^([a-zA-Z0-9,]*)(\.[0-9]{2})*$"  ErrorMessage="Invalid ICD9 Code" ForeColor="Red" ValidationGroup="validInfo">*
                                            </asp:RegularExpressionValidator >
                                            <img id="img2" src="images/Exclamation.jpg" style="visibility: hidden" />--%>
                                            <%-- ValidationExpression="^([a-zA-Z0-9]*)(\.[0-9]{2})*$"--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 153px; text-align: left;">Claim Closed Date:
                                        </td>
                                        <td style="width: 170px">
                                            <asp:TextBox ID="txtClaimCloseDate" runat="server" Width="180px" CssClass="Textbox"
                                                TabIndex="51" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender27" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtClaimCloseDate">
                                            </cc1:MaskedEditExtender>
                                        </td>
                                        <td></td>
                                        <td style="width: 170px; text-align: right">Claim Closed Reason:
                                        </td>
                                        <td style="width: 203px">
                                            <asp:DropDownList ID="ddlClaimClosedReason" runat="server" Width="194" CssClass="Textbox"
                                                TabIndex="52" onchange="showcommonoption(this);">
                                                <%-- OnSelectedIndexChanged="ddlClaimClosedReason_SelectedIndexChanged" AutoPostBack="true">--%>
                                            </asp:DropDownList>
                                            <%--  <asp:TextBox ID="txtClaimCloseReason" runat="server" Width="168px" CssClass="Textbox"
                                                TabIndex="52"></asp:TextBox>--%>
                                        </td>
                                    </tr>

                                    <tr id="trclaimreason" style="display: none">
                                        <td colspan="3"></td>
                                        <td valign="top" style="text-align: right">Other :
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="txtclaimreasonother" runat="server" Width="171px" CssClass="Textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 129px; text-indent: 5px; height: 20px" valign="bottom">Details:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 129px; text-indent: 5px; height: 20px" valign="bottom">
                                                        <strong><font size="1">(Max 2500 Chars )</font></strong>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </td>
                                        <td colspan="4">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>

                                                        <asp:TextBox ID="txtClaimDetails" runat="server" Height="77px" MaxLength="2500" TextMode="MultiLine"
                                                            Width="543px" CssClass="Textbox" TabIndex="53"></asp:TextBox>
                                                        <%--          <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtClaimDetails"
                                                            ForeColor="Red" ValidationExpression="^[a-zA-Z0-9' ']{1,25}$" ErrorMessage="Invalid Claim Details"
                                                            ValidationGroup="validInfo">
                                                        </asp:RegularExpressionValidator>--%>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 179px" valign="top">
                    <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                        <tr>
                            <td align="center" class="SectionHeader">HCB Activity
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 142px">
                                <table border="0" cellpadding="0" cellspacing="0" style="text-indent: 5px">
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left" valign="bottom">
                                            <font style="text-decoration: underline; font-weight: bold">VISIT</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">Date Claimant Contacted:&nbsp;&nbsp;&nbsp;
                                            <asp:TextBox ID="txtclaimantfirstcontacted" runat="server" CssClass="Textbox"
                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Mask="99/99/9999"
                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtclaimantfirstcontacted">
                                            </cc1:MaskedEditExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left"></td>
                                    </tr>
                                    <%-- <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="upHomeVisits" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlHomeVisit" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="0" id="tblHomeVisit" runat="server">
                                                            <tr>
                                                                <td style="width: 140px" valign="middle">Date of Appointment:
                                                                </td>
                                                                <td style="width: 203px">
                                                                    <asp:TextBox ID="txtAppointmentDate" runat="server" Width="171px" CssClass="Textbox"
                                                                        TabIndex="54" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtAppointmentDate">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>
                                                                <td style="width: 150px" valign="middle">Date Report Completed:
                                                                </td>
                                                                <td style="width: 203px">
                                                                    <asp:TextBox ID="txtDateReportComp" runat="server" Width="171px" CssClass="Textbox"
                                                                        TabIndex="55" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateReportComp">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 140px" valign="middle">Type Of Visit:</td>
                                                                <td style="width: 203px">
                                                                    <asp:DropDownList ID="ddlTypeOfVisit" runat="server" Width="177px" CssClass="Textbox" TabIndex="55">
                                                                    </asp:DropDownList>

                                                                </td>
                                                                <td style="width: 140px" valign="middle">Fee Charged (£):</td>
                                                                <td style="width: 203px">
                                                                    <asp:TextBox ID="txtFeeChargeHomeVisit" runat="server" onkeypress="javascript:return numberOnly(this,event);" TabIndex="55" Width="171px"></asp:TextBox>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="4" align="right">
                                                                    <asp:Button ID="btnSaveVisit" runat="server" Text="Save Visit" OnClick="btnSaveVisit_Click" /></td>

                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlGRIDHomeVisit" runat="server">
                                                        <table style="width: 100%;" id="tbGRIDHomeVisit" runat="server">

                                                            <tr>
                                                                <td>

                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td align="right">

                                                                                <asp:LinkButton Style="float: right; padding: 0px 0px 0px 0px;" ID="lnkAdd" runat="server" OnClick="lnkAdd_Click"> Add New Visit</asp:LinkButton></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>

                                                                                <asp:GridView ID="grdHomeVisit" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="grdHomeVisit_RowDataBound">
                                                                                    <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" HorizontalAlign="Center" />
                                                                                    <RowStyle HorizontalAlign="Center" />
                                                                                    <Columns>

                                                                                        <asp:BoundField DataField="AppointmentDate" HeaderText="Date of appointment" />
                                                                                        <asp:BoundField DataField="ReportCompDate" HeaderText="Date Report Completed" />
                                                                                        <%--         <asp:BoundField DataField="Name" HeaderText="Type Of Visit" />--%>
                                                                                        <asp:TemplateField HeaderText="Type Of Visit">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="lnkTypeofvisit" runat="server" CommandName="visit" OnCommand="lnk_Command" Text='<% #Eval("Name")%>'
                                                                                                    CommandArgument='<%# string.Concat(Eval("HCBVisitId"),",",Eval("AppointmentDate"),",",Eval("ReportCompDate"),",",Eval("FeeCharged"),",",Eval("TYpeOfVisit")) %>'> Edit</asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Fee Charged" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblVisitFee" runat="server" Text='<%# String.Format("{0:n2}",Eval("FeeCharged"))%>'></asp:Label>
                                                                                            </ItemTemplate>

                                                                                        </asp:TemplateField>
                                                                                        <%--  <asp:BoundField DataField="FeeCharged" HeaderText="Fee Charged" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />--%>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField Value='<%#Eval("HCBReference") %>' ID="hdnHCBVisitId" runat="server" />
                                                                                                <asp:LinkButton ID="lnkeEdit1" runat="server" CommandName="edt" OnCommand="lnk_Command"
                                                                                                    CommandArgument='<%# string.Concat(Eval("HCBVisitId"),",",Eval("AppointmentDate"),",",Eval("ReportCompDate"),",",Eval("FeeCharged"),",",Eval("TYpeOfVisit")) %>'> Edit</asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <%--<asp:ImageButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("HCBVisitId")%>'
                                                                                            OnClientClick="return confirm('You want to delete this visit ?');" OnCommand="lnk_Command"
                                                                                            CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />--%>
                                                                                                <asp:ImageButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("HCBVisitId")%>'
                                                                                                    OnClientClick="return confirm('You want to delete this visit ?');" OnCommand="lnk_Command"
                                                                                                    CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                                <asp:HiddenField ID="hdnhcbvisitfee" Value="" runat="server" />
                                                                                <asp:HiddenField ID="hdntype" Value="" runat="server" />
                                                                                <asp:HiddenField ID="hdnhcbtelephonevisitfee" Value="" runat="server" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>


                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="upHomeVisits" runat="server">
                                                <ProgressTemplate>
                                                    <img alt="progress" src="Images/wait.gif" />
                                                    Processing...
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border-bottom: 1px solid black">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left" valign="bottom">
                                            <font style="text-decoration: underline; font-weight: bold">TELEPHONE INTERVENTION</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trHCBTelephoneintervention" runat="server">
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="updcall1" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlTelephoneIntervention" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                            <tr>

                                                                <td>Date:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCall1Date" runat="server" CssClass="Textbox" TabIndex="56"
                                                                        onchange="validateDate(this);DisableDateControls(1);"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCall1Date">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>
                                                                <td style="text-align: right;">Time:
                                                                </td>
                                                                <td valign="middle">
                                                                    <asp:TextBox ID="txtCall1Time" runat="server" CssClass="Textbox" TabIndex="57" Width="40px"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                                        MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtCall1Time">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>
                                                                <td valign="middle" align="left">hours
                                                                </td>
                                                                <td style="text-align: right;">Type of Call:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlTypeofCall" runat="server" CssClass="Textbox" Width="134px">
                                                                    </asp:DropDownList>
                                                                </td>

                                                            </tr>
                                                            <tr>

                                                                <td style="width: 80px; vertical-align: top;">Failed Calls:
                                                                </td>
                                                                <td>
                                                                    <asp:ListBox ID="lstFailedCalls" runat="server" Width="97%"></asp:ListBox>
                                                                </td>
                                                                <td style="text-align: left; vertical-align: top;" colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>



                                                                            <td>
                                                                                <asp:LinkButton ID="lnkAddFailedCall" runat="server" Text="Add Failed Call" TabIndex="60"
                                                                                    Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none;"
                                                                                    CommandArgument="1" CommandName="Add" OnClick="lnkFailedCallPage_Click">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:LinkButton ID="lnkRemoveFailCall1" runat="server" Text="Remove Failed Call"
                                                                                    Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none"
                                                                                    CommandArgument="1" CommandName="del" OnClientClick="javascript:return validateFaildcall(1);"
                                                                                    OnClick="lnkFailedCallPage_Click" TabIndex="61">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                </td>


                                                                <td style="text-align: right;">Fee Charged:</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFeeCharge" runat="server" CssClass="Textbox" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox></td>


                                                            </tr>
                                                            <tr>

                                                                <td style="width: 20%">Expected RTW Date(Optimum):
                                                                </td>
                                                                <td valign="middle">
                                                                    <asp:TextBox ID="txtRTWDateOptimum" runat="server" CssClass="Textbox" TabIndex="62"
                                                                        onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateOptimum">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>
                                                                <td colspan="3">&nbsp;</td>
                                                                <td style="text-align: right;" valign="middle">Expected RTW Date (Maximum):
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRTWDateMax" runat="server" CssClass="Textbox"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender18" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="MonthDayYear" TargetControlID="txtRTWDateMax">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>

                                                            </tr>
                                                            <tr>

                                                                <td>Case Management Recommended?
                                                                </td>
                                                                <td style="text-align: left;" valign="middle">&nbsp;<asp:DropDownList ID="ddlCaseMngtRecom" runat="server" TabIndex="63" CssClass="Textbox" Width="148">
                                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                                                </td>
                                                                <td colspan="3">&nbsp;</td>
                                                                <td style="text-align: right; padding-top: 15px;" valign="middle">Next Call Scheduled:
                                                                </td>
                                                                <td>&nbsp;&nbsp;<asp:TextBox ID="txtNxtCallSchd" runat="server" CssClass="Textbox"
                                                                    TabIndex="64" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender9" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtNxtCallSchd">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td colspan="7" align="right">
                                                                    <asp:Button ID="btnSaveTelephone" runat="server" Text="Save Telephone intervention" OnClick="btnSaveTelephone_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlGridTelephoneIntervention" runat="server">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td align="right">

                                                                    <asp:LinkButton Style="float: right; padding: 0px 0px 0px 0px;" ID="lnkAddTelephone" runat="server"> Add New Telephone Intervention</asp:LinkButton></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="grdTelephoneIntervention" runat="server" AutoGenerateColumns="false" Width="100%"
                                                                        OnRowDataBound="grdTelephoneIntervention_OnRowDataBound">
                                                                        <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" HorizontalAlign="Center" />
                                                                        <RowStyle HorizontalAlign="Center" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="CallDate" HeaderText="Call Date" />
                                                                            <asp:BoundField DataField="CallCaseMngtRecom" HeaderText="Case Management Recommended" />
                                                                            <asp:BoundField DataField="TypeofCall" HeaderText="Type of Call" />
                                                                            <%--    <asp:BoundField DataField="FeeCharged" HeaderText="Fee Charged" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />--%>
                                                                            <asp:TemplateField HeaderText="Fee Charged" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblVisitFee" runat="server" Text='<%# String.Format("{0:n2}",Eval("FeeCharged"))%>'></asp:Label>
                                                                                </ItemTemplate>

                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField Value='<%#Eval("HCBTelephoneID") %>' ID="hdnHCBTelephoneID" runat="server" />
                                                                                    <%-- <asp:LinkButton ID="lnkeditTele" runat="server" CommandName="edt" OnCommand="lnk_CommandGrid"
                                                                                        CommandArgument='<%# string.Concat(Eval("HCBTelephoneID"),",",Eval("CallDate"),",",Eval("CallTime"),",",Eval("FeeCharged"),",",Eval("TypeofCall")) %>'> Edit</asp:LinkButton>--%>
                                                                                    <asp:LinkButton ID="lnkeditTele" runat="server" CommandName="edt" OnCommand="lnk_CommandGrid"
                                                                                        CommandArgument='<% #Eval("HCBTelephoneID")%>'> Edit</asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>

                                                                                    <asp:ImageButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("HCBTelephoneID")%>'
                                                                                        OnClientClick="return confirm('You want to delete this Telephone Activity ?');" OnCommand="lnk_CommandGrid"
                                                                                        CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID="upHomeVisits" runat="server">
                                                <ProgressTemplate>
                                                    <img alt="progress" src="Images/wait.gif" />
                                                    Processing...
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4" style="border-bottom: 1px solid black">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left" valign="bottom">
                                            <font style="text-decoration: underline; font-weight: bold">CASE MANAGEMENT</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="upCaseMgnt" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlCaseMgnt" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="0" id="Table1" runat="server" width="100%">
                                                            <tr>
                                                                <td>Case Management Approved by MetLife:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlCaseMgntapproved" runat="server" Width="173px" AutoPostBack="true" OnSelectedIndexChanged="ddlCaseMgntapproved_SelectedIndexChanged">
                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr id="trCaseMgntApproved" runat="server">
                                                                <td>Case Management Funding Limit Approved by MetLife (£):</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCaseMgntFundingLimit" runat="server" onkeypress="javascript:return numberOnly(this,event);" Width="171px"></asp:TextBox>

                                                                </td>
                                                            </tr>
                                                            <tr id="trCurrentCaseTotal" runat="server">
                                                                <td>Current Case Total (£):</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCurrentCaseTotal" runat="server" Enabled="false" onkeypress="javascript:return numberOnly(this,event);" Width="171px"></asp:TextBox>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="right">
                                                                    <asp:Button ID="btnCaseMgntSave" runat="server" Text="Save Case Management" OnClick="btnCaseMgntSave_Click" OnClientClick="return ValidateCaseMgnt();" /></td>
                                                            </tr>
                                                            <tr id="trAddnewCasemgntCost">
                                                                <td colspan="2" align="right">
                                                                    <asp:LinkButton Style="float: right; padding: 0px 0px 0px 0px;" ID="lnkCaseMgntCost" runat="server"> Add Case Management Cost</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:GridView ID="grdCaseMgntCost" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="grdCaseMgntCost_RowDataBound">
                                                                        <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" HorizontalAlign="Center" />
                                                                        <RowStyle HorizontalAlign="Center" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="CaseDate" HeaderText="Date" />
                                                                            <asp:BoundField DataField="CaseHours" HeaderText="No. of Hours" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                                            <%--<asp:BoundField DataField="CaseRate" HeaderText="Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />--%>
                                                                            <asp:TemplateField HeaderText="Rate" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblVisitFee" runat="server" Text='<%# String.Format("{0:n2}",Eval("CaseRate"))%>'></asp:Label>
                                                                                </ItemTemplate>

                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Cost" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCost" runat="server" Text='<%# String.Format("{0:n2}",Eval("CaseCost"))%>'></asp:Label>

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <%-- <asp:BoundField DataField="CaseCost" HeaderText="Cost" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />--%>

                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <%--  <asp:HiddenField Value='<%#Eval("CaseMgmtCostId") %>' ID="hdnCaseMgmtCostId" runat="server" />--%>
                                                                                    <asp:LinkButton ID="lnkedittele" runat="server" CommandName="edt" OnCommand="lnk_CommandCaseMgnt"
                                                                                        CommandArgument='<%# string.Concat(Eval("CaseMgmtCostId"),",",Eval("CaseDate"),",",Eval("CaseHours"),",",Eval("CaseRate"),",",Eval("CaseCost")) %>'> Edit</asp:LinkButton>
                                                                                    <%--  <asp:LinkButton ID="lnkeditCaseMgntCost" runat="server" CommandName="edt" OnCommand="lnk_CommandCaseMgnt"
                                                                                        CommandArgument='<% #Eval("CaseMgmtCostId")%>'> Edit</asp:LinkButton>--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>

                                                                                    <asp:ImageButton Visible="true" ID="lnkDeleteCaseMgntCost" runat="server" CommandArgument='<% #Eval("CaseMgmtCostId")%>'
                                                                                        OnClientClick="return confirm('You want to delete this Case Management Cost ?');" OnCommand="lnk_CommandCaseMgnt"
                                                                                        CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <%--   <tr>
                                        <td colspan="4">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left">
                                            <font style="text-decoration: underline; font-weight: bold">CALL 2</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="updFcall2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="text-align: left; width: 11%" valign="top">Date:
                                                                        </td>
                                                                        <td style="width: 40%">
                                                                            <asp:TextBox ID="txtCall2Date" runat="server" Width="171px" CssClass="Textbox" TabIndex="65"
                                                                                onchange='validateDate(this);DisableDateControls(2);'></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender10" runat="server" Mask="99/99/9999"
                                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCall2Date">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>
                                                                        <td style="text-align: right; width: 10%" valign="middle">Time:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCall2Time" runat="server" Width="40px" CssClass="Textbox" TabIndex="66"></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender11" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                                                MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtCall2Time">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>
                                                                        <td valign="middle" align="left" width="100%">hours
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 80px;">Failed Calls:
                                                                        </td>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 25%">
                                                                                        <asp:ListBox ID="lstFailCall2" runat="server" Width="60%"></asp:ListBox>
                                                                                    </td>
                                                                    
                                                                                    <td style="width: 20%; text-align: left">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:LinkButton ID="lnkFailCall2" runat="server" Text="Add Failed Call" Font-Names="Arial"
                                                                                                        Font-Size="9pt" ForeColor="blue" Style="text-decoration: none" TabIndex="69"
                                                                                                        CommandArgument="2" CommandName="Add" OnClick="lnkFailedCall_Click">
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:LinkButton ID="lnkRemoveFailCall2" runat="server" Text="Remove Failed Call"
                                                                                                        TabIndex="70" Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none"
                                                                                                        CommandArgument="2" CommandName="del" OnClick="lnkFailedCall_Click" OnClientClick="javascript:return validateFaildcall(2);">
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="cbCall2Aborted" runat="server" Text="Aborted Call"
                                                                                                        AutoPostBack="True" OnCheckedChanged="cbCall2Aborted_CheckedChanged" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td style="width: 17%">Expected RTW Date:
                                                                        </td>
                                                                        <td>&nbsp;<asp:TextBox ID="txtCall2RTWDateOpt" runat="server" CssClass="Textbox" TabIndex="71"
                                                                            onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender13" runat="server" Mask="99/99/9999"
                                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCall2RTWDateOpt">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>--%>
                                    <%--<td>
                                                                    Expected RTW Date (Maximum):
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="txtCall2RTWDateMax" runat="server" CssClass="Textbox" TabIndex="24"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender14" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="MonthDayYear" TargetControlID="txtCall2RTWDateMax">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>
                                                                  
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 240px">Case Management Recommended?
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlCaseMngtRecom2" runat="server" TabIndex="72">
                                                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 240px">Is the original RTW Date realistic?
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlRTWReal" runat="server" TabIndex="73">
                                                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 65%">If No, add explanation in the Case Notes section and enter envisaged RTW Date:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtEnvisagedRTWDate" runat="server" Width="171px" CssClass="Textbox"
                                                                                TabIndex="74" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender16" runat="server" Mask="99/99/9999"
                                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtEnvisagedRTWDate">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 140px">Next Call Scheduled:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCall2NxtSchDate" runat="server" Width="171px" CssClass="Textbox"
                                                                                TabIndex="75" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender15" runat="server" Mask="99/99/9999"
                                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCall2NxtSchDate">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border-bottom: 1px solid black">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left">
                                            <font style="text-decoration: underline; font-weight: bold">CALL 3</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="upfdfcall3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="3" width="100%">
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 11%; text-align: left" valign="top">Date:
                                                                        </td>
                                                                        <td style="width: 40%">
                                                                            <asp:TextBox ID="txtCall3Date" runat="server" Width="171px" CssClass="Textbox" TabIndex="76"
                                                                                onchange='validateDate(this);DisableDateControls(3);'></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender17" runat="server" Mask="99/99/9999"
                                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCall3Date">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>
                                                                        <td style="text-align: right; width: 10%" valign="middle">Time:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td style="">
                                                                            <asp:TextBox ID="txtCall3Time" runat="server" Width="40px" CssClass="Textbox" TabIndex="77"></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender18" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                                                MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtCall3Time">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>
                                                                        <td valign="middle" align="left" width="100%">hours
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 80px;">Failed Calls:
                                                                        </td>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 25%">
                                                                                        <asp:ListBox ID="lstFailCall3" runat="server" Width="60%"></asp:ListBox>
                                                                                    </td>
                                                                                    <%--<td style="width: 20%; text-align: right">
                                                                                <asp:TextBox ID="txtFailCall3Date" runat="server" Width="120px" CssClass="Textbox"
                                                                                    TabIndex="78" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender19" runat="server" Mask="99/99/9999"
                                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtFailCall3Date">
                                                                                </cc1:MaskedEditExtender>
                                                                            </td>
                                                                            <td style="width: 25%; text-align: center; padding-top: 3%">
                                                                                <asp:TextBox ID="txtFailCall3Time" runat="server" CssClass="Textbox" TabIndex="79"
                                                                                    Width="40%"></asp:TextBox>
                                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender31" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                                                    MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtFailCall3Time">
                                                                                </cc1:MaskedEditExtender>
                                                                                <br /> OnClientClick=" :return GetFailedDet(3);"OnClientClick=" :return RemoveFailCall(3);
                                                                                <font size="2px" face="Arial" color="gray">(24 hr format)</font>
                                                                            </td>--%>
                                    <%-- <td style="width: 20%; text-align: left">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:LinkButton ID="lnkFailCall3" runat="server" Text="Add Failed Call" Font-Names="Arial"
                                                                                                        Font-Size="9pt" ForeColor="blue" Style="text-decoration: none" TabIndex="80"
                                                                                                        CommandArgument="3" CommandName="add" OnClick="lnkFailedCall_Click">
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:LinkButton ID="lnkRemoveFailCall3" runat="server" Text="Remove Failed Call"
                                                                                                        Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none"
                                                                                                        TabIndex="81" CommandArgument="3" CommandName="del" OnClick="lnkFailedCall_Click"
                                                                                                        OnClientClick="javascript:return validateFaildcall(3);">
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="cbCall3Aborted" runat="server" Text="Aborted Call"
                                                                                                        AutoPostBack="True" OnCheckedChanged="cbCall3Aborted_CheckedChanged" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td style="width: 17%">Expected RTW Date:
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <asp:TextBox ID="txtCall3RTWDateOpt" runat="server" CssClass="Textbox" TabIndex="82"
                                                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender20" runat="server" Mask="99/99/9999"
                                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCall3RTWDateOpt">
                                                                            </cc1:MaskedEditExtender>
                                                                        </td>--%>
                                    <%--<td>
                                                                    Expected RTW Date (Maximum):
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:TextBox ID="txtCall3RTWDateMax" runat="server" CssClass="Textbox" TabIndex="24"></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender21" runat="server" Mask="99/99/9999"
                                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="MonthDayYear" TargetControlID="txtCall3RTWDateMax">
                                                                    </cc1:MaskedEditExtender>
                                                                </td>--%>
                                    <%--   </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 180px">Further Intervention Required?
                                                            </td>
                                                            <td style="text-align: left" colspan="3">
                                                                <asp:DropDownList ID="ddlFurtherInterReq" runat="server" TabIndex="83">
                                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>RTW Date Agreed:
                                                            </td>
                                                            <td style="width: 203px; text-align: left" colspan="3">
                                                                <asp:TextBox ID="txtRTWDateAgreed" runat="server" Width="171px" CssClass="Textbox"
                                                                    TabIndex="84" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender22" runat="server" Mask="99/99/9999"
                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateAgreed">
                                                                </cc1:MaskedEditExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Case Management Involved?
                                                            </td>
                                                            <td style="text-align: left" colspan="3">
                                                                <asp:DropDownList ID="ddlCaseMngtInvolve3" runat="server" TabIndex="85">
                                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 30%">Rehabilitation Services Purchased?
                                                            </td>
                                                            <td style="text-align: left" colspan="3">
                                                                <asp:DropDownList ID="ddlRehabServPurchase3" runat="server" TabIndex="86">
                                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
                                    <tr>
                                        <td colspan="4" style="border-bottom: 1px solid black">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left" valign="bottom">
                                            <font style="text-decoration: underline; font-weight: bold">FUNDED TREATMENT</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="upRehab" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel1" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="0" id="Table2" runat="server" width="100%">
                                                            <tr>
                                                                <td colspan="2">Current Case Total (£):<span style="padding-left: 50px;"><asp:TextBox ID="txtRehabCurrentCaseTotal" runat="server" Enabled="false" Text=""></asp:TextBox></span> </td>

                                                            </tr>
                                                            <tr>
                                                                <td align="right" colspan="2">
                                                                    <asp:LinkButton ID="lnkAddRehabilitation" runat="server" Text="Add New funded Treatment Session"></asp:LinkButton></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:GridView ID="grdRehabilitation" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="grdRehabilitation_RowDataBound">
                                                                        <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" HorizontalAlign="Center" />
                                                                        <RowStyle HorizontalAlign="Center" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="RehabTypeName" HeaderText="Type" />
                                                                            <asp:BoundField DataField="RehabProviderName" HeaderText="Provider" />
                                                                            <asp:BoundField DataField="InstructedDate" HeaderText="Instructed Date" />
                                                                            <%--<asp:BoundField DataField="RehabilitationFee" HeaderText="Fee Charged" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />--%>
                                                                            <asp:TemplateField HeaderText="Fee Charged" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCostRehab" runat="server" Text='<%# String.Format("{0:n2}",Eval("RehabilitationFee"))%>'></asp:Label>

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkeditRehab" runat="server" CommandName="edt" OnCommand="lnk_CommandRehab"
                                                                                        CommandArgument='<%# string.Concat(Eval("RehabilitationId"),",",Eval("RehabilitationFee"),",",Eval("RehabilitationTypeId"),",",Eval("RehabilitationProviderId"),",",Eval("InstructedDate"),",",Eval("ReportedDate")) %>'> Edit</asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>

                                                                                    <asp:ImageButton Visible="true" ID="lnkeDeleteRehab" runat="server" CommandArgument='<% #Eval("RehabilitationId")%>'
                                                                                        OnClientClick="return confirm('You want to delete this Funded Treatment Session ?');" OnCommand="lnk_CommandRehab"
                                                                                        CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 368px" align="left" valign="top">
                                            <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px">
                                                <tr>
                                                    <td align="center" class="SectionHeader" style="height: 19px">
                                                        <%--<a name="Notes">--%>Case Notes</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 290px" valign="top">
                                                        <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 15px" width="100%">
                                                            <tr>
                                                                <td colspan="5" style="height: 295px" valign="top">
                                                                    <div style="text-align: left">
                                                                        <asp:UpdatePanel ID="UpdPnlNotes" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td style="width: 709px; height: 250px; text-align: left" valign="top" colspan="2">

                                                                                            <div id="lblNotes" runat="server" width="698px" style="overflow: auto; padding-left: 2%; height: 200px">
                                                                                            </div>

                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10px"></td>
                                                                                        <td style="width: 709px">
                                                                                            <asp:TextBox ID="txtAddNote" runat="server" Height="124px" TextMode="MultiLine" Width="692px"
                                                                                                CssClass="Textbox" TabIndex="87"></asp:TextBox>
                                                                                            <cc1:TextBoxWatermarkExtender ID="WaterExtd" runat="server" TargetControlID="txtAddNote"
                                                                                                WatermarkText="Type your note here.  Once finished click the 'Save Note' button.">
                                                                                            </cc1:TextBoxWatermarkExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10px; height: 39px"></td>
                                                                                        <td style="width: 709px; height: 39px">
                                                                                            <asp:Button ID="btnSaveNote" runat="server" CausesValidation="true" OnClientClick="javascript:return PreNoteSaveCheck();"
                                                                                                Text="Save Note" CssClass="ButtonClass" Width="75px" TabIndex="88" OnClick="btnSaveNote_Click" />
                                                                                            &nbsp; &nbsp; &nbsp;<asp:CheckBox ID="chkSendEmail" runat="server" Checked="True"
                                                                                                Text="Send Email Notification" TabIndex="89" /> &nbsp; &nbsp; &nbsp;
                                                                                            <asp:CheckBox ID="chkImportantMail" runat ="server" Text="Important" TabIndex="90" CssClass="CheckboxTextSpace" /> 
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="btnSaveNote" EventName="Click" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                        <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdPnlNotes" runat="server">
                                                                            <ProgressTemplate>
                                                                                <img alt="progress" src="Images/wait.gif" />
                                                                                Processing...
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 150px" valign="top">
                                            <asp:UpdatePanel ID="upAdmin" runat="server">
                                                <ContentTemplate>
                                                    <table cellpadding="3px" cellspacing="0" class="DataInputSection" style="width: 100%; height: 56px; text-indent: 5px">
                                                        <tr>
                                                            <td align="center" class="SectionHeader" colspan="2">Administration & Financial
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 23%; text-align: left">Date Received by HCB:</td>
                                                            <td style="text-align: left" class="auto-style1">

                                                                <asp:TextBox ID="txtHCBReceivedDate" runat="server" MaxLength="10"
                                                                    Width="171px" Font-Bold="true" ForeColor="DarkGray" Enabled="false"
                                                                    CssClass="Textbox" TabIndex="90"
                                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender23"
                                                                    runat="server" Mask="99/99/9999" InputDirection="LeftToRight"
                                                                    MaskType="Date" UserDateFormat="DayMonthYear"
                                                                    TargetControlID="txtHCBReceivedDate">
                                                                </cc1:MaskedEditExtender>

                                                            </td>
                                                        </tr>
                                                        <tr>

                                                            <td style="text-align: left">Date Sent to Nurse:</td>
                                                            <td style="text-align: left" class="auto-style1">
                                                                <asp:TextBox ID="txtDateSentToNurse" runat="server"
                                                                    MaxLength="10" Width="170px"
                                                                    CssClass="Textbox" TabIndex="91"
                                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender24" runat="server"
                                                                    Mask="99/99/9999" InputDirection="LeftToRight"
                                                                    MaskType="Date" UserDateFormat="DayMonthYear"
                                                                    TargetControlID="txtDateSentToNurse">
                                                                </cc1:MaskedEditExtender>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">Case Cancelled:
                                                            </td>
                                                            <td style="text-align: left" class="auto-style1">
                                                                <asp:TextBox ID="txtCaseCancelled" runat="server" CssClass="Textbox" MaxLength="10"
                                                                    TabIndex="92" Width="170px" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender26" runat="server" Mask="99/99/9999"
                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCaseCancelled">
                                                                </cc1:MaskedEditExtender>
                                                            </td>

                                                        </tr>

                                                        <tr>
                                                            <td style="text-align: left">Case Closed:
                                                            </td>
                                                            <td style="text-align: left" class="auto-style1">
                                                                <asp:TextBox ID="txtCaseClosed" runat="server" CssClass="Textbox" MaxLength="10"
                                                                    TabIndex="94" Width="170px" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender29" runat="server" Mask="99/99/9999"
                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCaseClosed">
                                                                </cc1:MaskedEditExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">Reason Closed:
                                                            </td>
                                                            <td style="text-align: left" class="auto-style1">
                                                                <asp:DropDownList ID="ddlReasonCancel" runat="server" Width="330px" CssClass="Textbox"
                                                                    TabIndex="93">
                                                                </asp:DropDownList>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table style="width: 100%; text-indent: 5px">
                                                                    <tr>
                                                                        <td style="width: 23%; text-align: left">HCB Fees Charged(£):                                        
                                                                        </td>

                                                                        <td style="width: 15%; text-align: left;" class="auto-style1">


                                                                            <asp:TextBox ID="txtFeeCharged" runat="server" Width="90px" CssClass="Textbox" Enabled="false"
                                                                                TabIndex="95"></asp:TextBox>

                                                                        </td>
                                                                        <td style="text-align: left">Rehab Costs(£):                                        
                                                                        </td>
                                                                        <td style="text-align: left">
                                                                            <asp:TextBox ID="txtRehabcost" runat="server" Width="90px" CssClass="Textbox" Enabled="false"
                                                                                TabIndex="96"></asp:TextBox>
                                                                        </td>
                                                                        <td style="text-align: left">Total Case Costs(£):                                        
                                                                        </td>
                                                                        <td style="text-align: left">
                                                                            <asp:TextBox ID="txttotalcasecost" runat="server" Width="90px" CssClass="Textbox" Enabled="false"
                                                                                TabIndex="97"></asp:TextBox>
                                                                        </td>

                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdCaseDocs" align="left" style="height: 130px" valign="top">
                                            <table border="1" cellpadding="0" cellspacing="0" class="DataInputSection" style="width: 100%;"
                                                id="tblCaseDocs">
                                                <tr>
                                                    <td align="center" class="SectionHeader" style="height: 18px">
                                                        <%--   <a name="upload" />--%>Case Documents
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="tdCaseDocs2" style="height: 80px; padding-top: 10px;" valign="top">
                                                        <table border="0" cellpadding="3" cellspacing="0" style="text-indent: 5px">
                                                            <tr>
                                                                <td>Document Name:
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:DropDownList ID="ddlDocuments" runat="server" Width="480px" CssClass="Textbox"
                                                                        TabIndex="95">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="REQddlDocuments" runat="server" ControlToValidate="ddlDocuments"
                                                                        Display="Dynamic" InitialValue="0" CssClass="ErrorMessage" ValidationGroup="validateFIle"
                                                                        ErrorMessage="Select file"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Instructions form uploaded:
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:CheckBox ID="chkFormUploaded" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding-left: 0px;">
                                                                    <font size="1">
                                                <asp:Label ID="lblDocs" runat="server" Width="134px"></asp:Label></font>
                                                                </td>
                                                                <%-- <td colspan="4">
                                                                    <asp:Button ID="btnDownloadDocument" runat="server" Text="Download" ValidationGroup="validateFIle"
                                                                        CssClass="ButtonClass" Width="75px" TabIndex="96" OnClick="btnDownloadDocument_Click" />
                                                                    <asp:Button ID="btnShowUpload" runat="server" Text="Upload" UseSubmitBehavior="False"
                                                                        CssClass="ButtonClass" Width="75px" TabIndex="98" OnClientClick="return showupload();" />
                                                                    <asp:Button ID="btnRemoveDoc" runat="server" ValidationGroup="validateFIle" Text="Remove"
                                                                        CssClass="ButtonClass" Width="75px" TabIndex="97" OnClick="btnRemoveDoc_Click" /><%-- OnClientClick="javascript:return deletedocument();" 
                                                                    
                                                                </td> --%>
                                                                <td colspan="3">

                                                                    <asp:Button ID="btnDownloadDocument" runat="server" Text="Download" ValidationGroup="validateFIle"
                                                                        CssClass="ButtonClass" Width="75px" TabIndex="96" OnClick="btnDownloadDocument_Click" />
                                                                    <asp:Button ID="btnShowUpload" runat="server" Text="Upload" UseSubmitBehavior="False"
                                                                        CssClass="ButtonClass" Width="75px" TabIndex="98" OnClientClick="return showupload();" />
                                                                </td>
                                                                <td style="text-align: right">
                                                                    <asp:Button ID="btnRemoveDoc" runat="server" ValidationGroup="validateFIle" Text="Remove"
                                                                        CssClass="ButtonClass" Width="75px" TabIndex="97" OnClick="btnRemoveDoc_Click" />

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5">
                                                                    <table id="tblDocUpload" runat="server" style="display: none">
                                                                        <tr>
                                                                            <td style="padding-left: 0px; width: 175px;">Upload Document:
                                                                            </td>
                                                                            <td colspan="4">
                                                                                <asp:FileUpload ID="FileUploadDoc" ValidationGroup="validateupload" runat="server"
                                                                                    Width="464px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td colspan="4">
                                                                                <asp:CheckBox ID="chkfoverwrite" runat="server" ToolTip="Check if you want to overwrite if file wioth same name already uploaded" />
                                                                                Overwrite file
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:RequiredFieldValidator ID="reqFileUploadDoc" runat="server" ControlToValidate="FileUploadDoc"
                                                                                    Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateupload" ErrorMessage="Select file to upload"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="REGFileUploadDoc" runat="server" ValidationGroup="validateupload"
                                                                                    CssClass="ErrorMessage" ErrorMessage="Only .docx, .doc, .xls, .xlsx, .pdf, .tif, .wav, .mp3, .zip and .rar files are allowed"
                                                                                    Display="Dynamic" ValidationExpression="^.+(.wav|.WAV|.mp3|.MP3|.doc|.DOC|.docx|.DOCX|.tif|.TIF|.pdf|.PDF|.xls|.XLS|.xlsx|.XLSX|.zip|.ZIP|.rar|.RAR)$"
                                                                                    ControlToValidate="FileUploadDoc"> </asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-left: 0px;"></td>
                                                                            <td>
                                                                                <asp:Button ID="btnUpload" ValidationGroup="validateupload" runat="server" Text="Upload Doc"
                                                                                    CssClass="ButtonClass" Width="90px" TabIndex="99" OnClick="btnUpload_Click" /><%--OnClientClick="if(document.getElementById('FileUploadDoc').value==''){document.getElementById('lblUpload').innerText='';alert('Browse to a file prior to upload!');return false;};"--%><%--OnClientClick="if(document.getElementById('FileUploadDoc').value==''){document.getElementById('lblUpload').innerText='';alert('Browse to a file prior to upload!');return false;};"--%>
                                                                                <asp:Button ID="btnCancle" runat="server" Text="Cancel" OnClientClick="return Hideupload();" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center">
                                                                                <asp:Label ID="lblUpload" runat="server" CssClass="ErrorMessage" ForeColor="Red"
                                                                                    Width="459px"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 41px">
                                            <asp:Button ID="btnCreate" runat="server" CausesValidation="False" Text="New Record"
                                                UseSubmitBehavior="False" CssClass="ButtonClass" Width="125px" TabIndex="100"
                                                OnClick="btnCreate_Click" />
                                            &nbsp;<asp:Button ID="btnDelete" runat="server" OnClientClick="if (!confirm_delete()) return false;"
                                                Text="Delete Record" UseSubmitBehavior="False" CssClass="ButtonClass" Width="125px"
                                                TabIndex="101" Enabled="False" />&nbsp;
                    <asp:Button ID="btnSave" runat="server" Text="Save Record" OnClick="btnSave_Click"
                        ValidationGroup="validInfo" CausesValidation="true" CssClass="ButtonClass" Width="125px"
                        TabIndex="102" OnClientClick="javascript:return validatesave();" /><br />
                                            <asp:Button ID="btnEMAIL" runat="server" Text="** TEST email TEST **" UseSubmitBehavior="False"
                                                CssClass="ButtonClass" Width="125px" TabIndex="40" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 100%" valign="top">
                                            <asp:Label ID="statusLabel" runat="server" CssClass="ErrorMessage" ForeColor="Red"
                                                Width="726px"></asp:Label>
                                            <br />
                                            <asp:ValidationSummary ID="ValidationSummary" ValidationGroup="validInfo" runat="server"
                                                ShowMessageBox="True" Width="729px" ForeColor="Red" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 100%" valign="top">&nbsp;
                    <asp:HiddenField ID="hidDurationID" runat="server" />
                                            <asp:HiddenField ID="hidInsurance1ID" runat="server" />
                                            <asp:HiddenField ID="hidInsurance2ID" runat="server" />
                                            <asp:HiddenField ID="hidInsurance3ID" runat="server" />
                                            <asp:HiddenField ID="hidInsurance4ID" runat="server" />
                                            <asp:HiddenField ID="hidFailedCall1ID" runat="server" />
                                            <asp:HiddenField ID="hidFailedCall2ID" runat="server" />
                                            <asp:HiddenField ID="hidFailedCall3ID" runat="server" />
                                            <asp:HiddenField ID="hidNoteID" runat="server" />
                                            <asp:HiddenField ID="hidShowDoc" runat="server" />
                                            <asp:HiddenField ID="hidFailCall1" runat="server" />
                                            <asp:HiddenField ID="hidFailCall2" runat="server" />
                                            <asp:HiddenField ID="hidFailCall3" runat="server" />
                                            <asp:HiddenField ID="hidHomeFee" runat="server" />
                                            <asp:HiddenField ID="hidCall1Fee" runat="server" />
                                            <asp:HiddenField ID="hidCall2Fee" runat="server" />
                                            <asp:HiddenField ID="hidCall3Fee" runat="server" />
                                            <asp:HiddenField ID="hidLogUserType" runat="server" />

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <%--<asp:LinkButton runat="server" ID="lbRehabPopupDummy" />
    <cc1:ModalPopupExtender BackgroundCssClass="modalBackground" runat="server" OkControlID="lbRehabPopupDummy" 
    CancelControlID="btnRehabPopupClose"
        TargetControlID="btnRehabModify" PopupControlID="pRehabPopup" />--%>
    <%--<asp:Panel ID="pRehabPopup" BorderWidth="1px" BorderColor="Black" class="DataInputSection" Width="600" runat="server">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <table style="padding: 5px 5px 5px 5px;" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td nowrap="nowrap" align="left" class="SectionHeader"
                            style="height: 19px !important;">Case Management Costs</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td nowrap="nowrap" align="center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" nowrap="nowrap">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtRehabPopupDate" ErrorMessage="RequiredFieldValidator"
                                            ForeColor="Red" SetFocusOnError="True" ValidationGroup="vgRehabPopup">*</asp:RequiredFieldValidator>
                                        Case Management Date:
                                        <asp:TextBox ID="txtRehabPopupDate" runat="server" CssClass="Textbox"
                                            MaxLength="10" onchange="javascript:return validateDate(this);" TabIndex="91"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server"
                                            InputDirection="LeftToRight" Mask="99/99/9999" MaskType="Date"
                                            TargetControlID="txtRehabPopupDate" UserDateFormat="DayMonthYear">
                                        </cc1:MaskedEditExtender>
                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtRehabPopupCost" Display="Dynamic"
                                            ErrorMessage="RequiredFieldValidator" ForeColor="Red" SetFocusOnError="True"
                                            ValidationGroup="vgRehabPopup">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server"
                                            ControlToValidate="txtRehabPopupCost" Display="Dynamic"
                                            ErrorMessage="CompareValidator" ForeColor="#FF3300" Operator="GreaterThanEqual"
                                            SetFocusOnError="True" Type="Double" ValidationGroup="vgRehabPopup"
                                            ValueToCompare="0">*</asp:CompareValidator>
                                        Cost (£):&nbsp;
                               <asp:TextBox ID="txtRehabPopupCost" runat="server" CssClass="Textbox"
                                   TabIndex="95" Width="100px"></asp:TextBox>
                                        <asp:Button ID="btnRehabPopupAdd" runat="server"
                                            OnClick="btnRehabPopupAdd_Click" Text="Add" ValidationGroup="vgRehabPopup" />
                                        <asp:Button ID="btnRehabSave" runat="server" OnClick="btnRehabSave_Click"
                                            Text="Save" ValidationGroup="vgRehabPopup" Visible="False" />
                                        <asp:Button ID="btnRehabCancel" runat="server" OnClick="btnRehabCancel_Click"
                                            Text="Cancel" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" nowrap="nowrap" style="border-bottom: 1px solid black">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap">&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvRehabCosts" runat="server" AutoGenerateColumns="False"
                                            OnRowCommand="gvRehabCosts_RowCommand" Width="98%" BorderWidth="2px">
                                            <AlternatingRowStyle CssClass="odd gradeX" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnRehabPopupEdit" runat="server" Text="[Edit]"
                                                            CommandArgument='<%# Eval("CRC_ID") %>' CommandName="Modify" />
                                                        &nbsp;<asp:LinkButton ID="btnRehabPopupDelete" runat="server"
                                                            CommandArgument='<%# Eval("CRC_ID") %>' CommandName="Remove"
                                                            Text="[Remove]" />
                                                        &nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RehabDate" HeaderText="Case Management Date"
                                                    DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                                                <asp:BoundField DataField="Cost" HeaderText="Cost (£)"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle BackColor="#2459A7" ForeColor="White" />
                                            <RowStyle CssClass="even gradeC" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>
                <asp:HiddenField ID="hidCRC_ID" ViewStateMode="Enabled" runat="server" />

            </ContentTemplate>
        </asp:UpdatePanel>
        <div align="Center">
            <br />
            <asp:Button ID="btnRehabPopupClose" Text="Close" runat="server" />
            <br />
            <br />
        </div>

    </asp:Panel>--%>
    <div id="backgroundPopup1" onclick="closepopup()">
    </div>

    <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="divHomeVisit" class="popupContact" style="height: 260px; text-align: center">
                <table style="height: 100px; width: 100%;">
                    <tr style="background-color: Gray; color: White;">
                        <th colspan="4" style="text-align: center; vertical-align: middle">Home Visit
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 140px">Date of Appointment:
                        </td>
                        <td style="width: 203px">
                            <asp:TextBox ID="txtDateOfAppointmentPop" runat="server" Width="171px" CssClass="Textbox"
                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MskEditExtender12" runat="server" Mask="99/99/9999"
                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateOfAppointmentPop">
                            </cc1:MaskedEditExtender>
                        </td>
                        <td style="width: 150px">Date Report Completed:
                        </td>
                        <td style="width: 203px">
                            <asp:TextBox ID="txtDateReportCompPop" runat="server" Width="171px" CssClass="Textbox"
                                TabIndex="55" onchange="javascript:return validateDate(this);"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MskdEditExtender19" runat="server" Mask="99/99/9999"
                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateReportCompPop">
                            </cc1:MaskedEditExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 140px">Type Of Visit:</td>
                        <td style="width: 203px">
                            <asp:DropDownList ID="ddlTypeOfVisitPop" runat="server" Width="177px" CssClass="Textbox">
                            </asp:DropDownList>




                        </td>
                        <td style="width: 140px">Fee Charged (£):</td>
                        <td style="width: 203px">
                            <asp:TextBox ID="txtFeeChargedPop" runat="server" onkeypress="javascript:return numberOnly(this,event);" Width="171px" CssClass="Textbox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center">
                            <asp:Button runat="server" ID="btnSaveHomeVisit" Text="Save"
                                OnClick="btnSaveHomeVisit_Click" />
                            <asp:Button ID="btnCancelHomeVisit" runat="server" Text="Cancel" OnClientClick="return closepopup();" />
                        </td>
                    </tr>


                </table>
            </div>
            <div id="divTelephone" class="popupContact" style="height: 400px; width: 850px; text-align: center">
                <table style="height: 100px; width: 100%;">
                    <tr style="background-color: Gray; color: White;">
                        <th colspan="4" style="text-align: center; vertical-align: middle">TELEPHONE INTERVENTION
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>Date:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCallDatePop" runat="server" Width="95%" CssClass="Textbox"
                                                    onchange="validateDate(this);DisableDateControls(1);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender10" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCallDatePop">
                                                </cc1:MaskedEditExtender>
                                            </td>
                                            <td style="text-align: right; padding-left: 20px;">Time:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCallTimePop" runat="server" CssClass="Textbox" Width="40px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender11" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                    MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtCallTimePop">
                                                </cc1:MaskedEditExtender>
                                            </td>
                                            <td align="left">hours
                                            </td>
                                            <td>Type of Call:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlTypeofCallPop" runat="server" Width="155px">
                                                    <%-- <asp:ListItem Text="call1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="call2" Value="2"></asp:ListItem>--%>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">Failed Calls:
                                            </td>
                                            <td>
                                                <asp:ListBox ID="lslFailedCallPop" runat="server" Width="97%"></asp:ListBox>
                                            </td>
                                            <td style="text-align: left; vertical-align: top;" colspan="3">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lnkFailedCallPop" runat="server" Text="Add Failed Call"
                                                                Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none;"
                                                                OnClientClick="return AddItem();">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lnkFailedPop" runat="server" Text="Remove Failed Call"
                                                                Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none"
                                                                OnClientClick="javascript:return validateFaildcallPop(1);"
                                                                TabIndex="61">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="vertical-align: top;">Fee Charged (£):</td>
                                            <td style="vertical-align: top;">
                                                <asp:TextBox ID="txtFeeChargedTelPop" runat="server" Width="152px" CssClass="Textbox" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Expected RTW Date(Optimum):
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRTWDateOptimumPop" runat="server" CssClass="Textbox" Width="152px"
                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender12" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateOptimumPop">
                                                </cc1:MaskedEditExtender>
                                            </td>
                                            <td colspan="3">&nbsp;</td>
                                            <td>Expected RTW Date(Maximum):
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRTWDateMaxPop" runat="server" CssClass="Textbox" Width="152px"
                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender13" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateMaxPop">
                                                </cc1:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>Case Management Recommended?
                                            </td>
                                            <td style="text-align: left;">&nbsp;<asp:DropDownList ID="ddlCaseMgmtPop" runat="server">
                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Value="No">No</asp:ListItem>
                                            </asp:DropDownList>

                                            </td>
                                            <td colspan="3">&nbsp;</td>
                                            <td>Next Call Scheduled:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtNxtCallSchdPop" runat="server" CssClass="Textbox" Width="152px"
                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender15" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtNxtCallSchdPop">
                                                </cc1:MaskedEditExtender>
                                            </td>
                                            <%-- </tr>
                                                </table>
                                            </td>--%>
                                        </tr>
                                        <tr>
                                            <td colspan="7" style="text-align: center">
                                                <asp:HiddenField ID="hdnFailedCallList" runat="server" Value="" />
                                                <asp:Button runat="server" ID="btnTelephoneActivity" Text="Save"
                                                    OnClick="btnTelephoneActivitySave_Click" OnClientClick="AddListFailedCall();" />
                                                <asp:Button ID="btnTelephoneCancel" runat="server" Text="Cancel" OnClientClick="return closepopup();" OnClick="btnTelephoneCancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                    <%--   <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td >
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 11%">Date:
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:TextBox ID="txtCallDatePop" runat="server" Width="171px" CssClass="Textbox" 
                                                                onchange="validateDate(this);DisableDateControls(1);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender10" runat="server" Mask="99/99/9999"
                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCallDatePop">
                                                            </cc1:MaskedEditExtender>
                                                        </td>
                                                        <td style="text-align: right; width: 10%">Time:
                                                        </td>
                                                        <td valign="middle">
                                                            <asp:TextBox ID="txtCallTimePop" runat="server" CssClass="Textbox"  Width="40px"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender11" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                                MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtCallTimePop">
                                                            </cc1:MaskedEditExtender>
                                                        </td>
                                                        <td valign="middle" align="left" width="100%">hours
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 80px;">Failed Calls:
                                                        </td>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width: 25%">
                                                                        <asp:ListBox ID="ListBox1" runat="server" Width="60%"></asp:ListBox>
                                                                    </td>

                                                                    <td style="width: 20%; text-align: left">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Add Failed Call" 
                                                                                        Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none;"
                                                                                        CommandArgument="1" CommandName="Add" OnClick="lnkFailedCall_Click">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Remove Failed Call"
                                                                                        Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none"
                                                                                        CommandArgument="1" CommandName="del" OnClientClick="javascript:return validateFaildcall(1);"
                                                                                        OnClick="lnkFailedCall_Click" >
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>

                                                                                    <asp:CheckBox ID="CheckBox1" runat="server" Text="Aborted Call"
                                                                                        AutoPostBack="True" OnCheckedChanged="cbCall1Aborted_CheckedChanged" ValidationGroup="vgCall1" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 20%">Expected RTW Date
                                                        </td>
                                                        <td valign="middle">
                                                            <asp:TextBox ID="txtRTWDateOptimumPop" runat="server" CssClass="Textbox" 
                                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender12" runat="server" Mask="99/99/9999"
                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateOptimumPop">
                                                            </cc1:MaskedEditExtender>
                                                        </td>
                                         
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>Case Management Recommended?
                                                        </td>
                                                        <td style="text-align: left;">&nbsp;<asp:DropDownList ID="DropDownList1" runat="server" >
                                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                            &nbsp;&nbsp; &nbsp;&nbsp;
                                                        </td>
                                                        <td>Next Call Scheduled:
                                                        </td>
                                                        <td>&nbsp;&nbsp;<asp:TextBox ID="txtNxtCallSchdPop" runat="server" Width="171px" CssClass="Textbox"
                                                             onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender13" runat="server" Mask="99/99/9999"
                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtNxtCallSchdPop">
                                                            </cc1:MaskedEditExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divCaseManagementCost" class="popupContact" style="height: 270px; width: 600px; text-align: center">
                <table style="height: 100px; width: 100%;">
                    <tr style="background-color: Gray; color: White;">
                        <th colspan="4" style="text-align: center; vertical-align: middle">CASE MANAGEMENT COST
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <%--    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>   --%>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>Date:
                                    </td>
                                    <td style="width: 203px">
                                        <asp:TextBox ID="txtCaseMgntDate" runat="server" Width="171px" CssClass="Textbox"
                                            onchange="javascript:return validateDate(this);"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender16" runat="server" Mask="99/99/9999"
                                            InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCaseMgntDate">
                                        </cc1:MaskedEditExtender>
                                    </td>
                                    <td>No. of Hours:
                                    </td>
                                    <td style="width: 203px">
                                        <asp:TextBox ID="txtNoofHour" runat="server" Width="171px" CssClass="Textbox" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>

                                    </td>

                                </tr>
                                <tr>
                                    <td>Rate (£):
                                    </td>
                                    <td style="width: 203px">
                                        <asp:TextBox ID="txtRate" runat="server" Width="171px" CssClass="Textbox"
                                            onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>

                                    </td>
                                    <td>Cost (£):
                                    </td>
                                    <td style="width: 203px">
                                        <asp:TextBox ID="txtCaseMgntCost" runat="server" Width="171px" CssClass="Textbox" Enabled="false"
                                            onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <asp:Button runat="server" ID="btnCaseMgntCostSave" Text="Save" OnClick="btnCaseMgntCostSave_Click" />
                                        <asp:Button ID="btnCaseMgntcancel" runat="server" Text="Cancel" OnClientClick="return closepopup();" />
                                    </td>
                                </tr>
                            </table>
                            <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divRehabilitation" class="popupContact" style="height: 300px; width: 600px; text-align: center">
                <table style="height: 100px; width: 100%;">
                    <tr style="background-color: Gray; color: White;">
                        <th style="text-align: center; vertical-align: middle">FUNDED TREATMENT
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>Type:
                                    </td>
                                    <td style="width: 203px">
                                        <asp:DropDownList ID="ddlRehabType" runat="server" Width="175px" CssClass="Textbox">
                                        </asp:DropDownList>

                                    </td>
                                    <td>Provider:
                                    </td>
                                    <td style="width: 203px">
                                        <asp:DropDownList ID="ddlRehabProvider" runat="server" Width="175px" CssClass="Textbox">
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 140px">Date Instructed:
                                    </td>
                                    <td style="width: 203px">
                                        <asp:TextBox ID="txtDateInstructed" runat="server" Width="171px" CssClass="Textbox"
                                            onchange="javascript:return validateDate(this);"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="msktxtDateInstructed" runat="server" Mask="99/99/9999"
                                            InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateInstructed">
                                        </cc1:MaskedEditExtender>
                                    </td>
                                    <td style="width: 150px">Date Reported:
                                    </td>
                                    <td style="width: 203px">
                                        <asp:TextBox ID="txtDateReported" runat="server" Width="171px" CssClass="Textbox"
                                            TabIndex="55" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="msktxtDateReported" runat="server" Mask="99/99/9999"
                                            InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateReported">
                                        </cc1:MaskedEditExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fee Charged (£):</td>
                                    <td style="width: 203px">
                                        <asp:TextBox ID="txtRehabFeeCharged" runat="server" onkeypress="javascript:return numberOnly(this,event);" Width="171px" CssClass="Textbox"></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <asp:Button runat="server" ID="btnRehabilitationSave" Text="Save"
                                            OnClick="btnRehabilitationSave_Click" />
                                        <asp:Button ID="btnRehabilitationCancel" runat="server" Text="Cancel" OnClientClick="return closepopup();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divServiceRequired" class="popupContact" style="height: auto; overflow: auto; width: 600px; text-align: center; overflow-y: auto">
                <table style="height: 100px; width: 100%;">
                    <tr style="background-color: Gray; color: White;">
                        <th style="text-align: center; vertical-align: middle">SERVICE REQUIRED
                        </th>
                    </tr>
                    <tr style="height: 15px;">
                        <td>
                            <%-- <br />--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="padding-left: 80px;">
                            <asp:Label ID="lblServiceRequired" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnServiceCancel" runat="server" Text="OK" OnClientClick="return closepopup();" /></td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="hdnVisitId" runat="server" Value="" />
            <asp:HiddenField ID="hdnTelephoneActivityId" runat="server" Value="" />
            <asp:HiddenField ID="hdbCaseManagementCostId" runat="server" Value="" />
            <asp:HiddenField ID="hdnServiceRequired" runat="server" Value="" />
            <asp:HiddenField ID="hdnRehabilitationId" runat="server" Value="" />

            <asp:HiddenField ID="hdnpostbackservicerequired" runat="server" Value="" />
        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>
