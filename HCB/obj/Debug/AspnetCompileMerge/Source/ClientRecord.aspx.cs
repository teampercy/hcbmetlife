﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using System.Data.OleDb;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.IO;
using System.Web.Services;
using System.Collections;
using System.Web.Script.Serialization;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class ClientRecord : System.Web.UI.Page
    {

        #region Variables
        static int failedcallcount = 0, failedcallcountPop=0;
        private bool ExistingCall1Aborted
        {
            get
            {
                if (ViewState["ExistingCall1Aborted"] == null)
                {
                    ViewState["ExistingCall1Aborted"] = false;
                }
                return (bool)ViewState["ExistingCall1Aborted"];
            }
            set
            {
                ViewState["ExistingCall1Aborted"] = value;
            }
        } //Keep track of existing call aborted status so that an email can be triggered incase the status of call aborted changes
        private bool ExistingCall2Aborted
        {
            get
            {
                if (ViewState["ExistingCall2Aborted"] == null)
                {
                    ViewState["ExistingCall2Aborted"] = false;
                }
                return (bool)ViewState["ExistingCall2Aborted"];
            }
            set
            {
                ViewState["ExistingCall2Aborted"] = value;
            }
        } //Keep track of existing call aborted status so that an email can be triggered incase the status of call aborted changes
        private bool ExistingCall3Aborted
        {
            get
            {
                if (ViewState["ExistingCall3Aborted"] == null)
                {
                    ViewState["ExistingCall3Aborted"] = false;
                }
                return (bool)ViewState["ExistingCall3Aborted"];
            }
            set
            {
                ViewState["ExistingCall3Aborted"] = value;
            }
        } //Keep track of existing call aborted status so that an email can be triggered incase the status of call aborted changes

        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
              //  hdnpostbackservicerequired.Value = hdnServiceRequired.Value;
            lblMsg.Text = AntiXss.HtmlEncode(string.Empty);
            txtRate.Attributes.Add("onchange", "javascript:CalculateCaseCost()");
            txtNoofHour.Attributes.Add("onchange", "javascript:CalculateCaseCost()");
           // cbCall1Aborted.Attributes.Add("onclick", "javascript:if(Call1AbortedChanged()) __doPostBack('" + cbCall1Aborted.ClientID + "',''); else return false;");
            //cbCall2Aborted.Attributes.Add("onclick", "javascript:if(Call2AbortedChanged()) __doPostBack('" + cbCall2Aborted.ClientID + "',''); else return false;");
            //cbCall3Aborted.Attributes.Add("onclick", "javascript:if(Call3AbortedChanged()) __doPostBack('" + cbCall3Aborted.ClientID + "',''); else return false;");

            if (!Page.IsPostBack)
            {
                ddlCaseMgntapproved.Enabled = false;
                txtCaseMgntFundingLimit.Enabled = false;
                txtCurrentCaseTotal.Enabled = false;
                lnkCaseMgntCost.Enabled = false;
                grdCaseMgntCost.Enabled = false;
                btnCaseMgntSave.Enabled = false;

                
    
                #region FillDropDowns


                GetClaimAssessor();
                FillClaimManager();
                FillServiceReq();
                FillEmployeeStatus();
               // FillCorpPartner();
                FillWaitingPeriod();
                FillProductType();
                FillIllnessInjury();
                FillClaimCloseReason();
                FillReasonClosed();
                FillAssessorTeam();
                FillBrand();
                FillTypeOfVisit();
                FillIncapacityDefinition();
                FillTypeOfCall();
                FillRehabilitationType();
                FillRehabilitationProvider();
                FillSchemeName();
               // FillSchemeNumber();
                #endregion
                //pnlHomeVisit.Visible = false;
                //pnlGRIDHomeVisit.Visible = false;
                Session["NewHCBRef"] = "";

                if (Request.QueryString != null)
                {
                    if (Request.QueryString["mode"] != null)
                    {
                        if (Request.QueryString["mode"] == "create")
                        {
                            Clear();
                            pnlTelephoneIntervention.Enabled = false;
                            lnkAddTelephone.Enabled = false;
                            lnkAddRehabilitation.Enabled = false;
                            Session["ClientHcbRef"] = null;
                        }
                    }
                }
                if (Request.QueryString["HcbReportId"] != null)
                {
                    Session["ClientHcbRef"] = Request.QueryString["HcbReportId"];
                }
                if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "0")
                    {
                        FillClientDetails();
                        InsertAmendment(Session["ClientHcbRef"].ToString());
                    }
                    else
                    {
                        Clear();
                    }
                }
                else
                {
                    tbGRIDHomeVisit.Visible = false;
                    Clear();
                }

                FillMetLifeStaticValue();
                try
                {
                    if (Session["UserType"] != null)
                    {
                        if (Session["UserType"].ToString() == "Admin")
                        {
                            DisableNonAdminControls();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
                        }
                        else if (Session["UserType"].ToString() == "Inco")
                        {
                            DisableNonIncoControls();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
                        }
                        else if (Session["UserType"].ToString() == "Nurse")
                        {
                            DisableNonNurseControls();
                        }

                        hidLogUserType.Value = Session["UserType"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/Login.aspx");
                }
                if (lnkAddTelephone.Enabled)
                {
                    string  strTelephone1 = "#divTelephone";

                    //lnkAdd.OnClientClick = "return PopupAddNew('" + homevisit + "')";
                    lnkAddTelephone.OnClientClick = "return PopupAddNew('" + strTelephone1 + "')";
                }
                
                if (lnkCaseMgntCost.Enabled)
                {
                    string strCaseMgntCost = "#divCaseManagementCost";
                    lnkCaseMgntCost.OnClientClick = "return PopupAddNew('" + strCaseMgntCost + "')";
                }
                if (lnkAddRehabilitation.Enabled)
                {
                    string strRehabilitation = "#divRehabilitation";
                    lnkAddRehabilitation.OnClientClick = "return PopupAddNew('" + strRehabilitation + "')";
                }

            }
            }
            catch (Exception ex)
            {

                lblMsg.Text=AntiXss.HtmlEncode("Invalid Text entered");
            }
            
        }

        protected void ddlClaimAssessor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlClaimAssessor.SelectedIndex > 0)
                {
                    Users usr = new Users();
                    usr.UserId = Convert.ToInt32(ddlClaimAssessor.SelectedValue);
                    DataSet ds = usr.GetIncoDet();
                    DataTable dt = new DataTable();

                    if (ds != null)
                    {
                        dt = ds.Tables["Assessor"];

                        txtClaimHandlerTelephone.Text = dt.Rows[0]["TelephoneNumber"].ToString();
                        txtClaimHandlerEmail.Text = dt.Rows[0]["EmailAddress"].ToString();
                    }
                }
                else
                {
                    txtClaimHandlerTelephone.Text = "";
                    txtClaimHandlerEmail.Text = "";
                }
            }
            catch (Exception ex)
            {
                txtClaimHandlerTelephone.Text = "";
                txtClaimHandlerEmail.Text = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string query = "";
                OleDbCommand cmd;
                int ID;
                string AdminQuery = "";
                string AdminEmail = "";
                string CaseMngrEmail = "";
                bool Exists = false;

                ClientInfo objClientInfo = new ClientInfo();
                HCBInsurance insure = new HCBInsurance();
                //objClientInfo.IncoID = 1;

                try
                {
                    if (Session["ClientHcbRef"] != null)
                    {
                        if (Session["ClientHcbRef"].ToString() != "")
                        {
                        }
                    }
                    else
                    {
                        if (CheckExistingClient())
                        {
                            Exists = true;
                            //Clear();
                        }
                        else
                        {
                            Exists = false;
                        }
                    }

                    conn.Open();

                    #region ClientRecord




                    if (ddlCaseMngr.SelectedIndex > 0)
                    {
                        objClientInfo.HCBNurseID = Convert.ToInt32(ddlCaseMngr.SelectedValue);
                        objClientInfo.NurseName = ddlCaseMngr.SelectedItem.Text;
                        DataSet dsquery = new DataSet();
                        dsquery = objClientInfo.GetUsersEmailAddress(Convert.ToInt32(ddlCaseMngr.SelectedValue));
                        if (dsquery != null && dsquery.Tables.Count > 0)
                        {
                            if (dsquery.Tables[0].Rows.Count > 0)
                            {
                                CaseMngrEmail = dsquery.Tables[0].Rows[0]["EmailAddress"].ToString();
                            }
                        }
                        //query = "select EmailAddress from Users where UserID = " + ddlCaseMngr.SelectedValue;
                        //OleDbCommand cmdCaseMngr = new OleDbCommand(query, conn);
                        //cmdCaseMngr.CommandText = query;
                        //cmdCaseMngr.CommandType = CommandType.Text;
                        //CaseMngrEmail = cmdCaseMngr.ExecuteScalar().ToString();

                    }
                    else
                    {
                        objClientInfo.HCBNurseID = 0;
                        objClientInfo.NurseName = "";
                    }

                    try
                    {
                        if (ViewState["ClientIncoID"] != null)
                        {
                            if (ViewState["ClientIncoID"].ToString() != "")
                            {
                                objClientInfo.IncoID = Convert.ToInt32(ViewState["ClientIncoID"]);
                            }

                            if (ViewState["ClientIncoName"] != null)
                            {
                                if (ViewState["ClientIncoName"].ToString() != "")
                                {
                                    objClientInfo.IncoName = ViewState["ClientIncoName"].ToString();
                                }
                            }
                        }
                        else if (Session["LoggedInUserId"] != null)
                        {
                            if (Session["LoggedInUserId"].ToString() != "")
                            {
                                objClientInfo.IncoID = Convert.ToInt32(Session["LoggedInUserId"]);

                                if (Session["LoggedInUserName"] != null)
                                {
                                    if (Session["LoggedInUserName"].ToString() != "")
                                    {
                                        objClientInfo.IncoName = Session["LoggedInUserName"].ToString();
                                    }
                                    else
                                    {
                                        objClientInfo.IncoName = string.Empty;
                                    }
                                }
                                else
                                {
                                    objClientInfo.IncoName = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Login.aspx");
                        }
                    }
                    catch (Exception ex)
                    {
                        objClientInfo.IncoID = 0;
                        objClientInfo.IncoName = "";
                    }

                    if (ddlClaimAssessor.SelectedIndex > 0)
                    {
                        objClientInfo.ClaimHandlerID = Convert.ToInt32(ddlClaimAssessor.SelectedValue);
                        objClientInfo.ClaimHandlerName = ddlClaimAssessor.SelectedItem.Text;
                    }
                    else
                    {
                        objClientInfo.ClaimHandlerID = 0;
                        objClientInfo.ClaimHandlerName = "";
                    }

                    objClientInfo.ClaimHandlerTelephone = txtClaimHandlerTelephone.Text;
                    objClientInfo.ClaimHandlerEmail = txtClaimHandlerEmail.Text;

                    if (ddlAssessorTeam.SelectedIndex > 0)
                    {

                        if (ddlAssessorTeam.SelectedIndex == 11)
                        {
                            objClientInfo.AssessorTeamOther = txtTeamOther.Text;
                        }
                        else
                        {
                            objClientInfo.AssessorTeamOther = "";
                        }
                            objClientInfo.AssessorTeamID = Convert.ToInt32(ddlAssessorTeam.SelectedValue);
                        objClientInfo.AssessorTeamName = ddlAssessorTeam.SelectedItem.Text;
                    }
                    else
                    {
                        objClientInfo.AssessorTeamID = 0;
                        objClientInfo.AssessorTeamName = "";
                    }
                    if (ddlSchemeName.SelectedIndex > 0)
                    {
                        objClientInfo.SchemeNameId = Convert.ToInt32(ddlSchemeName.SelectedValue);                        
                    }
                    else
                    {
                        objClientInfo.SchemeNameId = 0;
                       
                    }
                    objClientInfo.SchemeNumber = txtSchemeNumber.Text.Trim();
                    //if (ddlMetSchemeNumber.SelectedIndex > 0)
                    //{
                    //    objClientInfo.SchemeNumberId = Convert.ToInt32(ddlMetSchemeNumber.SelectedValue);
                    //}
                    //else
                    //{
                    //    objClientInfo.SchemeNumberId = 0;

                    //}
                    objClientInfo.AssessorTeamTelephone = txtTeamTelephone.Text;
                    objClientInfo.AssessorTeamEmail = txtTeamEmail.Text;

                    //objClientInfo.Salutation = ddlSalutation.SelectedValue;
                    objClientInfo.ClientForenames = txtClientFName.Text;
                    objClientInfo.ClientSurname = txtClientSurname.Text;

                    if (txtClientDOB.Text != "")
                    {
                        if (txtClientDOB.Text != "00/00/0000" || txtClientDOB.Text != "01/01/1900")
                        {
                            objClientInfo.ClientDOB = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtClientDOB.Text));
                        }
                        else
                        {
                            objClientInfo.ClientDOB = "";
                        }
                    }
                    else
                    {
                        objClientInfo.ClientDOB = "";
                    }

                    //objClientInfo.ClientAddress = txtClientAddress.Text;
                    
                    objClientInfo.ClientAddress1=txtAddress1.Text;
                    objClientInfo.ClientAddress2 = txtAddress2.Text;
                    objClientInfo.ClientCity = txtCity.Text;
                    objClientInfo.ClientCounty = txtCounty.Text;
                    objClientInfo.ClientWorkTelephone = txtClientWorkTelephone.Text;
                    objClientInfo.ClientMobileTelephone = txtClientMobileTelephone.Text;
                    objClientInfo.ClientPostCode = txtClientPostCode.Text;
                    objClientInfo.ClientOccupation = txtOccupation.Text;


                    
                    //foreach (ListItem listItem in lstServiceReq.Items)
                    //{
                    //    if (listItem.Selected == true)
                    //    {
                    //        objClientInfo.ClientServiceReq += listItem.Value + ",";
                    //    }
                    //}
                    if(hdnServiceRequired.Value!="")
                    {
                        if (hdnServiceRequired.Value.Contains("multiselect"))
                        {
                            objClientInfo.ClientServiceReq = hdnServiceRequired.Value.Substring(16);
                        }
                        else
                        {
                            objClientInfo.ClientServiceReq = hdnServiceRequired.Value;
                        }

                        string[] a = objClientInfo.ClientServiceReq.Split(',');
                        for (int i = 0; i < a.Length; i++)
                        {
                            if (a[i] == "11595")
                            {
                                objClientInfo.ServiceRequiredOther = txtservicerequiredother.Text;
                            }
                        }
                    }
                   

                    if (ddlGender.SelectedValue != "0")
                    {
                        objClientInfo.Gender = Convert.ToString(ddlGender.SelectedValue);
                    }
                    objClientInfo.EmployerContact = txtEmployerContract.Text;
                    objClientInfo.EmployerName = txtEmployerName.Text;

                   

                    if (txtIncapacityDay.Text != "")
                    {
                        if (txtIncapacityDay.Text != "00/00/0000" || txtIncapacityDay.Text != "01/01/1900")
                        {
                            objClientInfo.IncapacityDay = string.Format("{0: dd/MMM/yyyy}", Convert.ToDateTime(txtIncapacityDay.Text));
                        }
                        else
                        {
                            objClientInfo.IncapacityDay = "";
                        }
                    }
                    else
                    {
                        objClientInfo.IncapacityDay = "";
                    }

                    if (ddlInjury.SelectedIndex > 0)
                    {
                        if (ddlInjury.SelectedIndex == 8)
                        {
                            objClientInfo.InjuryOther = txtillnessinjuryother.Text;

                        }

                        objClientInfo.IllnessInjury = Convert.ToInt32(ddlInjury.SelectedValue);
                    }
                    else
                    {
                        objClientInfo.IllnessInjury = 0;
                    }

                    objClientInfo.DisabilityType = txtDisabilityType.Text;
                    objClientInfo.ICD9Code = txtICD9Code.Text;

                    if (txtClaimCloseDate.Text != "")
                    {
                        if (txtClaimCloseDate.Text != "00/00/0000" || txtClaimCloseDate.Text != "01/01/1900")
                        {
                            objClientInfo.ClaimCloseDate = string.Format("{0: dd/MMM/yyyy}", Convert.ToDateTime(txtClaimCloseDate.Text));
                        }
                        else
                        {
                            objClientInfo.ClaimCloseDate = "";
                        }
                    }
                    else
                    {
                        objClientInfo.ClaimCloseDate = "";
                    }


                    if (txtclaimantfirstcontacted.Text != "")
                    {
                        if (txtclaimantfirstcontacted.Text != "00/00/0000" || txtclaimantfirstcontacted.Text != "01/01/1900")
                        {
                            objClientInfo.DateClaimantfirstContact = string.Format("{0: dd/MMM/yyyy}", Convert.ToDateTime(txtclaimantfirstcontacted.Text));
                        }
                        else
                        {
                            objClientInfo.DateClaimantfirstContact = "";
                        }
                    }
                    else
                    {
                        objClientInfo.DateClaimantfirstContact = "";
                    }

                    if (ddlClaimClosedReason.SelectedIndex > 0)
                    {
                        if (ddlClaimClosedReason.SelectedIndex == 6)
                        {
                            objClientInfo.ClaimReasonOther = txtclaimreasonother.Text;
                        }
                        objClientInfo.ClaimCloseReason = Convert.ToInt32(ddlClaimClosedReason.SelectedValue);
                    }
                    else
                    {
                        objClientInfo.ClaimCloseReason = 0;
                    }
                    //objClientInfo.ClaimReasonClosed = txtClaimCloseReason.Text.Trim();

                    objClientInfo.ClaimDetails = txtClaimDetails.Text;
                    if (!String.IsNullOrEmpty(txtHCBReceivedDate.Text))
                    {
                        //objClientInfo.HCBReceivedDate = string.Format("{0:dd-MMM-yyyy}", DateTime.Today.Date);
                        objClientInfo.HCBReceivedDate = string.Format("{0:dd-MMM-yyyy}", Convert.ToDateTime(txtHCBReceivedDate.Text.Trim()));
                    }
                    else
                    {
                        objClientInfo.HCBReceivedDate = "";
                            
                    }

                    if (txtDateSentToNurse.Text != "")
                    {
                        //objClientInfo.DateSentToNurse = txtDateSentToNurse.Text;
                        objClientInfo.DateSentToNurse = string.Format("{0:dd-MMM-yyyy}", Convert.ToDateTime(txtDateSentToNurse.Text.Trim()));
                    }
                    else
                    {
                        objClientInfo.DateSentToNurse = "";
                    }

                    if (txtCaseClosed.Text != "")
                    {
                        objClientInfo.DateClosed = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCaseClosed.Text));

                    }
                    else
                    {
                        objClientInfo.DateClosed = "";
                    }

                    if (txtCaseCancelled.Text != "")
                    {
                        objClientInfo.DateCancelled = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCaseCancelled.Text));

                    }
                    else
                    {
                        objClientInfo.DateCancelled = "";
                    }

                    if (ddlReasonCancel.SelectedIndex > 0)
                    {
                        objClientInfo.CancellationReason = Convert.ToInt32(ddlReasonCancel.SelectedValue);
                    }
                    else
                    {
                        objClientInfo.CancellationReason = 0;
                    }

                    if (txtFeeCharged.Text != "")
                    {
                        objClientInfo.FeeCharged = Convert.ToDouble(txtFeeCharged.Text);
                    }
                    else
                    {
                        objClientInfo.FeeCharged = 0;
                    }

                    if (txtRehabcost.Text != "")
                    {
                        objClientInfo.RehabCost = Convert.ToDouble(txtRehabcost.Text);
                    }
                    else
                    {
                        objClientInfo.RehabCost = 0;
                    }



                    if (Session["ClientHcbRef"] != null)
                    {
                        //string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
                        objClientInfo.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());

                        updateAmendment();
                        DataSet dschk = new DataSet();
                        dschk = objClientInfo.CheckServiceReqAlter();
                        if (dschk != null)
                        {
                            if (dschk.Tables[0].Rows[0]["IsEdit"].ToString() == "0")
                            {
                                bool IsCheckedEmail = chkSendEmail.Checked;
                                chkSendEmail.Checked = true;                                
                                createNote(AntiXss.HtmlEncode("Service Required has changed."), false);
                                chkSendEmail.Checked = IsCheckedEmail;
                                //chkSendEmail.Checked = true;
                            }
                        }

                    }
                    else if (txtHCBReference.Text != "")
                    {
                        objClientInfo.HCBReference = Convert.ToInt32(txtHCBReference.Text);
                    }
                    else
                    {
                        objClientInfo.HCBReference = 0;
                    }
                    if (chkFormUploaded.Checked)
                    {
                        objClientInfo.IsDocumentUpload = true;
                    }
                    else
                    {
                        objClientInfo.IsDocumentUpload = false;
                    }
                    

                    ID = (int)objClientInfo.Save();
                    if(objClientInfo.ClientServiceReq!="")
                    {
                        string[] servicerequiredarray = objClientInfo.ClientServiceReq.Split(',');
                        DataSet dsServiceRequired = new DataSet();
                        lblServiceRequired.Text = "<b>Service Required selected are:</b></br>";
                        for (int i = 0; i < servicerequiredarray.Length; i++)
                        {
                            dsServiceRequired = objClientInfo.GetDetailServiceRequired(Convert.ToInt32(servicerequiredarray[i]));
                                    if (dsServiceRequired != null && dsServiceRequired.Tables.Count > 0)
                                    {
                                        lblServiceRequired.Text += AntiXss.HtmlEncode(dsServiceRequired.Tables[0].Rows[0]["Name"].ToString())+"</br>";
                                    }
                        }                    
                            
                    }
                    else
                    {
                                hdnServiceRequired.Value = "";
                                lblServiceRequired.Text = "<b>No Service Required Selected.</b>";
                    }
                    if (Session["ClientHcbRef"] != null)
                    {
                        updateAmendment();
                    }
                    else
                    {
                        InsertAmendment(ID.ToString());
                        #region Documents
                        string strRefFolder = "", strPath = "";
                        strPath = Server.MapPath(ConfigurationManager.AppSettings["DocumentPath"].ToString());// ConfigurationManager.AppSettings["DocumentPath"].ToString() + "";
                        if (strPath != "")
                        {
                            //strPath = Server.MapPath(strPath);
                            if (ViewState["reffolder"] != null)
                                strRefFolder = AntiXss.HtmlEncode((string)ViewState["reffolder"]);
                            if (AntiXss.HtmlEncode(strRefFolder.Trim()) != "")
                            {
                                if (!Directory.Exists(AntiXss.HtmlEncode(strPath) + "\\" + ID))
                                {
                                    Directory.CreateDirectory(AntiXss.HtmlEncode(strPath) + "\\" + ID);
                                }
                                string[] strfilenames = Directory.GetFiles(AntiXss.HtmlEncode(strPath) + "\\" + AntiXss.HtmlEncode(strRefFolder));
                                foreach (string str in strfilenames)//insert details and move file
                                {
                                    string filename = Path.GetFileName(str);
                                    if (File.Exists(AntiXss.HtmlEncode(str)))
                                    {
                                        File.Move(AntiXss.HtmlEncode(str), AntiXss.HtmlEncode(strPath) + "\\" + ID + "\\" + AntiXss.HtmlEncode(filename));
                                        InsertDocDetails(ID.ToString(), AntiXss.HtmlEncode(filename));
                                    }
                                }
                                if (Directory.GetFiles(AntiXss.HtmlEncode(strPath) + "\\" + AntiXss.HtmlEncode(strRefFolder)).Count() < 1)
                                {
                                    Directory.Delete(AntiXss.HtmlEncode(strPath) + "\\" + AntiXss.HtmlEncode(strRefFolder));//del folder
                                    ViewState["reffolder"] = null;
                                    ViewState["filelist"] = null;

                                }
                                else
                                {
                                    //error
                                }
                            }
                        }
                        #endregion
                    }

                    #endregion

                    if (ID != 0)
                    {
                        if (Session["ClientHcbRef"] != null)
                        {
                            if (Session["ClientHcbRef"].ToString() != "")
                            {
                            }
                        }
                        else
                        {
                            Session["NewHCBRef"] = ID;
                        }
                        btnSaveNote.Enabled = true;
                        chkSendEmail.Enabled = true;

                        #region DisplayHCBRef

                        if (ID.ToString().Length >= 5)
                        {
                            txtHCBReference.Text = "MTL" + ID.ToString();
                        }
                        else if (ID.ToString().Length == 4)
                        {
                            txtHCBReference.Text = "MTL0" + ID.ToString();
                        }
                        else if (ID.ToString().Length == 3)
                        {
                            txtHCBReference.Text = "MTL00" + ID.ToString();
                        }
                        else if (ID.ToString().Length == 2)
                        {
                            txtHCBReference.Text = "MTL000" + ID.ToString();
                        }
                        else if (ID.ToString().Length == 1)
                        {
                            txtHCBReference.Text = "MTL0000" + ID.ToString();
                        }

                        #endregion

                        #region HCBDuration

                        //HCBFeeCharged FeeChargeObj = new HCBFeeCharged();
                        //DataSet dsFee = new DataSet();

                        //dsFee = FeeChargeObj.GetFeeCharged();

                        //if (Session["UserType"] != null)
                        //{
                        //    if (Session["UserType"].ToString() == "Nurse")
                        //    {

                        //        HCBDuration hcbDuration = new HCBDuration();

                        //        if (hidDurationID.Value != "")
                        //        {
                        //            hcbDuration.DurationID = Convert.ToInt32(hidDurationID.Value);
                        //            hcbDuration.HCBReference = ID;
                        //        }
                        //        else
                        //        {
                        //            hcbDuration.DurationID = 0;
                        //            hcbDuration.HCBReference = ID;
                        //        }

                        //        if (txtAppointmentDate.Text != "")
                        //        {
                        //            if (txtAppointmentDate.Text != "01/01/1900")
                        //            {
                        //                if (txtAppointmentDate.Text != "1/1/1900 12:00:00 AM")
                        //                {
                        //                    hcbDuration.AppointmentDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtAppointmentDate.Text));
                        //                }
                        //                else
                        //                {
                        //                    hcbDuration.AppointmentDate = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                hcbDuration.AppointmentDate = "";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            hcbDuration.AppointmentDate = "";
                        //        }

                        //        if (txtDateReportComp.Text != "")
                        //        {
                        //            if (txtDateReportComp.Text != "01/01/1900")
                        //            {
                        //                if (txtDateReportComp.Text != "1/1/1900 12:00:00 AM")
                        //                {
                        //                    hcbDuration.ReportCompDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateReportComp.Text));
                        //                }
                        //                else
                        //                {
                        //                    hcbDuration.ReportCompDate = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                hcbDuration.ReportCompDate = "";
                        //            }

                        //            if (hidHomeFee.Value == "" || hidHomeFee.Value == "0")
                        //            {
                        //                if (dsFee != null)
                        //                {
                        //                    if (dsFee.Tables[0].Rows.Count > 0)
                        //                    {
                        //                        hcbDuration.HomeVisitCurrency = Convert.ToDouble(dsFee.Tables[0].Rows[0]["HomeVisitCurrency"].ToString());
                        //                    }
                        //                }
                        //            }
                        //            else
                        //            {
                        //                hcbDuration.HomeVisitCurrency = Convert.ToDouble(hidHomeFee.Value);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            hcbDuration.ReportCompDate = "";
                        //        }

                        //        if (txtCall1Date.Text != "")
                        //        {
                        //            if (txtCall1Date.Text != "01/01/1900")
                        //            {
                        //                if (txtCall1Date.Text != "1/1/1900 12:00:00 AM")
                        //                {
                        //                    hcbDuration.Call1Date = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCall1Date.Text));
                        //                }
                        //                else
                        //                {
                        //                    hcbDuration.Call1Date = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                hcbDuration.Call1Date = "";
                        //            }

                                  
                        //            if (dsFee != null)
                        //            {
                        //                if (dsFee.Tables[0].Rows.Count > 0)
                        //                {
                        //                    hcbDuration.Call1Fee = Convert.ToDouble(dsFee.Tables[0].Rows[0]["Call1Fee"].ToString());
                        //                }
                        //            }
                                  
                        //            if (cbCall1Aborted.Checked == true)
                        //            {
                        //                hcbDuration.Call1Aborted = true;
                                       

                        //                if (dsFee != null)
                        //                {
                        //                    if (dsFee.Tables[0].Rows.Count > 0)
                        //                    {
                        //                        hcbDuration.Call1Fee = Convert.ToDouble(dsFee.Tables[0].Rows[0]["CallAbortedFee"].ToString());
                        //                    }
                        //                }
                        //            }


                        //        }
                        //        else
                        //        {
                        //            hcbDuration.Call1Date = "";
                        //        }

                        //        if (txtCall1Time.Text != "")
                        //        {
                        //            //if (txtCall2Time.Text == "__:__")
                        //            //{
                        //            //    hcbDuration.Call2Time = "";
                        //            //}
                        //            //else
                        //            //{
                        //            hcbDuration.Call1Time = txtCall1Time.Text;
                        //            //}
                        //        }

                              

                        //        if (txtRTWDateOptimum.Text != "")
                        //        {
                        //            if (txtRTWDateOptimum.Text != "01/01/1900")
                        //            {
                        //                if (txtRTWDateOptimum.Text != "1/1/1900 12:00:00 AM")
                        //                {
                        //                    hcbDuration.Call1ExpectRTWDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtRTWDateOptimum.Text));
                        //                }
                        //                else
                        //                {
                        //                    hcbDuration.Call1ExpectRTWDate = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                hcbDuration.Call1ExpectRTWDate = "";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            hcbDuration.Call1ExpectRTWDate = "";
                        //        }

                        //        if (ddlCaseMngtRecom.SelectedIndex > 0)
                        //        {
                        //            hcbDuration.Call1CaseMngtRecom = ddlCaseMngtRecom.SelectedItem.Text;
                        //        }

                        //        if (txtNxtCallSchd.Text != "")
                        //        {
                        //            if (txtNxtCallSchd.Text != "01/01/1900")
                        //            {
                        //                if (txtNxtCallSchd.Text != "1/1/1900 12:00:00 AM")
                        //                {
                        //                    hcbDuration.Call1NxtSchedule = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtNxtCallSchd.Text));
                        //                }
                        //                else
                        //                {
                        //                    hcbDuration.Call1NxtSchedule = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                hcbDuration.Call1NxtSchedule = "";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            hcbDuration.Call1NxtSchedule = "";
                        //        }

                        //        //if (txtCall2Date.Text != "")
                        //        //{
                        //        //    if (txtCall2Date.Text != "01/01/1900")
                        //        //    {
                        //        //        if (txtCall2Date.Text != "1/1/1900 12:00:00 AM")
                        //        //        {
                        //        //            hcbDuration.Call2Date = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCall2Date.Text));
                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            hcbDuration.Call2Date = "";
                        //        //        }
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        hcbDuration.Call2Date = "";
                        //        //    }

                        //        //    //if (hidCall2Fee.Value == "" || hidCall2Fee.Value == "0")
                        //        //    //{
                        //        //    if (dsFee != null)
                        //        //    {
                        //        //        if (dsFee.Tables[0].Rows.Count > 0)
                        //        //        {
                        //        //            hcbDuration.Call2Fee = Convert.ToDouble(dsFee.Tables[0].Rows[0]["Call2Fee"].ToString());
                        //        //        }
                        //        //    }
                        //        //    //}
                        //        //    //else
                        //        //    //{
                        //        //    //    hcbDuration.Call2Fee = Convert.ToDouble(hidCall2Fee.Value);
                        //        //    //}

                        //        //    if (cbCall2Aborted.Checked == true)
                        //        //    {
                        //        //        hcbDuration.Call2Aborted = true;
                        //        //        //cbCall2Aborted.Enabled = false;//SG

                        //        //        if (dsFee != null)
                        //        //        {
                        //        //            if (dsFee.Tables[0].Rows.Count > 0)
                        //        //            {
                        //        //                hcbDuration.Call2Fee = Convert.ToDouble(dsFee.Tables[0].Rows[0]["CallAbortedFee"].ToString());
                        //        //            }
                        //        //        }
                        //        //    }


                        //        //}
                        //        //else
                        //        //{
                        //        //    hcbDuration.Call2Date = "";
                        //        //}

                        //        //if (txtCall2Time.Text != "")
                        //        //{
                        //        //    if (txtCall2Time.Text == "__:__")
                        //        //    {
                        //        //        hcbDuration.Call2Time = "";
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        hcbDuration.Call2Time = txtCall2Time.Text;
                        //        //    }
                        //        //}

                        //        //if (txtCall2FailCalls.Text != "")
                        //        //{
                        //        //    hcbDuration.Call2Failed = txtCall2FailCalls.Text;
                        //        //}

                        //        //if (txtCall2RTWDateOpt.Text != "")
                        //        //{
                        //        //    if (txtCall2Date.Text != "01/01/1900")
                        //        //    {
                        //        //        if (txtCall2Date.Text != "1/1/1900 12:00:00 AM")
                        //        //        {
                        //        //            hcbDuration.Call2ExpectRTWDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCall2Date.Text));
                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            hcbDuration.Call2ExpectRTWDate = "";
                        //        //        }
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        hcbDuration.Call2ExpectRTWDate = "";
                        //        //    }
                        //        //}
                        //        //else
                        //        //{
                        //        //    hcbDuration.Call2ExpectRTWDate = "";
                        //        //}

                        //        //if (ddlCaseMngtRecom2.SelectedIndex > 0)
                        //        //{
                        //        //    hcbDuration.Call2CasemngtRecom = ddlCaseMngtRecom2.SelectedItem.Text;
                        //        //}

                        //        //if (ddlRTWReal.SelectedIndex > 0)
                        //        //{
                        //        //    hcbDuration.Call2OriginalRTW = ddlRTWReal.SelectedItem.Text;
                        //        //}

                        //        //if (txtEnvisagedRTWDate.Text != "")
                        //        //{
                        //        //    if (txtEnvisagedRTWDate.Text != "01/01/1900")
                        //        //    {
                        //        //        if (txtEnvisagedRTWDate.Text != "1/1/1900 12:00:00 AM")
                        //        //        {
                        //        //            hcbDuration.Call2EnvisagedDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtEnvisagedRTWDate.Text));
                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            hcbDuration.Call2EnvisagedDate = "";
                        //        //        }
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        hcbDuration.Call2EnvisagedDate = "";
                        //        //    }
                        //        //}
                        //        //else
                        //        //{
                        //        //    hcbDuration.Call2EnvisagedDate = "";
                        //        //}

                        //        //if (txtCall2NxtSchDate.Text != "")
                        //        //{
                        //        //    if (txtCall2NxtSchDate.Text != "01/01/1900")
                        //        //    {
                        //        //        if (txtCall2NxtSchDate.Text != "1/1/1900 12:00:00 AM")
                        //        //        {
                        //        //            hcbDuration.Call2NxtSchedule = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCall2NxtSchDate.Text));
                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            hcbDuration.Call2NxtSchedule = "";
                        //        //        }
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        hcbDuration.Call2NxtSchedule = "";
                        //        //    }
                        //        //}
                        //        //else
                        //        //{
                        //        //    hcbDuration.Call2NxtSchedule = "";
                        //        //}

                        //        //if (txtCall3Date.Text != "")
                        //        //{
                        //        //    if (txtCall3Date.Text != "01/01/1900")
                        //        //    {
                        //        //        if (txtCall3Date.Text != "1/1/1900 12:00:00 AM")
                        //        //        {
                        //        //            hcbDuration.Call3Date = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCall3Date.Text));
                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            hcbDuration.Call3Date = "";
                        //        //        }
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        hcbDuration.Call3Date = "";
                        //        //    }

                        //        //    //if (hidCall3Fee.Value == "" || hidCall3Fee.Value == "0")
                        //        //    //{
                        //        //    if (dsFee != null)
                        //        //    {
                        //        //        if (dsFee.Tables[0].Rows.Count > 0)
                        //        //        {
                        //        //            hcbDuration.Call3Fee = Convert.ToDouble(dsFee.Tables[0].Rows[0]["Call3Fee"].ToString());
                        //        //        }
                        //        //    }
                        //        //    //}
                        //        //    //else
                        //        //    //{
                        //        //    //    hcbDuration.Call3Fee = Convert.ToDouble(hidCall3Fee.Value);
                        //        //    //}


                        //        //    if (cbCall3Aborted.Checked == true)
                        //        //    {
                        //        //        hcbDuration.Call3Aborted = true;
                        //        //        //cbCall3Aborted.Enabled = false;//SG
                        //        //        if (dsFee != null)
                        //        //        {
                        //        //            if (dsFee.Tables[0].Rows.Count > 0)
                        //        //            {
                        //        //                hcbDuration.Call3Fee = Convert.ToDouble(dsFee.Tables[0].Rows[0]["CallAbortedFee"].ToString());
                        //        //            }
                        //        //        }
                        //        //    }

                        //        //}
                        //        //else
                        //        //{
                        //        //    hcbDuration.Call3Date = "";
                        //        //}

                        //        //if (txtCall3Time.Text != "")
                        //        //{
                        //        //    hcbDuration.Call3Time = txtCall3Time.Text;
                        //        //}

                        //        //if (txtCall3FailCall.Text != "")
                        //        //{
                        //        //    hcbDuration.Call3Failed = txtCall3FailCall.Text;
                        //        //}

                        //        //if (txtCall3RTWDateOpt.Text != "")
                        //        //{
                        //        //    if (txtCall3RTWDateOpt.Text != "01/01/1900")
                        //        //    {
                        //        //        if (txtCall3RTWDateOpt.Text != "1/1/1900 12:00:00 AM")
                        //        //        {
                        //        //            hcbDuration.Call3ExpectRTWDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCall3RTWDateOpt.Text));
                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            hcbDuration.Call3ExpectRTWDate = "";
                        //        //        }
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        hcbDuration.Call3ExpectRTWDate = "";
                        //        //    }
                        //        //}
                        //        //else
                        //        //{
                        //        //    hcbDuration.Call3ExpectRTWDate = "";
                        //        //}

                        //        //if (ddlFurtherInterReq.Text != "")
                        //        //{
                        //        //    hcbDuration.Call3FurtherIntervention = ddlFurtherInterReq.SelectedItem.Text;
                        //        //}

                        //        //if (txtRTWDateAgreed.Text != "")
                        //        //{
                        //        //    if (txtRTWDateAgreed.Text != "01/01/1900")
                        //        //    {
                        //        //        if (txtRTWDateAgreed.Text != "1/1/1900 12:00:00 AM")
                        //        //        {
                        //        //            hcbDuration.Call3RTWDateAgreed = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtRTWDateAgreed.Text));
                        //        //        }
                        //        //    }
                        //        //}

                        //        //if (ddlCaseMngtInvolve3.SelectedIndex > 0)
                        //        //{
                        //        //    hcbDuration.Call3CaseMngtInvolve = ddlCaseMngtInvolve3.SelectedItem.Text;
                        //        //}

                        //        //if (ddlRehabServPurchase3.SelectedIndex > 0)
                        //        //{
                        //        //    hcbDuration.Call3RehabServicePurchase = ddlRehabServPurchase3.SelectedItem.Text;
                        //        //}

                        //        if (cbCall1Aborted.Checked)
                        //        {
                        //            disableCall1Controls();
                        //        }
                        //        //if (cbCall2Aborted.Checked)
                        //        //{
                        //        //    disableCall2Controls();
                        //        //}
                        //        //if (cbCall3Aborted.Checked)
                        //        //{
                        //        //    disableCall3Controls();
                        //        //}

                        //        hidDurationID.Value = hcbDuration.Save().ToString();
                        //        if (hidDurationID.Value == "0")
                        //        {
                        //            hidDurationID.Value = "";
                        //        }


                        //    }
                        //}

                        #endregion

                        #region HCBInsurance

                        bool Insure1 = false;
                        bool Insure2 = false;
                        bool Insure3 = false;
                        bool Insure4 = false;

                        if (txtPolicyNumber.Text != "")
                        {
                            insure.ClaimReference = txtPolicyNumber.Text;
                        }
                        else
                        {
                            insure.ClaimReference = "";
                        }

                        //if (ddlCorpPartner.SelectedIndex > 0)
                        //{
                        //    insure.CorporatePartner = Convert.ToInt32(ddlCorpPartner.SelectedValue);
                        //}
                        //else
                        //{
                        //    insure.CorporatePartner = 0;
                        //}

                        if (ddlWaitingPeriod.SelectedIndex > 0)
                        {


                            if (ddlWaitingPeriod.SelectedIndex == 5)
                            {
                                insure.DefferedPeriodOther = txtDeferredOther.Text;

                            }
                           
                            insure.WaitingPeriod = Convert.ToInt32(ddlWaitingPeriod.SelectedValue);
                        }
                        else
                        {
                            insure.WaitingPeriod = 0;
                        }

                        //if (ddlProdType.SelectedIndex > 0)
                        //{
                        //    insure.ProductType = Convert.ToInt32(ddlProdType.SelectedValue);
                        //}
                        //else
                        //{
                        //    insure.ProductType = 0;
                        //}
                        if (ddlIncapacityDefinition.SelectedValue != "0")
                        {
                            insure.IncapacityDefinition = Convert.ToInt32(ddlIncapacityDefinition.SelectedValue);
                        }
                        else
                        {
                            insure.IncapacityDefinition = 0;
                        }

                        if (txtMonthlyBenefit.Text != "")
                        {
                            insure.MonthlyBenefit = Convert.ToDouble(txtMonthlyBenefit.Text);
                        }
                        else
                        {
                            insure.MonthlyBenefit = 0;
                        }

                        if (txtEndBenefit.Text != "")
                        {
                            if (txtEndBenefit.Text != "01/01/1900")
                            {
                                if (txtEndBenefit.Text != "1/1/1900 12:00:00 AM")
                                {
                                    insure.EndofBenefit = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtEndBenefit.Text));
                                }
                                else
                                {
                                    insure.EndofBenefit = "";
                                }
                            }
                            else
                            {
                                insure.EndofBenefit = "";
                            }
                        }
                        else
                        {
                            insure.EndofBenefit = "";
                        }

                        if (Session["ClientHcbRef"] != null)
                        {
                            //string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
                            insure.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                            //Insure1 = insure.Save(2);
                        }
                        //else if (txtHCBReference.Text != "")
                        //{
                        //    insure.HCBReference = Convert.ToInt32(txtHCBReference.Text);
                        //    //Insure1 = insure.Save(2);
                        //}
                        else
                        {
                            insure.HCBReference = ID;
                            //Insure1 = insure.Save(1);
                        }

                        if (hidInsurance1ID.Value == "")
                        {
                            Insure1 = insure.Save(1);
                        }
                        else
                        {
                            insure.InsuranceId = Convert.ToInt32(hidInsurance1ID.Value);
                            Insure1 = insure.Save(2);
                        }



                       
                        //if (Insure1)
                        //{
                        //    if (txtClaimRef2.Text != "")
                        //    {
                        //        insure.ClaimReference = txtClaimRef2.Text;
                        //    }
                        //    else
                        //    {
                        //        insure.ClaimReference = "";
                        //    }

                        //    if (ddlCorpPart2.SelectedIndex > 0)
                        //    {
                        //        insure.CorporatePartner = Convert.ToInt32(ddlCorpPart2.SelectedValue);
                        //    }
                        //    else
                        //    {
                        //        insure.CorporatePartner = 0;
                        //    }

                        //    if (ddlWaitPeriod2.SelectedIndex > 0)
                        //    {
                        //        insure.WaitingPeriod = Convert.ToInt32(ddlWaitPeriod2.SelectedValue);
                        //    }
                        //    else
                        //    {
                        //        insure.CorporatePartner = 0;
                        //    }

                        //    if (ddlprodType2.SelectedIndex > 0)
                        //    {
                        //        insure.ProductType = Convert.ToInt32(ddlprodType2.SelectedValue);
                        //    }
                        //    else
                        //    {
                        //        insure.CorporatePartner = 0;
                        //    }

                        //    if (txtMnthBenefit2.Text != "")
                        //    {
                        //        insure.MonthlyBenefit = Convert.ToDouble(txtMnthBenefit2.Text);
                        //    }
                        //    else
                        //    {
                        //        insure.CorporatePartner = 0;
                        //    }

                        //    if (txtEndBenefit2.Text != "")
                        //    {
                        //        if (txtEndBenefit2.Text != "01/01/1900")
                        //        {
                        //            if (txtEndBenefit2.Text != "1/1/1900 12:00:00 AM")
                        //            {
                        //                insure.EndofBenefit = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtEndBenefit2.Text));
                        //            }
                        //            else
                        //            {
                        //                insure.EndofBenefit = "";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            insure.EndofBenefit = "";
                        //        }
                        //    }
                        //    else
                        //    {
                        //        insure.EndofBenefit = "";
                        //    }

                        //    if (Session["ClientHcbRef"] != null)
                        //    {
                        //        //string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
                        //        insure.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        //        //Insure2 = insure.Save(2);
                        //    }
                        //    //else if (txtHCBReference.Text != "")
                        //    //{
                        //    //    insure.HCBReference = Convert.ToInt32(txtHCBReference.Text);
                        //    //    //Insure2 = insure.Save(2);
                        //    //}
                        //    else
                        //    {
                        //        insure.HCBReference = ID;
                        //        //Insure2 = insure.Save(1);
                        //    }

                        //    if (hidInsurance2ID.Value == "")
                        //    {
                        //        Insure2 = insure.Save(1);
                        //    }
                        //    else
                        //    {
                        //        insure.InsuranceId = Convert.ToInt32(hidInsurance2ID.Value);
                        //        Insure2 = insure.Save(2);
                        //    }

                        //    if (Insure2)
                        //    {
                        //        if (txtClaimRef3.Text != "")
                        //        {
                        //            insure.ClaimReference = txtClaimRef3.Text;
                        //        }
                        //        else
                        //        {
                        //            insure.ClaimReference = "";
                        //        }

                        //        if (ddlCorpPart3.SelectedIndex > 0)
                        //        {
                        //            insure.CorporatePartner = Convert.ToInt32(ddlCorpPart3.SelectedValue);
                        //        }
                        //        else
                        //        {
                        //            insure.CorporatePartner = 0;
                        //        }

                        //        if (ddlWaitPeriod3.SelectedIndex > 0)
                        //        {
                        //            insure.WaitingPeriod = Convert.ToInt32(ddlWaitPeriod3.SelectedValue);
                        //        }
                        //        else
                        //        {
                        //            insure.WaitingPeriod = 0;
                        //        }

                        //        if (ddlProdType3.SelectedIndex > 0)
                        //        {
                        //            insure.ProductType = Convert.ToInt32(ddlProdType3.SelectedValue);
                        //        }
                        //        else
                        //        {
                        //            insure.ProductType = 0;
                        //        }

                        //        if (txtMnthBenefit3.Text != "")
                        //        {
                        //            insure.MonthlyBenefit = Convert.ToDouble(txtMnthBenefit3.Text);
                        //        }
                        //        else
                        //        {
                        //            insure.MonthlyBenefit = 0;
                        //        }

                        //        if (txtEndBenefit3.Text != "")
                        //        {
                        //            if (txtEndBenefit3.Text != "01/01/1900")
                        //            {
                        //                if (txtEndBenefit3.Text != "1/1/1900 12:00:00 AM")
                        //                {
                        //                    insure.EndofBenefit = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtEndBenefit3.Text));
                        //                }
                        //                else
                        //                {
                        //                    insure.EndofBenefit = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                insure.EndofBenefit = "";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            insure.EndofBenefit = "";
                        //        }

                        //        if (Session["ClientHcbRef"] != null)
                        //        {
                        //            //string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
                        //            insure.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());

                        //        }
                        //        //else if (txtHCBReference.Text != "")
                        //        //{
                        //        //    insure.HCBReference = Convert.ToInt32(txtHCBReference.Text);
                        //        //    //Insure3 = insure.Save(2);
                        //        //}
                        //        else
                        //        {
                        //            insure.HCBReference = ID;
                        //            //Insure3 = insure.Save(1);
                        //        }

                        //        if (hidInsurance3ID.Value == "")
                        //        {
                        //            Insure3 = insure.Save(1);
                        //        }
                        //        else
                        //        {
                        //            insure.InsuranceId = Convert.ToInt32(hidInsurance3ID.Value);
                        //            Insure3 = insure.Save(2);
                        //        }

                        //        if (Insure3)
                        //        {
                        //            if (txtClaimRef4.Text != "")
                        //            {
                        //                insure.ClaimReference = txtClaimRef4.Text;
                        //            }
                        //            else
                        //            {
                        //                insure.ClaimReference = "";
                        //            }

                        //            if (ddlCorpPart4.SelectedIndex > 0)
                        //            {
                        //                insure.CorporatePartner = Convert.ToInt32(ddlCorpPart4.SelectedValue);
                        //            }
                        //            else
                        //            {
                        //                insure.CorporatePartner = 0;
                        //            }

                        //            if (ddlWaitPeriod4.SelectedIndex > 0)
                        //            {
                        //                insure.WaitingPeriod = Convert.ToInt32(ddlWaitPeriod4.SelectedValue);
                        //            }
                        //            else
                        //            {
                        //                insure.WaitingPeriod = 0;
                        //            }

                        //            if (ddlProdType4.SelectedIndex > 0)
                        //            {
                        //                insure.ProductType = Convert.ToInt32(ddlProdType4.SelectedValue);
                        //            }
                        //            else
                        //            {
                        //                insure.ProductType = 0;
                        //            }

                        //            if (txtMnthBenefit4.Text != "")
                        //            {
                        //                insure.MonthlyBenefit = Convert.ToDouble(txtMnthBenefit4.Text);
                        //            }
                        //            else
                        //            {
                        //                insure.MonthlyBenefit = 0;
                        //            }

                        //            if (txtEndBenefit4.Text != "")
                        //            {
                        //                if (txtEndBenefit4.Text != "01/01/1900")
                        //                {
                        //                    if (txtEndBenefit4.Text != "1/1/1900 12:00:00 AM")
                        //                    {
                        //                        insure.EndofBenefit = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtEndBenefit4.Text));
                        //                    }
                        //                    else
                        //                    {
                        //                        insure.EndofBenefit = "";
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    insure.EndofBenefit = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                insure.EndofBenefit = "";
                        //            }

                        //            if (Session["ClientHcbRef"] != null)
                        //            {
                        //                //string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
                        //                insure.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        //                Insure4 = insure.Save(2);
                        //            }
                        //            //else if (txtHCBReference.Text != "")
                        //            //{
                        //            //    insure.HCBReference = Convert.ToInt32(txtHCBReference.Text);
                        //            //    Insure4 = insure.Save(2);
                        //            //}
                        //            else
                        //            {
                        //                insure.HCBReference = ID;
                        //                Insure4 = insure.Save(1);
                        //            }

                        //            if (hidInsurance4ID.Value == "")
                        //            {
                        //                Insure4 = insure.Save(1);
                        //            }
                        //            else
                        //            {
                        //                Insure4 = insure.Save(2);
                        //            }

                        //            if (Insure4)
                        //            {

                        //            }
                        //        }
                        //    }
                        //}

                        #endregion

                        #region Notes

                        //Create a new note and send an email alert when call aborted status changes
                        //if (ExistingCall1Aborted != cbCall1Aborted.Checked)
                        //{
                        //    string strMsgCall1Aborted = "Call 1 has been ";
                        //    if (cbCall1Aborted.Checked)
                        //    {
                        //        strMsgCall1Aborted += "Aborted";
                        //    }
                        //    else
                        //    {
                        //        strMsgCall1Aborted += "Unaborted";
                        //    }
                        //    createNote(strMsgCall1Aborted, false);
                        //    ExistingCall1Aborted = cbCall1Aborted.Checked;
                        //}
                        //if (ExistingCall2Aborted != cbCall2Aborted.Checked)
                        //{
                        //    string strMsgCall2Aborted = "Call 2 has been ";
                        //    if (cbCall2Aborted.Checked)
                        //    {
                        //        strMsgCall2Aborted += "Aborted";
                        //    }
                        //    else
                        //    {
                        //        strMsgCall2Aborted += "Unaborted";
                        //    }
                        //    createNote(strMsgCall2Aborted, false);
                        //    ExistingCall2Aborted = cbCall2Aborted.Checked;
                        //}
                        //if (ExistingCall3Aborted != cbCall3Aborted.Checked)
                        //{
                        //    string strMsgCall3Aborted = "Call 3 has been ";
                        //    if (cbCall3Aborted.Checked)
                        //    {
                        //        strMsgCall3Aborted += "Aborted";
                        //    }
                        //    else
                        //    {
                        //        strMsgCall3Aborted += "Unaborted";
                        //    }
                        //    createNote(strMsgCall3Aborted, false);
                        //    ExistingCall3Aborted = cbCall3Aborted.Checked;
                        //}

                        #endregion

                        GetFailcall(ID);
                    }
                    if (objClientInfo.HCBReference == 0)
                    {
                        SendMailToewClientCase(ID);
                    }
                    lblMsg.Text = AntiXss.HtmlEncode("Claim details are saved.");
                }
                catch (Exception ex)
                {
                    conn.Close();
                    lblMsg.Text = AntiXss.HtmlEncode("Claim details are not saved.");
                }
         }
        }
        public void SendMailToewClientCase(Int32 RefID)
        {
            try
            {
                string RefNo = "";
                bool MailSent = false;
                string CaseMngrEmail = "";
                string CaseAdminEmail = string.Empty;
                string AdminEmail = "";
                string TeamEmail = "";                 
                string MailSubject = "";
                string mailBody = "";
                object[] ArrMailAdd = null;
                string IncoEmail = "";
                ArrayList MailList = new ArrayList();
                if (RefID != 0)
                {
                    if (RefID.ToString().Length >= 5)
                    {
                        RefNo = "MTL" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 4)
                    {
                        RefNo = "MTL0" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 3)
                    {
                        RefNo = "MTL00" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 2)
                    {
                        RefNo = "MTL000" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 1)
                    {
                        RefNo = "MTL0000" + RefID.ToString();
                    }
                    AdminEmail = ConfigurationManager.AppSettings["AdminEmailAddress"].ToString();
                    TeamEmail = txtTeamEmail.Text.Trim();
                    ClientInfo objClientInfo = new ClientInfo();

                    if (ddlCaseMngr.SelectedIndex > 0)
                    {

                        DataSet dsquery = new DataSet();
                        dsquery = objClientInfo.GetUsersEmailAddress(Convert.ToInt32(ddlCaseMngr.SelectedValue));
                        if (dsquery != null && dsquery.Tables.Count > 0)
                        {
                            if (dsquery.Tables[0].Rows.Count > 0)
                            {
                                CaseMngrEmail = dsquery.Tables[0].Rows[0]["EmailAddress"].ToString();
                            }
                        }
                    }
                    DataSet dsCasequery = new DataSet();
                    dsCasequery = objClientInfo.GetAdminEmailAddress(RefID);
                    if (dsCasequery != null && dsCasequery.Tables.Count > 0)
                    {
                        CaseAdminEmail = dsCasequery.Tables[0].Rows[0]["EmailAddress"].ToString();
                    }
                    if (Session["LoggedInUserId"] != null && Session["LoggedInUserId"] != "")
                    {
                        DataSet dsquerys = new DataSet();
                        dsquerys = objClientInfo.GetUsersEmailAddress(Convert.ToInt32(Session["LoggedInUserId"].ToString()));
                        if (dsquerys != null && dsquerys.Tables.Count > 0)
                        {
                            if (dsquerys.Tables[0].Rows.Count > 0)
                            {
                                IncoEmail = dsquerys.Tables[0].Rows[0]["EmailAddress"].ToString();
                            }
                        }                        
                    }
                    string UserName=Session["LogUserEmailAdd"].ToString();
                    //if (Session["UserType"] != null)
                    //{
                    //    if (Session["UserType"].ToString() == "Inco")
                    //    {
                            MailSubject = "New Case HCB Reference No. is " + RefNo + " and MetLife Claim Reference is " + txtPolicyNumber.Text;
                           
                    // mailBody = Session["LoggedInUserName"].ToString() + " has created a new Client Case to the MTL system, under reference " + RefNo + ".";
                           // mailBody ="New Case has been registered by "+Session["LoggedInUserName"].ToString()+" in the System.<br/><table><tr><td>";
                            mailBody = "<table><tr><td>";
                            mailBody += "New Case has been registered by " + Session["LoggedInUserName"].ToString() + " in the System.</td></tr>";
                            mailBody += "<tr><td>MetLife HCB Reference No.:- " + RefNo + "</td></tr>";
                            mailBody += "<tr><td>MetLife Claim Reference:- " + txtPolicyNumber.Text.Trim() + "</td></tr>";
                        MailList.Add(AdminEmail);
                            MailList.Add(CaseMngrEmail);
                            if (CaseAdminEmail != "") //If block added on 13052014
                            {
                                MailList.Add(CaseAdminEmail);
                            }
                            if (TeamEmail != "") //If block added on 20062012
                            {
                                MailList.Add(TeamEmail);
                            }
                            if (IncoEmail != "")
                            {
                                MailList.Add(IncoEmail);
                            }
                            ArrMailAdd = MailList.ToArray();
                            bool IsimportantMail = true;
                            MailSent = SendMail(ArrMailAdd, mailBody, RefNo, MailSubject, IsimportantMail);
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {             
               
            }
            
        }
        //protected void ddlClaimClosedReason_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlClaimClosedReason.SelectedIndex >= 0)
        //    {
        //        if (ddlClaimClosedReason.SelectedIndex == 6)
        //        {
        //            trclaimreason.Visible = true;
        //        }
        //        else
        //        {
        //            trclaimreason.Visible = false;
        //        }
        //    }

        //}
        //protected void ddlInjury_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlInjury.SelectedIndex >= 0)
        //    {
        //        if (ddlInjury.SelectedIndex == 7)
        //        {
        //            trillnessinjury.Visible = true;
        //        }
        //        else
        //        {

        //            trillnessinjury.Visible = false;
        //        }

        //    }

        //}

        //protected void ddlWaitingPeriod_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlWaitingPeriod.SelectedIndex >= 0)
        //    {
        //        if (ddlWaitingPeriod.SelectedIndex == 5)
        //        {
        //            trDeferredPeriod.Visible = true;
        //        }
        //        else
        //        {
        //            trDeferredPeriod.Visible = false;
        //        }

        //    }
        //}

        protected void ddlAssessorTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet dsAssess = new DataSet();

            try
            {
                if (ddlAssessorTeam.SelectedIndex >= 0)
                {
                    HCBBLL.MasterListBL mstrbl = new MasterListBL();

                    int TeamID = Convert.ToInt32(ddlAssessorTeam.SelectedValue);

                    dsAssess = mstrbl.GetAssessorTeamDetails(TeamID);

                    if (dsAssess != null)
                    {
                        txtTeamEmail.Text = dsAssess.Tables["AssessDetails"].Rows[0]["TeamEmailID"].ToString();

                        txtTeamTelephone.Text = dsAssess.Tables["AssessDetails"].Rows[0]["PhoneNum"].ToString();
                    }


                    //if (ddlAssessorTeam.SelectedIndex == 11)
                    //{
                    //    trteamassessor.Visible = true;
                    //    //trteamassessor.Attributes.Add("style", "visibility:visible");
                    //   // tdteamother.Attributes.Add("style", "visibility:visible");
                    // //   tdteamtxtother.Attributes.Add("style", "visibility:visible");
                    //    //txtTeamOther.Attributes.Add("style", "visibility:visible");
                    //}
                    //else
                    //{
                    //    trteamassessor.Visible = false;
                    //   // trteamassessor.Attributes.Add("style", "display:none");
                    //}
                        //System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "ShowTeamAssessorOther();", true); 
                 
                }
                else
                {
                    txtTeamEmail.Text = "";
                    txtTeamTelephone.Text = "";
                }

            }
            catch (Exception ex)
            {
                txtTeamEmail.Text = "";
                txtTeamTelephone.Text = "";
            }
        }
        private int getHCBRefID()
        {
            int valueToReturn = 0;
            if (Session["NewHCBRef"] != null)
            {
                if (Session["NewHCBRef"].ToString() != "")
                {
                    valueToReturn = Convert.ToInt32(Session["NewHCBRef"].ToString());
                }
                else if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "")
                    {
                        valueToReturn = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                    }
                }
            }
            else if (Session["ClientHcbRef"] != null)
            {
                if (Session["ClientHcbRef"].ToString() != "")
                {
                    valueToReturn = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                }
            }
            return valueToReturn;
        }
        private void createNote(string noteText, bool displaySuccessAlert)
        {
            if (Session["NewHCBRef"] != null)
            {
                if (Session["NewHCBRef"].ToString() != "")
                {
                    CreateMail(Convert.ToInt32(Session["NewHCBRef"].ToString()), noteText, displaySuccessAlert);
                }
                else if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "")
                    {
                        CreateMail(Convert.ToInt32(Session["ClientHcbRef"].ToString()), noteText, displaySuccessAlert);
                    }
                }
            }
            else if (Session["ClientHcbRef"] != null)
            {
                if (Session["ClientHcbRef"].ToString() != "")
                {
                    CreateMail(Convert.ToInt32(Session["ClientHcbRef"].ToString()), noteText, displaySuccessAlert);
                }
            }
        }
        protected void btnSaveNote_Click(object sender, EventArgs e)
        {
            //if (Page.IsValid)
            //{
                createNote(AntiXss.HtmlEncode(txtAddNote.Text.Trim().ToString()), true);
            //createNote(txtAddNote.Text.Trim().ToString(), true);
                txtAddNote.Text = "";
            //}
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            //Clear();
            try
            {
                Session["ClientHcbRef"] = null;
                Session["NewHCBRef"] = null;
                Response.Redirect("ClientRecord.aspx", false);
            }
            catch (Exception ex)
            { }
        }

        //protected void txtClientDOB_TextChanged(object sender, EventArgs e)
        //{
        //    ClientInfo info = new ClientInfo();
        //    int HcbRef = 0;

        //    try
        //    {
        //        if (txtClientSurname.Text != "")
        //        {
        //            if (txtClientDOB.Text != "")
        //            {
        //                HcbRef = info.ClientExists(txtClientSurname.Text, txtClientDOB.Text);

        //                if (HcbRef > 0)
        //                {
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Client Exists", "alert('Surname and DOB check! – This claimant may already exist on the database, please check before you create a new record');", true);
        //                }
        //                else if (HcbRef == -1)
        //                {
        //                    lblMsg.Text = "Some Error Occured";
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMsg.Text = "Some Error Occured";
        //    }
        //}

        protected void btnCheckClient_Click(object sender, EventArgs e)
        {
            ClientInfo info = new ClientInfo();
            int HcbRef = 0;

            try
            {
                if (txtClientSurname.Text != "")
                {
                    if (txtClientDOB.Text != "")
                    {
                        HcbRef = info.ClientExists(txtClientSurname.Text, txtClientDOB.Text);

                        if (HcbRef > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Client Exists", "alert('Surname and DOB check! – This claimant may already exist on the database, please check before you create a new record');", true);
                            txtClientDOB.Focus();
                        }
                        else if (HcbRef == -1)
                        {
                            lblMsg.Text = AntiXss.HtmlEncode("Some Error Occured");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMsg.Text =AntiXss.HtmlEncode("Some Error Occured");
            }
        }

        #region Documents

        protected void btnDownloadDocument_Click(object sender, EventArgs e)
        {
            string strPath = Server.MapPath(ConfigurationManager.AppSettings["DocumentPath"].ToString());// System.Configuration.ConfigurationManager.AppSettings["DocumentPath"] + "";
           // string strPath = System.Configuration.ConfigurationManager.AppSettings["DocumentPath"] + "";

            try
            {
                if (strPath != "")
                {
                    string strHCBRef = "";
                    if (Session["ClientHcbRef"] != null)
                    {
                        strHCBRef = (string)Session["ClientHcbRef"];
                    }
                    else
                    {
                        if (ViewState["reffolder"] != null)
                            strHCBRef = (string)ViewState["reffolder"];
                    }

                    Response.Clear();

                    if (ddlDocuments.SelectedIndex > 0)
                    {
                        string strDocumentPath = Server.MapPath(ConfigurationManager.AppSettings["DocumentPath"].ToString());// ConfigurationManager.AppSettings["DocumentPath"];
                        FileInfo fi = new FileInfo(strDocumentPath + "\\" + strHCBRef + "\\" + ddlDocuments.SelectedItem.Text);
                        switch (fi.Extension.ToString())
                        {
                            case ".doc":
                                Response.ClearContent();
                                Response.ContentType = "application/msword";
                                break;
                            case ".wav":
                                Response.ContentType = "application/octet-stream";
                                //Response.AddHeader("Content-Length", "304578");
                                break;
                            case ".pdf":
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("Content-Length", fi.Length.ToString());
                                break;
                            case ".tif":
                                Response.ContentType = "image/tiff";
                                break;
                            case ".xls":
                                Response.ContentType = "application/vnd.ms-excel";
                                Response.AddHeader("Content-Length", fi.Length.ToString());
                                break;
                            case ".xlsx":
                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                Response.AddHeader("Content-Length", fi.Length.ToString());
                                break;
                            default:
                                Response.ContentType = "application/octet-stream";
                                break;
                        }

                        //Response.ContentType="application/pdf" or else
                        Response.CacheControl = "no-cache";
                        //Response.AddHeader("Pragma", "no-cache");
                        //Response.AddHeader("content-disposition", "attachment;filename=" + ddlDocuments.SelectedItem.Text);

                        Response.ClearHeaders();
                        //Response.ContentType = corspBean.ContentType ;
                        Response.AppendHeader("content-disposition", "attachment;filename=" + ddlDocuments.SelectedItem.Text);
                        //Response.BinaryWrite("content-disposition", "attachment;filename=" + ddlDocuments.SelectedItem.Text

                        Response.TransmitFile(AntiXss.HtmlEncode(strDocumentPath) + "\\" + AntiXss.HtmlEncode(strHCBRef) + "\\" + AntiXss.HtmlEncode(ddlDocuments.SelectedItem.Text), 0, fi.Length);
                        //Response.Flush();
                        //Response.Close();
                        Response.End();
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "DownloadDoc", "alert('Please select a document for Download');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DownloadDoc", "alert('" + ex.Message + "');", true);
            }
        }

        protected void btnRemoveDoc_Click(object sender, EventArgs e)
        {
            string strPath = Server.MapPath(ConfigurationManager.AppSettings["DocumentPath"].ToString()); //System.Configuration.ConfigurationManager.AppSettings["DocumentPath"] + "";
            hdnpostbackservicerequired.Value = hdnServiceRequired.Value;

            try
            {
                if (strPath != "")
                {
                    string strHCBRef = ""; bool ISnew = true;
                    if (Session["ClientHcbRef"] != null)
                    {
                        strHCBRef = (string)Session["ClientHcbRef"];
                        ISnew = false;
                    }
                    else
                    {
                        if (ViewState["reffolder"] != null)
                            strHCBRef = (string)ViewState["reffolder"];
                    }

                    if (ddlDocuments.SelectedIndex > 0)
                    {
                        if (File.Exists(AntiXss.HtmlEncode(strPath) + "\\" + AntiXss.HtmlEncode(strHCBRef) + "\\" + AntiXss.HtmlEncode(ddlDocuments.SelectedItem.Text)))
                        {
                            try
                            {
                                if (strHCBRef != "")
                                {
                                    File.Delete(AntiXss.HtmlEncode(strPath) + "\\" + AntiXss.HtmlEncode(strHCBRef) + "\\" + AntiXss.HtmlEncode(ddlDocuments.SelectedItem.Text));
                                    if (ISnew)
                                    {
                                        if (ViewState["filelist"] != null)
                                        {
                                            List<TempFileList> tplist = new List<TempFileList>();
                                            tplist = (List<TempFileList>)ViewState["filelist"];
                                            TempFileList file = new TempFileList();
                                            file.filename = ddlDocuments.SelectedItem.Text;
                                            tplist.RemoveAt(tplist.FindIndex(FindComputer));
                                            ViewState["filelist"] = tplist;
                                        }
                                    }
                                    else
                                    {
                                        Documents doc = new Documents();
                                        doc.DocID = Convert.ToInt32(ddlDocuments.SelectedItem.Value.Trim());
                                        doc.DeleteDocument();
                                    }
                                    GetDocuments();
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "DownloadDoc", "alert('Please select a document for Removing');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DownloadDoc", "alert('" + ex.Message + "');", true);
            }
        }

        protected void btnShowUpload_Click(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadDoc.HasFile)
                {
                    hdnpostbackservicerequired.Value = hdnServiceRequired.Value;

                    string strPath = Server.MapPath(ConfigurationManager.AppSettings["DocumentPath"].ToString());// System.Configuration.ConfigurationManager.AppSettings["DocumentPath"] + "";
                    if (strPath != "")
                    {
                        //strPath = Server.MapPath(strPath);
                        string strRefNo = "";
                        bool ISCreate = true;
                        string filename = FileUploadDoc.FileName;
                        bool filechk = true;
                        if (Session["ClientHcbRef"] != null)
                        {
                            strRefNo = AntiXss.HtmlEncode((string)Session["ClientHcbRef"]);
                            ISCreate = false;
                        }
                        else
                        {

                            if (ViewState["reffolder"] != null)
                            {
                                strRefNo = AntiXss.HtmlEncode((string)ViewState["reffolder"]);
                            }
                            else
                            {
                                Random rnd = new Random((10 + DateTime.Now.Millisecond));
                                strRefNo = AntiXss.HtmlEncode(rnd.Next(10000, 99999).ToString());
                                strRefNo = "temp" + strRefNo;
                                ViewState.Add("reffolder", strRefNo);
                            }
                        }
                        bool fileExists = false;
                        if (!Directory.Exists(strPath + "\\" + AntiXss.HtmlEncode(strRefNo)))
                        {
                            Directory.CreateDirectory(strPath + "\\" + AntiXss.HtmlEncode(strRefNo));
                        }
                        else
                        {

                            if (File.Exists(strPath + "\\" + AntiXss.HtmlEncode(strRefNo) + "\\" + FileUploadDoc.FileName))
                            {
                                if (!chkfoverwrite.Checked)
                                {
                                    lblUpload.Text = AntiXss.HtmlEncode("File with same name already exists");
                                    Page.ClientScript.RegisterStartupScript(GetType(), "Showuploader", "showupload();", true);
                                    filechk = false;
                                }
                                fileExists = true;
                            }

                        }
                        if (filechk)
                        {
                            FileUploadDoc.SaveAs(strPath + "\\" + AntiXss.HtmlEncode(strRefNo) + "\\" + FileUploadDoc.FileName);
                            FileUploadDoc.FileName.Remove(0);
                            if (!fileExists)
                            {
                                if (!ISCreate)
                                {
                                    InsertDocDetails(AntiXss.HtmlEncode(strRefNo), filename);
                                }
                                else
                                {

                                    List<TempFileList> tpList = new List<TempFileList>();
                                    TempFileList File1 = new TempFileList();
                                    File1.filename = filename;
                                    if (ViewState["filelist"] != null)
                                    {
                                        tpList = (List<TempFileList>)ViewState["filelist"];
                                        tpList.Add(File1);
                                    }
                                    else
                                    {
                                        tpList.Add(File1);
                                        ViewState.Add("filelist", tpList);
                                    }

                                }
                            }

                        }


                        //write code in case of failed upload check databseentry ad hide uploader
                    }
                    GetDocuments();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.Replace(',', ' ');
                ClientScript.RegisterStartupScript(this.GetType(), "", "alert('" + msg + "');");
            }
        }

        #endregion

        #endregion

        #region Methods
        private void BindTelephoneActivity()
        {
            DataSet dsTelephone = new DataSet();
            TelephoneActivity obj = new TelephoneActivity();
            dsTelephone = obj.GetTelephoneActivity(Convert.ToInt32(Session["ClientHcbRef"].ToString()));
            if (dsTelephone != null && dsTelephone.Tables[0].Rows.Count > 0)
            {
              
                //lnkAdd.Visible = true;
                //tblHomeVisit.Visible = false;
                //tbGRIDHomeVisit.Visible = true;
                grdTelephoneIntervention.DataSource = dsTelephone.Tables[0];
                grdTelephoneIntervention.DataBind();
                pnlTelephoneIntervention.Visible = false;
                pnlGridTelephoneIntervention.Visible = true;

            }

            else
            {
                clearTelephoneActivity();
                grdTelephoneIntervention.DataSource = dsTelephone.Tables[0];
                grdTelephoneIntervention.DataBind();
                pnlTelephoneIntervention.Visible = true;
                pnlGridTelephoneIntervention.Visible = false;

                //lnkAdd.Visible = false;
                //tblHomeVisit.Visible = true;
                //tbGRIDHomeVisit.Visible = false;
                //upHomeVisits.Update();
            }

        }
        private void BindHomeVisit()
        {
            DataSet dsHomeVisit = new DataSet();
            HCBActivityVisit hcbInfo = new HCBActivityVisit();
            dsHomeVisit = hcbInfo.GetHCBVisitActivity(Convert.ToInt32(Session["ClientHcbRef"].ToString()));


            if (dsHomeVisit != null && dsHomeVisit.Tables[0].Rows.Count > 0)
            {
                if (Session["UserType"].ToString() == "Inco")
                {
                    pnlHomeVisit.Visible = false;
                    pnlGRIDHomeVisit.Visible = true;
                }
                if (Session["UserType"].ToString() == "Admin" || Session["UserType"].ToString() == "Nurse")
                {
                    pnlHomeVisit.Visible = false;
                    pnlGRIDHomeVisit.Visible = true;
                    lnkAdd.Visible = false;
                }
               
                //lnkAdd.Visible = true;
                //tblHomeVisit.Visible = false;
                //tbGRIDHomeVisit.Visible = true;
                grdHomeVisit.DataSource = dsHomeVisit.Tables[0];
                grdHomeVisit.DataBind();

            }

            else
            {
                grdHomeVisit.DataSource = dsHomeVisit.Tables[0];
                grdHomeVisit.DataBind();
                if (Session["UserType"].ToString() == "Nurse")
                {
                    pnlHomeVisit.Visible = true;
                    pnlGRIDHomeVisit.Visible = false;

                }
                else
                {
                    pnlHomeVisit.Visible = true;
                    pnlHomeVisit.Enabled = false;
                    pnlGRIDHomeVisit.Visible = false;
                }
                //lnkAdd.Visible = false;
                //tblHomeVisit.Visible = true;
                //tbGRIDHomeVisit.Visible = false;
                //upHomeVisits.Update();
            }
            //    if (dtHomeVisit != null)
            //    {
            //        if (dtHomeVisit.Rows.Count > 0)
            //        {

            //            lnkAdd.Visible = true;
            //            tblHomeVisit.Visible = false;
            //            tbGRIDHomeVisit.Visible = true;
            //            grdHomeVisit.DataSource = dtHomeVisit;
            //            grdHomeVisit.DataBind();          

            //            //upHomeVisits.Update();

            //        }
            //        else
            //        {
            //            lnkAdd.Visible = false;
            //            tblHomeVisit.Visible = true;
            //            tbGRIDHomeVisit.Visible = false;
            //            grdHomeVisit.DataSource = null;
            //            grdHomeVisit.DataBind();
            //            //upHomeVisits.Update();
            //        }
            //    }
            //    else
            //    {
            //        lnkAdd.Visible = false;
            //        tblHomeVisit.Visible = true;
            //        tbGRIDHomeVisit.Visible = false;
            //        grdHomeVisit.DataSource = null;
            //        grdHomeVisit.DataBind();
            //        //upHomeVisits.Update();
            //    }
            //}
            //else
            //{
            //    grdHomeVisit.DataSource = null;
            //    grdHomeVisit.DataBind();
            //    lnkAdd.Visible = false;
            //    tblHomeVisit.Visible = true;
            //    tbGRIDHomeVisit.Visible = false;

            //    //upHomeVisits.Update();
            //}
        }
        private void BindRehabilitation()
        {
            DataSet ds = new DataSet();
            Rehabilitation obj = new Rehabilitation();
            ds = obj.GetListHCBRehabilitation(Convert.ToInt32(Session["ClientHcbRef"].ToString()));
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                grdRehabilitation.DataSource = ds.Tables[0];
                grdRehabilitation.DataBind();

            }
            else
            {
                txtRehabCurrentCaseTotal.Text = "";
                grdRehabilitation.DataSource = null;
                grdRehabilitation.DataBind();
            }
        }
        private void BindCaseManagementCost()
        {
            DataSet dsCasemgnt = new DataSet();
            CaseManagement hcbInfo = new CaseManagement();
            dsCasemgnt = hcbInfo.GetListHCBCaseManagementCost(Convert.ToInt32(Session["ClientHcbRef"].ToString()));


            if (dsCasemgnt != null && dsCasemgnt.Tables[0].Rows.Count > 0)
            {
                //if (Session["UserType"].ToString() == "Inco")
                //{
                //    pnlHomeVisit.Visible = false;
                //    pnlGRIDHomeVisit.Visible = true;
                //}
                //if (Session["UserType"].ToString() == "Admin" || Session["UserType"].ToString() == "Nurse")
                //{
                //    pnlHomeVisit.Visible = false;
                //    pnlGRIDHomeVisit.Visible = true;
                //    lnkAdd.Visible = false;
                //}
                grdCaseMgntCost.DataSource = dsCasemgnt.Tables[0];
                grdCaseMgntCost.DataBind();

            }

            else
            {
                grdCaseMgntCost.DataSource = dsCasemgnt.Tables[0];
                grdCaseMgntCost.DataBind();
                //if (Session["UserType"].ToString() == "Nurse")
                //{
                //    pnlHomeVisit.Visible = true;
                //    pnlGRIDHomeVisit.Visible = false;

                //}
                //else
                //{
                //    pnlHomeVisit.Visible = true;
                //    pnlHomeVisit.Enabled = false;
                //    pnlGRIDHomeVisit.Visible = false;
                //}
            }
          
        }
        private void FillClientDetails()
        {
            DataSet ds = new DataSet();
            DataSet dsDuration = new DataSet();
            DataTable dtDuration = new DataTable();
            //DataSet dsHomeVisit = new DataSet();

            try
            {
                if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "")
                    {
                        ClientInfo cltInfo = new ClientInfo();
                        if (Session["UserType"].ToString() == "Admin")
                        {
                            conn.Open();
                            string AdminId =string.Empty;
                            DataSet dsquery = new DataSet();
                            dsquery = cltInfo.GetClientRecordAdminId(Convert.ToInt32(Session["ClientHcbRef"].ToString()));
                            //string AdminId = dbcmd.ExecuteScalar().ToString();
                            if (dsquery != null)
                            {
                                if (dsquery.Tables[0].Rows.Count >0)
                                {
                                    AdminId = dsquery.Tables[0].Rows[0]["AdminId"].ToString();
                                }
                            }
                            //string AdminQuery = "select AdminId from ClientRecord where HCBReference = " + Session["ClientHcbRef"].ToString();
                            //OleDbCommand dbcmd = new OleDbCommand(AdminQuery, conn);
                            //string AdminId = dbcmd.ExecuteScalar().ToString();
                            //conn.Close();
                            if (AdminId.Trim() == "")
                            {
                                ClientInfo.SetAdmin(Convert.ToInt32(Session["ClientHcbRef"].ToString()), Convert.ToInt32(Session["LoggedInUserId"].ToString().Trim()));
                            }
                        }

                        #region Documenst
                        GetDocuments();
                        #endregion

                        #region ClientRecord

                        
                        ds = cltInfo.GetClientDet(Session["ClientHcbRef"].ToString());
                        DataTable dt = new DataTable();

                        if (ds != null)
                        {
                            dt = ds.Tables["ClientInfo"];

                            DataColumn dc1 = new DataColumn();
                            dc1.ColumnName = "RefNo";
                            dc1.DataType = System.Type.GetType("System.String");
                            dt.Columns.Add(dc1);
                            string RefNo = "";

                            foreach (DataRow dr in dt.Rows)
                            {

                                if (dr["HCBReference"].ToString().Length >= 5)
                                {
                                    RefNo = "MTL" + dr["HCBReference"].ToString();
                                }
                                else if (dr["HCBReference"].ToString().Length == 4)
                                {
                                    RefNo = "MTL0" + dr["HCBReference"].ToString();
                                }
                                else if (dr["HCBReference"].ToString().Length == 3)
                                {
                                    RefNo = "MTL00" + dr["HCBReference"].ToString();
                                }
                                else if (dr["HCBReference"].ToString().Length == 2)
                                {
                                    RefNo = "MTL000" + dr["HCBReference"].ToString();
                                }
                                else if (dr["HCBReference"].ToString().Length == 1)
                                {
                                    RefNo = "MTL0000" + dr["HCBReference"].ToString();
                                }

                                if (RefNo != "")
                                {
                                    //dt.Columns["RefNo"].DefaultValue = RefNo;
                                    dr["RefNo"] = RefNo;
                                    dt.AcceptChanges();
                                }
                            }

                            try
                            {
                                txtHCBReference.Text = ds.Tables["ClientInfo"].Rows[0]["RefNo"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtHCBReference.Text = "";
                            }

                            try
                            {
                                txtservicerequiredother.Text = ds.Tables["ClientInfo"].Rows[0]["ServiceRequiredOther"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtservicerequiredother.Text = "";
                            }
                            try
                            {

                                ddlCaseMngr.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["HCBNurseID"].ToString();
                            }
                            catch (Exception ex)
                            {
                                ddlCaseMngr.SelectedIndex = 0;
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["ClaimHandlerID"].ToString() != "")
                            {

                                ddlClaimAssessor.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["ClaimHandlerID"].ToString();
                            }
                            else
                            {
                                ddlClaimAssessor.SelectedIndex = 0;
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["ClaimHandlerEmail"].ToString() != "")
                            {
                                txtClaimHandlerEmail.Text = ds.Tables["ClientInfo"].Rows[0]["ClaimHandlerEmail"].ToString();
                            }
                            else
                            {
                                txtClaimHandlerEmail.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["ClaimHandlerTelephone"].ToString() != "")
                            {
                                txtClaimHandlerTelephone.Text = ds.Tables["ClientInfo"].Rows[0]["ClaimHandlerTelephone"].ToString();
                            }
                            else
                            {
                                txtClaimHandlerTelephone.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["AssessorTeamID"].ToString() != "")
                            {


                                if (ds.Tables["ClientInfo"].Rows[0]["AssessorTeamID"].ToString() == "14")
                                {


                                   // trteamassessor.Visible = true;
                                    if (ds.Tables["ClientInfo"].Rows[0]["AssesorTeamOther"].ToString() != "")
                                    {

                                        txtTeamOther.Text = ds.Tables["ClientInfo"].Rows[0]["AssesorTeamOther"].ToString();
                                    }
                                    else
                                    {
                                        txtTeamOther.Text = "";
                                    }

                                }
                                //else
                                //{
                                //    trteamassessor.Visible = false;
                                //}

                                ddlAssessorTeam.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["AssessorTeamID"].ToString();
                            }
                            else
                            {
                                ddlAssessorTeam.SelectedIndex = 0;
                            }



                            if (ds.Tables["ClientInfo"].Rows[0]["AssessorTeamEmail"].ToString() != "")
                            {
                                //txtTeamEmail.Text = ds.Tables["ClientInfo"].Rows[0]["AssessorTeamEmail"].ToString();
                            }
                            else
                            {
                                //txtTeamEmail.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["AssessorTeamTelephone"].ToString() != "")
                            {
                                //txtTeamTelephone.Text = ds.Tables["ClientInfo"].Rows[0]["AssessorTeamTelephone"].ToString();
                            }
                            else
                            {
                                //txtTeamTelephone.Text = "";
                            }

                            

                            try
                            {
                                //ddlSalutation.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["Salutation"].ToString();
                            }
                            catch (Exception ex)
                            {
                                //ddlSalutation.SelectedValue = "0";
                            }
                            try
                            {
                                txtClientFName.Text = ds.Tables["ClientInfo"].Rows[0]["ClientForenames"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtClientFName.Text = "";
                            }

                            try
                            {
                                txtClientSurname.Text = ds.Tables["ClientInfo"].Rows[0]["ClientSurname"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtClientSurname.Text = "";
                            }

                            try
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["ClientDOB"].ToString() != "")
                                {
                                    if (ds.Tables["ClientInfo"].Rows[0]["ClientDOB"].ToString() == "01/01/1900")
                                    {
                                        txtClientDOB.Text = "";
                                    }
                                    else if (ds.Tables["ClientInfo"].Rows[0]["ClientDOB"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtClientDOB.Text = "";
                                    }
                                    else
                                    {
                                        txtClientDOB.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["ClientDOB"]);
                                    }
                                }
                                else
                                {
                                    txtClientDOB.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtClientDOB.Text = "";
                            }

                            try
                            {
                               // txtClientAddress.Text = ds.Tables["ClientInfo"].Rows[0]["ClientAddress"].ToString();
                               txtAddress1.Text=ds.Tables["ClientInfo"].Rows[0]["ClientAddress1"].ToString();
                               txtAddress2.Text = ds.Tables["ClientInfo"].Rows[0]["ClientAddress2"].ToString();
                               txtCity.Text = ds.Tables["ClientInfo"].Rows[0]["ClientCity"].ToString();
                               txtCounty.Text = ds.Tables["ClientInfo"].Rows[0]["ClientCounty"].ToString();
                            }
                            catch (Exception ex)
                            {
                                //txtClientAddress.Text = "";
                                txtAddress1.Text="";
                                txtAddress2.Text="";
                                txtCity.Text="";
                                txtCounty.Text="";
                            }

                            try
                            {
                                txtClientMobileTelephone.Text = ds.Tables["ClientInfo"].Rows[0]["ClientMobileTelephone"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtClientMobileTelephone.Text = "";
                            }

                            try
                            {
                                txtClientWorkTelephone.Text = ds.Tables["ClientInfo"].Rows[0]["ClientWorkTelephone"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtClientWorkTelephone.Text = "";
                            }

                            //if (ds.Tables["ClientInfo"].Rows[0]["ClientServiceReq"].ToString() != "")
                            //{
                            //    string[] ClientServiceReq = ds.Tables["ClientInfo"].Rows[0]["ClientServiceReq"].ToString().Split(',');
                            //    for (int i = 0; i < ClientServiceReq.Length - 1; i++)
                            //    {
                            //        foreach (ListItem listItem in lstServiceReq.Items)
                            //        {
                            //            if (listItem.Value == ClientServiceReq[i])
                            //            {
                            //                listItem.Selected = true;
                            //            }
                            //        }
                            //    }
                                
                            //}
                            if (ds.Tables["ClientInfo"].Rows[0]["ClientServiceReq"].ToString() != "" )
                            {
                                hdnServiceRequired.Value = ds.Tables["ClientInfo"].Rows[0]["ClientServiceReq"].ToString();
                                string[] servicerequiredarray = hdnServiceRequired.Value.Split(',');
                                DataSet dsServiceRequired = new DataSet();
                                lblServiceRequired.Text = "<b>Service Required selected are:</b></br>";
                                for (int i = 0; i < servicerequiredarray.Length; i++)
                                {
                                    dsServiceRequired = cltInfo.GetDetailServiceRequired(Convert.ToInt32(servicerequiredarray[i]));
                                    if (dsServiceRequired != null && dsServiceRequired.Tables.Count > 0)
                                    {
                                        lblServiceRequired.Text += AntiXss.HtmlEncode(dsServiceRequired.Tables[0].Rows[0]["Name"].ToString())+"</br>";
                                    }
                                }
                            }
                            else
                            {
                                hdnServiceRequired.Value = "";
                                lblServiceRequired.Text = "<b>No Service Required Selected.</b>";
                            }
                            
                            try
                            {
                                txtClientPostCode.Text = ds.Tables["ClientInfo"].Rows[0]["ClientPostCode"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtClientPostCode.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["ClientOccupation"].ToString() != "")
                            {
                                txtOccupation.Text = ds.Tables["ClientInfo"].Rows[0]["ClientOccupation"].ToString();
                            }
                            else
                            {
                                txtOccupation.Text = "";
                            }

                            //if (ds.Tables["ClientInfo"].Rows[0]["ClientEmployeeStatus"].ToString() != "")
                            //{

                            //    ddlEmpStatus.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["ClientEmployeeStatus"].ToString();
                            //}
                            //else
                            //{
                            //    ddlEmpStatus.SelectedIndex = 0;
                            //}

                            //try
                            //{
                            //    if (ds.Tables["ClientInfo"].Rows[0]["ClientOccupationClass"].ToString() != "")
                            //    {

                            //        ddlBrand.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["ClientOccupationClass"].ToString();
                            //    }
                            //    else
                            //    {
                            //        ddlBrand.SelectedIndex = 0;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ddlBrand.SelectedIndex = 0;
                            //}
                            try
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["ClientGender"].ToString() != "")
                                {

                                    ddlGender.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["ClientGender"].ToString();
                                }
                                else
                                {
                                    ddlGender.SelectedValue = "0";
                                }
                            }
                            catch (Exception ex)
                            {
                                ddlGender.SelectedValue = "0";
                            }
                            try
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["SchemeNameId"].ToString() != "")
                                {

                                    ddlSchemeName.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["SchemeNameId"].ToString();
                                }
                                else
                                {
                                    ddlSchemeName.SelectedValue = "0";
                                }
                            }
                            catch (Exception ex)
                            {
                                ddlSchemeName.SelectedValue = "0";
                            }
                            try
                            {
                                txtSchemeNumber.Text = ds.Tables["ClientInfo"].Rows[0]["SchemeNumber"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtSchemeNumber.Text = "";
                            }
                            //try
                            //{
                            //    if (ds.Tables["ClientInfo"].Rows[0]["SchemeNumberId"].ToString() != "")
                            //    {

                            //        ddlMetSchemeNumber.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["SchemeNumberId"].ToString();
                            //    }
                            //    else
                            //    {
                            //        ddlMetSchemeNumber.SelectedValue = "0";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ddlMetSchemeNumber.SelectedValue = "0";
                            //}

                            try
                            {
                                txtEmployerName.Text = ds.Tables["ClientInfo"].Rows[0]["EmployerName"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtEmployerName.Text = "";
                            }
                            try
                            {
                                txtEmployerContract.Text = ds.Tables["ClientInfo"].Rows[0]["EmployerContact"].ToString();
                            }
                            catch (Exception ex)
                            {
                                txtEmployerContract.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["IncapacityDay"].ToString() != "")
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["IncapacityDay"].ToString() == "01/01/1900")
                                {
                                    txtIncapacityDay.Text = "";
                                }
                                else if (ds.Tables["ClientInfo"].Rows[0]["IncapacityDay"].ToString() == "1/1/1900 12:00:00 AM")
                                {
                                    txtIncapacityDay.Text = "";
                                }
                                else
                                {
                                    txtIncapacityDay.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["IncapacityDay"]);
                                }
                            }
                            else
                            {
                                txtIncapacityDay.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["IllnessInjury"].ToString() != "")
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["IllnessInjury"].ToString() == "12588")
                                {
                                  //  trillnessinjury.Visible = true;
                                    txtillnessinjuryother.Text = ds.Tables["ClientInfo"].Rows[0]["InjuryOther"].ToString();
                                }
                                //else
                                //{
                                //    trillnessinjury.Visible = false;
                                //}
                                ddlInjury.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["IllnessInjury"].ToString();
                            }
                            else
                            {
                                ddlInjury.SelectedIndex = 0;
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["DisabilityType"].ToString() != "")
                            {
                                txtDisabilityType.Text = ds.Tables["ClientInfo"].Rows[0]["DisabilityType"].ToString();
                            }
                            else
                            {
                                txtDisabilityType.Text = "";
                            }

                            try
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["ICD9Code"].ToString() != "")
                                {
                                    txtICD9Code.Text = ds.Tables["ClientInfo"].Rows[0]["ICD9Code"].ToString();
                                }
                                else
                                {
                                    txtICD9Code.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtICD9Code.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["ClaimCloseDate"].ToString() != "")
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["ClaimCloseDate"].ToString() == "01/01/1900")
                                {
                                    txtClaimCloseDate.Text = "";
                                }
                                else if (ds.Tables["ClientInfo"].Rows[0]["ClaimCloseDate"].ToString() == "1/1/1900 12:00:00 AM")
                                {
                                    txtClaimCloseDate.Text = "";
                                }
                                else
                                {
                                    txtClaimCloseDate.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["ClaimCloseDate"]);
                                }
                            }
                            else
                            {
                                txtClaimCloseDate.Text = "";
                            }
                            //txtClaimCloseReason.Text = ds.Tables["ClientInfo"].Rows[0]["ClaimCloseReason"].ToString();
                            if (ds.Tables["ClientInfo"].Rows[0]["ClaimCloseReason"].ToString() != "")
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["ClaimCloseReason"].ToString() == "12586")
                                {
                                   // trclaimreason.Visible = true;
                                    txtclaimreasonother.Text = ds.Tables["ClientInfo"].Rows[0]["ClaimReasonOther"].ToString();

                                }
                                //else
                                //{
                                //    trclaimreason.Visible = false;
                                //}

                                ddlClaimClosedReason.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["ClaimCloseReason"].ToString();
                            }
                            else
                            {
                                ddlClaimClosedReason.SelectedIndex = 0;
                            }
                            if (!string.IsNullOrEmpty(ds.Tables["ClientInfo"].Rows[0]["IsDocumentUpload"].ToString()))
                            {
                                if(ds.Tables["ClientInfo"].Rows[0]["IsDocumentUpload"].ToString()=="True")
                                {
                                    chkFormUploaded.Checked= true;
                                }
                                else{
                                 chkFormUploaded.Checked= false;
                                }
                               
                            }
                            else
                            {
                                chkFormUploaded.Checked = false;
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["ClaimDetails"].ToString() != "")
                            {
                                txtClaimDetails.Text = ds.Tables["ClientInfo"].Rows[0]["ClaimDetails"].ToString();
                            }
                            else
                            {
                                txtClaimDetails.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["HCBReceivedDate"].ToString() != "")
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["HCBReceivedDate"].ToString() == "01/01/1900")
                                {
                                    txtHCBReceivedDate.Text = "";
                                }
                                else if (ds.Tables["ClientInfo"].Rows[0]["HCBReceivedDate"].ToString() == "1/1/1900 12:00:00 AM")
                                {
                                    txtHCBReceivedDate.Text = "";
                                }
                                else
                                {
                                    txtHCBReceivedDate.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["HCBReceivedDate"]);
                                }
                            }
                            else
                            {
                                txtHCBReceivedDate.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["DateSentToNurse"].ToString() != "")
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["DateSentToNurse"].ToString() == "01/01/1900")
                                {
                                    txtDateSentToNurse.Text = "";
                                }
                                else if (ds.Tables["ClientInfo"].Rows[0]["DateSentToNurse"].ToString() == "1/1/1900 12:00:00 AM")
                                {
                                    txtDateSentToNurse.Text = "";
                                }
                                else
                                {
                                    txtDateSentToNurse.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["DateSentToNurse"]);
                                }
                            }
                            else
                            {
                                txtDateSentToNurse.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["CancellationReason"].ToString() != "")
                            {
                                ddlReasonCancel.SelectedValue = ds.Tables["ClientInfo"].Rows[0]["CancellationReason"].ToString();
                            }
                            else
                            {
                                ddlReasonCancel.SelectedIndex = 0;
                            }

                            try
                            {
                                if (ds.Tables["ClientInfo"].Rows[0]["DateClosed"].ToString() != "")
                                {
                                    if (ds.Tables["ClientInfo"].Rows[0]["DateClosed"].ToString() == "01/01/1900")
                                    {
                                        txtCaseClosed.Text = "";
                                    }
                                    else if (ds.Tables["ClientInfo"].Rows[0]["DateClosed"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtCaseClosed.Text = "";
                                    }
                                    else
                                    {
                                        txtCaseClosed.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["DateClosed"]);
                                    }
                                }
                                else
                                {
                                    txtCaseClosed.Text = "";
                                }

                                if (ds.Tables["ClientInfo"].Rows[0]["DateCancelled"].ToString() != "")
                                {
                                    if (ds.Tables["ClientInfo"].Rows[0]["DateCancelled"].ToString() == "01/01/1900")
                                    {
                                        txtCaseCancelled.Text = "";
                                    }
                                    else if (ds.Tables["ClientInfo"].Rows[0]["DateCancelled"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtCaseCancelled.Text = "";
                                    }
                                    else
                                    {
                                        txtCaseCancelled.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["DateCancelled"]);
                                    }
                                }
                                else
                                {
                                    txtCaseCancelled.Text = "";
                                }

                                if (ds.Tables["ClientInfo"].Rows[0]["DateClaimantFirstContacted"].ToString() != "")
                                {
                                    if (ds.Tables["ClientInfo"].Rows[0]["DateClaimantFirstContacted"].ToString() == "01/01/1900")
                                    {
                                        txtclaimantfirstcontacted.Text = "";
                                    }
                                    else if (ds.Tables["ClientInfo"].Rows[0]["DateClaimantFirstContacted"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtclaimantfirstcontacted.Text = "";
                                    }
                                    else
                                    {
                                        txtclaimantfirstcontacted.Text = string.Format("{0:dd/MM/yyyy}", ds.Tables["ClientInfo"].Rows[0]["DateClaimantFirstContacted"]);
                                    }
                                }
                                else
                                {
                                    txtclaimantfirstcontacted.Text = "";
                                }
                               

                            }
                            catch (Exception ex)
                            {
                                txtCaseClosed.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["FeeCharged"].ToString() != "")
                            {
                                txtFeeCharged.Text = ds.Tables["ClientInfo"].Rows[0]["FeeCharged"].ToString();
                            }
                            else
                            {
                                txtFeeCharged.Text = "";
                            }

                            if (ds.Tables["ClientInfo"].Rows[0]["RehabCost"].ToString() != "")
                            {
                                txtRehabcost.Text = ds.Tables["ClientInfo"].Rows[0]["RehabCost"].ToString();
                            }
                            else
                            {
                                txtRehabcost.Text = "";
                            }

                            if (ViewState["ClientIncoID"] != null)
                            {
                                if (ViewState["ClientIncoID"].ToString() != "")
                                {

                                }
                                else
                                {
                                    ViewState.Add("ClientIncoID", ds.Tables["ClientInfo"].Rows[0]["IncoID"].ToString());
                                }
                            }
                            else
                            {
                                ViewState.Add("ClientIncoID", ds.Tables["ClientInfo"].Rows[0]["IncoID"].ToString());
                            }

                            if (ViewState["ClientIncoName"] != null)
                            {
                                if (ViewState["ClientIncoName"].ToString() != "")
                                {

                                }
                                else
                                {
                                    ViewState.Add("ClientIncoName", ds.Tables["ClientInfo"].Rows[0]["IncoName"].ToString());
                                }
                            }
                            else
                            {
                                ViewState.Add("ClientIncoName", ds.Tables["ClientInfo"].Rows[0]["IncoName"].ToString());
                            }
                        }

                        #endregion

                        #region HCBDuration

                        HCBDuration duration = new HCBDuration();
                        duration.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());

                        dsDuration = duration.GetHCBDuration();

                        if (dsDuration != null)
                        {
                            dt = dsDuration.Tables["Duration"];

                            try
                            {
                                if (dt.Rows[0]["DurationID"].ToString() != "")
                                {
                                    hidDurationID.Value = dt.Rows[0]["DurationID"].ToString();
                                }
                                else
                                {
                                    hidDurationID.Value = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                hidDurationID.Value = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["AppointmentDate"].ToString() != "")
                                {
                                    if (dt.Rows[0]["AppointmentDate"].ToString() == "01/01/1900")
                                    {
                                        txtAppointmentDate.Text = "";
                                    }
                                    else if (dt.Rows[0]["AppointmentDate"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtAppointmentDate.Text = "";
                                    }
                                    else
                                    {
                                        txtAppointmentDate.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["AppointmentDate"]);
                                    }
                                }
                                else
                                {
                                    txtAppointmentDate.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtAppointmentDate.Text = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["ReportCompDate"].ToString() != "")
                                {
                                    if (dt.Rows[0]["ReportCompDate"].ToString() == "01/01/1900")
                                    {
                                        txtDateReportComp.Text = "";
                                    }
                                    else if (dt.Rows[0]["ReportCompDate"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtDateReportComp.Text = "";
                                    }
                                    else
                                    {
                                        txtDateReportComp.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["ReportCompDate"]);
                                    }
                                }
                                else
                                {
                                    txtDateReportComp.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtDateReportComp.Text = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call1Date"].ToString() != "")
                                {
                                    if (dt.Rows[0]["Call1Date"].ToString() == "01/01/1900")
                                    {
                                        txtCall1Date.Text = "";
                                    }
                                    else if (dt.Rows[0]["Call1Date"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtCall1Date.Text = "";
                                    }
                                    else
                                    {
                                        txtCall1Date.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call1Date"]);
                                    }
                                }
                                else
                                {
                                    txtCall1Date.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtCall1Date.Text = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call1Time"].ToString() != "")
                                {
                                    DateTime Call1Time = Convert.ToDateTime(dt.Rows[0]["Call1Time"]);
                                    txtCall1Time.Text = Call1Time.Hour.ToString() + ":" + Call1Time.Minute.ToString();
                                }
                                else
                                {
                                    txtCall1Time.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtCall1Time.Text = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call1ExpectRTWDate"].ToString() != "")
                                {
                                    if (dt.Rows[0]["Call1ExpectRTWDate"].ToString() == "01/01/1900")
                                    {
                                        txtRTWDateOptimum.Text = "";
                                    }
                                    else if (dt.Rows[0]["Call1ExpectRTWDate"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtRTWDateOptimum.Text = "";
                                    }
                                    else
                                    {
                                        txtRTWDateOptimum.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call1ExpectRTWDate"]);
                                    }
                                }
                                else
                                {
                                    txtRTWDateOptimum.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtRTWDateOptimum.Text = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call1CaseMngtRecom"].ToString() != "")
                                {
                                    ddlCaseMngtRecom.SelectedValue = dt.Rows[0]["Call1CaseMngtRecom"].ToString();
                                }
                                else
                                {
                                    ddlCaseMngtRecom.SelectedIndex = 0;
                                }
                            }
                            catch (Exception ex)
                            {
                                ddlCaseMngtRecom.SelectedIndex = 0;
                            }

                            try
                            {
                                if (dt.Rows[0]["Call1NxtSchedule"].ToString() != "")
                                {
                                    if (dt.Rows[0]["Call1NxtSchedule"].ToString() == "01/01/1900")
                                    {
                                        txtNxtCallSchd.Text = "";
                                    }
                                    else if (dt.Rows[0]["Call1NxtSchedule"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtNxtCallSchd.Text = "";
                                    }
                                    else
                                    {
                                        txtNxtCallSchd.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call1NxtSchedule"]);
                                    }
                                }
                                else
                                {
                                    txtNxtCallSchd.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtNxtCallSchd.Text = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call1Aborted"].ToString() != "")
                                {
                                    if (dt.Rows[0]["Call1Aborted"].ToString() == "True")
                                    {
                                       // cbCall1Aborted.Checked = true;
                                        ExistingCall1Aborted = true;
                                        //cbCall1Aborted.Enabled = false;//SG
                                    }
                                    else
                                    {
                                       // cbCall1Aborted.Checked = false;
                                        ExistingCall1Aborted = false;
                                    }
                                }
                                else
                                {
                                   // cbCall1Aborted.Checked = false;
                                    ExistingCall1Aborted = false;
                                }
                            }
                            catch (Exception ex)
                            {
                               // cbCall1Aborted.Checked = false;
                                ExistingCall1Aborted = false;
                            }

                            //try
                            //{
                            //    if (dt.Rows[0]["Call2Date"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call2Date"].ToString() == "01/01/1900")
                            //        {
                            //            txtCall2Date.Text = "";
                            //        }
                            //        else if (dt.Rows[0]["Call2Date"].ToString() == "1/1/1900 12:00:00 AM")
                            //        {
                            //            txtCall2Date.Text = "";
                            //        }
                            //        else
                            //        {
                            //            txtCall2Date.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call2Date"]);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        txtCall2Date.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtCall2Date.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call2Time"].ToString() != "")
                            //    {
                            //        DateTime Call2Time = Convert.ToDateTime(dt.Rows[0]["Call2Time"]);
                            //        txtCall2Time.Text = Call2Time.Hour.ToString() + ":" + Call2Time.Minute.ToString();
                            //    }
                            //    else
                            //    {
                            //        txtCall2Time.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtCall2Time.Text = "";
                            //}

                            // try
                            //{
                            //    if (dt.Rows[0]["Call2ExpectRTWDate"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call2ExpectRTWDate"].ToString() == "01/01/1900")
                            //        {
                            //            txtCall2RTWDateOpt.Text = "";
                            //        }
                            //        else if (dt.Rows[0]["Call2ExpectRTWDate"].ToString() == "1/1/1900 12:00:00 AM")
                            //        {
                            //            txtCall2RTWDateOpt.Text = "";
                            //        }
                            //        else
                            //        {
                            //            txtCall2RTWDateOpt.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call2ExpectRTWDate"]);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        txtCall2RTWDateOpt.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtCall2RTWDateOpt.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call2CasemngtRecom"].ToString() != "")
                            //    {
                            //        ddlCaseMngtRecom2.SelectedValue = dt.Rows[0]["Call2CasemngtRecom"].ToString();
                            //    }
                            //    else
                            //    {
                            //        ddlCaseMngtRecom2.SelectedIndex = 0;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ddlCaseMngtRecom2.SelectedIndex = 0;
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call2OriginalRTW"].ToString() != "")
                            //    {
                            //        ddlRTWReal.SelectedValue = dt.Rows[0]["Call2OriginalRTW"].ToString();
                            //    }
                            //    else
                            //    {
                            //        ddlRTWReal.SelectedIndex = 0;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ddlRTWReal.SelectedIndex = 0;
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call2EnvisagedDate"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call2EnvisagedDate"].ToString() == "01/01/1900")
                            //        {
                            //            txtEnvisagedRTWDate.Text = "";
                            //        }
                            //        else if (dt.Rows[0]["Call2EnvisagedDate"].ToString() == "1/1/1900 12:00:00 AM")
                            //        {
                            //            txtEnvisagedRTWDate.Text = "";
                            //        }
                            //        else
                            //        {
                            //            txtEnvisagedRTWDate.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call2EnvisagedDate"]);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        txtEnvisagedRTWDate.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtEnvisagedRTWDate.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call2NxtSchedule"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call2NxtSchedule"].ToString() == "01/01/1900")
                            //        {
                            //            txtCall2NxtSchDate.Text = "";
                            //        }
                            //        else if (dt.Rows[0]["Call2NxtSchedule"].ToString() == "1/1/1900 12:00:00 AM")
                            //        {
                            //            txtCall2NxtSchDate.Text = "";
                            //        }
                            //        else
                            //        {
                            //            txtCall2NxtSchDate.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call2NxtSchedule"]);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        txtCall2NxtSchDate.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtCall2NxtSchDate.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call2Aborted"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call2Aborted"].ToString() == "True")
                            //        {
                            //            cbCall2Aborted.Checked = true;
                            //            ExistingCall2Aborted = true;
                            //            //cbCall2Aborted.Enabled = false;//SG
                            //        }
                            //        else
                            //        {
                            //            cbCall2Aborted.Checked = false;
                            //            ExistingCall2Aborted = false;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        cbCall2Aborted.Checked = false;
                            //        ExistingCall2Aborted = false;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    cbCall2Aborted.Checked = false;
                            //    ExistingCall2Aborted = false;
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3Date"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call3Date"].ToString() == "01/01/1900")
                            //        {
                            //            txtCall3Date.Text = "";
                            //        }
                            //        else if (dt.Rows[0]["Call3Date"].ToString() == "1/1/1900 12:00:00 AM")
                            //        {
                            //            txtCall3Date.Text = "";
                            //        }
                            //        else
                            //        {
                            //            txtCall3Date.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call3Date"]);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        txtCall3Date.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtCall3Date.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3Time"].ToString() != "")
                            //    {
                            //        DateTime Call3Date = Convert.ToDateTime(dt.Rows[0]["Call3Time"]);
                            //        txtCall3Time.Text = Call3Date.Hour.ToString() + ":" + Call3Date.Minute.ToString();
                            //    }
                            //    else
                            //    {
                            //        txtCall3Time.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtCall3Time.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3ExpectRTWDate"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call3ExpectRTWDate"].ToString() == "01/01/1900")
                            //        {
                            //            txtCall3RTWDateOpt.Text = "";
                            //        }
                            //        else if (dt.Rows[0]["Call3Date"].ToString() == "1/1/1900 12:00:00 AM")
                            //        {
                            //            txtCall3RTWDateOpt.Text = "";
                            //        }
                            //        else
                            //        {
                            //            txtCall3RTWDateOpt.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call3ExpectRTWDate"]);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        txtCall3RTWDateOpt.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtCall3RTWDateOpt.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3FurtherIntervention"].ToString() != "")
                            //    {
                            //        ddlFurtherInterReq.SelectedValue = dt.Rows[0]["Call3FurtherIntervention"].ToString();
                            //    }
                            //    else
                            //    {
                            //        ddlFurtherInterReq.SelectedIndex = 0;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ddlFurtherInterReq.SelectedIndex = 0;
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3RTWDateAgreed"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call3RTWDateAgreed"].ToString() == "01/01/1900")
                            //        {
                            //            txtRTWDateAgreed.Text = "";
                            //        }
                            //        else if (dt.Rows[0]["Call3RTWDateAgreed"].ToString() == "1/1/1900 12:00:00 AM")
                            //        {
                            //            txtRTWDateAgreed.Text = "";
                            //        }
                            //        else
                            //        {
                            //            txtRTWDateAgreed.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Call3RTWDateAgreed"]);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        txtRTWDateAgreed.Text = "";
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    txtRTWDateAgreed.Text = "";
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3CaseMngtInvolve"].ToString() != "")
                            //    {
                            //        ddlCaseMngtInvolve3.SelectedValue = dt.Rows[0]["Call3CaseMngtInvolve"].ToString();
                            //    }
                            //    else
                            //    {
                            //        ddlCaseMngtInvolve3.SelectedIndex = 0;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ddlCaseMngtInvolve3.SelectedIndex = 0;
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3RehabServicePurchase"].ToString() != "")
                            //    {
                            //        ddlRehabServPurchase3.SelectedValue = dt.Rows[0]["Call3RehabServicePurchase"].ToString();
                            //    }
                            //    else
                            //    {
                            //        ddlRehabServPurchase3.SelectedIndex = 0;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ddlRehabServPurchase3.SelectedIndex = 0;
                            //}

                            //try
                            //{
                            //    if (dt.Rows[0]["Call3Aborted"].ToString() != "")
                            //    {
                            //        if (dt.Rows[0]["Call3Aborted"].ToString() == "True")
                            //        {
                            //            cbCall3Aborted.Checked = true;
                            //            ExistingCall3Aborted = true;
                            //            //cbCall3Aborted.Enabled = false;//SG
                            //        }
                            //        else
                            //        {
                            //            cbCall3Aborted.Checked = false;
                            //            ExistingCall3Aborted = false;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        cbCall3Aborted.Checked = false;
                            //        ExistingCall3Aborted = false;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    cbCall3Aborted.Checked = false;
                            //    ExistingCall3Aborted = false;
                            //}

                            try
                            {
                                if (dt.Rows[0]["HomeVisitFee"].ToString() != "")
                                {
                                    hidHomeFee.Value = dt.Rows[0]["HomeVisitFee"].ToString();
                                }
                                else
                                {
                                    hidHomeFee.Value = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                hidHomeFee.Value = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call1Fee"].ToString() != "")
                                {
                                    hidCall1Fee.Value = dt.Rows[0]["Call1Fee"].ToString();
                                }
                                else
                                {
                                    hidCall1Fee.Value = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                hidCall1Fee.Value = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call2Fee"].ToString() != "")
                                {
                                    hidCall2Fee.Value = dt.Rows[0]["Call2Fee"].ToString();
                                }
                                else
                                {
                                    hidCall2Fee.Value = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                hidCall2Fee.Value = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["Call3Fee"].ToString() != "")
                                {
                                    hidCall3Fee.Value = dt.Rows[0]["Call3Fee"].ToString();
                                }
                                else
                                {
                                    hidCall3Fee.Value = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                hidCall3Fee.Value = "";
                            }



                        }
                        //if (cbCall1Aborted.Checked)
                        //{
                        //    disableCall1Controls();
                        //}
                        //if (cbCall2Aborted.Checked)
                        //{
                        //    disableCall2Controls();
                        //}
                        //if (cbCall3Aborted.Checked)
                        //{
                        //    disableCall3Controls();
                        //}

                        #endregion

                        #region [Home Visit]
                        BindHomeVisit();
                        BindTelephoneActivity();
                        BindCaseManagementCost();
                        #endregion
                        #region [Rehabilitation]
                        BindRehabilitation();
                        #endregion
                        #region HCBFailCall

                        GetFailcall(Convert.ToInt32(Session["ClientHcbRef"]));

                        #endregion

                        #region HCBInsurance

                        HCBInsurance ins = new HCBInsurance();
                        DataSet dsInsure = new DataSet();
                        DataTable dtInsure = new DataTable();

                        ins.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());

                        try
                        {
                            dsInsure = ins.GetInsuranceDet();

                            if (dsInsure != null)
                            {
                                dtInsure = dsInsure.Tables["Insurance"];

                                if (dtInsure.Rows.Count > 0)
                                {
                                    #region Ins1
                                    try
                                    {
                                        hidInsurance1ID.Value = dtInsure.Rows[0]["InsuranceId"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        //hidInsurance1ID.Value = "";
                                    }

                                    try
                                    {
                                        txtPolicyNumber.Text = dtInsure.Rows[0]["ClaimReference"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        //txtPolicyNumber.Text = "";
                                    }

                                    //try
                                    //{
                                    //    ddlCorpPartner.SelectedValue = dtInsure.Rows[0]["CorporatePartner"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlCorpPartner.SelectedIndex = 0;
                                    //}

                                    try
                                    {
                                        ddlWaitingPeriod.SelectedValue = dtInsure.Rows[0]["WaitingPeriod"].ToString();
                                        if (dtInsure.Rows[0]["WaitingPeriod"].ToString() == "11600")
                                        {

                                           // trDeferredPeriod.Visible = true;
                                            //trDeferredPeriod.Visible = true;
                                            //trDeferredPeriod.Attributes.Add("style", "display:block");
                                            txtDeferredOther.Text = dtInsure.Rows[0]["DeferredPeriodOther"].ToString();

                                        }
                                        //else
                                        //{
                                        //    trDeferredPeriod.Visible = false;
                                        //   // trDeferredPeriod.Attributes.Add("style", "display:none");
                                        //    //trDeferredPeriod.Visible = false;
                                        //}
                                    }
                                    catch (Exception ex)
                                    {
                                        ddlWaitingPeriod.SelectedIndex = 0;
                                    }

                                    //try
                                    //{
                                    //    ddlProdType.SelectedValue = dtInsure.Rows[0]["ProductType"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlProdType.SelectedIndex = 0;
                                    //}
                                    try
                                    {
                                        ddlIncapacityDefinition.SelectedValue = dtInsure.Rows[0]["IncapacityDefinition"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        ddlIncapacityDefinition.SelectedValue = "0";
                                    }

                                    try
                                    {
                                        if (!string.IsNullOrEmpty(dtInsure.Rows[0]["MonthlyBenefit"].ToString()))
                                        {
                                            txtMonthlyBenefit.Text = String.Format("{0:n2}", Convert.ToDecimal(dtInsure.Rows[0]["MonthlyBenefit"].ToString()));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //txtMonthlyBenefit.Text = "";
                                    }

                                    try
                                    {
                                        if (dtInsure.Rows[0]["EndofBenefit"].ToString() != "")
                                        {
                                            if (dtInsure.Rows[0]["EndofBenefit"].ToString() == "01/01/1900")
                                            {
                                                txtEndBenefit.Text = "";

                                            }
                                            else if (dtInsure.Rows[0]["EndofBenefit"].ToString() == "1/1/1900 12:00:00 AM")
                                            {
                                                txtEndBenefit.Text = "";
                                            }
                                            else
                                            {
                                                txtEndBenefit.Text = string.Format("{0:dd/MM/yyyy}", dtInsure.Rows[0]["EndofBenefit"]);
                                            }
                                        }
                                        else
                                        {
                                            txtEndBenefit.Text = "";
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //txtEndBenefit.Text = "";
                                    }
                                    #endregion

                                    //#region Ins2
                                    //try
                                    //{
                                    //    hidInsurance2ID.Value = dtInsure.Rows[1]["InsuranceId"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    hidInsurance2ID.Value = "";
                                    //}

                                    //try
                                    //{
                                    //    if (dtInsure.Rows[1]["ClaimReference"].ToString() != "")
                                    //    {
                                    //        txtClaimRef2.Text = dtInsure.Rows[1]["ClaimReference"].ToString();
                                    //        trInsuranceDet2.Style.Add("display", "block");
                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //txtClaimRef2.Text = "";
                                    //}

                                    //try
                                    //{
                                    //    ddlCorpPart2.SelectedValue = dtInsure.Rows[1]["CorporatePartner"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlCorpPart2.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    ddlWaitPeriod2.SelectedValue = dtInsure.Rows[1]["WaitingPeriod"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlWaitPeriod2.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    ddlprodType2.SelectedValue = dtInsure.Rows[1]["ProductType"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlprodType2.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    txtMnthBenefit2.Text = dtInsure.Rows[1]["MonthlyBenefit"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //txtMnthBenefit2.Text = "";
                                    //}

                                    //try
                                    //{
                                    //    if (dtInsure.Rows[1]["EndofBenefit"].ToString() != "")
                                    //    {
                                    //        if (dtInsure.Rows[1]["EndofBenefit"].ToString() == "01/01/1900")
                                    //        {
                                    //            txtEndBenefit2.Text = "";
                                    //        }
                                    //        else if (dtInsure.Rows[1]["EndofBenefit"].ToString() == "1/1/1900 12:00:00 AM")
                                    //        {
                                    //            txtEndBenefit2.Text = "";
                                    //        }
                                    //        else
                                    //        {
                                    //            txtEndBenefit2.Text = string.Format("{0:dd/MM/yyyy}", dtInsure.Rows[1]["EndofBenefit"]);
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        txtEndBenefit2.Text = "";
                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //txtEndBenefit2.Text = "";
                                    //}
                                    //#endregion

                                    //#region Ins3
                                    //try
                                    //{
                                    //    hidInsurance3ID.Value = dtInsure.Rows[2]["InsuranceId"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //hidInsurance3ID.Value = "";
                                    //}

                                    //try
                                    //{
                                    //    if (dtInsure.Rows[2]["ClaimReference"].ToString() != "")
                                    //    {
                                    //        txtClaimRef3.Text = dtInsure.Rows[2]["ClaimReference"].ToString();
                                    //        trInsuranceDet3.Style.Add("display", "block");
                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //txtClaimRef3.Text = "";
                                    //}

                                    //try
                                    //{
                                    //    ddlCorpPart3.SelectedValue = dtInsure.Rows[2]["CorporatePartner"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlCorpPart3.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    ddlWaitPeriod3.SelectedValue = dtInsure.Rows[2]["WaitingPeriod"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlWaitPeriod3.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    ddlProdType3.SelectedValue = dtInsure.Rows[2]["ProductType"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlProdType3.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    txtMnthBenefit3.Text = dtInsure.Rows[2]["MonthlyBenefit"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    // txtMnthBenefit3.Text = "";
                                    //}

                                    //try
                                    //{
                                    //    if (dtInsure.Rows[2]["EndofBenefit"].ToString() != "")
                                    //    {
                                    //        if (dtInsure.Rows[2]["EndofBenefit"].ToString() == "01/01/1900")
                                    //        {
                                    //            txtEndBenefit3.Text = "";
                                    //        }
                                    //        else if (dtInsure.Rows[1]["EndofBenefit"].ToString() == "1/1/1900 12:00:00 AM")
                                    //        {
                                    //            txtEndBenefit3.Text = "";
                                    //        }
                                    //        else
                                    //        {
                                    //            txtEndBenefit3.Text = string.Format("{0:dd/MM/yyyy}", dtInsure.Rows[2]["EndofBenefit"]);
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        txtEndBenefit3.Text = "";
                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    // txtEndBenefit3.Text = "";
                                    //}
                                    //#endregion

                                    //#region Ins4
                                    ////DataTable dtIns4 = dtInsure;

                                    //try
                                    //{
                                    //    hidInsurance4ID.Value = dtInsure.Rows[3]["InsuranceId"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    // hidInsurance4ID.Value = "";
                                    //}

                                    //try
                                    //{
                                    //    if (dtInsure.Rows[3]["ClaimReference"].ToString() != "")
                                    //    {
                                    //        txtClaimRef4.Text = dtInsure.Rows[3]["ClaimReference"].ToString();
                                    //        trInsuranceGap4.Style.Add("display", "block");

                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //txtClaimRef4.Text = "";
                                    //}

                                    //try
                                    //{
                                    //    ddlCorpPart4.ClearSelection();

                                    //    ListItem item = ddlCorpPart4.Items.FindByValue(dtInsure.Rows[3]["CorporatePartner"].ToString());
                                    //    if (item != null)
                                    //        item.Selected = true;

                                    //    //ddlCorpPart4.SelectedValue = dtInsure.Rows[3]["CorporatePartner"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlCorpPart4.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    //ddlWaitPeriod4.SelectedValue = dtInsure.Rows[3]["WaitingPeriod"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlWaitPeriod4.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    //ddlProdType4.SelectedValue = dtInsure.Rows[3]["ProductType"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    ddlProdType4.SelectedIndex = 0;
                                    //}

                                    //try
                                    //{
                                    //    txtMnthBenefit4.Text = dtInsure.Rows[3]["MonthlyBenefit"].ToString();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //txtMnthBenefit4.Text = "";
                                    //}

                                    //try
                                    //{
                                    //    if (dtInsure.Rows[3]["EndofBenefit"].ToString() != "")
                                    //    {
                                    //        if (dtInsure.Rows[3]["EndofBenefit"].ToString() == "01/01/1900")
                                    //        {
                                    //            txtEndBenefit4.Text = "";
                                    //        }
                                    //        else if (dtInsure.Rows[3]["EndofBenefit"].ToString() == "1/1/1900 12:00:00 AM")
                                    //        {
                                    //            txtEndBenefit4.Text = "";
                                    //        }
                                    //        else
                                    //        {
                                    //            txtEndBenefit4.Text = string.Format("{0:dd/MM/yyyy}", dtInsure.Rows[3]["EndofBenefit"]);
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        txtEndBenefit4.Text = "";
                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    //txtEndBenefit4.Text = "";
                                    //}
                                    //#endregion
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            dsInsure.Dispose();
                            dtInsure.Dispose();
                        }

                        #endregion
                        #region [Case management Cost]
                        CaseManagement objcase=new CaseManagement();
                        DataSet dsCasemgnt = new DataSet();
                        dsCasemgnt = objcase.GetDetailHCBCaseManagement(Convert.ToInt32(Session["ClientHcbRef"].ToString()));
                        if (dsCasemgnt != null && dsCasemgnt.Tables.Count > 0)
                        {
                            DataTable dtCase = dsCasemgnt.Tables[0];
                            if (dtCase.Rows.Count > 0)
                            {
                                if (Session["UserType"].ToString() == "Inco")
                                {
                                    ddlCaseMgntapproved.Enabled = true;
                                    btnCaseMgntSave.Enabled = true;
                                }
                                else
                                {
                                    ddlCaseMgntapproved.Enabled = false;
                                   
                                }
                                if (dtCase.Rows[0]["ApprovedBy"].ToString() == "True")
                                {
                                    txtCaseMgntFundingLimit.Enabled = true;
                                    lnkCaseMgntCost.Enabled = true;
                                    grdCaseMgntCost.Enabled = true;
                                    btnCaseMgntSave.Enabled = true;
                                    ddlCaseMgntapproved.SelectedValue = "1";
                                }
                                else if (dtCase.Rows[0]["ApprovedBy"].ToString() == "False")
                                {
                                    txtCaseMgntFundingLimit.Enabled = false;
                                    lnkCaseMgntCost.Enabled = false;
                                    grdCaseMgntCost.Enabled = false;
                                   
                                    ddlCaseMgntapproved.SelectedValue = "0";
                                }
                                if (!string.IsNullOrEmpty(dtCase.Rows[0]["FundingLimit"].ToString()))
                                {
                                    txtCaseMgntFundingLimit.Text =String.Format("{0:n2}", Convert.ToDecimal(dtCase.Rows[0]["FundingLimit"].ToString()));
                                }
                                btnCaseMgntSave.Text = "Update Case Management";

                            }
                            else
                            {
                                if (Session["UserType"].ToString() == "Inco")
                                {
                                    ddlCaseMgntapproved.Enabled = true;
                                    //txtCaseMgntFundingLimit.Enabled = true;
                                    //lnkCaseMgntCost.Enabled = true;
                                    //grdCaseMgntCost.Enabled = true;
                                    btnCaseMgntSave.Enabled = true;
                                }
                                else
                                {
                                    ddlCaseMgntapproved.Enabled = false;
                                    txtCaseMgntFundingLimit.Enabled = false;
                                    lnkCaseMgntCost.Enabled = false;
                                    grdCaseMgntCost.Enabled = false;
                                    btnCaseMgntSave.Enabled = false;
                                }
                            }

                        }
                        else
                        {
                            if (Session["UserType"].ToString() == "Inco")
                            {
                                ddlCaseMgntapproved.Enabled = true;
                                //txtCaseMgntFundingLimit.Enabled = true;
                                //lnkCaseMgntCost.Enabled = true;
                                //grdCaseMgntCost.Enabled = true;
                                btnCaseMgntSave.Enabled = true;
                            }
                            else
                            {
                                ddlCaseMgntapproved.Enabled = false;
                                txtCaseMgntFundingLimit.Enabled = false;
                                lnkCaseMgntCost.Enabled = false;
                                grdCaseMgntCost.Enabled = false;
                                btnCaseMgntSave.Enabled = false;
                            }
                        }
                        #endregion
                        txtRehabcost.Text = (Convert.ToDecimal((!string.IsNullOrEmpty(txtRehabCurrentCaseTotal.Text))?txtRehabCurrentCaseTotal.Text:"0")).ToString();
                        if (getHCBRefID() == 0)
                        {
                            //enable only if the user clicks has a new claim form atleast once
                           // btnRehabModify.Enabled = false;
                        }
                        FillNotesLoad(Convert.ToInt32(Session["ClientHcbRef"]));
                      //  FillClientRehabilitationCosts(getHCBRefID());
                    }
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                ds.Dispose();
            }

            //Session["ClientHcbRef"] = "0";
            //dt.Columns.Add("RefNo", typeof(string));
        }

        //[WebMethod(EnableSession=true)]
        //public static string FillNotes(string InnerText)
        //{
        //    #region Notes

        //    OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        //    Notes note = new Notes();
        //    string query = "";
        //    string UserFullName = "";
        //    DataSet UserName = new DataSet();
        //    DataSet dsNotes = new DataSet();
        //    DataTable dtNotes = new DataTable();

        //    if (conn.State == ConnectionState.Open)
        //    {
        //        conn.Close();
        //    }

        //    conn.Open();
        //    note.HCBReference = Convert.ToInt32(HttpContext.Current.Session["ClientHcbRef"].ToString());

        //    dsNotes = note.GetNotes();

        //    if (dsNotes != null)
        //    {
        //        dtNotes = dsNotes.Tables["Notes"];

        //        try
        //        {
        //            if (dtNotes.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dtNotes.Rows.Count; i++)
        //                {
        //                    UserName = new DataSet();
        //                    query = "SELECT Users.UserID, Users.EmailAddress, Users.UserType, Users.FullName FROM Users WHERE (((Users.EmailAddress)= @EmailAdd));";
        //                    OleDbCommand cmd = new OleDbCommand(query, conn);
        //                    cmd.CommandType = CommandType.Text;

        //                    cmd.Parameters.Add(new OleDbParameter("EmailAdd", OleDbType.VarChar)).Value = dtNotes.Rows[i]["UserName"].ToString();

        //                    OleDbDataAdapter daUser = new OleDbDataAdapter();
        //                    daUser.SelectCommand = cmd;
        //                    daUser.Fill(UserName, "UserDet");

        //                    if (UserName != null)
        //                    {
        //                        if (UserName.Tables["UserDet"].Rows.Count > 0)
        //                        {
        //                            UserFullName = UserName.Tables["UserDet"].Rows[0]["FullName"].ToString();
        //                        }
        //                    }

        //                    if (dtNotes.Rows[i]["EmailResult"].ToString() == "OK(1)")
        //                    {
        //                        if (InnerText != "")
        //                        {
        //                            InnerText = InnerText.Trim() + "<br/><br/>" + dtNotes.Rows[i]["NoteDate"].ToString().Trim() + " " + "<img src='Images/Mail.jpg' />" + "<br/>" + dtNotes.Rows[i]["NoteText"].ToString().Trim() + "<br/>" + UserFullName;
        //                        }
        //                        else
        //                        {
        //                            InnerText = dtNotes.Rows[i]["NoteDate"].ToString().Trim() + " " + "<img src='Images/Mail.jpg' />" + "<br/>" + dtNotes.Rows[i]["NoteText"].ToString().Trim() + "<br/>" + UserFullName;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (InnerText != "")
        //                        {
        //                            InnerText = InnerText.Trim() + "<br/><br/>" + dtNotes.Rows[i]["NoteDate"].ToString().Trim() + "<br/>" + dtNotes.Rows[i]["NoteText"].ToString().Trim() + "<br/>" + UserFullName;
        //                        }
        //                        else
        //                        {
        //                            InnerText = dtNotes.Rows[i]["NoteDate"].ToString().Trim() + "<br/>" + dtNotes.Rows[i]["NoteText"].ToString().Trim() + "<br/>" + UserFullName;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            InnerText = "";
        //            dtNotes.Dispose();
        //            dsNotes.Dispose();
        //        }
        //    }

        //    return InnerText;

        //    #endregion
        //}

        private void FillNotesLoad(int RefID)
        {
            #region Notes

            Notes note = new Notes();
            string query = "";
            string UserFullName = "";
            DataSet UserName = new DataSet();
            DataSet dsNotes = new DataSet();
            DataTable dtNotes = new DataTable();
            lblNotes.InnerHtml = "";

            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            conn.Open();
            note.HCBReference = RefID;

            dsNotes = note.GetNotes();

            if (dsNotes != null)
            {
                dtNotes = dsNotes.Tables["Notes"];

                try
                {
                    ClientInfo obj = new ClientInfo();
                    if (dtNotes.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtNotes.Rows.Count; i++)
                        {
                            UserName = new DataSet();
                            UserName=obj.GetDetailUsersEmailId(dtNotes.Rows[i]["UserName"].ToString());
                            //query = "SELECT Users.UserID, Users.EmailAddress, Users.UserType, Users.FullName FROM Users WHERE (((Users.EmailAddress)= @EmailAdd));";
                            //OleDbCommand cmd = new OleDbCommand(query, conn);
                            //cmd.CommandType = CommandType.Text;

                            //cmd.Parameters.Add(new OleDbParameter("EmailAdd", OleDbType.VarChar)).Value = dtNotes.Rows[i]["UserName"].ToString();

                            //OleDbDataAdapter daUser = new OleDbDataAdapter();
                            //daUser.SelectCommand = cmd;
                            //daUser.Fill(UserName, "UserDet");

                            if (UserName != null)
                            {
                                if (UserName.Tables["UserDet"].Rows.Count > 0)
                                {
                                    UserFullName = UserName.Tables["UserDet"].Rows[0]["FullName"].ToString();
                                }
                            }

                            if (dtNotes.Rows[i]["EmailResult"].ToString() == "OK(1)")
                            {
                                if (lblNotes.InnerHtml != "")
                                {
                                    lblNotes.InnerHtml = lblNotes.InnerHtml.Trim() + "<br/><br/>" +AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteDate"].ToString().Trim()) + " " + "<img src='Images/Mail.jpg' />" + "<br/>" +Server.HtmlDecode(AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteText"].ToString().Trim())) + "<br/>" + AntiXss.HtmlEncode(UserFullName);
                                }
                                else
                                {
                                    
                                    lblNotes.InnerHtml = "<br/>"+ AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteDate"].ToString().Trim()) + " " + "<img src='Images/Mail.jpg' />" + "<br/>" +Server.HtmlDecode(AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteText"].ToString().Trim())) + "<br/>" + AntiXss.HtmlEncode(UserFullName);
                                }
                            }
                            else
                            {
                                if (lblNotes.InnerHtml != "")
                                {
                                    lblNotes.InnerHtml = lblNotes.InnerHtml.Trim() + "<br/><br/>" + AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteDate"].ToString().Trim()) + "<br/>" +Server.HtmlDecode(AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteText"].ToString().Trim())) + "<br/>" + AntiXss.HtmlEncode(UserFullName);
                                }
                                else
                                {
                                    lblNotes.InnerHtml = "<br/>"+AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteDate"].ToString().Trim()) + "<br/>" + Server.HtmlDecode(AntiXss.HtmlEncode(dtNotes.Rows[i]["NoteText"].ToString()).Trim()) + "<br/>" +AntiXss.HtmlEncode(UserFullName);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblNotes.InnerHtml = "";
                    dtNotes.Dispose();
                    dsNotes.Dispose();
                }
            }

            txtAddNote.Text = "";

            #endregion
        }

        private void GetClaimAssessor()
        {
            try
            {
                Users user = new Users();
                DataSet ds = user.GetClaimAssessor();
                ddlClaimAssessor.DataSource = ds.Tables["Users"];
                ddlClaimAssessor.DataTextField = "FullName";
                ddlClaimAssessor.DataValueField = "UserID";
                ddlClaimAssessor.DataBind();
                ListItem item = new ListItem("--Select Claim Assessor--", "0");
                ddlClaimAssessor.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Claim Assessor--", "0");
                ddlClaimAssessor.Items.Insert(0, item);
            }
        }

        private void FillClaimManager()
        {
            try
            {
                Users ouser = new Users();
                DataSet ds = ouser.GetClaimConsultant();
                ddlCaseMngr.DataSource = ds.Tables["Consultant"];
                ddlCaseMngr.DataTextField = "FullName";
                ddlCaseMngr.DataValueField = "UserID";
                ddlCaseMngr.DataBind();
                ListItem item = new ListItem("--Select Case Manager--", "0");
                ddlCaseMngr.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Case Manager--", "0");
                ddlCaseMngr.Items.Insert(0, item);
            }
        }

        private void FillServiceReq()
        {
            try
            {
                //HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                //DataSet ds = mstrServReq.GetMasterList(1);

                ////ddlServiceReq.DataSource = ds.Tables["Master"];
                ////ddlServiceReq.DataTextField = "Name";
                ////ddlServiceReq.DataValueField = "ID";
                ////ddlServiceReq.DataBind();
                //ListItem item = new ListItem("--Select Service Required--", "0");
                ////ddlServiceReq.Items.Insert(0, item);

                //lstServiceReq.Items.Clear();

                //lstServiceReq.DataSource = ds.Tables["Master"];
                //lstServiceReq.DataTextField = "Name";
                //lstServiceReq.DataValueField = "ID";
                //lstServiceReq.DataBind();
                //lstServiceReq.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                //ListItem item = new ListItem("--Select Service Required--", "0");
                //lstServiceReq.Items.Insert(0, item);
                //ddlServiceReq.Items.Insert(0, item);
            }
        }

        private void FillBrand()
        {
            try
            {
                //HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                //DataSet ds = mstrServReq.GetMasterList(13);

                //ddlBrand.DataSource = ds.Tables["Master"];
                //ddlBrand.DataTextField = "Name";
                //ddlBrand.DataValueField = "ID";
                //ddlBrand.DataBind();
                //ListItem item = new ListItem("--Select Brand--", "0");
                //ddlBrand.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                //ListItem item = new ListItem("--Select Brand--", "0");
                //ddlBrand.Items.Insert(0, item);
            }
        }

        private void FillEmployeeStatus()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(4);
                //-Comment by JAywanti
                //ddlEmpStatus.DataSource = ds.Tables["Master"];
                //ddlEmpStatus.DataTextField = "Name";
                //ddlEmpStatus.DataValueField = "ID";
                //ddlEmpStatus.DataBind();

                //ListItem item = new ListItem("--Select Employee Status--", "0");
                //ddlEmpStatus.Items.Insert(0, item);
                //
            }
            catch (Exception ex)
            {
                //-Comment by JAywanti
                //ListItem item = new ListItem("--Select Employee Status--", "0");
                //ddlEmpStatus.Items.Insert(0, item);
                //

            }
        }

        //private void FillCorpPartner()
        //{
        //    try
        //    {
        //        HCBBLL.MasterListBL mstrServReq = new MasterListBL();
        //        DataSet ds = mstrServReq.GetMasterList(9);

        //        ddlCorpPartner.DataSource = ds.Tables["Master"];
        //        ddlCorpPartner.DataTextField = "Name";
        //        ddlCorpPartner.DataValueField = "ID";
        //        ddlCorpPartner.DataBind();
        //        ListItem item = new ListItem("--Select Corporate Partner--", "0");
        //        ddlCorpPartner.Items.Insert(0, item);

        //        //ddlCorpPart2.DataSource = ds.Tables["Master"];
        //        //ddlCorpPart2.DataTextField = "Name";
        //        //ddlCorpPart2.DataValueField = "ID";
        //        //ddlCorpPart2.DataBind();
        //        //ListItem item1 = new ListItem("--Select Corporate Partner--", "0");
        //        //ddlCorpPart2.Items.Insert(0, item1);

        //        //ddlCorpPart3.DataSource = ds.Tables["Master"];
        //        //ddlCorpPart3.DataTextField = "Name";
        //        //ddlCorpPart3.DataValueField = "ID";
        //        //ddlCorpPart3.DataBind();
        //        //ListItem item2 = new ListItem("--Select Corporate Partner--", "0");
        //        //ddlCorpPart3.Items.Insert(0, item2);

        //        //ddlCorpPart4.DataSource = ds.Tables["Master"];
        //        //ddlCorpPart4.DataTextField = "Name";
        //        //ddlCorpPart4.DataValueField = "ID";
        //        //ddlCorpPart4.DataBind();
        //        //ListItem item3 = new ListItem("--Select Corporate Partner--", "0");
        //        //ddlCorpPart4.Items.Insert(0, item3);
        //    }
        //    catch (Exception ex)
        //    {
        //        ListItem item = new ListItem("--Select Corporate Partner--", "0");
        //        ddlCorpPartner.Items.Insert(0, item);

        //        //ListItem item1 = new ListItem("--Select Corporate Partner--", "0");
        //        //ddlCorpPart2.Items.Insert(0, item1);

        //        //ListItem item2 = new ListItem("--Select Corporate Partner--", "0");
        //        //ddlCorpPart3.Items.Insert(0, item2);

        //        //ListItem item3 = new ListItem("--Select Corporate Partner--", "0");
        //        //ddlCorpPart4.Items.Insert(0, item3);
        //    }
        //}

        private void FillWaitingPeriod()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(11);

                ddlWaitingPeriod.DataSource = ds.Tables["Master"];
                ddlWaitingPeriod.DataTextField = "Name";
                ddlWaitingPeriod.DataValueField = "ID";
                ddlWaitingPeriod.DataBind();
                ListItem item = new ListItem("--Select Waiting Period--", "0");
                ddlWaitingPeriod.Items.Insert(0, item);
                //ddlWaitingPeriod.ClearSelection();

                //ddlWaitPeriod2.DataSource = ds.Tables["Master"];
                //ddlWaitPeriod2.DataTextField = "Name";
                //ddlWaitPeriod2.DataValueField = "ID";
                //ddlWaitPeriod2.DataBind();
                //ListItem item1 = new ListItem("--Select Waiting Period--", "0");
                //ddlWaitPeriod2.Items.Insert(0, item1);
                //ddlWaitPeriod2.ClearSelection();

                //ddlWaitPeriod3.DataSource = ds.Tables["Master"];
                //ddlWaitPeriod3.DataTextField = "Name";
                //ddlWaitPeriod3.DataValueField = "ID";
                //ddlWaitPeriod3.DataBind();
                //ListItem item2 = new ListItem("--Select Waiting Period--", "0");
                //ddlWaitPeriod3.Items.Insert(0, item2);
                //ddlWaitPeriod3.ClearSelection();

                //ddlWaitPeriod4.DataSource = ds.Tables["Master"];
                //ddlWaitPeriod4.DataTextField = "Name";
                //ddlWaitPeriod4.DataValueField = "ID";
                //ddlWaitPeriod4.DataBind();
                //ListItem item3 = new ListItem("--Select Waiting Period--", "0");
                //ddlWaitPeriod4.Items.Insert(0, item3);
                //ddlWaitPeriod4.ClearSelection();
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Waiting Period--", "0");
                ddlWaitingPeriod.Items.Insert(0, item);

                //ListItem item1 = new ListItem("--Select Waiting Period--", "0");
                //ddlWaitPeriod2.Items.Insert(0, item1);

                //ListItem item2 = new ListItem("--Select Waiting Period--", "0");
                //ddlWaitPeriod3.Items.Insert(0, item2);

                //ListItem item3 = new ListItem("--Select Waiting Period--", "0");
                //ddlWaitPeriod4.Items.Insert(0, item3);
            }
        }

        private void FillProductType()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(10);

                //ddlProdType.DataSource = ds.Tables["Master"];
                //ddlProdType.DataTextField = "Name";
                //ddlProdType.DataValueField = "ID";
                //ddlProdType.DataBind();
                //ListItem item = new ListItem("--Select Product Type--", "0");
                //ddlProdType.Items.Insert(0, item);

                //ddlprodType2.DataSource = ds.Tables["Master"];
                //ddlprodType2.DataTextField = "Name";
                //ddlprodType2.DataValueField = "ID";
                //ddlprodType2.DataBind();
                //ListItem item1 = new ListItem("--Select Product Type--", "0");
                //ddlprodType2.Items.Insert(0, item1);

                //ddlProdType3.DataSource = ds.Tables["Master"];
                //ddlProdType3.DataTextField = "Name";
                //ddlProdType3.DataValueField = "ID";
                //ddlProdType3.DataBind();
                //ListItem item2 = new ListItem("--Select Product Type--", "0");
                //ddlProdType3.Items.Insert(0, item2);

                //ddlProdType4.DataSource = ds.Tables["Master"];
                //ddlProdType4.DataTextField = "Name";
                //ddlProdType4.DataValueField = "ID";
                //ddlProdType4.DataBind();
                //ListItem item3 = new ListItem("--Select Product Type--", "0");
                //ddlProdType4.Items.Insert(0, item3);
            }
            catch (Exception ex)
            {
                //ListItem item = new ListItem("--Select Product Type--", "0");
                //ddlProdType.Items.Insert(0, item);

                //ListItem item1 = new ListItem("--Select Product Type--", "0");
                //ddlprodType2.Items.Insert(0, item1);

                //ListItem item2 = new ListItem("--Select Product Type--", "0");
                //ddlProdType3.Items.Insert(0, item2);

                //ListItem item3 = new ListItem("--Select Product Type--", "0");
                //ddlProdType4.Items.Insert(0, item3);
            }
        }

        private void FillIllnessInjury()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(5);

                ddlInjury.DataSource = ds.Tables["Master"];
                ddlInjury.DataTextField = "Name";
                ddlInjury.DataValueField = "ID";
                ddlInjury.DataBind();
                ListItem item = new ListItem("--Select Illness or Injury--", "0");
                ddlInjury.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Illness or Injury--", "0");
                ddlInjury.Items.Insert(0, item);
            }
        }
        private void FillTypeOfVisit()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(15);

                ddlTypeOfVisit.DataSource = ds.Tables["Master"];
                ddlTypeOfVisit.DataTextField = "Name";
                ddlTypeOfVisit.DataValueField = "ID";
                ddlTypeOfVisit.DataBind();
                ListItem item = new ListItem("--Select Type of Visit--", "0");
                ddlTypeOfVisit.Items.Insert(0, item);

                ddlTypeOfVisitPop.DataSource = ds.Tables["Master"];
                ddlTypeOfVisitPop.DataTextField = "Name";
                ddlTypeOfVisitPop.DataValueField = "ID";
                ddlTypeOfVisitPop.DataBind();
                ddlTypeOfVisitPop.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Type of Visit--", "0");
                ddlTypeOfVisit.Items.Insert(0, item);
                ddlTypeOfVisitPop.Items.Insert(0, item);
            }
        }
        private void FillIncapacityDefinition()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(14);

                ddlIncapacityDefinition.DataSource = ds.Tables["Master"];
                ddlIncapacityDefinition.DataTextField = "Name";
                ddlIncapacityDefinition.DataValueField = "ID";
                ddlIncapacityDefinition.DataBind();
                ListItem item = new ListItem("--Select Incapacity Definition--", "0");
                ddlIncapacityDefinition.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Type of Visit--", "0");
                ddlTypeOfVisit.Items.Insert(0, item);
                ddlTypeOfVisitPop.Items.Insert(0, item);
            }
        }

        private void FillRehabilitationType()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(17);

                ddlRehabType.DataSource = ds.Tables["Master"];
                ddlRehabType.DataTextField = "Name";
                ddlRehabType.DataValueField = "ID";
                ddlRehabType.DataBind();
                ListItem item = new ListItem("--Select Type--", "0");
                ddlRehabType.Items.Insert(0, item);
             
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Type--", "0");
                ddlRehabType.Items.Insert(0, item);
                
            }
        }
        private void FillMetLifeStaticValue()
        {
            MetLifeStaticValue obj = new MetLifeStaticValue();
            DataSet ds = new DataSet();
            ds = obj.GetDetailMetLifeStaticValues();
            if (ds != null)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                  
                    txtTeamTelephone.Text = dt.Rows[0]["MetLifeTelephone"].ToString();
                    txtTeamEmail.Text = dt.Rows[0]["MetLifeEmail"].ToString();

                }
                else
                {
                    txtTeamTelephone.Text = string.Empty;
                    txtTeamEmail.Text = string.Empty;
                }
            }
            else
            {
                txtTeamTelephone.Text = string.Empty;
                txtTeamEmail.Text = string.Empty;
            }
        }
       
        private void FillSchemeName()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(19);

                ddlSchemeName.DataSource = ds.Tables["Master"];
                ddlSchemeName.DataTextField = "Name";
                ddlSchemeName.DataValueField = "ID";
                ddlSchemeName.DataBind();
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlSchemeName.Items.Insert(0, item);

            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Scheme Name--", "0");
                ddlSchemeName.Items.Insert(0, item);

            }
        }
        private void FillSchemeNumber()
        {
            try
            {
                //HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                //DataSet ds = mstrServReq.GetMasterList(20);

                //ddlMetSchemeNumber.DataSource = ds.Tables["Master"];
                //ddlMetSchemeNumber.DataTextField = "Name";
                //ddlMetSchemeNumber.DataValueField = "ID";
                //ddlMetSchemeNumber.DataBind();
                //ListItem item = new ListItem("--Select Scheme Number--", "0");
                //ddlMetSchemeNumber.Items.Insert(0, item);

            }
            catch (Exception ex)
            {
                //ListItem item = new ListItem("--Select Scheme Number--", "0");
                //ddlMetSchemeNumber.Items.Insert(0, item);

            }
        }
        private void FillRehabilitationProvider()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(18);

                ddlRehabProvider.DataSource = ds.Tables["Master"];
                ddlRehabProvider.DataTextField = "Name";
                ddlRehabProvider.DataValueField = "ID";
                ddlRehabProvider.DataBind();
                ListItem item = new ListItem("--Select Provider--", "0");
                ddlRehabProvider.Items.Insert(0, item);

            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Provider--", "0");
                ddlRehabProvider.Items.Insert(0, item);

            }
        }
        private void FillTypeOfCall()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(16);

                ddlTypeofCall.DataSource = ds.Tables["Master"];
                ddlTypeofCall.DataTextField = "Name";
                ddlTypeofCall.DataValueField = "ID";
                ddlTypeofCall.DataBind();
                ListItem item = new ListItem("--Select Type of Call--", "0");
                ddlTypeofCall.Items.Insert(0, item);
                ddlTypeofCallPop.DataSource = ds.Tables["Master"];
                ddlTypeofCallPop.DataTextField = "Name";
                ddlTypeofCallPop.DataValueField = "ID";
                ddlTypeofCallPop.DataBind();
                ddlTypeofCallPop.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Type of Call--", "0");
                ddlTypeofCall.Items.Insert(0, item);
                ddlTypeofCallPop.Items.Insert(0, item);
            }
        }
        private void FillClaimCloseReason()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(6);

                ddlClaimClosedReason.DataSource = ds.Tables["Master"];
                ddlClaimClosedReason.DataTextField = "Name";
                ddlClaimClosedReason.DataValueField = "ID";
                ddlClaimClosedReason.DataBind();
                ListItem item = new ListItem("--Select Claim Close Reason--", "0");
                ddlClaimClosedReason.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Claim Close Reason--", "0");
                ddlClaimClosedReason.Items.Insert(0, item);
            }
        }

        private void FillReasonClosed()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetMasterList(7);

                ddlReasonCancel.DataSource = ds.Tables["Master"];
                ddlReasonCancel.DataTextField = "Name";
                ddlReasonCancel.DataValueField = "ID";
                ddlReasonCancel.DataBind();
                ListItem item = new ListItem("--Select Reason Closed--", "0");
                ddlReasonCancel.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Reason Closed--", "0");
                ddlReasonCancel.Items.Insert(0, item);
            }
        }

        private void FillAssessorTeam()
        {
            try
            {
                HCBBLL.MasterListBL mstrServReq = new MasterListBL();
                DataSet ds = mstrServReq.GetAssessorList();

                ddlAssessorTeam.DataSource = ds.Tables["AssessorDetails"];
                ddlAssessorTeam.DataTextField = "Name";
                ddlAssessorTeam.DataValueField = "ID";
                ddlAssessorTeam.DataBind();
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlAssessorTeam.Items.Insert(0, item);
            }
            catch (Exception ex)
            {
                ListItem item = new ListItem("--Select Broker--", "0");
                ddlAssessorTeam.Items.Insert(0, item);
            }
        }

        private void Clear()
        {
            txtHCBReference.Text = "";
            txtFeeChargeHomeVisit.Text = string.Empty;
            txtClaimHandlerEmail.Text = "";
            txtClaimHandlerTelephone.Text = "";
            txtTeamEmail.Text = "";
            txtTeamTelephone.Text = "";
            txtClientFName.Text = "";
            txtClientSurname.Text = "";
            txtClientDOB.Text = "";
          //  txtClientAddress.Text = "";
            txtAddress1.Text="";
            txtAddress2.Text="";
            txtCity.Text="";
            txtCounty.Text="";
            txtClientMobileTelephone.Text = "";
            txtClientWorkTelephone.Text = "";
            txtClientPostCode.Text = "";
            txtOccupation.Text = "";
            txtPolicyNumber.Text = "";
            txtMonthlyBenefit.Text = "";
            txtEndBenefit.Text = "";
            //txtClaimRef2.Text = "";
            //txtMnthBenefit2.Text = "";
            //txtEndBenefit2.Text = "";
            //txtClaimRef3.Text = "";
            //txtMnthBenefit3.Text = "";
            //txtEndBenefit3.Text = "";
            //txtClaimRef4.Text = "";
            //txtMnthBenefit4.Text = "";
            //txtEndBenefit4.Text = "";
            txtIncapacityDay.Text = "";
            txtDisabilityType.Text = "";
            txtICD9Code.Text = "";
            txtClaimCloseDate.Text = "";
            txtClaimDetails.Text = "";
            txtAppointmentDate.Text = "";
            txtDateReportComp.Text = "";
            txtCall1Date.Text = "";
            txtCall1Time.Text = "";
            //txtFailedCallDate.Text = "";
            //txtFailedCallTime.Text = "";
            //txtCall2RTWDateOpt.Text = "";
            txtNxtCallSchd.Text = "";
            //txtCall2Date.Text = "";
            //txtCall2Time.Text = "";
            //txtCall2RTWDateOpt.Text = "";
            //txtEnvisagedRTWDate.Text = "";
            //txtCall2NxtSchDate.Text = "";
            //txtFailCall2Date.Text = "";
            //txtFailCall2Time.Text = "";
            //txtCall3Date.Text = "";
            //txtCall3Time.Text = "";
            //txtCall3RTWDateOpt.Text = "";
            //txtFailCall3Date.Text = "";
            //txtFailCall3Time.Text = "";
            //txtRTWDateAgreed.Text = "";
            txtAddNote.Text = "";
            txtHCBReceivedDate.Text = "";
            txtDateSentToNurse.Text = "";
            txtCaseClosed.Text = "";
            txtFeeCharged.Text = "";
            txtRehabcost.Text = "";
            //txtClaimCloseReason.Text = string.Empty;

            ddlCaseMngr.SelectedValue = "0";
            ddlAssessorTeam.SelectedValue = "0";
            ddlClaimAssessor.SelectedValue = "0";
            //ddlServiceReq.SelectedIndex = 0;
            //lstServiceReq.SelectedIndex = 0;
            ddlGender.SelectedValue = "0";
            ddlTypeOfVisit.SelectedValue = "0";
            //ddlEmpStatus.SelectedIndex = 0;
            //ddlBrand.SelectedIndex = 0;
            //ddlCorpPartner.SelectedIndex = 0;
            ddlWaitingPeriod.SelectedIndex = 0;
            //ddlProdType.SelectedIndex = 0;
            ddlIncapacityDefinition.SelectedValue = "0";
            //ddlprodType2.SelectedIndex = 0;
            //ddlCorpPart2.SelectedIndex = 0;
            //ddlWaitPeriod2.SelectedIndex = 0;
            //ddlCorpPart3.SelectedIndex = 0;
            //ddlWaitPeriod3.SelectedIndex = 0;
            //ddlProdType3.SelectedIndex = 0;
            //ddlCorpPart4.SelectedIndex = 0;
            //ddlWaitPeriod4.SelectedIndex = 0;
            //ddlProdType4.SelectedIndex = 0;
            ddlInjury.SelectedIndex = 0;
            ddlClaimClosedReason.SelectedIndex = 0;
            ddlCaseMngtRecom.SelectedIndex = 0;
            //ddlCaseMngtRecom2.SelectedIndex = 0;
            //ddlRTWReal.SelectedIndex = 0;
            //ddlFurtherInterReq.SelectedIndex = 0;
            //ddlCaseMngtInvolve3.SelectedIndex = 0;
            //ddlRehabServPurchase3.SelectedIndex = 0;
            ddlReasonCancel.SelectedIndex = 0;

            lstFailedCalls.Text = "";
            //lstFailCall2.Text = "";
            //lstFailCall3.Text = "";

            lnkAddFailedCall.Enabled = false;
            //lnkFailCall2.Enabled = false;
            //lnkFailCall3.Enabled = false;
            lnkRemoveFailCall1.Enabled = false;
            // lnkRemoveFailCall3.Enabled = false;

            btnSaveNote.Enabled = false;
            chkSendEmail.Enabled = false;
            btnSave.Text = "Save Record";
            Session["NewHCBRef"] = null;
        }

        private void DisableNonAdminControls()
        {
            txtHCBReceivedDate.Enabled=true;
            txtHCBReceivedDate.Font.Bold=false;
            txtHCBReceivedDate.ForeColor=System.Drawing.Color.Black;

            txtDateSentToNurse.Enabled = true;
            txtDateSentToNurse.Font.Bold=false;
            txtDateSentToNurse.ForeColor=System.Drawing.Color.Black;

            txtCaseCancelled.Enabled=true;
            txtCaseCancelled.Font.Bold=false;
            txtCaseCancelled.ForeColor=System.Drawing.Color.Black;


             txtCaseClosed.Enabled = true;
             txtCaseClosed.Font.Bold=false;
             txtCaseClosed.ForeColor=System.Drawing.Color.Black;



             txtFeeCharged.Enabled = true;
             txtFeeCharged.Font.Bold = true;
             txtFeeCharged.ForeColor = System.Drawing.Color.DarkGray;


             txtRehabcost.Enabled = false;
             txtRehabcost.ForeColor = System.Drawing.Color.DarkGray;
             txtRehabcost.Font.Bold = true;


             txttotalcasecost.Enabled=false;
             txttotalcasecost.ForeColor=System.Drawing.Color.DarkGray;
             txttotalcasecost.Font.Bold=true;
            txtNxtCallSchd.Enabled = false;
            txtNxtCallSchd.Font.Bold = true;
            txtNxtCallSchd.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateMax.Enabled = false;
            txtRTWDateMax.Font.Bold = true;
            txtRTWDateMax.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateOptimum.Enabled = false;
            txtRTWDateOptimum.Font.Bold = true;
            txtRTWDateOptimum.ForeColor = System.Drawing.Color.DarkGray;

            txtNxtCallSchdPop.Enabled = false;
            txtNxtCallSchdPop.Font.Bold = true;
            txtNxtCallSchdPop.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateMaxPop.Enabled = false;
            txtRTWDateMaxPop.Font.Bold = true;
            txtRTWDateMaxPop.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateOptimumPop.Enabled = false;
            txtRTWDateOptimumPop.Font.Bold = true;
            txtRTWDateOptimumPop.ForeColor = System.Drawing.Color.DarkGray;

            txtAppointmentDate.Enabled = false;
            txtAppointmentDate.Font.Bold = true;
            txtAppointmentDate.ForeColor = System.Drawing.Color.DarkGray;

            txtDateOfAppointmentPop.Enabled = false;
            txtDateOfAppointmentPop.Font.Bold = true;
            txtDateOfAppointmentPop.ForeColor = System.Drawing.Color.DarkGray;

            txtSchemeNumber.Enabled = false;
            txtSchemeNumber.Font.Bold = true;
            txtSchemeNumber.ForeColor = System.Drawing.Color.DarkGray;

            ddlTypeOfVisitPop.Attributes.Add("style", "font-weight:bold");
            ddlTypeOfVisitPop.Enabled = false;

            ddlTypeOfVisit.Attributes.Add("style", "font-weight:bold");
            ddlTypeOfVisit.Enabled = false;

            ddlAssessorTeam.Attributes.Add("style", "font-weight:bold");
            ddlAssessorTeam.Enabled = false;

            ddlClaimAssessor.Attributes.Add("style", "font-weight:bold");
            ddlClaimAssessor.Enabled = false;

            //ddlServiceReq.Attributes.Add("style", "font-weight:bold");
            //ddlServiceReq.Enabled = false;
            //lstServiceReq.Attributes.Add("style", "font-weight:bold");
            //this.lstServiceReq.Attributes.Add("disabled", "");
            //lstServiceReq.Enabled = false;

            //ddlEmpStatus.Attributes.Add("style", "font-weight:bold");
            //ddlEmpStatus.Enabled = false;

            //ddlBrand.Attributes.Add("style", "font-weight:bold");
            //ddlBrand.Enabled = false;

            ddlGender.Attributes.Add("style", "font-weight:bold");
            ddlGender.Enabled = false;

            //ddlCorpPartner.Attributes.Add("style", "font-weight:bold");
            //ddlCorpPartner.Enabled = false;

            ddlWaitingPeriod.Attributes.Add("style", "font-weight:bold");
            ddlWaitingPeriod.Enabled = false;

            ddlTypeOfVisit.Attributes.Add("style", "font-weight:bold");
            ddlTypeOfVisit.Enabled = false;

            //ddlProdType.Attributes.Add("style", "font-weight:bold");
            //ddlProdType.Enabled = false;

            ddlIncapacityDefinition.Attributes.Add("style", "font-weight:bold");
            ddlIncapacityDefinition.Enabled = false;

            ddlSchemeName.Attributes.Add("style", "font-weight:bold");
            ddlSchemeName.Enabled = false;

            //ddlMetSchemeNumber.Attributes.Add("style", "font-weight:bold");
            //ddlMetSchemeNumber.Enabled = false;

            ddlInjury.Attributes.Add("style", "font-weight:bold");
            ddlInjury.Enabled = false;

            ddlClaimClosedReason.Attributes.Add("style", "font-weight:bold");
            ddlClaimClosedReason.Enabled = false;

            //ddlCaseMngtRecom.Attributes.Add("style", "font-weight:bold");
           // ddlCaseMngtRecom.Enabled = false;

            //ddlCaseMngtRecom2.Attributes.Add("style", "font-weight:bold");
            //ddlCaseMngtRecom2.Enabled = false;

            //ddlRTWReal.Attributes.Add("style", "font-weight:bold");
            //ddlRTWReal.Enabled = false;

            //ddlFurtherInterReq.Attributes.Add("style", "font-weight:bold");
            //ddlFurtherInterReq.Enabled = false;

            //ddlCaseMngtInvolve3.Attributes.Add("style", "font-weight:bold");
            //ddlCaseMngtInvolve3.Enabled = false;

            //ddlRehabServPurchase3.Attributes.Add("style", "font-weight:bold");
            //ddlRehabServPurchase3.Enabled = false;

            //ddlCorpPart2.Attributes.Add("style", "font-weight:bold");
            //ddlCorpPart2.Enabled = false;

            //ddlWaitPeriod2.Attributes.Add("style", "font-weight:bold");
            //ddlWaitPeriod2.Enabled = false;

            //ddlprodType2.Attributes.Add("style", "font-weight:bold");
            //ddlprodType2.Enabled = false;

            //ddlCorpPart3.Attributes.Add("style", "font-weight:bold");
            //ddlCorpPart3.Enabled = false;

            //ddlWaitPeriod3.Attributes.Add("style", "font-weight:bold");
            //ddlWaitPeriod3.Enabled = false;

            //ddlProdType3.Attributes.Add("style", "font-weight:bold");
            //ddlProdType3.Enabled = false;

            //ddlCorpPart4.Attributes.Add("style", "font-weight:bold");
            //ddlCorpPart4.Enabled = false;

            //ddlWaitPeriod4.Attributes.Add("style", "font-weight:bold");
            //ddlWaitPeriod4.Enabled = false;

            //ddlProdType4.Attributes.Add("style", "font-weight:bold");
            //ddlProdType4.Enabled = false;
            //ddlDocuments.Enabled = false;

            //btnAddMoreDet1.Enabled = false;
            //btnAddMoreDet2.Enabled = false;
            //btnAddMoreDet3.Enabled = false;

            txtClaimHandlerEmail.Enabled = false;
            txtClaimHandlerEmail.ForeColor = System.Drawing.Color.DarkGray;
            txtClaimHandlerEmail.Font.Bold = true;

            txtTeamOther.Enabled = false;
            txtTeamOther.ForeColor = System.Drawing.Color.DarkGray;
            txtTeamOther.Font.Bold = true;

            //txtClaimCloseReason.Enabled = false;
            //txtClaimCloseReason.ForeColor = System.Drawing.Color.DarkGray;
            //txtClaimCloseReason.Font.Bold = true;


            txtClaimHandlerTelephone.Enabled = false;
            txtClaimHandlerTelephone.ForeColor = System.Drawing.Color.DarkGray;
            txtClaimHandlerTelephone.Font.Bold = true;

            txtTeamEmail.Enabled = false;
            txtTeamEmail.ForeColor = System.Drawing.Color.DarkGray;
            txtTeamEmail.Font.Bold = true;

            txtTeamTelephone.Enabled = false;
            txtTeamTelephone.ForeColor = System.Drawing.Color.DarkGray;
            txtTeamTelephone.Font.Bold = true;

            txtClientFName.Enabled = false;
            txtClientFName.ForeColor = System.Drawing.Color.DarkGray;
            txtClientFName.Font.Bold = true;

            //ddlSalutation.Enabled = false;
            //ddlSalutation.Attributes.Add("style", "font-weight:bold");

            txtClientMobileTelephone.Enabled = false;
            txtClientMobileTelephone.ForeColor = System.Drawing.Color.DarkGray;
            txtClientMobileTelephone.Font.Bold = true;



            txtClientPostCode.Enabled = false;
            txtClientPostCode.ForeColor = System.Drawing.Color.DarkGray;
            txtClientPostCode.Font.Bold = true;

            txtEmployerContract.Enabled = false;
            txtEmployerContract.ForeColor = System.Drawing.Color.DarkGray;
            txtEmployerContract.Font.Bold = true;

            txtEmployerName.Enabled = false;
            txtEmployerName.ForeColor = System.Drawing.Color.DarkGray;
            txtEmployerName.Font.Bold = true;

            txtClientSurname.Enabled = false;
            txtClientSurname.ForeColor = System.Drawing.Color.DarkGray;
            txtClientSurname.Font.Bold = true;

            txtClientWorkTelephone.Enabled = false;
            txtClientWorkTelephone.ForeColor = System.Drawing.Color.DarkGray;
            txtClientWorkTelephone.Font.Bold = true;

            txtClientDOB.Enabled = false;
            txtClientDOB.ForeColor = System.Drawing.Color.DarkGray;
            txtClientDOB.Font.Bold = true;

           // txtClientAddress.Enabled = false;
          //  txtClientAddress.ForeColor = System.Drawing.Color.DarkGray;
           // txtClientAddress.Font.Bold = true;


           txtAddress1.Enabled=false;
           txtAddress1.ForeColor=System.Drawing.Color.DarkGray;
           txtAddress1.Font.Bold=true;

           txtAddress2.Enabled = false;
           txtAddress2.ForeColor = System.Drawing.Color.DarkGray;
           txtAddress2.Font.Bold = true;
           
           txtCity.Enabled=false;
           txtCity.ForeColor=System.Drawing.Color.DarkGray;
           txtCity.Font.Bold=true;

           txtservicerequiredother.Enabled = false;
           txtservicerequiredother.ForeColor = System.Drawing.Color.DarkGray;
           txtservicerequiredother.Font.Bold = true;

           txtCounty.Enabled=false;
           txtCounty.ForeColor=System.Drawing.Color.DarkGray;
           txtCounty.Font.Bold=true;

            txtOccupation.Enabled = false;
            txtOccupation.ForeColor = System.Drawing.Color.DarkGray;
            txtOccupation.Font.Bold = true;

            txtPolicyNumber.Enabled = false;
            txtPolicyNumber.ForeColor = System.Drawing.Color.DarkGray;
            txtPolicyNumber.Font.Bold = true;

            txtDeferredOther.Enabled = false;
            txtDeferredOther.ForeColor = System.Drawing.Color.DarkGray;
            txtDeferredOther.Font.Bold = true;

            txtMonthlyBenefit.Enabled = false;
            txtMonthlyBenefit.ForeColor = System.Drawing.Color.DarkGray;
            txtMonthlyBenefit.Font.Bold = true;

            txtEndBenefit.Enabled = false;
            txtEndBenefit.ForeColor = System.Drawing.Color.DarkGray;
            txtEndBenefit.Font.Bold = true;

            txtIncapacityDay.Enabled = false;
            txtIncapacityDay.ForeColor = System.Drawing.Color.DarkGray;
            txtIncapacityDay.Font.Bold = true;

            txtclaimreasonother.Enabled = false;
            txtclaimreasonother.Font.Bold = true;
            txtclaimreasonother.ForeColor = System.Drawing.Color.DarkGray;

            txtillnessinjuryother.Enabled = false;
            txtillnessinjuryother.ForeColor = System.Drawing.Color.DarkGray;
            txtillnessinjuryother.Font.Bold = true;

            txtDisabilityType.Enabled = false;
            txtDisabilityType.ForeColor = System.Drawing.Color.DarkGray;
            txtDisabilityType.Font.Bold = true;

            txtICD9Code.Enabled = false;
            txtICD9Code.ForeColor = System.Drawing.Color.DarkGray;
            txtICD9Code.Font.Bold = true;

            txtClaimCloseDate.Enabled = false;
            txtClaimCloseDate.ForeColor = System.Drawing.Color.DarkGray;
            txtClaimCloseDate.Font.Bold = true;

            txtClaimDetails.Enabled = false;
            txtClaimDetails.ForeColor = System.Drawing.Color.DarkGray;
            txtClaimDetails.Font.Bold = true;

            txtAppointmentDate.Enabled = false;
            txtAppointmentDate.ForeColor = System.Drawing.Color.DarkGray;
            txtAppointmentDate.Font.Bold = true;

            txtFeeChargeHomeVisit.Enabled = true;
           // txtFeeChargeHomeVisit.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeChargeHomeVisit.Font.Bold = true;

            //lnkAdd.Enabled = false;
            //grdHomeVisit.Enabled = false;

            
            //pnlTelephoneIntervention.Enabled = false;
            //pnlGridTelephoneIntervention.Enabled = false;

            txtDateReportComp.Enabled = false;
            txtDateReportComp.ForeColor = System.Drawing.Color.DarkGray;
            txtDateReportComp.Font.Bold = true;

            //txtCall1Date.Enabled = false;
            //txtCall1Date.ReadOnly = true;

            //txtCall1Date.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall1Date.Font.Bold = true;

            //txtCall1Time.Enabled = false;
            //txtCall1Time.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall1Time.Font.Bold = true;

           // cbCall1Aborted.Enabled = false;
            btnSaveVisit.Enabled = false;
            //txtFailedCallDate.Enabled = false;
            //txtFailedCallTime.Enabled = false;

            //txtRTWDateOptimum.Enabled = false;
           // txtRTWDateOptimum.ForeColor = System.Drawing.Color.DarkGray;
            //txtRTWDateOptimum.Font.Bold = true;

           // txtNxtCallSchd.Enabled = false;
           // txtNxtCallSchd.ForeColor = System.Drawing.Color.DarkGray;
            //txtNxtCallSchd.Font.Bold = true;

            //txtCall2Date.Enabled = false;
            //txtCall2Date.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2Date.Font.Bold = true;

            //cbCall2Aborted.Enabled = false;

            //txtFailCall2Date.Enabled = false;
            //txtFailCall2Time.Enabled = false;
            //txtCall2NxtSchDate.Enabled = false;
            //txtCall2NxtSchDate.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2NxtSchDate.Font.Bold = true;

            //txtCall2RTWDateOpt.Enabled = false;
            //txtCall2RTWDateOpt.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2RTWDateOpt.Font.Bold = true;

            //txtCall2Time.Enabled = false;
            //txtCall2Time.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2Time.Font.Bold = true;

            //txtCall3Date.Enabled = false;
            //txtCall3Date.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall3Date.Font.Bold = true;

            //cbCall3Aborted.Enabled = false;

            //txtFailCall3Date.Enabled = false;
            //txtFailCall3Time.Enabled = false;
            //txtCall3RTWDateOpt.Enabled = false;
            //txtCall3RTWDateOpt.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall3RTWDateOpt.Font.Bold = true;

            //txtCall3Time.Enabled = false;
            //txtCall3Time.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall3Time.Font.Bold = true;

            //txtEnvisagedRTWDate.Enabled = false;
            //txtEnvisagedRTWDate.ForeColor = System.Drawing.Color.DarkGray;
            //txtEnvisagedRTWDate.Font.Bold = true;

            //txtRTWDateAgreed.Enabled = false;
            //txtRTWDateAgreed.ForeColor = System.Drawing.Color.DarkGray;
            //txtRTWDateAgreed.Font.Bold = true;

            //txtClaimRef2.Enabled = false;
            //txtClaimRef2.ForeColor = System.Drawing.Color.DarkGray;
            //txtClaimRef2.Font.Bold = true;

            //txtMnthBenefit2.Enabled = false;
            //txtMnthBenefit2.ForeColor = System.Drawing.Color.DarkGray;
            //txtMnthBenefit2.Font.Bold = true;

            //txtEndBenefit2.Enabled = false;
            //txtEndBenefit2.ForeColor = System.Drawing.Color.DarkGray;
            //txtEndBenefit2.Font.Bold = true;

            //txtClaimRef3.Enabled = false;
            //txtClaimRef3.ForeColor = System.Drawing.Color.DarkGray;
            //txtClaimRef3.Font.Bold = true;

            //txtMnthBenefit3.Enabled = false;
            //txtMnthBenefit3.ForeColor = System.Drawing.Color.DarkGray;
            //txtMnthBenefit3.Font.Bold = true;

            //txtEndBenefit3.Enabled = false;
            //txtEndBenefit3.ForeColor = System.Drawing.Color.DarkGray;
            //txtEndBenefit3.Font.Bold = true;

            //txtClaimRef4.Enabled = false;
            //txtClaimRef4.ForeColor = System.Drawing.Color.DarkGray;
            //txtClaimRef4.Font.Bold = true;

            //txtMnthBenefit4.Enabled = false;
            //txtMnthBenefit4.ForeColor = System.Drawing.Color.DarkGray;
            //txtMnthBenefit4.Font.Bold = true;

            //txtEndBenefit4.Enabled = false;
            //txtEndBenefit4.ForeColor = System.Drawing.Color.DarkGray;
            //txtEndBenefit4.Font.Bold = true;

            ddlReasonCancel.Enabled = true;
           // lstFailedCalls.Enabled = false;
            //lstFailedCalls.ForeColor = System.Drawing.Color.DarkGray;
            //lstFailedCalls.Font.Bold = true;

            //lstFailCall2.Enabled = false;
            //lstFailCall2.ForeColor = System.Drawing.Color.DarkGray;
            //lstFailCall2.Font.Bold = true;

            //lstFailCall3.Enabled = false;
            //lstFailCall3.ForeColor = System.Drawing.Color.DarkGray;
            //lstFailCall3.Font.Bold = true;
            txtFeeCharge.Enabled = true;
           // txtFeeCharge.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeCharge.Font.Bold = true;


            txtFeeChargedTelPop.Enabled = true;
            txtFeeChargedTelPop.Font.Bold = true;



            txtFeeChargedPop.Enabled = true;
            txtFeeChargedPop.Font.Bold = true;
          //  txtFeeChargedPop.ForeColor = System.Drawing.Color.DarkGray;


            txtRehabFeeCharged.Enabled = true;
          //  txtRehabFeeCharged.ForeColor = System.Drawing.Color.DarkGray;

            txtRehabFeeCharged.Font.Bold = true;



        }

        private void DisableNonIncoControls()
        {


            txtDateReported.Enabled = false;
            txtDateReported.Font.Bold = true;
            txtDateReported.ForeColor = System.Drawing.Color.DarkGray;
            txtCaseCancelled.Enabled = false;
            txtCaseCancelled.Font.Bold=true;
            txtCaseCancelled.ForeColor=System.Drawing.Color.DarkGray;


            txttotalcasecost.Enabled = false;
            txttotalcasecost.ForeColor = System.Drawing.Color.DarkGray;
            txttotalcasecost.Font.Bold = true;

            txtDateInstructed.Enabled = false;
            txtDateInstructed.Font.Bold = true;
            txtDateInstructed.ForeColor = System.Drawing.Color.DarkGray;

            txtNxtCallSchd.Enabled = false;
            txtNxtCallSchd.Font.Bold = true;
            txtNxtCallSchd.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateMax.Enabled = false;
            txtRTWDateMax.Font.Bold = true;
            txtRTWDateMax.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateOptimum.Enabled = false;
            txtRTWDateOptimum.Font.Bold = true;
            txtRTWDateOptimum.ForeColor = System.Drawing.Color.DarkGray;

            txtNxtCallSchdPop.Enabled = false;
            txtNxtCallSchdPop.Font.Bold = true;
            txtNxtCallSchdPop.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateMaxPop.Enabled = false;
            txtRTWDateMaxPop.Font.Bold = true;
            txtRTWDateMaxPop.ForeColor = System.Drawing.Color.DarkGray;

            txtRTWDateOptimumPop.Enabled = false;
            txtRTWDateOptimumPop.Font.Bold = true;
            txtRTWDateOptimumPop.ForeColor = System.Drawing.Color.DarkGray;

            txtAppointmentDate.Enabled = false;
            txtAppointmentDate.Font.Bold = true;
            txtAppointmentDate.ForeColor = System.Drawing.Color.DarkGray;

            txtDateOfAppointmentPop.Enabled = false;
            txtDateOfAppointmentPop.Font.Bold = true;
            txtDateOfAppointmentPop.ForeColor = System.Drawing.Color.DarkGray;

            ddlTypeOfVisitPop.Attributes.Add("style", "font-weight:bold");
            ddlTypeOfVisitPop.Enabled = false;

            ddlTypeOfVisit.Attributes.Add("style", "font-weight:bold");
            ddlTypeOfVisit.Enabled = false;


            txtDateReportComp.Enabled = false;
            txtDateReportComp.Font.Bold = true;
            txtDateReportComp.ForeColor = System.Drawing.Color.DarkGray;

            //txtFeeChargeHomeVisit.Enabled = false;
            //txtFeeChargeHomeVisit.Font.Bold = true;
            //txtFeeChargeHomeVisit.ForeColor = System.Drawing.Color.DarkGray;

            txtDateReportCompPop.Enabled = false;
            txtDateReportCompPop.Font.Bold = true;
            txtDateReportCompPop.ForeColor = System.Drawing.Color.DarkGray;

            txtFeeChargedPop.Enabled = false;
            txtFeeChargedPop.Font.Bold = true;
            txtFeeChargedPop.ForeColor = System.Drawing.Color.DarkGray;

            ddlCaseMngr.Attributes.Add("style", "font-weight:bold");
            ddlCaseMngr.Enabled = false;

            //ddlCaseMngtRecom.Attributes.Add("style", "font-weight:bold");
            //ddlCaseMngtRecom.Enabled = false;

            //ddlCaseMngtRecom2.Attributes.Add("style", "font-weight:bold");
            //ddlCaseMngtRecom2.Enabled = false;

            //ddlRTWReal.Attributes.Add("style", "font-weight:bold");
            //ddlRTWReal.Enabled = false;

            //ddlFurtherInterReq.Attributes.Add("style", "font-weight:bold");
            //ddlFurtherInterReq.Enabled = false;

            //ddlCaseMngtInvolve3.Attributes.Add("style", "font-weight:bold");
            //ddlCaseMngtInvolve3.Enabled = false;

            //ddlRehabServPurchase3.Attributes.Add("style", "font-weight:bold");
            //ddlRehabServPurchase3.Enabled = false;

            ddlReasonCancel.Attributes.Add("style", "font-weight:bold");
            ddlReasonCancel.Enabled = false;

            //btnRehabModify.Enabled = false;
            btnSaveVisit.Enabled = false;
            txtFeeChargeHomeVisit.Enabled = false;
            txtFeeChargeHomeVisit.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeChargeHomeVisit.Font.Bold = true;

            ddlTypeOfVisit.Attributes.Add("style", "font-weight:bold");
            ddlTypeOfVisit.Enabled = false;
            //ddlDocuments.Attributes.Add("style", "font-weight:bold");
            //ddlDocuments.Enabled = false;

            txtAppointmentDate.Enabled = false;
            txtAppointmentDate.ForeColor = System.Drawing.Color.DarkGray;
            txtAppointmentDate.Font.Bold = true;

            txtDateReportComp.Enabled = false;
            txtDateReportComp.ForeColor = System.Drawing.Color.DarkGray;
            txtDateReportComp.Font.Bold = true;

            //txtCall1Date.Enabled = false;
            //txtCall1Date.ForeColor = System.Drawing.Color.DarkGray;
           // txtCall1Date.Font.Bold = true;

            //txtCall1Time.Enabled = false;
            //txtCall1Time.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall1Time.Font.Bold = true;

            //txtFailedCallDate.Enabled = false;
            //txtFailedCallTime.Enabled = false;
            //txtRTWDateOptimum.Enabled = false;
            //txtRTWDateOptimum.ForeColor = System.Drawing.Color.DarkGray;
           // txtRTWDateOptimum.Font.Bold = true;

            //txtNxtCallSchd.Enabled = false;
            //txtNxtCallSchd.ForeColor = System.Drawing.Color.DarkGray;
            //txtNxtCallSchd.Font.Bold = true;

            //txtCall2Date.Enabled = false;
            //txtCall2Date.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2Date.Font.Bold = true;

            //txtFailCall2Time.Enabled = false;
            //txtFailCall2Date.Enabled = false;
            //txtCall2NxtSchDate.Enabled = false;
            //txtCall2NxtSchDate.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2NxtSchDate.Font.Bold = true;

            //txtCall2RTWDateOpt.Enabled = false;
            //txtCall2RTWDateOpt.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2RTWDateOpt.Font.Bold = true;

            //txtCall2Time.Enabled = false;
            //txtCall2Time.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall2Time.Font.Bold = true;

            //txtCall3Date.Enabled = false;
            //txtCall3Date.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall3Date.Font.Bold = true;

            //txtFailCall3Date.Enabled = false;
            //txtFailCall3Time.Enabled = false;
            //txtCall3RTWDateOpt.Enabled = false;
            //txtCall3RTWDateOpt.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall3RTWDateOpt.Font.Bold = true;

            //txtCall3Time.Enabled = false;
            //txtCall3Time.ForeColor = System.Drawing.Color.DarkGray;
            //txtCall3Time.Font.Bold = true;

            //txtEnvisagedRTWDate.Enabled = false;
            //txtEnvisagedRTWDate.ForeColor = System.Drawing.Color.DarkGray;
            //txtEnvisagedRTWDate.Font.Bold = true;

            //txtRTWDateAgreed.Enabled = false;
            //txtRTWDateAgreed.ForeColor = System.Drawing.Color.DarkGray;
            //txtRTWDateAgreed.Font.Bold = true;

            //txtEnvisagedRTWDate.Enabled = false;
            //txtEnvisagedRTWDate.ForeColor = System.Drawing.Color.DarkGray;
            //txtEnvisagedRTWDate.Font.Bold = true;

            //txtRTWDateAgreed.Enabled = false;
            //txtRTWDateAgreed.ForeColor = System.Drawing.Color.DarkGray;
            //txtRTWDateAgreed.Font.Bold = true;

            txtHCBReceivedDate.Enabled = false;
            txtHCBReceivedDate.ForeColor = System.Drawing.Color.DarkGray;
            txtHCBReceivedDate.Font.Bold = true;

            txtDateSentToNurse.Enabled = false;
            txtDateSentToNurse.ForeColor = System.Drawing.Color.DarkGray;
            txtDateSentToNurse.Font.Bold = true;

            txtCaseClosed.Enabled = false;
            txtCaseClosed.ForeColor = System.Drawing.Color.DarkGray;
            txtCaseClosed.Font.Bold = true;

            txtFeeCharged.Enabled = false;
            txtFeeCharged.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeCharged.Font.Bold = true;

            txtRehabcost.Enabled = false;
            txtRehabcost.ForeColor = System.Drawing.Color.DarkGray;
            txtRehabcost.Font.Bold = true;

            txtFeeCharge.Enabled = false;
            txtFeeCharge.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeCharge.Font.Bold = true;
            //Jaywanti on 11-04-2014
            //lstFailedCalls.Enabled = false;
            //lstFailedCalls.ForeColor = System.Drawing.Color.DarkGray;
            //lstFailedCalls.Font.Bold = true;
            //

            //lstFailCall2.Enabled = false;
            //lstFailCall2.ForeColor = System.Drawing.Color.DarkGray;
            //lstFailCall2.Font.Bold = true;


            //lstFailCall3.Enabled = false;
            //lstFailCall3.ForeColor = System.Drawing.Color.DarkGray;
            //lstFailCall3.Font.Bold = true;

            //cbCall1Aborted.Enabled = false;

            //lnkAdd.Enabled = false;
            //grdHomeVisit.Enabled = false;
            
           // pnlTelephoneIntervention.Enabled = false;
            //pnlGridTelephoneIntervention.Enabled = false;
            //cbCall2Aborted.Enabled = false;
            // cbCall3Aborted.Enabled = false;

            txtFeeChargedTelPop.Enabled = false;
            txtFeeChargedTelPop.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeChargedTelPop.Font.Bold = true;

            txtRehabFeeCharged.Enabled = false;
            txtRehabFeeCharged.ForeColor = System.Drawing.Color.DarkGray;

            txtRehabFeeCharged.Font.Bold = true;



        }

        private void DisableNonNurseControls()
        {
            txtDateReportComp.Enabled = false;
            txtDateReportComp.Font.Bold = true;
            txtDateReportComp.ForeColor = System.Drawing.Color.DarkGray;

            txtCaseCancelled.Enabled = false;
            txtCaseCancelled.Font.Bold = true;
            txtCaseCancelled.ForeColor = System.Drawing.Color.DarkGray;


            txttotalcasecost.Enabled = false;
            txttotalcasecost.ForeColor = System.Drawing.Color.DarkGray;
            txttotalcasecost.Font.Bold = true;
            txtFeeChargeHomeVisit.Enabled = false;
            txtFeeChargeHomeVisit.Font.Bold = true;
            txtFeeChargeHomeVisit.ForeColor = System.Drawing.Color.DarkGray;

            txtDateReportCompPop.Enabled = false;
            txtDateReportCompPop.Font.Bold = true;
            txtDateReportCompPop.ForeColor = System.Drawing.Color.DarkGray;

            txtFeeChargedPop.Enabled = false;
            txtFeeChargedPop.Font.Bold = true;
            txtFeeChargedPop.ForeColor = System.Drawing.Color.DarkGray;

            ddlCaseMngr.Attributes.Add("style", "font-weight:bold");
            ddlCaseMngr.Enabled = false;

            ddlAssessorTeam.Attributes.Add("style", "font-weight:bold");
            ddlAssessorTeam.Enabled = false;

            ddlClaimAssessor.Attributes.Add("style", "font-weight:bold");
            ddlClaimAssessor.Enabled = false;

            txtSchemeNumber.Enabled = false;
            txtSchemeNumber.Font.Bold = true;
            txtSchemeNumber.ForeColor = System.Drawing.Color.DarkGray;

            //ddlServiceReq.Attributes.Add("style", "font-weight:bold");
            //ddlServiceReq.Enabled = false;
            //lstServiceReq.Attributes.Add("style", "font-weight:bold");
            //this.lstServiceReq.Attributes.Add("disabled", "");
            //lstServiceReq.Enabled = false;

            //ddlEmpStatus.Attributes.Add("style", "font-weight:bold");
            //ddlEmpStatus.Enabled = false;

            ddlGender.Attributes.Add("style", "font-weight:bold");
            ddlGender.Enabled = false;

            //ddlBrand.Attributes.Add("style", "font-weight:bold");
            //ddlBrand.Enabled = false;

            //ddlCorpPartner.Attributes.Add("style", "font-weight:bold");
            //ddlCorpPartner.Enabled = false;

            ddlWaitingPeriod.Attributes.Add("style", "font-weight:bold");
            ddlWaitingPeriod.Enabled = false;

            //ddlProdType.Attributes.Add("style", "font-weight:bold");
            //ddlProdType.Enabled = false;

            ddlIncapacityDefinition.Attributes.Add("style", "font-weight:bold");
            ddlIncapacityDefinition.Enabled = false;
            ddlSchemeName.Attributes.Add("style", "font-weight:bold");
            ddlSchemeName.Enabled = false;

            //ddlMetSchemeNumber.Attributes.Add("style", "font-weight:bold");
            //ddlMetSchemeNumber.Enabled = false;

            ddlInjury.Attributes.Add("style", "font-weight:bold");
            ddlInjury.Enabled = false;

            ddlClaimClosedReason.Attributes.Add("style", "font-weight:bold");
            ddlClaimClosedReason.Enabled = false;

            ddlReasonCancel.Attributes.Add("style", "font-weight:bold");
            ddlReasonCancel.Enabled = false;

            //btnRehabModify.Enabled = false;
            //ddlCorpPart2.Enabled = false;
            //ddlWaitPeriod2.Enabled = false;
            //ddlprodType2.Enabled = false;
            //ddlCorpPart3.Enabled = false;
            //ddlWaitPeriod3.Enabled = false;
            //ddlProdType3.Enabled = false;
            //ddlCorpPart4.Enabled = false;
            //ddlWaitPeriod4.Enabled = false;
            //ddlProdType4.Enabled = false;

            //btnAddMoreDet1.Enabled = false;
            //btnAddMoreDet2.Enabled = false;
            //btnAddMoreDet3.Enabled = false;

            //txtClaimCloseReason.Enabled = false;
            //txtClaimCloseReason.ForeColor = System.Drawing.Color.DarkGray;
            //txtClaimCloseReason.Font.Bold = true;

            txtClaimHandlerEmail.Enabled = false;
            txtClaimHandlerEmail.Font.Bold = true;
            txtClaimHandlerEmail.ForeColor = System.Drawing.Color.DarkGray;

            txtTeamOther.Enabled = false;
            txtTeamOther.ForeColor = System.Drawing.Color.DarkGray;
            txtTeamOther.Font.Bold = true;

            txtClaimHandlerTelephone.Enabled = false;
            txtClaimHandlerTelephone.Font.Bold = true;
            txtClaimHandlerTelephone.ForeColor = System.Drawing.Color.DarkGray;

            txtTeamEmail.Enabled = false;
            txtTeamEmail.Font.Bold = true;
            txtTeamEmail.ForeColor = System.Drawing.Color.DarkGray;

            txtTeamTelephone.Enabled = false;
            txtTeamTelephone.Font.Bold = true;
            txtTeamTelephone.ForeColor = System.Drawing.Color.DarkGray;

            txtClientFName.Enabled = false;
            txtClientFName.Font.Bold = true;
            txtClientFName.ForeColor = System.Drawing.Color.DarkGray;


            //ddlSalutation.Enabled = false;
            // ddlSalutation.Attributes.Add("style", "font-weight:bold");

            txtClientMobileTelephone.Enabled = false;
            txtClientMobileTelephone.Font.Bold = true;
            txtClientMobileTelephone.ForeColor = System.Drawing.Color.DarkGray;


            txtClientPostCode.Enabled = false;
            txtClientPostCode.Font.Bold = true;
            txtClientPostCode.ForeColor = System.Drawing.Color.DarkGray;

            txtClientSurname.Enabled = false;
            txtClientSurname.Font.Bold = true;
            txtClientSurname.ForeColor = System.Drawing.Color.DarkGray;


            txtEmployerName.Enabled = false;
            txtEmployerName.Font.Bold = true;
            txtEmployerName.ForeColor = System.Drawing.Color.DarkGray;


            txtEmployerContract.Enabled = false;
            txtEmployerContract.Font.Bold = true;
            txtEmployerContract.ForeColor = System.Drawing.Color.DarkGray;

            txtClientWorkTelephone.Enabled = false;
            txtClientWorkTelephone.Font.Bold = true;
            txtClientWorkTelephone.ForeColor = System.Drawing.Color.DarkGray;

            txtClientDOB.Enabled = false;
            txtClientDOB.Font.Bold = true;
            txtClientDOB.ForeColor = System.Drawing.Color.DarkGray;

          //  txtClientAddress.Enabled = false;
           // txtClientAddress.Font.Bold = true;
           // txtClientAddress.ForeColor = System.Drawing.Color.DarkGray;

            txtAddress1.Enabled=false;
            txtAddress1.Font.Bold=true;
            txtAddress1.ForeColor=System.Drawing.Color.DarkGray;


            txtAddress2.Enabled = false;
            txtAddress2.Font.Bold = true;
            txtAddress2.ForeColor = System.Drawing.Color.DarkGray;


            txtCity.Enabled=false;
            txtCity.Font.Bold=true;
            txtCity.ForeColor=System.Drawing.Color.DarkGray;

            txtservicerequiredother.Enabled = false;
            txtservicerequiredother.ForeColor = System.Drawing.Color.DarkGray;
            txtservicerequiredother.Font.Bold = true;

            txtCounty.Enabled=false;
            txtCounty.Font.Bold=true;
            txtCounty.ForeColor=System.Drawing.Color.DarkGray;



            txtOccupation.Enabled = false;
            txtOccupation.Font.Bold = true;
            txtOccupation.ForeColor = System.Drawing.Color.DarkGray;

            txtPolicyNumber.Enabled = false;
            txtPolicyNumber.Font.Bold = true;
            txtPolicyNumber.ForeColor = System.Drawing.Color.DarkGray;

            txtDeferredOther.Enabled = false;
            txtDeferredOther.ForeColor = System.Drawing.Color.DarkGray;
            txtDeferredOther.Font.Bold = true;

            txtMonthlyBenefit.Enabled = false;
            txtMonthlyBenefit.Font.Bold = true;
            txtMonthlyBenefit.ForeColor = System.Drawing.Color.DarkGray;

            txtEndBenefit.Enabled = false;
            txtEndBenefit.Font.Bold = true;
            txtEndBenefit.ForeColor = System.Drawing.Color.DarkGray;

            txtIncapacityDay.Enabled = false;
            txtIncapacityDay.Font.Bold = true;
            txtIncapacityDay.ForeColor = System.Drawing.Color.DarkGray;

            txtclaimreasonother.Enabled = false;
            txtclaimreasonother.Font.Bold = true;
            txtclaimreasonother.ForeColor = System.Drawing.Color.DarkGray;

            txtillnessinjuryother.Enabled = false;
            txtillnessinjuryother.ForeColor = System.Drawing.Color.DarkGray;
            txtillnessinjuryother.Font.Bold = true;

            txtDisabilityType.Enabled = false;
            txtDisabilityType.Font.Bold = true;
            txtDisabilityType.ForeColor = System.Drawing.Color.DarkGray;

            txtICD9Code.Enabled = false;
            txtICD9Code.Font.Bold = true;
            txtICD9Code.ForeColor = System.Drawing.Color.DarkGray;

            txtClaimCloseDate.Enabled = false;
            txtClaimCloseDate.Font.Bold = true;
            txtClaimCloseDate.ForeColor = System.Drawing.Color.DarkGray;

            txtClaimDetails.Enabled = false;
            txtClaimDetails.Font.Bold = true;
            txtClaimDetails.ForeColor = System.Drawing.Color.DarkGray;

            txtHCBReceivedDate.Enabled = false;
            txtHCBReceivedDate.Font.Bold = true;
            txtHCBReceivedDate.ForeColor = System.Drawing.Color.DarkGray;

            txtDateSentToNurse.Enabled = false;
            txtDateSentToNurse.Font.Bold = true;
            txtDateSentToNurse.ForeColor = System.Drawing.Color.DarkGray;

            txtCaseClosed.Enabled = false;
            txtCaseClosed.Font.Bold = true;
            txtCaseClosed.ForeColor = System.Drawing.Color.DarkGray;

            txtFeeCharged.Enabled = false;
            txtFeeCharged.Font.Bold = true;
            txtFeeCharged.ForeColor = System.Drawing.Color.DarkGray;

            txtRehabcost.Enabled = false;
            txtRehabcost.Font.Bold = true;
            txtRehabcost.ForeColor = System.Drawing.Color.DarkGray;

            //txtClaimRef2.Enabled = false;
            //txtClaimRef2.Font.Bold = true;
            //txtClaimRef2.ForeColor = System.Drawing.Color.DarkGray;

            //txtMnthBenefit2.Enabled = false;
            //txtMnthBenefit2.Font.Bold = true;
            //txtMnthBenefit2.ForeColor = System.Drawing.Color.DarkGray;

            //txtEndBenefit2.Enabled = false;
            //txtEndBenefit2.Font.Bold = true;
            //txtEndBenefit2.ForeColor = System.Drawing.Color.DarkGray;

            //txtClaimRef3.Enabled = false;
            //txtClaimRef3.Font.Bold = true;
            //txtClaimRef3.ForeColor = System.Drawing.Color.DarkGray;

            //txtMnthBenefit3.Enabled = false;
            //txtMnthBenefit3.Font.Bold = true;
            //txtMnthBenefit3.ForeColor = System.Drawing.Color.DarkGray;

            //txtEndBenefit3.Enabled = false;
            //txtEndBenefit3.Font.Bold = true;
            //txtEndBenefit3.ForeColor = System.Drawing.Color.DarkGray;

            //txtClaimRef4.Enabled = false;
            //txtClaimRef4.Font.Bold = true;
            //txtClaimRef4.ForeColor = System.Drawing.Color.DarkGray;

            //txtMnthBenefit4.Enabled = false;
            //txtMnthBenefit4.Font.Bold = true;
            //txtMnthBenefit4.ForeColor = System.Drawing.Color.DarkGray;

            //txtEndBenefit4.Enabled = false;
            //txtEndBenefit4.Font.Bold = true;
            //txtEndBenefit4.ForeColor = System.Drawing.Color.DarkGray;

            txtFeeCharge.Enabled = false;
            txtFeeCharge.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeCharge.Font.Bold = true;


            txtFeeChargedTelPop.Enabled = false;
            txtFeeChargedTelPop.ForeColor = System.Drawing.Color.DarkGray;
            txtFeeChargedTelPop.Font.Bold = true;

            txtRehabFeeCharged.Enabled = false;
            txtRehabFeeCharged.ForeColor = System.Drawing.Color.DarkGray;

            txtRehabFeeCharged.Font.Bold = true;

        }

        private bool SendMail(object[] Arr, string MsgBody, string HCBRef, string Subject, bool IsImportantMail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"].ToString());
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SendersEmailAddress"].ToString(),"HCB MetLife");
                if (Arr.Length > 0)
                {
                    foreach (string strMail in Arr)
                    {
                        if (!string.IsNullOrEmpty(strMail))
                        {
                            mail.To.Add(strMail);
                        }

                    }
                }
                if (IsImportantMail)
                {
                    mail.Subject = Subject + " - IMPORTANT";
                    mail.Priority = MailPriority.High;
                }
                else
                {
                    mail.Subject = Subject;
                }
              
                mail.Body = MsgBody;
                mail.IsBodyHtml = true;
                System.Net.NetworkCredential objAuthentication = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUserName"].ToString(), ConfigurationManager.AppSettings["SmtpPassword"].ToString());

                smtp.UseDefaultCredentials = false;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"].ToString());
                smtp.EnableSsl = true;
                smtp.Credentials = objAuthentication;
                mail.ReplyTo = new MailAddress(ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
                smtp.Send(mail);

                //MailMessage oMsg = new MailMessage();
                //SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SmtpHost"]);
                //String portNo = System.Configuration.ConfigurationManager.AppSettings["SmtpPort"];

                //oMsg.From = (new MailAddress(ConfigurationManager.AppSettings["SmtpUserName"].ToString()));
                //oMsg.To.Add(new MailAddress("jjaywanti@gmail.com"));
                //if (IsImportantMail)
                //{
                  
                //    //string ImportantImagePath=Server.MapPath("~/Images/ImportantMail.png");
                //   // oMsg.Subject =string.replace("(Important)", "<span style='color: rgb(255,0,0);'>(Important)</span>")+ Subject;
                //    oMsg.Subject = "<span style='color: red;'>(Important)</span>" + Subject;
                //    oMsg.Priority = MailPriority.High;
                //}
                //else
                //{
                //    oMsg.Subject = Subject;
                //}
               
                //oMsg.Body = MsgBody;               
                //oMsg.IsBodyHtml = true;
               
                //System.Net.NetworkCredential basicAuthentication = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SmtpUserName"], System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"]);
                //smtp.UseDefaultCredentials = true;
                //smtp.Port = Int16.Parse(ConfigurationManager.AppSettings["SmtpPort"].ToString());
                //smtp.EnableSsl = true;
                //smtp.Credentials = basicAuthentication;
                //smtp.Send(oMsg);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool CreateMail(int RefID, string noteText, bool displaySuccessAlert)
        {
            bool hasConnectionBeenOpened = false;
            string RefNo = "";
            Notes note = new Notes();
            bool MailSent = false;
            string CaseMngrEmail = "";
            string CaseAdminEmail = string.Empty;
            string AdminEmail = "";
            string TeamEmail = ""; //Added on 20062012
            string AdminQuery = "";
            string query = "";
            string MailSubject = "";
            string mailBody = "";
            object[] ArrMailAdd = null;
            string IncoEmail = "";
            ArrayList MailList = new ArrayList();

            try
            {
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                    hasConnectionBeenOpened = true;
                }

                if (RefID != 0)
                {
                    if (RefID.ToString().Length >= 5)
                    {
                        RefNo = "MTL" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 4)
                    {
                        RefNo = "MTL0" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 3)
                    {
                        RefNo = "MTL00" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 2)
                    {
                        RefNo = "MTL000" + RefID.ToString();
                    }
                    else if (RefID.ToString().Length == 1)
                    {
                        RefNo = "MTL0000" + RefID.ToString();
                    }

                    //AdminQuery = "select AdminId from ClientRecord where HCBReference = " + RefID;
                    //OleDbCommand dbcmd = new OleDbCommand(AdminQuery, conn);
                    //string AdminId = dbcmd.ExecuteScalar().ToString();
                    //if (AdminId.Trim() != "" && AdminId != "0")
                    //{
                    //    AdminQuery = "select EmailAddress from Users where UserID = " + AdminId;
                    //}
                    //else
                    //{
                    //    AdminQuery = "select EmailAddress from Users where UserType = 'Admin'";
                    //}
                    //OleDbDataAdapter da = new OleDbDataAdapter(AdminQuery, conn);
                    //DataSet dsEmalil = new DataSet();
                    //da.Fill(dsEmalil);

                    //// if (dsEmalil != null && dsEmalil.Tables.Count > 0 && dsEmalil.Tables[0].Rows.Count > 0)
                    ////   {
                    //string[] mail = (from DataRow dr in dsEmalil.Tables[0].Rows
                    //                 select (string)dr["EmailAddress"]).ToArray();

                    AdminEmail = ConfigurationManager.AppSettings["AdminEmailAddress"].ToString();
                    TeamEmail = txtTeamEmail.Text.Trim(); //Added on 20062012                   
                    ClientInfo objClientInfo = new ClientInfo();
                   
                    if (ddlCaseMngr.SelectedIndex > 0)
                    {

                        DataSet dsquery = new DataSet();
                        dsquery = objClientInfo.GetUsersEmailAddress(Convert.ToInt32(ddlCaseMngr.SelectedValue));
                        if (dsquery != null && dsquery.Tables.Count > 0)
                        {
                            if (dsquery.Tables[0].Rows.Count > 0)
                            {
                                CaseMngrEmail = dsquery.Tables[0].Rows[0]["EmailAddress"].ToString();
                            }
                        }
                        
                        //query = "select EmailAddress from Users where UserID = " + ddlCaseMngr.SelectedValue;
                        //OleDbCommand cmdCaseMngr = new OleDbCommand(query, conn);
                        //cmdCaseMngr.CommandText = query;
                        //cmdCaseMngr.CommandType = CommandType.Text;
                        //CaseMngrEmail = cmdCaseMngr.ExecuteScalar().ToString();
                        //query = "";
                    }
                    DataSet dsCasequery = new DataSet();
                    dsCasequery = objClientInfo.GetAdminEmailAddress(RefID);
                    if (dsCasequery != null && dsCasequery.Tables.Count > 0)
                    {
                        CaseAdminEmail = dsCasequery.Tables[0].Rows[0]["EmailAddress"].ToString();
                    }
                    if (ViewState["ClientIncoID"] != null && ViewState["ClientIncoID"].ToString() != "")
                    {
                        DataSet dsquerys = new DataSet();
                        dsquerys = objClientInfo.GetUsersEmailAddress(Convert.ToInt32(ViewState["ClientIncoID"].ToString()));
                        if (dsquerys != null && dsquerys.Tables.Count > 0)
                        {
                            if (dsquerys.Tables[0].Rows.Count > 0)
                            {
                                IncoEmail = dsquerys.Tables[0].Rows[0]["EmailAddress"].ToString();
                            }
                        }
                        //query = "select EmailAddress from Users where UserID = " + ViewState["ClientIncoID"].ToString();
                        //OleDbCommand cmdIncoMail = new OleDbCommand(query, conn);
                        //cmdIncoMail.CommandText = query;
                        //cmdIncoMail.CommandType = CommandType.Text;
                        //IncoEmail = cmdIncoMail.ExecuteScalar().ToString();
                    }

                    note.HCBReference = RefID;
                    note.NoteText = noteText;
                    note.UserName = Session["LogUserEmailAdd"].ToString();
                    if (chkSendEmail.Checked)
                    {
                        bool IsImportantMail = false;
                        if (chkImportantMail.Checked != null)
                        {
                            IsImportantMail = chkImportantMail.Checked;
                        }
                        if (Session["UserType"] != null)
                        {
                            if (Session["UserType"].ToString() == "Inco")
                            {
                                if (Session["NewHCBRef"] != null)
                                {
                                    if (Session["NewHCBRef"].ToString() != "")
                                    {
                                        MailSubject = "New MetLife Instruction Logged, HCB Reference No. is " + RefNo+" and MetLife Claim Reference is "+txtPolicyNumber.Text.Trim();
                                        mailBody = Session["LoggedInUserName"].ToString() + " has delivered a new instruction to the MTL system, under reference " + RefNo + ". Please acknowledge receipt of this instruction, and commence work as soon as possible.";


                                        if (MailList.Count > 0)
                                        {
                                            MailList.Clear();

                                            if (ArrMailAdd != null)
                                            {
                                                if (ArrMailAdd.Length > 0)
                                                {
                                                    ArrMailAdd = null;
                                                }
                                            }

                                            MailList.Add(AdminEmail);
                                            if (TeamEmail != "") //If block added on 20062012
                                            {
                                                MailList.Add(TeamEmail);
                                            }
                                            
                                            ArrMailAdd = MailList.ToArray();
                                        }
                                        else
                                        {
                                            MailList.Add(AdminEmail);
                                            if (TeamEmail != "") //If block added on 20062012
                                            {
                                                MailList.Add(TeamEmail);
                                            }

                                            ArrMailAdd = MailList.ToArray();
                                        }
                                     
                                        MailSent = SendMail(ArrMailAdd, mailBody, Session["NewHCBRef"].ToString(), MailSubject, IsImportantMail);
                                    }
                                    else if (Session["ClientHcbRef"] != null)
                                    {
                                        if (Session["ClientHcbRef"].ToString() != "")
                                        {
                                            MailSubject ="HCB Reference No. is " + RefNo + " and MetLife Claim Reference is " + txtPolicyNumber.Text.Trim(); 

                                            mailBody = Session["LoggedInUserName"].ToString() + " has added a new note dated " + DateTime.Now + ". Please log on for further details.";
                                            if (displaySuccessAlert == false)
                                            {
                                                //chkSendEmail.Checked = false;
                                                mailBody = "Service Required has been changed by " + Session["LoggedInUserName"].ToString() + " dated " + DateTime.Now + ". Please log on for further details.";
                                            }
                                            if (MailList.Count > 0)
                                            {
                                                MailList.Clear();

                                                if (ArrMailAdd != null)
                                                {
                                                    if (ArrMailAdd.Length > 0)
                                                    {
                                                        ArrMailAdd = null;
                                                    }
                                                }

                                                MailList.Add(AdminEmail);
                                                MailList.Add(CaseMngrEmail);
                                                if (CaseAdminEmail != "") //If block added on 13052014
                                                {
                                                    MailList.Add(CaseAdminEmail);
                                                }
                                                if (TeamEmail != "") //If block added on 20062012
                                                {
                                                    MailList.Add(TeamEmail);
                                                }

                                                ArrMailAdd = MailList.ToArray();
                                            }
                                            else
                                            {
                                                MailList.Add(AdminEmail);
                                                MailList.Add(CaseMngrEmail);
                                                if (CaseAdminEmail != "") //If block added on 13052014
                                                {
                                                    MailList.Add(CaseAdminEmail);
                                                }
                                                if (TeamEmail != "") //If block added on 20062012
                                                {
                                                    MailList.Add(TeamEmail);
                                                }
                                                ArrMailAdd = MailList.ToArray();
                                            }
                                        }

                                        MailSent = SendMail(ArrMailAdd, mailBody, Session["ClientHcbRef"].ToString(), MailSubject,IsImportantMail);
                                    }
                                }
                                else if (Session["ClientHcbRef"] != null)
                                {
                                    if (Session["ClientHcbRef"].ToString() != "")
                                    {
                                        MailSubject = "HCB Reference No. is " + RefNo + " and MetLife Claim Reference is " + txtPolicyNumber.Text.Trim(); 
                                        mailBody = Session["LoggedInUserName"].ToString() + " has added a new note dated " + DateTime.Now + ". Please log on for further details.";
                                        if (displaySuccessAlert == false)
                                        {
                                           // chkSendEmail.Checked = false;
                                            mailBody = "Service Required has been changed by " + Session["LoggedInUserName"].ToString() + " dated " + DateTime.Now + ". Please log on for further details.";
                                        }
                                        if (MailList.Count > 0)
                                        {
                                            MailList.Clear();

                                            if (ArrMailAdd != null)
                                            {
                                                if (ArrMailAdd.Length > 0)
                                                {
                                                    ArrMailAdd = null;
                                                }
                                            }

                                            MailList.Add(AdminEmail);
                                            MailList.Add(CaseMngrEmail);
                                            if (CaseAdminEmail != "") //If block added on 13052014
                                            {
                                                MailList.Add(CaseAdminEmail);
                                            }
                                            if (TeamEmail != "") //If block added on 20062012
                                            {
                                                MailList.Add(TeamEmail);
                                            }
                                            ArrMailAdd = MailList.ToArray();
                                        }
                                        else
                                        {
                                            MailList.Add(AdminEmail);
                                            MailList.Add(CaseMngrEmail);
                                            if (TeamEmail != "") //If block added on 20062012
                                            {
                                                MailList.Add(TeamEmail);
                                            }
                                            ArrMailAdd = MailList.ToArray();
                                        }

                                        MailSent = SendMail(ArrMailAdd, mailBody, Session["ClientHcbRef"].ToString(), MailSubject, IsImportantMail);
                                    }
                                }
                            }
                            else if (Session["UserType"].ToString() == "Admin")
                            {
                                MailSubject = "HCB Reference No. is " + RefNo + " and MetLife Claim Reference is " + txtPolicyNumber.Text.Trim(); 

                                try
                                {
                                    mailBody = Session["LoggedInUserName"].ToString() + " has added a new note dated " + DateTime.Now + ". Please log on for further details.";
                                }
                                catch (Exception ex)
                                {
                                    mailBody = "";
                                }

                                if (MailList.Count > 0)
                                {
                                    MailList.Clear();

                                    if (ArrMailAdd != null)
                                    {
                                        if (ArrMailAdd.Length > 0)
                                        {
                                            ArrMailAdd = null;
                                        }
                                    }

                                    if (CaseMngrEmail != "")
                                    {
                                        MailList.Add(CaseMngrEmail);
                                    }

                                    if (IncoEmail != "")
                                    {
                                        MailList.Add(IncoEmail);
                                    }
                                    if (TeamEmail != "") //If block added on 20062012
                                    {
                                        MailList.Add(TeamEmail);
                                    }
                                    if (AdminEmail != "")
                                    {
                                        MailList.Add(AdminEmail);
                                    }

                                    ArrMailAdd = MailList.ToArray();
                                }
                                else
                                {

                                    if (CaseMngrEmail != "")
                                    {
                                        MailList.Add(CaseMngrEmail);
                                    }

                                    if (IncoEmail != "")
                                    {
                                        MailList.Add(IncoEmail);
                                    }
                                    if (TeamEmail != "") //If block added on 20062012
                                    {
                                        MailList.Add(TeamEmail);
                                    }
                                    if (AdminEmail != "")
                                    {
                                        MailList.Add(AdminEmail);
                                    }

                                    ArrMailAdd = MailList.ToArray();
                                }

                                MailSent = SendMail(ArrMailAdd, mailBody, Session["ClientHcbRef"].ToString(), MailSubject, IsImportantMail);
                            }
                            else if (Session["UserType"].ToString() == "Nurse")
                            {
                                MailSubject = "HCB Reference No. is " + RefNo + " and MetLife Claim Reference is " + txtPolicyNumber.Text.Trim(); 

                                try
                                {
                                    if (Session["LoggedInUserName"] != null)
                                    {
                                        if (Session["LoggedInUserName"].ToString() != "")
                                        {

                                            mailBody = Session["LoggedInUserName"].ToString() + " has added a note dated " + DateTime.Now + ". Please log on for further details.";

                                        }
                                        else
                                        {

                                            // mailBody = "HCB’s Nurse, had added a note within the DCP under reference " + RefNo + ". Please login and visit the relative case file for full information.";

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                }

                                if (MailList.Count > 0)
                                {
                                    MailList.Clear();

                                    if (ArrMailAdd != null)
                                    {
                                        if (ArrMailAdd.Length > 0)
                                        {
                                            ArrMailAdd = null;
                                        }
                                    }

                                    if (AdminEmail != "")
                                    {
                                        MailList.Add(AdminEmail);
                                    }

                                    if (IncoEmail != "")
                                    {
                                        MailList.Add(IncoEmail);
                                    }
                                    if (TeamEmail != "") //If block added on 20062012
                                    {
                                        MailList.Add(TeamEmail);
                                    }
                                    ArrMailAdd = MailList.ToArray();
                                }
                                else
                                {
                                    if (AdminEmail != "")
                                    {
                                        MailList.Add(AdminEmail);
                                    }

                                    if (IncoEmail != "")
                                    {
                                        MailList.Add(IncoEmail);
                                    }
                                    if (TeamEmail != "") //If block added on 20062012
                                    {
                                        MailList.Add(TeamEmail);
                                    }
                                    ArrMailAdd = MailList.ToArray();
                                }

                                //next 4 lines nees to be removed when moved to production. Used for testing purpose only
                                //MailList.Clear();
                                //MailList.Add("sajal.gupta@rhealtech.com");
                                //MailList.Add("juned.ahmed@rhealtech.com");
                                //ArrMailAdd = MailList.ToArray();

                                MailSent = SendMail(ArrMailAdd, mailBody, Session["ClientHcbRef"].ToString(), MailSubject, IsImportantMail);
                            }
                        }
                    }

                    if (MailSent)
                    {
                        note.EmailResult = "OK(1)";
                    }
                    else
                    {
                        note.EmailResult = "NOT SENT(0)";
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Mail", "alert('No Reference number is received')", true);
                    return false;
                }
            }
            catch (Exception eX)
            {
                conn.Close();
                return false;
                //Response.Redirect("~/Login.aspx");
            }



            if (note.Save())
            {
                lblNotes.InnerHtml = "";
                FillNotesLoad(Convert.ToInt32(RefID.ToString()));
                txtAddNote.Text = "";
                //Session["NewHCBRef"] = null;
                if (hasConnectionBeenOpened)
                {
                    //close the connection only if it was opened within this UDF. Else let the calling function handle the connection.
                    conn.Close();
                }
                if (MailSent && displaySuccessAlert)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Note", "alert('Note are Saved and mailed');", true);
                }
                if (MailSent && displaySuccessAlert==false)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Note", "alert('Service Required are Saved and mailed');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Note", "alert('Note Not Saved');");
            }

            return true;
        }

        //private bool SendMail(string EmailAdd1, string EmailAdd2, string EmailAdd3, string EmailAdd4, string EmailAdd5, string MsgBody, string HCBRef)
        //{
        //    try
        //    {
        //        MailMessage mail = new MailMessage();
        //        mail.BodyEncoding = Encoding.Default;

        //        mail.Subject = "Claim details for HCBReference " + HCBRef;
        //        mail.Body = MsgBody;

        //        if (EmailAdd1 != "")
        //        {
        //            if (EmailAdd2 != "")
        //            {
        //                if (EmailAdd3 != "")
        //                {
        //                    if (EmailAdd4 != "")
        //                    {
        //                        if (EmailAdd5 != "")
        //                        {
        //                            mail.To.Add(EmailAdd1 + "," + EmailAdd2 + "," + EmailAdd3 + "," + EmailAdd4 + "," + EmailAdd5);
        //                        }
        //                        else
        //                        {
        //                            mail.To.Add(EmailAdd1 + "," + EmailAdd2 + "," + EmailAdd3 + "," + EmailAdd4);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        mail.To.Add(EmailAdd1 + "," + EmailAdd2 + "," + EmailAdd3);
        //                    }
        //                }
        //                else
        //                {
        //                    mail.To.Add(EmailAdd1 + "," + EmailAdd2);
        //                }
        //            }
        //            else
        //            {
        //                mail.To.Add(EmailAdd1);
        //            }
        //        }

        //        mail.Priority = MailPriority.High;
        //        SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"].ToString());

        //        mail.From = new MailAddress(ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());

        //        smtp.Send(mail);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public void updateAmendment()
        {
            try
            {
                if (Session["Amdid"] != null)
                {
                    int AmdID = (int)Session["Amdid"];
                    //if (Session["ClientHcbRef"] != null)
                    //{
                    //    //Session["ClientHcbRef"] = "DCP00001";
                    //}
                    //else
                    //    Session.Add("ClientHcbRef", "DCP0001");
                    int userid = (int)Session["LoggedInUserId"];
                    string Hcbref = Session["ClientHcbRef"].ToString();
                    MasterListBL MBL = new MasterListBL();
                    MBL.UpdateAmendment(Convert.ToInt32(Hcbref), userid, 1, AmdID);

                    // in case further update possible then remove this condtion
                    Session["Amdid"] = null;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void InsertAmendment(string HCBRef)
        {
            //if (Session["ClientHcbRef"] != null)
            //{
            //    //Session["ClientHcbRef"] = "DCP00001";
            //}
            //else
            //    Session.Add("ClientHcbRef", "DCP0001");

            int userid = (int)Session["LoggedInUserId"];
            //string Hcbref = Session["ClientHcbRef"].ToString();
            MasterListBL MBL = new MasterListBL();
            int AmdId = MBL.InsertAmndment(Convert.ToInt32(HCBRef), userid, 0);
            if (AmdId != 0)
            {
                if (Session["Amdid"] != null)
                {
                    Session["AmdId"] = AmdId;
                }
                else Session.Add("AmdId", AmdId);
            }
        }

        private void GetFailcall(int RefID)
        {
            HCBFailedCall objFailCall = new HCBFailedCall();
            DataSet dsFailCall = new DataSet();
            DataTable dtFailCall = new DataTable();

            try
            {
                if (RefID.ToString() != "")
                {
                    objFailCall.HCBReference = RefID;


                    dsFailCall = objFailCall.GetFailedCallDet();

                    if (dsFailCall != null)
                    {
                        dtFailCall = dsFailCall.Tables["FailedCall"];

                        if (dtFailCall.Rows.Count > 0)
                        {
                            for (int r = 0; r < dtFailCall.Rows.Count; r++)
                            {
                                if (dtFailCall.Rows[r]["Call"].ToString() == "Call1")
                                {
                                    DataRow[] result1 = dtFailCall.Select("Call = 'Call1'");

                                    foreach (DataRow row in result1)
                                    {
                                        lstFailedCalls.DataSource = new DataView(dtFailCall, "Call = 'Call1'", "HCBFailTime,FailedCallID", DataViewRowState.CurrentRows);
                                        lstFailedCalls.DataTextField = "HCBFailTime";
                                        lstFailedCalls.DataValueField = "FailedCallID";
                                        lstFailedCalls.DataBind();
                                    }
                                }
                                //else if (dtFailCall.Rows[r]["Call"].ToString() == "Call2")
                                //{
                                //    DataRow[] result2 = dtFailCall.Select("Call = 'Call2'");
                                //    //lstFailCall2.DataSource = resu;

                                //    foreach (DataRow row in result2)
                                //    {
                                //        lstFailCall2.DataSource = new DataView(dtFailCall, "Call = 'Call2'", "HCBFailTime,FailedCallID", DataViewRowState.CurrentRows);
                                //        lstFailCall2.DataTextField = "HCBFailTime";
                                //        lstFailCall2.DataValueField = "FailedCallID";
                                //        lstFailCall2.DataBind();
                                //    }
                                //}
                                //else if (dtFailCall.Rows[r]["Call"].ToString() == "Call3")
                                //{
                                //    DataRow[] result3 = dtFailCall.Select("Call = 'Call3'");

                                //    foreach (DataRow row in result3)
                                //    {
                                //        lstFailCall3.DataSource = new DataView(dtFailCall, "Call = 'Call3'", "HCBFailTime,FailedCallID", DataViewRowState.CurrentRows);
                                //        lstFailCall3.DataTextField = "HCBFailTime";
                                //        lstFailCall3.DataValueField = "FailedCallID";
                                //        lstFailCall3.DataBind();
                                //    }
                                //}
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dsFailCall = null;
            }
        }

        private bool CheckExistingClient()
        {
            ClientInfo info = new ClientInfo();
            int HcbRef = 0;

            try
            {
                if (txtClientSurname.Text != "")
                {
                    if (txtClientDOB.Text != "")
                    {
                        HcbRef = info.ClientExists(txtClientSurname.Text, txtClientDOB.Text);

                        if (HcbRef > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Client Exists", "alert('Surname and DOB check! – This claimant may already exist on the database, please check before you create a new record');", true);
                            txtClientDOB.Focus();
                            return false;
                        }
                        else if (HcbRef == -1)
                        {
                            lblMsg.Text = AntiXss.HtmlEncode("Some Error Occured");
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                lblMsg.Text = AntiXss.HtmlEncode("Some Error Occured");
                return false;
            }
        }

        #region Documents

        public void GetDocuments()
        {
            ddlDocuments.Items.Clear();
            if (Session["ClientHcbRef"] != null)
            {
                DataSet ds;
                Documents Doc = new Documents();
                Doc.RefNo = Convert.ToInt32((string)Session["ClientHcbRef"]);
                ds = Doc.GetDocument();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ddlDocuments.DataSource = ds.Tables[0];
                    ddlDocuments.DataTextField = "DocumentName";
                    ddlDocuments.DataValueField = "DocumentID";
                    ddlDocuments.DataBind();
                    lblDocs.Text = AntiXss.HtmlEncode(ds.Tables[0].Rows.Count.ToString()) + " documents online";
                    lblDocs.Visible = true;
                }
            }
            else
            {
                int CountFile = 0;
                if (ViewState["filelist"] != null)
                {
                    List<TempFileList> tplist = new List<TempFileList>();
                    tplist = (List<TempFileList>)ViewState["filelist"];
                    
                    foreach (TempFileList file in tplist)
                    {
                        ddlDocuments.Items.Add(new ListItem(file.filename, file.filename));
                        CountFile += 1;
                    }
                    lblDocs.Text = AntiXss.HtmlEncode(CountFile.ToString()) + " documents online";
                    lblDocs.Visible = true;
                }
            }
            ddlDocuments.Items.Insert(0, new ListItem(" --- select ---", "0"));
           

        }

        private bool FindComputer(TempFileList tp)
        {
            if (tp.filename == ddlDocuments.SelectedItem.Text)
            {
                return true;
            }
            {
                return false;
            }
        }

        public void InsertDocDetails(string refno, string DocName)
        {
            Documents doc = new Documents();
            doc.RefNo = Convert.ToInt32(refno.Trim());
            doc.DocName = DocName;
            doc.InsertDocument();

            lblDocs.Text = "";
        }

        #endregion

        //[WebMethod(EnableSession = true)]
        //public static int InsertFailCall(string FailCall, int id)
        //{
        //    #region HCbFailCall

        //    string[] FailArr;
        //    string[] FailCallDateTime;
        //    string FailDate = "";
        //    string FailTime = "";
        //    int FailCallID = 0;

        //    HCBFailedCall failcall = new HCBFailedCall();

        //    try
        //    {
        //        if (HttpContext.Current.Session["ClientHcbRef"] != null)
        //        {
        //            //string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
        //            failcall.HCBReference = Convert.ToInt32(HttpContext.Current.Session["ClientHcbRef"].ToString());
        //            //updateAmendment();
        //        }

        //        if (id == 1)
        //        {
        //            if (FailCall != "")
        //            {
        //                try
        //                {
        //                    FailArr = FailCall.Split(',');

        //                    if (FailArr.Length > 0)
        //                    {
        //                        foreach (string strFail in FailArr)
        //                        {
        //                            FailCallDateTime = strFail.Split(' ');
        //                            //failcall = strFail;

        //                            if (FailCallDateTime.Length > 0)
        //                            {
        //                                //for(int i = 0; i <= FailCallDateTime.Length; i++)
        //                                //{
        //                                failcall.FailCallDate = FailCallDateTime[0].ToString();
        //                                failcall.FailCallTime = FailCallDateTime[1].ToString();
        //                                failcall.Call = "Call1";
        //                                FailCallID = failcall.Save();
        //                                //}
        //                            }

        //                        }

        //                    }
        //                    else
        //                    {
        //                        //failcall.FailedCallDuration = "";
        //                        failcall.Call = "";
        //                    }

        //                    FailArr = null;
        //                    FailCallDateTime = null;
        //                }
        //                catch (Exception ex)
        //                {
        //                    failcall.FailCallDate = "";
        //                    failcall.FailCallDate = "";
        //                    failcall.Call = "Call1";
        //                    //failcall.Save();
        //                }
        //            }
        //            else
        //            {
        //                failcall.FailCallDate = "";
        //                failcall.FailCallDate = "";
        //                failcall.Call = "";
        //            }
        //        }
        //        else if (id == 2)
        //        {
        //            if (FailCall != "")
        //            {
        //                try
        //                {
        //                    FailArr = FailCall.Split(',');

        //                    if (FailArr.Length > 0)
        //                    {
        //                        foreach (string strFail in FailArr)
        //                        {
        //                            FailCallDateTime = strFail.Split(' ');
        //                            //failcall = strFail;

        //                            if (FailCallDateTime.Length > 0)
        //                            {
        //                                //for(int i = 0; i <= FailCallDateTime.Length; i++)
        //                                //{
        //                                failcall.FailCallDate = FailCallDateTime[0].ToString();
        //                                failcall.FailCallTime = FailCallDateTime[1].ToString();
        //                                failcall.Call = "Call2";
        //                                FailCallID = failcall.Save();
        //                                //}
        //                            }

        //                        }

        //                    }
        //                    else
        //                    {
        //                        failcall.FailCallDate = "";
        //                        failcall.FailCallTime = "";
        //                        failcall.Call = "Call2";
        //                    }

        //                    FailArr = null;
        //                    FailCallDateTime = null;
        //                }
        //                catch (Exception ex)
        //                {
        //                    failcall.FailCallDate = "";
        //                    failcall.FailCallTime = "";
        //                    failcall.Call = "Call2";
        //                }
        //            }
        //            else
        //            {
        //                failcall.FailCallDate = "";
        //                failcall.FailCallTime = "";
        //                failcall.Call = "Call2";
        //            }
        //        }
        //        else if (id == 3)
        //        {
        //            if (FailCall != "")
        //            {
        //                try
        //                {
        //                    FailArr = FailCall.Split(',');

        //                    if (FailArr.Length > 0)
        //                    {
        //                        foreach (string strFail in FailArr)
        //                        {
        //                            FailCallDateTime = strFail.Split(' ');
        //                            //failcall = strFail;

        //                            if (FailCallDateTime.Length > 0)
        //                            {
        //                                //for(int i = 0; i <= FailCallDateTime.Length; i++)
        //                                //{
        //                                failcall.FailCallDate = FailCallDateTime[0].ToString();
        //                                failcall.FailCallTime = FailCallDateTime[1].ToString();
        //                                failcall.Call = "Call3";
        //                                FailCallID = failcall.Save();
        //                                //}
        //                            }

        //                        }

        //                    }
        //                    else
        //                    {
        //                        //failcall.FailedCallDuration = "";
        //                        failcall.Call = "";
        //                    }

        //                    FailArr = null;
        //                    FailCallDateTime = null;
        //                }
        //                catch (Exception ex)
        //                {
        //                    failcall.FailCallDate = "";
        //                    failcall.FailCallDate = "";
        //                    failcall.Call = "Call3";
        //                    //failcall.Save();
        //                }
        //            }
        //        }
        //        return FailCallID;
        //    }
        //    catch (Exception ex)
        //    {
        //        return 0;
        //    }
        //    #endregion
        //}

        public static int InsertFailCall(DateTime Dt,int TelephoneId)
        {
            #region HCbFailCall

            //string[] FailArr;
            //string[] FailCallDateTime;
            //string FailDate = "";
            //string FailTime = "";
            int FailCallID = 0;

            HCBFailedCall failcall = new HCBFailedCall();


            try
            {
                failcall.HCBTelephoneID = TelephoneId;
                if (HttpContext.Current.Session["ClientHcbRef"] != null)
                {
                    //string Hcbref = Session["ClientHcbRef"].ToString().Substring(3, Session["ClientHcbRef"].ToString().Length - 3);
                    failcall.HCBReference = Convert.ToInt32(HttpContext.Current.Session["ClientHcbRef"].ToString());
                    //updateAmendment();
                }

                failcall.FailCallDate = String.Format("{0:dd-MMM-yyyy}", Dt);
                //failcall.FailCallTime = FailCallDateTime[1].ToString();
                failcall.FailCallTime = String.Format("{0:HH:mm}", Dt);
                //failcall.Call = "Call" + id;
                FailCallID = failcall.Save();

                return FailCallID;
            }
            catch (Exception ex)
            {
                return 0;
            }
            #endregion
        }

        [WebMethod(EnableSession = true)]
        public static bool DeleteFailCall(int FailID)
        {
            HCBFailedCall hcbFail = new HCBFailedCall();
            bool del = false;

            if (FailID != 0)
            {
                hcbFail.FailedCallID = FailID;
                del = hcbFail.Delete();

                if (del)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        [Serializable]
        public class TempFileList
        {
            public string filename;
        }

        protected void lnkFailedCall_Click(object sender, EventArgs e)
        {
           // ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "Close", "telephonepopupopen()", true);
            LinkButton lnk = (LinkButton)sender;
            if (lnk.CommandName.ToLower().Trim() == "add")
            {
                DateTime dtNow = DateTime.Now;

               // int fcALLid = InsertFailCall(lnk.CommandArgument.Trim(), dtNow);
                switch (lnk.CommandArgument.Trim())
                {
                    case "1":
                        lslFailedCallPop.Items.Add(new ListItem(dtNow.ToString("dd/MM/yyyy HH:mm"), failedcallcountPop.ToString()));
                        lnkFailedPop.Enabled = true;
                        break;
                    //case "2":
                    //    //lstFailCall2.Items.Add(new ListItem(dtNow.ToString("dd/MM/yyyy HH:mm"), fcALLid.ToString()));
                    //    //lnkRemoveFailCall2.Enabled = true;

                    //    break;
                    //case "3":
                    //    lstFailCall3.Items.Add(new ListItem(dtNow.ToString("dd/MM/yyyy HH:mm"), fcALLid.ToString()));
                    //    lnkRemoveFailCall3.Enabled = true;
                    //    break;
                }

            }
            else if (lnk.CommandName.ToLower().Trim() == "del")
            {
                string strId = "0";
                switch (lnk.CommandArgument.Trim())
                {
                    case "1":
                        if (lslFailedCallPop.SelectedIndex > -1)
                        {
                            strId = lslFailedCallPop.SelectedItem.Value;
                            //DeleteFailCall(Convert.ToInt32(strId));
                            lslFailedCallPop.Items.Remove(lslFailedCallPop.SelectedItem);
                            if (lslFailedCallPop.Items.Count < 1) lnk.Enabled = false;
                        }
                        break;
                    //case "2":
                    //    if (lstFailCall2.SelectedIndex > -1)
                    //    {
                    //        strId = lstFailCall2.SelectedItem.Value;
                    //        DeleteFailCall(Convert.ToInt32(strId));
                    //        lstFailCall2.Items.Remove(lstFailCall2.SelectedItem);
                    //        if (lstFailCall2.Items.Count < 1) lnk.Enabled = false;
                    //    }
                    //    break;
                    //case "3":
                    //    if (lstFailCall3.SelectedIndex > -1)
                    //    {
                    //        strId = lstFailCall3.SelectedItem.Value;
                    //        DeleteFailCall(Convert.ToInt32(strId));
                    //        lstFailCall3.Items.Remove(lstFailCall3.SelectedItem);
                    //        if (lstFailCall3.Items.Count < 1) lnk.Enabled = false;
                    //    }
                    //    break;
                }
            }
          
        }
        protected void lnkFailedCallPage_Click(object sender, EventArgs e)
        {
            failedcallcount = failedcallcount + 1;
            LinkButton lnk = (LinkButton)sender;
            if (lnk.CommandName.ToLower().Trim() == "add")
            {
                DateTime dtNow = DateTime.Now;

                // int fcALLid = InsertFailCall(lnk.CommandArgument.Trim(), dtNow);
                switch (lnk.CommandArgument.Trim())
                {
                    case "1":
                        lstFailedCalls.Items.Add(new ListItem(dtNow.ToString("dd/MM/yyyy HH:mm"), failedcallcount.ToString()));
                        lnkRemoveFailCall1.Enabled = true;
                        break;
                    //case "2":
                    //    //lstFailCall2.Items.Add(new ListItem(dtNow.ToString("dd/MM/yyyy HH:mm"), fcALLid.ToString()));
                    //    //lnkRemoveFailCall2.Enabled = true;

                    //    break;
                    //case "3":
                    //    lstFailCall3.Items.Add(new ListItem(dtNow.ToString("dd/MM/yyyy HH:mm"), fcALLid.ToString()));
                    //    lnkRemoveFailCall3.Enabled = true;
                    //    break;
                }

            }
            else if (lnk.CommandName.ToLower().Trim() == "del")
            {
                string strId = "0";
                switch (lnk.CommandArgument.Trim())
                {
                    case "1":
                        if (lstFailedCalls.SelectedIndex > -1)
                        {
                            strId = lstFailedCalls.SelectedItem.Value;
                            DeleteFailCall(Convert.ToInt32(strId));
                            lstFailedCalls.Items.Remove(lstFailedCalls.SelectedItem);
                            if (lstFailedCalls.Items.Count < 1) lnk.Enabled = false;
                        }
                        break;
                    //case "2":
                    //    if (lstFailCall2.SelectedIndex > -1)
                    //    {
                    //        strId = lstFailCall2.SelectedItem.Value;
                    //        DeleteFailCall(Convert.ToInt32(strId));
                    //        lstFailCall2.Items.Remove(lstFailCall2.SelectedItem);
                    //        if (lstFailCall2.Items.Count < 1) lnk.Enabled = false;
                    //    }
                    //    break;
                    //case "3":
                    //    if (lstFailCall3.SelectedIndex > -1)
                    //    {
                    //        strId = lstFailCall3.SelectedItem.Value;
                    //        DeleteFailCall(Convert.ToInt32(strId));
                    //        lstFailCall3.Items.Remove(lstFailCall3.SelectedItem);
                    //        if (lstFailCall3.Items.Count < 1) lnk.Enabled = false;
                    //    }
                    //    break;
                }
            }

        }
        private void disableCall1Controls()
        {
            txtCall1Date.Enabled = false;


            txtCall1Time.Enabled = false;


            lstFailedCalls.Enabled = false;
            lnkAddFailedCall.Enabled = false;
            lnkRemoveFailCall1.Enabled = false;
            //cbCall1Aborted.Enabled = false;//SG
            //txtRTWDateOptimum.Enabled = false;
            txtRTWDateOptimum.ReadOnly = true;

            ddlCaseMngtRecom.Enabled = false;
            txtNxtCallSchd.Enabled = false;

        }
        private void disableCall2Controls()
        {
            //txtCall2Date.Enabled = false;
            //txtCall2Time.Enabled = false;
            //lstFailCall2.Enabled = false;
            //lnkFailCall2.Enabled = false;
            //lnkRemoveFailCall2.Enabled = false;
            //cbCall2Aborted.Enabled = false;//SG
            //txtCall2RTWDateOpt.Enabled = false;
            //ddlCaseMngtRecom2.Enabled = false;
            //ddlRTWReal.Enabled = false;
            //txtEnvisagedRTWDate.Enabled = false;
            //txtCall2NxtSchDate.Enabled = false;
        }
        private void disableCall3Controls()
        {
            //txtCall3Date.Enabled = false;
            //txtCall3Time.Enabled = false;
            //lstFailCall3.Enabled = false;
            //lnkFailCall3.Enabled = false;
            //lnkRemoveFailCall3.Enabled = false;
            ////cbCall3Aborted.Enabled = false;//SG
            //txtCall3RTWDateOpt.Enabled = false;
            //ddlFurtherInterReq.Enabled = false;
            //txtRTWDateAgreed.Enabled = false;
            //ddlCaseMngtInvolve3.Enabled = false;
            //ddlRehabServPurchase3.Enabled = false;
        }

        //protected void btnRehabPopupAdd_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ClientRehabilitationCosts crc = new ClientRehabilitationCosts();
        //        crc.HCBReference = getHCBRefID();
        //        crc.RehabDate = new DateTime(Convert.ToInt32(txtRehabPopupDate.Text.Substring(6, 4)), Convert.ToInt32(txtRehabPopupDate.Text.Substring(3, 2)), Convert.ToInt32(txtRehabPopupDate.Text.Substring(0, 2)));
        //        crc.Cost = Convert.ToDouble(txtRehabPopupCost.Text);
        //        crc.save();
        //        FillClientRehabilitationCosts(crc.HCBReference);
        //        clearRehabPopupInputs();
        //        processTotalRehabCost();
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //public void FillClientRehabilitationCosts(int HCBReference)
        //{
        //    ClientRehabilitationCosts crc = new ClientRehabilitationCosts();
        //    crc.HCBReference = HCBReference;
        //    DataSet ds = crc.GetClientRehabilitationCosts();
        //    gvRehabCosts.DataSource = ds.Tables[0];
        //    gvRehabCosts.DataBind();
        //}

        //protected void gvRehabCosts_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Modify")
        //        {
        //            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
        //            txtRehabPopupDate.Text = row.Cells[1].Text; //"RehabDate"
        //            txtRehabPopupCost.Text = row.Cells[2].Text; //"Cost"
        //            hidCRC_ID.Value = e.CommandArgument.ToString();

        //            btnRehabCancel.Visible = true;
        //            btnRehabPopupAdd.Visible = false;
        //            btnRehabSave.Visible = true;
        //        }
        //        else if (e.CommandName == "Remove")
        //        {
        //            ClientRehabilitationCosts crc = new ClientRehabilitationCosts();
        //            crc.CRC_ID = Convert.ToInt32(e.CommandArgument.ToString());
        //            crc.Delete();
        //            FillClientRehabilitationCosts(getHCBRefID());
        //            processTotalRehabCost();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        //protected void btnRehabSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ClientRehabilitationCosts crc = new ClientRehabilitationCosts();
        //        crc.CRC_ID = Convert.ToInt32(hidCRC_ID.Value);
        //        crc.RehabDate = new DateTime(Convert.ToInt32(txtRehabPopupDate.Text.Substring(6, 4)), Convert.ToInt32(txtRehabPopupDate.Text.Substring(3, 2)), Convert.ToInt32(txtRehabPopupDate.Text.Substring(0, 2)));
        //        crc.Cost = Convert.ToDouble(txtRehabPopupCost.Text);
        //        crc.save();
        //        FillClientRehabilitationCosts(getHCBRefID());
        //        clearRehabPopupInputs();
        //        btnRehabCancel.Visible = false;
        //        btnRehabPopupAdd.Visible = true;
        //        btnRehabSave.Visible = false;
        //        hidCRC_ID.Value = "";
        //        processTotalRehabCost();
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void clearRehabPopupInputs()
        //{
        //    txtRehabPopupDate.Text = "";
        //    txtRehabPopupCost.Text = "";
        //}

        //protected void btnRehabCancel_Click(object sender, EventArgs e)
        //{
        //    btnRehabCancel.Visible = false;
        //    btnRehabPopupAdd.Visible = true;
        //    btnRehabSave.Visible = false;
        //    hidCRC_ID.Value = "";
        //}
        private void processTotalRehabCost()
        {
            //txtRehabcost.Text = ""
            double totalRehabCost = 0;
            ClientInfo ci = new ClientInfo();
            ClientRehabilitationCosts crc = new ClientRehabilitationCosts();
            crc.HCBReference = getHCBRefID();
            totalRehabCost = crc.GetClientRehabilitationCostsTotal();
            txtRehabcost.Text = totalRehabCost + "";
            ci.HCBReference = crc.HCBReference;
            ci.RehabCost = totalRehabCost;
            ci.updateClientRehabilationCostsTotal();
            //upTotalRehabCost.Update();
        }

        protected void cbCall1Aborted_CheckedChanged(object sender, EventArgs e)
        {
            //txtCall1Date.Enabled = !cbCall1Aborted.Checked;
            //txtCall1Time.Enabled = !cbCall1Aborted.Checked;
            //lstFailedCalls.Enabled = !cbCall1Aborted.Checked;
            //lnkAddFailedCall.Enabled = !cbCall1Aborted.Checked;
            //lnkRemoveFailCall1.Enabled = !cbCall1Aborted.Checked;
            //txtRTWDateOptimum.Enabled = !cbCall1Aborted.Checked;
            //ddlCaseMngtRecom.Enabled = !cbCall1Aborted.Checked;
            //txtNxtCallSchd.Enabled = !cbCall1Aborted.Checked;
        }



        protected void cbCall2Aborted_CheckedChanged(object sender, EventArgs e)
        {
            //txtCall2Date.Enabled = !cbCall2Aborted.Checked;
            //txtCall2Time.Enabled = !cbCall2Aborted.Checked;
            //lstFailCall2.Enabled = !cbCall2Aborted.Checked;
            //lnkFailCall2.Enabled = !cbCall2Aborted.Checked;
            //lnkRemoveFailCall2.Enabled = !cbCall2Aborted.Checked;
            //txtCall2RTWDateOpt.Enabled = !cbCall2Aborted.Checked;
            //ddlCaseMngtRecom2.Enabled = !cbCall2Aborted.Checked;
            //ddlRTWReal.Enabled = !cbCall2Aborted.Checked;
            //txtEnvisagedRTWDate.Enabled = !cbCall2Aborted.Checked;
            //txtCall2NxtSchDate.Enabled = !cbCall2Aborted.Checked;
        }

        protected void cbCall3Aborted_CheckedChanged(object sender, EventArgs e)
        {
            //txtCall3Date.Enabled = !cbCall3Aborted.Checked;
            //txtCall3Time.Enabled = !cbCall3Aborted.Checked;
            //lstFailCall3.Enabled = !cbCall3Aborted.Checked;
            //lnkFailCall3.Enabled = !cbCall3Aborted.Checked;
            //lnkRemoveFailCall3.Enabled = !cbCall3Aborted.Checked;
            //txtCall3RTWDateOpt.Enabled = !cbCall3Aborted.Checked;
            //ddlFurtherInterReq.Enabled = !cbCall3Aborted.Checked;
            //txtRTWDateAgreed.Enabled = !cbCall3Aborted.Checked;
            //ddlCaseMngtInvolve3.Enabled = !cbCall3Aborted.Checked;
            //ddlRehabServPurchase3.Enabled = !cbCall3Aborted.Checked;
        }

        protected void btnSaveHomeVisit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "")
                    {
                        HCBActivityVisit obj = new HCBActivityVisit();
                        if (hdnVisitId != null)
                        {
                            if (hdnVisitId.Value != "")
                            {
                                obj.HCBVisitId = Convert.ToInt32(hdnVisitId.Value);
                            }
                        }
                        obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        if (txtDateOfAppointmentPop.Text.Trim() != "")
                        {
                            if (txtDateOfAppointmentPop.Text.Trim() != "01/01/1900")
                            {
                                if (txtDateOfAppointmentPop.Text.Trim() != "1/1/1900 12:00:00 AM")
                                {
                                    obj.AppointmentDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateOfAppointmentPop.Text.Trim()));
                                }
                                else
                                {
                                    obj.AppointmentDate = "";
                                }
                            }
                            else
                            {
                                obj.AppointmentDate = "";
                            }
                        }
                        else
                        {
                            obj.AppointmentDate = "";
                        }

                        if (txtDateReportCompPop.Text.Trim() != "")
                        {
                            if (txtDateReportCompPop.Text.Trim() != "01/01/1900")
                            {
                                if (txtDateReportCompPop.Text.Trim() != "1/1/1900 12:00:00 AM")
                                {
                                    obj.ReportCompDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateReportCompPop.Text.Trim()));
                                }
                                else
                                {
                                    obj.ReportCompDate = "";
                                }
                            }
                            else
                            {
                                obj.ReportCompDate = "";
                            }


                        }
                        else
                        {
                            obj.ReportCompDate = "";
                        }
                        try
                        {
                            if (!string.IsNullOrEmpty(txtFeeChargedPop.Text.Trim()))
                            {
                                obj.FeeCharged = Convert.ToDecimal(txtFeeChargedPop.Text.Trim());
                            }
                            if (ddlTypeOfVisitPop.SelectedValue != "")
                            {
                                obj.TYpeOfVisit = Convert.ToInt32(ddlTypeOfVisitPop.SelectedValue);
                            }

                            obj.Save();
                            hdnVisitId.Value = "";
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            BindHomeVisit();

                        }



                        ScriptManager.RegisterStartupScript(upHomeVisits, this.GetType(), "Close", "closepopup()", true);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void lnk_CommandRehab(object sender, CommandEventArgs e)
        {
            try
            {

                Rehabilitation obj = new Rehabilitation();

                string[] Val = e.CommandArgument.ToString().Split(',');
                if (e.CommandName.Trim().ToLower() == "edt" )
                {

                    //if (hdnVisitId != null)
                    //{

                    //    hdnVisitId.Value  = Val[0];
                    //}
                    hdnRehabilitationId.Value = Val[0];
                    obj.RehabilitationId = Convert.ToInt32(Val[0]);
                    if (!string.IsNullOrEmpty(Val[1]))
                    {
                        txtRehabFeeCharged.Text = String.Format("{0:n2}", Convert.ToDecimal(Val[1]));
                    }
                    ddlRehabType.SelectedValue = Val[2];
                    ddlRehabProvider.SelectedValue = Val[3];
                    txtDateInstructed.Text = Val[4];
                    txtDateReported.Text = Val[5];

                    ScriptManager.RegisterStartupScript(upd, this.GetType(), "edit", "loadPopup('#divRehabilitation')", true);

                }
                else if (e.CommandName.Trim().ToLower() == "del")
                {
                    obj.RehabilitationId = Convert.ToInt32(e.CommandArgument.ToString());
                    obj.DeleteHCBRehabilitation();
                    //BindCaseManagementCost();
                    //DeleteList(Convert.ToInt32(e.CommandArgument.ToString()));
                    //ScriptManager.RegisterStartupScript(gvReasonList,this.GetType,"Del",
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                BindRehabilitation();
            }

        }
        protected void lnk_CommandCaseMgnt(object sender, CommandEventArgs e)
        {
            try
            {

                CaseManagement obj = new CaseManagement();

                string[] Val = e.CommandArgument.ToString().Split(',');
                if (e.CommandName.Trim().ToLower() == "edt" )
                {

                    //if (hdnVisitId != null)
                    //{

                    //    hdnVisitId.Value  = Val[0];
                    //}
                    hdbCaseManagementCostId.Value = Val[0];
                    obj.CaseMgmtCostId = Convert.ToInt32(Val[0]);                  
                    txtCaseMgntDate.Text = Val[1];
                    txtNoofHour.Text = Val[2];
                    if (!string.IsNullOrEmpty(Val[3]))
                    {
                        txtRate.Text = String.Format("{0:n2}", Convert.ToDecimal(Val[3]));
                    }
                    if (!string.IsNullOrEmpty(Val[4]))
                    {
                        txtCaseMgntCost.Text = String.Format("{0:n2}", Convert.ToDecimal(Val[4]));
                    }
                    //txtCaseMgntCost.Text = Val[4];
                    //Page.ClientScript.RegisterClientScriptBlock(lnkeEdit1.GetType(), "OnClick", "<script>window.scroll(0,0); </script>");
                    ScriptManager.RegisterStartupScript(upd, this.GetType(), "edit", "loadPopup('#divCaseManagementCost')", true);

                }
                else if (e.CommandName.Trim().ToLower() == "del")
                {
                    obj.CaseMgmtCostId = Convert.ToInt32(e.CommandArgument.ToString());
                    obj.DeleteHCBCaseManagementCost();
                    //BindCaseManagementCost();
                    //DeleteList(Convert.ToInt32(e.CommandArgument.ToString()));
                    //ScriptManager.RegisterStartupScript(gvReasonList,this.GetType,"Del",
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                BindCaseManagementCost();
            }

        }
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            try
            {
                LinkButton lnkeEdit1 = (LinkButton)grdHomeVisit.FindControl("lnkeEdit1");
                HCBActivityVisit obj = new HCBActivityVisit();

                string[] Val = e.CommandArgument.ToString().Split(',');
                if (e.CommandName.Trim().ToLower() == "edt" || e.CommandName.Trim().ToLower() == "visit")
                {

                    //if (hdnVisitId != null)
                    //{

                    //    hdnVisitId.Value  = Val[0];
                    //}
                    hdnVisitId.Value = Val[0];
                    obj.HCBVisitId = Convert.ToInt32(Val[0]);


                    hdntype.Value = "edit";
                    txtDateOfAppointmentPop.Text = Val[1];
                    txtDateReportCompPop.Text = Val[2];
                    if (!string.IsNullOrEmpty(Val[4]))
                    {
                        ddlTypeOfVisitPop.SelectedValue = Val[4];
                    }
                    else
                    {
                        ddlTypeOfVisitPop.SelectedValue = "0";
                    }
                    if (!string.IsNullOrEmpty(Val[3]))
                    {
                        txtFeeChargedPop.Text = String.Format("{0:n2}", Convert.ToDecimal(Val[3]));
                    }
                    //Page.ClientScript.RegisterClientScriptBlock(lnkeEdit1.GetType(), "OnClick", "<script>window.scroll(0,0); </script>");
                    ScriptManager.RegisterStartupScript(upd, this.GetType(), "edit", "loadPopup('#divHomeVisit')", true);

                }
                else if (e.CommandName.Trim().ToLower() == "del")
                {
                    obj.HCBVisitId = Convert.ToInt32(e.CommandArgument.ToString());
                    obj.DeleteHCBVisitActivity();
                    BindHomeVisit();
                    //DeleteList(Convert.ToInt32(e.CommandArgument.ToString()));
                    //ScriptManager.RegisterStartupScript(gvReasonList,this.GetType,"Del",
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                BindHomeVisit();
            }

        }

        protected void btnSaveVisit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "")
                    {
                        HCBActivityVisit obj = new HCBActivityVisit();
                        if (hdnVisitId != null)
                        {
                            if (hdnVisitId.Value != "")
                            {
                                obj.HCBVisitId = Convert.ToInt32(hdnVisitId.Value);
                            }
                        }
                        obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        if (txtAppointmentDate.Text.Trim() != "")
                        {
                            if (txtAppointmentDate.Text.Trim() != "01/01/1900")
                            {
                                if (txtAppointmentDate.Text.Trim() != "1/1/1900 12:00:00 AM")
                                {
                                    obj.AppointmentDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtAppointmentDate.Text.Trim()));
                                }
                                else
                                {
                                    obj.AppointmentDate = "";
                                }
                            }
                            else
                            {
                                obj.AppointmentDate = "";
                            }
                        }
                        else
                        {
                            obj.AppointmentDate = "";
                        }

                        if (txtDateReportComp.Text.Trim() != "")
                        {
                            if (txtDateReportComp.Text.Trim() != "01/01/1900")
                            {
                                if (txtDateReportComp.Text.Trim() != "1/1/1900 12:00:00 AM")
                                {
                                    obj.ReportCompDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateReportComp.Text.Trim()));
                                }
                                else
                                {
                                    obj.ReportCompDate = "";
                                }
                            }
                            else
                            {
                                obj.ReportCompDate = "";
                            }


                        }
                        else
                        {
                            obj.ReportCompDate = "";
                        }
                        try
                        {
                            if (!string.IsNullOrEmpty(txtFeeChargeHomeVisit.Text.Trim()))
                            {
                                obj.FeeCharged = Convert.ToDecimal(txtFeeChargeHomeVisit.Text.Trim());
                            }
                            if (ddlTypeOfVisit.SelectedValue != "")
                            {
                                obj.TYpeOfVisit = Convert.ToInt32(ddlTypeOfVisit.SelectedValue);
                            }

                            obj.Save();
                            hdnVisitId.Value = "";
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            BindHomeVisit();

                        }
                        ScriptManager.RegisterStartupScript(upHomeVisits, this.GetType(), "Close", "closepopup()", true);


                        //ScriptManager.RegisterStartupScript(grdHomeVisit, this.GetType(), "edit", "closepopup()", true);
                    }
                }
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "SetHeightDiv", "SetHeightDiv()", true);
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void grdHomeVisit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            HCBActivityVisit obj = new HCBActivityVisit();
            obj.HCBVisitId = Convert.ToInt32(e.CommandArgument.ToString());
            obj.DeleteHCBVisitActivity();
            BindHomeVisit();
        }

        protected void btnTelephoneActivitySave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "")
                    {
                        TelephoneActivity obj = new TelephoneActivity();
                        if (hdnTelephoneActivityId != null)
                        {
                            if (hdnTelephoneActivityId.Value != "")
                            {
                                obj.HCBTelephoneID = Convert.ToInt32(hdnTelephoneActivityId.Value);
                            }
                        }
                        obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        if (txtCallDatePop.Text != "")
                        {
                            if (txtCallDatePop.Text != "01/01/1900")
                            {
                                if (txtCallDatePop.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCallDatePop.Text));
                                }
                                else
                                {
                                    obj.CallDate = "";
                                }
                            }
                            else
                            {
                                obj.CallDate = "";
                            }

                        }
                        else
                        {
                            obj.CallDate = "";
                        }
                        if (txtCallTimePop.Text != "")
                        {
                            if (txtCallTimePop.Text == "__:__")
                            {
                                obj.CallTime = "";
                            }
                            else
                            {
                                obj.CallTime = txtCallTimePop.Text;
                            }
                        }
                        if (ddlTypeofCallPop.SelectedIndex > 0)
                        {
                            obj.TypeOfCall = Convert.ToInt16(ddlTypeofCallPop.SelectedValue);
                        }
                        if (txtFeeChargedTelPop.Text != "")
                        {
                            obj.FeeCharged = Convert.ToDecimal(txtFeeChargedTelPop.Text.Trim());
                        }
                        if (txtRTWDateOptimumPop.Text != "")
                        {
                            if (txtRTWDateOptimumPop.Text != "01/01/1900")
                            {
                                if (txtRTWDateOptimumPop.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallExpectRTWDateOptimum = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtRTWDateOptimumPop.Text));
                                }
                                else
                                {
                                    obj.CallExpectRTWDateOptimum = "";
                                }
                            }
                            else
                            {
                                obj.CallExpectRTWDateOptimum = "";
                            }
                        }
                        else
                        {
                            obj.CallExpectRTWDateOptimum = "";
                        }
                        if (txtRTWDateMaxPop.Text != "")
                        {
                            if (txtRTWDateMaxPop.Text != "01/01/1900")
                            {
                                if (txtRTWDateMaxPop.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallExpectRTWDateMaximum = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtRTWDateMaxPop.Text));
                                }
                                else
                                {
                                    obj.CallExpectRTWDateMaximum = "";
                                }
                            }
                            else
                            {
                                obj.CallExpectRTWDateMaximum = "";
                            }
                        }
                        else
                        {
                            obj.CallExpectRTWDateMaximum = "";
                        }
                        if (ddlCaseMgmtPop.SelectedIndex > 0)
                        {
                            obj.CallCaseMngtRecom = ddlCaseMgmtPop.SelectedItem.Text;
                        }
                        if (txtNxtCallSchdPop.Text != "")
                        {
                            if (txtNxtCallSchdPop.Text != "01/01/1900")
                            {
                                if (txtNxtCallSchdPop.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallNxtSchedule = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtNxtCallSchdPop.Text));
                                }
                                else
                                {
                                    obj.CallNxtSchedule = "";
                                }
                            }
                            else
                            {
                                obj.CallNxtSchedule = "";
                            }
                        }
                        else
                        {
                            obj.CallNxtSchedule = "";
                        }

                        obj.HCBTelephoneID = obj.Save();
                        HCBFailedCall objfailed = new HCBFailedCall();
                        objfailed.HCBTelephoneID = obj.HCBTelephoneID;
                        objfailed.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        objfailed.Delete();
                        if (hdnFailedCallList.Value != "")
                        {
                            string[] strhdnFailedCallList = hdnFailedCallList.Value.Split(',');
                            for (int i = 0; i < strhdnFailedCallList.Length-1; i++)
                            {
                                DateTime dt = Convert.ToDateTime(strhdnFailedCallList[i]);
                                int fcALLid = InsertFailCall(dt, objfailed.HCBTelephoneID);
                            }
                        }
                        //foreach (ListItem listItem in lslFailedCallPop.Items)
                        //{
                        //    DateTime dt = Convert.ToDateTime(listItem.Text);
                        //    int fcALLid = InsertFailCall(dt, objfailed.HCBTelephoneID);                         

                        //}
                        hdnTelephoneActivityId.Value = "";
                        hdnFailedCallList.Value = "";
                       
                        
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                BindTelephoneActivity();
                ClearTelephoneActivityPop();
                
            }
            ScriptManager.RegisterStartupScript(updcall1, this.GetType(), "Close", "closepopup()", true);
        }
        protected void lnk_CommandGrid(object sender, CommandEventArgs e)
        {
            try
            {
                ClearTelephoneActivityPop();
                TelephoneActivity obj = new TelephoneActivity();
                obj.HCBTelephoneID = Convert.ToInt32(e.CommandArgument.ToString());
                hdnTelephoneActivityId.Value = e.CommandArgument.ToString();
                if (e.CommandName.Trim().ToLower() == "edt")
                {

                        DataSet dsTelephone = new DataSet();
                        DataTable dt=new DataTable();
                        dsTelephone = obj.GetDetailTelephoneActivity();
                        if (dsTelephone != null && dsTelephone.Tables[0].Rows.Count > 0)
                        {
                            dt=dsTelephone.Tables[0];
                            try
                            {
                                if (dt.Rows[0]["CallDate"].ToString() != "")
                                {
                                    if (dt.Rows[0]["CallDate"].ToString() == "01/01/1900")
                                    {
                                        txtCallDatePop.Text = "";
                                    }
                                    else if (dt.Rows[0]["CallDate"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtCallDatePop.Text = "";
                                    }
                                    else
                                    {
                                        txtCallDatePop.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["CallDate"].ToString());
                                    }
                                }
                                else
                                {
                                    txtCallDatePop.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtCallDatePop.Text = "";
                            }
                            try
                            {
                                if (dt.Rows[0]["CallTime"].ToString() != "")
                                {
                                    DateTime Call1Time = Convert.ToDateTime(dt.Rows[0]["CallTime"].ToString());
                                    txtCallTimePop.Text = Call1Time.Hour.ToString() + ":" + Call1Time.Minute.ToString();
                                }
                                else
                                {
                                    txtCallTimePop.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtCallTimePop.Text = "";
                            }

                            try
                            {
                                if (dt.Rows[0]["CallExpectRTWDateOptimum"].ToString() != "")
                                {
                                    if (dt.Rows[0]["CallExpectRTWDateOptimum"].ToString() == "01/01/1900")
                                    {
                                        txtRTWDateOptimumPop.Text = "";
                                    }
                                    else if (dt.Rows[0]["CallExpectRTWDateOptimum"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtRTWDateOptimumPop.Text = "";
                                    }
                                    else
                                    {
                                        txtRTWDateOptimumPop.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["CallExpectRTWDateOptimum"]);
                                    }
                                }
                                else
                                {
                                    txtRTWDateOptimumPop.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtRTWDateOptimumPop.Text = "";
                            }
                            try
                            {
                                if (dt.Rows[0]["CallExpectRTWDateMaximum"].ToString() != "")
                                {
                                    if (dt.Rows[0]["CallExpectRTWDateMaximum"].ToString() == "01/01/1900")
                                    {
                                        txtRTWDateMaxPop.Text = "";
                                    }
                                    else if (dt.Rows[0]["CallExpectRTWDateMaximum"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtRTWDateMaxPop.Text = "";
                                    }
                                    else
                                    {
                                        txtRTWDateMaxPop.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["CallExpectRTWDateMaximum"]);
                                    }
                                }
                                else
                                {
                                    txtRTWDateMaxPop.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtRTWDateMaxPop.Text = "";
                            }
                            try
                            {
                                if (dt.Rows[0]["CallCaseMngtRecom"].ToString() != "")
                                {
                                    ddlCaseMgmtPop.SelectedValue = dt.Rows[0]["CallCaseMngtRecom"].ToString();
                                }
                                else
                                {
                                    ddlCaseMgmtPop.SelectedIndex = 0;
                                }
                            }
                            catch (Exception ex)
                            {
                                ddlCaseMgmtPop.SelectedIndex = 0;
                            }

                            try
                            {
                                if (dt.Rows[0]["CallNxtSchedule"].ToString() != "")
                                {
                                    if (dt.Rows[0]["CallNxtSchedule"].ToString() == "01/01/1900")
                                    {
                                        txtNxtCallSchdPop.Text = "";
                                    }
                                    else if (dt.Rows[0]["CallNxtSchedule"].ToString() == "1/1/1900 12:00:00 AM")
                                    {
                                        txtNxtCallSchdPop.Text = "";
                                    }
                                    else
                                    {
                                        txtNxtCallSchdPop.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["CallNxtSchedule"]);
                                    }
                                }
                                else
                                {
                                    txtNxtCallSchdPop.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                txtNxtCallSchdPop.Text = "";
                            }
                            if (!string.IsNullOrEmpty(dt.Rows[0]["FeeCharged"].ToString()))
                            {
                                txtFeeChargedTelPop.Text = String.Format("{0:n2}", Convert.ToDecimal(dt.Rows[0]["FeeCharged"].ToString()));
                            }
                            if (dt.Rows[0]["TypeofCall"].ToString() != "")
                            {
                                ddlTypeofCallPop.SelectedValue = dt.Rows[0]["TypeofCall"].ToString();
                            }
                            HCBFailedCall objfailed = new HCBFailedCall();
                            objfailed.HCBTelephoneID = obj.HCBTelephoneID;
                            DataSet dsfailed = new DataSet();
                            dsfailed = objfailed.GetFailedCallDet();
                            if (dsfailed != null)
                            {
                                if (dsfailed.Tables[0].Rows.Count > 0)
                                {
                                    DataTable dtfailed = dsfailed.Tables[0];

                                    
                                    lslFailedCallPop.DataSource = dtfailed;
                                    lslFailedCallPop.DataTextField = "HCBFailTime";
                                    lslFailedCallPop.DataValueField = "rownumber";
                                    lslFailedCallPop.DataBind();

                                }
                                else
                                {
                                    lslFailedCallPop.DataSource = null;                                   
                                    lslFailedCallPop.DataBind();
                                }
                            }
                            //txtCallDatePop.Text = dt.Rows[0]["CallDate"].ToString();
                            //txtCallTimePop.Text = dt.Rows[0]["CallTime"].ToString();
                            //ddlTypeofCallPop.SelectedValue = dt.Rows[0]["TypeofCall"].ToString();
                            //txtFeeChargedTelPop.Text = dt.Rows[0]["FeeCharged"].ToString();
                            //txtRTWDateOptimumPop.Text = dt.Rows[0]["CallExpectRTWDateOptimum"].ToString();
                            //txtRTWDateMaxPop.Text = dt.Rows[0]["CallExpectRTWDateOptimum"].ToString();
                            //ddlCaseMgmtPop.SelectedValue = dt.Rows[0]["CallCaseMngtRecom"].ToString();
                            //txtNxtCallSchdPop.Text = dt.Rows[0]["CallNxtSchedule"].ToString();
                        }
                    
                    //hdnVisitId.Value = Val[0];
                   


                    //hdntype.Value = "edit";
                    //txtDateOfAppointmentPop.Text = Val[1];
                    //txtDateReportCompPop.Text = Val[2];
                    //ddlTypeOfVisitPop.SelectedValue = Val[4];
                    //txtFeeChargedPop.Text = Val[3];

                        ScriptManager.RegisterStartupScript(upd, this.GetType(), "edit", "loadPopup('#divTelephone')", true);

                }
                else if (e.CommandName.Trim().ToLower() == "del")
                {
                    obj.HCBTelephoneID = Convert.ToInt32(e.CommandArgument.ToString());
                    obj.DeleteTelephoneAcitivity();
                    BindTelephoneActivity();
                    //DeleteList(Convert.ToInt32(e.CommandArgument.ToString()));
                    //ScriptManager.RegisterStartupScript(gvReasonList,this.GetType,"Del",
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                BindTelephoneActivity();
            }

        }

        protected void btnSaveTelephone_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ClientHcbRef"] != null)
                {
                    if (Session["ClientHcbRef"].ToString() != "")
                    {
                        TelephoneActivity obj = new TelephoneActivity();
                        //if (hdnTelephoneActivityId != null)
                        //{
                        //    if (hdnTelephoneActivityId.Value != "")
                        //    {
                        //        obj.HCBTelephoneID = Convert.ToInt32(hdnTelephoneActivityId.Value);
                        //    }
                        //}
                        obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        if (txtCall1Date.Text != "")
                        {
                            if (txtCall1Date.Text != "01/01/1900")
                            {
                                if (txtCall1Date.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCall1Date.Text));
                                }
                                else
                                {
                                    obj.CallDate = "";
                                }
                            }
                            else
                            {
                                obj.CallDate = "";
                            }

                        }
                        else
                        {
                            obj.CallDate = "";
                        }
                        if (txtCall1Time.Text != "")
                        {
                            if (txtCall1Time.Text == "__:__")
                            {
                                obj.CallTime = "";
                            }
                            else
                            {
                                obj.CallTime = txtCall1Time.Text;
                            }
                        }
                        if (ddlTypeofCall.SelectedIndex > 0)
                        {
                            obj.TypeOfCall = Convert.ToInt16(ddlTypeofCall.SelectedValue);
                        }
                        if (txtFeeCharge.Text != "")
                        {
                            obj.FeeCharged = Convert.ToDecimal(txtFeeCharge.Text.Trim());
                        }
                        if (txtRTWDateOptimum.Text != "")
                        {
                            if (txtRTWDateOptimum.Text != "01/01/1900")
                            {
                                if (txtRTWDateOptimum.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallExpectRTWDateOptimum = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtRTWDateOptimum.Text));
                                }
                                else
                                {
                                    obj.CallExpectRTWDateOptimum = "";
                                }
                            }
                            else
                            {
                                obj.CallExpectRTWDateOptimum = "";
                            }
                        }
                        else
                        {
                            obj.CallExpectRTWDateOptimum = "";
                        }
                        if (txtRTWDateMax.Text != "")
                        {
                            if (txtRTWDateMax.Text != "01/01/1900")
                            {
                                if (txtRTWDateMax.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallExpectRTWDateMaximum = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtRTWDateMax.Text));
                                }
                                else
                                {
                                    obj.CallExpectRTWDateMaximum = "";
                                }
                            }
                            else
                            {
                                obj.CallExpectRTWDateMaximum = "";
                            }
                        }
                        else
                        {
                            obj.CallExpectRTWDateMaximum = "";
                        }
                        if (ddlCaseMngtRecom.SelectedIndex > 0)
                        {
                            obj.CallCaseMngtRecom = ddlCaseMngtRecom.SelectedItem.Text;
                        }
                        if (txtNxtCallSchd.Text != "")
                        {
                            if (txtNxtCallSchd.Text != "01/01/1900")
                            {
                                if (txtNxtCallSchd.Text != "1/1/1900 12:00:00 AM")
                                {
                                    obj.CallNxtSchedule = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtNxtCallSchd.Text));
                                }
                                else
                                {
                                    obj.CallNxtSchedule = "";
                                }
                            }
                            else
                            {
                                obj.CallNxtSchedule = "";
                            }
                        }
                        else
                        {
                            obj.CallNxtSchedule = "";
                        }

                        obj.HCBTelephoneID = obj.Save();
                        HCBFailedCall objfailed = new HCBFailedCall();
                        objfailed.HCBTelephoneID = obj.HCBTelephoneID;
                        objfailed.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                        objfailed.Delete();
                        foreach (ListItem listItem in lstFailedCalls.Items)
                        {
                            DateTime dt = Convert.ToDateTime(listItem.Text);
                            int fcALLid = InsertFailCall(dt, objfailed.HCBTelephoneID);
                            //objfailed.ClientServiceReq += listItem.Value + ",";

                        }
                        hdnTelephoneActivityId.Value = "";
                        //BindTelephoneActivity();
                        //ScriptManager.RegisterStartupScript(upHomeVisits, this.GetType(), "Close", "closepopup()", true);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                BindTelephoneActivity();
                ClearTelephoneActivityPop();
               
            }
            ScriptManager.RegisterStartupScript(updcall1, this.GetType(), "Close", "closepopup()", true);
        }
         private void clearTelephoneActivity()
        {
            txtCall1Date.Text = string.Empty;
            txtCall1Time.Text = string.Empty;
            ddlTypeofCall.SelectedIndex = 0;
            lstFailedCalls.Items.Clear();
            txtFeeCharge.Text = string.Empty;
            txtRTWDateOptimum.Text = string.Empty;
            txtRTWDateMax.Text = string.Empty;
            ddlCaseMngtRecom.SelectedValue = "";
            txtNxtCallSchd.Text=string.Empty;


        }
         private void ClearTelephoneActivityPop()
         {
             txtNxtCallSchdPop.Text = string.Empty;
             txtRTWDateMaxPop.Text = string.Empty;
             txtRTWDateOptimumPop.Text = string.Empty;
             txtFeeChargedTelPop.Text = string.Empty;
             txtCallTimePop.Text = string.Empty;
             txtCallDatePop.Text = string.Empty;
             ddlTypeofCallPop.SelectedIndex = 0;
             ddlCaseMgmtPop.SelectedValue = "";
             lslFailedCallPop.Items.Clear();
         }

         protected void btnTelephoneCancel_Click(object sender, EventArgs e)
         {
             ClearTelephoneActivityPop();
         }

         protected void lnkAdd_Click(object sender, EventArgs e)
         {
             try
             {
                 if (Session["ClientHcbRef"] != null)
                 {
                     if (Session["ClientHcbRef"].ToString() != "")
                     {
                         txtDateOfAppointmentPop.Text = string.Empty;
                         txtDateReportCompPop.Text = string.Empty;
                         txtFeeChargedPop.Text = string.Empty;
                         hdnVisitId.Value = "";
                         HCBActivityVisit obj = new HCBActivityVisit();
                         if (hdnVisitId != null)
                         {
                             if (hdnVisitId.Value != "")
                             {
                                 obj.HCBVisitId = Convert.ToInt32(hdnVisitId.Value);
                             }
                         }
                         obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                         if (txtDateOfAppointmentPop.Text.Trim() != "")
                         {
                             if (txtDateOfAppointmentPop.Text.Trim() != "01/01/1900")
                             {
                                 if (txtDateOfAppointmentPop.Text.Trim() != "1/1/1900 12:00:00 AM")
                                 {
                                     obj.AppointmentDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateOfAppointmentPop.Text.Trim()));
                                 }
                                 else
                                 {
                                     obj.AppointmentDate = "";
                                 }
                             }
                             else
                             {
                                 obj.AppointmentDate = "";
                             }
                         }
                         else
                         {
                             obj.AppointmentDate = "";
                         }

                         if (txtDateReportCompPop.Text.Trim() != "")
                         {
                             if (txtDateReportCompPop.Text.Trim() != "01/01/1900")
                             {
                                 if (txtDateReportCompPop.Text.Trim() != "1/1/1900 12:00:00 AM")
                                 {
                                     obj.ReportCompDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateReportCompPop.Text.Trim()));
                                 }
                                 else
                                 {
                                     obj.ReportCompDate = "";
                                 }
                             }
                             else
                             {
                                 obj.ReportCompDate = "";
                             }


                         }
                         else
                         {
                             obj.ReportCompDate = "";
                         }
                         try
                         {
                             if (!string.IsNullOrEmpty(txtFeeChargedPop.Text.Trim()))
                             {
                                 obj.FeeCharged = Convert.ToDecimal(txtFeeChargedPop.Text.Trim());
                             }
                             //if (ddlTypeOfVisitPop.SelectedValue != "")
                             //{
                             //    obj.TYpeOfVisit = 0;
                             //}

                             obj.Save();
                             hdnVisitId.Value = "";
                         }
                         catch (Exception)
                         {

                         }
                         finally
                         {
                             BindHomeVisit();

                         }
                     }
                 }
             }
             catch (Exception)
             {

                 throw;
             }
         }

         decimal HCBVisitFee = 0;
         protected void grdHomeVisit_RowDataBound(object sender, GridViewRowEventArgs e)
         {
             try
             {
            if (e.Row.RowType == DataControlRowType.DataRow)
             {
                 LinkButton lnk = (LinkButton)e.Row.FindControl("lnkTypeofvisit");
                 if (lnk != null)
                 {
                     if (string.IsNullOrEmpty(lnk.Text))
                     {
                         lnk.Text = "New Visit";
                         e.Row.BackColor = System.Drawing.Color.LightBlue;
                     }
                 }
                 if (DataBinder.Eval(e.Row.DataItem, "FeeCharged") != DBNull.Value)
                     HCBVisitFee += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "FeeCharged"));


             }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                hdnhcbvisitfee.Value = String.Format("{0:n2}", HCBVisitFee);
                decimal currentcase = 0;
                if (!string.IsNullOrEmpty(txtCurrentCaseTotal.Text))
                { currentcase = Convert.ToDecimal(txtCurrentCaseTotal.Text); }
                else
                { currentcase = 0; }
                totalFeeCharged(Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbvisitfee.Value)) ? hdnhcbvisitfee.Value : "0"), Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbtelephonevisitfee.Value)) ? hdnhcbtelephonevisitfee.Value : "0"), currentcase);
                txttotalcasecost.Text = (Convert.ToDecimal(!String.IsNullOrEmpty(txtRehabcost.Text) ? txtRehabcost.Text : "0")  + Convert.ToDecimal((!string.IsNullOrEmpty(txtFeeCharged.Text)) ? txtFeeCharged.Text : "0")).ToString();
                //txtFeeCharged.Text = (Convert.ToDecimal(txtCurrentCaseTotal.Text) + Convert.ToDecimal(hdnhcbtelephonevisitfee.Value) + Convert.ToDecimal(hdnhcbvisitfee.Value)).ToString();
            }
             }
             catch (Exception ex)
             {
                 
                 throw;
             }
         }
          Decimal HCBTelephoneVisitFee=0;
         protected void grdTelephoneIntervention_OnRowDataBound(object sender,GridViewRowEventArgs e)
         {
            try
            {
            if (e.Row.RowType == DataControlRowType.DataRow)
             {
                 if (DataBinder.Eval(e.Row.DataItem, "FeeCharged") != DBNull.Value)
                     HCBTelephoneVisitFee += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "FeeCharged"));
             }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                hdnhcbtelephonevisitfee.Value = String.Format("{0:n2}", HCBTelephoneVisitFee);
                decimal currentcase = 0;
                if (!string.IsNullOrEmpty(txtCurrentCaseTotal.Text))
                { currentcase = Convert.ToDecimal(txtCurrentCaseTotal.Text); }
                else
                { currentcase = 0; }
                totalFeeCharged(Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbvisitfee.Value))?hdnhcbvisitfee.Value:"0"), Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbtelephonevisitfee.Value))?hdnhcbtelephonevisitfee.Value:"0"), currentcase);
                txttotalcasecost.Text = (Convert.ToDecimal(!String.IsNullOrEmpty(txtRehabcost.Text) ? txtRehabcost.Text : "0")  + Convert.ToDecimal((!string.IsNullOrEmpty(txtFeeCharged.Text)) ? txtFeeCharged.Text : "0")).ToString();
               
            }
            }
            catch(Exception ex)
            {
                throw;
            }
             

         }
         protected void btnCaseMgntCostSave_Click(object sender, EventArgs e)
         {
             try
             {
                 if (Session["ClientHcbRef"] != null)
                 {
                     if (Session["ClientHcbRef"].ToString() != "")
                     {
                         CaseManagement obj = new CaseManagement();
                         if (hdbCaseManagementCostId != null)
                         {
                             if (hdbCaseManagementCostId.Value != "")
                             {
                                 obj.CaseMgmtCostId = Convert.ToInt32(hdbCaseManagementCostId.Value);
                             }
                         }
                         obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                         if (txtCaseMgntDate.Text.Trim() != "")
                         {
                             if (txtCaseMgntDate.Text.Trim() != "01/01/1900")
                             {
                                 if (txtCaseMgntDate.Text.Trim() != "1/1/1900 12:00:00 AM")
                                 {
                                     obj.CaseDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtCaseMgntDate.Text.Trim()));
                                 }
                                 else
                                 {
                                     obj.CaseDate = "";
                                 }
                             }
                             else
                             {
                                 obj.CaseDate = "";
                             }
                         }
                         else
                         {
                             obj.CaseDate = "";
                         }

                         
                         try
                         {
                             if (!string.IsNullOrEmpty(txtNoofHour.Text.Trim()))
                             {
                                 obj.CaseHours = Convert.ToDecimal(txtNoofHour.Text.Trim());
                             }
                             if (!string.IsNullOrEmpty(txtRate.Text.Trim()))
                             {
                                 obj.CaseRate = Convert.ToDecimal(txtRate.Text.Trim());
                             }
                             if (!string.IsNullOrEmpty(txtCaseMgntCost.Text.Trim()))
                             {
                                 obj.CaseCost = Convert.ToDecimal(txtCaseMgntCost.Text.Trim());
                             }
                             obj.Save();
                             hdbCaseManagementCostId.Value = "";
                         }
                         catch (Exception)
                         {

                         }
                         finally
                         {
                             BindCaseManagementCost();

                         }



                         ScriptManager.RegisterStartupScript(upCaseMgnt, this.GetType(), "Close", "closepopup()", true);
                     }
                 }
             }
             catch (Exception)
             {

                 throw;
             }
         }

         protected void ddlCaseMgntapproved_SelectedIndexChanged(object sender, EventArgs e)
         {

             btnCaseMgntSave.Enabled = true;
             if (ddlCaseMgntapproved.SelectedValue == "0")
             {
                 txtCaseMgntFundingLimit.Enabled = false;
                 txtCurrentCaseTotal.Enabled = false;
                 lnkCaseMgntCost.Enabled = false;
                 grdCaseMgntCost.Enabled = false;
             }
             else
             {
                 txtCaseMgntFundingLimit.Enabled = true;
                 txtCurrentCaseTotal.Enabled = false;
                 lnkCaseMgntCost.Enabled = true;
                 grdCaseMgntCost.Enabled = true;
             }
             if (lnkCaseMgntCost.Enabled)
             {
                 string strCaseMgntCost = "#divCaseManagementCost";
                 lnkCaseMgntCost.OnClientClick = "return PopupAddNew('" + strCaseMgntCost + "')";
             }
             else
             {
                 lnkCaseMgntCost.OnClientClick = "SaveHomeVisit()";
             }
         }

         protected void txtRate_TextChanged(object sender, EventArgs e)
         {
             CalculateTotalCost();
         }

         protected void txtNoofHour_TextChanged(object sender, EventArgs e)
         {
             CalculateTotalCost();
         }
         private void CalculateTotalCost()
         {
             if (!string.IsNullOrEmpty(txtRate.Text.Trim()) && !string.IsNullOrEmpty(txtNoofHour.Text.Trim()))
             {
                 decimal TotalCost = 0;
                 TotalCost = Convert.ToDecimal(txtRate.Text.Trim()) * Convert.ToDecimal(txtNoofHour.Text.Trim());
                 txtCaseMgntCost.Text = String.Format("{0:n2}", TotalCost); 
             }
         }
         decimal TotalManagementCost = 0;
         protected void grdCaseMgntCost_RowDataBound(object sender, GridViewRowEventArgs e)
         {
             if (e.Row.RowType == DataControlRowType.DataRow)
             {
                 if (DataBinder.Eval(e.Row.DataItem, "CaseCost") != DBNull.Value)
                     TotalManagementCost += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CaseCost"));
             }
             if (e.Row.RowType == DataControlRowType.Footer)
             {
                 txtCurrentCaseTotal.Text = String.Format("{0:n2}", TotalManagementCost); 
                 decimal currentcase = 0;
                 if (!string.IsNullOrEmpty(txtCurrentCaseTotal.Text))
                 { currentcase = Convert.ToDecimal(txtCurrentCaseTotal.Text); }
                 else
                 { currentcase = 0; }
                 totalFeeCharged(Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbvisitfee.Value)) ? hdnhcbvisitfee.Value : "0"), Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbtelephonevisitfee.Value)) ? hdnhcbtelephonevisitfee.Value : "0"), currentcase);
                 txttotalcasecost.Text = (Convert.ToDecimal(!String.IsNullOrEmpty(txtRehabcost.Text) ? txtRehabcost.Text : "0") + Convert.ToDecimal((!string.IsNullOrEmpty(txtFeeCharged.Text)) ? txtFeeCharged.Text : "0")).ToString();
             }
             
         }

         protected void btnCaseMgntSave_Click(object sender, EventArgs e)
         {
             CaseManagement obj = new CaseManagement();
             if (ddlCaseMgntapproved.SelectedValue == "1")
             {
                 obj.ApprovedBy = true;
             }
             else
             {
                obj.ApprovedBy =false;
             }
             obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
             if (!string.IsNullOrEmpty(txtCaseMgntFundingLimit.Text.Trim()))
             {
                 obj.FundingLimit = Convert.ToDecimal(txtCaseMgntFundingLimit.Text.Trim());
             }
             obj.InsertHCBCaseManagement();
             ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Closse", "DisplayMsgCaseMgnt()", true);
         }
         [WebMethod]
         public static ServiceRequiredDropDown[] BindDatatoDropdownService()
         {
             HCBBLL.MasterListBL mstrServReq = new MasterListBL();
             DataSet ds = mstrServReq.GetMasterList(1);
        
             DataTable dt = new DataTable();
             dt = ds.Tables[0];
             List<ServiceRequiredDropDown> details = new List<ServiceRequiredDropDown>();

            
                     foreach (DataRow dtrow in dt.Rows)
                     {
                         ServiceRequiredDropDown service = new ServiceRequiredDropDown();
                         service.ServiceId = Convert.ToInt32(dtrow["ID"].ToString());
                         service.ServiceName = dtrow["Name"].ToString();
                         details.Add(service);
                     }
                
             return details.ToArray();
         }
         [System.Web.Services.WebMethod]
         public static ArrayList BindDatatoDropdownService1()
         {
             ArrayList List = new ArrayList();
             HCBBLL.MasterListBL mstrServReq = new MasterListBL();
             DataSet ds = mstrServReq.GetMasterList(1);

             DataTable dt = new DataTable();
             dt = ds.Tables[0];
             for(int i=0;i<dt.Rows.Count;i++)
             {
                 List.Add(new ListItem(dt.Rows[i]["Name"].ToString(),dt.Rows[i]["ID"].ToString()));
               
             }
             return List;
         }

         protected void btnRehabilitationSave_Click(object sender, EventArgs e)
         {
             try
             {
                 if (Session["ClientHcbRef"] != null)
                 {
                     if (Session["ClientHcbRef"].ToString() != "")
                     {
                         Rehabilitation obj = new Rehabilitation();
                         if (hdnRehabilitationId != null)
                         {
                             if (hdnRehabilitationId.Value != "")
                             {
                                 obj.RehabilitationId = Convert.ToInt32(hdnRehabilitationId.Value);
                             }
                         }
                         obj.HCBReference = Convert.ToInt32(Session["ClientHcbRef"].ToString());
                         if (txtDateInstructed.Text.Trim() != "")
                         {
                             if (txtDateInstructed.Text.Trim() != "01/01/1900")
                             {
                                 if (txtDateInstructed.Text.Trim() != "1/1/1900 12:00:00 AM")
                                 {
                                     obj.InstructedDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateInstructed.Text.Trim()));
                                 }
                                 else
                                 {
                                     obj.InstructedDate = "";
                                 }
                             }
                             else
                             {
                                 obj.InstructedDate = "";
                             }
                         }
                         else
                         {
                             obj.InstructedDate = "";
                         }
                         if (txtDateReported.Text.Trim() != "")
                         {
                             if (txtDateReported.Text.Trim() != "01/01/1900")
                             {
                                 if (txtDateReported.Text.Trim() != "1/1/1900 12:00:00 AM")
                                 {
                                     obj.ReportedDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtDateReported.Text.Trim()));
                                 }
                                 else
                                 {
                                     obj.ReportedDate = "";
                                 }
                             }
                             else
                             {
                                 obj.ReportedDate = "";
                             }
                         }
                         else
                         {
                             obj.ReportedDate = "";
                         }

                         if (ddlRehabType.SelectedValue != "")
                         {
                             obj.RehabilitationTypeId =Convert.ToInt32(ddlRehabType.SelectedValue);
                         }
                         if (ddlRehabProvider.SelectedValue != "")
                         {
                             obj.RehabilitationProviderId = Convert.ToInt32(ddlRehabProvider.SelectedValue);
                         }
                         try
                         {
                             if (!string.IsNullOrEmpty(txtRehabFeeCharged.Text.Trim()))
                             {
                                 obj.RehabilitationFee = Convert.ToDecimal(txtRehabFeeCharged.Text.Trim());
                             }
                             
                             obj.Save();
                             hdnRehabilitationId.Value = "";
                         }
                         catch (Exception)
                         {

                         }
                         finally
                         {
                             BindRehabilitation();

                         }



                         ScriptManager.RegisterStartupScript(upRehab, this.GetType(), "Close", "closepopup()", true);
                     }
                 }
             }
             catch (Exception)
             {

                 throw;
             }
         }
         decimal TotalRehabCurrentCase = 0;
         protected void grdRehabilitation_RowDataBound(object sender, GridViewRowEventArgs e)
         {
             if (e.Row.RowType == DataControlRowType.DataRow)
             {
                 if (DataBinder.Eval(e.Row.DataItem, "RehabilitationFee") != DBNull.Value)
                     TotalRehabCurrentCase += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "RehabilitationFee"));
             }
             if (e.Row.RowType == DataControlRowType.Footer)
             {
                 txtRehabCurrentCaseTotal.Text = String.Format("{0:n2}", TotalRehabCurrentCase);

                 totalFeeCharged(Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbvisitfee.Value)) ? hdnhcbvisitfee.Value : "0"), Convert.ToDecimal((!string.IsNullOrEmpty(hdnhcbtelephonevisitfee.Value)) ? hdnhcbtelephonevisitfee.Value : "0"), Convert.ToDecimal((!string.IsNullOrEmpty(txtCurrentCaseTotal.Text)) ? txtCurrentCaseTotal.Text : "0"));
                 txtRehabcost.Text =  txtRehabCurrentCaseTotal.Text;
                 txttotalcasecost.Text = (Convert.ToDecimal(!String.IsNullOrEmpty(txtRehabcost.Text) ? txtRehabcost.Text : "0")+Convert.ToDecimal((!string.IsNullOrEmpty(txtFeeCharged.Text)) ? txtFeeCharged.Text : "0")).ToString();
             }
         }

         private void totalFeeCharged(decimal TotalManagementCost, decimal HCBTelephoneVisitFee, decimal HCBVisitFee)
         {
             decimal total = TotalManagementCost + HCBTelephoneVisitFee + HCBVisitFee;
             txtFeeCharged.Text = total.ToString();
             //decimal tot=0;
             //tot = total + Convert.ToDecimal(!String.IsNullOrEmpty(txtRehabcost.Text) ? txtRehabcost.Text : "0");
             //txttotalcasecost.Text=tot.ToString();
         }

         protected void ddlSchemeName_SelectedIndexChanged(object sender, EventArgs e)
         {
             if (ddlSchemeName.SelectedValue != "0" && ddlSchemeName.SelectedIndex != -1)
             {                 
                 DataSet ds = new DataSet();
                 ClientInfo obj = new ClientInfo();
                 ds = obj.GetSchemeNumber(Convert.ToInt32(ddlSchemeName.SelectedValue));
                 if (ds != null)
                 {
                     if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                     {
                         txtSchemeNumber.Text = ds.Tables[0].Rows[0]["RelatedValue"].ToString();
                     }
                 }
             }
         }
         [System.Web.Services.WebMethod(EnableSession = true)]
         public static string SetSchemeNumber(string ID)
         {
             string SchemeNumber = string.Empty;
             try
             {
                
                 DataSet ds = new DataSet();
                 ClientInfo obj = new ClientInfo();
                 ds = obj.GetSchemeNumber(Convert.ToInt32(ID));
                 if (ds != null)
                 {
                     if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                     {
                         SchemeNumber = ds.Tables[0].Rows[0]["RelatedValue"].ToString();
                     }
                 }
             }
             catch (Exception ex)
             {
                 //ErrorLog.WriteLog("Users.aspx", "SetActive", ex.Message);
             }
             return SchemeNumber;
         }

         //protected void txtFeeCharge_TextChanged(object sender, EventArgs e)
         //{

         //}
    }

}
