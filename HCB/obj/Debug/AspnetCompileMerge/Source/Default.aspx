﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="HCB.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div style="text-align: center; width:80%">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 190px; height: 160px" valign="middle">
                    <img src="Images/FirstDataLoginSuccess.png" vspace="15" alt="Login Success" />
                </td>
                <td class="MainText" style="width: 535px; text-align:left; font-size:15.5px; font-family:Calibri; color:#080808;" valign="top">
                    <br />
                    <br />
                    <span><strong>
                        Welcome
                        <asp:Label ID="lblUserFirstName" runat="server" Text=""></asp:Label>
                        </strong>
                    </span><br />
                    <br />
                    Access to this site is restricted to authorised <br />
                    individuals of MetLife and HCB staff only.  If you do<br />
                    not have the required authority to use this site,<br />
                    then <strong>logout immediately.</strong><br /><br />

                    The information on this site is confidential and as <br />
                    such is subject, but not limited to the following <br />
                    information security legislation:<br /><br />

                    Computer Misuse Act 1990 (UK)<br />
                    Data Protection Act 1998 (UK)<br />
                    Serious Crime Act 2015 (UK)<br />
                    Data Protection Act 1988 (Ireland)<br />
                    Data Protection (Amendment) Act 2003 (Ireland)
                    
                    &nbsp;<br />
                    <br />
                    <asp:Label ID="lblNews" runat="server" CssClass="News"></asp:Label>
                </td>
            </tr>            
        </table>
    </div>
</asp:Content>
