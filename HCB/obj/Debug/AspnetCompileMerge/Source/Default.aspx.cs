﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserType"] != null)
            {
                lblUserFirstName.Text = AntiXss.HtmlEncode(Session["LoggedInUserName"].ToString());
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
            //            if (Session["UserType"].ToString() == "Admin")
            //            {
            //                DisableNonAdminControls();
            //                Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
            //            }
            //            else if (Session["UserType"].ToString() == "Inco")
            //            {
            //                DisableNonIncoControls();
            //                Page.ClientScript.RegisterStartupScript(this.GetType(), "NonAdmin", "DisableButtons();", true);
            //            }
            //            else if (Session["UserType"].ToString() == "Nurse")
            //            {
            //                DisableNonNurseControls();
            //            }

            //            hidLogUserType.Value = Session["UserType"].ToString();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Response.Redirect("~/Login.aspx");
            //    }
           
        }
    }
}