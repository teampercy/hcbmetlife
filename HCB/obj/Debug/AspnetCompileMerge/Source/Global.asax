﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System" %>
<script Language="C#" RunAt="server">

   
    public void Application_Start(object sender, EventArgs e)
    {
        RegisterRoutes(System.Web.Routing.RouteTable.Routes);
        Application["OnlineUsers"] = 0;  
    }

    void RegisterRoutes(System.Web.Routing.RouteCollection routes)
    {
        routes.Ignore("{resource}.axd");
        routes.Ignore("{resource}.axd/{*pathInfo}");
        routes.RouteExistingFiles = false;

        //   routes.MapPageRoute("About",
        //"{FooterType}",
        //"~/FooterPages/About.aspx", true);
        
        routes.MapPageRoute("Admin",
            "{page}/{type}",
            "~/MasterList.aspx", true);
       
        
       

        //routes.MapPageRoute("Assessor",
        //    "{page}/{type}/{Name}",
        //    "~/Assessordetails.aspx", true);

        //routes.MapPageRoute("AboutBrowse", "{FooterType}", "~/FooterPages/{About}.aspx");

        // routes.MapPageRoute("UserURL",
        //"{UID}/{ProType}/{UserType}",
        //"~/ProfessionalHome.aspx", true);
        
    }

    protected void Session_Start(Object sender, EventArgs e)
    {
        Application.Lock();
        Application["OnlineUsers"] = (int)Application["OnlineUsers"] + 1;
        Application.UnLock();
    
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate 
        // mode is set to InProc in the Web.config file. 
        // If session mode is set to StateServer or SQLServer, 
        // the event is not raised.
        Application.Lock();
        if ((int)Application["OnlineUsers"] > 0)
            Application["OnlineUsers"] = (int)Application["OnlineUsers"] - 1;
        else
            Application["OnlineUsers"] = 0;  
        Application.UnLock();
    }
    void Application_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();

        if (ex is HttpRequestValidationException)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            int index = path.LastIndexOf('/');
            string strfilename = path.Substring(index + 1);
            Response.Clear();
            Response.StatusCode = 200;

            //Response.Write("hello");
            Response.Write(@"<html><head><title>HTML Not Allowed</title></head><body style='font-family: Arial, Sans-serif;'><h1>Oops!</h1><p>I'm sorry, but HTML entry is not allowed on that page.</p><p>Please make sure that your entries do not contain any angle brackets like &lt; or &gt;.</p><p><a href="+strfilename+">Go back</a></p></body></html>");
            Response.End();
        }
    }
</script>
