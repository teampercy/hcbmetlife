﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncoUsers.aspx.cs" MasterPageFile="~/Site.Master" Inherits="HCB.IncoUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function Validate()
        {
            
            if (document.getElementById('<%=ddlExprotTo.ClientID%>').value == "0")
            {
                alert('Please select type to Export:-PDF or EXCEL');
                return false;
            }
            return true;
        }
    </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="80%" style="margin: 0 auto;"
        class="MainText" id="tableMain">
        <tr>
            <td colspan="2" align="center">
                <div class="portlet-header">
                    <asp:Label CssClass="PageTitle" ID="lblTitle" runat="server">Inco Users</asp:Label>
                </div>
                <br />
            </td>
        </tr>
        <tr>
             <td style="float: left">
                <b>Export to </b>
                <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown">
                    <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                    <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo"
                    ValidationGroup="valdtdown"></asp:RequiredFieldValidator>
                 <asp:Button ID="lnkExport" ValidationGroup="valdtdown" runat="server" Text="Export"
                    OnClick="lnkExport_Click" OnClientClick="return Validate();"></asp:Button>
            </td>
            <td>
                
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblerr" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
      <div id="divReport" runat="server" style="overflow:auto;width:80%;">
      
    <div id="wassup">
    
    </div>
       
    
     </div>
</asp:Content>
