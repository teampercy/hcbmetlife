﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using HCBBLL;
using System.IO;
using iTextSharp;
using iTextSharp.text.api;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using Winnovative.WnvHtmlConvert;
using System.Globalization;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class IncoUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
                {
                    LoadIncoUsers();
                    lnkExport.Enabled = true;

                }
                else
                {
                    lnkExport.Enabled = false;
                    //Response.Redirect("~/Accessdenied.aspx", false);
                    Server.Transfer("~/Accessdenied.aspx");
                }
            }
        }
        private void LoadIncoUsers()
        {
            try
            {
                DataSet ds = MasterListBL.GetIncoUsers();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    LoadDataset(ds.Tables[0]);
                    ViewState.Add("dsreport", ds);
                    lblerr.Text = AntiXss.HtmlEncode(" "); 
                    lnkExport.Enabled = true;
                }
                else
                {
                    ViewState["dsreport"] = null;
                    divReport.InnerHtml = "";
                    lblerr.Text = AntiXss.HtmlEncode("No record exists"); 
                    lnkExport.Enabled = false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public void LoadDataset(DataTable dt)
        {
            #region code for dynemic report genretion
            StringBuilder sb = new StringBuilder();

            double grandTotal = 0.0;

            sb.Append("<table style='text-align:center;width:100%;' cellpadding=\"0\" border=\"2\" cellspacing=\"0\" class=\"display\" rel=\"datatable\" id=\"example\">");
            sb.Append("<thead>");
            sb.Append(" <tr style=\"background-color:#96B1D9 !important; \">");
            sb.Append("<td colspan='" + dt.Columns.Count + "' align='center' ><span class=\"PageTitle\" style=\"color:Black;\"> <b> INCO USERS </b> </span></td></tr>");
            sb.Append("<tr style=\"background-color: #2459A7 !important; color: white;\">");


            foreach (DataColumn dc in dt.Columns)
            {
                sb.Append(" <th><u>" + dc.ColumnName + "</u></th> ");
            }
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            DataTable dt2 = dt;
            if (dt2.Rows.Count > 0)
            {

                foreach (DataRow dr in dt2.Rows)
                {
                    sb.Append(" <tr>");

                    foreach (DataColumn dc in dt2.Columns)
                    {

                        if (dr.ToString() != "")
                        {
                            sb.Append(" <td>" + dr[dc.ColumnName].ToString() + "</td> ");

                        }
                        else
                            sb.Append(" <td>" + "." + "</td> ");
                    }
                    sb.Append("</tr> ");
                }
                sb.Append("</tbody></table>");
                sb.Append("<div style='text-align:right;width:90%;font-size:small'>");
                sb.Append("</div>");


                divReport.InnerHtml = sb.ToString();

            #endregion
            }
        }
        protected void lnkExport_Click(object sender, EventArgs e)
        {
            try
            {
                //  int Itype = SetType(ddlReportName.SelectedItem.Value.Trim());
                if (ViewState["dsreport"] != null)
                {
                    // DataSet ds = (DataSet)ViewState["dsreport"];
                    // if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string strStyles = "<head><style> tr.even { background-color: #F5FDF5; }</style></head>";
                        switch (ddlExprotTo.SelectedItem.Value)
                        {
                            case "2":
                                ExportToExcel(strStyles);
                                break;
                            case "1":
                                //ExportToPDf(ds.Tables[0]);
                                ConvertHTMLStringToPDF(strStyles, divReport.InnerHtml);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public void ConvertHTMLStringToPDF(string strStyle, string strHTML)
        {
            DataSet ds = new DataSet();
            ds = MasterListBL.GetPDFLicensekey();
            string LicenseKeyPdf = string.Empty;
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    LicenseKeyPdf = ds.Tables[0].Rows[0]["PdfLicenseKey"].ToString();
                }

            }

            // Dim htmlString As String = "<style>BODY{FONT-SIZE: 8.5pt;WORD-SPACING: normal;FONT-FAMILY : arial;} TD{FONT-SIZE: 8pt;FONT-FAMILY: arial;} .Box1{BORDER-RIGHT: #42419c 1px solid;BORDER-TOP: #42419c 1px solid;BORDER-LEFT: #42419c 1px solid;BORDER-BOTTOM: #42419c 1px solid} .textlab2{FONT-WEIGHT: bold;FONT-SIZE: 12px;WORD-SPACING: normal;COLOR: #cc0000;FONT-FAMILY: arial;} .textlab6{FONT-WEIGHT: bold;FONT-SIZE: 10pt;WORD-SPACING: normal;COLOR: navy;FONT-FAMILY: arial;} .textlab7{FONT-WEIGHT: bold;FONT-SIZE: 8.5pt;WORD-SPACING: normal;COLOR: navy;FONT-FAMILY: arial;} .textlab8{FONT-WEIGHT:normal;FONT-SIZE:10pt;WORD-SPACING:normal;FONT-FAMILY:arial;} 	</style><table class='Box1' width='90%' align='center'><tr><td>" & lblLogo.Text + "</td><td> " & lblHeading.Text + "</td></tr><tr><td colspan='2'> " & lblHtml.Text + lblNotes.Text + lblInstructions.Text & "</td></tr></table>"

            string htmlString = strStyle + strHTML;// strstyles + genrateHTML(strHTML);
           

            // Create the PDF converter. Optionally you can specify the virtual browser 
            // width as parameter. 1024 pixels is default, 0 means autodetect
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = LicenseKeyPdf;
            //pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Winnovative HTML to PDF Converter";

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename=IncoUser" + "_"+ DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
            response.Flush();
            response.BinaryWrite(pdfBytes);
            response.Flush();
            response.End();
        
        }
        public void ExportToExcel(string style)
        {
            try
            {
                Response.ContentType = "application/x-msexcel";
                Response.AddHeader("Content-Disposition", "attachment; filename=IncoUsers.xls");
                Response.ContentEncoding = Encoding.Default;
                StringWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                divReport.RenderControl(hw);
                Response.Write(style + tw.ToString());
                Response.End();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}