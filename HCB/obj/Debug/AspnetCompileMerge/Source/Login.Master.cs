﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HCB
{
    public partial class Login1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.LCID = 2057;
            CreateMenu();

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/Login.aspx");
        }
        public void CreateMenu()
        {
            if (Session["LoggedInUserId"] != null)
            {
                btnLogout.Visible = true;

            }

            else
            {
                btnLogout.Visible = false;

            }
        }
    }
}