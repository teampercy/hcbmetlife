﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="HCB.Login" %>
<%@ Register Src="~/ChangePassword.ascx" TagName="cp" TagPrefix="cp1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<meta http-equiv="Cache-Control" content="no-store" />
    <meta http-equiv="Pragma" content="no-cache" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="80%">
        <tr>
             <td style="width: 190px; height: 160px" valign="top">
                <img src="Images/FirstDataLoginSuccess.png" vspace="15" alt="Login Success" />
            </td>            
            <td class="MainText" style="width: 535px;" valign="top">     
                <table id="tblLogin" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" class="MainText"
                    width="100%">
                    <tr><td colspan="2">  <br />
                <br /><span style="font-size: 12pt"><strong>Login&nbsp;</strong></span><br />
                <br />
                Please enter your user name (email address) and password.<br /> <br />
                &nbsp;&nbsp;<br /></td></tr>
                    <tr>
                        <td style="text-align: right">
                            User Name:
                        </td>
                        <td style="text-align: left">
                            &nbsp;<asp:TextBox ID="txtUserName" runat="server" autocomplete="off" Width="60%" ValidationGroup="validatelogin"
                                CssClass="Textbox" AutoCompleteType="Disabled" BackColor="#ffffcc"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ValidationGroup="validatelogin" ID="userRequiredfieldvalidator" runat="server" ControlToValidate="txtUserName"
                                CssClass="ErrorMessage" ErrorMessage="User Name/Email is required.">
														<img src='images/Exclamation.jpg'></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            Password: 
                        </td>
                        <td style="text-align: left">
                            &nbsp;<asp:TextBox ID="txtPassword" runat="server" Width="60%" CssClass="Textbox" ValidationGroup="validatelogin"  BackColor="#ffffcc"
                                TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="passwordRequiredFieldValidator" runat="server" ControlToValidate="txtPassword" ValidationGroup="validatelogin"
                                CssClass="ErrorMessage" ErrorMessage="Password is required." Display="Dynamic">
														<img src='images/Exclamation.jpg'></asp:RequiredFieldValidator>
                                                        <%--<asp:RegularExpressionValidator ID="passowordRegularExpressionValidator" ValidationExpression="(?=^.{7,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_\d])(?=^.*[^\s].*$).*$"
                                                         runat="server" ControlToValidate="txtPassword" ValidationGroup="validatelogin"
                                                         CssClass="ErrorMessage" ErrorMessage="Password must be greater than 6 letter and must contain atleast one Alphanuemeric, Uppercase and Lowercase character. "
                                                          Display="Dynamic">
                                                         <img src='images/Exclamation.jpg'></asp:RequiredFieldValidator>
                                                         </asp:RegularExpressionValidator>--%>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="text-align: left">
                            &nbsp;<asp:Button ID="loginButton" ValidationGroup="validatelogin" runat="server" CssClass="ButtonClass" Height="22px"
                                Text="Login" Width="75px" OnClick="loginButton_Click" />
                            &nbsp; &nbsp;
                        </td>

                    </tr>
                    <tr><td colspan="2" align="center">
                        <asp:Label ID="statusLabel" runat="server" CssClass="ErrorMessage" ForeColor="Red"
                               ></asp:Label>
                        </td></tr>
                    <tr>
                        <td colspan="2" width="200" style="height: 90px">
                            <asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="ErrorMessage"
                                DisplayMode="List" Height="1px" Width="512px" />
                        </td>
                    </tr>
                </table>
                  <table id="tblChangePassword" runat="server" border="0" width="450px" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th style="font-size: large; color: #3B73C7;" colspan="2" align="center">Reset Password
                                 <%--  color: #3B73C7;--%>
                                        <hr width="100%" />
                                    </th>
                                </tr>
                             <%--   <tr>
                                    <td>
                                        <asp:Label ID="lblChangepassword" runat="server" Visible="false" CssClass="ErrorText" ForeColor="Red"></asp:Label></td>
                                </tr>--%>
                                <tr>
                                    <td>
                                      
                                        <cp1:cp ID="ucChangePassword" Title="te" runat="server" />
                                    </td>
                                </tr>
                            </table>
                <br />
            </td>
        </tr>
     <%--   <tr>
            <td style="width: 190px; height: 160px" valign="top">
                <img src="Images/MeetingImage.jpg" alt="Meeting" style="border-right: lightgrey 1px solid;
                    border-top: lightgrey 1px solid; border-left: lightgrey 1px solid; border-bottom: lightgrey 1px solid" />
            </td>
        </tr>
        <tr>
            <td style="width: 190px; height: 160px" valign="top">
                <img src="Images/HandshakeImage.jpg" alt="HandShake" style="border-right: lightgrey 1px solid;
                    border-top: lightgrey 1px solid; border-left: lightgrey 1px solid; border-bottom: lightgrey 1px solid" />
            </td>
        </tr>--%>
    </table>
</asp:Content>
