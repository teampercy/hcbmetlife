﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data.OleDb;
using System.Data.Common;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.LCID = 2057;
            if (!IsPostBack)
            {
                tblLogin.Visible = true;
                tblChangePassword.Visible = false;
            }
        }

        private void ValidateLogin(string UserName, string Password)
        {
            try
            {
                Users user = new Users();
                user.EmailAdd = UserName;
                user.password = Password;
                string Res = user.Login();
                if (Res == "Ok")
                {
                    SetLoginCredentials(user);
                }
                else if (Res == "error")
                {
                    statusLabel.Text = AntiXss.HtmlEncode(Res);
                }
                else
                {
                    statusLabel.Text = AntiXss.HtmlEncode(Res);
                }
                //Res = "test";
                //statusLabel.Text = Res;
            }
            catch (Exception ex)
            { 
                
            }
        }

        protected void loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                //if (DateTime.Now.Date < DateTime.Parse("14/07/2012"))
                //{
                    string ValUsername = AntiXss.HtmlEncode(txtUserName.Text.Trim().ToString());                   
                    string password = AntiXss.HtmlEncode(txtPassword.Text.Trim().ToString());
                    string ValPass = Encryption.Encrypt(password);
                    string decryptpassword = Encryption.Decrypt("J+WkvQqroAxCCRGFwU84e3P7LVMNPLojoAU3GYPddyQ=");
                    string decryptpassword12 = Encryption.Decrypt("lGBrp9v8QJ1mdBLrhcG/YS4Gy+lI6JnNzWK6k8YlNQQ=");
                    ValidateLogin(ValUsername, ValPass);
                //}
                //else
                //{
                //    statusLabel.Text = "Some Error Occured.";
                //}
            }
            catch (Exception ex)
            {
                statusLabel.Text = AntiXss.HtmlEncode("Some Error Occured");
            }
        }

        private void SetLoginCredentials(Users ouser)
        {
            try
            {
                if (ouser.UserId > 0)
                {
                    statusLabel.Visible = false;
                    Session["LoggedInUserId"] = ouser.UserId;
                    Session["TermsAccepted"] = ouser.TermsAccepted;
                    Session["LogUserEmailAdd"] = ouser.EmailAdd;
                    Session["UserType"] = ouser.UserType;
                    Session["LoggedInUserName"] = ouser.Name;
                    Session["LogUserActiveAcct"] = ouser.AccountActive;
                    
                    try
                    {
                        Session["LoggedInIncoId"] = ouser.IncoId;
                    }
                    catch (Exception ex)
                    {
                        Session["LoggedInIncoId"] = 0;
                    }
                    Session["LoggedNomivatedNurseId"] = ouser.NominatedNurseId;
                    Session["PasswordExpiryDate"] = ouser.PasswordExpiryDate;
                    Session["LastLoginDate"] = ouser.LastLogin;

                    if (ouser.IsPasswordExpired)
                    {
                        //lblInvalidMsg.Visible = true;
                        //lblInvalidMsg.Text = "Your password has expired.Please change your password.";
                        tblLogin.Visible = false;
                        tblChangePassword.Visible = true;
                        //lblChangepassword.Visible = true;
                        //lblChangepassword.Text = "Your password has expired.Please change your password.";
                        return;


                    }

                    if (string.IsNullOrEmpty(ouser.LastLogin))
                    {
                        if (ouser.UserType.ToLower() == "admin")
                        {
                            Response.Redirect("~/UpdateAdminUser.aspx?id=2", false);
                            return;
                        }
                        else
                        {
                            Response.Redirect("~/UserEditProfile.aspx?nid=2", false);
                            return;
                        }
                    }
                    //if (ouser.PasswordExpiryDate > DateTime.Now)
                    //    Response.Redirect("~/Default.aspx", false);
                    //else
                    //{
                    //    if (ouser.UserType.ToLower() == "admin")
                    //        Response.Redirect("~/UpdateAdminUser.aspx?id=2", false);
                    //    else
                    //        Response.Redirect("~/UserEditProfile.aspx?nid=2", false);
                    //}

                }
                else
                {
                    statusLabel.Visible = true;
                    //statusLabel.Text = "Invalid Username, Password. Please Try Again.";
                    statusLabel.Text = AntiXss.HtmlEncode("Some Error Occured");
                }
                Response.Redirect("~/Default.aspx", false);
            }
            catch (Exception ex)
            {
                //statusLabel.Text = ex.Message;
                statusLabel.Text = AntiXss.HtmlEncode("Some Error Occured");
            }
        }
    }
}