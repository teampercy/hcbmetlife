﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MIAverageTimeDetail.aspx.cs" Inherits="HCB.MIAverageTimeDetail" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" charset="utf-8">
        function ValidateDate() {
            try {
                var intFlag = 1;

                var txtFromDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
                var txtToDate = document.getElementById("<%=txtToDate.ClientID%>").value;

                var dtFromDate = new Date();
                dtFromDate.setFullYear(txtFromDate.substring(6, 10), txtFromDate.substring(3, 5) - 1, txtFromDate.substring(0, 2));

                var dtToDate = new Date();
                dtToDate.setFullYear(txtToDate.substring(6, 10), txtToDate.substring(3, 5) - 1, txtToDate.substring(0, 2));

                if (dtFromDate > dtToDate) {
                    intFlag = -1;
                    alert("From Date should not exceed To Date");
                }
                if (intFlag == 1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            catch (err) {
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table border="0" cellpadding="0" cellspacing="0" width="80%" style="margin: 0 auto;"
        class="MainText" id="tableMain">
        <tr>
            <td align="center" colspan="3">
                <div class="portlet-header">
                    <asp:Label CssClass="PageTitle" ID="lblTitle" runat="server">MI Charge Average Time Summary Report</asp:Label>
                </div>
                <br />
            </td>
        </tr>
        <tr>
            <td style="width: 65%" valign="middle">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="txtFromDate" ErrorMessage="RequiredFieldValidator"
                    ForeColor="Red" ValidationGroup="vgGenReport">*</asp:RequiredFieldValidator>
                <strong>From : </strong>

                <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10" Width="100px"
                    CssClass="Textbox" TabIndex="91" onchange="javascript:return validateDate(this);"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtToDate" ErrorMessage="RequiredFieldValidator"
                    ForeColor="Red" ValidationGroup="vgGenReport">*</asp:RequiredFieldValidator>
                <cc1:MaskedEditExtender ID="MaskedEditExtender24" runat="server" Mask="99/99/9999"
                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtFromDate">
                </cc1:MaskedEditExtender>

                <strong>To : </strong>
                <asp:TextBox ID="txtToDate" runat="server" MaxLength="10" Width="100px"
                    CssClass="Textbox" TabIndex="91" onchange="javascript:return validateDate(this);"></asp:TextBox>
                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtToDate">
                </cc1:MaskedEditExtender>

                &nbsp;<%--     Height="40px" Width="150px" BackColor="#9681FC" ForeColor="White"  --%><asp:Button
                    ID="btnGetReport" runat="server" Text="Generate Report" ValidationGroup="vgGenReport"
                    ToolTip="click to genrate report" OnClick="btnGetReport_Click" />

            </td>
            <td style="float: left">
                <b>Export to </b>
                <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown">
                    <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                    <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo"
                    ValidationGroup="valdtdown"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="lnkExport" ValidationGroup="valdtdown" runat="server" Text="Export"
                    OnClick="lnkExport_Click" Visible="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblerr" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="divDetail" runat ="server" >
                <table width="100%" visible="false" id="tblDetail" runat ="server" border="1" cellpadding="0" cellspacing="0">
                    <tr style="background-color:#96B1D9 !important;"><td colspan="2" align="center"><span style="color:black;font-weight:bold;">MI Charge Average Time Summary Report</span> </td></tr>
                    <%--<tr>
                        <td>Scheme Name</td>
                        <td align="center">
                            <asp:Label ID="lblSchemeName" runat="server"></asp:Label></td>
                    </tr>
                     <tr>
                        <td>MetLife Scheme Number</td>
                        <td align="center">
                            <asp:Label ID="lblSchemeNumber" runat="server"></asp:Label></td>
                    </tr>--%>
                     <tr>
                        <td>Average Contact Time</td>
                        <td align="center">
                            <asp:Label ID="lblAvgContactTime" runat="server"></asp:Label></td>
                    </tr>

                    <tr>
                        <td>Average Call Time</td>
                        <td align="center">
                            <asp:Label ID="lblAvgCallTime" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Average Report Completion Time</td>
                        <td align="center">
                            <asp:Label ID="lblRepCom" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Average Full Cycle Time</td>
                        <td align="center">
                            <asp:Label ID="lblAvgFullCycle" runat="server"></asp:Label></td>
                    </tr>
                </table>
                    </div>
            </td>
        </tr>
    </table>
</asp:Content>
