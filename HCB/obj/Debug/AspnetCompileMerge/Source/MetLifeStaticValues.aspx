﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="MetLifeStaticValues.aspx.cs" Inherits="HCB.MetLifeStaticValues" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/ControlValidation.js"></script>

    <script type="text/javascript">
       
        function ValidateEmail() {
            debugger;
            var txtMetLifeEmail = document.getElementById('<%=txtMetLifeEmail.ClientID%>');
            
            if ($.trim(txtMetLifeEmail.value) != "") {
                if (!IsValidEmailId(txtMetLifeEmail)) {
                    
                    
                    document.getElementById('<%=lblMessage.ClientID%>').innerHTML = "Please enter valid MetLife EmailId";
                    txtMetLifeEmail.focus();
                    return false;
                }
            }
            
            return true;
        }
    </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table border="0" cellpadding="0" cellspacing="0" style="width: 730px; margin: 0 auto;"
            class="MainText" id="tableMain">
         <tr><td colspan="2"><br /></td></tr>
        <tr>
            <td colspan="2">
                <b>Manage MetLife Static Values:-</b>
            </td>
        </tr>
        <tr><td colspan="2"><br /></td></tr>
        <tr>
            <td colspan="2" style="height: 18px;">
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>MetLife Telephone No.:</td>
            <td>
                <asp:TextBox ID="txtMetLifeTel" runat="server" Width="270px" onkeypress="javascript:return numberOnly(this,event);" MaxLength="12">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td>MetLife EmailId: </td>
            <td>
                <asp:TextBox ID="txtMetLifeEmail" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr><td colspan="2"><br /></td></tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="ADD" Width="100px"  OnClick="btnSave_Click" OnClientClick="javascript:return ValidateEmail();" />
            </td>
        </tr>
    </table>
     </asp:Content>