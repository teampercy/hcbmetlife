﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using System.Data.OleDb;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class MetLifeStaticValues : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }
        private void LoadData()
        {
            MetLifeStaticValue obj = new MetLifeStaticValue();
            DataSet ds = new DataSet();
            ds = obj.GetDetailMetLifeStaticValues();
            if (ds != null)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    btnSave.Text = "UPDATE";
                    txtMetLifeTel.Text = dt.Rows[0]["MetLifeTelephone"].ToString();
                    txtMetLifeEmail.Text = dt.Rows[0]["MetLifeEmail"].ToString();

                }
                else
                {
                    txtMetLifeTel.Text = string.Empty;
                    txtMetLifeEmail.Text = string.Empty;
                }
            }
            else
            {
                txtMetLifeTel.Text = string.Empty;
                txtMetLifeEmail.Text = string.Empty;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MetLifeStaticValue obj = new MetLifeStaticValue();
                obj.MetLifeTelephone = txtMetLifeTel.Text.Trim();
                obj.MetLifeEmail = txtMetLifeEmail.Text.Trim();
                obj.InsertMetLifeStaticValues();
                lblMessage.Text = AntiXss.HtmlEncode("Record Saved Successfully.");


            }

            catch (Exception ex)
            {
                //ErrorLog.WriteLog("Admin/CSSentToEmails.aspx", "btnSave_Click", ex.Message);
            }
        }
    }
}