﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace HCB
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.LCID = 2057;
            CreateMenu();
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/Login.aspx");
        }

        public void CreateMenu()
        {
            if (Session["LoggedInUserId"] != null)
            {
                if (Session["TermsAccepted"] != null && Session["TermsAccepted"].ToString() == "1")
                {
                    imgLogo.Src = Request.ApplicationPath.TrimEnd('/') + "/Images/MetLifeLogo.jpg";
                    //imgLogo.Src = Request.ApplicationPath.TrimEnd('/') + "/Images/Aviva logo - Copy.jpg";
                    imgHCBLogo.Src = Request.ApplicationPath.TrimEnd('/') + "/Images/HCBLogo.jpg";

                    DateTime dt = DateTime.Now.AddDays(-1);
                    if (Session["PasswordExpiryDate"] != null)
                    {
                        dt = (DateTime)Session["PasswordExpiryDate"];
                        if (dt > DateTime.Now)
                        {
                            hpViewOpen.NavigateUrl = "ViewClientRecords.aspx?mode=open";
                            hpViewOpen.CssClass = "menulink";

                            hpViewClose.NavigateUrl = "ViewClientRecords.aspx?mode=closed";
                            hpViewClose.CssClass = "menulink";

                            hpSearch.NavigateUrl = "ClientSearch.aspx";
                            hpSearch.CssClass = "menulink";

                            if (Session["UserType"].ToString() == "Inco")
                            {
                                tdAudit.Visible = true;
                                hpAuditTrail.NavigateUrl = "audittrail.aspx";
                                hpCreate.CssClass = "menulink";
                            }
                            else
                            {
                                tdAudit.Visible = false;
                            }

                            if (Session["UserType"] != null && Session["UserType"].ToString() == "Inco")
                            {
                                hpCreate.NavigateUrl = "ClientRecord.aspx?mode=create";
                                hpCreate.CssClass = "menulink";
                            }
                            else
                            {
                                hpCreate.CssClass = "menulinkdisabled";
                            }

                            if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
                            {
                                hpAdministration.NavigateUrl = "Administration.aspx";
                                hpAdministration.CssClass = "menulink";
                                hpEditProfile.CssClass = "menulinkdisabled";
                            }
                            else
                            {
                                hpEditProfile.NavigateUrl = "UserEditProfile.aspx?nid=1";
                                hpEditProfile.CssClass = "menulink";
                                
                                if (Session["UserType"] != null && Session["UserType"].ToString() == "Inco")
                                {
                                    hpAdministration.NavigateUrl = "Administration.aspx";
                                    hpAdministration.CssClass = "menulink";
                                    //hpAdministration.CssClass = "";
                                }
                                if (Session["UserType"] != null && Session["UserType"].ToString() == "Nurse")
                                {
                                    hpAdministration.CssClass = "menulinkdisabled";
                                    
                                }
                                
                            }

                            hpSitemap.NavigateUrl = "WebForm1.aspx";
                            hpSitemap.CssClass = "menulink";
                            //if (Session["UserType"] != null && Session["UserType"].ToString() == "Inco")
                            //{
                            //    hpCreate.CssClass = "";
                            //    hpCreate.CssClass = "menulink";
                            //}
                            //else
                            //{
                            //    hpCreate.CssClass = "";
                            //    hpCreate.CssClass = "menulinkdisabled";
                            //}
                        }
                        else
                        {
                            hpViewOpen.CssClass = "menulinkdisabled";
                            hpViewClose.CssClass = "menulinkdisabled";
                            hpSearch.CssClass = "menulinkdisabled";
                            hpCreate.CssClass = "menulinkdisabled";
                            hpEditProfile.CssClass = "menulink";
                            hpAdministration.CssClass = "menulinkdisabled";
                            hpSitemap.CssClass = "menulinkdisabled";
                            tdAudit.Visible = false;
                        }
                    }
                }
                else
                {
                    imgLogo.Visible = false;
                    imgHCBLogo.Visible = false;
                    hpViewOpen.CssClass = "menulinkdisabled";
                    hpViewClose.CssClass = "menulinkdisabled";
                    hpSearch.CssClass = "menulinkdisabled";
                    hpCreate.CssClass = "menulinkdisabled";
                    hpEditProfile.CssClass = "menulinkdisabled";
                    hpAdministration.CssClass = "menulinkdisabled";
                    hpSitemap.CssClass = "menulinkdisabled";
                }
            }

            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
}
