﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestMail.aspx.cs" Inherits="HCB.TestMail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table width="60%" align="center" >
            <tr>
                <td>&nbsp;</td>
                <td>
                    <b>Email Test using System.Net.Mail</b>                
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" >
                    <asp:Label runat="server" ID="lblEmailResponse" ForeColor="red" ></asp:Label>           
                </td>
            </tr>
            <tr>
                <td width="20%">
                    <asp:Label ID="lblRemoteHost" runat="server" Text="SMTP Host Server"></asp:Label>   
                </td>
                <td>
                    <asp:TextBox ID="txtRemoteHost" runat="server" text="mail.ttlhosting.co.uk" ></asp:TextBox> 
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Port Number"></asp:Label>   
                </td>
                <td>
                    <asp:TextBox ID="txtportno" runat="server" Text="587"  ></asp:TextBox> 
                </td>
            </tr>
                     <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="UserName"></asp:Label>   
                </td>
                <td>
                    <asp:TextBox ID="txtuseName" runat="server" Text="hcb@ttlcloud.local"  ></asp:TextBox> 
                </td>
            </tr>
                     <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Password"></asp:Label>   
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" Text="T3mppass!"  ></asp:TextBox> 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFromEmail" runat="server" Text="From Email Address"></asp:Label>   
                </td>
                <td>
                    <asp:TextBox ID="txtFromEmail" runat="server" Text="enquiries@hcbgroup.co.uk"  ></asp:TextBox> 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblToEmail" runat="server" Text="To Email Address"></asp:Label>   
                </td>
                <td>
                    <asp:TextBox ID="txtToEmail" runat="server" Text="percy@rhealtech.com" ></asp:TextBox> 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnSendEmail" runat="server" Text = "Send Email" 
                        onclick="btnSendEmail_Click" /> 
                </td>
            </tr>
            
        </table>
    
    </form>
</body>
</html>
