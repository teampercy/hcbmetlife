﻿<%@ Page Title="Edit Profile" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UserEditProfile.aspx.cs" Inherits="HCB.UserEditProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table style="width: 80%; text-align: center" cellpadding="0" border="0" cellspacing="0">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <font color="black" face="Arial" size="3"><b>Update User Profile</b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMsg" runat="server" Style="display: none" Font-Bold="true" Font-Names="Arial"
                        Font-Size="10pt" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="60%" align="center">
                        <tr>
                            <td align="left">
                                <asp:ValidationSummary ID="validateUserSummary" CssClass="ErrorMessage" HeaderText="Summary"
                                    DisplayMode="BulletList" ValidationGroup="validateUser" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="3" cellspacing="0" align="center">
                        <tr>
                            <td colspan="2" align="left">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 50%">
                                <font color="black" face="Arial" size="2">User Name(Email Address):</font>
                            </td>
                            <td style="text-align: left; width: 50%">
                                <asp:TextBox ID="txtUserName" runat="server" ValidationGroup="validateUser" CssClass="Textbox"
                                    Width="170px" Enabled="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtUserName" ToolTip="User Email Required" runat="server"
                                    ControlToValidate="txtUserName" Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateUser"
                                    ErrorMessage="User Email Required">
                           <img src='images/Exclamation.jpg'  alt="User Email Required">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ToolTip="Invalid Email address" ID="RGEtxtUserName"
                                    ControlToValidate="txtUserName" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    CssClass="ErrorMessage" ValidationGroup="validateUser" runat="server" ErrorMessage="Invalid Email address">  <img src='images/Exclamation.jpg' alt="Invalid Email address"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Name:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtName" runat="server" CssClass="Textbox" Width="170px" MaxLength="50" Enabled="true">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtName" runat="server" ControlToValidate="txtName"
                                    Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateUser" ErrorMessage="Name Required"
                                    ToolTip="Name Required">
                           <img src='images/Exclamation.jpg' alt="Name Required">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtName"
                                    ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid Name"
                                    ValidationGroup="validateUser"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Password:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="Textbox"
                                    Width="170px" MaxLength="15" Enabled="true">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtPassword" runat="server" ControlToValidate="txtPassword"
                                    Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateUser" ErrorMessage="Password is Required"
                                    ToolTip="Password is Required">
                           <img src='images/Exclamation.jpg' alt="Password is Required">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="REGtxtPassword" Display="Dynamic" ValidationExpression="(?=^.{7,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_\d])(?=^.*[^\s].*$).*$"
                                    runat="server" ControlToValidate="txtPassword" ValidationGroup="validateUser"
                                    ErrorMessage="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character."
                                    ToolTip="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character.">
                                                         <img src='images/Exclamation.jpg' alt="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character. ">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Address:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="Textbox" TextMode="MultiLine"
                                    Height="109px" Width="170px" Enabled="true">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtAddress" runat="server" ControlToValidate="txtAddress"
                                    Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateUser" ErrorMessage="Address Required"
                                    ToolTip="Address Required">
                           <img src='images/Exclamation.jpg' alt="Address Required">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAddress"
                                    ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$" ErrorMessage="Invalid Address"
                                    ValidationGroup="validateUser"></asp:RegularExpressionValidator>
                                    <%-- ^[a-zA-Z'.\s]{1,50}$ --%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Telephone Number:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtTeleNum" runat="server" CssClass="Textbox" Width="170px" MaxLength="12"
                                onkeypress="return CheckValues(event.keyCode, event.which,this);" Enabled="true">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtTeleNum" runat="server" ControlToValidate="txtTeleNum"
                                    Display="Dynamic" CssClass="ErrorMessage"
                                    ValidationGroup="validateUser" ErrorMessage="TelePhone No. Required" ToolTip="TelePhone No. Required">
                           <img src='images/Exclamation.jpg' alt="TelePhone No. Required">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Nominated Nurse:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlNomiNurse" runat="server" Width="175px" Enabled="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSave" ValidationGroup="validateUser" CausesValidation="true" runat="server"
                        Text="Save" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
