﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HCBBLL;
using System.Data;
using Microsoft.Security.Application;

namespace HCB
{
    public partial class UserEditProfile : System.Web.UI.Page
    {
        #region Variables
        Users user = new Users();
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["LoggedInUserId"] != null && Session["LoggedInUserId"].ToString() != "")
                {
                    if (Session["UserType"] != null && Session["UserType"].ToString() == "Admin")
                    {
                        //Response.Redirect("~/Accessdenied.aspx", false);
                        Server.Transfer("~/Accessdenied.aspx");
                    }
                    else
                    {
                        
                        FillNomiNurse();
                        LoadUserDetails();
                    }
                }

                if (Request.QueryString != null)
                {
                    if (Request.QueryString["nid"] != null && Request.QueryString["nid"] != "")
                    {
                        if (Request.QueryString["nid"].ToString() == "2")
                        {
                            lblMsg.Style.Add("display", "block");
                            lblMsg.Text = AntiXss.HtmlEncode("You have logged in for the first time. Please enter a new Password and Save to continue.");
                            DisableMenuLinks();
                        }
                        else
                        {
                            DisableControls();
                        }
                    }
                    else
                    {
                        DisableControls();
                    }
                }
                else
                {
                    DisableControls();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int id = 0;
                if (CheckPassword())
                {
                    try
                    {
                        try
                        {
                            user.UserId = Convert.ToInt32(Session["LoggedInUserId"].ToString());
                        }
                        catch (Exception ex)
                        {
                            Response.Redirect("~/Login.aspx");
                        }

                        if (txtAddress.Text == "")
                        {
                            user.Address = "";
                        }
                        else
                        {
                            user.Address = txtAddress.Text.Trim();
                        }

                        if (txtName.Text == "")
                        {
                            user.Name = "";
                        }
                        else
                        {
                            user.Name = txtName.Text.Trim();
                        }

                        if (txtPassword.Text == "")
                        {
                            user.password = "";
                        }
                        else
                        {
                            //user.password = txtPassword.Text.Trim();
                            //pasword in encrypted format
                            user.password = Encryption.Encrypt(txtPassword.Text.Trim());

                        }
                        if (txtTeleNum.Text == "")
                        {
                            user.PhoneNum = "";
                        }
                        else
                        {
                            user.PhoneNum = txtTeleNum.Text.Trim();
                        }

                        if (txtUserName.Text == "")
                        {
                            user.EmailAdd = "";
                        }
                        else
                        {
                            user.EmailAdd = txtUserName.Text.Trim();
                        }

                        if (ddlNomiNurse.SelectedValue == "0")
                        {
                            user.NominatedNurseId = 0;
                        }
                        else
                        {
                            user.NominatedNurseId = Convert.ToInt32(ddlNomiNurse.SelectedValue);
                        }


                        if (DateTime.Now > Convert.ToDateTime(ViewState["PasswordExpiryDate"]))
                        {
                            if (ViewState["OldPassword"] != null)
                            {
                                string oldPass = (string)ViewState["OldPassword"];
                                //oldPass = Encryption.Decrypt((string)ViewState["OldPassword"]);
                                if (oldPass == txtPassword.Text.Trim())
                                {
                                    if (ViewState["PasswordExpiryDate"] != null)
                                    {
                                        if (user.PasswordExpiryDate > DateTime.Now)
                                        {
                                            user.PasswordExpiryDate = (DateTime)ViewState["PasswordExpiryDate"];
                                        }
                                        else
                                        {
                                            lblMsg.Text = AntiXss.HtmlEncode("You cannot enter same password again.");
                                            return;
                                        }
                                    }
                                    else
                                        user.PasswordExpiryDate = DateTime.Now.AddDays(60);
                                }
                                else
                                    user.PasswordExpiryDate = DateTime.Now.AddDays(60);
                            }
                            else
                            {
                                user.PasswordExpiryDate = DateTime.Now.AddDays(60);
                            }
                        }
                        else
                        {
                            user.password = Encryption.Encrypt(txtPassword.Text);
                            user.PasswordExpiryDate = Convert.ToDateTime(ViewState["PasswordExpiryDate"]);
                        }

                        //if (Session["LastLoginDate"] != null)
                        //{
                        //    if (Session["LastLoginDate"].ToString() == "")
                        //    {
                        //        user.LastLogin = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                        //    }
                        //    else
                        //    {
                        //        user.LastLogin = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                        //    }
                        //}
                        //else
                        //{
                        //    user.LastLogin = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                        //}

                        id = user.Save();

                        if (id != 0)
                        {
                            lblMsg.Style.Add("display", "block");
                            lblMsg.Text = AntiXss.HtmlEncode("User details are updated successfully.");
                            Session["PasswordExpiryDate"] = user.PasswordExpiryDate;
                            Response.Redirect("~/Default.aspx", false);
                            // ClearControls();
                        }
                        else
                        {
                            lblMsg.Style.Add("display", "block");
                            lblMsg.Text = AntiXss.HtmlEncode("User details are not updated successfully.");
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMsg.Style.Add("display", "block");
                        lblMsg.Text = AntiXss.HtmlEncode("User details are not updated successfully.");
                    }
                }
            }
        }

        #endregion

        #region Methods

        public bool CheckPassword()
        {
            bool IsValid = false;

            if (ViewState["OldPassword"] != null)
            {
                string OldPassword = (string)ViewState["OldPassword"];
                if (txtPassword.Text.Trim() != OldPassword.Trim())
                {
                    IsValid = true;
                    user.PasswordExpiryDate = DateTime.Now.AddDays(90);
                }
                else
                {
                    if (Session["PasswordExpiryDate"] != null)
                    {
                        DateTime dt = (DateTime)Session["PasswordExpiryDate"];
                        if (dt < DateTime.Now.Date)
                        {
                            lblMsg.Style.Add("display", "block");
                            lblMsg.Text = AntiXss.HtmlEncode(" Password can not be same as old password");
                        }
                        else
                        {
                            user.PasswordExpiryDate = dt;
                            IsValid = true;
                        }
                    }
                }
            }
            else
            {
                lblMsg.Style.Add("display", "block");
                lblMsg.Text = AntiXss.HtmlEncode("UserId already exists");
            }
                


            return IsValid;
        }

        private void FillNomiNurse()
        {
            DataSet ds = user.GetClaimConsultant();
            ddlNomiNurse.DataSource = ds.Tables["Consultant"];
            ddlNomiNurse.DataTextField = "FullName";
            ddlNomiNurse.DataValueField = "UserID";
            ddlNomiNurse.DataBind();
            ListItem item = new ListItem("--Select Nurse--", "0");
            ddlNomiNurse.Items.Insert(0, item);
        }

        private void LoadUserDetails()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            
            try
            {
                user.UserId = Convert.ToInt32(Session["LoggedInUserId"].ToString());
            }
            catch (Exception ex)
            {
                user.UserId = 0;
            }

            ds = user.GetUserDet();

            if (ds != null)
            {
                if (user.UserType.Trim() == "Nurse" || user.UserType.Trim().ToLower() == "inco")
                {
                    ddlNomiNurse.Enabled = false;
                }
                dt = ds.Tables["UserDet"];

                try
                {
                    txtUserName.Text = user.EmailAdd;
                }
                catch (Exception ex)
                {
                    txtUserName.Text = "";
                }

                try
                {
                    txtName.Text = user.Name;
                }
                catch (Exception ex)
                {
                    txtName.Text = "";
                }

                try
                {
                    //txtPassword.Text = user.password;
                    string Pwd = Encryption.Decrypt(user.password);
                    txtPassword.Attributes.Add("value", Pwd);
                }
                catch (Exception ex)
                {
                    txtPassword.Text = "";
                }

                try
                {
                    txtAddress.Text = user.Address;
                }
                catch (Exception ex)
                {
                    txtAddress.Text = "";
                }

                try
                {
                    txtTeleNum.Text = user.PhoneNum;
                }
                catch (Exception ex)
                {
                    txtTeleNum.Text = "";
                }

                try
                {
                    ddlNomiNurse.SelectedValue = user.NominatedNurseId.ToString();
                }
                catch (Exception ex)
                {
                    ddlNomiNurse.SelectedValue = "0";
                }

                //if (user.PasswordExpiryDate < DateTime.Now)
                //{
                    ViewState["OldPassword"] = Encryption.Decrypt(user.password);
                //}

                    ViewState["PasswordExpiryDate"] = user.PasswordExpiryDate;
            }
        }

        private void ClearControls()
        {
            txtAddress.Text = "";
            txtName.Text = "";
            txtPassword.Text = "";
            txtTeleNum.Text = "";
            txtUserName.Text = "";
            ddlNomiNurse.SelectedValue = "0";
        }

        private void DisableControls()
        {
            txtAddress.Enabled = false;
            txtName.Enabled = false;
            txtPassword.Enabled = false;
            txtTeleNum.Enabled = false;
            txtUserName.Enabled = false;
            btnSave.Visible = false;
        }

        private void DisableMenuLinks()
        {
            HyperLink hypViewOpen = (HyperLink)this.Page.Master.FindControl("hpViewOpen");
            if (hypViewOpen != null)
            {
                hypViewOpen.Enabled = false;
                hypViewOpen.Style.Add("text-decoration", "none");
                hypViewOpen.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypViewClose = (HyperLink)this.Page.Master.FindControl("hpViewClose");
            if (hypViewClose != null)
            {
                hypViewClose.Enabled = false;
                hypViewClose.Style.Add("text-decoration", "none");
                hypViewClose.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypSearch = (HyperLink)this.Page.Master.FindControl("hpSearch");
            if (hypSearch != null)
            {
                hypSearch.Enabled = false;
                hypSearch.Style.Add("text-decoration", "none");
                hypSearch.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypCreate = (HyperLink)this.Page.Master.FindControl("hpCreate");
            if (hypCreate != null)
            {
                hypCreate.Enabled = false;
                hypCreate.Style.Add("text-decoration", "none");
                hypCreate.ForeColor = System.Drawing.Color.LightGray;
            }

            HyperLink hypAuditTrail = (HyperLink)this.Page.Master.FindControl("hpAuditTrail");
            if (hypAuditTrail != null)
            {
                hypAuditTrail.Enabled = false;
                hypAuditTrail.Style.Add("text-decoration", "none");
                hypAuditTrail.ForeColor = System.Drawing.Color.LightGray;
            }
            HyperLink hypSiteMap = (HyperLink)this.Page.Master.FindControl("hpSitemap");
            if (hypSiteMap != null)
            {
                hypSiteMap.Enabled = false;
                hypSiteMap.Style.Add("text-decoration", "none");
                hypSiteMap.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        #endregion
    }
}