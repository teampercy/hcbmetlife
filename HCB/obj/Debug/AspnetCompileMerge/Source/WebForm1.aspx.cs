﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HCB
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        

        protected void Menu1_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            SiteMapNode node = e.Item.DataItem as SiteMapNode;
            if (!string.IsNullOrEmpty(node["usertype"]))
            {
                //bool isVisible;

                string role = Convert.ToString(node["usertype"]);
                if (role == "admin")
                {
                    e.Item.Parent.ChildItems.Remove(e.Item);
                    
                }
            }

        }

        protected void TreeView1_DataBound(object sender, EventArgs e)
        {

            

        }

        protected void TreeView1_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {

            SiteMapNode node = e.Node.DataItem as SiteMapNode;

            try
            {
                string usertype = Session["UserType"].ToString();

                // for Inco and Nurse  administration is removed
                if (usertype.ToLower() != "admin")
                {
                    if (node.Title.ToLower() == "administration")
                    {
                        e.Node.Parent.ChildNodes.Remove(e.Node);
                    }
                }

                // for administration is EditProfile is removed
                if (usertype.ToLower() == "admin")
                {
                    if (node.Title.ToLower() == "edit profile")
                    {
                        e.Node.Parent.ChildNodes.Remove(e.Node);

                    }

                }
            }
            catch(Exception ex)
            {
            
            }



        }

       
    }
}