﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Administration.aspx.cs" Inherits="HCB.Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta http-equiv="Cache-Control" content="no-store" />
    <meta http-equiv="Pragma" content="no-cache" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table border="0" cellpadding="7" cellspacing="0" width="100%">
            <tr id="trAdminTool" runat ="server" >
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center">
                                <font color="black" face="Arial" size="3"><b>Administrative Tools</b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr id="trUserPro" runat ="server" >
                <td style="text-align: center">
                    <b><a href="UpdateAdminUser.aspx?id=1" style="font-family: Arial; color: Black; font-size: 13px;
                        text-decoration: none">User Profiles</a></b>
                </td>
            </tr>
            <tr id="trSerReg" runat ="server" >
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ServiceRequired"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Service Required List</a></b>
                </td>
            </tr>
            <%--<tr>
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ReasonCanceld"}) %>' style="font-family: Arial; color: Black; font-size: 13px;
                    text-decoration: none">Maintain Reason Cancelled List</a></b>
                </td>
            </tr>--%>
            <tr id="trIllInj" runat ="server" >
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "IllnessInjury"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Illness or Injury</a></b>
                </td>
            </tr>
            <tr id="trClaimClosed" runat ="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ClaimClosed"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Claim Closed Reason</a></b>
                </td>
            </tr>
            <tr id="trReasonClosed"  runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ReasonClosed"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Reason Closed</a></b>
                </td>
            </tr>
            <tr id="trBroker" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "Broker"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Broker</a></b>
                </td>
            </tr>
            <tr id="trFeeCharge" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "feecharged"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Fee Charged</a></b>
                </td>
            </tr>
            <tr id="trCorPartner" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "CorporatePartner"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Corporate Partner</a></b>
                </td>
            </tr>
            <tr id="trProdType" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ProductType"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Product Type</a></b>
                </td>
            </tr>
            <tr id="trWaitPeriod" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "WaitingPeriod"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Waiting Period</a></b>
                </td>
            </tr>
            <tr id="trEmpStatus" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "EmploymentStatus"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Employment Status</a></b>
                </td>
            </tr>
            <tr id="trBrand" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "Brand"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Brand</a></b>
                </td>
            </tr>
            <%--<tr>
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "EmailAddress"}) %>' style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Team Email Address</a></b>
                </td>
            </tr>--%>
            <%--<tr>
                <td style="text-align: center">
                    <b><a href="BackupDatabase.aspx" style="font-family: Arial; color: Black; font-size: 13px;
                        text-decoration: none">Backup Database</a></b>
                </td>
            </tr>--%>

             <tr id="trIncapacity" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "IncapacityDefination"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Incapacity Defination</a></b>
                </td>
            </tr>

            <tr id="trTypeVisit" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "TypeOfVisit"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Type Of Visit</a></b>
                </td>
            </tr>

             <tr id="trTypeCall" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "TypeOfCalls"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Type Of Calls</a></b>
                </td>
            </tr>


             <tr id="trRehabType" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "FundedTreatmentType"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Funded Treatment Type</a></b>
                </td>
            </tr>

             <tr id="trRehabPro" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "FundedTreatmentProvider"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Funded Treatment Provider</a></b>
                </td>
            </tr>
              <tr id="trSchemeName" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "SchemeName"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        Scheme Name</a></b>
                </td>
            </tr>
         <%--     <tr id="trSchemeNumber" runat="server">
                <td style="text-align: center">
                    <b><a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "SchemeNumber"}) %>'
                        style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">
                        MetLife Scheme Number</a></b>
                </td>
            </tr>--%>

             <tr id="trManageMt" runat="server">
                <td style="text-align: center">
                    <b><a href="MetLifeStaticValues.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        Manage MetLife Constants</a></b>
                </td>
            </tr>

            <tr id="trManInfo" runat="server">
                <td style="text-align: center">
                    <b><a href="GenerateReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        Management Information </a></b>
                </td>
            </tr>
            <tr id="trAuditTrail" runat="server">
                <td style="text-align: center">
                    <b><a href="audittrail.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        Audit Trail</a></b>
                </td>
            </tr>
            <tr id="trMIChargeReport" runat="server">
                <td style="text-align: center">
                    <b><a href="MonthlyReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        MI Charge Reports</a></b>
                </td>
            </tr>
             <tr id="trAverage" runat="server">
                <td style="text-align: center">
                    <b><a href="MIAverageTimeSummary.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        MI Charge Average Time Detail</a><%--<a href="MonthlyReport.aspx">ports</a>--%></b>
                </td>
            </tr>
             <tr id="trDetail" runat="server">
                <td style="text-align: center">
                    <b><a href="MIAverageTimeDetail.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        MI Charge Average Time Summary</a><%--<a href="MonthlyReport.aspx">ports</a>--%></b>
                </td>
            </tr>
  <%--          <tr>
                <td style="text-align: center">
                    <b><a href="DynamicReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        Dynamic Reports</a></b>
                </td>
            </tr>--%>
             <tr id="trIncoUser" runat="server">
                <td style="text-align: center">
                    <b><a href="IncoUsers.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">
                        Inco Users</a></b>
                </td>
            </tr>
            <tr id="trOnlineUser" runat="server">
                <td align="left" style="padding-left: 100px;">
                    <asp:Label Text="" ID="lblOnlineUsers" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
