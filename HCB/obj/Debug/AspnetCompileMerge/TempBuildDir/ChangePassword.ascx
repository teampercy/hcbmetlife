﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.ascx.cs" Inherits="HCB.ChangePassword" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script type="text/javascript">

    function ValidateChangePassword() {
        // debugger;

        var PWD = document.getElementById('<%=txtNewPassword.ClientID %>');
        var CPWD = document.getElementById('<%=txtConfirmPassword.ClientID %>');
        var CurrentPassword = document.getElementById('<%=txtCurrentPassword.ClientID %>');
        var username = document.getElementById('<%=txtUsername.ClientID %>');

        if (username.value == "") {
            alert('Please enter username!');
            UName.focus();
            return false;
        }
        if (PWD.value == "") {
            alert('Please enter password!');
            UName.focus();
            return false;
        }
        if (CPWD.value == "") {
            alert('Please enter confirm password!');
            CPWD.focus();
            return false;
        }
        if (PWD.value != CPWD.value) {
            alert('Password does not match!');
            CPWD.focus();
            return false;
        }
        if (CurrentPassword.value == CPWD.value) {

            alert('Current and New Password must not same!');
            CPWD.focus();
            return false;
        }


        return true;
    }

</script>

<table>
    <tr>
        <td>
            <asp:Label ID="lblChangepassword" runat="server" CssClass="ErrorText" ForeColor="Red" Text="Your password has expired.Please change your password."></asp:Label></td>
    </tr>
</table>
<table id="tblUpdateMessage" runat="server" width="90%" align="center" border="0"
    cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <asp:Label Text="" ID="lblUpdateMessage" runat="server"  />
        </td>

    </tr>
    <tr>
        <td>
            <asp:LinkButton ID="lubtRedirectTo" Text="Click here to continue to login." runat="server" Font-Size="Medium"
                OnClick="Unnamed1_Click" />
        </td>
    </tr>
</table>

            <table border="0" cellpadding="0" cellspacing="0" width="60%" align="center">
                <tr>
                    <td align="left">
                        <asp:ValidationSummary ID="validateUserSummary" CssClass="ErrorMessage" DisplayMode="BulletList"
                            ValidationGroup="validateUser" HeaderText="Summary" runat="server" />
                    </td>
                </tr>
            </table>

<table id="tblUpdateInput" runat="server" border="0" cellpadding="4" cellspacing="4"
    width="100%">
   
    <tr>
        <td style="width: 150px;" align="right">
            <label>
                Username
            </label>
        </td>
        <td align="left">
            <asp:TextBox runat="server" ID="txtUsername" Width="200px" />
        </td>
    </tr>
    <tr>
        <td align="right">
            <label>
                Current Password
            </label>
        </td>
        <td align="left">
            <asp:TextBox runat="server" ID="txtCurrentPassword" TextMode="Password" Width="200px" />
        </td>
    </tr>
    <tr>
        <td align="right">
            <label>
                New Password</label>
        </td>
        <td align="left">
            <asp:TextBox runat="server" ID="txtNewPassword" Width="200px" TextMode="Password" />
            <asp:RegularExpressionValidator ID="REGtxtPassword" Display="Dynamic" ValidationExpression="(?=^.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_\d])(?=^.*[^\s].*$).*$"
                runat="server" ControlToValidate="txtNewPassword" ValidationGroup="validateUser"
                ErrorMessage="New Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character."
                ToolTip="New Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character.">
                                                         <img src='images/Exclamation.jpg' alt="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character. ">
            </asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <label>
                Confirm Password</label>
        </td>
        <td align="left">
            <asp:TextBox runat="server" ID="txtConfirmPassword" Width="200px" TextMode="Password" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:Button Text="Reset Password" ID="btnChangePassword" runat="server" OnClientClick="return ValidateChangePassword();" ValidationGroup="validateUser"
                OnClick="btnChangePassword_Click" />
        </td>
    </tr>

</table>

