﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ClientSearch.aspx.cs" Inherits="HCB.ClientSearch"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/Validation.js" type="text/javascript"></script>
    <link id="Link1" rel="Stylesheet" href="Styles/Slate/plugin.css" type="text/css"
        media="screen" title="slate1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function Validate() {
            debugger;
            var bStatus = true;
            var msg = "";
            var txtHcfNO = document.getElementById('<%= txtHcbRef.ClientID %>');
            if (Trim(txtHcfNO.value) != "") {
                //return (checkNumber(txtHcfNO, "HCB Reference"));
            }

            var txtdate = document.getElementById('<%= txtClaimDOB.ClientID %>').value;
            txtdate = Trim(txtdate);
            if (txtdate != "") {
                var msg = validateDate11(txtdate);
                if (msg != "" && msg != "ok") {
                    alert(msg);
                    return false;
                }
            }
            return true;
        }
        function ValidateDOB(dateid) {
            //debugger;
            var msg;
            var sValidChars;
            var nCounter;
            var datval = dateid.value;
            msg = "ok";
            sValidChars = "1234567890/";

            if (Trim(datval) != "") {
                if (datval.length != 10) {
                    msg = " Please enter in exact format (dd/mm/yyyy) e.g. 02/01/2000 in ";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                for (nCounter = 0; nCounter < datval.length; nCounter++) {

                    if (sValidChars.indexOf(datval.substring(nCounter, nCounter + 1)) == -1) {

                        msg = " Please enter in exact format (dd/mm/yyyy)";
                        alert(msg);
                        date.focus();
                        nCounter = datval.length;
                        return msg;
                    }

                }

                if (datval.indexOf("/") != 2) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 3) != 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 6) < 10 && datval.indexOf("/", 6) > 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                var month;
                var day;
                var year;
                month = parseInt(TrimNumber(datval.substring(3, 5)));
                day = parseInt(TrimNumber(datval.substring(0, 2)));
                year = parseInt(datval.substring(6, 10));

                if (month > 12 || month < 1) {
                    msg = " Invalid Month";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (year < 1900 || year > 3000) {
                    msg = " Invalid year. Should be in (1900-3000)";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    if (day > 31 || day < 1) {
                        msg = " Days should not be more than 31 or less than 1 for entered Month";
                        alert(msg);
                        date.focus();
                        return msg;
                    }
                }
                else {
                    if (month == 2 && (year % 4) == 0) {
                        if (day > 29 || day < 1) {
                            msg = " Days should not be more than 29 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else if (month == 2 && (year % 4) != 0) {
                        if (day > 28 || day < 1) {
                            msg = " Days should not be more than 28 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else {
                        if (day > 30 || day < 1) {
                            msg = " Days should not be more than 30 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                }
                //return msg;
               
                //_doPostBack('btnCheck', 'OnClick');
                // return false;
            }
        }

        
    </script>
    <asp:ScriptManager ID="ScptMngr1" runat="server">
    </asp:ScriptManager>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 80%; margin: 0 auto;"
            class="MainText" id="tableMain">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td align="center" style="height: 19px">
                                <font color="black" face="Arial" size="3"><b>Client Search</b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="60%" align="center">
                        <tr>
                            <td align="left">
                                <asp:ValidationSummary ID="validateUserSummary" CssClass="ErrorMessage" HeaderText="Summary" ShowSummary="false"
                                    DisplayMode="BulletList" ValidationGroup="validateSearch" runat="server" ShowMessageBox="true" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="text-align: right;">
                <td style="text-align: right;">
                
                    <table border="0" cellpadding="3" cellspacing="0" align="center">
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">HCB Reference:</font>
                            </td>
                            <td style="text-align: left" colspan="2">
                                <asp:TextBox ID="txtHcbRef" runat="server" CssClass="Textbox" Width="55%">
                                </asp:TextBox>
                                <asp:RegularExpressionValidator ID="REGtxtHcbRef" ErrorMessage="Enter valid HCBReference eg. (DCP**** )" ControlToValidate="txtHcbRef"
                                 ToolTip="Enter valid HCBReference eg. (DCP**** )" Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                    runat="server" ValidationGroup="validateSearch" >
                                     <img src='images/Exclamation.jpg'>
                                </asp:RegularExpressionValidator>
                                <%--^\bDCP\d{4}\b$--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Case Manager:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlCaseMngr" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Case Assessor:</font>
                            </td>
                            <td style="text-align: left" colspan="2">
                                <asp:DropDownList ID="ddlClaimAssessor" runat="server" Width="58%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <%--<tr>
                            <td style="text-align:left">
                                <font color="black" face="Arial" size="2">Reference Number:</font> 
                            </td>
                            <td style="text-align:left;">
                                <asp:TextBox ID="txtRefNum" runat="server" CssClass="Textbox" Width="94%">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RdbRefNumList" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="w">Exact</asp:ListItem>
                                    <asp:ListItem>Contains</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Claimant Surname:</font>
                            </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtClaimSurname" runat="server" CssClass="Textbox" Width="94%">
                                </asp:TextBox>
                                <asp:RegularExpressionValidator ID="REGtxtsurname" ErrorMessage="Enter a valid Surname" ControlToValidate="txtClaimSurname"
                                 ToolTip="Enter a valid Surname." Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                    runat="server" ValidationGroup="validateSearch">
                                     <img src='images/Exclamation.jpg'>
                                </asp:RegularExpressionValidator>
                                <%--^[a-zA-Z'./s]{1,15}$--%>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RdbClaimSurname" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Exact</asp:ListItem>
                                    <asp:ListItem>Contains</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Claimant Forename:</font>
                            </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtClaimForename" runat="server" CssClass="Textbox" Width="94%">
                                </asp:TextBox>

                                <asp:RegularExpressionValidator ID="REGtxtClaimForename" ErrorMessage="Enter a valid Name" ControlToValidate="txtClaimForename"
                                 ToolTip="Enter a valid Name." Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                    runat="server" ValidationGroup="validateSearch">
                                     <img src='images/Exclamation.jpg'>
                                </asp:RegularExpressionValidator>
                                <%--^[a-zA-Z'./s]{1,15}$--%>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RdbClaimForename" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Exact</asp:ListItem>
                                    <asp:ListItem>Contains</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Claimant Date of Birth:</font>
                            </td>
                            <td style="text-align: left;" colspan="2">
                                <asp:TextBox ID="txtClaimDOB" runat="server" MaxLength="10" Width="55%" onchange="javascript:return ValidateDOB(this);"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender26" runat="server" Mask="99/99/9999"
                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtClaimDOB">
                                </cc1:MaskedEditExtender>
                                <img id="img21" src="images/Exclamation.jpg" style="visibility: hidden" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <font color="black" face="Arial" size="2">Claimant Address:</font>
                            </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtClaimAddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ErrorMessage="Enter a valid Address" ControlToValidate="txtClaimAddress"
                                 ToolTip="Enter a valid Address." Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                    runat="server" ValidationGroup="validateSearch">
                                     <img src='images/Exclamation.jpg'>
                                </asp:RegularExpressionValidator>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RdbClaimAdd" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Exact</asp:ListItem>
                                    <asp:ListItem>Contains</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClientClick="return Validate();" ValidationGroup="validateSearch"
                        OnClick="btnSearch_Click" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbltest" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <table>
        <tr>
            <td colspan="3" style="height: 301px" valign="top">
                <div style="text-align: left">
                    <div align="center">
                        <br />
                        <asp:Label ID="lblTitle" runat="server" CssClass="pageheader" Text="Client Records"
                            Visible="false"></asp:Label></div>
                    <div align="left">
                        <br />
                        <asp:GridView Width="650px" ID="gvResults" class="GridView" runat="server" GridLines="Both"
                            AutoGenerateColumns="false" AllowSorting="True" PageSize="3" HeaderStyle-Height="25px"
                            BorderColor="silver" AllowPaging="true" HeaderStyle-BackColor="#2459A7" HeaderStyle-ForeColor="White"
                            BorderStyle="Solid" border="0" OnRowCommand="gvResults_RowCommand" OnPageIndexChanging="gvResults_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="HCB Ref. No." ItemStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkHCBRef" runat="server" BorderStyle="None" Text='<%# Eval("RefNo") %>'
                                            CommandArgument='<%# Eval("HCBReference") %>' CommandName="HCBRef" Style="text-decoration: none">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Client Name" DataField="ClientName" ItemStyle-Width="25%" />
                                <asp:BoundField HeaderText="DOB" DataField="ClientDOB" ItemStyle-Width="15%" />
                                <asp:BoundField HeaderText="Claim Assessor" DataField="ClaimHandlerName" ItemStyle-Width="20%" />
                                <asp:BoundField HeaderText="Manager" DataField="NurseName" ItemStyle-Width="20%" />
                            </Columns>
                            <AlternatingRowStyle BackColor="#F5FDF5" />
                            <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center" />
                            <FooterStyle CssClass="GridFooterStyle" />
                            <RowStyle CssClass="GridViewRowStyle" HorizontalAlign="Center" />
                            <EditRowStyle CssClass="GridViewEditRowStyle" />
                            <AlternatingRowStyle CssClass="GridAlternateRowStyle" />
                            <SelectedRowStyle CssClass="GridSelectedeRowStyle" />
                            <PagerStyle CssClass="GridViewPagerStyle" />
                            <EmptyDataTemplate>
                                <asp:Label ID="lblemp" runat="server" CssClass="pageheader"> No records Found </asp:Label>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <br />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
