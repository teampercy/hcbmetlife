﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="MasterList.aspx.cs" Inherits="HCB.MasterList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var popupStatus = 0;
        var popupDivVal = '';
        function loadPopup(popupDiv) {
            //debugger;
            popupDivVal = popupDiv;

            //loads popup only if it is disabled

            if (popupStatus == 0) {
                centerPopup(popupDiv);
                $("#backgroundPopup1").css({
                    "opacity": "0.7"
                });

                $("#backgroundPopup1").fadeIn("slow");
                $(popupDiv).fadeIn("slow");
                popupStatus = 1;
            }
            return false;
        }
        //        $("#backgroundPopup1").click(function () {

        //            disablePopup(popupDivVal);
        //        });
        function disablePopup(popupdiv) {

            //disables popup only if it is enabled

            if (popupStatus == 1) {
                $("#backgroundPopup1").fadeOut("slow");
                $(popupdiv).fadeOut("slow");
                popupStatus = 0;
            }
        }
        function centerPopup(popupdiv) {
            //request data for centering

            var windowWidth = document.documentElement.clientWidth;
            var windowHeight = document.documentElement.clientHeight;
            var popupHeight = $(popupdiv).height();
            var popupWidth = $(popupdiv).width();
            //centering  
            $(popupdiv).css({
                "position": "absolute",
                "top": 200,
                // windowHeight / 2 - popupHeight / 2,
                "left": windowWidth / 2 - popupWidth / 2
            });
            //only need force for IE6  

            $("#backgroundPopup1").css({
                "height": windowHeight
            });
            return false;
        }
        function closepopup() {
            if (popupDivVal == '#Popup') {
                disablePopup('#Popup');
                document.getElementById('<%= txtName.ClientID %>').value = "";
                document.getElementById('<%= txtSchemeNumber.ClientID %>').value = "";
            }
            if (popupDivVal == '#AssessPopup') {
                disablePopup('#AssessPopup');
                document.getElementById('<%= txtAName.ClientID %>').value = "";
                document.getElementById('<%= txtAEmail.ClientID %>').value = "";
                document.getElementById('<%= txtATeleNum.ClientID %>').value = "";
            }
        }
    </script>
    <script type="text/javascript">

        function PopupAddNew() {
            debugger;
            var mySessionVar = '<%= Session["Type"] %>';
            if (mySessionVar != null) {

                if (mySessionVar == 8) {
                    loadPopup('#AssessPopup');
                }                
                else {
                    
                    loadPopup('#Popup');
                }
            }
            return false;
        }
        function ConfirmDelete(str) {
            var v = confirm(' You sure want to delete ' + str + ' ?');
            alert(v);
            return false;
        }
    </script>
    <style type="text/css">
        #backgroundPopup1
        {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6*/
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: #000000;
            border: 1px solid #cecece;
            z-index: 4;
        }
        .popupContact
        {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6 height: 200px;*/
            width: 550px;
            background: #FFFFFF;
            border: 2px solid #cecece;
            z-index: 5;
            font-size: 13px;
        }
        .popupContact tr
        {
            height: 40px;
            text-align: right;
            vertical-align: text-top;
        }
        .popupContactClose
        {
            font-size: 14px;
            line-height: 14px;
            right: 6px;
            top: 4px;
            position: absolute;
            color: #6fa5fd;
            font-weight: 700;
            display: block;
        }
        
        #overlay
        {
            position: fixed;
            z-index: 9 !important;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=80);
            opacity: 0.80;
            -moz-opacity: 0.80;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="scManager" runat="server">
    </asp:ScriptManager>
    <div id="backgroundPopup1" onclick="closepopup()">
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 80%; margin: 0 auto;"
            class="MainText" id="tableMain">
            <tr>
                <td align="center">
                    <asp:Label CssClass="PageTitle" ID="lblTitle" runat="server"></asp:Label>
                    <%--<><b>Maintain Reason Cancelled</b></font>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton Style="float: right; padding: 0px 50px 0px 0px;" ID="lnkAdd" runat="server"
                        OnClientClick="return  PopupAddNew();"> Add New</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upGVList" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvReasonList" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                                Width="95%" OnRowCreated="gvReasonList_RowCreated1" OnRowDataBound="gvReasonList_RowDataBound">
                                <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" />
                                <Columns>
                                    <asp:BoundField HeaderText="Name" DataField="Name" />
                                     <asp:BoundField HeaderText="Scheme Number" DataField="RelatedValue" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkeEdit" runat="server" CommandName="edt" OnCommand="lnk_Command"
                                                CommandArgument='<%# string.Concat(Eval("ID"),",",Eval("Name"),",",Eval("RelatedValue")) %>'> Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--OnCommand="lnk_Command"  onrowcommand="gvReasonList_RowCommand"--%>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton Visible="true" ID="lnkeDelete" runat="server" CommandArgument='<% #Eval("ID") %>'
                                                OnClientClick="return confirm('You want to delete this record ?');" OnCommand="lnk_Command"
                                                CommandName="del" ImageUrl="Images/Del.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="hdntype" Value="" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="Gvassessor" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                                Width="95%" OnRowCreated="gvReasonList_RowCreated1">
                                <HeaderStyle BorderStyle="solid" ForeColor="black" CssClass="gridhead1" />
                                <Columns>
                                    <asp:BoundField HeaderText="Name" DataField="Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkeEdit1" runat="server" CommandName="edt" OnCommand="lnk_Command"
                                                CommandArgument='<%# string.Concat(Eval("ID"),",",Eval("Name"),",",Eval("TeamEmailID"),",",Eval("TeamPhoneNo")) %>'> Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--OnCommand="lnk_Command"  onrowcommand="gvReasonList_RowCommand"--%>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("ID") %>'
                                                OnClientClick="return confirm('You want to delete this record ?');" OnCommand="lnk_Command"
                                                CommandName="del" ImageUrl="Images/Del.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" Value="" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divFee" runat="server" style="display:none; text-align:center">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align:center" colspan="2">
                                    <asp:Label ID="lblFeeTitle" runat="server" Text="Fees Structure" Font-Bold="true" 
                                    Font-Names="Arial" Font-Size="11pt" ForeColor="Black">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:50%">
                                    <asp:Label ID="lblHomeVisit" runat="server" Text="Home Visit Fee: " Font-Names="Arial" Font-Size="10pt">
                                    </asp:Label>
                                </td>
                                <td style="text-align:left; width:50%; padding-left:1%">
                                    <asp:TextBox ID="txtHomeVisit" runat="server" Width="20%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:50%">
                                    <asp:Label ID="lblCall1Fee" runat="server" Text="Call1 Fee: " Font-Names="Arial" Font-Size="10pt">
                                    </asp:Label>
                                </td>
                                <td style="text-align:left; width:50%; padding-left:1%">
                                    <asp:TextBox ID="txtCall1Fee" runat="server" Width="20%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:50%">
                                    <asp:Label ID="lblCall2Fee" runat="server" Text="Call2 Fee: " Font-Names="Arial" Font-Size="10pt">
                                    </asp:Label>
                                </td>
                                <td style="text-align:left; width:50%; padding-left:1%">
                                    <asp:TextBox ID="txtCall2Fee" runat="server" Width="20%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:50%">
                                    <asp:Label ID="lblCall3Fee" runat="server" Text="Call3 Fee: " Font-Names="Arial" Font-Size="10pt">
                                    </asp:Label>
                                </td>
                                <td style="text-align:left; width:50%; padding-left:1%">
                                    <asp:TextBox ID="txtCall3Fee" runat="server" Width="20%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:50%">
                                    <asp:Label ID="lblCallAborted" runat="server" Text="Aborted Call Fee: " Font-Names="Arial" Font-Size="10pt">
                                    </asp:Label>
                                </td>
                                <td style="text-align:left; width:50%; padding-left:1%">
                                    <asp:TextBox ID="txtCallAborted" runat="server" Width="20%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblMessage" runat="server" Font-Names="Arial" Font-Size="10pt" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center" colspan="2">
                                    <asp:Button ID="btnSaveFee" runat="server" Text="Save" OnClick="btnSaveFee_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <div id="Popup" class="popupContact" style="height: 210px; text-align:center">
                    <table style="height: 100px; width: 100%;">
                        <thead style="background-color: Gray; color: White;">
                            <tr>
                                <th colspan="2" style="text-align: center; vertical-align:middle">
                                    <asp:Label ID="lblhead" runat="server"></asp:Label>
                                </th>
                            </tr>
                        </thead>
                        <tr>
                            <td colspan="2" align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" CssClass="ErrorMessage" ValidationGroup="validateATxt"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 200px;">
                                <asp:Label ID="lblvalue" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="text-align:left">
                                <asp:TextBox ID="txtName" ValidationGroup="validateTxt" Width="250px" MaxLength="30"
                                    runat="server"></asp:TextBox>
                                    <br />
                                <asp:RequiredFieldValidator ID="rqftxtName" runat="server" ControlToValidate="txtName"
                                    ErrorMessage="can not be blank" ValidationGroup="validateTxt"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExName" runat="server" ValidationGroup="validateTxt"
                                ValidationExpression="^[a-zA-Z0-9\s.\-_']+$" ControlToValidate="txtName" 
                                ErrorMessage="Please enter valid data."></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                          <tr id="trSchemeNumber" runat ="server" >
                            <td style="width: 200px;">
                                <asp:Label ID="lblSchemeNumber" Text="Scheme Number" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="text-align:left">
                                <asp:TextBox ID="txtSchemeNumber" ValidationGroup="validateTxt" Width="250px" MaxLength="30"
                                    runat="server"></asp:TextBox>
                                    <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSchemeNumber"
                                    ErrorMessage="can not be blank" ValidationGroup="validateTxt"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="validateTxt"
                                ValidationExpression="^[a-zA-Z0-9\s.\-_']+$" ControlToValidate="txtSchemeNumber" 
                                ErrorMessage="Please enter valid data."></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <asp:Button runat="server" ID="btnSave" ValidationGroup="validateTxt" Text="Save"
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btncancle" runat="server" Text="Cancel" OnClick="btn_Cancle" OnClientClick="return closepopup();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updAssess" runat="server" >
            <ContentTemplate>
                <div id="AssessPopup" class="popupContact">
                    <table style="vertical-align: middle;" width="100%">
                        <tr style="background-color: Gray; color: White;">
                            <th colspan="2" style="text-align: center; vertical-align:middle">
                                Assessor Detail
                            </th>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" >
                                <asp:ValidationSummary ID="validateUserSummary" CssClass="ErrorMessage" ValidationGroup="validateATxt"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 200px;">
                                Assessor Name:
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtAName" ValidationGroup="validateATxt" runat="server" MaxLength="30"
                                    Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAName"
                                    Display="Dynamic" CssClass="ErrorMessage" ErrorMessage="Assessor name required"
                                    ValidationGroup="validateATxt"><img src='../images/Exclamation.jpg'> </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email ID:
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtAEmail" ValidationGroup="validateATxt" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqftxtAEmail" runat="server" ControlToValidate="txtAEmail"
                                    Display="Dynamic" CssClass="ErrorMessage" ErrorMessage="Email Required" ValidationGroup="validateATxt"><img src='../images/Exclamation.jpg'> </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RGEtxtUserName" ControlToValidate="txtAEmail"
                                    Display="Dynamic" CssClass="ErrorMessage" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="validateATxt" runat="server" ErrorMessage="Invalid Email address"><img src='../images/Exclamation.jpg'> 
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Telephone Number:
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtATeleNum" runat="server" MaxLength="12" ValidationGroup="validateATxt"
                                    onkeypress="return CheckValues(event.keyCode, event.which,this);" CssClass="Textbox"
                                    Width="150px">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtATeleNum" runat="server" ControlToValidate="txtATeleNum"
                                    Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateATxt" ErrorMessage="TelePhone No. Required">
                           <img src='../images/Exclamation.jpg'>     
                                </asp:RequiredFieldValidator>
                                <%--<asp:RegularExpressionValidator ID="RegtxtTeleNum" ControlToValidate="txtATeleNum"
                                    Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateATxt" runat="server"
                                    ErrorMessage="Invalid Formet Required Formet '123-123-1234'" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">
                                   <img src='../images/Exclamation.jpg'> 
                                </asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <asp:Button runat="server" ID="Button1" ValidationGroup="validateATxt" Text="Save"
                                    CommandName="assessor" OnClick="btnSave_Click" />
                                <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="btn_Cancle" OnClientClick="return closepopup();" />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script type="text/javascript">
           
        </script>
    </div>
</asp:Content>
