﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ServiceReqList.aspx.cs" Inherits="HCB.ServiceReqList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <font color="black" face="Arial" size="3"><b>Maintain Service Required</b></font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:ListBox ID="lstService" runat="server" SelectionMode="Single" Width="30%" Height="140px">
                            </asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width:48%; text-align:right">
                            <font color="black" face="Arial" size="2">Service Name:</font> 
                        </td>
                        <td style="text-align:left">
                            &nbsp;&nbsp;<asp:TextBox ID="txtServiceName" runat="server" CssClass="Textbox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnCreate" runat="server" Text="Create" CssClass="ButtonClass" />
                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="ButtonClass" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="ButtonClass" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>
