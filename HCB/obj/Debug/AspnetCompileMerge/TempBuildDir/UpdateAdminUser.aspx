﻿<%@ Page Title="Manage Users" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UpdateAdminUser.aspx.cs" Inherits="HCB.UpdateAdminUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="80%" align="center">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center">
                                <font color="black" face="Arial" size="3"><b>User Profile Maintenance</b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" Font-Names="Arial" Font-Size="10pt"
                        ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr style="width: 60%">
                <td style="text-align: right">
                    <table border="0" cellpadding="0" cellspacing="0" width="60%" align="center">
                        <tr>
                            <td align="left">
                                <asp:ValidationSummary ID="validateUserSummary" CssClass="ErrorMessage" DisplayMode="BulletList"
                                    ValidationGroup="validateUser" HeaderText="Summary" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="3" cellspacing="0" align="center">
                        <tr>
                            <td colspan="2" align="left">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 50%">
                                <font color="black" face="Arial" size="2">User Name(Email Address):</font>
                            </td>
                            <td style="text-align: left; width: 50%">
                                <asp:TextBox ID="txtUserName" runat="server" CssClass="Textbox" Width="170px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtUserName" runat="server" ControlToValidate="txtUserName"
                                    CssClass="ErrorMessage" ValidationGroup="validateUser" Display="Dynamic" ToolTip="User Email Required"
                                    ErrorMessage="User Email Required">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RGEtxtUserName" ControlToValidate="txtUserName"
                                    CssClass="ErrorMessage" ValidationGroup="validateUser" runat="server" ErrorMessage="Invalid Email address"
                                    ToolTip="Invalid Email address" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"> 
                                     <img src='images/Exclamation.jpg'></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Name:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtName" runat="server" CssClass="Textbox" Width="170px">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtName" runat="server" ControlToValidate="txtName"
                                    CssClass="ErrorMessage" ValidationGroup="validateUser" Display="Dynamic" ToolTip="Name Required"
                                    ErrorMessage="Name Required">
                                    <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="nameRegex" runat="server" ControlToValidate="txtName"
                                        ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid Name"
                                        ValidationGroup="validateUser">
                                    </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Password:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="Textbox"
                                    Width="170px">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtPassword" runat="server" ControlToValidate="txtPassword"
                                    CssClass="ErrorMessage" ValidationGroup="validateUser" Display="Dynamic" ErrorMessage="Password is Required"
                                    ToolTip="Password is Required">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="REGtxtPassword" Display="Dynamic" ValidationExpression="(?=^.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_\d])(?=^.*[^\s].*$).*$"
                                    runat="server" ControlToValidate="txtPassword" ValidationGroup="validateUser"
                                    ErrorMessage="Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character."
                                    ToolTip="Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character.">
                                                         <img src='images/Exclamation.jpg' alt="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character. ">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Address:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="Textbox" TextMode="MultiLine"
                                    Height="109px" Width="170px">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtAddress" runat="server" ControlToValidate="txtAddress"
                                    CssClass="ErrorMessage" ValidationGroup="validateUser" Display="Dynamic" ToolTip="Address Required"
                                    ErrorMessage="Address Required">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAddress"
                                    ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$" ErrorMessage="Invalid Address"
                                    ValidationGroup="validateUser"></asp:RegularExpressionValidator>
                                 <%--   ^[a-zA-Z'.|\n\s' ']{1,1000}$--^[a-zA-Z0-9'(.\n\s)]{1,1000}$--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Telephone Number:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtTeleNum" runat="server" MaxLength="12" onkeypress="return CheckValues(event.keyCode, event.which,this);"
                                    CssClass="Textbox" Width="170px">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RQFtxtTeleNum" runat="server" ControlToValidate="txtTeleNum"
                                    CssClass="ErrorMessage" ValidationGroup="validateUser" Display="Dynamic" ToolTip="TelePhone No. Required"
                                    ErrorMessage="TelePhone No. Required">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTeleNum"
                                    ForeColor="Red" ValidationExpression="^[0-9]+$" ErrorMessage="Invalid Telephone Number"
                                    ValidationGroup="validateUser"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Account Status:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlAcctStatus" runat="server" Width="180px">
                                    <asp:ListItem Value="0">-- Select Account Status --</asp:ListItem>
                                    <asp:ListItem Value="1">Enabled</asp:ListItem>
                                    <asp:ListItem Value="2">Disabled</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RQFddlAcctStatus" runat="server" ControlToValidate="ddlAcctStatus"
                                    InitialValue="0" CssClass="ErrorMessage" ValidationGroup="validateUser" Display="Dynamic"
                                    ToolTip="Select Account Status" ErrorMessage="Select Account Status">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">User Type:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlUserType" runat="server" Width="175px">
                                    <asp:ListItem Value="0">-- Select User Type --</asp:ListItem>
                                    <asp:ListItem Value="1">Inco</asp:ListItem>
                                    <asp:ListItem Value="2">Admin</asp:ListItem>
                                    <asp:ListItem Value="3">Nurse</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RQFusertypw" runat="server" ControlToValidate="ddlUserType"
                                    InitialValue="0" CssClass="ErrorMessage" ValidationGroup="validateUser" Display="Dynamic"
                                    ToolTip="Select user type" ErrorMessage="Select user type">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Insurance Company Name:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlInsCompName" runat="server" Width="198px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Nominated Nurse:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlNominatedNurse" runat="server" Width="175px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trLoadUser" runat="server">
                            <td style="text-align: left">
                                <font color="black" face="Arial" size="2">Load User:</font>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlLoadUser" runat="server" Width="175px" AutoPostBack="true" CausesValidation="true"
                                    OnSelectedIndexChanged="ddlLoadUser_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" />
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete this User?');"
                        OnClick="btnDelete_Click" />
                    <asp:Button ID="btnSave" runat="server" ValidationGroup="validateUser" Text="Save"
                        OnClick="btnSave_Click" />
                    <asp:HiddenField ID="hdnUserId" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
