﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ViewClientRecords.aspx.cs" Inherits="HCB.ViewClientRecords" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link id="Link1" rel="Stylesheet" href="Styles/Slate/plugin.css" type="text/css"
        media="screen" title="slate1" runat="server" />
    <script type="text/javascript" src="Scripts/Slate/jquery.1.4.2.min.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.portlet.js"></script>
    <script type="text/javascript" src="Scripts/Slate/plugin.js"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            slate.init();
            slate.portlet.init();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function CallClientDet() {

        }

    </script>
    <div id="content" class="xfluid">
        <div class="portlet x12">
            <div class="portlet-header">
                <asp:Label CssClass="PageTitle" ID="lblTitle" runat="server"></asp:Label></div>
            <br />
            <div class="portlet-content">
                <table cellpadding="0" border="2" cellspacing="0" class="display" rel="datatable"
                    id="example">
                    <thead style="height: 30px;">
                        <tr style="background-color: #2459A7 !important; color: white;">
                            <th>
                                Ref No
                            </th>
                            <th>
                                Client Name
                            </th>
                            <th>
                                Date Of Birth
                            </th>
                            <th>
                                Claim Assessor
                            </th>
                            <th>
                                Manager
                            </th>
                        </tr>
                    </thead>
                    <a name="plugin"></a>
                    <tbody>
                        <asp:Repeater ID="rptResults" runat="server">
                            <ItemTemplate>
                                <tr class="odd gradeX">
                                    <td>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBReference") %>'
                                            OnCommand="lnk_Command" CommandName="HCBRef" />
                                    </td>
                                    <td>
                                        <%# Eval("ClientName")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClientDOB")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClaimHandlerName")%>
                                    </td>
                                    <td>
                                        <%# Eval("NurseName")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="even gradeC">
                                    <td>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBReference") %>'
                                            OnCommand="lnk_Command" CommandName="HCBRef" />
                                    </td>
                                    <td>
                                        <%# Eval("ClientName")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClientDOB")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClaimHandlerName")%>
                                    </td>
                                    <td>
                                        <%# Eval("NurseName")%>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <div id="wassup" runat="server">
                </div>
            </div>
        </div>
    </div>
    <%--<table>
        <tr>
            <td colspan="3" style="height: 301px" valign="top">
                <div style="text-align: left">
                    <div align="center">
                        <br />
                        <asp:Label ID="Label1" runat="server" CssClass="pageheader" Text="View Client Records"></asp:Label></div>
                    <div align="left">
                        <br />
                       <%-- <asp:GridView ID="gvResults" CssClass="GridView" runat="server" GridLines="Both"
                            AutoGenerateColumns="false" AllowSorting="True" BorderColor="silver" BorderStyle="Solid"
                            border="0" OnRowCommand="gvResults_RowCommand" PageSize="3" AllowPaging="true"
                            OnPageIndexChanging="gvResults_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="HCB Reference" ItemStyle-Width="13%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkHCBRef" runat="server" BorderStyle="None" Text='<%# Eval("RefNo") %>'
                                            CommandArgument='<%# Eval("HCBReference") %>' CommandName="HCBRef" Style="text-decoration: none">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Client Name" DataField="ClientName" ItemStyle-Width="35%" />
                                <asp:BoundField HeaderText="DOB" DataField="ClientDOB" />
                                <asp:BoundField HeaderText="Claim Assessor" DataField="ClaimHandlerName" ItemStyle-Width="20%" />
                                <asp:BoundField HeaderText="Manager" DataField="NurseName" ItemStyle-Width="20%" />
                            </Columns>
                            <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center" />
                            <FooterStyle CssClass="GridFooterStyle" />
                            <RowStyle CssClass="GridViewRowStyle" />
                            <EditRowStyle CssClass="GridViewEditRowStyle" />
                            <AlternatingRowStyle CssClass="GridAlternateRowStyle" />
                            <SelectedRowStyle CssClass="GridSelectedeRowStyle" />
                            <PagerStyle CssClass="GridViewPagerStyle" />
                            <EmptyDataTemplate>
                                <asp:Label ID="lblemp" runat="server" CssClass="pageheader"> No records Found </asp:Label>
                            </EmptyDataTemplate> 
                    </div>
                </div>
                <br />
            </td>
        </tr>
    </table>--%>
</asp:Content>
