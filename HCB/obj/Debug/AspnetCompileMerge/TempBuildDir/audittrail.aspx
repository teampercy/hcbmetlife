﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="audittrail.aspx.cs" Inherits="HCB.audittrail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
<link rel="Stylesheet" href="Styles/Slate/plugin.css" type="text/css" media="screen" title="slate1"  runat="server"/>
<style type="text/css">
 
/* tr.odd {
	background-color: #E2E4FF;
}

tr.even {
	background-color: white;
}
thead
{
    background-color : Gray;
    color:White;
} */
 
</style>
  <script type="text/javascript" src="Scripts/Slate/jquery.1.4.2.min.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.portlet.js"></script>
    <script type="text/javascript" src="Scripts/Slate/plugin.js"></script>
  
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            slate.init();
            slate.portlet.init();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="content" class="xfluid">
        <div class="portlet x12">
         <div class="portlet-header"><asp:Label CssClass="PageTitle" ID="lblTitle" runat="server">Audit Trail</asp:Label></div>
          <br />
          
        <div class="portlet-content">
            <a name="plugin"></a>
            <table cellpadding="0" border="2" cellspacing="0" class="display" rel="datatable"
                id="example"> 
                <thead style="height:30px;">
                   <tr   style="background-color:#2459A7 !important; color:white;">
                        <th>
                            Ref No
                        </th>
                        <th>
                            Client Name
                        </th>
                        <th>
                            DateTime
                        </th>
                        <th>
                            Operation Type
                        </th>
                     </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptQuotes" runat="server">
                        <ItemTemplate>
                            <tr class="odd gradeX">
                                <td>
                                    <asp:LinkButton runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBRefNo") %>'
                                        OnCommand="lnk_Command" />
                                </td>
                                <td>
                                    <%# Eval("FullName")%>
                                </td>
                                <td>
                                    <%# Eval("TimeAccessed")%>
                                </td>
                                <td>
                                    <%# Eval("AmdType")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="even gradeC">
                                <td>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBRefNo") %>'
                                        OnCommand="lnk_Command" />
                                </td>
                                <td>
                                    <%# Eval("FullName")%>
                                </td>
                                <td>
                                    <%# Eval("TimeAccessed")%>
                                </td>
                                <td>
                                    <%# Eval("AmdType")%>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
        </div>
    </div>
  
</asp:Content>
