﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Accessdenied.aspx.cs" Inherits="HCB.Accessdenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            <img src="Images/LadyImage.jpg" vspace="15" style="border: 1px lightgrey solid;" />
        </div>
        <div class="col-md-9">
            <label class="control-label bold margin-top-15 font-lg font-red">You don't have enough rights to access this page.</label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <img src="Images/MeetingImage.jpg" vspace="15" style="border: 1px lightgrey solid;" />
        </div>
        <div class="col-md-9"></div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <img src="Images/HandshakeImage.jpg" vspace="15" style="border: 1px lightgrey solid;" />
        </div>
        <div class="col-md-9"></div>
    </div>


    <%--<div style="text-align: center; width:80%">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/LadyImage.jpg" vspace="15" style="border: 1px lightgrey solid;" />
                </td>
                <td class="MainText" style="width: 535px;" rowspan="3" valign="top">
                    <br />
                    <br />
                    <span style="font-size: 12pt"><strong> 
                        You don't have enough rights to access this page.</strong></span><br />
                    <br />
                  
                </td>
            </tr>
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/MeetingImage.jpg" style="border: 1px lightgrey solid;" />
                </td>
            </tr>
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/HandshakeImage.jpg" style="border: 1px lightgrey solid;" />
                </td>
            </tr>
        </table>
    </div>--%>
</asp:Content>
