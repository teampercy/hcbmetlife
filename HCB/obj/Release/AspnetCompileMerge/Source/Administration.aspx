﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Administration.aspx.cs" Inherits="HCB.Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta http-equiv="Cache-Control" content="no-store" />
    <meta http-equiv="Pragma" content="no-cache" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- BEGIN PAGE HEAD-->
    <%--<div class="page-head">
        <div class="container bg-grey">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1 class="font-blue">ADMINISTRATIVE TOOLS</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>--%>
    <!-- END PAGE HEAD-->
    <%--<br />--%>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="portlet light portlet-fit ">
                <%--<div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                <span class="caption-subject font-green bold uppercase">Simple - Extended 1</span>
                <div class="caption-desc font-grey-cascade">Default list element style. Activate by adding
                    <pre class="mt-code">.list-simple ext-1</pre>
                    class to the
                    <pre class="mt-code">ul</pre>
                    element. </div>
                    </div>
                </div>--%>
                <div class="portlet-body">
                    <div class="mt-element-list">
                        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp" runat="server">
                            <div class="list-head-title-container">
                                <%--<div class="list-date">Nov 8, 2015</div>--%>
                                <%--<a href="javascript:;">Concept Proof</a>--%>
                                <h3 class="list-title font-lg font-white">Administrative Tools</h3>
                            </div>
                        </div>
                        <div class="mt-list-container list-simple ext-1 text-left">
                            <ul>
                                <li class="mt-list-item done" runat="server" id="trUserPro">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="UpdateAdminUser.aspx?id=1" style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">User Profiles</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trSerReg">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ServiceRequired"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Service Required List</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trIllInj">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "IllnessInjury"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Illness or Injury</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trClaimClosed">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ClaimClosed"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Claim Closed Reason</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trReasonClosed">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ReasonClosed"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Reason Closed</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trBroker">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "Broker"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Broker</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trFeeCharge">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "feecharged"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Fee Charged</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trCorPartner">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "CorporatePartner"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Corporate Partner</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trProdType">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "ProductType"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Product Type</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trWaitPeriod">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "WaitingPeriod"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Waiting Period</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trEmpStatus">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "EmploymentStatus"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Employment Status</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trBrand">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "Brand"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Brand</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trIncapacity">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "IncapacityDefination"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Incapacity Defination</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trTypeVisit">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "TypeOfVisit"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Type Of Visit</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trTypeCall">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "TypeOfCalls"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Type Of Calls</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trRehabType">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "FundedTreatmentType"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Funded Treatment Type</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trRehabPro">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "FundedTreatmentProvider"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Funded Treatment Provider</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trSchemeName">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href='<%= Page.GetRouteUrl("Admin",new { page = "Admin" , type = "SchemeName"}) %>'
                                                style="font-family: Arial; color: Black; font-size: 13px; text-decoration: none">Scheme Name</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trManageMt">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="MetLifeStaticValues.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">Manage MetLife Constants</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trManInfo">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="GenerateReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">Management Information </a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trAuditTrail">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="audittrail.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">Audit Trail</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trRTWDuration">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="RTWDuration.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">Average RTW Duration Reports</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trMIFullDataReport">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="FullDataReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">MI Full Data Report</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trSLAExceptionReport">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="SLAExceptionReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">SLA Exception Report</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trManagementInfoReport">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="MetlifeManagementInformationReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">Metlife Management Information Report</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trMIChargeReport">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="MonthlyReport.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">MI Charge Reports</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trAverage">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="MIAverageTimeSummary.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">MI Charge Average Time Detail</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trDetail">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="MIAverageTimeDetail.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">MI Charge Average Time Summary</a>
                                        </h3>
                                    </div>
                                </li>

                                <li class="mt-list-item done" runat="server" id="trIncoUser">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="IncoUsers.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">Inco Users</a>
                                        </h3>
                                    </div>
                                </li>

                                <%--<li class="mt-list-item done" runat="server" id="trReferralForm">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <a href="ReferralFormList.aspx" style="font-family: Arial; color: Blue; font-size: 13px;">Referral Form</a>
                                        </h3>
                                    </div>
                                </li>--%>

                                <%-- <li class="mt-list-item done" runat="server" id="trOnlineUser">
                                    <div class="list-icon-container done">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="list-item-content">
                                        <h3 class="uppercase">
                                            <asp:Label Text="" ID="lblOnlineUsers" runat="server" />
                                        </h3>
                                    </div>
                                </li>--%>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3"></div>

        <div runat="server" id="trOnlineUser" class="row">
            <div class="col-lg-4 text-left">
                <asp:Label Text="" ID="lblOnlineUsers" runat="server" CssClass="font-md" />
            </div>
        </div>
    </div>
    
</asp:Content>
