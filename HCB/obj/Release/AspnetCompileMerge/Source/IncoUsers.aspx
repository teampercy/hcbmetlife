﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncoUsers.aspx.cs" MasterPageFile="~/Site.Master" Inherits="HCB.IncoUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function Validate() {

            if (document.getElementById('<%=ddlExprotTo.ClientID%>').value == "0") {
                alert('Please select type to Export:-PDF or EXCEL');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server">Inco Users</asp:Label>
            </div>
        </div>

        <div class="portlet-body">
            <div class="portlet-content font-md">
                <div class="form-group">
                    <label class="control-label font-md">Export to</label>
                    <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown" CssClass="form-control input-md input-inline input-circle">
                        <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                        <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo"
                        ValidationGroup="valdtdown"></asp:RequiredFieldValidator>
                    <asp:Button ID="lnkExport" ValidationGroup="valdtdown" runat="server" Text="Export" CssClass="btn btn-circle blue bold"
                        OnClick="lnkExport_Click" OnClientClick="return Validate();"></asp:Button>
                </div>

                <div class="form-group">
                    <asp:Label ID="lblerr" runat="server" CssClass="font-red bold"></asp:Label>
                </div>

                <div id="divReport" runat="server">
                    <div id="wassup">

                    </div>
                </div>
            </div>
        </div>
    </div>
    
</asp:Content>
