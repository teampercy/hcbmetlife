﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RTWDuration.aspx.cs" Inherits="HCB.RTWDuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //function pageLoad(sender, args) {
        $(document).ready(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
        //}
    </script>

    <style type="text/css">
        .dataTables_length, .dataTables_filter, .dataTables_info, .pagination {
            display: none;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server">Monthly Average RTW Duration Reports</asp:Label>
            </div>
        </div>

        <div class="portlet-body form">
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnOpenAll" CssClass="btn form-control input-medium"
                                    Style="border: 1px solid #c2cad8;" Text="ALL Cases" OnClick="btnOpenAll_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Scheme Name</label>
                                <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="ddlRTWDurationScheme" CssClass="form-control input-medium"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlRTWDurationScheme_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Broker</label>
                                <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="ddlRTWDurationBroker" CssClass="form-control input-medium"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlRTWDurationBroker_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Date Range</label>
                                <div class="col-md-6">
                                    <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                        <asp:TextBox runat="server" CssClass="form-control" name="from" ID="txtRTWDurationFromDate"></asp:TextBox>
                                        <span class="input-group-addon">to </span>
                                        <asp:TextBox runat="server" CssClass="form-control" name="to" ID="txtRTWDurationToDate"></asp:TextBox>
                                    </div>
                                    <!-- /input-group -->
                                    <span class="help-block">Select date range </span>
                                </div>
                                <div class="col-md-1">
                                    <asp:LinkButton runat="server" ID="btnRTWDurationDateRange" CssClass="btn green" OnClick="btnRTWDurationDateRange_Click">
                                            <i class="fa fa-check-circle"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblerr" runat="server" CssClass="font-red bold"></asp:Label>
                    </div>

                    <div id="divReport" runat="server">
                    </div>

                </div>
            </div>
        </div>



    </div>
</asp:Content>


