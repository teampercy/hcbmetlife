﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SLAExceptionReport.aspx.cs" Inherits="HCB.SLAExceptionReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });

        function openReportDetail(val) {            
            val = val.substr(3);            
            $.ajax({
                url: "MonthlyReport.aspx/SetUrl",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ 'val': val }),
                success: function () {
                    //window.open("ClientRecord.aspx?HcbReportId=" + val, '_blank'); //To open window in New Tab
                    window.open("ClientRecord.aspx", '_self');    //To open window in Self Tab        
                }
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server">SLA Exception Report</asp:Label>
            </div>
        </div>

        <div class="portlet-body form">
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Date Range</label>
                                <div class="col-md-6">
                                    <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                        <asp:TextBox runat="server" CssClass="form-control" name="from" ID="txtSLAFromDate"></asp:TextBox>
                                        <span class="input-group-addon">to </span>
                                        <asp:TextBox runat="server" CssClass="form-control" name="to" ID="txtSLAToDate"></asp:TextBox>
                                    </div>
                                    <!-- /input-group -->
                                    <span class="help-block">Select date range </span>
                                </div>
                                <div class="col-md-1">
                                    <asp:LinkButton runat="server" ID="btnSLADateRange" CssClass="btn green" OnClick="btnSLADateRange_Click">
                                            <i class="fa fa-check-circle"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-4">Export to</label>
                                <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown" CssClass="form-control input-md input-inline">
                                    <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo"
                                    ValidationGroup="valdtdown"></asp:RequiredFieldValidator>
                                <asp:Button ID="lnkExport" ValidationGroup="valdtdown" runat="server" Text="Export" CssClass="btn blue bold"
                                    OnClick="lnkExport_Click"></asp:Button>

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblerr" runat="server" CssClass="font-red bold"></asp:Label>
                    </div>

                    <div id="divReport" runat="server">
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
