﻿function funOpenWin(vURL, vWinName, vHeight, vWidth, vScroll, vResize) {
    vLeftPosition = (screen.width) ? (screen.width - vWidth) / 2 : 100;
    vTopPosition = (screen.height) ? (screen.height - vHeight) / 2 : 100;
    vSettings = 'width=' + (vWidth) + ',height=' + (vHeight) + ',top=' + (vTopPosition) + ',left=' + (vLeftPosition) + ',scrollbars=' + vScroll + ',location=no,directories=no,status=yes,menubar=no,addressbars=no,toolbar=no,resizable=' + vResize;
    //window.open(urlencode(vURL),vWinName,vSettings);
    win = window.open(vURL, vWinName, vSettings);
    //if (parseInt(navigator.appVersion) >= 4) { win.window.focus(); }
}
function validate_integer(field) {
    var valid = "0123456789.+-"
    var ok = "yes";
    var temp;
    for (var i = 0; i < field.value.length; i++) {
        temp = "" + field.value.substring(i, i + 1)
        if (valid.indexOf(temp) == "-1") {
            ok = "no";
        }
    }
    if (ok == "no") {
        return false;
    }
    else {
        return true;
    }
}
function validate_positiveNo(field, label) {
    if (field.value < 0) {
        alert(label + " cannot be negative");
        field.focus();
        return false;
    }
    else {
        return true;
    }
}
function TrimString(stringS) {
    var stringD = ""
    var i, j

    // Check if stringS is already empty
    if (stringS.length == 0)
        return stringD

    // Trim the leading spaces.
    // i = first non-space char or EOF
    i = 0
    while (i < stringS.length) {
        if (stringS.charAt(i) != " ")
            break

        i++
    }

    // Trim the ending spaces
    // j = last non-space char or 
    j = stringS.length - 1
    while (j >= i) {
        if (stringS.charAt(j) != " ")
            break

        j--
    }

    // Copy non-space chars from stringS to stringD
    while (i <= j) {
        stringD += stringS.charAt(i)
        i++
    }

    return stringD
}

function validate_Number(field) {
    var valid = "0123456789"
    var ok = "yes";
    var temp;

    field.value = Trim(field.value)
    for (var i = 0; i < field.value.length; i++) {
        temp = "" + field.value.substring(i, i + 1)
        if (valid.indexOf(temp) == "-1") {
            ok = "no";
        }
    }
    if (ok == "no") {
        return false;
    }
    else {
        return true;
    }
}

function validate_DecimalNumber(field) {
    var valid = "0123456789."
    var ok = "yes";
    var temp;
    if (isNaN(field.value)) {
        ok = "no";
    }
    if (ok == "yes") {
        for (var i = 0; i < field.value.length; i++) {
            temp = "" + field.value.substring(i, i + 1)
            if (valid.indexOf(temp) == "-1") {
                ok = "no";
            }
        }
    }
    if (ok == "no") {
        return false;
    }
    else {
        return true;
    }
}


function validate_Username(field) {
    var valid = "0123456789_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" + chr(32)
    var ok = "yes";
    var temp;
    for (var i = 0; i < field.value.length; i++) {
        temp = "" + field.value.substring(i, i + 1)
        if (valid.indexOf(temp) == "-1") {
            ok = "no";
        }
    }
    if (ok == "no") {
        return false;
    }
    else {
        return true;
    }
}
function LTrim(str) {
    var whitespace = new String(" \t\n\r");

    var s = new String(str);

    if (whitespace.indexOf(s.charAt(0)) != -1) {
        var j = 0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
            j++;
        s = s.substring(j, i);
    }
    return s;
}
function RTrim(str) {
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length - 1)) != -1) {
        var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
            i--;
        s = s.substring(0, i + 1);
    }
    return s;
}
function Trim(str) {
    return RTrim(LTrim(str));
}





// function to validate Date in mm/dd/yyyy Format. 
// takes value to be validated.
// returns "ok" OR "empty" or Related Error Message

function validateDate(date) {
    var msg;
    var sValidChars;
    var nCounter;
    msg = "ok";
    sValidChars = "1234567890/";
    if (Trim(date) != "") {

        if (date.length != 10) {
            msg = " Please enter in exact format (dd/mm/yyyy) e.g. 02/01/2000";
            return msg;
        }



        for (nCounter = 0; nCounter < date.length; nCounter++) {

            if (sValidChars.indexOf(date.substring(nCounter, nCounter + 1)) == -1) {

                msg = " Please enter in exact format (xx/xx/xxxx)";
                nCounter = date.length;
                return msg;
            }

        }
        if (date.indexOf("/") != 2) {
            msg = "Invalid From date : Please enter in exact format (xx/xx/xxxx)";
            return msg;
        }

        if (date.indexOf("/", 3) != 5) {
            msg = "Invalid From date : Please enter in exact format (xx/xx/xxxx)";
            return msg;
        }

        if (date.indexOf("/", 6) < 10 && date.indexOf("/", 6) > 5) {
            msg = "Invalid From date : Please enter in exact format (xx/xx/xxxx)";
            return msg;
        }

        var month;
        var day;
        var year;
        month = parseInt(TrimNumber(date.substring(3, 5)));
        day = parseInt(TrimNumber(date.substring(0, 2)));
        year = parseInt(date.substring(6, 10));

        if (month > 12 || month < 1) {
            msg = " Invalid Month";
            return msg;
        }
        if (year < 1900 || year > 3000) {
            msg = " Invalid year. Should be in (1900-3000)";
            return msg;
        }
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            if (day > 31 || day < 1) {
                msg = " Days should not be more than 31 or less than 1 for entered Month";
                return msg;
            }
        }
        else {
            if (month == 2 && (year % 4) == 0) {
                if (day > 29 || day < 1) {
                    msg = " Days should not be more than 29 or less than 1 for entered Month";
                    return msg;
                }
            }
            else if (month == 2 && (year % 4) != 0) {
                if (day > 28 || day < 1) {
                    msg = " Days should not be more than 28 or less than 1 for entered Month";
                    return msg;
                }
            }
            else {
                if (day > 30 || day < 1) {
                    msg = " Days should not be more than 30 or less than 1 for entered Month";
                    return msg;
                }
            }
        }
        return msg;

    }
    msg = "empty";
    return msg;
}

// used in function validateDate()
function TrimNumber(number) {
    var temp = number.toString();
    if (temp.length == 2 && temp.charAt(0) == "0") {
        return temp.charAt(1);
    }
    else
        return number;
}
//-------------------------------------
//Trims Begining & Trailing space chars 
//-------------------------------------
function trim(strValue) {
    while (strValue.substring(0, 1) == " ")
        strValue = strValue.substring(1);
    while (strValue.substring((strValue.length - 1), strValue.length) == " ")
        strValue = strValue.substring(0, strValue.length - 1, strValue.length);
    return strValue;
}

function isEmpty(y, label, foc) {
    str = trim(y.value)
    newstr = ""
    retstr = ""
    strlen = str.length
    for (start = 0; start < strlen; start++) {
        if (str.substring(start, start + 1) != " ")
        { newstr = str.substring(start, strlen) }
    }

    strlen = newstr.length
    for (start = strlen; start > 0; start--) {
        if (newstr.substring(start - 1, start) != " ")
        { retstr = newstr.substring(0, start) }
    }
    if (retstr == "") {
        if (foc != 1) {
            alert(label + " is required")
            y.focus()
        }
        return true
    }
    else {
        return false
    }

}

function checkNumber(str, label) {


    theinput = str.value
    thelength = theinput.length
    for (i = 0; i < thelength; i++) {
        thechar = theinput.substring(i, i + 1)
        thechar = thechar.toUpperCase()
        if (thechar >= "A" && thechar <= "Z") {
            foc = 0;
            break;
        }
        else {
            foc = 1;
        }
    }

    if (foc == 0) {
        alert(label + " cannot contain Characters.")
        //str.value=""
        str.focus()
        return false
    }
    return true;
};

function rmspaces(x) {
    var leftx = 0;
    var rightx = x.length - 1;
    while (x.charAt(leftx) == ' ') leftx++;
    while (x.charAt(rightx) == ' ') --rightx;
    var q = x.substr(leftx, rightx - leftx + 1);
    if ((leftx == x.length) && (rightx == -1)) q = '';
    return (q)
};

function GetcheckedValue(argument, radioname) {
    var len, checkedvalue, i;
    if (argument.length) {
        len = argument.length;
        for (i = 0; i < len; i++) {
            e = argument[i];
            if (e.checked) {
                checkedvalue = e.value;
            }
        }
    }
    else {
        if (argument.checked) {
            checkedvalue = argument.value;
        }
        else {
            checkedvalue = '';
        }
    }
    return checkedvalue;
}


function isDataChecked(argument, recordcount, label) {
    flag = 0
    if (recordcount == 1) {
        if (!argument.checked) {
            alert("Please Select Something first")
            argument.focus()
            return true
        }
    }
    else {
        for (i = 0; i < recordcount; i++) {
            if (argument[i].checked) {
                flag = 1;
                break;
            }

        }
        if (flag != 1) {
            alert("Please Select a " + label + " first")
            argument[0].focus()
            return true
        }
        else {
            return false;
        }
    }
}

//for validating email
function checkemail(email, label) {
    var emailReg = "([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(([a-zA-Z0-9_-])*\.([a-zA-Z0-9_-])+)+";
    var regex = new RegExp(emailReg);
    src = Trim(email.value)
    if (!regex.test(src)) {
        alert("Please Enter a valid Email Id")
        email.select();
        return true
    }
    else {
        return false
    }

}



// for checking whether all combobox are empty or not
function isEmptycombo(str, label, foc) {
    //var data=str.value
    var data = str.options[str.options.selectedIndex].value
    if ((data == "") || (data == "-1") || (data == "0")) {
        if (foc != 1) {
            alert("Please select " + label + " from the list ")
            str.focus()
        }
        return true
    }
    else {
        return false
    }

};

function isNotProperPhoneNo(ele, label) {
    var msg
    msg = ''
    if (!isEmpty(ele, label)) {

        msg = validate_phone(ele.value)
        if (msg != "ok") {
            alert("Enter correct " + label + " in format shown (xxx-xxx-xxxx)");
            ele.focus();
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return true;
    }

}

function checkFloatNumber(ele, label) {
    theinput = ele.value;
    if (theinput != "") {
        if (isNaN(theinput)) {
            alert("\"" + label + "\"" + " is a invalid entry.");
            ele.select();
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
}

function checkSignedNumber(sign, ele, label) {
    if (!isEmpty(ele, label)) {
        if (checkFloatNumber(ele, label)) {
            if (Trim(sign) == "+") {
                if ((parseFloat(ele.value)) >= 0) {
                    return true;
                }
                else {
                    alert(label + " cannot be negative value")
                    ele.select();
                    return false;
                }
            }
            else if (Trim(sign) == "-") {
                if ((parseFloat(ele.value)) < 0) {
                    return true;
                }
                else {
                    alert(label + " cannot be postive value")
                    ele.select();
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    {
        return false;
    }
}


function RestrictChars(field, maxchar) {
    if (field.value.length > maxchar) {
        alert("Please enter a text of less than " + maxchar + " characters");
        field.focus();
        return false;
    }
    return true;
}

function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}

function findPosY(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;
    return curtop;
}

function openPage(URL) {
    if (Trim(URL) != "") {
        if (URL.indexOf('http://') == -1 && URL.indexOf('https://') == -1) {
            window.open('http://' + URL)
        }
        else {
            window.open(URL)
        }
    }
}

//  Password Validation functions
/*  PASSOWRD RULES

·  It must be a minimum of 8 characters, but no more than 20 characters in length. 
·  It must contain at least 1 uppercase and 1 lowercase letter. 
·  It must contain at least 1 number. 
·  A character cannot be repeated more than 4 times (e.g.  ABCBDBEBFB). 
·  A character cannot be consecutive more than 2 times (e.g. ABCCCDEF). 
·  Minimum password age is 1 day (i.e. if you change your password today, you cannot change it again for 24 hours).
*/




function chkPwdCharRepeatation(varNewPassword) {
    var strPassword = varNewPassword.value
    var len;
    var i;
    var j;
    var ch;
    var ch1;
    var charCount;
    len = varNewPassword.value.length;
    strPassword = strPassword.toUpperCase()
    for (i = 0; i < len; i++) {
        ch = strPassword.charAt(i);
        charCount = 0;
        for (j = 0; j < len; j++) {
            ch1 = strPassword.charAt(j);
            if (ch == ch1) {
                charCount = charCount + 1
            }
            if (charCount > 4) {
                alert("Please do not repeat any character more than 4 times");
                //varNewPassword.value=""
                //varConfirmPassword.value=""
                return false;
            }
        }
    }
    return true;
}

function chkPwdCase(varNewPassword) {

    var strUpper = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    var strLower = new String("abcdefghijklmnopqrstuvwxyz")
    var strNumeric = new String("0123456789")
    var strPassword = varNewPassword.value
    var ch;
    var i;
    var chLower;
    var chNumeric
    var countUpper = 0;
    var countLower = 0;
    var countNumeric = 0;
    var len;
    len = varNewPassword.value.length;

    for (i = 0; i < len; i++) {
        ch = strPassword.charAt(i);
        ch = strUpper.match(ch)
        if (ch != null) {
            countUpper = countUpper + 1;
        }
        chLower = strPassword.charAt(i);
        chLower = strLower.match(chLower);
        if (chLower != null) {
            countLower = countLower + 1;
        }
        chNumeric = strPassword.charAt(i);
        chNumeric = strNumeric.match(chNumeric);
        if (chNumeric != null) {
            countNumeric = countNumeric + 1;
        }
    }

    if (countUpper < 1) {
        alert("Please enter at least one uppercase character");
        return false;
    }

    if (countLower < 1) {
        alert("Please enter at least one lowercase character");
        return false;
    }

    if (countNumeric < 1) {
        alert("Please enter at least one numeric character");
        return false;
    }
    if (countLower > 0 && countNumeric > 0 && countUpper > 0) {
        return true;
    }
}

function chkPwdConsCharRepetation(varNewPassword) {

    var pValue = varNewPassword.value.toUpperCase();
    var iCnt = 0;
    var jCnt = 0;
    var booExit = false;
    for (iCnt = 0; iCnt < pValue.length; iCnt++) {
        var count = 1;
        var chkChar = pValue.substr(iCnt, 1);
        for (jCnt = iCnt + 1; jCnt < pValue.length; jCnt++) {
            if (pValue.substr(jCnt, 1) == chkChar) {
                if (count == 2) {
                    alert("Please do not repeat consecutive character more than 2 times");
                    return false;
                }
                else {
                    count++;
                }
            }
            else {
                break;
            }
        }

    }
    return true;
}

function CheckZip(Location) {
    var src
    src = rmspaces(Location.value);
    var citystateReg = '([a-zA-Z]+, *[a-zA-Z]{2}$)';

    if (isNaN(src) || src.indexOf(', ') != -1) {
        var regex = new RegExp(citystateReg);
        if (!regex.test(src)) {
            alert("Please Enter a valid City, State in Format Chicago, IL ")
            Location.select();
            return false
        }
        else {
            return true
        }
    }
    else {
        if (src.length == 5 && !isNaN(src)) {
            if (src == '11111' || src == '33333' || src == '66666' || src == '77777' || src == '88888' || src == '99999') {
                alert("Invalid zip Location");
                Location.select();
                return false;
            }
            else {
                return true;
            }
        }
        else {
            alert("Please Enter a valid Zip in Format XXXXX ")
            Location.select();
            return false
        }
    }
}

function rmspaces(x) {
    var leftx = 0;
    var rightx = x.length - 1;
    while (x.charAt(leftx) == ' ') leftx++;
    while (x.charAt(rightx) == ' ') --rightx;
    var q = x.substr(leftx, rightx - leftx + 1);
    if ((leftx == x.length) && (rightx == -1)) q = '';
    return (q)
}

function ValidateTaxId(TaxId) {
    var str;
    if (TaxId.length > 11 || TaxId.length < 9) {
        //str = "Tax ID should be between 9-11 characters";
        //return str;	
        return false;
    }

    if (TaxId.indexOf(" ") >= 0)	//it is a 0 based index
    {
        //            str = "spaces are not allowed";
        //            return str;		
        return false;
    }

    if (TaxId.length == 9) {
        if (isNaN(TaxId.substr(0, 9))) {
            //            str = "Tax id should be integer";
            //            return str;		
            return false;
        }
    }
    else if (TaxId.length == 10) {
        if (TaxId.indexOf("-") != 2 || isNaN(TaxId.substr(0, 2)) || isNaN(TaxId.substr(3, 7))) {
            //                str ="incorrect format for 10 digits";
            //                return str;
            return false;
        }
    }
    else if (TaxId.length == 11) {
        if (TaxId.indexOf("-") != 3 || TaxId.indexOf("-", 4) != 6 || isNaN(TaxId.substr(0, 3)) || isNaN(TaxId.substr(4, 2)) || isNaN(TaxId.substr(7, 4))) {
            //                str ="incorrect format for 11 digits";
            //                return str;
            return false;
        }
    }
    return true;
}



//function to validate the zip

function validate_zip(zipval) {
    var str;
    str = "ok";

    if (Trim(zipval) == "") {
        str = "Zip is required";
        return str;
    }
    if (zipval.indexOf(" ") >= 0)	//it is a 0 based index
    {
        str = "Enter a correct zip";
        return str;
    }
    if (zipval.length != 5) {
        str = "Zip should be of 5 digits";
        return str;
    }

    if (isNaN(zipval)) {
        str = "Zip should be numeric";
        return str;
    }

    //in case all of the above are ok			
    return str; //this will contain ok
}



//function to validate the phone number
function validatePhone(phoneval) {
    debugger;
    var str;
    str = "ok";

    if (Trim(phoneval) == "") {
        str = "Phone number is required";
        // alert(str);
        return str;
    }


    if (phoneval.length < 11) {
        str = "Phone number should be of 11 digits";
        //alert(str);
        return str;
    }

    //        if (phoneval.indexOf(" ") >= 0)	//it is a 0 based index
    //        {
    //            str = "Enter a correct phone number";
    //            //alert(str);
    //            return str;
    //        }

    //check for the presence of - in the phone number

//    if (phoneval.indexOf("-") != 3) {
//        str = "correct format of phone number is (xxx-xxx-xxxx)";
//        // alert(str);
//        return str;
//    }

//    if (phoneval.indexOf("-", 5) != 7) {
//        str = "correct format phone number is (xxx-xxx-xxxx)";
//        //alert(str);
//        return str;
//    }

    //the individual elements should be numeric

    if (isNaN(phoneval.substr(0, 3)) || isNaN(phoneval.substr(4, 3)) || isNaN(phoneval.substr(8, 4))) {
        str = "only numeric digits are allowed for phone numbers";
        //alert(str);
        return str;
    }
    return str;

}

//function to validate the fax number

function validatefax(Faxval) {

    var str;
    str = "ok";

    //        if (Trim(Faxval) == "") {
    //            str = "Fax is required";
    //            // alert(str);
    //            return str;
    //        }


    if (Faxval.length < 11) {
        str = "correct format fax number is (xxx-xxx-xxxx)";
        //alert(str);
        return str;
    }

    //        if (phoneval.indexOf(" ") >= 0)	//it is a 0 based index
    //        {
    //            str = "Enter a correct phone number";
    //            //alert(str);
    //            return str;
    //        }

    //check for the presence of - in the phone number

    if (Faxval.indexOf("-") != 3) {
        str = "correct format of fax number is (xxx-xxx-xxxx)";
        // alert(str);
        return str;
    }

    if (Faxval.indexOf("-", 5) != 7) {
        str = "correct format of fax number is (xxx-xxx-xxxx)";
        //alert(str);
        return str;
    }

    //the individual elements should be numeric

    if (isNaN(Faxval.substr(0, 3)) || isNaN(Faxval.substr(4, 3)) || isNaN(Faxval.substr(8, 4))) {
        str = "only numeric digits are allowed for fax number";
        //alert(str);
        return str;
    }
    return str;

}

//function to check the validity of an email entry
function ValidateName(Name) {

    var str;
    str = "ok";
    var RegName = /^([\s.A-Za-z]+)$/;
    if (!RegName.test(Name)) {
        str = "Invalid Name";
        return str;

    }
    return str;
}
function emailCheck(emailStr) {

    var str;
    str = "ok";
    var RegExEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (!RegExEmail.test(emailStr)) {
        str = "Enter Valid E-mail Address";
        return str;

    }
    return str;
}


function ValidatePassword(varNewPassword) {
    // debugger;
    var str = "ok";
    if (varNewPassword.length >= 8 && varNewPassword.length <= 12) {
        return str;
    }
    else {
        str = "Password should be between 8-12 characters.";
    }

    return str;

}
function valid_repassword(password1, password2) {
    //debugger;
    oldpass = password1
    newpass = password2.value
    str = "ok"
    if (oldpass == newpass) {
        return str;
    }
    else {

        str = " Passwords do not match";
        password2.value = ""
        password2.focus()

    }
    return str;
}
function validateRequireField(FieldName, Fieldval) {

    if (Trim(Fieldval) == "") {
        str = FieldName + " is required";
        alert(str);
        return false;
    }

}
function is_valid_url(url) {
    //debugger;
    str = "ok";
    if (url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/)) {
        return str;
    }
    else {
        str = "Invalid URL. (e.g. http://www.yourdomain.com)";
    }
    return str;
}
function validate_year(yearval) {
    yearval.value = trim(yearval.value);
    if (validate_Number(yearval) == false) {
        return false;
    }
    if (!((yearval.value.length == 4) || (yearval.value.length == 2))) {
        return false;
    }

}
function doClick(buttonName, e) {
    //the purpose of this function is to allow the enter key to 
    //point to the correct button to click.
    //alert('called');   
    var key;
    var browser = window.navigator.appName;

    if (window.event)
        key = window.event.keyCode;     //IE
    else
        key = e.which;     //firefox


    if (key == 13) {
        //debugger;
        //Get the button the user wants to have clicked
        var btn = document.getElementById(buttonName)

       
        if (btn != null) { //If we find the button click it
             btn.focus();

             if (browser == 'Microsoft Internet Explorer')
                btn.click();
            else
                eval(btn.href);
            
        }
    }
}
function GC_PostBack(buttonName) {
    jQuery(buttonName).trigger("click");
}

function url_validate(userUrl)
{
    str = "ok";
    var regUrl =/^(((ht|f){1}(tp:[/][/]){1})|((www.){1}))[-a-zA-Z0-9@:%_\+.~#?&//=]+$/;   
    if (regUrl.test(userUrl) == false) {
        str = "Invalid URL";
    }
    else {
        return str;
    }
return str;
}
//    function doClick(btId,e) {
//        var bt = document.getElementById(btId);
//        if (typeof bt == 'object') {
//            if (navigator.appName.indexOf("Netscape") > (-1)) {
//                if (e.keyCode == 13) {
//                    bt.click();
//                    return false;
//                }
//            }
//            if (navigator.appName.indexOf("Microsoft Internet Explorer") > (-1)) {
//                if (event.keyCode == 13) {

//                    bt.click();
//                    event.cancelBubble = true;
//                    return false;
//                }
//            }
//        }
//    }

//JAY  
 function CheckValues($char, $mozChar, txt) { 
    var keyCode;
    var browser;
    var arr = '0123456789.';
    if ($mozChar != null) {
        keyCode = $mozChar;
        browser = "fire"; 
        //alert($mozChar);
        //alert('You have pressed ' + String.fromCharCode(keyCode) + ' in Mozilla' + keyCode);
    }
    else {
        keyCode = $char;
        browser = "ie";
        //alert('You have pressed ' + String.fromCharCode(keyCode) + ' in IE' + keyCode);
    }

    var cha = String.fromCharCode(keyCode);
               
    if (browser == "fire") {
            
        if (keyCode != 46 && keyCode != 8 && keyCode != 0) {
            if (arr.indexOf(cha) == -1) {
                return false;
            }
            else if (cha == '.') {
                if (txt.value.indexOf('.') > -1)
                    return false;
            }

        }
    }
    else {
        if (arr.indexOf(cha) == -1) {
            return false;
        }
        else if (cha == '.')
        {
            if (txt.value.indexOf('.') > -1)
                return false;
        }

    }

} 

            //date in the dd/mm/yyyy format
            function validateDate11(date) {
            debugger;
            var msg;
            var sValidChars;
            var nCounter;
            msg = "ok";
            sValidChars = "1234567890/";
            if (Trim(date) != "") {

                if (date.length != 10) {
                    msg = " Please enter in exact format (dd/mm/yyyy) e.g. 22/01/2000";
                    return msg;
                }
                for (nCounter = 0; nCounter < date.length; nCounter++) {

                    if (sValidChars.indexOf(date.substring(nCounter, nCounter + 1)) == -1) {

                        msg = " Please enter in exact format (xx/xx/xxxx)";
                        nCounter = date.length;
                        return msg;
                    }

                }
                if (date.indexOf("/") != 2) {
                    msg = "Invalid From date : Please enter in exact format (xx/xx/xxxx)";
                    return msg;
                }

                if (date.indexOf("/", 3) != 5) {
                    msg = "Invalid From date : Please enter in exact format (xx/xx/xxxx)";
                    return msg;
                }

                if (date.indexOf("/", 6) < 10 && date.indexOf("/", 6) > 5) {
                    msg = "Invalid From date : Please enter in exact format (xx/xx/xxxx)";
                    return msg;
                }

                var month;
                var day;
                var year;
                day = parseInt(TrimNumber(date.substring(0, 2)));
                month = parseInt(TrimNumber(date.substring(3, 5)));
                year = parseInt(date.substring(6, 10));

                if (month > 12 || month < 1) {
                    msg = " Invalid Month";
                    return msg;
                }
                if (year < 1900 || year > 3000) {
                    msg = " Invalid year. Should be in (1900-3000)";
                    return msg;
                }
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    if (day > 31 || day < 1) {
                        msg = " Days should not be more than 31 or less than 1 for entered Month";
                        return msg;
                    }
                }
                else {
                    if (month == 2 && (year % 4) == 0) {
                        if (day > 29 || day < 1) {
                            msg = " Days should not be more than 29 or less than 1 for entered Month";
                            return msg;
                        }
                    }
                    else if (month == 2 && (year % 4) != 0) {
                        if (day > 28 || day < 1) {
                            msg = " Days should not be more than 28 or less than 1 for entered Month";
                            return msg;
                        }
                    }
                    else {
                        if (day > 30 || day < 1) {
                            msg = " Days should not be more than 30 or less than 1 for entered Month";
                            return msg;
                        }
                    }
                }
                return msg;

            }
            msg = "empty";
            return msg;
        }
 