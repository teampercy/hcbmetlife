﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.ascx.cs" Inherits="HCB.ChangePassword" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script type="text/javascript">

    function ValidateChangePassword() {
        // debugger;

        var PWD = document.getElementById('<%=txtNewPassword.ClientID %>');
        var CPWD = document.getElementById('<%=txtConfirmPassword.ClientID %>');
        var CurrentPassword = document.getElementById('<%=txtCurrentPassword.ClientID %>');
        var username = document.getElementById('<%=txtUsername.ClientID %>');

        if (username.value == "") {
            alert('Please enter username!');
            UName.focus();
            return false;
        }
        if (PWD.value == "") {
            alert('Please enter password!');
            UName.focus();
            return false;
        }
        if (CPWD.value == "") {
            alert('Please enter confirm password!');
            CPWD.focus();
            return false;
        }
        if (PWD.value != CPWD.value) {
            alert('Password does not match!');
            CPWD.focus();
            return false;
        }
        if (CurrentPassword.value == CPWD.value) {

            alert('Current and New Password must not same!');
            CPWD.focus();
            return false;
        }


        return true;
    }

</script>

<div class="form-horizontal">
    <div class="form-body">
<div class="form-group">
    <asp:Label ID="lblChangepassword" runat="server" Class="alert font-md font-red bold" Text="Your password has expired.Please change your password."></asp:Label>
</div>

<div id="tblUpdateMessage" runat="server" class="form-group">
    <div class="row">
    <asp:Label Text="" ID="lblUpdateMessage" runat="server" Class="alert font-md bold"/>
    </div>
    <div class="row">
    <asp:LinkButton ID="lubtRedirectTo" Text="Click here to continue to login." runat="server" Class="font-lg"
        OnClick="Unnamed1_Click" />
    </div>
</div>

<div class="form-group">
    <asp:ValidationSummary ID="validateUserSummary" CssClass="font-red bold font-sm text-left" DisplayMode="BulletList"
        ValidationGroup="validateUser" HeaderText="Summary" runat="server" />
</div>

<div id="tblUpdateInput" runat="server">
    <div class="form-group">
        <label class="col-md-5 control-label font-md bold">
            Username
        </label>
        <div class="col-md-7">
            <asp:TextBox runat="server" ID="txtUsername" Class="form-control input-circle" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-5 control-label font-md bold">
            Current Password
        </label>
        <div class="col-md-7">
            <asp:TextBox runat="server" ID="txtCurrentPassword" TextMode="Password" class="form-control input-circle" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-5 control-label font-md bold">
            New Password</label>
        <div class="col-md-7">
            <asp:TextBox runat="server" ID="txtNewPassword" TextMode="Password" Class="form-control input-circle" />
            <asp:RegularExpressionValidator ID="REGtxtPassword" Display="Dynamic" ValidationExpression="(?=^.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_\d])(?=^.*[^\s].*$).*$"
                runat="server" ControlToValidate="txtNewPassword" ValidationGroup="validateUser"
                ErrorMessage="New Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character."
                ToolTip="New Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character.">
                                                         <img src='images/Exclamation.jpg' alt="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character. ">
            </asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-5 control-label font-md bold">
            Confirm Password</label>
        <div class="col-md-7">
            <asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="Password" Class="form-control input-circle" />
        </div>
    </div>

    <div class="form-actions">
        <asp:Button Text="Reset Password" ID="btnChangePassword" runat="server" OnClientClick="return ValidateChangePassword();" ValidationGroup="validateUser"
            OnClick="btnChangePassword_Click" Class="btn btn-circle green" />
    </div>
</div>
        </div>
</div>
