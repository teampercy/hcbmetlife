﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="HCB.Default" %>

<%@ Register TagPrefix="UC" TagName="DashboardCounter" Src="~/UserControls/DashboardCounter.ascx" %>
<%@ Register TagPrefix="UC" TagName="DashboardActivity" Src="~/UserControls/DashboardActivity.ascx" %>
<%@ Register TagPrefix="UC" TagName="DashboardSchemeACtivity" Src="~/UserControls/DashboardSchemeActivity.ascx" %>
<%@ Register TagPrefix="UC" TagName="DashboardClaimantAge" Src="~/UserControls/DashboardClaimantAge.ascx" %>
<%@ Register TagPrefix="UC" TagName="DahsboardClaimantGender" Src="~/UserControls/DashboardClaimantGender.ascx" %>
<%@ Register TagPrefix="UC" TagName="DashboardCausationMonitor" Src="~/UserControls/DashboardCausationMonitor.ascx" %>
<%@ Register TagPrefix="UC" TagName="DashboardCaseClosed" Src="~/UserControls/DashboardClaimClosed.ascx" %>
<%@ Register TagPrefix="UC" TagName="DahsboardClaimDuration" Src="~/UserControls/DashboardAvgClaimDuration.ascx" %>
<%@ Register TagPrefix="UC" TagName="ReferralChart" Src="~/UserControls/ReferralChart.ascx" %>
<%@ Register TagPrefix="UC" TagName="ClaimPerEmployerChart" Src="~/UserControls/ClaimPerEmployerChart.ascx" %>
<%@ Register TagPrefix="UC" TagName="ClaimPerBrokerChart" Src="~/UserControls/ClaimPerBrokerChart.ascx" %>
<%@ Register TagPrefix="UC" TagName="ClaimPerSchemeChart" Src="~/UserControls/ClaimPerSchemeChart.ascx" %>
<%@ Register TagPrefix="UC" TagName="NursesLocationMap" Src="~/UserControls/NursesLocationMap.ascx" %>
<%@ Register TagPrefix="UC" TagName="DashboardRTWDuration" Src="~/UserControls/DashboardAvgRTWDuration.ascx" %>
<%@ Register TagPrefix="UC" TagName="DashboardReturnToWorkReasonChart" Src="~/UserControls/DashboardReturnToWorkReasonChart.ascx" %>
<%@ Register TagPrefix="UC" TagName="InitialContactChart" Src="~/UserControls/InitialContactChart.ascx" %>
<%@ Register TagPrefix="UC" TagName="InitialVisitChart" Src="~/UserControls/InitialVisitChart.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        /*CSS for show bootstrap moda popup in screen center*/
        .modal {
        }

        .vertical-alignment-helper {
            display: table;
            height: 100%;
            width: 100%;
        }

        .vertical-align-center {
            /* To center vertically */
            display: table-cell;
            vertical-align: middle;
        }

        .modal-content {
            /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
            width: inherit;
            height: inherit;
            /* To center horizontally */
            margin: 0 auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scDashboard" EnablePartialRendering="true"></asp:ScriptManager>

    <div id="divMainDashboard" runat="server">
        <UC:DashboardCounter runat="server"></UC:DashboardCounter>

        <div class="row">
            <div class="col-md-12">
                <div class="mt-element-step">
                    <div class="row step-thin margin-bottom-20">
                        <div class="col-md-3 bg-grey mt-step-col active">
                            <div class="mt-step-title uppercase font-grey-cascade" style="padding-left: 0;">CASE MANAGEMENT</div>
                            <div class="mt-step-content font-grey-cascade" style="padding-left: 0;">Dashboard</div>
                        </div>
                        <div class="col-md-9 bg-grey mt-step-col" style="height: 65px;">
                            <%--<div class="mt-step-number bg-white font-grey"></div>--%>
                            <div class="mt-step-title font-grey-cascade" style="padding-top: 10px;">
                                <asp:Label runat="server">HCB Group and MetLife working in partnership</asp:Label>
                            </div>
                            <div class="mt-step-content font-grey-cascade"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DashboardActivity runat="server"></UC:DashboardActivity>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DashboardSchemeACtivity runat="server"></UC:DashboardSchemeACtivity>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:ClaimPerBrokerChart runat="server"></UC:ClaimPerBrokerChart>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:ClaimPerSchemeChart runat="server"></UC:ClaimPerSchemeChart>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DashboardRTWDuration runat="server"></UC:DashboardRTWDuration>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DashboardReturnToWorkReasonChart runat="server"></UC:DashboardReturnToWorkReasonChart>
            </div>
        </div>

        <%-- Page Break in PDF --%>
        <div style="font-size: 28px; page-break-after: always; background-color: black;"></div>
        <%-- End Page Break --%>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DashboardClaimantAge runat="server"></UC:DashboardClaimantAge>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DahsboardClaimantGender runat="server"></UC:DahsboardClaimantGender>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DashboardCausationMonitor runat="server"></UC:DashboardCausationMonitor>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DashboardCaseClosed runat="server"></UC:DashboardCaseClosed>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:InitialContactChart runat="server" ID="InitialContactChart"></UC:InitialContactChart>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:InitialVisitChart runat="server" ID="InitialVisitChart"></UC:InitialVisitChart>
            </div>
        </div>

        <%-- Page Break in PDF --%>
        <div style="font-size: 28px; page-break-after: always; background-color: black;"></div>
        <%-- End Page Break --%>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <%--<UC:ClaimPerEmployerChart runat="server"></UC:ClaimPerEmployerChart>--%>
                <UC:ReferralChart runat="server"></UC:ReferralChart>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <UC:DahsboardClaimDuration runat="server"></UC:DahsboardClaimDuration>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12" style="display:none;">
                <UC:NursesLocationMap runat="server"></UC:NursesLocationMap>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <%--<UC:ClaimPerEmployerChart runat="server"></UC:ClaimPerEmployerChart>--%>
                <div class="portlet light ">
                    <%--<div class="portlet-title">
                    <div class="caption ">
                        <span class="caption-subject font-dark bold uppercase">Reasons for referral</span>
                        <span class="caption-helper"></span>
                    </div>
                </div>--%>
                    <div class="portlet-body text-left">
                        Access to the MetLife Early Intervention and Funded 
                <br />
                        Management system is restricted to authorised individuals of<br />
                        MetLife and HCB Group staff only.  If you do not have the<br />
                        required authority to use this site, then <strong>logout immediately.</strong><br />
                        <br />

                        Users should not share their login details and passwords with 
                <br />
                        anyone else and should log out whenever leaving their work area.
                <br />
                        <br />

                        The information on this site is confidential and as such is subject,
                    <br />
                        but not limited to the following information security legislation:<br />
                        <br />

                        Computer Misuse Act 1990 (UK)<br />
                        Data Protection Act 2018 (UK)<br />
                        Serious Crime Act 2015 (UK)<br />
                        Data Protection Act 2018 (Ireland)<br />
                        <%--Data Protection (Amendment) Act 2003 (Ireland)--%>                    
                    <br />
                        <br />

                        Provided by:<br />
                        HCB Group working in partnership with MetLife<br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<div class="row">
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption ">
                        <span class="caption-subject font-dark bold uppercase">Broker</span>
                        <span class="caption-helper">Top 5 Brokers</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="ClaimPerBrokerChart1" style="width: 100%; height: 300px">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12" id="DT011">
            <div id="Chart11">
                <div>
                    Broker CHart
                </div>
            </div>
            <div id="divPositiveApplicationBar1" style="text-align: center">
                <img id="img1" src="" style="width: 100%" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBrokerChart" runat="server" Value="" />--%>


    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <%--<asp:Button ID="btnExport" Text="Export" runat="server" OnClick="btnExport_Click" />--%>
            <asp:Button ID="btnHtmlToPdf" runat="server" Text="Export Dashboard" CssClass="btn green" OnClick="btnHtmlToPdf_Click" />

        </div>
    </div>

    <script type="text/javascript" src="<%=Page.ResolveUrl("https://www.gstatic.com/charts/loader.js") %>"></script>
    <%-- <script type="text/javascript" src="Scripts/html2canvas.min.js"></script>--%>

    <script type="text/javascript">
        $(function () {
            google.charts.load('current', { 'packages': ['corechart'] });
            google.charts.setOnLoadCallback(function () {
                DrawReferralChart();
                //DrawClaimPerEmployerChart();
                DrawClaimPerBrokerChart();
                DrawClaimPerSchemeChart();
                DrawReturnToWorkReasonChart();
                DrawInitialContactChart();
                DrawInitialVisitChart();
            });
            //google.charts.setOnLoadCallback(DrawClaimPerBrokerChart1);
        });
    </script>
    <%--<script type="text/javascript">        

        $(function () {
            debugger;
            $("#btnExportToPdf").click(function () {
                debugger;
                html2canvas($("#DT011"), {
                    onrendered: function (canvas) {
                        debugger;
                        var imgsrc = canvas.toDataURL("image/png");
                        console.log(imgsrc);
                        $("#img1").attr('src', imgsrc);
                        $("#divPositiveApplicationBar1").show();
                    }
                });
            });
        });

    </script>--%>

    <%--<script type="text/javascript">
        function DrawClaimPerBrokerChart1() {
            debugger;
            $.ajax({
                url: "Default.aspx/GetTotalClaimPerBroker",
                type: "POST",
                contentType: "application/json;charset=utf-8",
                success: function (res) {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Broker');
                    data.addColumn('number', 'Claims');
                    $.each(res.d, function (BrokerName, TotalCases) {
                        data.addRow([BrokerName, TotalCases]);
                    });

                    var options = {
                        is3D: true,
                        //legend: "bottom",
                        //pieSliceText: 'value',
                        title: "Total Cases per Broker"
                    };
                    var chart = new google.visualization.PieChart($('#ClaimPerBrokerChart1')[0]);
                    chart.draw(data, options);
                    debugger;
                    //var image = '<img src="' + chart.getImageURI() + '">';
                    //$("[id*=btnExport]").click(function () {
                    //    $("input[name=chart_data]").val(chart.getImageURI());
                    //});

                    //$("[id*=btnExport]").click(function () {
                    debugger;
                    var chartdata = chart.getImageURI();
                    $("#img1").attr("src", chartdata);
                    event.preventDefault();

                    html2canvas($("#DT011")[0]).then(function (canvas) {
                        debugger;
                        var base641 = canvas.toDataURL();
                        $("[id*=hdnBrokerChart]").val(base641);
                    });
                    //});

                },
                failure: function (response) {
                    alert("Unable to display Broker chart");
                }
            });
        }

    </script>--%>
</asp:Content>
