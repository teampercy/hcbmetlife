﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="GenerateReport.aspx.cs" Inherits="HCB.GenrateReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--<link id="Link1" rel="Stylesheet" href="Styles/Slate/plugin.css" type="text/css"
        media="screen" title="slate1" runat="server" />--%>
    <meta http-equiv="Cache-Control" content="no-store" />
    <meta http-equiv="Pragma" content="no-cache" />
    <%--<script type="text/javascript" src="Scripts/Slate/jquery.1.4.2.min.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.portlet.js"></script>
    <script type="text/javascript" src="Scripts/Slate/plugin.js"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            slate.init();
            slate.portlet.init();
        });


    </script>--%>
    <%--<style type="text/css">
        #example_length {
            float: right;
        }

            #example_length select {
                width: 100px;
            }

            #example_length select {
                width: 100px;
            }

        #example_filter {
            visibility: hidden;
            width: 10%;
            float: left;
            height: 10px;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server">Management Information</asp:Label>
            </div>
        </div>

        <div class="portlet-body">
            <div class="portlet-content font-md">
                <div class="form-group">
                    <label class="control-label font-md">Select Report</label>
                    <asp:DropDownList ID="ddlReportName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReportName_SelectedIndexChanged" CssClass="form-control input-md input-inline input-circle">
                    </asp:DropDownList>
                    <label class="control-label font-md">Export to</label>
                    <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown" CssClass="form-control input-md input-inline input-circle">
                        <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                        <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo" ValidationGroup="valdtdown"></asp:RequiredFieldValidator>

                    <asp:Button ID="lnkExport" runat="server" Text="Export" ValidationGroup="valdtdown" CssClass="btn btn-circle blue bold"
                        OnClick="lnkExport_Click"></asp:Button>
                </div>
                
                <div id="DivContainer" runat="server" style="display: inline">
                </div>
            </div>
        </div>

    </div>

    <%--<table border="0" cellpadding="0" cellspacing="0" width="80%" style="float: right; margin: 0 auto;"
        class="MainText" id="tableMain">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td>
                <b>Select Report </b>
                <asp:DropDownList ID="ddlReportName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReportName_SelectedIndexChanged">--%>
    <%--          <asp:ListItem Text="-- select --" Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Service Required" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Employeement status" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Illness Injury" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Claim closed Reason" Value="6"></asp:ListItem>
                    <asp:ListItem Text="Reason Closed" Value="7"></asp:ListItem>
                    <asp:ListItem Text="Broker" Value="8"></asp:ListItem>
                    <asp:ListItem Text="Corporate Partner" Value="9"></asp:ListItem>
                    <asp:ListItem Text="Product Type" Value="10"></asp:ListItem>
                    <asp:ListItem Text="Waiting period" Value="11"></asp:ListItem>
                   
                    <asp:ListItem Text="Incapacity Definition" Value="14"></asp:ListItem>
                    <asp:ListItem Text="Type Of Visit" Value="15"></asp:ListItem>
                    <asp:ListItem Text="Type Of Calls" Value="16"></asp:ListItem>--%>
    <%--</asp:DropDownList>
            </td>
            <td>
                <b>Export to </b>
                <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown">
                    <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                    <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo" ValidationGroup="valdtdown"></asp:RequiredFieldValidator>

                <asp:Button ID="lnkExport" runat="server" Text="Export" ValidationGroup="valdtdown"
                    OnClick="lnkExport_Click"></asp:Button>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
    </table>

    <div id="DivContainer" runat="server" style="display: inline">
    </div>--%>
</asp:Content>
