﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="MIAverageTimeSummary.aspx.cs" Inherits="HCB.MIAverageTimeSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--<link id="Link1" rel="Stylesheet" href="Styles/Slate/plugin.css" type="text/css"
        media="screen" title="slate1" runat="server" />
    <script type="text/javascript" src="Scripts/Slate/jquery.1.4.2.min.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.portlet.js"></script>
    <script type="text/javascript" src="Scripts/Slate/plugin.js"></script>--%>
    <script type="text/javascript" charset="utf-8">
        //$(document).ready(function () {
        //    slate.init();
        //    slate.portlet.init();
        //});
        //To Open new window for view report details.
        function openReportDetail(val) {

            //var value = $("#Hcbreport").text();
            val = val.substr(3);
            $.ajax({
                url: "MonthlyReport.aspx/SetUrl",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ 'val': val }),
                success: function () {
                    //window.open("ClientRecord.aspx?HcbReportId=" + val, '_blank'); //To open window in New Tab
                    window.open("ClientRecord.aspx", '_self');    //To open window in Self Tab        
                }
            });            
        }
        function ValidateDate() {
            try {
                var intFlag = 1;

                var txtFromDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
                var txtToDate = document.getElementById("<%=txtToDate.ClientID%>").value;

                var dtFromDate = new Date();
                dtFromDate.setFullYear(txtFromDate.substring(6, 10), txtFromDate.substring(3, 5) - 1, txtFromDate.substring(0, 2));

                var dtToDate = new Date();
                dtToDate.setFullYear(txtToDate.substring(6, 10), txtToDate.substring(3, 5) - 1, txtToDate.substring(0, 2));

                if (dtFromDate > dtToDate) {
                    intFlag = -1;
                    alert("From Date should not exceed To Date");
                }
                if (intFlag == 1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            catch (err) {
            }
        }
    </script>
    <%--<style type="text/css">
        #example_length {
            float: right;
        }

            #example_length select {
                width: 100px;
            }

            #example_length select {
                width: 100px;
            }

        #example_filter {
            visibility: hidden;
            width: 10%;
            float: left;
            height: 10px;
        }

        .sorting_asc {
            background-image: none;
        }

        .sorting_desc {
            background-image: none;
        }

        .sorting {
            background-image: none;
        }

        .sorting_asc_disabled {
            background-image: none;
        }

        .sorting_desc_disabled {
            background-image: none;
        }

        .dataTables_wrapper {
            width: 90%;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server">MI Charge Average Time Detail Report</asp:Label>
            </div>
        </div>

        <div class="portlet-body">
            <div class="portlet-content font-md">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txtFromDate" ErrorMessage="RequiredFieldValidator"
                                ForeColor="Red" ValidationGroup="vgGenReport">*</asp:RequiredFieldValidator>
                            <label class="control-label font-md bold">From </label>

                            <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10" CssClass="form-control input-md input-inline input-small"
                                TabIndex="91" onchange="javascript:return validateDate(this);"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="txtToDate" ErrorMessage="RequiredFieldValidator"
                                ForeColor="Red" ValidationGroup="vgGenReport">*</asp:RequiredFieldValidator>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender24" runat="server" Mask="99/99/9999"
                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtFromDate">
                            </cc1:MaskedEditExtender>

                            <label class="control-label font-md bold">To </label>
                            <asp:TextBox ID="txtToDate" runat="server" MaxLength="10" CssClass="form-control input-md input-inline input-small"
                                TabIndex="91" onchange="javascript:return validateDate(this);"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtToDate">
                            </cc1:MaskedEditExtender>
                            <asp:Button CssClass="btn btn-circle blue bold"
                                ID="btnGetReport" runat="server" Text="Generate Report" ValidationGroup="vgGenReport"
                                ToolTip="click to genrate report" OnClick="btnGetReport_Click" />
                        </div>
                    </div>

                    <div class="col-md-2"></div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label font-md bold">Export to </label>
                            <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown" CssClass="form-control input-md input-inline input-circle">
                                <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                                <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo"
                                ValidationGroup="valdtdown"></asp:RequiredFieldValidator>
                            <asp:Button ID="lnkExport" ValidationGroup="valdtdown" runat="server" Text="Export" CssClass="btn btn-circle blue bold"
                                OnClick="lnkExport_Click"></asp:Button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="lblerr" runat="server" CssClass="font-red bold"></asp:Label>
                </div>

                <div id="divReport" runat="server" style="overflow: auto;">

                    <div id="wassup">
                    </div>

                </div>

            </div>
        </div>

    </div>

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table border="0" cellpadding="0" cellspacing="0" width="80%" style="margin: 0 auto;"
        class="MainText" id="tableMain">
        <tr>
            <td colspan="3" align="center">
                <div class="portlet-header">
                    <asp:Label CssClass="PageTitle" ID="lblTitle" runat="server">MI Charge Average Time Detail Report</asp:Label>
                </div>
                <br />
            </td>
        </tr>
        <tr>
            <td style="width: 65%" valign="middle">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="txtFromDate" ErrorMessage="RequiredFieldValidator"
                    ForeColor="Red" ValidationGroup="vgGenReport">*</asp:RequiredFieldValidator>
                <strong>From : </strong>

                <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10" Width="100px"
                    CssClass="Textbox" TabIndex="91" onchange="javascript:return validateDate(this);"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtToDate" ErrorMessage="RequiredFieldValidator"
                    ForeColor="Red" ValidationGroup="vgGenReport">*</asp:RequiredFieldValidator>
                <cc1:MaskedEditExtender ID="MaskedEditExtender24" runat="server" Mask="99/99/9999"
                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtFromDate">
                </cc1:MaskedEditExtender>

                <strong>To : </strong>
                <asp:TextBox ID="txtToDate" runat="server" MaxLength="10" Width="100px"
                    CssClass="Textbox" TabIndex="91" onchange="javascript:return validateDate(this);"></asp:TextBox>
                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtToDate">
                </cc1:MaskedEditExtender>

                &nbsp;--%><%--     Height="40px" Width="150px" BackColor="#9681FC" ForeColor="White"  --%><%--<asp:Button
                    ID="btnGetReport" runat="server" Text="Generate Report" ValidationGroup="vgGenReport"
                    ToolTip="click to genrate report" OnClick="btnGetReport_Click" />

            </td>
            <td style="float: left">
                <b>Export to </b>
                <asp:DropDownList ID="ddlExprotTo" runat="server" ValidationGroup="valdtdown">
                    <asp:ListItem Text="-- select --" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="PDF" Value="1"></asp:ListItem>
                    <asp:ListItem Text="EXCEL" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rqfddlExprotTo" runat="server" InitialValue="0" ControlToValidate="ddlExprotTo"
                    ValidationGroup="valdtdown"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="lnkExport" ValidationGroup="valdtdown" runat="server" Text="Export"
                    OnClick="lnkExport_Click"></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblerr" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <div id="divReport" runat="server" style="overflow: auto;">

        <div id="wassup">
        </div>

    </div>--%>
</asp:Content>
