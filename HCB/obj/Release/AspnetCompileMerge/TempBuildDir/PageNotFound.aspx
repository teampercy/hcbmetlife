﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageNotFound.aspx.cs" Inherits="HCB.PageNotFound"  MasterPageFile="~/Site.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div style="text-align: center; width:80%">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/LadyImage.jpg" vspace="15" style="border: 1px lightgrey solid;" />
                </td>
                <td class="MainText" style="width: 535px;" rowspan="3" valign="top">
                    <br />
                    <br />
                    <span style="font-size: 12pt"><strong> 
                        Page Not Found </strong></span><br />
                    <br />
                  
                </td>
            </tr>
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/MeetingImage.jpg" style="border: 1px lightgrey solid;" />
                </td>
            </tr>
            <tr>
                <td style="width: 190px; height: 160px" valign="top">
                    <img src="Images/HandshakeImage.jpg" style="border: 1px lightgrey solid;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
