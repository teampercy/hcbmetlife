﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Referral.aspx.cs" Inherits="HCB.Referral" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });

        function ValidateForm() {            
            var ReferralName = document.getElementById('<%=txtReferralName.ClientID%>');
            if (ReferralName.value == "") {
                alert('Referral Name is Required');
                ReferralName.focus();
                return false;
            }

            var ReferralJobTitle = document.getElementById('<%=txtReferralJobTitle.ClientID%>');
            if (ReferralJobTitle.value == "") {
                alert('Referral Job title is Required');
                ReferralJobTitle.focus();
                return false;
            }

            var ReferralCompanyName = document.getElementById('<%=txtReferralCompanyName.ClientID%>');
            if (ReferralCompanyName.value == "") {
                alert('Referral Company Name Name is Required');
                ReferralCompanyName.focus();
                return false;
            }

            var ReferralTelephone = document.getElementById('<%=txtReferralTelephone.ClientID%>');
            if (ReferralTelephone.value == "") {
                alert('Telephone is Required');
                ReferralTelephone.focus();
                return false;
            }

            var ReferralEmail = document.getElementById('<%=txtReferralEmail.ClientID%>');
            if (ReferralEmail.value == "") {
                alert('Email is Required');
                ReferralEmail.focus();
                return false;
            }

            var DateOfReferral = document.getElementById('<%=txtDateReferral.ClientID%>');
            if (DateOfReferral.value == "") {
                alert('Date Of Referral is Required');
                DateOfReferral.focus();
                return false;
            }

            return true;
        }        

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12 text-left bold">
            Please complete the referral form and email to <u>enquiries@hcbgroup.co.uk</u>
            <br />
            <br />
            For general enquiries or to discuss a case prior to referral please contact us: <u>enquiries@hcbgroup.co.uk</u>
            <br />
            Tel: 01235 519924<br />
            Fax: 01235 512466<br />
            <br />
            CONFIDENTIAL<br />
            <br />
        </div>
    </div>

    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Referral Form
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">
                    <div class="form-group">
                        <asp:Label ID="lblMsg" runat="server" Font-Bold="true" Font-Names="Arial" Font-Size="12pt"
                            ForeColor="Red"></asp:Label>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Name</label>
                                <asp:TextBox ID="txtReferralName" runat="server" autocomplete="off"
                                    class="form-control form-control-solid placeholder-no-fix" placeholder="Name" name="referralname" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Job Title</label>
                                <asp:TextBox ID="txtReferralJobTitle" runat="server" autocomplete="off"
                                    class="form-control form-control-solid placeholder-no-fix" placeholder="Job Title" name="referraljobtitle" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Company/Organisation Name</label>
                                <asp:TextBox ID="txtReferralCompanyName" runat="server" autocomplete="off"
                                    class="form-control form-control-solid placeholder-no-fix" placeholder="Company/Organisation Name" name="referralcompanyname" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Address</label>
                                <asp:TextBox ID="txtReferralAddress" runat="server" autocomplete="off"
                                    class="form-control form-control-solid placeholder-no-fix" placeholder="Address & Postcode" name="referraladdress" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group text-left">
                                <label class="control-label">Your Contact Details:</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-4">Telephone</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtReferralTelephone" runat="server" MaxLength="12"
                                        CssClass="form-control input-medium"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-9">
                                    <asp:TextBox ID="txtReferralEmail" runat="server" CssClass="form-control input-large"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-4">Date of Referral:</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtDateReferral" runat="server" CssClass="form-control input-small date-picker"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Name of Employee:</label>
                                <asp:TextBox ID="txtEmployeeName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Address:</label>
                                <asp:TextBox ID="txtEmployeeAddress" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Date of Birth:</label>
                                <asp:TextBox ID="txtDateOfBirth" runat="server" CssClass="form-control date-picker"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Email Address:</label>
                                <asp:TextBox ID="txtEmployeeEmail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Employee’s job title and working location:</label>
                                <asp:TextBox ID="txtEmployeeJobTitleAndLocation" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Contact telephone number:</label>
                                <asp:TextBox ID="txtEmployeeTelephone" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Line Manager’s name and job title:</label>
                                <asp:TextBox ID="txtLineManagerNameAndJobTitle" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Contact telephone number / email address:</label>
                                <asp:TextBox ID="txtLineManagerTelephoneOrEmail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Is the Employee?</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:RadioButtonList ID="RdbIsEmployee" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Attending Work&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem>Not Attending Work&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem>Pre-natal&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem>Post-natal&nbsp;&nbsp;</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">Has the employee a full or part-time contract?</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:RadioButtonList ID="RdbEmployeeContract" runat="server" RepeatDirection="Horizontal"
                                    OnSelectedIndexChanged="RdbEmployeeContract_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Selected="True">Full-time&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem>Part-time&nbsp;&nbsp;</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div id="divPartTimeDetails" runat="server">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-left">
                                    <label class="control-label">If part-time, please indicate the working times: </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group text-left">
                                    <label class="control-label">Monday</label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group text-left">
                                    <label class="control-label">Tuesday</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group text-left">
                                    <label class="control-label">Wednesday</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group text-left">
                                    <label class="control-label">Thursday</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group text-left">
                                    <label class="control-label">Friday</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:TextBox ID="txtPartTimeFromMonday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>to
                                <asp:TextBox ID="txtPartTimeToMonday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:TextBox ID="txtPartTimeFromTuesday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>to
                                <asp:TextBox ID="txtPartTimeToTuesday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:TextBox ID="txtPartTimeFromWednesday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>to
                                <asp:TextBox ID="txtPartTimeToWednesday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:TextBox ID="txtPartTimeFromThursday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>to
                                <asp:TextBox ID="txtPartTimeToThursday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:TextBox ID="txtPartTimeFromFriday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>to
                                <asp:TextBox ID="txtPartTimeToFriday" runat="server" CssClass="form-control" placeholder="time am/pm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">1.	Is the employee aware of the reasons for this referral?</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:RadioButtonList ID="RdbEmployeeAware" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Yes&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem>No&nbsp;&nbsp;</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">2.	Has consent been given?</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:RadioButtonList ID="RdbConsentGiven" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Yes&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem>No&nbsp;&nbsp;</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-left">
                                <label class="control-label">3.	What, in broad outline, are the reasons for this referral?</label>
                                <asp:TextBox ID="txtBroadOutline" runat="server" TextMode="MultiLine" CssClass="form-control"
                                    Height="109px"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-left">
                                <label class="control-label">4.	Date of illness / condition onset:</label>
                                <asp:TextBox ID="txtDateOfIllness" runat="server" CssClass="form-control input-small date-picker"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-left">
                                <label class="control-label">5.	Is there any other information you consider to be relevant to this referral?</label>
                                <asp:TextBox ID="txtOtherInformation" runat="server" TextMode="MultiLine" CssClass="form-control"
                                    Height="109px"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-left">
                                <label class="control-label">6.	Is there any other specific advice you require from us?</label>
                                <asp:TextBox ID="txtOtherAdvice" runat="server" TextMode="MultiLine" CssClass="form-control"
                                    Height="109px"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-left">
                                <label class="control-label">7.	Is there any relevant information to add?</label>
                                <asp:TextBox ID="txtAddRelevantInformation" runat="server" TextMode="MultiLine" CssClass="form-control"
                                    Height="109px"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-left">
                                <label class="control-label">8.	Please give details of any previous sickness/absence. </label>
                                <label class="control-label">
                                    If absences have been a cause of concern please include dates, actual number of working days
                                     lost and diagnosis or reasons for absence (absence record can be attached)</label>
                                <asp:TextBox ID="txtDetailsOfPreviousSickness" runat="server" TextMode="MultiLine" CssClass="form-control"
                                    Height="109px"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left">
                                <label class="control-label">9.	Is the employee covered by medical insurance?</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:RadioButtonList ID="RdbEmployeeMedicalInsurance" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Yes&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem>No&nbsp;&nbsp;</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-left bold">
                                <br />
                                <br />
                                Declaration<br />
                                <br />
                                I/we declare that the information given
                                <br />
                                <br />
                                Please attach any relevant documents (e.g. medical reports)<br />
                                Please return your completed form to <u>enquiries@hcbgroup.co.uk</u>
                                <br />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn blue" OnClick="btnSubmit_Click"
                                    OnClientClick="return ValidateForm();"/>
                                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btn blue" OnClick="btnCancel_Click" />
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn grey-salsa btn-outline" OnClick="btnClear_Click" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
