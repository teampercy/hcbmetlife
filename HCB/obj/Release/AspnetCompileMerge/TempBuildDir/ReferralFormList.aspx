﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReferralFormList.aspx.cs" Inherits="HCB.ReferralFormList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        /*Javascript for Gridview Datatable*/
        $(document).ready(function () {
            GridDataTable();
        });

        function GridDataTable() {                    
            $("table.table-striped").prepend($("<thead></thead>").append($("table.table-striped").find("tr:first"))).dataTable();                                              
            $('table.table-striped').DataTable();
            return false;
        }  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Referral Form List
            </div>
        </div>

        <div class="portlet-body font-md">
            <div class="form-group">
                <asp:LinkButton ID="lnkAddReferralForm" runat="server" OnClick="lnkAddReferralForm_Click" CssClass="btn red btn-circle pull-right">
                    <i class="fa fa-plus"></i> Add Referral Form</asp:LinkButton>
            </div>
            <br />
            <br />
            <asp:GridView runat="server" ID="gvReferralForm" AutoGenerateColumns="false" EmptyDataText="No Records Found "
                CssClass="table table-striped table-bordered table-hover" OnRowCommand="EditReferralForm" OnRowDeleting="DeleteReferralForm">
                <HeaderStyle CssClass="font-white bg-green-steel" />
                <Columns>
                    <asp:BoundField HeaderText="Name" DataField="ReferralName" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" />
                    <asp:BoundField HeaderText="Job Title" DataField="ReferralJobTitle" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" />
                    <asp:BoundField HeaderText="Company Name" DataField="ReferralCompanyName" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" />

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" CssClass="btn btn-outline btn-circle btn-sm purple"
                                CommandArgument='<%#Eval("ReferralFormID") %>'>
                                        <i class="fa fa-edit"></i> Edit</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<% #Eval("ReferralFormID") %>'
                                OnClientClick="return confirm('You want to delete this record ?');"
                                CommandName="Delete" CssClass="btn btn-outline btn-circle dark btn-sm black">
                                        <i class="fa fa-trash-o"></i> Delete
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>
