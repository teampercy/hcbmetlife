﻿<%@ Page Title="Manage Users" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UpdateAdminUser.aspx.cs" Inherits="HCB.UpdateAdminUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>User Profile Maintenance
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="form-group">
                        <div class="col-md-7">
                            <asp:Label ID="lblMsg" runat="server" CssClass="font-red bold font-sm"></asp:Label>
                            <br />
                            <asp:ValidationSummary ID="validateUserSummary" CssClass="font-red bold font-sm text-left" DisplayMode="BulletList"
                                    ValidationGroup="validateUser" HeaderText="Summary" runat="server" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">User Name(Email Address)</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFtxtUserName" runat="server" ControlToValidate="txtUserName"
                                    CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic" ToolTip="User Email Required"
                                    ErrorMessage="User Email Required">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RGEtxtUserName" ControlToValidate="txtUserName"
                                    CssClass="font-red bold font-sm" ValidationGroup="validateUser" runat="server" ErrorMessage="Invalid Email address"
                                    ToolTip="Invalid Email address" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"> 
                                     <img src='images/Exclamation.jpg'></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Name</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFtxtName" runat="server" ControlToValidate="txtName"
                                    CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic" ToolTip="Name Required"
                                    ErrorMessage="Name Required">
                                    <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="nameRegex" runat="server" ControlToValidate="txtName"
                                    ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid Name" Display="Dynamic"
                                    ValidationGroup="validateUser">
                                </asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Password</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFtxtPassword" runat="server" ControlToValidate="txtPassword"
                                    CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic" ErrorMessage="Password is Required"
                                    ToolTip="Password is Required">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="REGtxtPassword" Display="Dynamic" ValidationExpression="(?=^.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_\d])(?=^.*[^\s].*$).*$"
                                    runat="server" ControlToValidate="txtPassword" ValidationGroup="validateUser"
                                    ErrorMessage="Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character."
                                    ToolTip="Password must be atleast 8 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character.">
                                                         <img src='images/Exclamation.jpg' alt="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character. ">
                                </asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Address</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine"
                                    Height="109px" >
                                </asp:TextBox>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFtxtAddress" runat="server" ControlToValidate="txtAddress"
                                    CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic" ToolTip="Address Required"
                                    ErrorMessage="Address Required">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAddress"
                                    ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$" ErrorMessage="Invalid Address"
                                    ValidationGroup="validateUser" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">City</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFtxtCity" runat="server" ControlToValidate="txtCity"
                                    CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic" ToolTip="City Required"
                                    ErrorMessage="City Required">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCity"
                                    ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid City"
                                    ValidationGroup="validateUser" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Telephone Number</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtTeleNum" runat="server" MaxLength="12" onkeypress="return CheckValues(event.keyCode, event.which,this);"
                                    CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFtxtTeleNum" runat="server" ControlToValidate="txtTeleNum"
                                    CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic" ToolTip="TelePhone No. Required"
                                    ErrorMessage="TelePhone No. Required">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTeleNum"
                                    ForeColor="Red" ValidationExpression="^[0-9]+$" ErrorMessage="Invalid Telephone Number"
                                    ValidationGroup="validateUser" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Account Status</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlAcctStatus" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">-- Select Account Status --</asp:ListItem>
                                    <asp:ListItem Value="1">Enabled</asp:ListItem>
                                    <asp:ListItem Value="2">Disabled</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFddlAcctStatus" runat="server" ControlToValidate="ddlAcctStatus"
                                    InitialValue="0" CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic"
                                    ToolTip="Select Account Status" ErrorMessage="Select Account Status">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">User Type</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlUserType" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">-- Select User Type --</asp:ListItem>
                                    <asp:ListItem Value="1">Inco</asp:ListItem>
                                    <asp:ListItem Value="2">Admin</asp:ListItem>
                                    <asp:ListItem Value="3">Nurse</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        <div class="col-md-2">
                                <asp:RequiredFieldValidator ID="RQFusertypw" runat="server" ControlToValidate="ddlUserType"
                                    InitialValue="0" CssClass="font-red bold font-sm" ValidationGroup="validateUser" Display="Dynamic"
                                    ToolTip="Select user type" ErrorMessage="Select user type">
                           <img src='images/Exclamation.jpg'>
                                </asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Insurance Company Name</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlInsCompName" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Nominated Nurse</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlNominatedNurse" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group" id="trLoadUser" runat="server">
                        <label class="col-md-3 control-label font-md">Load User</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlLoadUser" runat="server" CssClass="form-control" AutoPostBack="true" CausesValidation="true"
                                    OnSelectedIndexChanged="ddlLoadUser_SelectedIndexChanged">
                                </asp:DropDownList>
                        </div>
                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-9">
                            <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" CssClass="btn btn-circle blue bold"/>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete this User?');"
                        OnClick="btnDelete_Click" CssClass="btn btn-circle blue bold"/>
                    <asp:Button ID="btnSave" runat="server" ValidationGroup="validateUser" Text="Save"
                        OnClick="btnSave_Click" CssClass="btn btn-circle blue bold"/>
                    <asp:HiddenField ID="hdnUserId" runat="server" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
</asp:Content>
