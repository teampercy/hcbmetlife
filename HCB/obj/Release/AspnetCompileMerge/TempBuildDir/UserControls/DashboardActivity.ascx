﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardActivity.ascx.cs" Inherits="HCB.UserControls.DashboardActivity" %>

<!-- BEGIN BROKER ACTIVITY PORTLET-->
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark hide"></i>
            <span class="caption-subject font-dark bold uppercase">Broker Activity</span>
        </div>
    </div>

    <div class="portlet-body form">
        <div class="form-horizontal">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-4">Select Broker</label>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlDashboardBroker" CssClass="form-control input-medium" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlDashboardBroker_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-4">Date Range</label>
                            <div class="col-md-6">
                                <%--<div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                    <asp:TextBox runat="server" class="form-control" name="from" ID="txtFromDate" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                                    <span class="input-group-addon">to </span>                                    
                                    <asp:TextBox runat="server" class="form-control" name="to" ID="txtToDate"></asp:TextBox>                                    
                                </div>--%>
                                <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                    <asp:TextBox runat="server" class="form-control" name="from" ID="txtFromDate"></asp:TextBox>
                                    <span class="input-group-addon">to </span>
                                    <asp:TextBox runat="server" class="form-control" name="to" ID="txtToDate"></asp:TextBox>
                                </div>
                                <!-- /input-group -->
                                <span class="help-block">Select date range </span>
                            </div>
                            <div class="col-md-1" style="padding-left:5px;">                                                                
                                <asp:LinkButton runat="server" ID="btnDateRange" CssClass="btn green" OnClick="btnDateRange_Click">
                                    <i class="fa fa-check-circle"></i>
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <asp:Button runat="server" ID="btnOpen" CssClass="btn blue btn-lg" Text="Open Cases" OnClick="btnOpen_Click" data-toggle="modal" />
                    </div>
                    <div class="col-md-5">
                        <asp:Button runat="server" ID="btnClose" CssClass="btn blue btn-lg" Text="Closed Cases" OnClick="btnClose_Click" data-toggle="modal" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%-- Modal popup for Displaying Open & Closed cases on selection of Broker --%>
<div id="divBrokerCases" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title font-green bold">
                        <asp:Label runat="server" ID="lblPopupHeader"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="tblBrokerCases">
                        <thead>
                            <tr class="font-white bg-green-steel">
                                <th>Ref No</th>
                                <th>Scheme Name</th>
                                <th>Claimant Name</th>
                                <th>HCB Received Date</th>
                            </tr>
                        </thead>

                        <tbody>
                            <asp:Repeater ID="rptResults" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBReference") %>'
                                                OnCommand="lnk_Command" CommandName="HCBRef" />
                                        </td>
                                        <td>
                                            <%# Eval("SchemeName")%>
                                        </td>
                                        <td>
                                            <%# Eval("ClientName")%>
                                        </td>
                                        <td>
                                            <%# Eval("HCBReceivedDate")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBReference") %>'
                                                OnCommand="lnk_Command" CommandName="HCBRef" />
                                        </td>
                                        <td>
                                            <%# Eval("SchemeName")%>
                                        </td>
                                        <td>
                                            <%# Eval("ClientName")%>
                                        </td>
                                        <td>
                                            <%# Eval("HCBReceivedDate")%>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>

                    <div class="modal-footer">
                        <asp:Button ID="btnClosePopup" runat="server" data-dismiss="modal" CssClass="btn green bold" Text="OK" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });

        $('#tblBrokerCases').DataTable();
    });

    //Popup code written on Master Page

    //function PopupAddNew() {
    //    debugger;
    //    alert('Open');
    //    //$('#btnOpen').attr('data-target', '#divBrokerCases');
    //    //$('#divBrokerCases').modal('show');
    //}

</script>

