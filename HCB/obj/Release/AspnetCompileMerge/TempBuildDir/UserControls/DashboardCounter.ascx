﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardCounter.ascx.cs" Inherits="HCB.UserControls.DashboardCounter" %>

<div class="row widget-row">
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
            <h4 class="widget-thumb-heading">Open Cases</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-green icon-bulb"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">&nbsp;</span>                    
                    <asp:Label CssClass="widget-thumb-body-stat" runat="server" ID="lblOpenCaseCounter" data-counter="counterup">0</asp:Label>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
            <h4 class="widget-thumb-heading">Closed Cases </h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-red icon-layers"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">&nbsp;</span>
                    <asp:Label CssClass="widget-thumb-body-stat" runat="server" ID="lblCloseCaseCounter" data-counter="counterup">0</asp:Label>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
            <h4 class="widget-thumb-heading">Employers</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">&nbsp;</span>
                     <asp:Label CssClass="widget-thumb-body-stat" runat="server" ID="lblEmployersCounter" data-counter="counterup">0</asp:Label>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
            <h4 class="widget-thumb-heading">Average RTW Duration</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">Days</span>
                   <asp:Label CssClass="widget-thumb-body-stat" runat="server" ID="lblAverageClaimDurationCounter" data-counter="counterup">0</asp:Label>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
</div>
<script>
    //$(function () {
    //    $("[data-counter='counterup']").counterUp({
    //        delay: 10,
    //        time: 1000
    //    });
    //});
</script>