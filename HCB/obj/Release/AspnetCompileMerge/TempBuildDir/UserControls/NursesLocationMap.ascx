﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NursesLocationMap.ascx.cs" Inherits="HCB.UserControls.NursesLocationMap" %>

<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark hide"></i>
            <span class="caption-subject font-dark bold uppercase">Locations</span>
            <span class="caption-helper">HCB Nurses</span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="NursesLocationMap" style="height: 300px">
        </div>
    </div>
</div>
<script>
    var map = null;
    var geocoder = null;
    var infowindow = null;
    var TempLocations = [];
    $(function () {

        $.ajax({
            url: 'Default.aspx/GetNursesLocations',
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (respone) {
                ShowGoogleMap(respone.d);
            },
            fail: function (error) {
                alert("Unable to load show google map");
            }
        });

    });

    function ShowGoogleMap(NursesLocations) {       
        map = new google.maps.Map($("#NursesLocationMap")[0], {
            zoom: 6,
            center: { lat: 51.5074, lng: 0.1278 }, // Center on 'London'  
            streetViewControl: false,
            mapTypeControl: false,
            fullscreenControl: true,
            scaleControl: true,
            minZoom: 1,
            maxZoom: 9
        });

        geocoder = new google.maps.Geocoder();
        for (var i = 0; i < NursesLocations.length; i++) {
            AddMarker(i, NursesLocations);
        }

    }


    function AddMarker(i, NursesLocations) {      
        geocoder.geocode({ 'address': NursesLocations[i].City }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                var Marker = new google.maps.Marker({
                    position: results[0].geometry.location,
                    map: map
                });
                google.maps.event.addListener(Marker, 'click', function () {
                    if (infowindow) {
                        infowindow.close();
                    }
                    infowindow = new google.maps.InfoWindow({
                        content: "<b>" + NursesLocations[i].Name + "</b></br>" + NursesLocations[i].Address
                    });
                    infowindow.open(map, Marker);
                });
                TempLocations.push(Marker);
                if (TempLocations.length == NursesLocations.length) {
                    var markerCluster = new MarkerClusterer(map, TempLocations,
                        { imagePath: 'Images/GoogleMap/m' });
                }
            }
        });
    }

</script>

