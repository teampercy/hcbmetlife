﻿<%@ Page Title="Edit Profile" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UserEditProfile.aspx.cs" Inherits="HCB.UserEditProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- BEGIN PAGE HEAD-->
    <%--<div class="page-head">
        <div class="container bg-grey">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1 class="font-blue">PROFILE</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>--%>
    <!-- END PAGE HEAD-->    
    <%--<br />--%>
    <div class="portlet box blue" >
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Update User Profile
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-7">
                            <asp:Label ID="lblMsg" runat="server" Style="display: none" Font-Bold="true" Font-Names="Arial"
                                Font-Size="10pt" ForeColor="Red"></asp:Label><br />
                            <asp:ValidationSummary ID="validateUserSummary" CssClass="font-red bold font-sm text-left" HeaderText="Summary"
                                DisplayMode="BulletList" ValidationGroup="validateUser" runat="server" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">User Name(Email Address)</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtUserName" runat="server" ValidationGroup="validateUser" CssClass="form-control"
                                 Enabled="true"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RQFtxtUserName" ToolTip="User Email Required" runat="server"
                                ControlToValidate="txtUserName" Display="Dynamic" CssClass="font-red bold font-sm" ValidationGroup="validateUser"
                                ErrorMessage="User Email Required">
                           <img src='images/Exclamation.jpg'  alt="User Email Required">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ToolTip="Invalid Email address" ID="RGEtxtUserName"
                                ControlToValidate="txtUserName" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                CssClass="font-red bold font-sm" ValidationGroup="validateUser" runat="server" ErrorMessage="Invalid Email address">  <img src='images/Exclamation.jpg' alt="Invalid Email address"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Name</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="50" Enabled="true">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RQFtxtName" runat="server" ControlToValidate="txtName"
                                Display="Dynamic" CssClass="font-red bold font-sm" ValidationGroup="validateUser" ErrorMessage="Name Required"
                                ToolTip="Name Required">
                           <img src='images/Exclamation.jpg' alt="Name Required">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtName"
                                ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid Name"
                                ValidationGroup="validateUser"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Password</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control"
                                 MaxLength="15" Enabled="true">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RQFtxtPassword" runat="server" ControlToValidate="txtPassword"
                                Display="Dynamic" CssClass="font-red bold font-sm" ValidationGroup="validateUser" ErrorMessage="Password is Required"
                                ToolTip="Password is Required">
                           <img src='images/Exclamation.jpg' alt="Password is Required">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="REGtxtPassword" Display="Dynamic" ValidationExpression="(?=^.{7,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_\d])(?=^.*[^\s].*$).*$"
                                runat="server" ControlToValidate="txtPassword" ValidationGroup="validateUser"
                                ErrorMessage="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character."
                                ToolTip="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character.">
                                                         <img src='images/Exclamation.jpg' alt="Password must be greater than 6 letters and must contain at least one Alphanumeric, Uppercase and Lowercase character. ">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Address</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine"
                                Height="109px" Enabled="true">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RQFtxtAddress" runat="server" ControlToValidate="txtAddress"
                                Display="Dynamic" CssClass="font-red bold font-sm" ValidationGroup="validateUser" ErrorMessage="Address Required"
                                ToolTip="Address Required">
                           <img src='images/Exclamation.jpg' alt="Address Required">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAddress"
                                ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$" ErrorMessage="Invalid Address"
                                ValidationGroup="validateUser"></asp:RegularExpressionValidator>
                            <%-- ^[a-zA-Z'.\s]{1,50}$ --%>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">City</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" Enabled="true">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RQFtxtCity" runat="server" ControlToValidate="txtCity"
                                Display="Dynamic" CssClass="font-red bold font-sm" ValidationGroup="validateUser" ErrorMessage="City Required"
                                ToolTip="City Required">
                           <img src='images/Exclamation.jpg' alt="City Required">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCity"
                                ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid City"
                                ValidationGroup="validateUser"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Telephone Number</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtTeleNum" runat="server" CssClass="form-control" MaxLength="12"
                                onkeypress="return CheckValues(event.keyCode, event.which,this);" Enabled="true">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RQFtxtTeleNum" runat="server" ControlToValidate="txtTeleNum"
                                Display="Dynamic" CssClass="font-red bold font-sm"
                                ValidationGroup="validateUser" ErrorMessage="TelePhone No. Required" ToolTip="TelePhone No. Required">
                           <img src='images/Exclamation.jpg' alt="TelePhone No. Required">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Nominated Nurse</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlNomiNurse" runat="server" Enabled="true" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-9">
                            <%--<button type="submit" class="btn btn-circle green">Submit</button>
                            <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>--%>
                            <asp:Button ID="btnSave" ValidationGroup="validateUser" CausesValidation="true" runat="server" CssClass="btn btn-circle green"
                                Text="Save" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
</asp:Content>
