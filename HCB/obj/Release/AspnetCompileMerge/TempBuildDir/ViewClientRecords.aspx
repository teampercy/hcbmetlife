﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ViewClientRecords.aspx.cs" Inherits="HCB.ViewClientRecords" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <%-- <link id="Link1" rel="Stylesheet" href="Styles/Slate/plugin.css" type="text/css"
        media="screen" title="slate1" runat="server" />
    <script type="text/javascript" src="Scripts/Slate/jquery.1.4.2.min.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.js"></script>
    <script type="text/javascript" src="Scripts/Slate/slate.portlet.js"></script>
    <script type="text/javascript" src="Scripts/Slate/plugin.js"></script>--%>
    <%--<script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            slate.init();
            slate.portlet.init();
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<script type="text/javascript">

        function CallClientDet() {

        }

    </script>--%>
    <!-- BEGIN PAGE HEAD-->
    <%--<div class="page-head">
        <div class="container bg-grey">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1 class="font-blue">
                    <asp:Label ID="lblheadTitle" runat="server"></asp:Label>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>--%>
    <!-- END PAGE HEAD-->
   
    <%--<br />--%>
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </div>
        </div>

        <div class="portlet-body">
            <div class="portlet-content font-md">
                <table class="table table-striped table-bordered table-hover table-header-fixed" rel="datatable" id="sample_2">
                    <thead>
                        <tr class="font-white bg-green-steel">
                            <th>Ref No</th>
                            <th>Client Name</th>
                            <th>Date Of Birth</th>
                            <th>Claim Assessor</th>
                            <th>Manager</th>
                        </tr>
                    </thead>                    
                    
                    <tbody>
                        <asp:Repeater ID="rptResults" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBReference") %>'
                                            OnCommand="lnk_Command" CommandName="HCBRef" />
                                    </td>
                                    <td>
                                        <%# Eval("ClientName")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClientDOB")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClaimHandlerName")%>
                                    </td>
                                    <td>
                                        <%# Eval("NurseName")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("RefNo") %>' CommandArgument='<%# Eval("HCBReference") %>'
                                            OnCommand="lnk_Command" CommandName="HCBRef" />
                                    </td>
                                    <td>
                                        <%# Eval("ClientName")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClientDOB")%>
                                    </td>
                                    <td>
                                        <%# Eval("ClaimHandlerName")%>
                                    </td>
                                    <td>
                                        <%# Eval("NurseName")%>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <div id="wassup" runat="server">
                </div>
            </div>
        </div>

    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
    
</asp:Content>
