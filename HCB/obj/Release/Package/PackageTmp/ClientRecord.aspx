﻿<%@ Page Title="Client Record" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ClientRecord.aspx.cs" Inherits="HCB.ClientRecord" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/Validation.js" language="javascript" type="text/javascript"></script>

    <%-- <link rel="stylesheet" href="Styles/bootstrap-3.1.1.min.css" type="text/css" />--%>
    <%--<link rel="stylesheet" href="Styles/bootstrap-multiselect.css" type="text/css" />--%>
    <%--  <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="js/bootstrap-2.3.2.min.js"></script>--%>
    <%--<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>--%>
    <%--    <link href="Styles/GridView.css" rel="stylesheet" />
    <link href="Styles/Site.css" rel="stylesheet" />
    <link href="Styles/Styles.css" rel="stylesheet" />--%>
    <style type="text/css">
        .CheckboxTextSpace label {
            padding-left: 5px;
        }
        /*CSS for show bootstrap moda popup in screen center*/
        .modal {
        }

        .vertical-alignment-helper {
            display: table;
            height: 100%;
            width: 100%;
        }

        .vertical-align-center {
            /* To center vertically */
            display: table-cell;
            vertical-align: middle;
        }

        .modal-content {
            /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
            width: inherit;
            height: inherit;
            /* To center horizontally */
            margin: 0 auto;
        }
    </style>
    <%--<style type="text/css">
        .CheckboxTextSpace label {
            padding-left: 5px;
        }

        #backgroundPopup1 {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6*/
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: #000000;
            border: 1px solid #cecece;
            z-index: 4;
        }

        .popupContact {
            display: none;
            position: fixed;
            _position: absolute; /* hack for internet explorer 6 height: 200px;*/
            width: 700px;
            background: #FFFFFF;
            border: 2px solid #cecece;
            z-index: 5;
            font-size: 13px;
        }

            .popupContact tr {
                height: 40px;
                text-align: right;
                vertical-align: text-top;
            }

        .popupContactClose {
            font-size: 14px;
            line-height: 14px;
            right: 6px;
            top: 4px;
            position: absolute;
            color: #6fa5fd;
            font-weight: 700;
            display: block;
        }

        #overlay {
            position: fixed;
            z-index: 9;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=80);
            opacity: 0.80;
            -moz-opacity: 0.80;
        }
    </style>--%>
    <%--        <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        function SetSchemeNumber() {
            debugger;
            var SchemeName = document.getElementById('<%=ddlSchemeName.ClientID%>');
            if (SchemeName.value != "0") {
                //alert(locationid );
                $.ajax({
                    type: "POST",
                    url: "ClientRecord.aspx/SetSchemeNumber",
                    data: '{ID: "' + SchemeName.value + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        debugger;
                        if (response.d != "") {
                            document.getElementById('<%=txtSchemeNumber.ClientID%>').value = response.d;
                        }
                        else {
                            document.getElementById('<%=txtSchemeNumber.ClientID%>').value = "";
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                document.getElementById('<%=txtSchemeNumber.ClientID%>').value = "";
            }
            return false;
        }
        $(document).ready(function () {



            $('#<%=btnUpload.ClientID%>').click(function () {

                document.getElementById('<%=hdnServiceRequired.ClientID%>').value = $('#chkSer').val();

            });

            $('#<%=btnRemoveDoc.ClientID%>').click(function () {

                document.getElementById('<%=hdnServiceRequired.ClientID%>').value = $('#chkSer').val();

            });

            var brokervalue = $('#<%=ddlAssessorTeam.ClientID %>').val();
            var deferredvalue = $('#<%=ddlWaitingPeriod.ClientID %>').val();
            var injuryvalue = $('#<%=ddlInjury.ClientID %>').val();
            var reasonvalue = $('#<%=ddlClaimClosedReason.ClientID %>').val();
            if (brokervalue == "14") {
                $('#trteamassessor').fadeIn("slow");
            }
            else {
                $('#trteamassessor').fadeOut("slow");
            }

            if (deferredvalue == "11600") {

                $('#trDeferredPeriod').fadeIn("slow");
            }
            else {

                $('#trDeferredPeriod').fadeOut("slow");
            }

            if (injuryvalue == "13980") {   //Sudhakar 12-9-17 change value from 12588 to 13980 for ddlInjury dropdown

                $('#trillnessinjury').fadeIn("slow");
            }
            else {

                $('#trillnessinjury').fadeOut("slow");
            }

            if (reasonvalue == "12586") {

                $('#trclaimreason').fadeIn("slow");
            }
            else {

                $('#trclaimreason').fadeOut("slow");
            }
            var roleID = '<%= Session["UserType"]%>'
            if (roleID == "Inco") {
                var x = document.getElementById("chkSer");
                x.disabled = false;
            }
            else {
                var x = document.getElementById("chkSer");
                x.disabled = true;
            }
            $('#chkSer').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 0
            });
            // $("#multiselect").multiselect('dataprovider', dataService);
            $('#btnget').click(function () {
                alert($('#chkSer').val());
            });
            debugger;
            $.ajax({
                type: "POST",
                url: "ClientRecord.aspx/BindDatatoDropdownService1",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function OnPopulateControl(response) {
                    debugger;
                    list = response.d;
                    if (list.length > 0) {
                        //$select.multiselect('enable');
                        //$("#province").empty().append('<option value="0">Please select</option>');
                        $.each(list, function () {
                            $("#chkSer").append($("<option ></option>").val(this['Value']).html(this['Text']));
                        });
                        var arrayser = new Array();
                        debugger;

                        var servicevalue = $('#<%=hdnpostbackservicerequired.ClientID%>').val();
                        if (servicevalue != "") {
                            arrayser = document.getElementById('<%=hdnpostbackservicerequired.ClientID%>').value.split(",");
                        }
                        else {
                            arrayser = document.getElementById('<%=hdnServiceRequired.ClientID%>').value.split(",");
                        }
                        i = 0;
                        var dataservicerequired = new Array();
                        for (i; i < arrayser.length; i++) {
                            //selectedarray.push(''+arrayser[i]+'');
                            //dataservicerequired.push("5");
                            //dataservicerequired.push("6");
                            dataservicerequired.push('' + arrayser[i] + '');
                            if (arrayser[i] == "11595") {
                                $('#trservicerequired').fadeIn("slow");
                            }
                            else {
                                $('#trservicerequired').fadeOut("slow");
                            }
                            debugger;
                            //dataservicerequired.push(''+6+'');
                        }
                        $("#chkSer").val(dataservicerequired);
                        //debugger;

                        //showservicerequired();

                    }
                    else {
                        $("#chkSer").empty().append('<option selected="selected" value="0">Not available<option>');
                    }
                    $("#chkSer").multiselect('rebuild'); //refresh the select here
                },
                error: function () {
                    alert("Error");
                }
            });
            // debugger;
            // showservicerequired();

        });
    </script>
    <script type="text/javascript">



        function showcommonoption(data) {

            debugger;
            var value1 = $(data).val();
            var ojectname = $(data).attr('ID');
            ojectname = ojectname.substring(12);
            if (ojectname == "ddlAssessorTeam") {



                if (value1 != null) {
                    if (value1 == "14") {

                        $('#trteamassessor').fadeIn("slow");
                    }
                    else {
                        $('#trteamassessor').fadeOut("slow");
                    }

                }
            }
            if (ojectname == "ddlWaitingPeriod") {

                if (value1 != null) {
                    if (value1 == "11600") {

                        $('#trDeferredPeriod').fadeIn("slow");
                    }
                    else {
                        $('#trDeferredPeriod').fadeOut("slow");
                    }

                }

            }
            if (ojectname == "ddlInjury") {


                if (value1 != null) {
                    if (value1 == "13980") {    //Sudhakar 12-9-17 change value 12588 to 13980 for other item selection in dropdown ddlInjury

                        $('#trillnessinjury').fadeIn("slow");
                    }
                    else {
                        $('#trillnessinjury').fadeOut("slow");
                    }

                }
            }
            if (ojectname == "ddlClaimClosedReason") {

                if (value1 != null) {
                    if (value1 == "12586") {

                        $('#trclaimreason').fadeIn("slow");
                    }
                    else {
                        $('#trclaimreason').fadeOut("slow");
                    }

                }
            }
        }

        function showservicerequired() {
            debugger;
            var value1 = $('#chkSer').val();


            //  var array1;
            // array1 = $('#chkSer').val().split(",");
            i = 0;
            if (value1 != null) {
                for (i; i < value1.length; i++) {

                    if (value1[i] == "11595") {
                        $('#trservicerequired').fadeIn("slow");
                    }
                    else {
                        $('#trservicerequired').fadeOut("slow");
                    }
                }
            }
            if (value1 == null) {

                $('#trservicerequired').fadeOut("slow");

            }
        }

        function SaveHomeVisit() {
            //abc
        }
        function CalculateCaseCost() {
            debugger;
            //alert('hi');
            if (document.getElementById('<%=txtRate.ClientID%>').value != '' && document.getElementById('<%=txtNoofHour.ClientID%>').value != '') {
                document.getElementById('<%=txtCaseMgntCost.ClientID%>').value = parseFloat(parseFloat(document.getElementById('<%=txtRate.ClientID%>').value).toFixed(2) * parseFloat(document.getElementById('<%=txtNoofHour.ClientID%>').value).toFixed(2)).toFixed(2)
            }
            else { document.getElementById('<%=txtCaseMgntCost.ClientID%>').value = ''; }
            return false;
        }
        function ValidateCaseMgnt() {
            debugger;
            //alert(document.getElementById('<%=ddlCaseMgntapproved.ClientID%>').value);
            if (document.getElementById('<%=ddlCaseMgntapproved.ClientID%>').value == '1') {
                if (document.getElementById('<%=txtCaseMgntFundingLimit.ClientID%>').value == '') {
                    alert('Case Management Funding Limit cannot be blank');
                    txtCaseMgntFundingLimit.focus();
                    return false;
                }
            }
            return true;
        }
        function DisplayMsgCaseMgnt() {
            alert('Case Management saved successfully.');
        }
    </script>
    <script type="text/javascript">
        //var popupStatus = 0;
        var popupDivVal = '';
        function loadPopup(popupDiv) {

            popupDivVal = popupDiv;
            if (popupDivVal == "#divServiceRequired") {
                $('#divServiceRequired').modal('show');
            }
            else if (popupDivVal == "#divHomeVisit") {
                $('#divHomeVisit').modal('show');
            }
            else if (popupDivVal == "#divTelephone") {
                $('#divTelephone').modal('show');
            }
            else if (popupDivVal == "#divCaseManagementCost") {
                $('#divCaseManagementCost').modal('show');
            }
            else if (popupDivVal == "#divRehabilitation") {
                $('#divRehabilitation').modal('show');
            }

            //alert(popupDivVal);
            //loads popup only if it is disabled

            //if (popupStatus == 0) {

            //    centerPopup(popupDiv);
            //    $("#backgroundPopup1").css({
            //        "opacity": "0.7"
            //    });

            //    $("#backgroundPopup1").fadeIn("slow");
            //    $(popupDiv).fadeIn("slow");
            //    popupStatus = 1;
            //}

            // window.scrollTo(0, document.body.clientHeight);


            return false;
        }
        //        $("#backgroundPopup1").click(function () {

        //            disablePopup(popupDivVal);
        //        });
        //function disablePopup(popupdiv) {

        //    //disables popup only if it is enabled

        //    if (popupStatus == 1) {

        //        $("#backgroundPopup1").fadeOut("slow");
        //        $(popupdiv).fadeOut("slow");
        //        popupStatus = 0;
        //    }
        //}
        //function centerPopup(popupdiv) {

        //    //request data for centering
        //    // alert('loadPopup');
        //    // alert(popupDiv);
        //    var windowWidth = document.documentElement.clientWidth;
        //    var windowHeight = document.documentElement.clientHeight;
        //    var popupHeight = $(popupdiv).height();
        //    var popupWidth = $(popupdiv).width();
        //    //var popupHeight = 400;
        //    //var popupWidth = 400;
        //    //centering  
        //    $(popupdiv).css({
        //        "position": "absolute",
        //        "top": (windowHeight / 2 - popupHeight / 2) + $(window).scrollTop(),
        //        "left": windowWidth / 2 - popupWidth / 2
        //    });
        //    //only need force for IE6  

        //    $("#backgroundPopup1").css({
        //        "height": windowHeight
        //    });
        //    return false;
        //}
        function closepopup() {

            if (popupDivVal == '#divHomeVisit') {
                //disablePopup('#divHomeVisit');
                $('#divHomeVisit').modal('hide');

            }
            else if (popupDivVal == '#divTelephone') {
                document.getElementById('<%=hdnTelephoneActivityId.ClientID%>').value = "";
                //disablePopup('#divTelephone');
                $('#divTelephone').modal('hide');

            }
            else if (popupDivVal == '#divCaseManagementCost') {
                document.getElementById('<%=hdbCaseManagementCostId.ClientID%>').value = "";
                //disablePopup('#divCaseManagementCost');
                $('#divCaseManagementCost').modal('hide');
            }
            else if (popupDivVal == '#divRehabilitation') {
                document.getElementById('<%=hdnRehabilitationId.ClientID%>').value = "";
                //disablePopup('#divRehabilitation');
                $('#divRehabilitation').modal('hide');
            }
            else if (popupDivVal == '#divServiceRequired') {
                document.getElementById('<%=lblServiceRequired.ClientID%>').value = "";
                //disablePopup('#divServiceRequired');
                    $('#divServiceRequired').modal('hide');
                }
    $(".modal-backdrop").remove();
}
function DeleteItem() {
    //popupStatus = 0;

    document.getElementById('<%=lslFailedCallPop.ClientID%>').remove((document.getElementById('<%=lslFailedCallPop.ClientID%>').value - 1));
    return false;
}
function AddListFailedCall() {
    debugger;
    var hdnFailedCallList = document.getElementById('<%=hdnFailedCallList.ClientID%>');
    var sel = document.getElementById('<%=lslFailedCallPop.ClientID%>');
    var listLength = sel.options.length;
    for (var i = 0; i < listLength; i++) {
        hdnFailedCallList.value += sel.options[i].text + ',';
        //document.getElementByID("list2").add(new Option(sel.options[i].value));
    }

}
function AddItem() {
    // Create an Option object
    //popupStatus = 0;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = dd + '/' + mm + '/' + yyyy + ' ' + today.getHours() + ':' + today.getMinutes();

    var opt = document.createElement("option");

    //  Add an Option object to Drop Down/List Box
    opt.text = today;

    var intcount;
    intcount = document.getElementById('<%=lslFailedCallPop.ClientID%>').length;
    opt.value = intcount + 1;
    document.getElementById('<%=lslFailedCallPop.ClientID%>').options.add(opt);

    //popupStatus = 1;
    return false;
    // Assign text and value to Option object
    //opt.text = today;
    //opt.value = today;



}
    </script>
    <script type="text/javascript">
        function SetHeightDiv()
        { }
        function telephonepopupopen() {
            //alert('hi');
            popupDivVal = '';
            popupStatus = 0;

            //popupDivVal
            PopupAddNew('#divTelephone');
            return false;
        }
    </script>
    <script type="text/javascript">

        function PopupAddNew(id) {
            debugger;
            if (id == '#divHomeVisit') {
                document.getElementById('<%=txtDateOfAppointmentPop.ClientID%>').value = '';
                document.getElementById('<%=txtDateReportCompPop.ClientID%>').value = '';
                document.getElementById('<%=txtFeeChargedPop.ClientID%>').value = '';
                document.getElementById('<%=ddlTypeOfVisitPop.ClientID%>').value = '0';
                //alert()
                // document.getElementById('<%=ddlTypeOfVisitPop.ClientID%>').selectedValue = '0';
                // $("#ddlTypeOfVisitPop").val('0')
                document.getElementById('<%=hdnVisitId.ClientID%>').value = '0';

            }
            else if (id == '#divTelephone') {

                //$("#lslFailedCallPop").empty();

            }
            else if (id == '#divCaseManagementCost') {
                document.getElementById('<%=txtCaseMgntDate.ClientID%>').value = '';
                document.getElementById('<%=txtNoofHour.ClientID%>').value = '';
                document.getElementById('<%=txtRate.ClientID%>').value = '';
                document.getElementById('<%=txtCaseMgntCost.ClientID%>').value = '';
            }
            else if (id == '#divRehabilitation') {
                document.getElementById('<%=ddlRehabType.ClientID%>').value = '0';
                document.getElementById('<%=ddlRehabProvider.ClientID%>').value = '0';
                document.getElementById('<%=txtDateInstructed.ClientID%>').value = '';
                document.getElementById('<%=txtDateReported.ClientID%>').value = '';
                document.getElementById('<%=txtRehabFeeCharged.ClientID%>').value = '';
            }
    loadPopup(id);

    return false;
}
function ConfirmDelete(str) {
    var v = confirm(' You sure want to delete ' + str + ' ?');
    alert(v);
    return false;
}
    </script>
    <script language="javascript" type="text/javascript">
        function Call1AbortedChanged() {

            var flag;
            if (document.getElementById('<%=txtCall1Date.ClientID %>').value.length == 0 || document.getElementById('<%=txtCall1Time.ClientID %>').value.length == 0) {
                alert("Call 1 Date or Time not provided.");
                flag = false;
            }

            //return flag;
        }

        function Call2AbortedChanged() {
            var flag;

            return flag;

        }
        function Call3AbortedChanged() {
            var flag;

            return flag;

        }

        function validatesave() {
            debugger;
            var str;

            var Name = document.getElementById('<%=txtClientFName.ClientID %>');
            if (Name.value == "") {
                str = "Claimant Name is required";
                alert(str);
                Name.focus();
                return false;
            }

            var claimantdob = document.getElementById('<%=txtClientDOB.ClientID %>');
            if (Trim(claimantdob.value) == "") {
                str = "Claimant Date of Birth is required";
                alert(str);
                claimantdob.focus();
                return false;
            }
            else {
                str = validateDate(claimantdob)
                if (str == "ok")
                { }
                else {
                    alert(str);
                    claimantdob.focus();
                    return false;
                }
            }

            var PolicyNum = document.getElementById('<%=txtPolicyNumber.ClientID %>');
            if (Trim(PolicyNum.value) == "") {
                str = "Claim Reference number is required";
                alert(str);
                PolicyNum.focus();
                return false;
                //if (PolicyNum.is(':disabled') == true) {
                //    return false;
                //}
                //else {
                //    PolicyNum.focus();
                //    return false;
                //}
            }

            // debugger;
            var LogUserType = $('#<%=hidLogUserType.ClientID %>');
            if (LogUserType != null) {
                if (LogUserType.val() == "Admin") {
                    var DateNurse = $('#<%=txtDateSentToNurse.ClientID %>');
                    if (DateNurse != null) {
                        if (DateNurse.val() == "") {
                            str = "Date Sent to Nurse field cannot be blank";
                            alert(str);
                            DateNurse.focus();
                            return false;
                        }
                    }
                }
            }


            //

            <%--var ICD9 = document.getElementById('<%=txtICD9Code.ClientID %>');
            var Msg = "Please enter ICD9 code in proper format i.e. 000.00,000.00 etc ";
            if (ICD9 != null) {
                if (ICD9.value != "") {
                    var result = ICD9.value.split(",");
                    if (result.length > 0) {
                        for (var j = 0; j < result.length; j++) {
                            var strnew = result[j];
                            strnew = strnew.trim();
                            var regExp = /^\d+(\.\d{0,9})?$/;
                            var Input = strnew;
                            if (!regExp.test(Input)) {
                                alert("Please enter numeric value in ICD9 code in proper format i.e. 000.00,000.00 etc ");
                                //ICD9.value = "";
                                ICD9.focus();
                                return false;
                            }
                            else {
                                if (strnew.length != 6) {
                                    alert(Msg);
                                    //ICD9.value = "";
                                    ICD9.focus();
                                    return false;
                                }
                                else {
                                    var splitnew = strnew.split(".");
                                    if (splitnew.length > 1) {
                                        if (splitnew[0].length != 3 || splitnew[1].length != 2) {
                                            alert(Msg);
                                            // ICD9.value = "";
                                            ICD9.focus();
                                            return false;
                                        }
                                    }
                                    else {
                                        alert(Msg);
                                        //ICD9.value = "";
                                        ICD9.focus();
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                }
            }--%>
            //

            var EndBenef = document.getElementById('<%=txtEndBenefit.ClientID %>');
            if (EndBenef != null) {
                if (EndBenef.value != "") {
                    str = validateDate(EndBenef)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }




            var IncapDate = document.getElementById('<%=txtIncapacityDay.ClientID %>');
            debugger;
            if (IncapDate != null) {
                if (IncapDate.value != "") {
                    str = validateDate(IncapDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
                else {
                    str = "Date First Absent field cannot be blank";
                    alert(str);
                    IncapDate.focus();
                    return false;
                }
            }
            var ddlInjury = document.getElementById('<%=ddlInjury.ClientID%>');
            if (ddlInjury != null) {
                debugger;
                //alert($('option:selected', $('#ddlInjury')).index());
                if (document.getElementById('<%=ddlInjury.ClientID %>').value == "0") {
                    str = "Illness or Injury field cannot be blank";
                    alert(str);
                    ddlInjury.focus();
                    return false;
                }
            }
            var ClaimCloseDate = document.getElementById('<%=txtClaimCloseDate.ClientID %>');

            if (ClaimCloseDate != null) {
                if (ClaimCloseDate.value != "") {
                    str = validateDate(ClaimCloseDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var ApptDate = document.getElementById('<%=txtAppointmentDate.ClientID %>');
            if (ApptDate != null) {
                if (ApptDate.value != "") {
                    str = validateDate(ApptDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var ReportCompDate = document.getElementById('<%=txtDateReportComp.ClientID %>');
            if (ReportCompDate != null) {
                if (ReportCompDate.value != "") {
                    str = validateDate(ReportCompDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var Call1 = document.getElementById('<%=txtCall1Date.ClientID %>');
            if (Call1 != null) {
                if (Call1.value != "") {
                    str = validateDate(Call1)
                    if (str == "ok") {

                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }





            var RTWDate1 = document.getElementById('<%=txtRTWDateOptimum.ClientID %>');
            if (RTWDate1 != null) {
                if (RTWDate1.value != "") {
                    str = validateDate(RTWDate1)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var NxtSchDate1 = document.getElementById('<%=txtNxtCallSchd.ClientID %>');
            if (NxtSchDate1 != null) {
                if (NxtSchDate1.value != "") {
                    str = validateDate(NxtSchDate1)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }





            var NurseDate = document.getElementById('<%=txtDateSentToNurse.ClientID %>');
            if (NurseDate != null) {
                if (NurseDate.value != "") {
                    str = validateDate(NurseDate)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var CaseClose = document.getElementById('<%=txtCaseClosed.ClientID %>');
            if (CaseClose != null) {
                if (CaseClose.value != "") {
                    str = validateDate(CaseClose)
                    if (str == "ok") {
                    }
                    else {
                        alert(str);
                        return false;
                    }
                }
            }

            var claimhandlerphn = document.getElementById('<%=txtClaimHandlerTelephone.ClientID %>');
            if (Trim(claimhandlerphn.value) == "") {
                str = " Claim Handler phone number is required";
                alert(str);
                return false;
            }

            var Email = document.getElementById('<%=txtClaimHandlerEmail.ClientID %>');
            if (Trim(Email.value) == "") {
                str = "Email is required";
                alert(str);
                return false;
            }
            else {
                str = emailCheck(Email.value)
                if (str == "ok") {
                }
                else {
                    alert(str);
                    return false;
                }
            }
            var teamPhn = document.getElementById('<%=txtTeamTelephone.ClientID %>');
            if (Trim(teamPhn.value) == "") {
                str = "Assessor Team phone number is required";
                alert(str);
                return false;
            }

            var clientaddress1 = document.getElementById('<%=txtAddress1.ClientID %>');
            if (Trim(clientaddress1) == "") {
                str = "Address1 is required";
                alert(str);
                return false;
            }

            var teamEmail = document.getElementById('<%=txtTeamEmail.ClientID %>');
            if (Trim(teamEmail.value) == "") {
                str = "Email is required";
                alert(str);
                return false;
            }
            else {
                str = emailCheck(teamEmail.value)
                //                if (str == "ok") {

                //                }
                //                else {
                //                    //alert(str);
                //                    return false;
                //                }
            }
            document.getElementById('<%=hdnServiceRequired.ClientID%>').value = $('#chkSer').val();
            var servReqd = $('#chkSer').val();

            if (servReqd == null || Trim(servReqd) == "") {
                str = "Please select atleast one Service Required";
                alert(str);
                return false;
            }
            //alert(document.getElementById('<%=ddlDocuments.ClientID %>').options.length);
            if (document.getElementById('<%=ddlDocuments.ClientID %>').options.length < 2) {
                alert("YOU HAVE NOT UPLOADED THE INSTRUCTION FORM. PLEASE UPLOAD THE INSTRUCTION FORM");
                return false;
            }
            debugger;



            return true;
        }



        function PreNoteSaveCheck() {
            debugger;
            var blnTest = false
            var NoteVal = $('#<%=txtAddNote.ClientID %>').val();

            if (NoteVal == "Type your note here.  Once finished click the 'Save Note' button.")
            { blnTest = true };

            if (NoteVal == "")
            { blnTest = true };

            if (blnTest == true) {
                alert('A valid note is required before save!');
                return false;
            }
            return true;
        }

        function numberOnly(txt, e) {
            //debugger;
            var arr = '0123456789.';
            var code;
            var code1;
            if (window.event) {
                code = e.keyCode;
            }
            else {
                code = e.which;
            }

            var cha = String.fromCharCode(code);
            if (arr.indexOf(cha) == -1) {
                return false;
            }
            else if (cha == '.') {
                if (txt.value.indexOf('.') > -1)
                    return false;
            }
            if (txt.value.indexOf('.') > -1) {
                code1 = txt.value.indexOf('.');
                if (txt.value.length == code1 + 3)
                    return false;
            }
        }

        function validateDate(date) {
            debugger;
            var msg;
            var sValidChars;
            var nCounter;
            var datval = date.value;
            msg = "ok";
            sValidChars = "1234567890/";
            if (Trim(datval) != "") {

                if (datval.length != 10) {
                    msg = " Please enter in exact format (dd/mm/yyyy) e.g. 02/01/2000 in ";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }

                for (nCounter = 0; nCounter < datval.length; nCounter++) {

                    if (sValidChars.indexOf(datval.substring(nCounter, nCounter + 1)) == -1) {

                        msg = " Please enter in exact format (dd/mm/yyyy)";
                        alert(msg);
                        date.value = '';
                        date.focus();
                        nCounter = datval.length;
                        return msg;
                    }

                }
                if (datval.indexOf("/") != 2) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 3) != 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 6) < 10 && datval.indexOf("/", 6) > 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }
                debugger;

                var month;
                var day;
                var year;
                month = parseInt(TrimNumber(datval.substring(3, 5)));
                day = parseInt(TrimNumber(datval.substring(0, 2)));
                year = parseInt(datval.substring(6, 10));

                if (month > 12 || month < 1) {
                    msg = " Invalid Month";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }
                if (year < 1900 || year > 3000) {
                    msg = " Invalid year. Should be in (1900-3000)";
                    alert(msg);
                    date.value = '';
                    date.focus();
                    return msg;
                }
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    if (day > 31 || day < 1) {
                        msg = " Days should not be more than 31 or less than 1 for entered Month";
                        alert(msg);
                        date.value = '';
                        date.focus();
                        return msg;
                    }
                }
                else {
                    if (month == 2 && (year % 4) == 0) {
                        debugger;
                        if (day > 29 || day < 1) {
                            msg = " Days should not be more than 29 or less than 1 for entered Month";
                            alert(msg);
                            date.value = '';
                            date.focus();
                            return msg;
                        }
                    }
                    else if (month == 2 && (year % 4) != 0) {
                        if (day > 28 || day < 1) {
                            msg = " Days should not be more than 28 or less than 1 for entered Month";
                            alert(msg);
                            date.value = '';
                            date.focus();
                            return msg;
                        }
                    }
                    else {
                        if (day > 30 || day < 1) {
                            msg = " Days should not be more than 30 or less than 1 for entered Month";
                            alert(msg);
                            date.value = '';
                            date.focus();
                            return msg;
                        }
                    }
                }
                return msg;

            }
            //msg = "empty";
            return msg;
        }

        function ValidateDOB(dateid) {
            debugger;
            var msg;
            var sValidChars;
            var nCounter;
            var datval = dateid.value;
            msg = "ok";
            sValidChars = "1234567890/";

            if (Trim(datval) != "") {
                if (datval.length != 10) {
                    msg = " Please enter in exact format (dd/mm/yyyy) e.g. 02/01/2000 in ";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                for (nCounter = 0; nCounter < datval.length; nCounter++) {

                    if (sValidChars.indexOf(datval.substring(nCounter, nCounter + 1)) == -1) {

                        msg = " Please enter in exact format (dd/mm/yyyy)";
                        alert(msg);
                        date.focus();
                        nCounter = datval.length;
                        return msg;
                    }

                }

                if (datval.indexOf("/") != 2) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 3) != 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 6) < 10 && datval.indexOf("/", 6) > 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                var month;
                var day;
                var year;
                month = parseInt(TrimNumber(datval.substring(3, 5)));
                day = parseInt(TrimNumber(datval.substring(0, 2)));
                year = parseInt(datval.substring(6, 10));

                if (month > 12 || month < 1) {
                    msg = " Invalid Month";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (year < 1900 || year > 3000) {
                    msg = " Invalid year. Should be in (1900-3000)";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    if (day > 31 || day < 1) {
                        msg = " Days should not be more than 31 or less than 1 for entered Month";
                        alert(msg);
                        date.focus();
                        return msg;
                    }
                }
                else {
                    if (month == 2 && (year % 4) == 0) {
                        if (day > 29 || day < 1) {
                            msg = " Days should not be more than 29 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else if (month == 2 && (year % 4) != 0) {
                        if (day > 28 || day < 1) {
                            msg = " Days should not be more than 28 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else {
                        if (day > 30 || day < 1) {
                            msg = " Days should not be more than 30 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                }
                //return msg;
                $('#<%=btnCheckClient.ClientID %>').click();
                //_doPostBack('btnCheck', 'OnClick');
                // return false;
            }
        }

        function CloseInsurance(id) {
            $('txtClaimRef2').val('');
            $('trInsuranceDet2').hide();
        }



        function GetFailedDet(id) {
            debugger;
            var Faildate;
            var FailTime;
            var FailedDateTime;
            var hidFailCall;
            var Company = false;


            if (id == 1) {
                var d = new Date();

                if ($('#<%=lnkAddFailedCall.ClientID %>').is(':disabled') == false) {
                    if (d != null) {
                        var date = d.format("dd/MM/yyyy hh:mm");
                        //var Time = d.getHours() + ":" + d.getMinutes();
                        var FailDate = date;

                        //$('#<%=lstFailedCalls.ClientID %>').append("<option>" + FailDate + "</option>");

                        var fcall = $.ajax({
                            type: "POST",
                            url: "ClientRecord.aspx/InsertFailCall",
                            data: { FailCall: FailDate, id: 1 },
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data, status, xhr) {
                                Company = data.d;
                                if (Company != 0) {
                                    $('#<%=lstFailedCalls.ClientID %>').append("<option value='" + Company + "'>" + FailDate + "</option>");

                                }
                            },
                            error: function (xhr, status, error) {
                                alert("Error occured" + status);
                            }
                        });
                    }
                }
            }

            return false;
        }

        function validateFaildcall(rid) {
            //debugger;
            if (rid == 1) {
                if ($('#<%=lnkRemoveFailCall1.ClientID %>').is(':disabled') == false) {
                    if ($('#<%=lstFailedCalls.ClientID%>').attr("selectedIndex") < 0) {
                        alert('select a call to delete');
                        $('#<%=lstFailedCalls.ClientID%>').focus();
                        return false;
                    }
                }
            }

            return true;

        }

        function validateFaildcallPop(rid) {

            //debugger;
            if (rid == 1) {
                if ($('#<%=lnkFailedPop.ClientID %>').is(':disabled') == false) {
                    if ($('#<%=lslFailedCallPop.ClientID%>').attr("selectedIndex") < 0) {
                        alert('select a call to delete');
                        $('#<%=lslFailedCallPop.ClientID%>').focus();
                        return false;
                    }
                }
            }
            DeleteItem();
            popupStatus = 1;
            return false;

        }
        function RemoveFailCall(rid) {
            //debugger;
            var lstvalue;
            var HidValue;

            if (rid == 1) {
                if ($('#<%=lnkRemoveFailCall1.ClientID %>').is(':disabled') == false) {
                    lstvalue = $('#<%=lstFailedCalls.ClientID %> option:selected').val();

                    if (lstvalue != "") {

                        var Delfcall = $.ajax({
                            type: "POST",
                            url: "FailedCall.aspx/DeleteFailCall",
                            data: "{'FailID':'" + lstvalue + "'}",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data, status, xhr) {
                                Company = data.d;
                            },
                            error: function (xhr, status, error) {
                                alert("Error occured" + status);
                            }
                        });

                    }
                    $('#<%=lstFailedCalls.ClientID %> option:selected').remove();
                }
            }


            return false;
        }

        function UpdateFailCall(uid) {
            var NewVal;
            var listid;

            if (uid == 1) {
                listid = $('#<%=lstFailedCalls.ClientID %> option:selected').val();
            }
        }
        function deletedocument() {
            //debugger;
            alert('called');
            if (confirm('You sure want delete this file?'))
                return true;
            else
                return false;
        }

        function showupload() {
            debugger;
            document.getElementById('<%= tblDocUpload.ClientID %>').style.display = "block";
            document.getElementById('<%= hidShowDoc.ClientID %>').value = "1";
            return false;
        }
        function Hideupload() {
            //debugger;
            var fileupload = document.getElementById('<%= tblDocUpload.ClientID %>');
            document.getElementById('<%= tblDocUpload.ClientID %>').style.display = "none";
            document.getElementById('<%= hidShowDoc.ClientID %>').value = "0";
            document.getElementById('<%= btnCancle.ClientID %>').checked = false;

            return false;
        }

        function ActiveButton() {
            debugger;
            if ($('#<%=chkSendEmail.ClientID %>').is(':checked')) {
                $('#<%=btnSaveNote.ClientID %>').attr('disabled', false);
            }
            else {
                $('#<%=btnSaveNote.ClientID %>').attr('disabled', true);
            }
        }

        function SaveNotes() {

            debugger;
            var chkSendMail;
            var txtAddNote;
            var ddlCaseMngrSelectedIndex;
            var ddlCaseMngrSelectedValue;
            var txtClaimHandlerEmail;
            var lblNotes;

            // chkSendMail = $('#<%=chkSendEmail.ClientID %>' ).attr('checked');

            chkSendMail = $('#<%=chkSendEmail.ClientID %>').is(":checked");
            txtAddNote = $('#<%=txtAddNote.ClientID %>').val();


            ddlCaseMngrSelectedIndex = $('#<%=ddlCaseMngr.ClientID %>').get(0).selectedIndex;
            ddlCaseMngrSelectedValue = $('#<%=ddlCaseMngr.ClientID %>' + ' option:selected').val();
            txtClaimHandlerEmail = $('#<%=txtClaimHandlerEmail.ClientID %>').val();
            lblNotes = $('#<%=lblNotes.ClientID %>');
            lblNotesInnerHtml = $('#<%=lblNotes.ClientID %>').html();

            $.ajax({
                type: "POST",
                url: "ClientRecord.aspx/SaveNote",
                data: "{'InnerText':'" + lblNotesInnerHtml + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, status, xhr) {
                    //alert("true");

                    //alert("Response from method is : " + data.d);
                    if (data.d == true) {
                        $(lblNotes).append('<br/><br/>' + txtAddNote);
                    }
                    else {
                        alert("Note could not be saved.");
                    }

                    $('#<%=txtAddNote.ClientID %>').val('');
                },
                error: function (xhr, status, error) {
                    alert("Error occured :<br/> Status -" + status + "<br/> Error: " + error.toString());
                }
            });

            return false;
        }

        function DisableDateControls(dateID) {
            //debugger;
            var DateVal;

            if (dateID == 1) {
                DateVal = $('#<%=txtCall1Date.ClientID %>').val();

                if (DateVal != "") {
                    $('#<%=lstFailedCalls.ClientID %>').attr("disabled", "disabled");
                    $('#<%=lnkAddFailedCall.ClientID %>').attr("disabled", "disabled");
                    $('#<%=lnkRemoveFailCall1.ClientID %>').attr("disabled", "disabled");
                }
            }


            return false;
        }

        function CallDateFunctions(date, rid) {
            //debugger;
            validateDate(date);
            DisableDateControls(rid);
            return false;
        }

        function DisableButtons() {
            //debugger;
            $('#<%=lnkAddFailedCall.ClientID %>').attr("disabled", "disabled");
            //$('#<%=lnkAddFailedCall.ClientID %>').attr("OnClientClick").remove();
            $('#<%=lnkRemoveFailCall1.ClientID %>').attr("disabled", "disabled");
            //$('#<%=lnkRemoveFailCall1.ClientID %>').attr("OnClientClick").remove();
        }



        //   function ShowDeferredPeriodOther() {

        //      debugger;
        //    var value = ').val();
        //  if (value == "11600") {

        //    debugger;

        //  $').fadeIn("slow");

        //  }
        //  else
        //    {
        // (').fadeOut("slow");
        //   }

        //} 

    </script>
    <%--<style type="text/css">
        .ButtonClass {
        }

        .modalBackground {
            background-color: #707070;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }

        .auto-style1 {
            width: 108px;
        }

        .auto-style2 {
            width: 158px;
        }

        .auto-style3 {
            height: 29px;
        }

        .auto-style4 {
            width: 158px;
            height: 29px;
        }
    </style>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptMngr" runat="server">
    </asp:ScriptManager>
    <div id="divScroll"></div>

    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Case Details
            </div>

        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">
                    <div class="form-group">
                        <asp:Label ID="lblMsg" runat="server" Font-Bold="true" Font-Names="Arial" Font-Size="12pt"
                            ForeColor="Red"></asp:Label>
                    </div>

                    <h3 class="form-section bold font-blue text-left">HCB References&nbsp; (HCB Use Only)</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">HCB Ref</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtHCBReference" runat="server" autocomplete="off" MaxLength="50"
                                        CssClass="form-control input-medium" ReadOnly="True" TabIndex="1" Enabled="false"
                                        ForeColor="DarkGray" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Case Manager</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlCaseMngr" runat="server" CssClass="form-control input-medium"
                                        TabIndex="2">
                                    </asp:DropDownList>
                                    <img id="img_ddlNurses" src="images/Exclamation.jpg" style="visibility: hidden" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section bold font-blue text-left">MetLife Contact Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Claim Assessor</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlClaimAssessor" runat="server" CssClass="form-control input-medium"
                                        TabIndex="3" OnSelectedIndexChanged="ddlClaimAssessor_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Telephone</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClaimHandlerTelephone" runat="server" MaxLength="12"
                                        CssClass="form-control input-medium" TabIndex="4" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Email Address</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClaimHandlerEmail" runat="server" CssClass="form-control input-large"
                                        TabIndex="5"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Broker</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlAssessorTeam" runat="server" CssClass="form-control input-medium" TabIndex="6" onchange="showcommonoption(this);">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">MetLife Claims phone</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTeamTelephone" runat="server" MaxLength="12" CssClass="form-control input-medium"
                                        TabIndex="7" onkeypress="javascript:return numberOnly(this,event);" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="trteamassessor" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Other</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTeamOther" runat="server" CssClass="form-control input-medium"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">MetLife Claims Team Email</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTeamEmail" runat="server" CssClass="form-control input-large" TabIndex="8" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>


                    <h3 class="form-section bold font-blue text-left">Claimant Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">First Name</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClientFName" runat="server" MaxLength="50" CssClass="form-control input-medium"
                                        TabIndex="9"></asp:TextBox>
                                    <span class="help-block pull-left">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtClientFName"
                                            ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid First Name" Display="Dynamic"
                                            ValidationGroup="validInfo">
                                        </asp:RegularExpressionValidator>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Surname</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClientSurname" runat="server" MaxLength="50" CssClass="form-control input-medium"
                                        TabIndex="10"></asp:TextBox>
                                    <span class="help-block pull-left">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtClientSurname"
                                            ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ErrorMessage="Invalid Surname" Display="Dynamic"
                                            ValidationGroup="validInfo">
                                        </asp:RegularExpressionValidator>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">DOB</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClientDOB" runat="server" MaxLength="10" CssClass="form-control input-medium"
                                        AutoPostBack="true" TabIndex="11" onchange="javascript:return ValidateDOB(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender17" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtClientDOB">
                                    </cc1:MaskedEditExtender>
                                    <asp:Button ID="btnCheckClient" runat="server" Style="display: none" OnClick="btnCheckClient_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Gender</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlGender" runat="server" TabIndex="20" CssClass="form-control input-medium">
                                        <asp:ListItem Value="0" Text="-Select Gender-"></asp:ListItem>
                                        <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                                        <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Address1</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control input-medium" TabIndex="12" MaxLength="200">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tel No.</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClientWorkTelephone" runat="server" MaxLength="12"
                                        CssClass="form-control input-medium" TabIndex="13" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Address2</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control input-medium" TabIndex="14" MaxLength="200">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Mobile No.</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClientMobileTelephone" runat="server" MaxLength="12"
                                        CssClass="form-control input-medium" TabIndex="15" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">City</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control input-medium" TabIndex="16" MaxLength="200">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Service Required</label>
                                <div class="col-md-7 text-left">
                                    <select id="chkSer" multiple="multiple" class="multiselect form-control input-medium" onchange="showservicerequired()">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="trservicerequired" style="display: none">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Other</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtservicerequiredother" runat="server" CssClass="form-control input-medium"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">County</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtCounty" runat="server" CssClass="form-control input-medium" TabIndex="17" MaxLength="200">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Occupation</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtOccupation" runat="server" CssClass="form-control input-medium" TabIndex="18"></asp:TextBox>
                                    <span class="help-block pull-left">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtOccupation"
                                            ForeColor="Red" ValidationExpression="^[a-zA-Z.' ']{1,255}$" ErrorMessage="Invalid Occupation"
                                            ValidationGroup="validInfo">
                                        </asp:RegularExpressionValidator>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Postcode</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClientPostCode" runat="server" CssClass="form-control input-small" MaxLength="10"
                                        TabIndex="19"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtClientPostCode"
                                        ForeColor="Red" ValidationExpression="^[a-zA-Z0-9' ']{1,50}$" ErrorMessage="Invalid PostCode"
                                        ValidationGroup="validInfo">
                                    </asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>

                    <h3 class="form-section bold font-blue text-left">Employer Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Employer Name</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtEmployerName" runat="server" CssClass="form-control input-medium" TabIndex="22"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Employer Contact</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtEmployerContract" runat="server" CssClass="form-control input-medium" TabIndex="23"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Employer Email</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtEmployerEmail" runat="server" CssClass="form-control input-large"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section bold font-blue text-left">MetLife Scheme Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">MetLife Claim Ref.</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtPolicyNumber" runat="server" CssClass="form-control input-medium uppercase"
                                        TabIndex="24" MaxLength="25"></asp:TextBox>
                                    <span class="help-block pull-left">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtPolicyNumber"
                                            ForeColor="Red" ValidationExpression="^[a-zA-Z0-9'']{1,25}$" ErrorMessage="Invalid Claim Ref. Number"
                                            ValidationGroup="validInfo">
                                        </asp:RegularExpressionValidator>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Incapacity Definition</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlIncapacityDefinition" runat="server" CssClass="form-control input-medium" TabIndex="25">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Deferred Period</label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddlWaitingPeriod" runat="server" CssClass="form-control input-medium"
                                        TabIndex="26" onchange="showcommonoption(this);">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-1">
                                    <img id="img1" src="images/Exclamation.jpg" style="visibility: hidden" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">End of Benefit Term</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtEndBenefit" runat="server" CssClass="form-control input-medium" TabIndex="27" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender28" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtEndBenefit">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="trDeferredPeriod" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Other</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDeferredOther" runat="server" CssClass="form-control input-medium"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Monthly Benefit (£)</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtMonthlyBenefit" runat="server" CssClass="form-control input-medium" MaxLength="15"
                                        TabIndex="28" onkeypress="javascript:return numberOnly(this,event)"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Scheme Name</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlSchemeName" runat="server" TabIndex="21" CssClass="form-control input-medium" onchange="javascript:SetSchemeNumber();">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Eligible for EAP?</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlElligibleEAP" runat="server" TabIndex="30" CssClass="form-control input-medium">
                                        <asp:ListItem Value="0" Text="-Select Eligibility-"></asp:ListItem>
                                        <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">MetLife Scheme Number</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtSchemeNumber" runat="server" CssClass="form-control input-medium" TabIndex="29"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section bold font-blue text-left">Claim Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:LinkButton ID="lnkViewServiceRequired" runat="server" CssClass="btn btn-circle blue pull-left" Text="View Service Required" OnClientClick="return PopupAddNew('#divServiceRequired');"></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">
                                    <asp:RequiredFieldValidator ID="ReqValIncapDay" runat="server" ControlToValidate="txtIncapacityDay"
                                        Display="Dynamic" ToolTip="1st Incapacity Day is required" ForeColor="Red" SetFocusOnError="true"
                                        ValidationGroup="validInfo">*</asp:RequiredFieldValidator>
                                    Date First Absent</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtIncapacityDay" runat="server" CssClass="form-control input-medium"
                                        TabIndex="31" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtIncapacityDay">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">
                                    <asp:RequiredFieldValidator ID="ReqValIllness" runat="server" InitialValue="0" ControlToValidate="ddlInjury"
                                        Display="Dynamic" ToolTip="Illness or Injury should be selected" ForeColor="Red"
                                        SetFocusOnError="true" ValidationGroup="validInfo">*</asp:RequiredFieldValidator>
                                    Illness or Injury</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlInjury" runat="server" CssClass="form-control input-medium"
                                        TabIndex="32" onchange="showcommonoption(this);">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="trillnessinjury" style="display: none">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Other</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtillnessinjuryother" runat="server" CssClass="form-control input-medium"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Disability Type</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDisabilityType" runat="server" CssClass="form-control input-large"
                                        TabIndex="33"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label pull-left">(per MDG)</label>
                        </div>
                    </div>

                    <%--<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">ICD9 Code</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtICD9Code" runat="server" CssClass="form-control input-large" TabIndex="34"
                                        MaxLength="40"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label pull-left bold font-red">
                                <font face="Arial" size="2">(Enter ICD9 codes separated
                                                by commas i.e 000.00,000.00 etc.)</font>
                            </label>
                        </div>
                    </div>--%>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Date HCB Group instructed (by MetLife)</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtHCBGroupInstructedDate" runat="server" CssClass="form-control input-medium"
                                        onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender14" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtHCBGroupInstructedDate">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Claim Closed Date</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClaimCloseDate" runat="server" CssClass="form-control input-medium"
                                        TabIndex="35" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender27" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtClaimCloseDate">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Claim Closed Reason</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlClaimClosedReason" runat="server" CssClass="form-control input-medium"
                                        TabIndex="36" onchange="showcommonoption(this);">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="trclaimreason" style="display: none">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Other</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtclaimreasonother" runat="server" CssClass="form-control input-medium"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">
                                    Details<br />
                                    <strong><font size="1">(Max 2500 Chars )</font></strong>
                                </label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtClaimDetails" runat="server" Height="77px" MaxLength="2500" TextMode="MultiLine"
                                        CssClass="form-control input-xlarge" TabIndex="37"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>

    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Case Notes
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">
                    <asp:UpdatePanel ID="UpdPnlNotes" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="lblNotes" runat="server" class="text-left" width="698px" style="overflow: auto; padding-left: 2%; height: 200px">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtAddNote" runat="server" Height="124px" TextMode="MultiLine"
                                            CssClass="form-control" TabIndex="61"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="WaterExtd" runat="server" TargetControlID="txtAddNote"
                                            WatermarkText="Type your note here.  Once finished click the 'Save Note' button.">
                                        </cc1:TextBoxWatermarkExtender>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-actions pull-left">
                                        <asp:Button ID="btnSaveNote" runat="server" CausesValidation="true" OnClientClick="javascript:return PreNoteSaveCheck();"
                                            Text="Save Note" CssClass="btn btn-circle blue" TabIndex="62" OnClick="btnSaveNote_Click" />
                                        &nbsp; &nbsp; &nbsp;<asp:CheckBox ID="chkSendEmail" runat="server" Checked="True"
                                            Text="Send Email Notification" TabIndex="63" Visible="false" />
                                        &nbsp; &nbsp; &nbsp;

                                        <asp:CheckBox ID="chkSendEmailMetLife" runat="server" 
                                            Text="Notify MetLife" TabIndex="83"  CssClass="CheckboxTextSpace"/>
                                        &nbsp; &nbsp; &nbsp;
                                         <asp:CheckBox ID="chkSendEmailHCB" runat="server" 
                                            Text="Notify HCB Admin" TabIndex="84"  CssClass="CheckboxTextSpace"/>
                                        &nbsp; &nbsp; &nbsp;
                                         <asp:CheckBox ID="chkSendEmailCaseManager" runat="server"
                                            Text="Notify Case Manager" TabIndex="85"  CssClass="CheckboxTextSpace"/>
                                        &nbsp; &nbsp; &nbsp;
                                           <asp:CheckBox ID="chkImportantMail" runat="server" Text="Important" TabIndex="64" CssClass="CheckboxTextSpace"/>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSaveNote" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdPnlNotes" runat="server">
                        <ProgressTemplate>
                            <img alt="progress" src="Images/wait.gif" />
                            Processing...
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>HCB Activity
            </div>

        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">

                    <h3 class="form-section bold font-blue text-left">FIRST CONTACT WITH CLAIMANT</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <%--<label class="control-label col-md-5">Date of first contact with Claimant</label>--%>
                                <label class="control-label col-md-5">First contact attempt with Claimant</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtFirstContactClaimantDate" runat="server" CssClass="form-control input-medium"
                                        onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender19" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtFirstContactClaimantDate">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Type Of Contact</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlContactType" runat="server" CssClass="form-control input-medium">
                                        <asp:ListItem Text="--Select Type of Contact--" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Telephone-Home" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Telephone-Mobile" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Email-Home" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Email-Work" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">                                
                                <label class="control-label col-md-5">First verbal contact with Claimant</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtVerbalContactClaimantDate" runat="server" CssClass="form-control input-medium"
                                        onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtVerbalContactClaimantDate">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5"></label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtContactTypeDetails" runat="server" CssClass="form-control input-medium"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Next Review Date</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtNextReviewDate" runat="server" CssClass="form-control input-medium"
                                        onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender20" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtNextReviewDate">
                                    </cc1:MaskedEditExtender>
                                    <asp:HiddenField ID="hdnNextPreviewDate" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnNextPreviewDateEmailSent" runat="server" Value="" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section bold font-blue text-left">TELEPHONE INTERVENTION</h3>
                    <asp:UpdatePanel ID="updcall1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTelephoneIntervention" runat="server">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Date</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtCall1Date" runat="server" CssClass="form-control input-small" TabIndex="43"
                                                    onchange="validateDate(this);DisableDateControls(1);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCall1Date">
                                                </cc1:MaskedEditExtender>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Time</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtCall1Time" runat="server" CssClass="form-control input-xsmall" TabIndex="44"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                    MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtCall1Time">
                                                </cc1:MaskedEditExtender>
                                            </div>
                                            <label class="control-label col-md-3">hours</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Type of Call</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlTypeofCall" runat="server" CssClass="form-control input-medium" TabIndex="45">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Failed Calls</label>
                                            <div class="col-md-7">
                                                <asp:ListBox ID="lstFailedCalls" runat="server" CssClass="form-control input-medium" TabIndex="46"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <asp:LinkButton ID="lnkAddFailedCall" runat="server" Text="Add Failed Call" TabIndex="47"
                                                Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none;"
                                                CommandArgument="1" CommandName="Add" OnClick="lnkFailedCallPage_Click">
                                            </asp:LinkButton>
                                        </div>
                                        <div class="form-group">
                                            <asp:LinkButton ID="lnkRemoveFailCall1" runat="server" Text="Remove Failed Call"
                                                Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none"
                                                CommandArgument="1" CommandName="del" OnClientClick="javascript:return validateFaildcall(1);"
                                                OnClick="lnkFailedCallPage_Click" TabIndex="48">
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Fee Charged</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtFeeCharge" runat="server" TabIndex="49" CssClass="form-control input-medium" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Expected RTW Date(Optimum)</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRTWDateOptimum" runat="server" CssClass="form-control input-medium" TabIndex="50"
                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateOptimum">
                                                </cc1:MaskedEditExtender>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Expected RTW Date (Maximum)</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRTWDateMax" runat="server" CssClass="form-control input-medium" TabIndex="51"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender18" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="MonthDayYear" TargetControlID="txtRTWDateMax">
                                                </cc1:MaskedEditExtender>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Case Management Recommended?</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlCaseMngtRecom" runat="server" TabIndex="52" CssClass="form-control input-medium">
                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-5 display-hide">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Next Call Scheduled</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtNxtCallSchd" runat="server" CssClass="form-control input-medium"
                                                    TabIndex="53" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender9" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtNxtCallSchd">
                                                </cc1:MaskedEditExtender>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-actions">
                                            <asp:Button ID="btnSaveTelephone" runat="server" CssClass="btn btn-circle blue pull-right" Text="Save Telephone intervention" OnClick="btnSaveTelephone_Click" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="pnlGridTelephoneIntervention" runat="server">
                                <br />
                                <asp:LinkButton ID="lnkAddTelephone" runat="server" CssClass="btn btn-circle blue pull-right"> Add New Telephone Intervention</asp:LinkButton>
                                <div class="table-scrollable">
                                    <asp:GridView ID="grdTelephoneIntervention" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-advance"
                                        OnRowDataBound="grdTelephoneIntervention_OnRowDataBound">
                                        <HeaderStyle CssClass="font-white bg-green-steel" HorizontalAlign="Center" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:BoundField DataField="CallDate" HeaderText="Call Date" />
                                            <asp:BoundField DataField="CallCaseMngtRecom" HeaderText="Case Management Recommended" />
                                            <asp:BoundField DataField="TypeofCall" HeaderText="Type of Call" />

                                            <asp:TemplateField HeaderText="Fee Charged" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVisitFee" runat="server" Text='<%# String.Format("{0:n2}",Eval("FeeCharged"))%>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HiddenField Value='<%#Eval("HCBTelephoneID") %>' ID="hdnHCBTelephoneID" runat="server" />

                                                    <asp:LinkButton ID="lnkeditTele" runat="server" CommandName="edt" OnCommand="lnk_CommandGrid" CssClass="btn btn-outline btn-circle btn-sm purple"
                                                        CommandArgument='<% #Eval("HCBTelephoneID")%>'><i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>

                                                    <%--<asp:ImageButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("HCBTelephoneID")%>'
                                                        OnClientClick="return confirm('You want to delete this Telephone Activity ?');" OnCommand="lnk_CommandGrid"
                                                        CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />--%>
                                                    <asp:LinkButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("HCBTelephoneID")%>'
                                                        OnClientClick="return confirm('You want to delete this Telephone Activity ?');" OnCommand="lnk_CommandGrid"
                                                        CommandName="del" CssClass="btn btn-outline btn-circle dark btn-sm black">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID="upHomeVisits" runat="server">
                        <ProgressTemplate>
                            <img alt="progress" src="Images/wait.gif" />
                            Processing...
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <h3 class="form-section bold font-blue text-left">VISIT</h3>
                    <%--<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Date Claimant Contacted</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtclaimantfirstcontacted" runat="server" CssClass="form-control input-medium" TabIndex="38"
                                        onchange="javascript:return validateDate(this);"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Mask="99/99/9999"
                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtclaimantfirstcontacted">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>--%>

                    <asp:UpdatePanel ID="upHomeVisits" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlHomeVisit" runat="server">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Date of Appointment</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtAppointmentDate" runat="server" CssClass="form-control input-medium"
                                                    TabIndex="39" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtAppointmentDate">
                                                </cc1:MaskedEditExtender>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Date Report Completed</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtDateReportComp" runat="server" CssClass="form-control input-medium"
                                                    TabIndex="40" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateReportComp">
                                                </cc1:MaskedEditExtender>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Type Of Visit</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlTypeOfVisit" runat="server" CssClass="form-control input-medium" TabIndex="41">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Fee Charged (£)</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtFeeChargeHomeVisit" CssClass="form-control input-medium" runat="server" onkeypress="javascript:return numberOnly(this,event);" TabIndex="42" Width="171px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-actions">
                                            <asp:Button ID="btnSaveVisit" runat="server" Text="Save Visit" OnClick="btnSaveVisit_Click" CssClass="btn btn-circle blue pull-right" /></td>
                                        </div>
                                    </div>
                                </div>
                                <%--</div>--%>
                            </asp:Panel>

                            <asp:Panel ID="pnlGRIDHomeVisit" runat="server">
                                <div class="table-scrollable" id="tbGRIDHomeVisit" runat="server">
                                    <br />
                                    <asp:LinkButton ID="lnkAdd" runat="server" CssClass="btn btn-circle blue pull-right" OnClick="lnkAdd_Click"> Add New Visit</asp:LinkButton>
                                    <asp:GridView ID="grdHomeVisit" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdHomeVisit_RowDataBound" CssClass="table table-striped table-bordered table-advance">
                                        <HeaderStyle CssClass="font-white bg-green-steel" HorizontalAlign="Center" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <Columns>

                                            <asp:BoundField DataField="AppointmentDate" HeaderText="Date of appointment" />
                                            <asp:BoundField DataField="ReportCompDate" HeaderText="Date Report Completed" />

                                            <asp:TemplateField HeaderText="Type Of Visit">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTypeofvisit" runat="server" CommandName="visit" OnCommand="lnk_Command" Text='<% #Eval("Name")%>'
                                                        CommandArgument='<%# string.Concat(Eval("HCBVisitId"),",",Eval("AppointmentDate"),",",Eval("ReportCompDate"),",",Eval("FeeCharged"),",",Eval("TYpeOfVisit")) %>'> Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fee Charged" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVisitFee" runat="server" Text='<%# String.Format("{0:n2}",Eval("FeeCharged"))%>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HiddenField Value='<%#Eval("HCBReference") %>' ID="hdnHCBVisitId" runat="server" />
                                                    <asp:LinkButton ID="lnkeEdit1" runat="server" CommandName="edt" OnCommand="lnk_Command" CssClass="btn btn-outline btn-circle btn-sm purple"
                                                        CommandArgument='<%# string.Concat(Eval("HCBVisitId"),",",Eval("AppointmentDate"),",",Eval("ReportCompDate"),",",Eval("FeeCharged"),",",Eval("TYpeOfVisit")) %>'><i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%--<asp:ImageButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("HCBVisitId")%>'
                                                        OnClientClick="return confirm('You want to delete this visit ?');" OnCommand="lnk_Command"
                                                        CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />--%>
                                                    <asp:LinkButton Visible="true" ID="lnkeDelete1" runat="server" CommandArgument='<% #Eval("HCBVisitId")%>'
                                                        OnClientClick="return confirm('You want to delete this visit ?');" OnCommand="lnk_Command"
                                                        CommandName="del" CssClass="btn btn-outline btn-circle dark btn-sm black">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:HiddenField ID="hdnhcbvisitfee" Value="" runat="server" />
                                    <asp:HiddenField ID="hdntype" Value="" runat="server" />
                                    <asp:HiddenField ID="hdnhcbtelephonevisitfee" Value="" runat="server" />
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="upHomeVisits" runat="server">
                        <ProgressTemplate>
                            <img alt="progress" src="Images/wait.gif" />
                            Processing...
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <h3 class="form-section bold font-blue text-left">CASE MANAGEMENT</h3>
                    <asp:UpdatePanel ID="upCaseMgnt" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlCaseMgnt" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Case Management Approved by MetLife</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlCaseMgntapproved" runat="server" CssClass="form-control input-medium" TabIndex="54" AutoPostBack="true" OnSelectedIndexChanged="ddlCaseMgntapproved_SelectedIndexChanged">
                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="trCaseMgntApproved" runat="server">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Case Management Funding Limit Approved by MetLife (£)</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtCaseMgntFundingLimit" runat="server" TabIndex="55" onkeypress="javascript:return numberOnly(this,event);" CssClass="form-control input-medium"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="trCurrentCaseTotal" runat="server">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Current Case Total (£)</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtCurrentCaseTotal" runat="server" TabIndex="56" Enabled="false" onkeypress="javascript:return numberOnly(this,event);" CssClass="form-control input-medium"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-actions">
                                            <asp:Button ID="btnCaseMgntSave" runat="server" TabIndex="57" CssClass="btn btn-circle blue pull-right" Text="Save Case Management" OnClick="btnCaseMgntSave_Click" OnClientClick="return ValidateCaseMgnt();" /><br />
                                        </div>
                                        <div id="trAddnewCasemgntCost" runat="server">
                                            <br />
                                            <asp:LinkButton ID="lnkCaseMgntCost" runat="server" CssClass="btn btn-circle blue pull-right" TabIndex="58"> Add Case Management Cost</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-scrollable">
                                    <asp:GridView ID="grdCaseMgntCost" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-advance" OnRowDataBound="grdCaseMgntCost_RowDataBound">
                                        <HeaderStyle CssClass="font-white bg-green-steel" HorizontalAlign="Center" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:BoundField DataField="CaseDate" HeaderText="Date" />
                                            <asp:BoundField DataField="CaseHours" HeaderText="No. of Hours" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />

                                            <asp:TemplateField HeaderText="Rate" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVisitFee" runat="server" Text='<%# String.Format("{0:n2}",Eval("CaseRate"))%>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cost" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCost" runat="server" Text='<%# String.Format("{0:n2}",Eval("CaseCost"))%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField>
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="lnkedittele" runat="server" CommandName="edt" OnCommand="lnk_CommandCaseMgnt"
                                                        CommandArgument='<%# string.Concat(Eval("CaseMgmtCostId"),",",Eval("CaseDate"),",",Eval("CaseHours"),",",Eval("CaseRate"),",",Eval("CaseCost")) %>'
                                                        CssClass="btn btn-outline btn-circle btn-sm purple"> 
                                                        <i class="fa fa-edit"></i>Edit
                                                    </asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>

                                                    <%--<asp:ImageButton Visible="true" ID="lnkDeleteCaseMgntCost" runat="server" CommandArgument='<% #Eval("CaseMgmtCostId")%>'
                                                        OnClientClick="return confirm('You want to delete this Case Management Cost ?');" OnCommand="lnk_CommandCaseMgnt"
                                                        CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />--%>
                                                    <asp:LinkButton Visible="true" ID="lnkDeleteCaseMgntCost" runat="server" CommandArgument='<% #Eval("CaseMgmtCostId")%>'
                                                        OnClientClick="return confirm('You want to delete this Case Management Cost ?');" OnCommand="lnk_CommandCaseMgnt"
                                                        CommandName="del" CssClass="btn btn-outline btn-circle dark btn-sm black">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                    <h3 class="form-section bold font-blue text-left">FUNDED TREATMENT</h3>
                    <asp:UpdatePanel ID="upRehab" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Current Case Total (£)</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRehabCurrentCaseTotal" runat="server" CssClass="form-control input-medium" TabIndex="59" Enabled="false" Text=""></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-actions">
                                            <asp:LinkButton ID="lnkAddRehabilitation" runat="server" CssClass="btn btn-circle blue pull-right" Text="Add New funded Treatment Session" TabIndex="60"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-scrollable">
                                    <asp:GridView ID="grdRehabilitation" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-advance" OnRowDataBound="grdRehabilitation_RowDataBound">
                                        <HeaderStyle CssClass="font-white bg-green-steel" HorizontalAlign="Center" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:BoundField DataField="RehabTypeName" HeaderText="Type" />
                                            <asp:BoundField DataField="RehabProviderName" HeaderText="Provider" />
                                            <asp:BoundField DataField="InstructedDate" HeaderText="Instructed Date" />

                                            <asp:TemplateField HeaderText="Fee Charged" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCostRehab" runat="server" Text='<%# String.Format("{0:n2}",Eval("RehabilitationFee"))%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkeditRehab" runat="server" CommandName="edt" OnCommand="lnk_CommandRehab"
                                                        CommandArgument='<%# string.Concat(Eval("RehabilitationId"),",",Eval("RehabilitationFee"),",",Eval("RehabilitationTypeId"),",",Eval("RehabilitationProviderId"),",",Eval("InstructedDate"),",",Eval("ReportedDate")) %>'
                                                        CssClass="btn btn-outline btn-circle btn-sm purple"> 
                                                        <i class="fa fa-edit"></i>Edit
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>

                                                    <%--<asp:ImageButton Visible="true" ID="lnkeDeleteRehab" runat="server" CommandArgument='<% #Eval("RehabilitationId")%>'
                                                        OnClientClick="return confirm('You want to delete this Funded Treatment Session ?');" OnCommand="lnk_CommandRehab"
                                                        CommandName="del" ImageUrl="Images/Del.gif" Height="13px" Width="13px" />--%>
                                                    <asp:LinkButton Visible="true" ID="lnkeDeleteRehab" runat="server" CommandArgument='<% #Eval("RehabilitationId")%>'
                                                        OnClientClick="return confirm('You want to delete this Funded Treatment Session ?');" OnCommand="lnk_CommandRehab"
                                                        CommandName="del" CssClass="btn btn-outline btn-circle dark btn-sm black">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:UpdatePanel ID="upAdmin" runat="server">
                        <ContentTemplate>
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Administration & Financial
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <div class="form-horizontal">
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Date Received by HCB</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txtHCBReceivedDate" runat="server" MaxLength="10"
                                                                ForeColor="DarkGray" Enabled="false" CssClass="form-control input-medium bold" TabIndex="65"
                                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender23"
                                                                runat="server" Mask="99/99/9999" InputDirection="LeftToRight"
                                                                MaskType="Date" UserDateFormat="DayMonthYear"
                                                                TargetControlID="txtHCBReceivedDate">
                                                            </cc1:MaskedEditExtender>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Date Sent to Nurse</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txtDateSentToNurse" runat="server"
                                                                MaxLength="10" CssClass="form-control input-medium" TabIndex="66"
                                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender24" runat="server"
                                                                Mask="99/99/9999" InputDirection="LeftToRight"
                                                                MaskType="Date" UserDateFormat="DayMonthYear"
                                                                TargetControlID="txtDateSentToNurse">
                                                            </cc1:MaskedEditExtender>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Case Cancelled</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txtCaseCancelled" runat="server" CssClass="form-control input-medium" MaxLength="10"
                                                                TabIndex="67" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender26" runat="server" Mask="99/99/9999"
                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCaseCancelled">
                                                            </cc1:MaskedEditExtender>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Return to work date</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txtReturnToWorkDate" runat="server" CssClass="form-control input-medium" MaxLength="10"
                                                                onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender8" runat="server" Mask="99/99/9999"
                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtReturnToWorkDate">
                                                            </cc1:MaskedEditExtender>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Return to work Reason</label>
                                                        <div class="col-md-7">
                                                            <asp:DropDownList ID="ddlReturnToWorkReason" runat="server" CssClass="form-control input-medium">
                                                                <asp:ListItem Text="--Select Reason--" Value="0" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Return to work – full time" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Return to work – part time" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Case Closed</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txtCaseClosed" runat="server" CssClass="form-control input-medium" MaxLength="10"
                                                                TabIndex="68" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender29" runat="server" Mask="99/99/9999"
                                                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCaseClosed">
                                                            </cc1:MaskedEditExtender>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Reason Closed</label>
                                                        <div class="col-md-7">
                                                            <asp:DropDownList ID="ddlReasonCancel" runat="server" CssClass="form-control input-medium"
                                                                TabIndex="69">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">HCB Fees Charged(£)</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txtFeeCharged" runat="server" CssClass="form-control input-small" Enabled="false"
                                                                TabIndex="70"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Rehab Costs(£)</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txtRehabcost" runat="server" CssClass="form-control input-small" Enabled="false"
                                                                TabIndex="71"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Total Case Costs(£)</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="txttotalcasecost" runat="server" CssClass="form-control input-small" Enabled="false"
                                                                TabIndex="72"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="portlet box blue" id="tdCaseDocs" runat="server">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>Case Documents
                            </div>
                        </div>
                        <div class="portlet-body form" id="tdCaseDocs2" runat="server">
                            <!-- BEGIN FORM-->
                            <div class="form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Document Name</label>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlDocuments" runat="server" CssClass="form-control input-xlarge"
                                                        TabIndex="73">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="REQddlDocuments" runat="server" ControlToValidate="ddlDocuments"
                                                        Display="Dynamic" InitialValue="0" CssClass="ErrorMessage" ValidationGroup="validateFIle"
                                                        ErrorMessage="Select file"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Instructions form uploaded</label>
                                                <div class="col-md-7">
                                                    <asp:CheckBox ID="chkFormUploaded" runat="server" CssClass="pull-left" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <font size="1"><asp:Label ID="lblDocs" runat="server" Width="134px"></asp:Label></font>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Button ID="btnDownloadDocument" runat="server" Text="Download" ValidationGroup="validateFIle"
                                                CssClass="btn btn-circle blue" TabIndex="74" OnClick="btnDownloadDocument_Click" />
                                            <asp:Button ID="btnShowUpload" runat="server" Text="Upload" UseSubmitBehavior="False"
                                                CssClass="btn btn-circle blue" TabIndex="75" OnClientClick="return showupload();" />
                                            <asp:Button ID="btnRemoveDoc" runat="server" ValidationGroup="validateFIle" Text="Remove"
                                                CssClass="btn btn-circle blue" TabIndex="76" OnClick="btnRemoveDoc_Click" />
                                        </div>
                                    </div>
                                    <br />

                                    <div class="row" id="tblDocUpload" runat="server" style="display: none">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">Upload Document</label>
                                                    <div class="col-md-7">
                                                        <asp:FileUpload ID="FileUploadDoc" ValidationGroup="validateupload" runat="server"
                                                            Width="464px" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-md-5"></div>
                                                    <div class="col-md-7">
                                                        <asp:CheckBox ID="chkfoverwrite" runat="server" CssClass="pull-left" ToolTip="Check if you want to overwrite if file wioth same name already uploaded" />
                                                        <label class="pull-left">Overwrite file</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <asp:RequiredFieldValidator ID="reqFileUploadDoc" runat="server" ControlToValidate="FileUploadDoc"
                                                Display="Dynamic" CssClass="ErrorMessage" ValidationGroup="validateupload" ErrorMessage="Select file to upload"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="REGFileUploadDoc" runat="server" ValidationGroup="validateupload"
                                                CssClass="ErrorMessage" ErrorMessage="Only .docx, .doc, .xls, .xlsx, .pdf, .tif, .wav, .mp3, .zip and .rar files are allowed"
                                                Display="Dynamic" ValidationExpression="^.+(.wav|.WAV|.mp3|.MP3|.doc|.DOC|.docx|.DOCX|.tif|.TIF|.pdf|.PDF|.xls|.XLS|.xlsx|.XLSX|.zip|.ZIP|.rar|.RAR)$"
                                                ControlToValidate="FileUploadDoc"> </asp:RegularExpressionValidator>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-actions">
                                                    <asp:Button ID="btnUpload" ValidationGroup="validateupload" runat="server" Text="Upload Doc"
                                                        CssClass="btn btn-circle blue" TabIndex="77" OnClick="btnUpload_Click" />
                                                    <asp:Button ID="btnCancle" runat="server" Text="Cancel" CssClass="btn btn-circle grey-salsa btn-outline" OnClientClick="return Hideupload();" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <asp:Label ID="lblUpload" runat="server" CssClass="ErrorMessage" ForeColor="Red"
                                                Width="459px"></asp:Label>
                                        </div>

                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions">
                                <asp:Button ID="btnCreate" runat="server" CausesValidation="False" Text="New Record"
                                    UseSubmitBehavior="False" CssClass="btn btn-circle blue bold" TabIndex="78"
                                    OnClick="btnCreate_Click" />
                                <asp:Button ID="btnDelete" runat="server" OnClientClick="if (!confirm_delete()) return false;"
                                    Text="Delete Record" UseSubmitBehavior="False" CssClass="btn btn-circle blue bold"
                                    TabIndex="79" Enabled="False" />
                                <asp:Button ID="btnSave" runat="server" Text="Save Record" OnClick="btnSave_Click"
                                    ValidationGroup="validInfo" CausesValidation="true" CssClass="btn btn-circle blue bold"
                                    TabIndex="80" OnClientClick="javascript:return validatesave();" /><br />
                                <asp:Button ID="btnEMAIL" runat="server" Text="** TEST email TEST **" UseSubmitBehavior="False"
                                    CssClass="ButtonClass" Width="125px" TabIndex="81" Visible="false" />

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <asp:Label ID="statusLabel" runat="server" CssClass="ErrorMessage" ForeColor="Red"
                                    Width="726px"></asp:Label>
                                <br />
                                <asp:ValidationSummary ID="ValidationSummary" ValidationGroup="validInfo" runat="server"
                                    ShowMessageBox="True" Width="729px" ForeColor="Red" />
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField ID="hidDurationID" runat="server" />
                    <asp:HiddenField ID="hidInsurance1ID" runat="server" />
                    <asp:HiddenField ID="hidInsurance2ID" runat="server" />
                    <asp:HiddenField ID="hidInsurance3ID" runat="server" />
                    <asp:HiddenField ID="hidInsurance4ID" runat="server" />
                    <asp:HiddenField ID="hidFailedCall1ID" runat="server" />
                    <asp:HiddenField ID="hidFailedCall2ID" runat="server" />
                    <asp:HiddenField ID="hidFailedCall3ID" runat="server" />
                    <asp:HiddenField ID="hidNoteID" runat="server" />
                    <asp:HiddenField ID="hidShowDoc" runat="server" />
                    <asp:HiddenField ID="hidFailCall1" runat="server" />
                    <asp:HiddenField ID="hidFailCall2" runat="server" />
                    <asp:HiddenField ID="hidFailCall3" runat="server" />
                    <asp:HiddenField ID="hidHomeFee" runat="server" />
                    <asp:HiddenField ID="hidCall1Fee" runat="server" />
                    <asp:HiddenField ID="hidCall2Fee" runat="server" />
                    <asp:HiddenField ID="hidCall3Fee" runat="server" />
                    <asp:HiddenField ID="hidLogUserType" runat="server" />

                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>


    <%--<div class="row">
        <div class="col-md-6">
            <div class="form-group">
            </div>
        </div>
    </div>--%>

    <%--<div id="backgroundPopup1" onclick="closepopup()">
    </div>--%>

    <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Always">
        <ContentTemplate>

            <!-- Modal Popup for Add New Visit-->
            <div id="divHomeVisit" class="modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog modal-lg vertical-align-center">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title font-green bold">Home Visit</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-body">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label font-md">Date of Appointment</label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtDateOfAppointmentPop" runat="server" CssClass="form-control"
                                                        onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="MskEditExtender12" runat="server" Mask="99/99/9999"
                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateOfAppointmentPop">
                                                    </cc1:MaskedEditExtender>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label font-md">Date Report Completed</label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtDateReportCompPop" runat="server" CssClass="form-control"
                                                        TabIndex="82" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="MskdEditExtender19" runat="server" Mask="99/99/9999"
                                                        InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateReportCompPop">
                                                    </cc1:MaskedEditExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label font-md">Type Of Visit</label>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlTypeOfVisitPop" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label font-md">Fee Charged (£)</label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtFeeChargedPop" runat="server" onkeypress="javascript:return numberOnly(this,event);" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnSaveHomeVisit" Text="Save" CssClass="btn btn-circle green"
                                    OnClick="btnSaveHomeVisit_Click" />
                                <asp:Button ID="btnCancelHomeVisit" runat="server" data-dismiss="modal" CssClass="btn btn-circle grey-salsa btn-outline" Text="Cancel" OnClientClick="return closepopup();" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Modal Popup for Telephone Intervention-->
            <div id="divTelephone" class="modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center modal-full">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title font-green bold">TELEPHONE INTERVENTION</h4>
                            </div>

                            <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="modal-body form">
                                        <div class="form-horizontal">
                                            <div class="form-body">

                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Date</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox ID="txtCallDatePop" runat="server" CssClass="form-control"
                                                                    onchange="validateDate(this);DisableDateControls(1);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender10" runat="server" Mask="99/99/9999"
                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCallDatePop">
                                                                </cc1:MaskedEditExtender>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label font-md">Time</label>
                                                            <div class="col-md-6">
                                                                <asp:TextBox ID="txtCallTimePop" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender11" runat="server" Mask="99:99" InputDirection="LeftToRight"
                                                                    MaskType="Time" UserTimeFormat="TwentyFourHour" TargetControlID="txtCallTimePop">
                                                                </cc1:MaskedEditExtender>
                                                            </div>
                                                            <label class="col-md-3 control-label font-md">hours</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Type of Call</label>
                                                            <div class="col-md-7">
                                                                <asp:DropDownList ID="ddlTypeofCallPop" runat="server" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Failed Calls</label>
                                                            <div class="col-md-7">
                                                                <asp:ListBox ID="lslFailedCallPop" runat="server" CssClass="form-control"></asp:ListBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="lnkFailedCallPop" runat="server" Text="Add Failed Call"
                                                                Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none;"
                                                                OnClientClick="return AddItem();">
                                                            </asp:LinkButton><br />
                                                            <asp:LinkButton ID="lnkFailedPop" runat="server" Text="Remove Failed Call"
                                                                Font-Names="Arial" Font-Size="9pt" ForeColor="blue" Style="text-decoration: none"
                                                                OnClientClick="javascript:return validateFaildcallPop(1);"
                                                                TabIndex="61">
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Fee Charged (£)</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox ID="txtFeeChargedTelPop" runat="server" CssClass="form-control" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Expected RTW Date(Optimum)</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox ID="txtRTWDateOptimumPop" runat="server" CssClass="form-control"
                                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender12" runat="server" Mask="99/99/9999"
                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateOptimumPop">
                                                                </cc1:MaskedEditExtender>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Expected RTW Date(Maximum)</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox ID="txtRTWDateMaxPop" runat="server" CssClass="form-control"
                                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender13" runat="server" Mask="99/99/9999"
                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtRTWDateMaxPop">
                                                                </cc1:MaskedEditExtender>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Case Management Recommended?</label>
                                                            <div class="col-md-7">
                                                                <asp:DropDownList ID="ddlCaseMgmtPop" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 display-hide">
                                                        <div class="form-group">
                                                            <label class="col-md-5 control-label font-md">Next Call Scheduled</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox ID="txtNxtCallSchdPop" runat="server" CssClass="form-control"
                                                                    onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender15" runat="server" Mask="99/99/9999"
                                                                    InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtNxtCallSchdPop">
                                                                </cc1:MaskedEditExtender>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <asp:HiddenField ID="hdnFailedCallList" runat="server" Value="" />
                                        <asp:Button runat="server" ID="btnTelephoneActivity" Text="Save" CssClass="btn btn-circle green"
                                            OnClick="btnTelephoneActivitySave_Click" OnClientClick="AddListFailedCall();" />
                                        <asp:Button ID="btnTelephoneCancel" runat="server" CssClass="btn btn-circle grey-salsa btn-outline" data-dismiss="modal" Text="Cancel" OnClientClick="return closepopup();" OnClick="btnTelephoneCancel_Click" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Modal Popup for Add Case Management Cost -->
            <div id="divCaseManagementCost" class="modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title font-green bold">CASE MANAGEMENT COST</h4>
                            </div>
                            <div class="modal-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Date</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtCaseMgntDate" runat="server" CssClass="form-control"
                                                            onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                        <cc1:MaskedEditExtender ID="MaskedEditExtender16" runat="server" Mask="99/99/9999"
                                                            InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtCaseMgntDate">
                                                        </cc1:MaskedEditExtender>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">No. of Hours</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtNoofHour" runat="server" CssClass="form-control" onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Rate (£)</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtRate" runat="server" CssClass="form-control"
                                                            onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Cost (£)</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtCaseMgntCost" runat="server" CssClass="form-control" Enabled="false"
                                                            onkeypress="javascript:return numberOnly(this,event);"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnCaseMgntCostSave" Text="Save" OnClick="btnCaseMgntCostSave_Click" CssClass="btn btn-circle green" />
                                <asp:Button ID="btnCaseMgntcancel" runat="server" data-dismiss="modal" CssClass="btn btn-circle grey-salsa btn-outline" Text="Cancel" OnClientClick="return closepopup();" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Modal Popup for Add New Funded Treatment Session-->
            <div id="divRehabilitation" class="modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title font-green bold">FUNDED TREATMENT</h4>
                            </div>
                            <div class="modal-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Type</label>
                                                    <div class="col-md-7">
                                                        <asp:DropDownList ID="ddlRehabType" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Provider</label>
                                                    <div class="col-md-7">
                                                        <asp:DropDownList ID="ddlRehabProvider" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Date Instructed</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtDateInstructed" runat="server" CssClass="form-control"
                                                            onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                        <cc1:MaskedEditExtender ID="msktxtDateInstructed" runat="server" Mask="99/99/9999"
                                                            InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateInstructed">
                                                        </cc1:MaskedEditExtender>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Date Reported</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtDateReported" runat="server" CssClass="form-control"
                                                            TabIndex="55" onchange="javascript:return validateDate(this);"></asp:TextBox>
                                                        <cc1:MaskedEditExtender ID="msktxtDateReported" runat="server" Mask="99/99/9999"
                                                            InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtDateReported">
                                                        </cc1:MaskedEditExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label font-md">Fee Charged (£)</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtRehabFeeCharged" runat="server" onkeypress="javascript:return numberOnly(this,event);" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnRehabilitationSave" Text="Save" CssClass="btn btn-circle green"
                                    OnClick="btnRehabilitationSave_Click" />
                                <asp:Button ID="btnRehabilitationCancel" runat="server" data-dismiss="modal" CssClass="btn btn-circle grey-salsa btn-outline" Text="Cancel" OnClientClick="return closepopup();" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Modal Popup for View Service Required -->
            <div id="divServiceRequired" class="modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title font-green bold">SERVICE REQUIRED</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblServiceRequired" CssClass="col-md-12 control-label font-md" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <asp:Button ID="btnServiceCancel" runat="server" data-dismiss="modal" CssClass="btn btn-circle green bold" Text="OK" OnClientClick="return closepopup();" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="hdnVisitId" runat="server" Value="" />
            <asp:HiddenField ID="hdnTelephoneActivityId" runat="server" Value="" />
            <asp:HiddenField ID="hdbCaseManagementCostId" runat="server" Value="" />
            <asp:HiddenField ID="hdnServiceRequired" runat="server" Value="" />
            <asp:HiddenField ID="hdnRehabilitationId" runat="server" Value="" />

            <asp:HiddenField ID="hdnpostbackservicerequired" runat="server" Value="" />
        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>
