﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ClientSearch.aspx.cs" Inherits="HCB.ClientSearch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/Validation.js" type="text/javascript"></script>
    <link id="Link1" rel="Stylesheet" href="Styles/Slate/plugin.css" type="text/css"
        media="screen" title="slate1" runat="server" />
    <link href="Styles/Paging.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function Validate() {
            debugger;
            var bStatus = true;
            var msg = "";
            var txtHcfNO = document.getElementById('<%= txtHcbRef.ClientID %>');
            if (Trim(txtHcfNO.value) != "") {
                //return (checkNumber(txtHcfNO, "HCB Reference"));
            }

            var txtdate = document.getElementById('<%= txtClaimDOB.ClientID %>').value;
            txtdate = Trim(txtdate);
            if (txtdate != "") {
                var msg = validateDate11(txtdate);
                if (msg != "" && msg != "ok") {
                    alert(msg);
                    return false;
                }
            }
            return true;
        }
        function ValidateDOB(dateid) {
            //debugger;
            var msg;
            var sValidChars;
            var nCounter;
            var datval = dateid.value;
            msg = "ok";
            sValidChars = "1234567890/";

            if (Trim(datval) != "") {
                if (datval.length != 10) {
                    msg = " Please enter in exact format (dd/mm/yyyy) e.g. 02/01/2000 in ";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                for (nCounter = 0; nCounter < datval.length; nCounter++) {

                    if (sValidChars.indexOf(datval.substring(nCounter, nCounter + 1)) == -1) {

                        msg = " Please enter in exact format (dd/mm/yyyy)";
                        alert(msg);
                        date.focus();
                        nCounter = datval.length;
                        return msg;
                    }

                }

                if (datval.indexOf("/") != 2) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 3) != 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                if (datval.indexOf("/", 6) < 10 && datval.indexOf("/", 6) > 5) {
                    msg = "Invalid From date : Please enter in exact format (dd/mm/yyyy)";
                    alert(msg);
                    date.focus();
                    return msg;
                }

                var month;
                var day;
                var year;
                month = parseInt(TrimNumber(datval.substring(3, 5)));
                day = parseInt(TrimNumber(datval.substring(0, 2)));
                year = parseInt(datval.substring(6, 10));

                if (month > 12 || month < 1) {
                    msg = " Invalid Month";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (year < 1900 || year > 3000) {
                    msg = " Invalid year. Should be in (1900-3000)";
                    alert(msg);
                    date.focus();
                    return msg;
                }
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    if (day > 31 || day < 1) {
                        msg = " Days should not be more than 31 or less than 1 for entered Month";
                        alert(msg);
                        date.focus();
                        return msg;
                    }
                }
                else {
                    if (month == 2 && (year % 4) == 0) {
                        if (day > 29 || day < 1) {
                            msg = " Days should not be more than 29 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else if (month == 2 && (year % 4) != 0) {
                        if (day > 28 || day < 1) {
                            msg = " Days should not be more than 28 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                    else {
                        if (day > 30 || day < 1) {
                            msg = " Days should not be more than 30 or less than 1 for entered Month";
                            alert(msg);
                            date.focus();
                            return msg;
                        }
                    }
                }
                //return msg;

                //_doPostBack('btnCheck', 'OnClick');
                // return false;
            }
        }


    </script>
    <asp:ScriptManager ID="ScptMngr1" runat="server">
    </asp:ScriptManager>

    <!-- BEGIN PAGE HEAD-->
    <%--<div class="page-head">
        <div class="container bg-grey">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1 class="font-blue">SEARCH</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>--%>
    <!-- END PAGE HEAD-->
    <%--<br />--%>
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Client Search
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="form-group">
                        <div class="col-md-7">
                            <asp:ValidationSummary ID="validateUserSummary" CssClass="font-red bold font-sm text-left" HeaderText="Summary" ShowSummary="false"
                                DisplayMode="BulletList" ValidationGroup="validateSearch" runat="server" ShowMessageBox="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">HCB Reference</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtHcbRef" runat="server" CssClass="form-control">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ID="REGtxtHcbRef" ErrorMessage="Enter valid HCBReference eg. (DCP**** )" ControlToValidate="txtHcbRef"
                                ToolTip="Enter valid HCBReference eg. (DCP**** )" Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                runat="server" ValidationGroup="validateSearch">
                                     <img src='images/Exclamation.jpg'>
                            </asp:RegularExpressionValidator>
                            <%--^\bDCP\d{4}\b$--%>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Case Manager</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlCaseMngr" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Case Assessor</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlClaimAssessor" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Claimant Surname</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtClaimSurname" runat="server" CssClass="form-control">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ID="REGtxtsurname" ErrorMessage="Enter a valid Surname" ControlToValidate="txtClaimSurname"
                                ToolTip="Enter a valid Surname." Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                runat="server" ValidationGroup="validateSearch">
                                     <img src='images/Exclamation.jpg'>
                            </asp:RegularExpressionValidator>
                            <%--^[a-zA-Z'./s]{1,15}$--%>
                        </div>
                        <div class="col-md-3">
                            <asp:RadioButtonList ID="RdbClaimSurname" runat="server" RepeatDirection="Horizontal" CssClass="font-md">
                                <asp:ListItem Selected="True">Exact&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem>Contains</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Claimant Forename</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtClaimForename" runat="server" CssClass="form-control">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ID="REGtxtClaimForename" ErrorMessage="Enter a valid Name" ControlToValidate="txtClaimForename"
                                ToolTip="Enter a valid Name." Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                runat="server" ValidationGroup="validateSearch">
                                     <img src='images/Exclamation.jpg'>
                            </asp:RegularExpressionValidator>
                            <%--^[a-zA-Z'./s]{1,15}$--%>
                        </div>
                        <div class="col-md-3">
                            <asp:RadioButtonList ID="RdbClaimForename" runat="server" RepeatDirection="Horizontal" CssClass="font-md">
                                <asp:ListItem Selected="True">Exact&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem>Contains</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Claimant Date of Birth</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtClaimDOB" runat="server" MaxLength="10" CssClass="form-control" onchange="javascript:return ValidateDOB(this);"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender26" runat="server" Mask="99/99/9999"
                                InputDirection="LeftToRight" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="txtClaimDOB">
                            </cc1:MaskedEditExtender>                            
                        </div>
                        <div class="col-md-2">
                            <img id="img21" src="images/Exclamation.jpg" style="visibility: hidden" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label font-md">Claimant Address</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtClaimAddress" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ErrorMessage="Enter a valid Address" ControlToValidate="txtClaimAddress"
                                ToolTip="Enter a valid Address." Display="Dynamic" ValidationExpression="^[a-zA-Z'.\s | \d | \- | \/ | \$ | \£ | \€ | \( | \) | \ | \! | \% | \+ | \& | \, | \! $]{1,200}$"
                                runat="server" ValidationGroup="validateSearch">
                                     <img src='images/Exclamation.jpg'>
                            </asp:RegularExpressionValidator>
                        </div>
                        <div class="col-md-3">
                            <asp:RadioButtonList ID="RdbClaimAdd" runat="server" RepeatDirection="Horizontal" CssClass="font-md">
                                <asp:ListItem Selected="True">Exact&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem>Contains</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-9">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClientClick="return Validate();" ValidationGroup="validateSearch"
                                OnClick="btnSearch_Click" CssClass="btn btn-circle blue" />
                            <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CssClass="btn btn-circle grey-salsa btn-outline"/>
                        </div>
                        <asp:Label ID="lbltest" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>    

    <div class="portlet box blue" id="gvportletbox" runat="server">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <asp:Label ID="lblTitle" runat="server" Text="Client Records"
                            Visible="false"></asp:Label>
            </div>
        </div>
        <div class="portlet-body">            
            <div class="table-scrollable">
                <%--<div class="form-body">

                    <div class="form-group">--%>
                        <div class="col-md-10">
                            <asp:GridView ID="gvResults" class="table table-striped" runat="server" GridLines="Both"
                            AutoGenerateColumns="false" AllowSorting="True" PageSize="3" BorderColor="silver" AllowPaging="true"  
                            BorderStyle="Solid" border="0" OnRowCommand="gvResults_RowCommand" OnPageIndexChanging="gvResults_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="HCB Ref. No." ItemStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkHCBRef" runat="server" BorderStyle="None" Text='<%# Eval("RefNo") %>'
                                            CommandArgument='<%# Eval("HCBReference") %>' CommandName="HCBRef" Style="text-decoration: none">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Client Name" DataField="ClientName"/>
                                <asp:BoundField HeaderText="DOB" DataField="ClientDOB"/>
                                <asp:BoundField HeaderText="Claim Assessor" DataField="ClaimHandlerName"/>
                                <asp:BoundField HeaderText="Manager" DataField="NurseName"/>
                            </Columns>
                            <AlternatingRowStyle BackColor="#F5FDF5" />
                            <HeaderStyle CssClass="font-white bg-green-steel" HorizontalAlign="Center" />
                            <FooterStyle CssClass="GridFooterStyle" />
                            <RowStyle CssClass="GridViewRowStyle" HorizontalAlign="Center" />
                            <EditRowStyle CssClass="GridViewEditRowStyle" />
                            <AlternatingRowStyle CssClass="GridAlternateRowStyle" />
                            <SelectedRowStyle CssClass="GridSelectedeRowStyle" />
                            <PagerStyle CssClass="pagination-ys bold" />
                            <EmptyDataTemplate>                                
                                <asp:Label ID="lblemp" runat="server" CssClass="font-lg bold"> No records Found </asp:Label>                                    
                            </EmptyDataTemplate>
                        </asp:GridView>
                        </div>
                    <%--</div>

                </div>--%>
            </div>            
        </div>
    </div>
</asp:Content>
