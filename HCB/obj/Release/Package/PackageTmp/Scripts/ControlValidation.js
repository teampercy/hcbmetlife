﻿function IsValidEmailId(Input) {
    debugger;
    var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    if (!regExp.test($.trim(Input.value))) {
        Input.value = "";
        Input.focus();
    }
    else {

        return true;
    }

    return false;
}
function numberOnly(txt, e) {
    //debugger;
    var arr = '0123456789.';
    var code;
    var code1;
    if (window.event) {
        code = e.keyCode;
    }
    else {
        code = e.which;
    }

    var cha = String.fromCharCode(code);
    if (arr.indexOf(cha) == -1) {
        return false;
    }
    else if (cha == '.') {
        if (txt.value.indexOf('.') > -1)
            return false;
    }
    if (txt.value.indexOf('.') > -1) {
        code1 = txt.value.indexOf('.');
        if (txt.value.length == code1 + 3)
            return false;
    }
}
function AllowNumberChars(event, Control, AllowNegative, FractionDigits) {

    var maxLen =Control.getAttribute('maxlength');
    
    //If max field length has been reached and an illegal char
    //has been entered then the last legal char will be removed.
    //Therefore test and exit if necessary.
    if (maxLen) {
        if (Control.value.length >maxLen) {
            Control.value=RemoveNonNumbers(Control,AllowNegative,FractionDigits);
            return;
        }
        
        if (Control.value.length >0) {
            if (Control.value.substring(0,1) !="-"){
                if (Control.value.length >maxLen-1) {
                    Control.value=RemoveNonNumbers(Control,AllowNegative,FractionDigits);
                    return;
                }
            }
        }
    }

    var KeyCode;
   
    if (event.keyCode) {KeyCode = event.keyCode;} //Internet Explorer
    if (event.which) {KeyCode = event.which;}     //Other browsers  
    
    if (KeyCode==37){return;}  //Cursor left
    if (KeyCode==39){return;}  //Cursor right
    if (KeyCode==86){return;}  //Control V
    
    Control.value=RemoveNonNumbers(Control,AllowNegative,FractionDigits);
}

function RemoveNonNumbers(Control,AllowNegative,FractionDigits) {

    var Val=""
    var PointPos=Control.value.indexOf(".")
    
    for (x=0; x<=Control.value.length; x=x+1) {
        KeyCode=Control.value.charCodeAt(x)
        
        if (KeyCode>=48 && KeyCode<=57) {
            Val=Val + String.fromCharCode(KeyCode)
        }
        if (x==PointPos) {Val=Val + "."}
          
        if (AllowNegative==true) {
            if (x==0 && KeyCode==45) {                 
                Val=Val + String.fromCharCode(KeyCode)
            }
        }
    }
    
    if (FractionDigits!=null) {
   
        var DecPlaces

        PointPos=Val.indexOf(".")

        if (PointPos >=0) {
            var terminate = PointPos + FractionDigits +1;
            if (terminate > Val.length){terminate=Val.length};
            
            DecPlaces=Val.substring(PointPos+1,terminate);
      
            Val=Val.substring(0,PointPos+1);
            Val+=DecPlaces
        }
    }
    
    return Val;
}


function isValidDate(objControl) {

    var tryDate   
    var Stroke1
    var Stroke2
    var varDate 

    varDate = objControl.value
    varDate = varDate.replace(/-/g,"/")
    
    Stroke1 = varDate.indexOf("/", 0)
    Stroke2 = varDate.indexOf("/", Stroke1 + 1)

    if (Stroke1==0 || Stroke2==0) {return false};
    
    varDays = varDate.substring(0 , Stroke1)
    varMonths = varDate.substring(Stroke1+1 , Stroke2)
    varYears = varDate.substring(Stroke2+1 , varDate.length)
    
    if (varYears.length == 2) {   
        varYears = '20'+ varYears
    }
    
    var varD
    var varM
    var varFormattedDate
    var tryDate
    
    varD = '0' + varDays
    varM = '0' + varMonths
    
    varD = varD.substring(varD.length , (varD.length -2))
    varM = varM.substring(varM.length , (varM.length -2))
    
    varFormattedDate = varD + '/' + varM + '/' + varYears

    tryDate = new Date(varYears, varMonths-1, varDays);          
  
    if (tryDate.getFullYear() ==varYears && tryDate.getMonth() ==varMonths-1  && tryDate.getDate() ==varDays) {
        objControl.value = varFormattedDate
        return true;
    }      
    
    alert('Entered date is invalid!\nFormat: DD/MM/YYYY');   
    return false;
}


