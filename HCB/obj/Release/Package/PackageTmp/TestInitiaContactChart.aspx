﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TestInitiaContactChart.aspx.cs" Inherits="HCB.TestInitiaContactChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption ">
                <span class="caption-subject font-dark bold uppercase">Initial Contact</span>
                <span class="caption-helper"></span>
            </div>
        </div>
        <div class="portlet-body">
            <div id="InitialContactChart" style="width: 100%; height: 300px">
            </div>
        </div>
    </div>

    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption ">
                <span class="caption-subject font-dark bold uppercase">Initial Visit</span>
                <span class="caption-helper"></span>
            </div>
        </div>
        <div class="portlet-body">
            <div id="InitialVisitChart" style="width: 100%; height: 300px">
            </div>
        </div>
    </div>

    <asp:Button ID="btnChart" runat="server" OnClick="btnChart_Click" Text="Export" />

    <script type="text/javascript" src="<%=Page.ResolveUrl("https://www.gstatic.com/charts/loader.js") %>"></script>
    <script type="text/javascript">
        $(function () {
            google.charts.load('current', { 'packages': ['corechart'] });
            google.charts.setOnLoadCallback(function () {
                DrawInitialContactChart();
                DrawInitialVisitChart();
            });

        });
    </script>
    <script type="text/javascript">
        function DrawInitialContactChart() {
            $.ajax({
                url: "TestInitiaContactChart.aspx/GetInitialChartData",
                type: "POST",
                data: JSON.stringify({ 'ChartType': 1 }),
                contentType: "application/json;charset=utf-8",
                success: function (res) {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Month');
                    //data.addColumn('number', 'Duration');
                    data.addColumn('number', 'SLA Timescale');
                    data.addColumn('number', 'Average');
                    //DurationLegendData = res.d.find(obj => obj.Legend==='Duration').ChartData;
                    SLALegendData = res.d.find(obj => obj.Legend === 'SLA').ChartData;
                    AverageLegendData = res.d.find(obj => obj.Legend === 'Average').ChartData;

                    for (var month = 1; month <= 12; month++) {
                        //data.addRow([moment.months(month-1), DurationLegendData[month - 1].YaxisValue, SLALegendData[month - 1].YaxisValue, AverageLegendData[month - 1].YaxisValue]);
                        data.addRow([moment.monthsShort(month - 1), SLALegendData[month - 1].YaxisValue, AverageLegendData[month - 1].YaxisValue]);
                    }

                    var options = {
                        title: 'Instruction to Claimant Contact - Year ' + new Date().getFullYear(),
                        legend: { position: 'bottom' }
                    };

                    var chart = new google.visualization.LineChart($('#InitialContactChart')[0]);
                    chart.draw(data, options);
                },
                fail: function (response) {
                    alert("Unable to display initial contact chart");
                }
            });
        }
    </script>

    <script type="text/javascript">
    function DrawInitialVisitChart() {
        $.ajax({
            url: "TestInitiaContactChart.aspx/GetInitialChartData",
            type: "POST",
            data: JSON.stringify({'ChartType':2}),
            contentType: "application/json;charset=utf-8",
            success: function (res) {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Month');
                //data.addColumn('number', 'Duration');
                data.addColumn('number', 'SLA Timescale');
                data.addColumn('number', 'Average');
                //DurationLegendData = res.d.find(obj => obj.Legend==='Duration').ChartData;
                SLALegendData = res.d.find(obj => obj.Legend==='SLA').ChartData;
                AverageLegendData = res.d.find(obj => obj.Legend==='Average').ChartData;

                for (var month = 1; month <= 12; month++) {
                    
                    //data.addRow([moment.months(month-1), DurationLegendData[month - 1].YaxisValue, SLALegendData[month - 1].YaxisValue, AverageLegendData[month - 1].YaxisValue]);
                    data.addRow([moment.monthsShort(month - 1), SLALegendData[month - 1].YaxisValue, AverageLegendData[month - 1].YaxisValue]);
                }
                
                var options = {
                    title: 'First Contact to Site/Home Visit  - Year '+new Date().getFullYear(),
                    legend: { position: 'bottom' }
                    
                };

                var chart = new google.visualization.LineChart($('#InitialVisitChart')[0]);
                chart.draw(data, options);
                
            },
            fail: function (response) {
                alert("Unable to display initial visit chart");
            }
        });
    }
</script>
</asp:Content>
