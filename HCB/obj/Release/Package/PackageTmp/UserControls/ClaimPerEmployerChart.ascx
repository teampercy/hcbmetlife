﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClaimPerEmployerChart.ascx.cs" Inherits="HCB.UserControls.ClaimPerEmployerChart" %>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption ">
            <span class="caption-subject font-dark bold uppercase">Employers</span>
            <span class="caption-helper">HCB Group service users</span>
        </div>        
    </div>
    <div class="portlet-body">
        <div id="ClaimPerEmployerChart" style="width:100%;height:300px">
        </div>
    </div>
</div>
<script type="text/javascript">

    function DrawClaimPerEmployerChart() {

        $.ajax({
            url: "Default.aspx/GetTotalClaimPerEmployer",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Employer');
                data.addColumn('number', 'Claims');
                $.each(res.d, function (EmployerName, TotalClaim) {
                    data.addRow([EmployerName,TotalClaim]);
                });       

                var options = {                                      
                    is3D: true,
                    legend: "bottom",
                    pieSliceText: 'value',
                    title:"Total Cases per Employer"
                };                
                var chart = new google.visualization.PieChart($('#ClaimPerEmployerChart')[0]);
                chart.draw(data, options);
            },
            failure: function (response) {
                alert("Unable to display Employer chart");
            }
        });
       
    }
</script>
