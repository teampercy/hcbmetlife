﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardCausationMonitor.ascx.cs" Inherits="HCB.UserControls.DashboardCausationMonitor" %>
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Reasons For Referral</span>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnOpenAll" CssClass="btn form-control input-medium"
                                         style="border:1px solid #c2cad8;" Text="ALL Cases" OnClick="btnOpenAll_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Scheme Name</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardInjuryScheme" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardInjuryScheme_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Broker</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardInjuryBroker" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardInjuryBroker_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Date Range</label>
                                    <div class="col-md-6">
                                        <div class="input-group input-medium input-daterange date-picker" data-date-format="dd/mm/yyyy">
                                            <asp:TextBox runat="server" CssClass="form-control" name="from" ID="txtInjuryFromDate"></asp:TextBox>
                                            <span class="input-group-addon">to </span>
                                            <asp:TextBox runat="server" CssClass="form-control" name="to" ID="txtInjuryToDate"></asp:TextBox>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">Select date range </span>
                                    </div>
                                    <div class="col-md-1">                                        
                                        <asp:LinkButton runat="server" ID="btnDateRange" CssClass="btn green" OnClick="btnDateRange_Click">
                                            <i class="fa fa-check-circle"></i>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--<div class="row">
                            <div class="col-md-12 text-left">
                                <div class="panel panel-warning">
                                    <ul class="list-group">
                                        <li class="list-group-item">Psychiatric
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                        <li class="list-group-item">Musculo Skeletal
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                        <li class="list-group-item">Cardiac/Stroke
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                        <li class="list-group-item">Injury
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                        <li class="list-group-item">Neurological
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                        <li class="list-group-item">Cancer
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                        <li class="list-group-item">Other
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                        <li class="list-group-item">Unknown
                                 <span class="badge badge-success">11 </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>--%>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-warning">
                                    <asp:GridView runat="server" ID="gvInjuryPercent" AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered">
                                        <Columns>
                                            <asp:BoundField HeaderText="Illness Injury Name" DataField="IllnessInjuryName" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left" />
                                            <asp:BoundField HeaderText="Percentage" DataField="IllnessInjuryPercentage" DataFormatString="{0} %" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" />
                                        </Columns>
                                        <HeaderStyle CssClass="font-white bg-green-steel" />
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblEmptyRecordText" runat="server" CssClass="font-md font-red bold"> No records Found </asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    function pageLoad(sender, args) {
        $(document).ready(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
    }
</script>
