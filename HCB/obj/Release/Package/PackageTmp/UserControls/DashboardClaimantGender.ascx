﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardClaimantGender.ascx.cs" Inherits="HCB.UserControls.DashboardClaimantGender" %>
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Claimant Gender</span>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnOpenAll" CssClass="btn form-control input-medium" 
                                        style="border:1px solid #c2cad8;" Text="ALL Cases" OnClick="btnOpenAll_Click"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Scheme Name</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardGenderScheme" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardGenderScheme_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Select Broker</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList runat="server" ID="ddlDashboardGenderBroker" CssClass="form-control input-medium"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardGenderBroker_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Date Range</label>
                                    <div class="col-md-6">
                                        <div class="input-group input-medium input-daterange date-picker" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                            <asp:TextBox runat="server" CssClass="form-control" name="from" ID="txtGenderFromDate"></asp:TextBox>
                                            <span class="input-group-addon">to </span>
                                            <asp:TextBox runat="server" CssClass="form-control" name="to" ID="txtGenderToDate"></asp:TextBox>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">Select date range </span>
                                    </div>
                                    <div class="col-md-1">                                        
                                        <asp:LinkButton runat="server" ID="btnGenderDateRange" CssClass="btn green" OnClick="btnGenderDateRange_Click">
                                            <i class="fa fa-check-circle"></i>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="pricing-content-1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="price-column-container border-active">
                                        <div class="price-table-head bg-green">
                                            <h2 class="no-margin">Claimant Male %</h2>
                                        </div>
                                        <div class="arrow-down border-top-green"></div>
                                        <div class="price-table-pricing">
                                            <h3>
                                                <sup class="price-sign">%</sup>
                                                <asp:Label runat="server" ID="lblMalePercent"></asp:Label>
                                            </h3>
                                            <p>Male</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="price-column-container border-active">
                                        <div class="price-table-head bg-green">
                                            <h2 class="no-margin">Claimant Female %</h2>
                                        </div>
                                        <div class="arrow-down border-top-green"></div>
                                        <div class="price-table-pricing">
                                            <h3>
                                                <sup class="price-sign">%</sup>
                                                <asp:Label runat="server" ID="lblFemalePercent"></asp:Label>
                                            </h3>
                                            <p>Female</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
    });
</script>
