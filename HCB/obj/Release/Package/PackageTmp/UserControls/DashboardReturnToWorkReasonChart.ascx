﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardReturnToWorkReasonChart.ascx.cs" Inherits="HCB.UserControls.DashboardReturnToWorkReasonChart" %>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption ">
            <span class="caption-subject font-dark bold uppercase">Return To Work Reason</span>
            <span class="caption-helper"></span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="ReturnToWorkReasonChart" style="width:100%;height:300px">
        </div>
    </div>
</div>
<script type="text/javascript">
    function DrawReturnToWorkReasonChart() {
        $.ajax({
            url: "Default.aspx/GetTotalClaimPerReturnToWorkReason",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'ReturnToWorkReason');
                data.addColumn('number', 'Return To Work Reason');
                $.each(res.d, function (RTWReason, TotalClaim) {
                    data.addRow([RTWReason, TotalClaim]);
                });

                var options = {                    
                    legend: "bottom",
                    colors: ['#1BBC9B'],
                    //isStacked:"percent"
                    //pieSliceText: 'value',
                    //title: "Total Cases per Broker"
                    vAxis: { minValue: 0, maxValue: 100, format: '#\'%\'' },
                    bar: { groupWidth: '30%' }
                };

                var chart = new google.visualization.ColumnChart($('#ReturnToWorkReasonChart')[0]);
                chart.draw(data,options);
            },
            failure: function (response) {
                alert("Unable to display Return To Work Reason chart");
            }
        })
    }
</script>
