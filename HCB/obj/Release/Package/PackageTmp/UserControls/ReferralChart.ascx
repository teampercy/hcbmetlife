﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReferralChart.ascx.cs" Inherits="HCB.UserControls.ReferralChart" %>

<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption ">
            <span class="caption-subject font-dark bold uppercase">Reasons for referral</span>
            <span class="caption-helper"></span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="ReferralChart" style="width:100%;height:300px">
        </div>
    </div>
</div>
<script type="text/javascript">
    function DrawReferralChart() {

        $.ajax({
            url: "Default.aspx/GetTotalClaimPerInjury",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'IllnessInjuryName');
                data.addColumn('number', 'Claims');
                $.each(res.d, function (Illness, TotalClaim) {
                    data.addRow([Illness, TotalClaim]);
                });                
                var chart = new google.visualization.ColumnChart($('#ReferralChart')[0]);
                chart.draw(data);
                
            },
            failure: function (response) {
                alert("Unable to display Referral chart");
            }
        });
    }
</script>


