﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class CaseManagement
    {
        #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        #endregion

        #region Constructor

        public CaseManagement()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion
        #region Properties

        public int CaseMgmtId
        { get; set; }

        public int HCBReference
        { get; set; }

        public int CaseMgmtCostId
        { get; set; }
        public decimal FundingLimit
        { get; set; }
        public bool ApprovedBy { get; set; }
        public string CaseDate { get; set; }
        public decimal CaseHours { get; set; }
        public decimal CaseRate { get; set; }
        public decimal CaseCost { get; set; }
        #endregion
        #region [Method for Case Management]
        public DataSet GetDetailHCBCaseManagement(int HCBReference)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetDetailHCBCaseManagement";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "CaseMgnt";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public int InsertHCBCaseManagement()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "InsertHCBCaseManagement";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.HCBReference > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                }
                this.db.AddInParameter(cmd, "@ApprovedBy", DbType.Boolean, this.ApprovedBy);
                if (this.FundingLimit > 0)
                {

                    this.db.AddInParameter(cmd, "@FundingLimit", DbType.Decimal, this.FundingLimit);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FundingLimit", DbType.Decimal, DBNull.Value);
                }                
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        #endregion
        #region [Method for Case Management Cost]
        public DataSet GetListHCBCaseManagementCost(int HCBReference)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetListHCBCaseManagementCost";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "CaseMgntCost";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public DataSet GetDetailHCBCaseManagementCost(int CaseMgmtCostId)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetDetailHCBCaseManagementCost";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (CaseMgmtCostId > 0)
                {
                    this.db.AddInParameter(cmd, "CaseMgmtCostId", DbType.Int32, CaseMgmtCostId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "CaseMgmtCostId", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "CaseMgntCost";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public int Save()
        {
            if (this.CaseMgmtCostId == 0)
            {
                return this.Insert();

            }
            else
            {
                if (this.CaseMgmtCostId > 0)
                {
                    this.Update();
                    return CaseMgmtCostId;
                }
                else
                {
                    this.CaseMgmtCostId = 0;
                    return CaseMgmtCostId;
                }
            }
        }
        private int Insert()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "InsertHCBCaseManagementCost";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.HCBReference > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CaseDate))
                {
                    this.db.AddInParameter(cmd, "@CaseDate", DbType.String, this.CaseDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseDate", DbType.String, DBNull.Value);
                }
                if (this.CaseHours > 0)
                {
                    this.db.AddInParameter(cmd, "@CaseHours", DbType.Decimal, this.CaseHours);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseHours", DbType.Decimal, DBNull.Value);
                }
                if (this.CaseRate > 0)
                {

                    this.db.AddInParameter(cmd, "@CaseRate", DbType.Decimal, this.CaseRate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseRate", DbType.Decimal, DBNull.Value);
                }
                if (this.CaseCost > 0)
                {

                    this.db.AddInParameter(cmd, "@CaseCost", DbType.Decimal, this.CaseCost);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseCost", DbType.Decimal, DBNull.Value);
                }
                //this.db.ExecuteDataSet(cmd);

                //string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();

                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        private int Update()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "UpdateHCBCaseManagementCost";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.CaseMgmtCostId > 0)
                {

                    this.db.AddInParameter(cmd, "@CaseMgmtCostId", DbType.Int32, this.CaseMgmtCostId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseMgmtCostId", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CaseDate))
                {
                    this.db.AddInParameter(cmd, "@CaseDate", DbType.String, this.CaseDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseDate", DbType.String, DBNull.Value);
                }
                if (this.CaseHours > 0)
                {
                    this.db.AddInParameter(cmd, "@CaseHours", DbType.Decimal, this.CaseHours);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseHours", DbType.Decimal, DBNull.Value);
                }
                if (this.CaseRate > 0)
                {

                    this.db.AddInParameter(cmd, "@CaseRate", DbType.Decimal, this.CaseRate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseRate", DbType.Decimal, DBNull.Value);
                }
                if (this.CaseCost > 0)
                {

                    this.db.AddInParameter(cmd, "@CaseCost", DbType.Decimal, this.CaseCost);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CaseCost", DbType.Decimal, DBNull.Value);
                }

                cmd.ExecuteNonQuery();

                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        public DataSet DeleteHCBCaseManagementCost()
        {
            DataSet ds = new DataSet();

            try
            {

                DbCommand comm = this.db.GetStoredProcCommand("DeleteHCBCaseManagementCost");
                this.db.AddInParameter(comm, "@CaseMgmtCostId", DbType.Int32, this.CaseMgmtCostId);
                this.db.ExecuteNonQuery(comm);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion
    }
}
