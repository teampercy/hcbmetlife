﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace HCBBLL
{
    public class ClientInfo
    {
        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public ClientInfo()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties
        public int SchemeNumberId
        { get; set; }
        public int SchemeNameId
        { get; set; }
        public string SchemeNumber
        { get; set; }
        public int HCBReference
        { get; set; }
        public int IsEditServiceReq { get; set; }
        public bool IsDocumentUpload
        { get; set; }
        public int IncoID
        { get; set; }

        public string IncoName
        { get; set; }

        public int HCBNurseID
        { get; set; }

        public string NurseName
        { get; set; }

        public int ClaimHandlerID
        { get; set; }

        public string ClaimHandlerName
        { get; set; }

        public string ClaimHandlerEmail
        { get; set; }

        public string ClaimHandlerTelephone
        { get; set; }

        public int AssessorTeamID
        { get; set; }

        public string AssessorTeamName
        { get; set; }

        public string AssessorTeamTelephone
        { get; set; }

        public string AssessorTeamEmail
        { get; set; }

        public string ClientSurname
        { get; set; }

        public string ClientForenames
        { get; set; }

        public string ClientDOB
        { get; set; }

        public string ClientAddress
        { get; set; }

        public string ClientPostCode
        { get; set; }

        public string ClientHomeTelephone
        { get; set; }

        public string ClientWorkTelephone
        { get; set; }

        public string ClientMobileTelephone
        { get; set; }

        public string ClientOccupation
        { get; set; }
        public string EmployerName
        { get; set; }
        public string EmployerContact
        { get; set; }
        public string EmployerEmail
        { get; set; }
        public string Gender
        { get; set; }

        public string EligibleEAP
        { get; set; }

        public string ClientOccupationClass
        { get; set; }

        public string ClientServiceReq
        { get; set; }

        public int ClientEmployeeStatus
        { get; set; }

        public string IncapacityDay
        { get; set; }

        public int IllnessInjury
        { get; set; }

        public string DisabilityType
        { get; set; }

        //public string ICD9Code
        //{ get; set; }

        public string HCBGroupInstructedDate
        { get; set; }

        public string ClaimCloseDate
        { get; set; }

        public int ClaimCloseReason
        { get; set; }

        public string ClaimDetails
        { get; set; }
        public int ClaimReasonClosed
        { get; set; }

        public string HCBReceivedDate
        { get; set; }

        public string DateSentToNurse
        { get; set; }

        public string ReturnToWorkDate
        { get; set; }

        public string ReturnToWorkReason
        { get; set; }

        public string DateClosed
        { get; set; }

        public int CancellationReason
        { get; set; }

        public double FeeCharged
        { get; set; }

        public double RehabCost
        { get; set; }


        public string Salutation
        { get; set; }

        public string ClientAddress1
        {
            get;
            set;
        }
        public string ClientAddress2
        {
            get;
            set;
        }

        public string ClientPostalCode
        {
            get;
            set;
        }
        public string ClientCounty
        {
            get;
            set;
        }

        public string ClientCity
        {
            get;
            set;
        }

        public string DateCancelled
        {
            get;
            set;
        }
        //public string DateClaimantfirstContact
        //{
        //    get;
        //    set;
        //}

        public string AssessorTeamOther
        {
            get;
            set;
        }
        public string InjuryOther
        {
            get;
            set;
        }

        public string ClaimReasonOther
        {
            get;
            set;
        }

        public string ServiceRequiredOther
        {
            get;
            set;
        }

        public string FirstContactClaimantDate
        { get; set; }

        public string TypeOfContact
        { get; set; }

        public string TypeOfContactDetails
        { get; set; }

        public string FirstVerbalContactClaimantDate
        { get; set; }

        public string NextReviewDate
        { get; set; }

        public bool IsNextReviewDateEmailSent
        { get; set; }

        public string LastNextReviewDate
        { get; set; }

        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public int LastModifiedBy { get; set; }
        #endregion

        #region Methods        
        //public DataSet GetDetailUsersEmailId(string EmailId)
        public DataSet GetDetailUsersEmailId(int LoggedInUserID)
        {
            DataSet ds = new DataSet();
            try
            {
                string queryClient = "GetUserAsPerEmailId";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                //if (!string.IsNullOrEmpty(EmailId))
                //{
                //    this.db.AddInParameter(cmd, "EmailAddress", DbType.String, EmailId);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "EmailAddress", DbType.String, DBNull.Value);
                //}
                if (LoggedInUserID > 0)
                {
                    this.db.AddInParameter(cmd, "LoggedInUserID", DbType.Int32, LoggedInUserID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "LoggedInUserID", DbType.Int32, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "UserDet";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }

        public DataSet GetUsersEmailAddress(Int32 UserId)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetUsersEmailAddress";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                if (UserId > 0)
                {
                    this.db.AddInParameter(cmd, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "UserId", DbType.Int32, DBNull.Value);
                }
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "UserDet";

                if (ds != null & ds.Tables["UserDet"].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public DataSet GetAdminEmailAddress(Int32 HCBReference)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetAdminEmailAddress";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                if (HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "UserDet";

                if (ds != null & ds.Tables["UserDet"].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public DataSet GetClientList(int UserId, string usertype, bool Isopen)
        {
            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            try
            {
                //string strQuery = "SELECT ClientRecord.HCBReference AS HCBReference, ClientRecord.HCBNurseID, ClientRecord.NurseName, ClientRecord.ClaimHandlerID, ClientRecord.ClaimHandlerName, ClientRecord.ClientForenames&' '&ClientRecord.ClientSurname AS ClientName, Format(ClientRecord.ClientDOB,\"dd/MM/yyyy\") AS ClientDOB, \"DCP\"+Right(\"00000\" +CStr(ClientRecord.HCBReference),5) AS RefNo  FROM ClientRecord ";
                //string strCond = " WHERE ";
                //if (Isopen)
                //    strCond += " ( DateClosed Is Null Or DateClosed>DATE() )"; //open
                //else
                //    strCond += " ( DateClosed Is Not Null Or DateClosed<=DATE())"; //Close
                //switch (usertype.ToLower())
                //{
                //    case "nurse":
                //        strCond += " and ClientRecord.HCBNurseID = " + UserId; //nurse
                //        break;
                //    case "admin":
                //        break;
                //    default:
                //        break;
                //}
                //strCond += " ORDER BY ClientRecord.HCBReference;";
                //conn = DbConnection.OpenConnection();                
                //string queryClient = strQuery + strCond;                
                //DbCommand cmd = this.db.GetSqlStringCommand(queryClient);
                //ds = this.db.ExecuteDataSet(cmd);

                string queryClient = "GetClientList";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(cmd, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(cmd, "usertype", DbType.String, DBNull.Value);
                }
                if (UserId > 0)
                {
                    this.db.AddInParameter(cmd, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "UserId", DbType.Int32, DBNull.Value);
                }
                this.db.AddInParameter(cmd, "Isopen", DbType.Boolean, Isopen);

                ds = this.db.ExecuteDataSet(cmd);


                ds.Tables[0].TableName = "Client";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds = null;
                //throw;
            }

            return ds;
        }

        public DataSet GetclientClosedList()
        {
            DataSet ds = new DataSet();

            try
            {

                string queryClient = "GetClientDetClosed";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Client";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds = null;
            }

            return ds;
        }
        public DataSet CheckServiceReqAlter()
        {
            DataSet ds = new DataSet();

            try
            {

                string queryClient = "CheckServiceReqAlter";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClientServiceReq))
                {
                    this.db.AddInParameter(cmd, "ClientServiceReq", DbType.String, this.ClientServiceReq);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ClientServiceReq", DbType.String, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Client";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds = null;
            }

            return ds;
        }
        public DataSet GetDetailServiceRequired(int ServiceRequiredId)
        {
            DataSet ds = new DataSet();

            try
            {

                string queryClient = "GetDetailServiceRequired";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                if (ServiceRequiredId > 0)
                {
                    this.db.AddInParameter(cmd, "ServiceRequiredID", DbType.Int32, ServiceRequiredId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ServiceRequiredID", DbType.Int32, DBNull.Value);
                }
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "ServiceRequired";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds = null;
            }

            return ds;
        }
        public DataSet GetClientDet(string HCBRef)
        {
            DataSet ds = new DataSet();

            try
            {

                string queryClient = "GetClientInfo";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, HCBRef);
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "ClientInfo";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {
                //throw;
                ds = null;
            }

            return ds;
        }

        public int Save()
        {
            int hid = 0;

            if (this.HCBReference == 0)
            {
                hid = this.Insert();
                return hid;
            }
            else
            {
                if (this.HCBReference > 0)
                {
                    if (this.Update())
                    {
                        return this.HCBReference;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    this.HCBReference = 0;
                    return 0;
                }
            }
        }

        private int Insert()
        {
            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {
                conn = DbConnection.OpenConnection();
                string queryClient = "CreateClientRecord";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                this.db.AddOutParameter(cmd, "@HCBReference", DbType.Int32, 1024);

                if (this.IncoID > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("IncoID", OleDbType.Integer)).Value = this.IncoID;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("IncoID", OleDbType.Integer)).Value = DBNull.Value;

                }
                if (!String.IsNullOrEmpty(this.IncoName))
                {
                    cmd.Parameters.Add(new OleDbParameter("IncoName", OleDbType.VarChar)).Value = this.IncoName;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("IncoName", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (this.HCBNurseID > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBNurseID", OleDbType.Integer)).Value = this.HCBNurseID;
                }
                else
                {
                    cmd.Parameters.Add("HCBNurseID", OleDbType.Integer).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.NurseName))
                {
                    cmd.Parameters.Add(new OleDbParameter("NurseName", OleDbType.VarChar)).Value = this.NurseName;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("NurseName", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (this.ClaimHandlerID > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerID", OleDbType.Integer)).Value = this.ClaimHandlerID;

                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerID", OleDbType.Integer)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClaimHandlerName))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerName", OleDbType.VarChar)).Value = this.ClaimHandlerName;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerName", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClaimHandlerEmail))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerEmail", OleDbType.VarChar)).Value = this.ClaimHandlerEmail;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerEmail", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClaimHandlerTelephone))
                {

                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerTelephone", OleDbType.VarChar)).Value = this.ClaimHandlerTelephone;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimHandlerTelephone", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (this.AssessorTeamID > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamID", OleDbType.Integer)).Value = this.AssessorTeamID;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamID", OleDbType.Integer)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.AssessorTeamName))
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamName", OleDbType.VarChar)).Value = this.AssessorTeamName;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamName", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.AssessorTeamTelephone))
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamTelephone", OleDbType.VarChar)).Value = this.AssessorTeamTelephone;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamTelephone", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.AssessorTeamEmail))
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamEmail", OleDbType.VarChar)).Value = this.AssessorTeamEmail;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("AssessorTeamEmail", OleDbType.VarChar)).Value = DBNull.Value;
                }

                //if (!string.IsNullOrEmpty(this.Salutation))
                //{
                //    cmd.Parameters.Add(new OleDbParameter("Salutation", OleDbType.VarChar)).Value = this.Salutation;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("Salutation", OleDbType.VarChar)).Value = DBNull.Value;
                //}

                if (!string.IsNullOrEmpty(this.ClientSurname))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientSurname", OleDbType.VarChar)).Value = this.ClientSurname;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientSurname", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClientForenames))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientForenames", OleDbType.VarChar)).Value = this.ClientForenames;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientForenames", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClientDOB))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientDOB", OleDbType.Date)).Value = this.ClientDOB;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientDOB", OleDbType.Date)).Value = DBNull.Value;
                }
                //if (!string.IsNullOrEmpty(this.ClientAddress))
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClientAddress", OleDbType.VarChar)).Value = this.ClientAddress;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClientAddress", OleDbType.VarChar)).Value = DBNull.Value;
                //}
                if (!string.IsNullOrEmpty(this.ClientPostCode))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientPostCode", OleDbType.VarChar)).Value = this.ClientPostCode;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientPostCode", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClientHomeTelephone))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientHomeTelephone", OleDbType.VarChar)).Value = this.ClientHomeTelephone;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientHomeTelephone", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClientWorkTelephone))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientWorkTelephone", OleDbType.VarChar)).Value = this.ClientWorkTelephone;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientWorkTelephone", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClientMobileTelephone))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientMobileTelephone", OleDbType.VarChar)).Value = this.ClientMobileTelephone;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientMobileTelephone", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClientOccupation))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientOccupation", OleDbType.VarChar)).Value = this.ClientOccupation;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientOccupation", OleDbType.VarChar)).Value = DBNull.Value;
                }
                //if (!string.IsNullOrEmpty(this.ClientOccupationClass))
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClientOccupationClass", OleDbType.VarChar)).Value = this.ClientOccupationClass;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClientOccupationClass", OleDbType.VarChar)).Value = DBNull.Value;
                //}
                if (!string.IsNullOrEmpty(this.ClientServiceReq))
                {
                    this.db.AddInParameter(cmd, "@ClientServiceReq", DbType.String, this.ClientServiceReq);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientServiceReq", DbType.String, DBNull.Value);
                }

                //if (this.ClientEmployeeStatus > 0)
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClientEmployeeStatus", OleDbType.Integer)).Value = this.ClientEmployeeStatus;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClientEmployeeStatus", OleDbType.Integer)).Value = DBNull.Value;
                //}
                if (!string.IsNullOrEmpty(this.IncapacityDay))
                {
                    cmd.Parameters.Add(new OleDbParameter("IncapacityDay", OleDbType.VarChar)).Value = this.IncapacityDay;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("IncapacityDay", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (this.IllnessInjury > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("IllnessInjury", OleDbType.Integer)).Value = this.IllnessInjury;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("IllnessInjury", OleDbType.Integer)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.DisabilityType))
                {
                    cmd.Parameters.Add(new OleDbParameter("DisabilityType", OleDbType.VarChar)).Value = this.DisabilityType;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("DisabilityType", OleDbType.VarChar)).Value = DBNull.Value;
                }
                //if (!string.IsNullOrEmpty(this.ICD9Code))
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ICD9Code", OleDbType.VarChar)).Value = this.ICD9Code;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ICD9Code", OleDbType.VarChar)).Value = DBNull.Value;
                //}
                if (!string.IsNullOrEmpty(this.HCBGroupInstructedDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBGroupInstructedDate", OleDbType.VarChar)).Value = this.HCBGroupInstructedDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBGroupInstructedDate", OleDbType.VarChar)).Value = DBNull.Value;
                }

                if (!string.IsNullOrEmpty(this.ClaimCloseDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimCloseDate", OleDbType.VarChar)).Value = this.ClaimCloseDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimCloseDate", OleDbType.VarChar)).Value = DBNull.Value;
                }
                //if (!string.IsNullOrEmpty(this.ClaimReasonClosed))
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClaimCloseReason", OleDbType.VarChar)).Value = this.ClaimReasonClosed;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("ClaimCloseReason", OleDbType.VarChar)).Value = DBNull.Value;
                //}

                if (this.ClaimCloseReason > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimCloseReason", OleDbType.Integer)).Value = this.ClaimCloseReason;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimCloseReason", OleDbType.Integer)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClaimDetails))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimDetails", OleDbType.VarChar)).Value = this.ClaimDetails;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimDetails", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.HCBReceivedDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBReceivedDate", OleDbType.VarChar)).Value = this.HCBReceivedDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBReceivedDate", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.DateSentToNurse))
                {
                    cmd.Parameters.Add(new OleDbParameter("DateSentToNurse", OleDbType.VarChar)).Value = this.DateSentToNurse;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("DateSentToNurse", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ReturnToWorkDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("ReturnToWorkDate", OleDbType.VarChar)).Value = this.ReturnToWorkDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ReturnToWorkDate", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ReturnToWorkReason))
                {
                    cmd.Parameters.Add(new OleDbParameter("ReturnToWorkReason", OleDbType.VarChar)).Value = this.ReturnToWorkReason;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ReturnToWorkReason", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.DateClosed))
                {
                    cmd.Parameters.Add(new OleDbParameter("DateClosed", OleDbType.VarChar)).Value = this.DateClosed;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("DateClosed", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (this.CancellationReason > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("CancellationReason", OleDbType.Integer)).Value = this.CancellationReason;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("CancellationReason", OleDbType.Integer)).Value = DBNull.Value;
                }
                if (this.FeeCharged > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("FeeCharged", OleDbType.Double)).Value = this.FeeCharged;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("FeeCharged", OleDbType.Double)).Value = DBNull.Value;
                }
                if (this.RehabCost > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("RehabCost", OleDbType.Double)).Value = this.RehabCost;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("RehabCost", OleDbType.Double)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.Gender))
                {
                    cmd.Parameters.Add(new OleDbParameter("Gender", OleDbType.VarChar)).Value = this.Gender;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("Gender", OleDbType.VarChar)).Value = DBNull.Value;
                }

                if (!string.IsNullOrEmpty(this.EligibleEAP))
                {
                    cmd.Parameters.Add(new OleDbParameter("EligibleEAP", OleDbType.VarChar)).Value = this.EligibleEAP;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("EligibleEAP", OleDbType.VarChar)).Value = DBNull.Value;
                }

                if (!string.IsNullOrEmpty(this.EmployerName))
                {
                    cmd.Parameters.Add(new OleDbParameter("EmployerName", OleDbType.VarChar)).Value = this.EmployerName;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("EmployerName", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.EmployerContact))
                {
                    cmd.Parameters.Add(new OleDbParameter("EmployerContact", OleDbType.VarChar)).Value = this.EmployerContact;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("EmployerContact", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.EmployerEmail))
                {
                    cmd.Parameters.Add(new OleDbParameter("EmployerEmail", OleDbType.VarChar)).Value = this.EmployerEmail;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("EmployerEmail", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.ClientAddress1))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientAddress1", OleDbType.VarChar)).Value = this.ClientAddress1;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientAddress1", OleDbType.VarChar)).Value = DBNull.Value;
                }


                if (!string.IsNullOrEmpty(this.ClientAddress2))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientAddress2", OleDbType.VarChar)).Value = this.ClientAddress2;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientAddress2", OleDbType.VarChar)).Value = DBNull.Value;
                }


                if (!string.IsNullOrEmpty(this.ClientCity))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientCity", OleDbType.VarChar)).Value = this.ClientCity;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientCity", OleDbType.VarChar)).Value = DBNull.Value;
                }

                if (!string.IsNullOrEmpty(this.ClientCounty))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientCounty", OleDbType.VarChar)).Value = this.ClientCounty;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClientCounty", OleDbType.VarChar)).Value = DBNull.Value;
                }
                cmd.Parameters.Add(new OleDbParameter("IsDocumentUpload", OleDbType.Boolean)).Value = this.IsDocumentUpload;
                if (!string.IsNullOrEmpty(this.DateCancelled))
                {
                    cmd.Parameters.Add(new OleDbParameter("DateCancelled", OleDbType.VarChar)).Value = this.DateCancelled;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("DateCancelled", OleDbType.VarChar)).Value = DBNull.Value;
                }
                //if (!string.IsNullOrEmpty(this.DateClaimantfirstContact))
                //{
                //    cmd.Parameters.Add(new OleDbParameter("DateClaimantFirstContacted", OleDbType.VarChar)).Value = this.DateClaimantfirstContact;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("DateClaimantFirstContacted", OleDbType.VarChar)).Value = DBNull.Value;
                //}

                if (!String.IsNullOrEmpty(this.AssessorTeamOther))
                {
                    cmd.Parameters.Add(new OleDbParameter("AssesorTeamOther", OleDbType.VarChar)).Value = this.AssessorTeamOther;

                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("AssesorTeamOther", OleDbType.VarChar)).Value = DBNull.Value;
                }

                if (!String.IsNullOrEmpty(this.InjuryOther))
                {
                    cmd.Parameters.Add(new OleDbParameter("InjuryOther", OleDbType.VarChar)).Value = this.InjuryOther;

                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("InjuryOther", OleDbType.VarChar)).Value = DBNull.Value;
                }


                if (!String.IsNullOrEmpty(this.ClaimReasonOther))
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimReasonOther", OleDbType.VarChar)).Value = this.ClaimReasonOther;

                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ClaimReasonOther", OleDbType.VarChar)).Value = DBNull.Value;
                }

                if (!String.IsNullOrEmpty(this.ServiceRequiredOther))
                {
                    cmd.Parameters.Add(new OleDbParameter("ServiceRequiredOther", OleDbType.VarChar)).Value = this.ServiceRequiredOther;

                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("ServiceRequiredOther", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (this.SchemeNameId > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("SchemeName", OleDbType.Integer)).Value = this.SchemeNameId;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("SchemeName", OleDbType.Integer)).Value = DBNull.Value;

                }
                if (!String.IsNullOrEmpty(this.SchemeNumber))
                {
                    cmd.Parameters.Add(new OleDbParameter("SchemeNumber", OleDbType.VarChar)).Value = this.SchemeNumber;

                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("SchemeNumber", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.FirstContactClaimantDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("FirstContactClaimantDate", OleDbType.VarChar)).Value = this.FirstContactClaimantDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("FirstContactClaimantDate", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.TypeOfContact))
                {
                    cmd.Parameters.Add(new OleDbParameter("TypeOfContact", OleDbType.VarChar)).Value = this.TypeOfContact;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("TypeOfContact", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.TypeOfContactDetails))
                {
                    cmd.Parameters.Add(new OleDbParameter("TypeOfContactDetails", OleDbType.VarChar)).Value = this.TypeOfContactDetails;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("TypeOfContactDetails", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.FirstVerbalContactClaimantDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("FirstVerbalContactClaimantDate", OleDbType.VarChar)).Value = this.FirstVerbalContactClaimantDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("FirstVerbalContactClaimantDate", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(this.NextReviewDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("NextReviewDate", OleDbType.VarChar)).Value = this.NextReviewDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("NextReviewDate", OleDbType.VarChar)).Value = DBNull.Value;
                }

                cmd.Parameters.Add(new OleDbParameter("IsNextReviewDateEmailSent", OleDbType.Boolean)).Value = this.IsNextReviewDateEmailSent;

                if (!string.IsNullOrEmpty(this.LastNextReviewDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("LastNextReviewDate", OleDbType.VarChar)).Value = this.LastNextReviewDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("LastNextReviewDate", OleDbType.VarChar)).Value = DBNull.Value;
                }

                if(this.CreatedOn>DateTime.MinValue)
                {                    
                    this.db.AddInParameter(cmd, "@CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CreatedOn", DbType.DateTime, DBNull.Value);
                }

                if (this.CreatedBy > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("CreatedBy", OleDbType.Integer)).Value = this.CreatedBy;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("CreatedBy", OleDbType.Integer)).Value = DBNull.Value;
                }
                                
                //if (this.SchemeNumberId > 0)
                //{
                //    cmd.Parameters.Add(new OleDbParameter("SchemeNumberId", OleDbType.Integer)).Value = this.SchemeNumberId;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new OleDbParameter("SchemeNumberId", OleDbType.Integer)).Value = DBNull.Value;

                //}
                //string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();
                id = Convert.ToInt32(this.db.GetParameterValue(cmd, "@HCBReference"));
                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                conn.Close();
                ds.Dispose();
                return 0;
            }
            finally
            {

                DbConnection.CloseConnection(conn);
            }

            return id;
            //return true;
        }

        private bool Update()
        {
            DataSet ds = new DataSet();

            try
            {

                string queryClient = "UpdateClientRecord";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);

                this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                if (this.IncoID > 0)
                {
                    this.db.AddInParameter(cmd, "@IncoID", DbType.Int32, this.IncoID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@IncoID", DbType.Int32, DBNull.Value);

                }
                if (!String.IsNullOrEmpty(this.IncoName))
                {
                    this.db.AddInParameter(cmd, "@IncoName", DbType.String, this.IncoName);

                }
                else
                {
                    this.db.AddInParameter(cmd, "@IncoName", DbType.String, DBNull.Value);
                }
                if (this.HCBNurseID > 0)
                {
                    this.db.AddInParameter(cmd, "@HCBNurseID", DbType.Int32, this.HCBNurseID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBNurseID", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.NurseName))
                {
                    this.db.AddInParameter(cmd, "@NurseName", DbType.String, this.NurseName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@NurseName", DbType.String, DBNull.Value);
                }
                if (this.ClaimHandlerID > 0)
                {
                    this.db.AddInParameter(cmd, "@ClaimHandlerID", DbType.Int32, this.ClaimHandlerID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimHandlerID", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClaimHandlerName))
                {
                    this.db.AddInParameter(cmd, "@ClaimHandlerName", DbType.String, this.ClaimHandlerName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimHandlerName", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClaimHandlerEmail))
                {
                    this.db.AddInParameter(cmd, "@ClaimHandlerEmail", DbType.String, this.ClaimHandlerEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimHandlerEmail", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClaimHandlerTelephone))
                {
                    //cmd.Parameters.Add(this.ClaimHandlerTelephone));
                    this.db.AddInParameter(cmd, "@ClaimHandlerTelephone", DbType.String, this.ClaimHandlerTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimHandlerTelephone", DbType.String, DBNull.Value);
                }
                if (this.AssessorTeamID > 0)
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamID", DbType.Int32, this.AssessorTeamID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamID", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.AssessorTeamName))
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamName", DbType.String, this.AssessorTeamName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamName", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.AssessorTeamTelephone))
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamTelephone", DbType.String, this.AssessorTeamTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamTelephone", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.AssessorTeamEmail))
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamEmail", DbType.String, this.AssessorTeamEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AssessorTeamEmail", DbType.String, DBNull.Value);
                }
                //if (!string.IsNullOrEmpty(this.Salutation))
                //{
                //    this.db.AddInParameter(cmd, "@Salutation", DbType.String,  this.Salutation);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@Salutation", DbType.String,   DBNull.Value);
                //}
                if (!string.IsNullOrEmpty(this.ClientSurname))
                {
                    this.db.AddInParameter(cmd, "@ClientSurname", DbType.String, this.ClientSurname);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientSurname", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClientForenames))
                {
                    this.db.AddInParameter(cmd, "@ClientForenames", DbType.String, this.ClientForenames);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientForenames", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClientDOB))
                {
                    this.db.AddInParameter(cmd, "@ClientDOB", DbType.String, this.ClientDOB);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientDOB", DbType.String, DBNull.Value);
                }
                //if (!string.IsNullOrEmpty(this.ClientAddress))
                //{
                //    this.db.AddInParameter(cmd, "@ClientAddress", DbType.String,  this.ClientAddress);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@ClientAddress", DbType.String,  DBNull.Value);
                //}
                if (!string.IsNullOrEmpty(this.ClientPostCode))
                {
                    this.db.AddInParameter(cmd, "@ClientPostCode", DbType.String, this.ClientPostCode);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientPostCode", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClientHomeTelephone))
                {
                    this.db.AddInParameter(cmd, "@ClientHomeTelephone", DbType.String, this.ClientHomeTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientHomeTelephone", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClientWorkTelephone))
                {
                    this.db.AddInParameter(cmd, "@ClientWorkTelephone", DbType.String, this.ClientWorkTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientWorkTelephone", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClientMobileTelephone))
                {
                    this.db.AddInParameter(cmd, "@ClientMobileTelephone", DbType.String, this.ClientMobileTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientMobileTelephone", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClientOccupation))
                {
                    this.db.AddInParameter(cmd, "@ClientOccupation", DbType.String, this.ClientOccupation);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientOccupation", DbType.String, DBNull.Value);
                }
                //if (!string.IsNullOrEmpty(this.ClientOccupationClass))
                //{
                //    this.db.AddInParameter(cmd, "@ClientOccupationClass", DbType.String, this.ClientOccupationClass);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@ClientOccupationClass", DbType.String, DBNull.Value);
                //}

                if (!string.IsNullOrEmpty(this.ClientServiceReq))
                {
                    this.db.AddInParameter(cmd, "@ClientServiceReq", DbType.String, this.ClientServiceReq);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientServiceReq", DbType.String, DBNull.Value);
                }
                //if (this.ClientEmployeeStatus > 0)
                //{
                //    this.db.AddInParameter(cmd, "@ClientEmployeeStatus", DbType.Int32, this.ClientEmployeeStatus);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@ClientEmployeeStatus", DbType.Int32, DBNull.Value);
                //}
                if (!string.IsNullOrEmpty(this.IncapacityDay))
                {
                    this.db.AddInParameter(cmd, "@IncapacityDay", DbType.String, this.IncapacityDay);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@IncapacityDay", DbType.String, DBNull.Value);
                }
                if (this.IllnessInjury > 0)
                {
                    this.db.AddInParameter(cmd, "@IllnessInjury", DbType.Int32, this.IllnessInjury);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@IllnessInjury", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.DisabilityType))
                {
                    this.db.AddInParameter(cmd, "@DisabilityType", DbType.String, this.DisabilityType);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@DisabilityType", DbType.String, DBNull.Value);
                }
                //if (!string.IsNullOrEmpty(this.ICD9Code))
                //{
                //    this.db.AddInParameter(cmd, "@ICD9Code", DbType.String,  this.ICD9Code);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@ICD9Code", DbType.String,  DBNull.Value);
                //}
                if (!string.IsNullOrEmpty(this.HCBGroupInstructedDate))
                {
                    this.db.AddInParameter(cmd, "@HCBGroupInstructedDate", DbType.String, this.HCBGroupInstructedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBGroupInstructedDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClaimCloseDate))
                {
                    this.db.AddInParameter(cmd, "@ClaimCloseDate", DbType.String, this.ClaimCloseDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimCloseDate", DbType.String, DBNull.Value);
                }
                if (this.ClaimCloseReason > 0)
                {
                    this.db.AddInParameter(cmd, "@ClaimCloseReason", DbType.Int32, this.ClaimCloseReason);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimCloseReason", DbType.Int32, DBNull.Value);
                }
                //if (!string.IsNullOrEmpty(this.ClaimReasonClosed))
                //{
                //    this.db.AddInParameter(cmd, "@ClaimCloseReason", DbType.String, this.ClaimReasonClosed);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@ClaimCloseReason", DbType.String, DBNull.Value);
                //}

                if (!string.IsNullOrEmpty(this.ClaimDetails))
                {
                    this.db.AddInParameter(cmd, "@ClaimDetails", DbType.String, this.ClaimDetails);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimDetails", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.HCBReceivedDate))
                {
                    this.db.AddInParameter(cmd, "@HCBReceivedDate", DbType.String, this.HCBReceivedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReceivedDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.DateSentToNurse))
                {
                    this.db.AddInParameter(cmd, "@DateSentToNurse", DbType.String, this.DateSentToNurse);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@DateSentToNurse", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReturnToWorkDate))
                {
                    this.db.AddInParameter(cmd, "@ReturnToWorkDate", DbType.String, this.ReturnToWorkDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReturnToWorkDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReturnToWorkReason))
                {
                    this.db.AddInParameter(cmd, "@ReturnToWorkReason", DbType.String, this.ReturnToWorkReason);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReturnToWorkReason", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.DateClosed))
                {
                    this.db.AddInParameter(cmd, "@DateClosed", DbType.String, this.DateClosed);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@DateClosed", DbType.String, DBNull.Value);
                }

                if (this.CancellationReason > 0)
                {
                    this.db.AddInParameter(cmd, "@CancellationReason", DbType.Int32, this.CancellationReason);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CancellationReason", DbType.Int32, DBNull.Value);
                }
                if (this.FeeCharged > 0)
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Double, this.FeeCharged);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Double, DBNull.Value);
                }
                if (this.RehabCost > 0)
                {
                    this.db.AddInParameter(cmd, "@RehabCost", DbType.Double, this.RehabCost);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabCost", DbType.Double, DBNull.Value);
                }


                if (!string.IsNullOrEmpty(this.Gender))
                {
                    this.db.AddInParameter(cmd, "@Gender", DbType.String, this.Gender);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Gender", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.EligibleEAP))
                {
                    this.db.AddInParameter(cmd, "@EligibleEAP", DbType.String, this.EligibleEAP);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@EligibleEAP", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.EmployerName))
                {
                    this.db.AddInParameter(cmd, "@EmployerName", DbType.String, this.EmployerName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@EmployerName", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.EmployerContact))
                {
                    this.db.AddInParameter(cmd, "@EmployerContact", DbType.String, this.EmployerContact);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@EmployerContact", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.EmployerEmail))
                {
                    this.db.AddInParameter(cmd, "@EmployerEmail", DbType.String, this.EmployerEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@EmployerEmail", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.ClientAddress1))
                {
                    this.db.AddInParameter(cmd, "@ClientAddress1", DbType.String, this.ClientAddress1);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientAddress1", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.ClientAddress2))
                {
                    this.db.AddInParameter(cmd, "@ClientAddress2", DbType.String, this.ClientAddress2);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientAddress2", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.ClientCity))
                {
                    this.db.AddInParameter(cmd, "@ClientCity", DbType.String, this.ClientCity);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientCity", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.ClientCounty))
                {
                    this.db.AddInParameter(cmd, "@ClientCounty", DbType.String, this.ClientCounty);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClientCounty", DbType.String, DBNull.Value);
                }
                this.db.AddInParameter(cmd, "@IsDocumentUpload", DbType.Boolean, this.IsDocumentUpload);
                if (!string.IsNullOrEmpty(this.DateCancelled))
                {
                    this.db.AddInParameter(cmd, "@DateCancelled", DbType.String, this.DateCancelled);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@DateCancelled", DbType.String, DBNull.Value);
                }
                //if (!string.IsNullOrEmpty(this.DateClaimantfirstContact))
                //{
                //    this.db.AddInParameter(cmd, "@DateClaimantFirstContacted", DbType.String, this.DateClaimantfirstContact);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@DateClaimantFirstContacted", DbType.String,DBNull.Value);
                //}
                if (!string.IsNullOrEmpty(this.AssessorTeamOther))
                {
                    this.db.AddInParameter(cmd, "@AssesorTeamOther", DbType.String, this.AssessorTeamOther);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AssesorTeamOther", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.InjuryOther))
                {
                    this.db.AddInParameter(cmd, "@InjuryOther", DbType.String, this.InjuryOther);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@InjuryOther", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.ClaimReasonOther))
                {
                    this.db.AddInParameter(cmd, "@ClaimReasonOther", DbType.String, this.ClaimReasonOther);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ClaimReasonOther", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ServiceRequiredOther))
                {
                    this.db.AddInParameter(cmd, "@ServiceRequiredOther", DbType.String, this.ServiceRequiredOther);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ServiceRequiredOther", DbType.String, DBNull.Value);
                }


                if (this.SchemeNameId > 0)
                {
                    this.db.AddInParameter(cmd, "@SchemeName", DbType.Int32, this.SchemeNameId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@SchemeName", DbType.Int32, DBNull.Value);

                }
                if (!string.IsNullOrEmpty(this.SchemeNumber))
                {
                    this.db.AddInParameter(cmd, "@SchemeNumber", DbType.String, this.SchemeNumber);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@SchemeNumber", DbType.String, DBNull.Value);
                }
                //if (this.SchemeNumberId > 0)
                //{
                //    this.db.AddInParameter(cmd, "@SchemeNumberId", DbType.Int32, this.SchemeNumberId);                    
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@SchemeNumberId", DbType.Int32, DBNull.Value);    
                //}
                if (!string.IsNullOrEmpty(this.FirstContactClaimantDate))
                {
                    this.db.AddInParameter(cmd, "@FirstContactClaimantDate", DbType.String, this.FirstContactClaimantDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FirstContactClaimantDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.TypeOfContact))
                {
                    this.db.AddInParameter(cmd, "@TypeOfContact", DbType.String, this.TypeOfContact);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@TypeOfContact", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.TypeOfContactDetails))
                {
                    this.db.AddInParameter(cmd, "@TypeOfContactDetails", DbType.String, this.TypeOfContactDetails);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@TypeOfContactDetails", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.FirstVerbalContactClaimantDate))
                {
                    this.db.AddInParameter(cmd, "@FirstVerbalContactClaimantDate", DbType.String, this.FirstVerbalContactClaimantDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FirstVerbalContactClaimantDate", DbType.String, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.NextReviewDate))
                {
                    this.db.AddInParameter(cmd, "@NextReviewDate", DbType.String, this.NextReviewDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@NextReviewDate", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(cmd, "@IsNextReviewDateEmailSent", DbType.Boolean, this.IsNextReviewDateEmailSent);


                if (!string.IsNullOrEmpty(this.LastNextReviewDate))
                {
                    this.db.AddInParameter(cmd, "@LastNextReviewDate", DbType.String, this.LastNextReviewDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@LastNextReviewDate", DbType.String, DBNull.Value);
                }
                                
                if(this.LastModifiedOn>DateTime.MinValue)
                {
                    this.db.AddInParameter(cmd, "@LastModifiedOn", DbType.DateTime, this.LastModifiedOn);                    
                }
                else
                {
                    this.db.AddInParameter(cmd, "@LastModifiedOn", DbType.DateTime, DBNull.Value);
                }

                if(this.LastModifiedBy>0)
                {
                    this.db.AddInParameter(cmd, "@LastModifiedBy", DbType.Int32, this.LastModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@LastModifiedBy", DbType.Int32, DBNull.Value);
                }

                this.db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }
        public DataSet GetClientRecordAdminId(int HcbRef)
        {
            DataSet ds = new DataSet();
            try
            {

                string queryClient = "GetClientRecordAdminId";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                this.db.AddInParameter(cmd, "@HcbRef", DbType.Int32, HcbRef);
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "ClientInfo";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {
                //throw;
                ds = null;
            }
            return ds;
        }
        public DataSet GetSchemeNumber(int ShcemeNameId)
        {
            DataSet ds = new DataSet();
            try
            {

                string queryClient = "GetSchemeNumber";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                this.db.AddInParameter(cmd, "@ShcemeNameId", DbType.Int32, ShcemeNameId);
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "ShcemeName";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {
                //throw;
                ds = null;
            }
            return ds;
        }
        public static void SetAdmin(int HcbRef, int AdminID)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                //DbCommand comm = db.GetSqlStringCommand("update ClientRecord set    AdminId = '" + AdminID + "' where HCBReference = " + HcbRef);
                //db.ExecuteNonQuery(comm);

                string UpdateQuery = "UpdateClientRecordAdminId";

                DbCommand comm = db.GetStoredProcCommand(UpdateQuery);


                if (HcbRef > 0)
                {
                    db.AddInParameter(comm, "HcbRef", DbType.Int32, HcbRef);
                }
                else
                {
                    db.AddInParameter(comm, "HcbRef", DbType.Int32, DBNull.Value);
                }
                if (AdminID > 0)
                {
                    db.AddInParameter(comm, "AdminID", DbType.Int32, AdminID);
                }
                else
                {
                    db.AddInParameter(comm, "AdminID", DbType.Int32, DBNull.Value);
                }


                db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                //throw;
            }
        }
        public int ClientExists(string Surname, string DOB)
        {
            int Id = 0;
            DbCommand com = null;

            try
            {
                string Query = "GetClientBySurnameAndDOB";
                com = this.db.GetStoredProcCommand(Query);



                this.db.AddInParameter(com, "@Surname", DbType.String, Surname);
                this.db.AddInParameter(com, "@ClientDOB", DbType.DateTime, DOB);

                Id = Convert.ToInt32(this.db.ExecuteScalar(com));
            }
            catch (Exception ex)
            {
                Id = -1;
            }

            return Id;
        }

        //public int ClientExists(string Surname, string DOB)
        //{
        //    int Id = 0;
        //    DbCommand com = null;

        //    try
        //    {
        //        string Query = "SELECT ClientRecord.HCBReference, ClientRecord.ClientSurname, ClientRecord.ClientForenames, ClientRecord.ClientDOB FROM ClientRecord WHERE ClientRecord.ClientSurname=@Surname and ClientRecord.ClientDOB = @ClientDOB";
        //        com = this.db.GetSqlStringCommand(Query);

        //        this.db.AddInParameter(com, "@Surname", DbType.String, Surname);
        //        this.db.AddInParameter(com, "@ClientDOB", DbType.String, DOB);

        //        Id = Convert.ToInt32(this.db.ExecuteScalar(com));
        //    }
        //    catch (Exception ex)
        //    {
        //        Id = -1;
        //    }

        //    return Id;
        //}

        public bool updateClientRehabilationCostsTotal()
        {
            try
            {
                string UpdateQuery = "UpdateRehabilitationCostsTotal";

                DbCommand comm = this.db.GetStoredProcCommand(UpdateQuery);


                if (this.RehabCost > 0)
                {
                    this.db.AddInParameter(comm, "RehabCost", DbType.Double, this.RehabCost);
                }
                else
                {
                    this.db.AddInParameter(comm, "RehabCost", DbType.Double, DBNull.Value);
                }
                this.db.AddInParameter(comm, "HCBReference", DbType.Int32, this.HCBReference);

                this.db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region Dashboard Page Methods

        public int GetTotalClaimCounter(int UserId, string usertype, bool Isopen)
        {
            int ClaimCaseCounter = 0;
            using (DataSet dsClientInfo = GetClientList(UserId, usertype, Isopen))
            {
                if (dsClientInfo != null && dsClientInfo.Tables.Count > 0)
                {
                    using (DataTable dtClientInfo = dsClientInfo.Tables[0])
                    {
                        ClaimCaseCounter = dtClientInfo.Rows.Count;
                    }
                }
            }
            return ClaimCaseCounter;
        }

        public double GetAverageClaimDuration(int UserId, string usertype)
        {
            double AverageClaimDuration = 0;
            try
            {
                using (DataSet dsClientInfo = GetClientList(UserId, usertype, false))
                {
                    if (dsClientInfo != null && dsClientInfo.Tables.Count > 0)
                    {
                        using (DataTable dtClientInfo = dsClientInfo.Tables[0])
                        {
                            //AverageClaimDuration = dtClientInfo.AsEnumerable().Average(dr => (DateTime.Parse(dr["HCBReceivedDate"].ToString()) - DateTime.Parse(dr["DateClosed"].ToString())).TotalDays);
                            AverageClaimDuration = dtClientInfo.AsEnumerable().Average(dr => (DateTime.Parse(dr["DateClosed"].ToString()) - DateTime.Parse(dr["HCBReceivedDate"].ToString())).TotalDays);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                AverageClaimDuration = 0;
            }
            return AverageClaimDuration;
        }

        public int GetEmployerCount(int UserId, string usertype)
        {
            int EmployerCounter = 0;
            try
            {
                using (DataSet dsClientInfo = GetClientList(UserId, usertype, true))
                {
                    if (dsClientInfo != null && dsClientInfo.Tables.Count > 0)
                    {
                        using (DataTable dtClientInfo = dsClientInfo.Tables[0])
                        {
                            EmployerCounter = dtClientInfo.AsEnumerable().Where(dr => !string.IsNullOrEmpty(Convert.ToString(dr["EmployerName"]))).Select(dr => dr["EmployerName"]).Distinct().Count();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EmployerCounter = 0;
            }

            return EmployerCounter;
        }

        public Dictionary<string, int> GetTotalClaimPerEmployer(int UserId, string usertype)
        {
            Dictionary<string, int> ClaimPerEmployer = new Dictionary<string, int>();

            using (DataSet dsClientList = GetClientList(UserId, usertype, true))
            {
                if (dsClientList != null && dsClientList.Tables.Count > 0)
                {
                    using (DataTable dtClientList = dsClientList.Tables[0])
                    {
                        ClaimPerEmployer = (from drc in dtClientList.AsEnumerable()
                                            where !string.IsNullOrEmpty(Convert.ToString(drc["EmployerName"]))
                                            group drc by drc.Field<string>("EmployerName") into grp
                                            select new
                                            {
                                                EmployerName = grp.Key,
                                                TotalClaim = grp.Count()
                                            }).ToDictionary(k => k.EmployerName, k => k.TotalClaim);
                    }
                }
            }

            return ClaimPerEmployer;
        }

        //to get Highest cases Top 5 Brokers
        public DataSet GetDashboardTopFiveAssessorList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardTop5BrokerList");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardTopFiveBrokerList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        public Dictionary<string, int> GetTotalClaimPerBroker(int UserId, string usertype)
        {
            Dictionary<string, int> ClaimPerBroker = new Dictionary<string, int>();

            using (DataSet dsBrokerList = GetDashboardTopFiveAssessorList(UserId, usertype))
            {
                if (dsBrokerList != null && dsBrokerList.Tables.Count > 0)
                {
                    using (DataTable dtBrokerList = dsBrokerList.Tables[0])
                    {
                        ClaimPerBroker = (from drc in dtBrokerList.AsEnumerable()
                                          where !string.IsNullOrEmpty(Convert.ToString(drc["AssessorTeamName"]))
                                          select new
                                          {
                                              BrokerName = Convert.ToString(drc["AssessorTeamName"]),
                                              TotalCases = Convert.ToInt32(drc["BrokerCount"])
                                          }).ToDictionary(k => k.BrokerName, k => k.TotalCases);
                    }
                }
            }
            return ClaimPerBroker;
        }

        //to get Highest cases Top 5 Scheme List
        public DataSet GetDashboardTopFiveSchemeList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardTop5SchemeList");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardTopFiveSchemeList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        public Dictionary<string, int> GetTotalClaimPerScheme(int UserId, string usertype)
        {
            Dictionary<string, int> ClaimPerScheme = new Dictionary<string, int>();

            using (DataSet dsSchemeList = GetDashboardTopFiveSchemeList(UserId, usertype))
            {
                if (dsSchemeList != null && dsSchemeList.Tables.Count > 0)
                {
                    using (DataTable dtSchemeList = dsSchemeList.Tables[0])
                    {
                        ClaimPerScheme = (from drc in dtSchemeList.AsEnumerable()
                                          where !string.IsNullOrEmpty(Convert.ToString(drc["SchemeName"]))
                                          select new
                                          {
                                              SchemeName = Convert.ToString(drc["SchemeName"]),
                                              TotalCases = Convert.ToInt32(drc["SchemeCount"])
                                          }).ToDictionary(k => k.SchemeName, k => k.TotalCases);

                    }
                }
            }

            return ClaimPerScheme;
        }

        public Dictionary<string, int> GetTotalClaimPerInjury(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            Dictionary<string, int> ClaimPerInjury = new Dictionary<string, int>();
            try
            {
                using (DataSet dsClaimsPerInjury = GetDashboardIllnessInjuryPercentage(UserId, usertype, AssessorTeamID, SchemeNameId, startDate, endDate))
                {
                    if (dsClaimsPerInjury != null && dsClaimsPerInjury.Tables.Count > 0)
                    {
                        using (DataTable dtClaimsPerInjury = dsClaimsPerInjury.Tables[0])
                        {
                            foreach (DataRow dr in dtClaimsPerInjury.Rows)
                            {
                                ClaimPerInjury.Add(Convert.ToString(dr["IllnessInjuryName"]), (int)dr["InjuryCount"]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return ClaimPerInjury;
        }

        public Dictionary<string, double> GetTotalClaimPerReturnToWorkReason(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            Dictionary<string, double> ClaimPerReturnToWorkReason = new Dictionary<string, double>();
            try
            {
                DataSet dsClaimPerReturnToWorkReason = new DataSet();
                DbCommand com = db.GetStoredProcCommand("GetDashboardReturnToWorkReasonPercentage");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                dsClaimPerReturnToWorkReason = this.db.ExecuteDataSet(com);
                dsClaimPerReturnToWorkReason.Tables[0].TableName = "DashboardReturnToWorkReasonPercentage";

                if (dsClaimPerReturnToWorkReason != null & dsClaimPerReturnToWorkReason.Tables[0].Rows.Count < 1)
                    dsClaimPerReturnToWorkReason = null;

                if (dsClaimPerReturnToWorkReason != null & dsClaimPerReturnToWorkReason.Tables[0].Rows.Count >= 1)
                {
                    using (DataTable dtClaimPerReturnToWorkReason = dsClaimPerReturnToWorkReason.Tables[0])
                    {
                        foreach (DataRow dr in dtClaimPerReturnToWorkReason.Rows)
                        {
                            ClaimPerReturnToWorkReason.Add(Convert.ToString(dr["ReturnToWorkReason"]), Convert.ToDouble(dr["ReturnToWorkReasonPercentage"]));
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return ClaimPerReturnToWorkReason;
        }

        //public Dictionary<string, int> GetTotalClaimPerInjury()
        //{
        //    Dictionary<string, int> ClaimPerInjury = new Dictionary<string, int>();
        //    try
        //    {
        //        DbCommand comm = db.GetStoredProcCommand("usp_GetTotalClaimPerInjury");
        //        using (DataSet dsClaimsPerInjury = db.ExecuteDataSet(comm))
        //        {
        //            if (dsClaimsPerInjury != null && dsClaimsPerInjury.Tables.Count > 0)
        //            {
        //                using (DataTable dtClaimsPerInjury = dsClaimsPerInjury.Tables[0])
        //                {
        //                    foreach (DataRow dr in dtClaimsPerInjury.Rows)
        //                    {
        //                        ClaimPerInjury.Add(Convert.ToString(dr["Illness"]), (int)dr["ClaimCount"]);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return ClaimPerInjury;
        //}

        //To fetch Broker List on Dashboard:-Only those brokers whose cases are related to respected users

        public DataSet GetDashboardAssessorList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardBrokerList");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardBrokerList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }

        //To fetch Scheme List on Dashboard:-Only those Scheme Name whose cases are related to respected users
        public DataSet GetDashboardSchemeList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardSchemeList");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardSchemeList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Fetch respected users Case details on selection of Broker in dropdown
        public DataSet GetDashboradAssessorCasesList(int UserId, string usertype, bool Isopen, int AssessorTeamId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardBrokerCasesList");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "Isopen", DbType.Boolean, Isopen);

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardBrokerCasesList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Fetch respected users Case details on selection of scheme Name in dropdown
        public DataSet GetDashboradSchemeCasesList(int UserId, string usertype, bool Isopen, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardSchemeCasesList");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "Isopen", DbType.Boolean, Isopen);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardSchemeCasesList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Get Claimant Average Age for respected users assign cases
        public DataSet GetDashboardClaimantAverageAge(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardClaimantAverageAge");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardClaimantAverageAge";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Get Claimant Average Claim Duration for respected users assign cases
        public DataSet GetDashboardAverageClaimDuration(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardAverageClaimDuration");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardAverageClaimDuration";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Get Claimant Average RTW Duration for respected users assign cases
        public DataSet GetDashboardAverageRTWDuration(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardAverageRTWDuration");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardAverageRTWDuration";
                ds.Tables[1].TableName = "DashboardMonthlyAverageRTWDuration";

                if (ds != null & ds.Tables[0].Rows.Count < 1 & ds.Tables[1].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Get Case records from ClientRecord table where Initial Contact SLA exceeds 2 days & Initial Visit SLA exceed 5 days
        public DataSet GetSLAExceptionData(DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetSLAExceptionReport");

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "SLAExceptionTable";

                if (ds != null & ds.Tables[0].Rows.Count < 1 & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Get Case records from ClientRecord table where Initial Contact SLA exceeds 2 days & Initial Visit SLA exceed 5 days
        public DataSet GetMIFullCaseRecordData(DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetFullCaseRecordDetails");

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "FullCaseRecordDataTable";

                if (ds != null & ds.Tables[0].Rows.Count < 1 & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;
        }

        //Get Claimant Gender Percantage for respected users assign cases
        public DataSet GetDashboardClaimantGenderPercentage(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardClaimantGenderPercentage");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardClaimantGenderPercentage";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {

            }
            return ds;
        }

        //Get Illness Injury Percantage for respected users assign cases
        public DataSet GetDashboardIllnessInjuryPercentage(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardIllnessInjuryPercentage");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardIllnessInjuryPercentage";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {

            }
            return ds;
        }

        public DataSet GetDashboardClosedCasePercentage(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetDashboardClaimClosedReasonPercentage");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "DashboardClosedCasePercentage";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {

            }
            return ds;
        }

        // Get Metlife Management Information.
        public DataSet GetMetlifeManagementInformation(int UserId, string usertype, int AssessorTeamId, int SchemeNameId, DateTime startDate, DateTime endDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetMetlifeManagementInformation");
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                this.db.AddInParameter(com, "AssessorTeamId", DbType.Int32, AssessorTeamId);

                this.db.AddInParameter(com, "SchemeNameId", DbType.Int32, SchemeNameId);

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "TotalCases";
                ds.Tables[1].TableName = "ReturnToWorkReason";
                ds.Tables[2].TableName = "TotalCasesClosed";
                ds.Tables[3].TableName = "CancellationReason";
                ds.Tables[4].TableName = "AverageClaimsDuration";
                ds.Tables[5].TableName = "IllnessInjury";

                if (ds != null & ds.Tables[0].Rows.Count < 1 & ds.Tables[1].Rows.Count < 1)
                {
                    ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }
            return ds;

        }

        public List<DashboardInitialChart> GetInitialChartData(InitialChartTypes ChartType, int UserId, string usertype, DateTime startDate, DateTime endDate)
        {
            List<DashboardInitialChart> ChartData = new List<DashboardInitialChart>();
            try
            {
                string SPName = ChartType == InitialChartTypes.InstructionToClaimantContact ? "usp_GetInitialContactData" : "usp_GetInitialVisitData";
                DbCommand com = db.GetStoredProcCommand(SPName);
                if (!string.IsNullOrEmpty(usertype))
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, usertype);
                }
                else
                {
                    this.db.AddInParameter(com, "usertype", DbType.String, DBNull.Value);
                }

                if (UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
                }

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(com, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MinValue < endDate)
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(com, "enddate", DbType.DateTime, DBNull.Value);
                }

                using (DataSet ds = db.ExecuteDataSet(com))
                {
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        //ChartData.Add(GetChartDataFromDatatable("Duration", ds.Tables[0]));
                        ChartData.Add(GetChartDataFromDatatable("SLA", ds.Tables[2]));
                        ChartData.Add(GetChartDataFromDatatable("Average", ds.Tables[2]));
                    }
                }
            }
            catch (Exception ex)
            {
                //Log exception
            }
            return ChartData;
        }

        private DashboardInitialChart GetChartDataFromDatatable(string Legend, DataTable dtChartData)
        {

            DashboardInitialChart obj = new DashboardInitialChart();
            obj.Legend = Legend;
            List<InitialChartData> DataList = new List<InitialChartData>();
            /*It is assume that chart x axis will have months for now. So we are creating collection having 
             * month and data
            */
            foreach (DataRow dr in dtChartData.Rows)
            {
                DataList.Add(new InitialChartData()
                {
                    XaxisValue = Convert.ToString(dr["Month-Year"]),
                    YaxisValue = Convert.ToInt32(dr[Legend])
                });
            }
            obj.ChartData = DataList;
            return obj;
        }

        #endregion
    }



    public class DashboardInitialChart
    {
        public string Legend { get; set; }
        public List<InitialChartData> ChartData { get; set; }
    }

    public class InitialChartData
    {
        public string XaxisValue { get; set; }
        public int YaxisValue { get; set; }
    }

    public enum InitialChartTypes
    {
        InstructionToClaimantContact = 1,
        ClaimantContactToHomeVisit
    }
}
