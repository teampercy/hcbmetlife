﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class ClientRehabilitationCosts
    {
        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public ClientRehabilitationCosts()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

          #region Properties

        public int CRC_ID
        { get; set; }

        public int HCBReference
        { get; set; }

        public DateTime RehabDate
        { get; set; }

        public double Cost
        { get; set; }

        #endregion

        #region Methods

       public bool save()
       {
           if (this.CRC_ID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.CRC_ID > 0)
                {
                    return this.Update();
                }
            }

            return true;
       }

        private bool Insert()
        {           

            try
            {
                string InsertQuery = "InsertRehabilitationCosts";
               

                DbCommand comm = this.db.GetStoredProcCommand(InsertQuery);
                

                
                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(comm,"@HCBReference", DbType.Int32,this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(comm,"@HCBReference", DbType.Int32,DBNull.Value);
                }

                if (this.RehabDate !=null)
                {
                    this.db.AddInParameter(comm,"@RehabDate",DbType.DateTime , this.RehabDate);
                }
                else
                {
                    this.db.AddInParameter(comm,"@RehabDate",DbType.DateTime , DBNull.Value);
                }

                if (this.Cost > 0)
                {
                    this.db.AddInParameter(comm,"@Cost", DbType.Double,this.Cost);
                }
                else
                {
                    this.db.AddInParameter(comm,"@Cost", DbType.Double,DBNull.Value);
                }

                


                this.db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {               
                return false;
            }
                       
            return true;
        }

           private bool Update()
        {         

            try
            {
                string UpdateQuery = "UpdateRehabilitationCosts";
              
                DbCommand comm = this.db.GetStoredProcCommand(UpdateQuery);

               

                 if (this.RehabDate != null)
                 {
                     this.db.AddInParameter(comm, "RehabDate", DbType.DateTime, this.RehabDate);
                 }
                 else
                 {
                     this.db.AddInParameter(comm, "RehabDate", DbType.DateTime, DBNull.Value);
                 }

                 if (this.Cost > 0)
                 {
                     this.db.AddInParameter(comm, "Cost", DbType.Double, this.Cost);
                 }
                 else
                 {
                     this.db.AddInParameter(comm, "Cost", DbType.Double, DBNull.Value);
                 }
                 if (this.CRC_ID > 0)
                 {
                     this.db.AddInParameter(comm, "CRC_ID", DbType.Int32, this.CRC_ID);
                 }
                 else
                 {
                     this.db.AddInParameter(comm, "CRC_ID", DbType.Int32, DBNull.Value);
                 }
                
                 this.db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

           public bool Delete()
           {

               try
               {
                   string UpdateQuery = "DeleteRehabilitationCosts";

                   DbCommand comm = this.db.GetStoredProcCommand(UpdateQuery);

                   if (this.CRC_ID > 0)
                   {
                       this.db.AddInParameter(comm, "CRC_ID", DbType.Int32, this.CRC_ID);
                   }
                   else
                   {
                       this.db.AddInParameter(comm, "CRC_ID", DbType.Int32, DBNull.Value);
                   }

                   this.db.ExecuteNonQuery(comm);
               }
               catch (Exception ex)
               {
                   return false;
               }
               return true;
           }

         public DataSet GetClientRehabilitationCosts()
        {
            DataSet ds = new DataSet();
           
            try
            {
                string GetQuery = "GetClientRehabilitationCosts";
                DbCommand cmd = this.db.GetStoredProcCommand(GetQuery);

                 if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(cmd);

            }
            catch (Exception ex)
            {               
                ds = null;
            }

            return ds;
        }

         public Double GetClientRehabilitationCostsTotal()
         {
             double valueToReturn = 0;

             try
             {
                 string GetQuery = "GetClientRehabilitationCostsTotal";
                 DbCommand cmd = this.db.GetStoredProcCommand(GetQuery);

                 if (this.HCBReference > 0)
                 {
                     this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                 }
                 else
                 {
                     this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                 }

                 valueToReturn = Convert.ToDouble(this.db.ExecuteScalar(cmd));

             }
             catch (Exception ex)
             {
                 valueToReturn = 0;
             }

             return valueToReturn;
         }

        
        #endregion
    }
}
