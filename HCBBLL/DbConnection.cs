﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace HCBBLL
{
    public class DbConnection
    {
       
        public  static OleDbConnection  OpenConnection()
        {
            OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
            conn.Open();
            return conn;
        }
       
        public static  void CloseConnection(OleDbConnection conn)
        {
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
                conn = null;
            }
        }
    }
}
