﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace HCBBLL
{
    public class Documents
    {
        #region Variables
        Database db;
        #endregion

        #region constructor
        /// <summary>
        /// 
        /// </summary>
        public Documents()
        {
            db = DatabaseFactory.CreateDatabase("HCB_Database_winology");

        }
        #endregion

        #region properties

        public string DocName
        {
            get;
            set;
        }

        public int DocID
        {
            get;
            set;
        }

        public int RefNo
        {
            get;
            set;
        }

        public int LoggedInId
        {
            get;
            set;
        }

        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet GetDocument()
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");

                DbCommand dbcom = db.GetStoredProcCommand("GetClientDocument");
                db.AddInParameter(dbcom, "RefNo", DbType.Int32, this.RefNo);
                ds = db.ExecuteDataSet(dbcom);
                return ds;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet DeleteDocument()
        {
            try
            {
                DataSet ds = new DataSet();

                DbCommand dbcom = db.GetStoredProcCommand("DeleteClientDocument");
                db.AddInParameter(dbcom, "DocumentId", DbType.Int32, this.DocID);
                db.AddInParameter(dbcom, "RemovedBy", DbType.Int32, this.LoggedInId);
                ds = db.ExecuteDataSet(dbcom);
                return ds;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertDocument()
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                DbCommand dbcom = db.GetStoredProcCommand("InsertClientDocument");
                db.AddInParameter(dbcom, "RefNo", DbType.Int32, this.RefNo);
                db.AddInParameter(dbcom, "DocumentName", DbType.String, this.DocName);
                db.AddInParameter(dbcom, "UploadedBy", DbType.Int32, this.LoggedInId);
                db.ExecuteNonQuery(dbcom);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        
    }
}
