﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Security.Cryptography;
using System.Collections;
using System.Configuration;


    public static class Encryption
    {

        public static string Decrypt(string textToBeDecrypted)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            //string password = "SLSL";
            string password = ConfigurationManager.AppSettings["EncryptPassword"].ToString();
            string decryptedData;

            try
            {
                byte[] encryptedData = Convert.FromBase64String(textToBeDecrypted);

                byte[] salt = Encoding.ASCII.GetBytes(password.Length.ToString());
                //Making of the key for decryption 
                PasswordDeriveBytes secretKey = new PasswordDeriveBytes(password, salt);
                //Creates a symmetric Rijndael decryptor object. 
                ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));

                MemoryStream memoryStream = new MemoryStream(encryptedData);
                //Defines the cryptographics stream for decryption.THe stream contains decrpted data 
                CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

                //Converting to string 
                byte[] byteArr = new byte[encryptedData.Length - 1];
                int decryptedCount = cryptoStream.Read(byteArr, 0, encryptedData.Length - 1);
                decryptedData = Encoding.Unicode.GetString(byteArr, 0, decryptedCount);

                memoryStream.Close();
                cryptoStream.Close();
            }
            catch
            {
                decryptedData = "";
            }
            return decryptedData;
        }

        public static string Encrypt(string textToBeEncrypted)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            //string strPassword = "SLSL";
            string strPassword = ConfigurationManager.AppSettings["EncryptPassword"].ToString();
            byte[] plainText = System.Text.Encoding.Unicode.GetBytes(textToBeEncrypted);
            byte[] salt = Encoding.ASCII.GetBytes(strPassword.Length.ToString());
            PasswordDeriveBytes secretKey = new PasswordDeriveBytes(strPassword, salt);

            //Creates a symmetric encryptor object. 
            ICryptoTransform encryptor = RijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            //Defines a stream that links data streams to cryptographic transformations 
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainText, 0, plainText.Length);

            //Writes the final state and clears the buffer 
            cryptoStream.FlushFinalBlock();
            byte[] cipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            string encryptedData = Convert.ToBase64String(cipherBytes);

            return encryptedData;
        }

        public static string GenerateNewPassword(int intNumberOfChars)
        {
            string allowedChars = "";
            string newPassword = "";
            string temp = "";

            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";

            char[] separator = { ',' };
            string[] arr = allowedChars.Split(separator);

            Random rndNum = new Random();
            for (int i = 0; i <= intNumberOfChars; i++)
            {
                temp = arr[rndNum.Next(0, arr.Length)];
                newPassword += temp;
            }
            return newPassword;
        }

        // <add key="EncryptionKey" value="@1B2c3D4e5F6g7H8"/> 

        private static byte[] key = { };
        private static byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };

        public static string EncryptionKey
        {
            get { return ConfigurationManager.AppSettings["EncryptionKey"].ToString(); }
            //get { return "@1B2c3D4e5F6g7H8"; }//ConfigurationManager.AppSettings("EncryptionKey").ToString(); }
        }

        public static string DecryptCookie(string stringToDecrypt)
        {
            byte[] inputByteArray = new byte[stringToDecrypt.Length + 1];
            try
            {
                stringToDecrypt = stringToDecrypt.Replace(" ", "+");
                key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }



        public static string EncryptCookie(string stringToEncrypt)
        {
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

    }

