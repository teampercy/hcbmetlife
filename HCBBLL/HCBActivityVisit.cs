﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class HCBActivityVisit
    {
       #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        #endregion

        #region Constructor

        public HCBActivityVisit()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion
        #region Properties

        public int HCBVisitId
        { get; set; }

        public int HCBReference
        { get; set; }

        public string AppointmentDate
        { get; set; }

        public string ReportCompDate
        { get; set; }
        public int TYpeOfVisit
        { get; set; }
        public decimal FeeCharged
        { get; set; }
        public bool IsActive { get; set; }
        #endregion

        public DataSet GetHCBVisitActivity(int HCBReference)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetHCBVisitActivity";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Vist";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public int Save()
        {
            if (this.HCBVisitId == 0)
            {
                return this.Insert();

            }
            else
            {
                if (this.HCBVisitId > 0)
                {
                    this.Update();
                    return HCBVisitId;
                }
                else
                {
                    this.HCBVisitId = 0;
                    return HCBVisitId;
                }
            }
        }
        private int Insert()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "CreateHCBVisitActivity";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.HCBReference > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.AppointmentDate))
                {
                    this.db.AddInParameter(cmd, "@AppointmentDate", DbType.String, this.AppointmentDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AppointmentDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReportCompDate))
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, this.ReportCompDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, DBNull.Value);
                }
                if (this.FeeCharged > 0)
                {

                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, this.FeeCharged);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, DBNull.Value);
                }
                if (this.TYpeOfVisit > 0)
                {

                    this.db.AddInParameter(cmd, "@TYpeOfVisit", DbType.Int32, this.TYpeOfVisit);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@TYpeOfVisit", DbType.Int32, DBNull.Value);
                }
                //this.db.ExecuteDataSet(cmd);

                //string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();

                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        private int Update()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "UpdateHCBVisitActivity";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.HCBVisitId > 0)
                {
                    this.db.AddInParameter(cmd, "@HCBVisitId", DbType.Int32, this.HCBVisitId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBVisitId", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.AppointmentDate))
                {
                    this.db.AddInParameter(cmd, "@AppointmentDate", DbType.String, this.AppointmentDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AppointmentDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReportCompDate))
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, this.ReportCompDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, DBNull.Value);
                }
                if (this.FeeCharged > 0)
                {

                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, this.FeeCharged);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, DBNull.Value);
                }
                if (this.TYpeOfVisit > 0)
                {

                    this.db.AddInParameter(cmd, "@TYpeOfVisit", DbType.Int32, this.TYpeOfVisit);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@TYpeOfVisit", DbType.Int32, DBNull.Value);
                }
                
                //this.db.ExecuteDataSet(cmd);

                //string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();

                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        public DataSet DeleteHCBVisitActivity()
        {
            DataSet ds = new DataSet();

            try
            {

                DbCommand comm = this.db.GetStoredProcCommand("DeleteHCBVisitActivity");
                this.db.AddInParameter(comm, "@HCBVisitId", DbType.Int32, this.HCBVisitId);
                this.db.ExecuteNonQuery(comm);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

    }
}
