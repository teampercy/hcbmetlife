﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class HCBDuration
    {

        #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        #endregion

        #region Constructor

        public HCBDuration()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties

        public int DurationID
        { get; set; }

        public int HCBReference
        { get; set; }

        public string AppointmentDate
        { get; set; }

        public string ReportCompDate
        { get; set; }

        public string Call1Date
        { get; set; }

        public string Call1Time
        { get; set; }

        public string Call1ExpectRTWDate
        { get; set; }

        public string Call1CaseMngtRecom
        { get; set; }

        public string Call1NxtSchedule
        { get; set; }

        public bool Call1Aborted
        { get; set; }

        public string Call2Date
        { get; set; }

        public string Call2Time
        { get; set; }

        public string Call2ExpectRTWDate
        { get; set; }

        public string Call2CasemngtRecom
        { get; set; }

        public string Call2OriginalRTW
        { get; set; }

        public string Call2EnvisagedDate
        { get; set; }

        public string Call2NxtSchedule
        { get; set; }

        public bool Call2Aborted
        { get; set; }

        public string Call3Date
        { get; set; }

        public string Call3Time
        { get; set; }

        public string Call3ExpectRTWDate
        { get; set; }

        public string Call3FurtherIntervention
        { get; set; }

        public string Call3RTWDateAgreed
        { get; set; }

        public string Call3CaseMngtInvolve
        { get; set; }

        public string Call3RehabServicePurchase
        { get; set; }

        public bool Call3Aborted
        { get; set; }


        public double HomeVisitCurrency
        { get; set; }

        public double Call1Fee
        { get; set; }

        public double Call2Fee
        { get; set; }

        public double Call3Fee
        { get; set; }

        #endregion

        #region Methods

        public int Save()
        {
            if (this.DurationID == 0)
            {
                return this.Insert();

            }
            else
            {
                if (this.DurationID > 0)
                {
                    this.Update();
                    return DurationID;
                }
                else
                {
                    this.DurationID = 0;
                    return DurationID;
                }
            }
        }

        private int Insert()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "CreateHCBDuration";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.HCBReference > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.AppointmentDate))
                {
                    this.db.AddInParameter(cmd, "@AppointmentDate", DbType.String, this.AppointmentDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@AppointmentDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReportCompDate))
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, this.ReportCompDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1Date))
                {
                    this.db.AddInParameter(cmd, "@Call1Date", DbType.String, this.Call1Date);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1Date", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1Time))
                {
                    this.db.AddInParameter(cmd, "@Call1Time", DbType.String, this.Call1Time);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1Time", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1ExpectRTWDate))
                {
                    this.db.AddInParameter(cmd, "@Call1ExpectRTWDate", DbType.String, this.Call1ExpectRTWDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1ExpectRTWDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1CaseMngtRecom))
                {

                    this.db.AddInParameter(cmd, "@Call1CaseMngtRecom", DbType.String, this.Call1CaseMngtRecom);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1CaseMngtRecom", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1NxtSchedule))
                {
                    this.db.AddInParameter(cmd, "@Call1NxtSchedule", DbType.String, this.Call1NxtSchedule);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1NxtSchedule", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(cmd, "@Call1Aborted", DbType.Boolean, this.Call1Aborted);
                                

                if (!string.IsNullOrEmpty(this.Call2Date))
                {
                    this.db.AddInParameter(cmd, "@Call2Date", DbType.String, this.Call2Date);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2Date", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2Time))
                {
                    this.db.AddInParameter(cmd, "@Call2Time", DbType.String, this.Call2Time);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2Time", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2ExpectRTWDate))
                {
                    this.db.AddInParameter(cmd, "@Call2ExpectRTWDate", DbType.String, this.Call2ExpectRTWDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2ExpectRTWDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2CasemngtRecom))
                {
                    this.db.AddInParameter(cmd, "@Call2CasemngtRecom", DbType.String, this.Call2CasemngtRecom);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2CasemngtRecom", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2OriginalRTW))
                {
                    this.db.AddInParameter(cmd, "@Call2OriginalRTW", DbType.String, this.Call2OriginalRTW);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2OriginalRTW", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2EnvisagedDate))
                {
                    this.db.AddInParameter(cmd, "@Call2EnvisagedDate", DbType.String, this.Call2EnvisagedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2EnvisagedDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2NxtSchedule))
                {
                    this.db.AddInParameter(cmd, "@Call2NxtSchedule", DbType.String, this.Call2NxtSchedule);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2NxtSchedule", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(cmd, "@Call2Aborted", DbType.Boolean, this.Call2Aborted);

                if (!string.IsNullOrEmpty(this.Call3Date))
                {
                    this.db.AddInParameter(cmd, "@Call3Date", DbType.String, this.Call3Date);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3Date", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3Time))
                {
                    this.db.AddInParameter(cmd, "@Call3Time", DbType.String, this.Call3Time);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3Time", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3ExpectRTWDate))
                {
                    this.db.AddInParameter(cmd, "@Call3ExpectRTWDate", DbType.String, this.Call3ExpectRTWDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3ExpectRTWDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3FurtherIntervention))
                {
                    this.db.AddInParameter(cmd, "@Call3FurtherIntervention", DbType.String, this.Call3FurtherIntervention);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3FurtherIntervention", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3RTWDateAgreed))
                {
                    this.db.AddInParameter(cmd, "@Call3RTWDateAgreed", DbType.String, this.Call3RTWDateAgreed);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3RTWDateAgreed", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3CaseMngtInvolve))
                {
                    this.db.AddInParameter(cmd, "@Call3CaseMngtInvolve", DbType.String, this.Call3CaseMngtInvolve);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3CaseMngtInvolve", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3RehabServicePurchase))
                {
                    this.db.AddInParameter(cmd, "@Call3RehabServicePurchase", DbType.String, this.Call3RehabServicePurchase);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3RehabServicePurchase", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(cmd, "@Call3Aborted", DbType.Boolean, this.Call3Aborted);

                if (this.HomeVisitCurrency > 0)
                {
                    this.db.AddInParameter(cmd, "@HomeVisitFee", DbType.String, this.HomeVisitCurrency);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HomeVisitFee", DbType.String, DBNull.Value);
                }
                if (this.Call1Fee > 0)
                {
                    this.db.AddInParameter(cmd, "@Call1Fee", DbType.String, this.Call1Fee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1Fee", DbType.String, DBNull.Value);
                }
                if (this.Call2Fee > 0)
                {
                    this.db.AddInParameter(cmd, "@Call2Fee", DbType.String, this.Call2Fee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2Fee", DbType.String, DBNull.Value);
                }
                if (this.Call3Fee > 0)
                {
                    this.db.AddInParameter(cmd, "@Call3Fee", DbType.String, this.Call3Fee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3Fee", DbType.String, DBNull.Value);
                }
                
                //this.db.ExecuteDataSet(cmd);

                string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();
                cmd.CommandText = query1;
                cmd.CommandType = CommandType.Text;
                id = (int)cmd.ExecuteScalar();
                
            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }

        private bool Update()
        {
            
            try
            {
                string queryClient = "UpdateDurationDet";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);

                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "@RefNo", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RefNo", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.AppointmentDate))
                {
                    this.db.AddInParameter(cmd, "@ApptDate", DbType.String, this.AppointmentDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ApptDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReportCompDate))
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, this.ReportCompDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReportCompDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1Date))
                {
                    this.db.AddInParameter(cmd, "@Call1Date", DbType.String, this.Call1Date);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1Date", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1Time))
                {
                    this.db.AddInParameter(cmd, "@Call1Time", DbType.String, this.Call1Time);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1Time", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1ExpectRTWDate))
                {
                    this.db.AddInParameter(cmd, "@Call1ExpRTWDate", DbType.String, this.Call1ExpectRTWDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1ExpRTWDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1CaseMngtRecom))
                {
                    this.db.AddInParameter(cmd, "@Call1CaseMngt", DbType.String, this.Call1CaseMngtRecom);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1CaseMngt", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call1NxtSchedule))
                {
                    this.db.AddInParameter(cmd, "@Call1NxtSchedule", DbType.String, this.Call1NxtSchedule);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1NxtSchedule", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(cmd, "@Call1Aborted", DbType.Boolean, this.Call1Aborted);

                if (!string.IsNullOrEmpty(this.Call2Date))
                {
                    this.db.AddInParameter(cmd, "@Call2Date", DbType.String, this.Call2Date);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2Date", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2Time))
                {
                    this.db.AddInParameter(cmd, "@Call2Time", DbType.String, this.Call2Time);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2Time", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2ExpectRTWDate))
                {
                    this.db.AddInParameter(cmd, "@Call2ExpectRTWDate", DbType.String, this.Call2ExpectRTWDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2ExpectRTWDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2CasemngtRecom))
                {
                    this.db.AddInParameter(cmd, "@Call2CasemngtRecom", DbType.String, this.Call2CasemngtRecom);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2CasemngtRecom", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2OriginalRTW))
                {
                    this.db.AddInParameter(cmd, "@Call2OriginalRTW", DbType.String, this.Call2OriginalRTW);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2OriginalRTW", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2EnvisagedDate))
                {
                    this.db.AddInParameter(cmd, "@Call2EnvisagedDate", DbType.String, this.Call2EnvisagedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2EnvisagedDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call2NxtSchedule))
                {
                    this.db.AddInParameter(cmd, "@Call2NxtSchedule", DbType.String, this.Call2NxtSchedule);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2NxtSchedule", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(cmd, "@Call2Aborted", DbType.Boolean, this.Call2Aborted);


                if (!string.IsNullOrEmpty(this.Call3Date))
                {
                    this.db.AddInParameter(cmd, "@Call3Date", DbType.String, this.Call3Date);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3Date", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3Time))
                {
                    this.db.AddInParameter(cmd, "@Call3Time", DbType.String, this.Call3Time);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3Time", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3ExpectRTWDate))
                {
                    this.db.AddInParameter(cmd, "@Call3ExpectRTWDate", DbType.String, this.Call3ExpectRTWDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3ExpectRTWDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3FurtherIntervention))
                {
                    this.db.AddInParameter(cmd, "@Call3FurtherIntervention", DbType.String, this.Call3FurtherIntervention);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3FurtherIntervention", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3RTWDateAgreed))
                {
                    this.db.AddInParameter(cmd, "@Call3RTWDateAgreed", DbType.String, this.Call3RTWDateAgreed);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3RTWDateAgreed", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3CaseMngtInvolve))
                {
                    this.db.AddInParameter(cmd, "@Call3CaseMngtInvolve", DbType.String, this.Call3CaseMngtInvolve);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3CaseMngtInvolve", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call3RehabServicePurchase))
                {
                    this.db.AddInParameter(cmd, "@Call3RehabServicePurchase", DbType.String, this.Call3RehabServicePurchase);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3RehabServicePurchase", DbType.String, DBNull.Value);
                }
                this.db.AddInParameter(cmd, "@Call3Aborted", DbType.Boolean, this.Call3Aborted);

                if (this.HomeVisitCurrency > 0)
                {
                    this.db.AddInParameter(cmd, "@HomeVisitFee", DbType.Int32, this.HomeVisitCurrency);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HomeVisitFee", DbType.Int32, DBNull.Value);
                }
                if (this.Call1Fee > 0)
                {
                    this.db.AddInParameter(cmd, "@Call1Fee", DbType.Int32, this.Call1Fee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call1Fee", DbType.Int32, DBNull.Value);
                }
                if (this.Call2Fee > 0)
                {
                    this.db.AddInParameter(cmd, "@Call2Fee", DbType.Int32, this.Call2Fee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call2Feez", DbType.Int32, DBNull.Value);
                }
                if (this.Call3Fee > 0)
                {
                    this.db.AddInParameter(cmd, "@Call3Fee", DbType.Int32, this.Call3Fee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call3Fee", DbType.Int32, DBNull.Value);
                }

                if (this.DurationID > 0)
                {
                    this.db.AddInParameter(cmd, "@DuationID", DbType.Int32, this.DurationID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@DuationID", DbType.Int32, DBNull.Value);
                }
                
                this.db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return false;
            }
           
            return true;
        }

        public DataSet GetHCBDuration()
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetHCBDuration";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBRef", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBRef", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Duration";

                if (ds != null & ds.Tables["Duration"].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }

        #endregion
    }
}
