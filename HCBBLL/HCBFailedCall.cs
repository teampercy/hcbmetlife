﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBBLL
{
    public class HCBFailedCall
    {
        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public HCBFailedCall()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties

        public int FailedCallID
        { get; set; }
        public int HCBTelephoneID
        { get; set; }

        public int HCBReference
        { get; set; }

        public string FailCallDate
        { get; set; }

        public string FailCallTime
        { get; set; }

        public string Call
        { get; set; }

        #endregion

        #region Methods

        public int Save()
        {
            int fid = 0;

            if (this.FailedCallID == 0)
            {
                fid = this.Insert();
                return fid;
            }
            else
            {
                if (this.FailedCallID > 0)
                {
                    if (this.Update())
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    this.FailedCallID = 0;
                    return 0;
                }
            }
        }

        private int Insert()
        {
            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {
                conn = DbConnection.OpenConnection();
                string queryClient = "CreateHCBFailedCall";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                

                //DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                if (this.HCBReference > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBReference", OleDbType.Integer)).Value = this.HCBReference;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBReference", OleDbType.Integer)).Value = DBNull.Value;
                }
                if (!String.IsNullOrEmpty(this.FailCallDate))
                {
                    cmd.Parameters.Add(new OleDbParameter("FailCallDate", OleDbType.VarChar)).Value = this.FailCallDate;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("FailCallDate", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (!String.IsNullOrEmpty(this.FailCallTime))
                {
                    cmd.Parameters.Add(new OleDbParameter("FailCallTime", OleDbType.VarChar)).Value = this.FailCallTime;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("FailCallTime", OleDbType.VarChar)).Value = DBNull.Value;
                }
                if (this.HCBTelephoneID > 0)
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBTelephoneID", OleDbType.Integer)).Value = this.HCBTelephoneID;
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("HCBTelephoneID", OleDbType.Integer)).Value = DBNull.Value;
                }

               // string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();
                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                DbConnection.CloseConnection(conn);
                ds.Dispose();
                return 0;
            }
            finally
            {
                DbConnection.CloseConnection(conn);
            }


            return id;
        }

        private bool Update()
        {
            DataSet ds = new DataSet();

            try
            {

                string queryClient = "UpdateHCBFailedCall";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);

                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);

                }
                if (!String.IsNullOrEmpty(this.FailCallDate))
                {
                    this.db.AddInParameter(cmd, "@FailCallDate", DbType.String, this.FailCallDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FailCallDate", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.FailCallTime))
                {
                    this.db.AddInParameter(cmd, "@FailCallTime", DbType.String, this.FailCallTime);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FailCallTime", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Call))
                {
                    this.db.AddInParameter(cmd, "@Call", DbType.String, this.Call);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@Call", DbType.String, DBNull.Value);
                }

                this.db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ds.Dispose();
                return false;
            }
            
            return true;
        }

        public bool Delete()
        {
            //OleDbConnection conn = null;
           
            try
            {
                string queryClient = "DeleteHCBFailCall";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);

                //conn = DbConnection.OpenConnection();
                
                //if (conn.State == ConnectionState.Open)
                //{
                //    conn.Close();
                //}
                //conn.Open();

                if (this.HCBTelephoneID > 0)
                {
                    this.db.AddInParameter(cmd, "@HCBTelephoneID", DbType.Int32, this.HCBTelephoneID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBTelephoneID", DbType.Int32, DBNull.Value);
                }

                this.db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //conn.Close();
                return false;
            }
            finally
            {
                //DbConnection.CloseConnection(conn);
            }
            //cmd.ExecuteNonQuery();
            return true;
        }
        
        public DataSet GetFailedCallDet()
        {
            DataSet ds = new DataSet();
            
            try
            {

                string queryClient = "GetHCBFailCall";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);

                if (this.HCBTelephoneID > 0)
                {
                    this.db.AddInParameter(cmd, "@HCBTelephoneID", DbType.Int32, this.HCBTelephoneID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBTelephoneID", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "FailedCall";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }

        #endregion
    }
}
