﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class HCBFeeCharged
    {
        #region Variables

        OleDbConnection conn = null;
        private Database db;

        #endregion

        #region Constructor

        public HCBFeeCharged()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion
        
        #region Properties

        public int FeeChargedID
        { get; set; }

        public double HomeVisitCurrency
        { get; set; }

        public double Call1Fee
        { get; set; }

        public double Call2Fee
        { get; set; }

        public double Call3Fee
        { get; set; }

        public double CallAbortedFee
        { get; set; }

        public string ModifiedDate
        { get; set; }

        #endregion

    

        #region Methods

        public bool Save()
        {
            if (this.FeeChargedID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.FeeChargedID > 0)
                {
                    return this.Update();
                }
            }

            return true;
        }

        private bool Insert()
        {           

            try
            {
                string InsertQuery = "InsertHCBFeeCharge";
               

                DbCommand comm = this.db.GetStoredProcCommand(InsertQuery);
                

                if (this.HomeVisitCurrency > 0)
                {                    
                    this.db.AddInParameter(comm,"HomeVisitCurrency", DbType.Double,this.HomeVisitCurrency);
                }
                else
                {
                    this.db.AddInParameter(comm,"HomeVisitCurrency", DbType.Double,DBNull.Value);
                }

                if (this.Call1Fee > 0)
                {
                    this.db.AddInParameter(comm,"Call1Fee", DbType.Double,this.Call1Fee);
                }
                else
                {
                    this.db.AddInParameter(comm,"Call1Fee", DbType.Double,DBNull.Value);
                }

                if (this.Call2Fee > 0)
                {
                    this.db.AddInParameter(comm,"Call2Fee", DbType.Double,this.Call2Fee);
                }
                else
                {
                    this.db.AddInParameter(comm,"Call2Fee", DbType.Double,DBNull.Value);
                }

                if (this.Call3Fee > 0)
                {
                    this.db.AddInParameter(comm,"Call3Fee", DbType.Double,this.Call3Fee);
                }
                else
                {
                    this.db.AddInParameter(comm,"Call3Fee", DbType.Double,DBNull.Value);
                }

                if (this.CallAbortedFee > 0)
                {
                    this.db.AddInParameter(comm, "CallAbortedFee", DbType.Double, this.CallAbortedFee);
                }
                else
                {
                    this.db.AddInParameter(comm, "CallAbortedFee", DbType.Double, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.ModifiedDate))
                {
                    this.db.AddInParameter(comm,"DateModified",DbType.String , this.ModifiedDate);
                }
                else
                {
                    this.db.AddInParameter(comm,"DateModified",DbType.String , DBNull.Value);
                }

                this.db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {               
                return false;
            }
                       
            return true;
        }

        private bool Update()
        {         

            try
            {
                string UpdateQuery = "UpdateFeeCharge";
              
                DbCommand comm = this.db.GetStoredProcCommand(UpdateQuery);

                if (this.HomeVisitCurrency > 0)
                {
                    this.db.AddInParameter(comm,"HomeVisitCurrency", DbType.Double,this.HomeVisitCurrency);
                }
                else
                {
                    this.db.AddInParameter(comm,"HomeVisitCurrency", DbType.Double,DBNull.Value);
                }

                if (this.Call1Fee > 0)
                {
                    this.db.AddInParameter(comm,"Call1Fee", DbType.Double,this.Call1Fee);
                }
                else
                {
                    this.db.AddInParameter(comm,"Call1Fee", DbType.Double,DBNull.Value);
                }

                if (this.Call2Fee > 0)
                {
                    this.db.AddInParameter(comm,"Call2Fee", DbType.Double,this.Call2Fee);
                }
                else
                {
                    this.db.AddInParameter(comm,"Call2Fee", DbType.Double,DBNull.Value);
                }

                if (this.Call3Fee > 0)
                {
                    this.db.AddInParameter(comm,"Call3Fee", DbType.Double,this.Call3Fee);
                }
                else
                {
                    this.db.AddInParameter(comm,"Call3Fee", DbType.Double,DBNull.Value);
                }

                if (this.CallAbortedFee > 0)
                {
                    this.db.AddInParameter(comm, "CallAbortedFee", DbType.Double, this.CallAbortedFee);
                }
                else
                {
                    this.db.AddInParameter(comm, "CallAbortedFee", DbType.Double, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.ModifiedDate))
                {
                    this.db.AddInParameter(comm,"DateModified",DbType.String , this.ModifiedDate);
                }
                else
                {
                    this.db.AddInParameter(comm,"DateModified",DbType.String , DBNull.Value);
                }

                this.db.AddInParameter(comm,"FeeChargeID",DbType.Int32, this.FeeChargedID);

                this.db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public DataSet GetFeeCharged()
        {
            DataSet ds = new DataSet();
           
            try
            {
                string GetQuery = "GetFeeCharged";
                DbCommand cmd = this.db.GetStoredProcCommand(GetQuery);

                ds = this.db.ExecuteDataSet(cmd);

            }
            catch (Exception ex)
            {               
                ds = null;
            }

            return ds;
        }

        #endregion
    }
}
