﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBBLL
{
    public class HCBInsurance
    {
        #region Variables Declaration

        private Database db;
        #endregion

        #region Constructors
        public HCBInsurance()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties

        public int InsuranceId
        { get; set; }

        public int HCBReference
        { get; set; }

        public string ClaimReference
        { get; set; }

        public int CorporatePartner
        { get; set; }

        public int WaitingPeriod
        { get; set; }

        public int ProductType
        { get; set; }

        public int IncapacityDefinition
        { get; set; }

        public double MonthlyBenefit
        { get; set; }

        public string EndofBenefit
        { get; set; }

        public string DefferedPeriodOther
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public bool Save(int id)
        {
            if (id == 1)
            {
                return this.Insert();
            }
            else if (id == 2)
            {
                return this.Update();
            }
            else
            {
                this.InsuranceId = 0;
                return false;
            }
        }

        private bool Insert()
        {
            DataSet ds = new DataSet();
            
            try
            {   
                DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBInsurance");

                #region Parameters
                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,this.HCBReference);
                   
                }
                else
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClaimReference))
                {
                    this.db.AddInParameter(cmd,"@ClaimReference", DbType.String, this.ClaimReference);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@ClaimReference", DbType.String, DBNull.Value);
                }
                if (this.CorporatePartner > 0)
                {
                    this.db.AddInParameter(cmd,"@CorporatePartner", DbType.String, this.CorporatePartner);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@CorporatePartner", DbType.String, DBNull.Value);
                }
                if (this.WaitingPeriod > 0)
                {
                    this.db.AddInParameter(cmd,"@WaitingPeriod", DbType.Int32, this.WaitingPeriod);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@WaitingPeriod", DbType.Int32, DBNull.Value);
                }
                //if (this.ProductType > 0)
                //{
                //    this.db.AddInParameter(cmd,"@ProductType", DbType.Int32, this.ProductType);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd,"@ProductType", DbType.Int32, DBNull.Value);
                //}
                if (this.MonthlyBenefit > 0)
                {
                    this.db.AddInParameter(cmd,"@MonthlyBenefit", DbType.Double, this.MonthlyBenefit);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@MonthlyBenefit",  DbType.Double,DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.EndofBenefit))
                {
                    this.db.AddInParameter(cmd,"@EndofBenefit", DbType.String, this.EndofBenefit);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@EndofBenefit", DbType.String, DBNull.Value);
                }
                if (this.IncapacityDefinition>0)
                {
                    this.db.AddInParameter(cmd, "@IncapacityDefinition", DbType.Int32, this.IncapacityDefinition);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@IncapacityDefinition", DbType.Int32, DBNull.Value);
                }


                if(!string.IsNullOrEmpty(this.DefferedPeriodOther))
                {
                    this.db.AddInParameter(cmd, "@DefferedPeriodOther", DbType.String, this.DefferedPeriodOther);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@DefferedPeriodOther", DbType.String, DBNull.Value);
                }

                #endregion

                this.db.ExecuteNonQuery(cmd);
                                
            }
            catch (Exception ex)
            {
                ds.Dispose();
                return false;
            }
            finally
            {
                
                
            }

            return true;
        }

        private bool Update()
        {
            DataSet ds = new DataSet();
           
            try
            {                
                DbCommand cmd = this.db.GetStoredProcCommand("UpdateHCBInsurance");

                #region Parameters
                if (this.InsuranceId > 0)
                {
                    this.db.AddInParameter(cmd, "@InsuranceId", DbType.Int32, this.InsuranceId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@InsuranceId", DbType.Int32, DBNull.Value);
                }
                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ClaimReference))
                {
                    this.db.AddInParameter(cmd,"@ClaimReference",DbType.String, this.ClaimReference);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@ClaimReference",DbType.String, DBNull.Value);
                }
                if (this.CorporatePartner > 0)
                {
                    this.db.AddInParameter(cmd,"@CorporatePartner",DbType.String, this.CorporatePartner);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@CorporatePartner",DbType.String, DBNull.Value);
                }
                if (this.WaitingPeriod > 0)
                {
                    this.db.AddInParameter(cmd,"@WaitingPeriod", DbType.Int32,this.WaitingPeriod);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@WaitingPeriod", DbType.Int32,DBNull.Value);
                }
                //if (this.ProductType > 0)
                //{
                //    this.db.AddInParameter(cmd,"@ProductType", DbType.Int32,this.ProductType);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd,"@ProductType", DbType.Int32,DBNull.Value);
                //}
                if (this.MonthlyBenefit > 0)
                {
                    this.db.AddInParameter(cmd,"@MonthlyBenefit", DbType.Double, this.MonthlyBenefit);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@MonthlyBenefit", DbType.Double,  DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.EndofBenefit))
                {
                    this.db.AddInParameter(cmd,"@EndofBenefit",DbType.String, this.EndofBenefit);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@EndofBenefit",DbType.String, DBNull.Value);
                }
                
                if (this.IncapacityDefinition>0)
                {
                    this.db.AddInParameter(cmd, "@IncapacityDefinition", DbType.Int32, this.IncapacityDefinition);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@IncapacityDefinition", DbType.Int32, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.DefferedPeriodOther))
                {
                    this.db.AddInParameter(cmd, "@DefferedPeriodOther", DbType.String, this.DefferedPeriodOther);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@DefferedPeriodOther", DbType.String, DBNull.Value);
                }

                #endregion

                this.db.ExecuteNonQuery(cmd);
                
            }
            catch (Exception ex)
            {
                ds.Dispose();  
                return false;
            }
            finally
            {
                             
            }

            return true;
        }

        public DataSet GetInsuranceDet()
        {
            DataSet ds = new DataSet();
            
            try
            {
               
                DbCommand cmd = this.db.GetStoredProcCommand("GetHCBInsurance");

              
                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,DBNull.Value);
                }
                
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Insurance";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {
                throw;
            }

            return ds;
        }

        #endregion
    }
}
