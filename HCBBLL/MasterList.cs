﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Configuration;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace HCBBLL
{
    public class MasterListBL
    {

        #region Variable Declaration
        private Database db;
        OleDbConnection conn = new OleDbConnection(ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());

        #endregion

        #region Constructor

        public MasterListBL()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties

        public int ReasonID
        {
            get;
            set;
        }

        public string Reason
        {
            get;
            set;
        }

        #endregion

        #region Methods

        //public DataSet GetReasons()
        //{
        //    DataSet ds = new DataSet();

        //    try
        //    {
        //        conn.Open();

        //        OleDbCommand comm = new OleDbCommand("GetReasonCancelled", conn);
        //        comm.CommandType = CommandType.StoredProcedure;
        //        OleDbDataAdapter da = new OleDbDataAdapter();
        //        da.SelectCommand = comm;

        //        da.Fill(ds, "Reason");

        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        conn.Close();
        //    }

        //    return ds;
        //}


        public DataSet GetAssessorList()
        {
            DataSet ds = new DataSet();

            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetAssessorDetails");
                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "AssessorDetails";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }

        public DataSet GetAssessorTeamDetails(int AssessorTeamID)
        {
            DataSet ds = new DataSet();

            try
            {
                DbCommand com = this.db.GetStoredProcCommand("GetAssessorTeamDet");

                if (AssessorTeamID > 0)
                {
                    this.db.AddInParameter(com, "AssessID", DbType.Int32, AssessorTeamID);
                }
                else
                {
                    this.db.AddInParameter(com, "AssessID", DbType.Int32, 0);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "AssessDetails";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {               
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public DataSet GetTypeList()
        {
            DataSet ds = new DataSet();

            try
            {

                DbCommand com = this.db.GetStoredProcCommand("GetTypeList");
                
                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "TypeList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                throw;
            }

            return ds;
        }
        public DataSet GetMasterList(int TypeID)
        {
            DataSet ds = new DataSet();

            try
            {
                
                DbCommand com = this.db.GetStoredProcCommand("GetMasterLIst");
                if (TypeID > 0)
                {
                    this.db.AddInParameter(com, "typeId", DbType.Int32, TypeID);
                }
                else
                {
                    this.db.AddInParameter(com, "typeId", DbType.Int32, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "Master";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                throw;
            }

            return ds;
        }

        public DataSet UpdateAssessorList(int ID, string AsswssorEmail, string AssessorPhoneNo,string Name)
        {
            DataSet ds = new DataSet();

            try
            {
                DbCommand comm = this.db.GetStoredProcCommand("UpdateAssessorDetails");

                this.db.AddInParameter(comm,"@TeamEmailId", DbType.String, AsswssorEmail);
                this.db.AddInParameter(comm,"@TeamPhone", DbType.String, AssessorPhoneNo);
                this.db.AddInParameter(comm,"@AssessorName", DbType.String, Name);
                this.db.AddInParameter(comm,"@ID", DbType.Int32,ID);

                this.db.ExecuteNonQuery(comm);
                
            }
            catch (Exception ex)
            {                
                throw ex;
            }

            return ds;
        }

        public DataSet UpdateMasterList(int ID, string strName, int TypeID,string strRelatedValue)
        {
            DataSet ds = new DataSet();

            try
            {
               
                DbCommand comm = this.db.GetStoredProcCommand("UpdateMasterList");

                
                
                this.db.AddInParameter(comm,"@TypeId", DbType.Int32, TypeID);
                this.db.AddInParameter(comm, "@Id", DbType.Int32, ID);
                this.db.AddInParameter(comm, "@Name", DbType.String, strName);
                this.db.AddInParameter(comm, "@RelatedValue", DbType.String, strRelatedValue);
                this.db.ExecuteNonQuery(comm);
                
            }
            catch (Exception ex)
            {                
                throw ex;
            }

            return ds;
        }
        
        public void InsertAssessorList(string Email, string PhoneNo,string Name)
        {
            try
            {
               
                DbCommand comm = this.db.GetStoredProcCommand("InsertAssessorDetails");

                this.db.AddInParameter(comm,"@EmailAddr", DbType.String,Email);
                this.db.AddInParameter(comm,"@PhoneNo", DbType.String, PhoneNo);
                this.db.AddInParameter(comm,"@AssessorName", DbType.String, Name);

                this.db.ExecuteNonQuery(comm);
                
            }
            catch (Exception ex)
            {               
                throw ex;
            }

        }
        
        public void InsertMasterList(string Name, int TypeID,string RelatedValue)
        {

            try
            {
                DbCommand comm = this.db.GetStoredProcCommand("InsertMasterList");

                this.db.AddInParameter(comm, "@Name", DbType.String,Name);
                this.db.AddInParameter(comm, "@TyprId", DbType.Int32,TypeID);
                this.db.AddInParameter(comm, "@RelatedValue", DbType.String, RelatedValue);

                this.db.ExecuteNonQuery(comm);
                
            }
            catch (Exception ex)
            {               
                throw ex;
            }

        }
        
        public DataSet DeleteAssessorList(int ID)
        {
            DataSet ds = new DataSet();

            try
            {
                DbCommand comm = this.db.GetStoredProcCommand("DeleteAssessorDetail");
                this.db.AddInParameter(comm, "@ID", DbType.Int32,ID);
                this.db.ExecuteNonQuery(comm);
                
            }
            catch (Exception ex)
            {               
                throw ex;
            }

            return ds;
        }
        
        public DataSet DeleteMasterList(int ID)
        {
            DataSet ds = new DataSet();

            try
            {
               
                DbCommand comm = this.db.GetStoredProcCommand("DeleteMasterList");
                this.db.AddInParameter(comm, "@ID", DbType.Int32,ID);
                this.db.ExecuteNonQuery(comm);
                
            }
            catch (Exception ex)
            {                
                throw ex;
            }

            return ds;
        }

        public static DataSet GetAdminList()
        {
            DataSet ds = new DataSet();
            try
            {
              
                Database db;
               db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                DbCommand comm = db.GetStoredProcCommand("GetAdminList");
                ds.Tables[0].TableName = "TypeList";

                ds = db.ExecuteDataSet(comm);

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        //Gaurav 20-03-2014
        public static DataSet GetInformationReport(int TypeID)
        {
            DataSet ds = new DataSet();
            string query="";
            int flag = 0;
            //OleDbConnection conn = new OleDbConnection(ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
            try
            {
                //conn.Open();
                Database db;
                DbCommand comm;
                db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                if (TypeID != 8)
                {
                    query = "GetMasterListByTypeId";
                }
                else
                {
                    query = "GetAssessorDetailsInfo";
                    flag=1;
                }
                
                comm = db.GetStoredProcCommand(query);
                if(flag==0)
                {
                    if(TypeID>0)
                    {
                            db.AddInParameter(comm,"TypeID",DbType.Int32,TypeID);
                    }
                    else
                    {
                            db.AddInParameter(comm, "TypeID", DbType.Int32,DBNull.Value);
                    }
                }
                ds = db.ExecuteDataSet(comm);
                ds.Tables[0].TableName = "Report";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                //conn.Close();
            }

            return ds;
        }
        public static DataSet GetPDFLicensekey()
        {
            DataSet ds = new DataSet();
            string query = "";
           
            //OleDbConnection conn = new OleDbConnection(ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
            try
            {
                //conn.Open();
                Database db;
                DbCommand comm;
                db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                query = "GetPDFLicensekey";
               
                comm = db.GetStoredProcCommand(query);
               
                ds = db.ExecuteDataSet(comm);
                ds.Tables[0].TableName = "PDF";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                //conn.Close();
            }

            return ds;
        }

        #endregion

        //Gaurav 20-03-2014
        public DataSet GetSearchList(string HcbReferance, int CaseManager, int CaseAssessor, bool IsexectSurName, string SurName, bool IsexectForeName, string ForeName, string DateOfBirth, bool IsexectAddr, string Addr)
        {


            string sqlquery = "GetClientRecordDetail"; 
               //GetQueryNew(HcbReferance, CaseManager, CaseAssessor, IsexectSurName, SurName, IsexectForeName, ForeName, DateOfBirth, IsexectAddr, Addr);
            DataSet ds = new DataSet();

            try
            {
                DbCommand comm = this.db.GetStoredProcCommand(sqlquery);
                comm.CommandType = CommandType.StoredProcedure;
                //bool ISFirst = true;
                
                if (HcbReferance.Trim() != "")
                {
                    int RefNo;
                    //string RefNo = HcbReferance.Substring(3);
                    int h=HcbReferance.Length;
                    if(h>=5)
                    {
                        RefNo = Convert.ToInt32(HcbReferance.Substring(3));
                    }
                    else
                    {
                        RefNo=Convert.ToInt32(HcbReferance.Trim());
                    }
                    
                    this.db.AddInParameter(comm, "@HCBReference", DbType.Int32, RefNo);
                }
                else
                {
                    this.db.AddInParameter(comm, "@HCBReference", DbType.Int16, DBNull.Value);
                }
                if (CaseManager != 0)
                {
                    this.db.AddInParameter(comm, "@CaseManager", DbType.Int16, CaseManager);
                }
                else
                {
                    this.db.AddInParameter(comm, "@CaseManager", DbType.Int16, DBNull.Value);

                }
                if (CaseAssessor != 0)
                {
                    this.db.AddInParameter(comm, "@CaseAssessor", DbType.Int16, CaseAssessor);
                }
                else
                {
                    this.db.AddInParameter(comm, "@CaseAssessor", DbType.Int16, DBNull.Value);

                }

                this.db.AddInParameter(comm,"@IsExectSurName",DbType.Boolean,IsexectSurName);

                if (SurName.Trim() != "")
                {
                    this.db.AddInParameter(comm, "@SurName", DbType.String, SurName);  
                }
                else
                {
                    this.db.AddInParameter(comm, "@SurName", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(comm, "@IsExectForName", DbType.Boolean, IsexectForeName);

                if (ForeName.Trim() != "")
                {
                    this.db.AddInParameter(comm, "@ForeName", DbType.String, ForeName);
                }
                else
                {
                    this.db.AddInParameter(comm, "@ForeName", DbType.String, DBNull.Value);
                }
                if (DateOfBirth.Trim() != "")
                {
                    this.db.AddInParameter(comm, "@DateOfBirth", DbType.DateTime, DateOfBirth);
                }
                else
                {
                    this.db.AddInParameter(comm, "@DateOfBirth", DbType.DateTime, DBNull.Value);
                }

                this.db.AddInParameter(comm, "@IsExectAddr", DbType.Boolean, IsexectAddr);

                if (Addr.Trim() != "")
                {
                    this.db.AddInParameter(comm, "@Addr", DbType.String, Addr);
                }
                else
                {
                    this.db.AddInParameter(comm, "@Addr", DbType.String, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(comm);
                ds.Tables[0].TableName = "ClientList";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            { 
            
            }
            return ds;
        }



       

        public string GetQuery(string HcbReferance, int CaseManager, int CaseAssessor, bool IsexectSurName, string SurName, bool IsexectForeName, string ForeName, string DateOfBirth, bool IsexectAddr, string Addr)
        {
            StringBuilder SqlQuery = new StringBuilder();
            //SqlQuery.Append(" where ");
            bool ISFirst = true;
            if (HcbReferance.Trim() != "")
            {
                string RefNo = HcbReferance.Substring(3);
                SqlQuery.Append(" HCBReference = " + RefNo.Trim() + "");
                ISFirst = false;
            }
            if (CaseManager != 0)
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                SqlQuery.Append(" HCBNurseID = " + CaseManager + "");
            }
            if (CaseAssessor != 0)
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                SqlQuery.Append(" IncoID = " + CaseAssessor + "");
            }
            if (SurName.Trim() != "")
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                if (IsexectSurName)
                {
                    
                    SqlQuery.Append(" ClientSurname = '" + SurName + "'");
                }
                else
                {
                    SqlQuery.Append(" ClientSurname like '%" + SurName + "%'");
                }

            }
            if (ForeName.Trim() != "")
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                if (IsexectForeName)
                {
                    SqlQuery.Append(" ClientForenames  = '" + ForeName + "'");
                }
                else
                {
                    SqlQuery.Append("  ClientForenames like '%" + ForeName + "%'");
                }

            }
            if (DateOfBirth.Trim() != "")
            {
                //DateTime dob = DateTime.Parse(DateOfBirth);
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                SqlQuery.Append(" DateValue(Format([ClientDOB],\"dd/mm/yyyy\")) = DateValue(\"" + DateOfBirth + "\")");
            }
            if (Addr.Trim() != "")
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                if (IsexectAddr)
                {
                    SqlQuery.Append(" ClientAddress   = '" + Addr.Trim() + "'");
                }
                else
                {
                    SqlQuery.Append("  ClientAddress  like '%" + Addr.Trim() + "%'");
                }
            }

            //string StrFinalQuery = "Select * from ClientRecord ";
            if (!ISFirst)
            {
                SqlQuery.Insert(0, " where ");
                //StrFinalQuery = StrFinalQuery + " where ";
                //StrFinalQuery = StrFinalQuery + SqlQuery.ToString();
            }
            SqlQuery.Insert(0, " Select \"DCP\"+Right(\"00000\"+CStr([HCBReference]),5) AS RefNo ,HCBReference , HCBNurseID, NurseName, ClaimHandlerID, ClaimHandlerName, ClientForenames + ' '+ ClientSurname AS ClientName,  Format([ClientRecord.ClientDOB],\"dd/mm/yyyy\") AS ClientDOB  from ClientRecord ");
            //lbltest.Text = SqlQuery.ToString();

            // SELECT ClientRecord.HCBReference, ClientRecord.HCBNurseID, ClientRecord.IncoID, ClientRecord.ClientSurname,
            // ClientRecord.ClientForenames, ClientRecord.ClientDOB, ClientRecord.ClientAddress
            // FROM ClientRecord

            //WHERE (((ClientRecord.)="@HVBReferance") AND ((ClientRecord.HCBNurseID)="@caseManager") AND ((ClientRecord.IncoID)="@CaseAssessor") AND ((ClientRecord.)="@ckientSurname") 
            // AND ((ClientRecord.ClientForenames)="@ForeName") AND ((ClientRecord.ClientDOB)="@DOB") AND ((ClientRecord.ClientAddress)="@Address"));
            SqlQuery.Append(" order by ClientRecord.HCBReference");

            return SqlQuery.ToString();
        }

        //Gaurav 20-03-2014
        public string GetQueryNew(string HcbReferance, int CaseManager, int CaseAssessor, bool IsexectSurName, string SurName, bool IsexectForeName, string ForeName, string DateOfBirth, bool IsexectAddr, string Addr)
        {
            StringBuilder SqlQuery = new StringBuilder();
           
            bool ISFirst = true;
            if (HcbReferance.Trim() != "")
            {
                string RefNo = HcbReferance.Substring(3);
                SqlQuery.Append(" HCBReference = @HCBReference");
                ISFirst = false;
            }
            if (CaseManager != 0)
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                SqlQuery.Append(" HCBNurseID = @HCBNurseID");
            }
            if (CaseAssessor != 0)
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                SqlQuery.Append(" IncoID = @IncoID");
            }
            if (SurName.Trim() != "")
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                if (IsexectSurName)
                {

                    SqlQuery.Append(" ClientSurname = @ClientSurname");
                }
                else
                {
                    SqlQuery.Append(" ClientSurname like('*' & @ClientSurname & '*')");
                }

            }
            if (ForeName.Trim() != "")
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                if (IsexectForeName)
                {
                    SqlQuery.Append(" ClientForenames  = @ClientForenames");
                }
                else
                {
                    SqlQuery.Append("  ClientForenames like ('*' & @ForeName& '*')");
                }

            }
            if (DateOfBirth.Trim() != "")
            {
                //DateTime dob = DateTime.Parse(DateOfBirth);
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                SqlQuery.Append(" DateValue(Format([ClientDOB],\"dd/mm/yyyy\")) = DateValue(@DateOfBirth)");
            }
            if (Addr.Trim() != "")
            {
                if (!ISFirst) SqlQuery.Append(" and ");
                else ISFirst = false;
                if (IsexectAddr)
                {
                    SqlQuery.Append(" ClientAddress   =  @ClientAddress");
                }
                else
                {
                    SqlQuery.Append("  ClientAddress  like ('*' & @ClientAddress & '*')");
                }
            }

            //string StrFinalQuery = "Select * from ClientRecord ";
            if (!ISFirst)
            {
                SqlQuery.Insert(0, " where ");
                //StrFinalQuery = StrFinalQuery + " where ";
                //StrFinalQuery = StrFinalQuery + SqlQuery.ToString();
            }
            SqlQuery.Insert(0, " Select \"DCP\"+Right(\"00000\"+CStr([HCBReference]),5) AS RefNo ,HCBReference , HCBNurseID, NurseName, ClaimHandlerID, ClaimHandlerName, ClientForenames + ' '+ ClientSurname AS ClientName,  Format([ClientRecord.ClientDOB],\"dd/mm/yyyy\") AS ClientDOB  from ClientRecord ");
            //lbltest.Text = SqlQuery.ToString();

            // SELECT ClientRecord.HCBReference, ClientRecord.HCBNurseID, ClientRecord.IncoID, ClientRecord.ClientSurname,
            // ClientRecord.ClientForenames, ClientRecord.ClientDOB, ClientRecord.ClientAddress
            // FROM ClientRecord

            //WHERE (((ClientRecord.)="@HVBReferance") AND ((ClientRecord.HCBNurseID)="@caseManager") AND ((ClientRecord.IncoID)="@CaseAssessor") AND ((ClientRecord.)="@ckientSurname") 
            // AND ((ClientRecord.ClientForenames)="@ForeName") AND ((ClientRecord.ClientDOB)="@DOB") AND ((ClientRecord.ClientAddress)="@Address"));
            SqlQuery.Append(" order by ClientRecord.HCBReference");

            return SqlQuery.ToString();
        }
        
        #region ClientAmndment

        //Gaurav 20-03-2014
        public static DataSet GetAmendment(int UID, string UserType)
        {            
            DataSet ds = new DataSet();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                
                //string Query = "SELECT AmendmentDetails.ID, AmendmentDetails.UserID, AmendmentDetails.HCBRefNo,Format(AmendmentDetails.TimeAccessed,\"dd/MM/yyyy hh:mm:ss AM/PM\") As TimeAccessed, Users.FullName,\"DCP\"+Right(\"00000\"+CStr(AmendmentDetails.HCBRefNo),5) AS RefNO, IIf(AmendmentDetails.Type=0,\"Viewed\",\"Updated\") AS AmdType FROM AmendmentDetails INNER JOIN Users ON AmendmentDetails.UserID=Users.UserID";
                string Query = "GetAmendmentDetailsByUserId";
                 
                 //IIF([col2]<10, "Less then 10", IIF([col2]<=20, "Less then20", "something else")
                //switch (UserType.ToLower())
                //{
                //    case "nurse":
                //        Query += " where AmendmentDetails.UserID = " + UID; //nurse
                //        break;
                //    case "inco":
                //        Query += " where AmendmentDetails.UserID = " + UID; //inco
                //        break;
                //    case "admin":
                //        break;
                //    default:
                //        break;
                //}


                DbCommand comm = db.GetStoredProcCommand(Query);

                if(UID>0)
                {
                        db.AddInParameter(comm,"UserId",DbType.Int32,UID);

                }
                else
                {
                    db.AddInParameter(comm, "UserId", DbType.Int32,DBNull.Value);
                }
                

                if(!String.IsNullOrEmpty(UserType))
                {
                    db.AddInParameter(comm,"UserType",DbType.String,UserType);
                }
                else
                {
                    db.AddInParameter(comm, "UserType", DbType.String, DBNull.Value);
                }

                ds = db.ExecuteDataSet(comm);

                ds.Tables[0].TableName = "Amend";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public void UpdateAmendment(int RefNo, int userid, int AmdType, int Amdid)
        {
            try
            {
                DbCommand comm = this.db.GetStoredProcCommand("UpdateAmendment");

                this.db.AddInParameter(comm, "@Type", DbType.Int64, AmdType);
                this.db.AddInParameter(comm, "@AmdID", DbType.Int64, Amdid);
                                
                this.db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        
        public int InsertAmndment(int RefNo, int userid, int AmdType)
        {
            try
            {
                conn.Open();
                Int32 Amdid = 0;
                OleDbCommand com = new OleDbCommand("InsertAmendment", conn);
                com.CommandType = CommandType.StoredProcedure;
                this.db.AddOutParameter(com, "@ID", DbType.Int32, 1024);
                com.Parameters.Add(new OleDbParameter("@userid", OleDbType.BigInt)).Value = userid;
                com.Parameters.Add(new OleDbParameter("@HCBRefNo", OleDbType.BigInt)).Value = RefNo;
                com.Parameters.Add(new OleDbParameter("@type", OleDbType.BigInt)).Value = AmdType;
                com.ExecuteNonQuery();
                Amdid = Convert.ToInt32(this.db.GetParameterValue(com, "@ID")); 
                //com.CommandType = CommandType.Text;
                //com.CommandText = "select @@Identity";
                //Amdid = Convert.ToInt32(com.ExecuteScalar());

                return Amdid;
            }
            catch (Exception ex)
            {
                conn.Close();                
                throw;
            }
        }

        //public static DataSet GetReport1(String query)
        //{

        //     DataSet dsLIst = new DataSet();
        //     try
        //     {
        //         Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        //         DbCommand comm = db.GetSqlStringCommand(query);

            
        //        dsLIst = db.ExecuteDataSet(comm);

        //        dsLIst.Tables[0].TableName = "Report";

        //        if (dsLIst != null & dsLIst.Tables[0].Rows.Count < 1)
        //            dsLIst = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        dsLIst = null;
        //    }
        //    return dsLIst;

        //}
        public static DataSet GetIncoUsers()
        {

            DataSet dsLIst = new DataSet();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                DbCommand comm = db.GetStoredProcCommand("GetIncoUsers");
                dsLIst = db.ExecuteDataSet(comm);

                dsLIst.Tables[0].TableName = "Report";

                if (dsLIst != null & dsLIst.Tables[0].Rows.Count < 1)
                    dsLIst = null;

            }
            catch (Exception ex)
            {
                dsLIst = null;
            }
            return dsLIst;

        }
        public static DataSet GetClientReportDetail(DateTime startDate, DateTime endDate)
        {

            DataSet dsLIst = new DataSet();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                DbCommand comm = db.GetStoredProcCommand("GetClientReportDetail");

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(comm, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(comm, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MaxValue > endDate)
                {
                    db.AddInParameter(comm, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(comm, "enddate", DbType.DateTime, DBNull.Value);
                }



                dsLIst = db.ExecuteDataSet(comm);

                dsLIst.Tables[0].TableName = "Report";

                if (dsLIst != null & dsLIst.Tables[0].Rows.Count < 1)
                    dsLIst = null;
            }
            catch (Exception ex)
            {
                dsLIst = null;
            }
            return dsLIst;
        }
        public static DataSet GetClientReport(DateTime startDate, DateTime endDate)
        {

            DataSet dsLIst = new DataSet();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                DbCommand comm = db.GetStoredProcCommand("GetClientReport");

                if (DateTime.MinValue < startDate)
                {
                    db.AddInParameter(comm, "startdate", DbType.DateTime, startDate);
                }
                else
                {
                    db.AddInParameter(comm, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MaxValue > endDate)
                {
                    db.AddInParameter(comm, "enddate", DbType.DateTime, endDate);
                }
                else
                {
                    db.AddInParameter(comm, "enddate", DbType.DateTime, DBNull.Value);
                }



                dsLIst = db.ExecuteDataSet(comm);

                dsLIst.Tables[0].TableName = "Report";

                if (dsLIst != null & dsLIst.Tables[0].Rows.Count < 1)
                    dsLIst = null;
            }
            catch (Exception ex)
            {
                dsLIst = null;
            }
            return dsLIst;
        }
        public static DataSet GetReport(DateTime startDate,DateTime endDate)
        {

            DataSet dsLIst = new DataSet();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                DbCommand comm = db.GetStoredProcCommand("GetMonthlyClientReport");

                if(DateTime.MinValue<startDate)
                {
                    db.AddInParameter(comm,"startdate",DbType.DateTime,startDate);
                }
                else
                {
                    db.AddInParameter(comm, "startdate", DbType.DateTime, DBNull.Value);
                }

                if (DateTime.MaxValue > endDate)
                {
                    db.AddInParameter(comm, "enddate", DbType.DateTime,endDate);
                }
                else
                {
                    db.AddInParameter(comm, "enddate", DbType.DateTime, DBNull.Value);
                }



                dsLIst = db.ExecuteDataSet(comm);

                dsLIst.Tables[0].TableName = "Report";

                if (dsLIst != null & dsLIst.Tables[0].Rows.Count < 1)
                    dsLIst = null;
            }
            catch (Exception ex)
            {
                dsLIst = null;
            }
            return dsLIst;
        }
        #endregion

    }
}
