﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class MetLifeStaticValue
    {
         #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        #endregion
         #region Constructor

        public MetLifeStaticValue()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion
        #region Properties

        public int MetLifeStaticValuesId
        { get; set; }

        public string MetLifeTelephone
        { get; set; }

        public string MetLifeEmail { get; set; }
        
        #endregion
        #region [Method for MetLifeStaticValues]
        public DataSet GetDetailMetLifeStaticValues()
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetDetailMetLifeStaticValues";
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);                
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "StaticValues";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }


        public void InsertMetLifeStaticValues()
        {
            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;            
            try
            {
                
                conn = DbConnection.OpenConnection();
                string queryClient = "InsertMetLifeStaticValues";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                if (!string.IsNullOrEmpty(this.MetLifeTelephone))
                {
                    this.db.AddInParameter(cmd, "@MetLifeTelephone", DbType.String, this.MetLifeTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@MetLifeTelephone", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.MetLifeEmail))
                {
                    this.db.AddInParameter(cmd, "@MetLifeEmail", DbType.String, this.MetLifeEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@MetLifeEmail", DbType.String, DBNull.Value);
                }
               
                cmd.ExecuteNonQuery();

                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
               
            }


           
        }
     
   
        #endregion
    }
}
