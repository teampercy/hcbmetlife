﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class Notes
    {
        #region variables
        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());

        #endregion

        #region Constructor

        public Notes()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties

        public int NoteID
        { get; set; }

        public int HCBReference
        { get; set; }

        public string NoteText
        { get; set; }

        public string UserName
        { get; set; }

        public DateTime DateStamp
        { get; set; }

        public string Email1
        { get; set; }

        public string Email2
        { get; set; }

        public string Email3
        { get; set; }

        public string Email4
        { get; set; }

        public string Email5
        { get; set; }

        public string EmailResult
        { get; set; }

        public int LoggedInUserID
        { get; set; }

        #endregion

        #region Methods

        public bool Save()
        {
            if (this.NoteID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.NoteID > 0)
                {
                    return this.Update();
                }
                else
                {
                    this.NoteID = 0;
                    return false;
                }
            }
        }

        private bool Insert()
        {
            DataSet ds = new DataSet();
            
            int id = 0;
            try
            {
                
                string queryClient = "CreateNote";
                
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);

                if (this.HCBReference > 0)
                {
                    
                     this.db.AddInParameter(cmd,"@HCBReference",DbType.Int32, this.HCBReference);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32, DBNull.Value);

                }
                if (!String.IsNullOrEmpty(this.NoteText))
                {
                     this.db.AddInParameter(cmd,"@Note", DbType.String,this.NoteText);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@Note", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.UserName))
                {
                     this.db.AddInParameter(cmd,"@UserName", DbType.String, this.UserName);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@UserName", DbType.String, DBNull.Value);
                }
                //if (this.DateStamp > DateTime.MinValue)
                //{
                //     this.db.AddInParameter(cmd,"@DateStamp", DbType.DateTime, this.DateStamp);
                //}
                //else
                //{
                //    this.db.AddInParameter(cmd, "@DateStamp", DbType.DateTime, DBNull.Value);
                //}
                if (!string.IsNullOrEmpty(this.Email1))
                {
                     this.db.AddInParameter(cmd,"@Email1", DbType.String, this.Email1);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@Email1", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email2))
                {
                     this.db.AddInParameter(cmd,"@Email2", DbType.String, this.Email2);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@Email2", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email3))
                {
                     this.db.AddInParameter(cmd,"@Email3", DbType.String, this.Email3);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@Email3", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email4))
                {                    
                     this.db.AddInParameter(cmd,"@Email4", DbType.String, this.Email4);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@Email4", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email5))
                {
                     this.db.AddInParameter(cmd,"@Email5", DbType.String, this.Email5);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@Email5", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.EmailResult))
                {
                     this.db.AddInParameter(cmd,"@EmailResult", DbType.String, this.EmailResult);
                }
                else
                {
                     this.db.AddInParameter(cmd,"@EmailResult", DbType.String, DBNull.Value);
                }
                if(this.LoggedInUserID>0)
                {
                    this.db.AddInParameter(cmd, "@LoggedInUserID", DbType.Int32, this.LoggedInUserID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@LoggedInUserID", DbType.Int32, DBNull.Value);
                }
               
                this.db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ds.Dispose();
              return false;
            }
            
            return true;
        }

        private bool Update()
        {
            try
            {
                
                DbCommand cmd = this.db.GetStoredProcCommand("UpdateNotes");


                if (this.HCBReference > 0)
                {
                   
                   this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,this.HCBReference);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@HCBReference", DbType.Int32,DBNull.Value);

                }
                if (!String.IsNullOrEmpty(this.NoteText))
                {
                   this.db.AddInParameter(cmd,"@Note", DbType.String, this.NoteText);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@Note", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.UserName))
                {
                   this.db.AddInParameter(cmd,"@UserName", DbType.String, this.UserName);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@UserName", DbType.String, DBNull.Value);
                }
                if (this.DateStamp > DateTime.MinValue)
                {
                   this.db.AddInParameter(cmd,"@DateStamp", DbType.Date ,this.DateStamp);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@DateStamp", DbType.Date, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email1))
                {
                   this.db.AddInParameter(cmd,"@Email1", DbType.String, this.Email1);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@Email1", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email2))
                {
                   this.db.AddInParameter(cmd,"@Email2", DbType.String, this.Email2);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@Email2", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email3))
                {
                   this.db.AddInParameter(cmd,"@Email3", DbType.String, this.Email3);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@Email3", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email4))
                {
                    
                   this.db.AddInParameter(cmd,"@Email4", DbType.String, this.Email4);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@Email4", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.Email5))
                {
                   this.db.AddInParameter(cmd,"@Email5", DbType.String, this.Email5);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@Email5", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.EmailResult))
                {
                   this.db.AddInParameter(cmd,"@EmailResult", DbType.String, this.EmailResult);
                }
                else
                {
                   this.db.AddInParameter(cmd,"@EmailResult", DbType.String, DBNull.Value);
                }
                if(this.LoggedInUserID>0)
                {
                    this.db.AddInParameter(cmd, "@LoggedInUserID", DbType.Int32, this.LoggedInUserID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@LoggedInUserID", DbType.Int32, DBNull.Value);
                }

                this.db.ExecuteNonQuery(cmd);
                
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                //conn.Close();
                return false;
            }

            return true;
        }

        public DataSet GetNotes()
        {
            DataSet ds = new DataSet();
           
            try
            {                
                string queryClient = "GetNotes";
                
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                if (this.HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }

                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Notes";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {               
                ds.Dispose();
                throw;
            }

            return ds;
        }

        #endregion
    }
}
