﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBBLL
{
    public class ReferralForm
    {
        #region Variable Declaration
        private Database db;
        #endregion

        #region Constructors
        public ReferralForm()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        /// <summary>
        /// Initializes a new instance of the ReferralForm class.
        /// </summary>
        /// <param name="ReferralFormId">Sets the value of ReferralFormId.</param>    
        public ReferralForm(int ReferralFormID)
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
            this.ReferralFormID = ReferralFormID;
        }
        #endregion

        #region Properties
        public int ReferralFormID { get; set; }
        public string ReferralName { get; set; }
        public string ReferralJobTitle { get; set; }
        public string ReferralCompanyName { get; set; }
        public string ReferralAddress { get; set; }
        public string ReferralTelephone { get; set; }
        public string ReferralEmail { get; set; }
        public string DateOfReferral { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAddress { get; set; }
        public string DateOfBirth { get; set; }
        public string EmployeeEmail { get; set; }
        public string EmployeeJobTitleAndLocation { get; set; }
        public string EmployeeTelephone { get; set; }
        public string LineManagerNameAndJobTitle { get; set; }
        public string LineManagerTelephoneOrEmail { get; set; }
        public string IsEmployee { get; set; }
        public string EmployeeContract { get; set; }
        public string PartTimeFromMonday { get; set; }
        public string PartTimeToMonday { get; set; }
        public string PartTimeFromTuesday { get; set; }
        public string PartTimeToTuesday { get; set; }
        public string PartTimeFromWednesday { get; set; }
        public string PartTimeToWednesday { get; set; }
        public string PartTimeFromThursday { get; set; }
        public string PartTimeToThursday { get; set; }
        public string PartTimeFromFriday { get; set; }
        public string PartTimeToFriday { get; set; }
        public string EmployeeAware { get; set; }
        public string ConsentGiven { get; set; }
        public string BroadOutline { get; set; }
        public string DateOfIllness { get; set; }
        public string OtherInformation { get; set; }
        public string OtherAdvice { get; set; }
        public string AddRelevantInformation { get; set; }
        public string DetailsOfPreviousSickness { get; set; }
        public string EmployeeMedicalInsurance { get; set; }
        #endregion

        #region Methods

        /// <summary>
        /// Loads the details for ReferralForm.
        /// </summary>
        /// <returns>True if Load operation is successful; Else False.</returns>
        public bool Load()
        {
            try
            {
                if (this.ReferralFormID != 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("ReferralFormGetDetails");
                    this.db.AddInParameter(com, "ReferralFormID", DbType.Int32, this.ReferralFormID);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.ReferralFormID = Convert.ToInt32(dt.Rows[0]["ReferralFormID"]);
                        this.ReferralName = Convert.ToString(dt.Rows[0]["ReferralName"]);
                        this.ReferralJobTitle = Convert.ToString(dt.Rows[0]["ReferralJobTitle"]);
                        this.ReferralCompanyName = Convert.ToString(dt.Rows[0]["ReferralCompanyName"]);
                        this.ReferralAddress = Convert.ToString(dt.Rows[0]["ReferralAddress"]);
                        this.ReferralTelephone = Convert.ToString(dt.Rows[0]["ReferralTelephone"]);
                        this.ReferralEmail = Convert.ToString(dt.Rows[0]["ReferralEmail"]);
                        this.DateOfReferral = Convert.ToString(dt.Rows[0]["DateOfReferral"]);
                        this.EmployeeName = Convert.ToString(dt.Rows[0]["EmployeeName"]);
                        this.EmployeeAddress = Convert.ToString(dt.Rows[0]["EmployeeAddress"]);
                        this.DateOfBirth = Convert.ToString(dt.Rows[0]["DateOfBirth"]);
                        this.EmployeeEmail = Convert.ToString(dt.Rows[0]["EmployeeEmail"]);
                        this.EmployeeJobTitleAndLocation = Convert.ToString(dt.Rows[0]["EmployeeJobTitleAndLocation"]);
                        this.EmployeeTelephone = Convert.ToString(dt.Rows[0]["EmployeeTelephone"]);
                        this.LineManagerNameAndJobTitle = Convert.ToString(dt.Rows[0]["LineManagerNameAndJobTitle"]);
                        this.LineManagerTelephoneOrEmail = Convert.ToString(dt.Rows[0]["LineManagerTelephoneOrEmail"]);
                        this.IsEmployee = Convert.ToString(dt.Rows[0]["IsEmployee"]);
                        this.EmployeeContract = Convert.ToString(dt.Rows[0]["EmployeeContract"]);
                        this.PartTimeFromMonday = Convert.ToString(dt.Rows[0]["PartTimeFromMonday"]);
                        this.PartTimeToMonday = Convert.ToString(dt.Rows[0]["PartTimeToMonday"]);
                        this.PartTimeFromTuesday = Convert.ToString(dt.Rows[0]["PartTimeFromTuesday"]);
                        this.PartTimeToTuesday = Convert.ToString(dt.Rows[0]["PartTimeToTuesday"]);
                        this.PartTimeFromWednesday = Convert.ToString(dt.Rows[0]["PartTimeFromWednesday"]);
                        this.PartTimeToWednesday = Convert.ToString(dt.Rows[0]["PartTimeToWednesday"]);
                        this.PartTimeFromThursday = Convert.ToString(dt.Rows[0]["PartTimeFromThursday"]);
                        this.PartTimeToThursday = Convert.ToString(dt.Rows[0]["PartTimeToThursday"]);
                        this.PartTimeFromFriday = Convert.ToString(dt.Rows[0]["PartTimeFromFriday"]);
                        this.PartTimeToFriday = Convert.ToString(dt.Rows[0]["PartTimeToFriday"]);
                        this.EmployeeAware = Convert.ToString(dt.Rows[0]["EmployeeAware"]);
                        this.ConsentGiven = Convert.ToString(dt.Rows[0]["ConsentGiven"]);
                        this.BroadOutline = Convert.ToString(dt.Rows[0]["BroadOutline"]);
                        this.DateOfIllness = Convert.ToString(dt.Rows[0]["DateOfIllness"]);
                        this.OtherInformation = Convert.ToString(dt.Rows[0]["OtherInformation"]);
                        this.OtherAdvice = Convert.ToString(dt.Rows[0]["OtherAdvice"]);
                        this.AddRelevantInformation = Convert.ToString(dt.Rows[0]["AddRelevantInformation"]);
                        this.DetailsOfPreviousSickness = Convert.ToString(dt.Rows[0]["DetailsOfPreviousSickness"]);
                        this.EmployeeMedicalInsurance = Convert.ToString(dt.Rows[0]["EmployeeMedicalInsurance"]);
                                                
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }

        /// <summary>
        /// Inserts details for ReferralForm if ReferralFormID = 0.
        /// Else updates details for ReferralForm.
        /// </summary>
        /// <returns>True if Save operation is successful; Else False.</returns>
        public bool Save()
        {            
            if(this.ReferralFormID==0)
            {
                return this.Insert();                
            }
            else
            {
                if (this.ReferralFormID > 0)
                {                    
                    return this.Update();
                }
                else
                {
                    this.ReferralFormID = 0;
                    return false;
                }
            }

        }

        /// <summary>
        /// Inserts details for ReferralForm.
        /// Saves newly created Id in ReferralFormID.
        /// </summary>
        /// <returns>True if Insert operation is successful; Else False.</returns>
        private bool Insert()
        {                      
            try
            {
                DbCommand cmd = this.db.GetStoredProcCommand("InsertReferralForm");

                this.db.AddOutParameter(cmd, "ReferralFormID", DbType.Int32, 1024);

                if(!String.IsNullOrEmpty(this.ReferralName))
                {
                    this.db.AddInParameter(cmd, "ReferralName", DbType.String, ReferralName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralName", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralJobTitle))
                {
                    this.db.AddInParameter(cmd, "ReferralJobTitle", DbType.String, ReferralJobTitle);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralJobTitle", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralCompanyName))
                {
                    this.db.AddInParameter(cmd, "ReferralCompanyName", DbType.String, ReferralCompanyName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralCompanyName", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralAddress))
                {
                    this.db.AddInParameter(cmd, "ReferralAddress", DbType.String, ReferralAddress);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralAddress", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralTelephone))
                {
                    this.db.AddInParameter(cmd, "ReferralTelephone", DbType.String, ReferralTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralTelephone", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralEmail))
                {
                    this.db.AddInParameter(cmd, "ReferralEmail", DbType.String, ReferralEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralEmail", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DateOfReferral))
                {
                    this.db.AddInParameter(cmd, "DateOfReferral", DbType.String, DateOfReferral);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DateOfReferral", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeName))
                {
                    this.db.AddInParameter(cmd, "EmployeeName", DbType.String, EmployeeName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeName", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeAddress))
                {
                    this.db.AddInParameter(cmd, "EmployeeAddress", DbType.String, EmployeeAddress);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeAddress", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DateOfBirth))
                {
                    this.db.AddInParameter(cmd, "DateOfBirth", DbType.String, DateOfBirth);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DateOfBirth", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeEmail))
                {
                    this.db.AddInParameter(cmd, "EmployeeEmail", DbType.String, EmployeeEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeEmail", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeJobTitleAndLocation))
                {
                    this.db.AddInParameter(cmd, "EmployeeJobTitleAndLocation", DbType.String, EmployeeJobTitleAndLocation);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeJobTitleAndLocation", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeTelephone))
                {
                    this.db.AddInParameter(cmd, "EmployeeTelephone", DbType.String, EmployeeTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeTelephone", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.LineManagerNameAndJobTitle))
                {
                    this.db.AddInParameter(cmd, "LineManagerNameAndJobTitle", DbType.String, LineManagerNameAndJobTitle);
                }
                else
                {
                    this.db.AddInParameter(cmd, "LineManagerNameAndJobTitle", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.LineManagerTelephoneOrEmail))
                {
                    this.db.AddInParameter(cmd, "LineManagerTelephoneOrEmail", DbType.String, LineManagerTelephoneOrEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "LineManagerTelephoneOrEmail", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.IsEmployee))
                {
                    this.db.AddInParameter(cmd, "IsEmployee", DbType.String, IsEmployee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "IsEmployee", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeContract))
                {
                    this.db.AddInParameter(cmd, "EmployeeContract", DbType.String, EmployeeContract);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeContract", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromMonday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromMonday", DbType.String, PartTimeFromMonday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromMonday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToMonday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToMonday", DbType.String, PartTimeToMonday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToMonday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromTuesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromTuesday", DbType.String, PartTimeFromTuesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromTuesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToTuesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToTuesday", DbType.String, PartTimeToTuesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToTuesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromWednesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromWednesday", DbType.String, PartTimeFromWednesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromWednesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToWednesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToWednesday", DbType.String, PartTimeToWednesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToWednesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromThursday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromThursday", DbType.String, PartTimeFromThursday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromThursday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToThursday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToThursday", DbType.String, PartTimeToThursday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToThursday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromFriday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromFriday", DbType.String, PartTimeFromFriday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromFriday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToFriday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToFriday", DbType.String, PartTimeToFriday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToFriday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeAware))
                {
                    this.db.AddInParameter(cmd, "EmployeeAware", DbType.String, EmployeeAware);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeAware", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ConsentGiven))
                {
                    this.db.AddInParameter(cmd, "ConsentGiven", DbType.String, ConsentGiven);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ConsentGiven", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.BroadOutline))
                {
                    this.db.AddInParameter(cmd, "BroadOutline", DbType.String, BroadOutline);
                }
                else
                {
                    this.db.AddInParameter(cmd, "BroadOutline", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DateOfIllness))
                {
                    this.db.AddInParameter(cmd, "DateOfIllness", DbType.String, DateOfIllness);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DateOfIllness", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.OtherInformation))
                {
                    this.db.AddInParameter(cmd, "OtherInformation", DbType.String, OtherInformation);
                }
                else
                {
                    this.db.AddInParameter(cmd, "OtherInformation", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.OtherAdvice))
                {
                    this.db.AddInParameter(cmd, "OtherAdvice", DbType.String, OtherAdvice);
                }
                else
                {
                    this.db.AddInParameter(cmd, "OtherAdvice", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.AddRelevantInformation))
                {
                    this.db.AddInParameter(cmd, "AddRelevantInformation", DbType.String, AddRelevantInformation);
                }
                else
                {
                    this.db.AddInParameter(cmd, "AddRelevantInformation", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DetailsOfPreviousSickness))
                {
                    this.db.AddInParameter(cmd, "DetailsOfPreviousSickness", DbType.String, DetailsOfPreviousSickness);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DetailsOfPreviousSickness", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeMedicalInsurance))
                {
                    this.db.AddInParameter(cmd, "EmployeeMedicalInsurance", DbType.String, EmployeeMedicalInsurance);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeMedicalInsurance", DbType.String, DBNull.Value);
                }

                this.db.ExecuteNonQuery(cmd);
                this.ReferralFormID = Convert.ToInt32(this.db.GetParameterValue(cmd, "ReferralFormID"));    // Read in the output parameter value

            }
            catch(Exception ex)
            {                
                // To Do: Handle Exception
                return false;
            }
            return this.ReferralFormID>0;
        }

        /// <summary>
        /// Updates details for ReferralForm.
        /// </summary>
        /// <returns>True if Update operation is successful; Else False.</returns>
        private bool Update()
        {
            try
            {
                DbCommand cmd = this.db.GetStoredProcCommand("ReferralFormUpdate");

                this.db.AddInParameter(cmd, "ReferralFormID", DbType.Int32, this.ReferralFormID);

                if (!String.IsNullOrEmpty(this.ReferralName))
                {
                    this.db.AddInParameter(cmd, "ReferralName", DbType.String, ReferralName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralName", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralJobTitle))
                {
                    this.db.AddInParameter(cmd, "ReferralJobTitle", DbType.String, ReferralJobTitle);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralJobTitle", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralCompanyName))
                {
                    this.db.AddInParameter(cmd, "ReferralCompanyName", DbType.String, ReferralCompanyName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralCompanyName", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralAddress))
                {
                    this.db.AddInParameter(cmd, "ReferralAddress", DbType.String, ReferralAddress);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralAddress", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralTelephone))
                {
                    this.db.AddInParameter(cmd, "ReferralTelephone", DbType.String, ReferralTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralTelephone", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ReferralEmail))
                {
                    this.db.AddInParameter(cmd, "ReferralEmail", DbType.String, ReferralEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ReferralEmail", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DateOfReferral))
                {
                    this.db.AddInParameter(cmd, "DateOfReferral", DbType.String, DateOfReferral);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DateOfReferral", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeName))
                {
                    this.db.AddInParameter(cmd, "EmployeeName", DbType.String, EmployeeName);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeName", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeAddress))
                {
                    this.db.AddInParameter(cmd, "EmployeeAddress", DbType.String, EmployeeAddress);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeAddress", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DateOfBirth))
                {
                    this.db.AddInParameter(cmd, "DateOfBirth", DbType.String, DateOfBirth);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DateOfBirth", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeEmail))
                {
                    this.db.AddInParameter(cmd, "EmployeeEmail", DbType.String, EmployeeEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeEmail", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeJobTitleAndLocation))
                {
                    this.db.AddInParameter(cmd, "EmployeeJobTitleAndLocation", DbType.String, EmployeeJobTitleAndLocation);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeJobTitleAndLocation", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeTelephone))
                {
                    this.db.AddInParameter(cmd, "EmployeeTelephone", DbType.String, EmployeeTelephone);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeTelephone", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.LineManagerNameAndJobTitle))
                {
                    this.db.AddInParameter(cmd, "LineManagerNameAndJobTitle", DbType.String, LineManagerNameAndJobTitle);
                }
                else
                {
                    this.db.AddInParameter(cmd, "LineManagerNameAndJobTitle", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.LineManagerTelephoneOrEmail))
                {
                    this.db.AddInParameter(cmd, "LineManagerTelephoneOrEmail", DbType.String, LineManagerTelephoneOrEmail);
                }
                else
                {
                    this.db.AddInParameter(cmd, "LineManagerTelephoneOrEmail", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.IsEmployee))
                {
                    this.db.AddInParameter(cmd, "IsEmployee", DbType.String, IsEmployee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "IsEmployee", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeContract))
                {
                    this.db.AddInParameter(cmd, "EmployeeContract", DbType.String, EmployeeContract);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeContract", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromMonday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromMonday", DbType.String, PartTimeFromMonday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromMonday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToMonday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToMonday", DbType.String, PartTimeToMonday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToMonday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromTuesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromTuesday", DbType.String, PartTimeFromTuesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromTuesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToTuesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToTuesday", DbType.String, PartTimeToTuesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToTuesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromWednesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromWednesday", DbType.String, PartTimeFromWednesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromWednesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToWednesday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToWednesday", DbType.String, PartTimeToWednesday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToWednesday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromThursday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromThursday", DbType.String, PartTimeFromThursday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromThursday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToThursday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToThursday", DbType.String, PartTimeToThursday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToThursday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeFromFriday))
                {
                    this.db.AddInParameter(cmd, "PartTimeFromFriday", DbType.String, PartTimeFromFriday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeFromFriday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.PartTimeToFriday))
                {
                    this.db.AddInParameter(cmd, "PartTimeToFriday", DbType.String, PartTimeToFriday);
                }
                else
                {
                    this.db.AddInParameter(cmd, "PartTimeToFriday", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeAware))
                {
                    this.db.AddInParameter(cmd, "EmployeeAware", DbType.String, EmployeeAware);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeAware", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.ConsentGiven))
                {
                    this.db.AddInParameter(cmd, "ConsentGiven", DbType.String, ConsentGiven);
                }
                else
                {
                    this.db.AddInParameter(cmd, "ConsentGiven", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.BroadOutline))
                {
                    this.db.AddInParameter(cmd, "BroadOutline", DbType.String, BroadOutline);
                }
                else
                {
                    this.db.AddInParameter(cmd, "BroadOutline", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DateOfIllness))
                {
                    this.db.AddInParameter(cmd, "DateOfIllness", DbType.String, DateOfIllness);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DateOfIllness", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.OtherInformation))
                {
                    this.db.AddInParameter(cmd, "OtherInformation", DbType.String, OtherInformation);
                }
                else
                {
                    this.db.AddInParameter(cmd, "OtherInformation", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.OtherAdvice))
                {
                    this.db.AddInParameter(cmd, "OtherAdvice", DbType.String, OtherAdvice);
                }
                else
                {
                    this.db.AddInParameter(cmd, "OtherAdvice", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.AddRelevantInformation))
                {
                    this.db.AddInParameter(cmd, "AddRelevantInformation", DbType.String, AddRelevantInformation);
                }
                else
                {
                    this.db.AddInParameter(cmd, "AddRelevantInformation", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.DetailsOfPreviousSickness))
                {
                    this.db.AddInParameter(cmd, "DetailsOfPreviousSickness", DbType.String, DetailsOfPreviousSickness);
                }
                else
                {
                    this.db.AddInParameter(cmd, "DetailsOfPreviousSickness", DbType.String, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmployeeMedicalInsurance))
                {
                    this.db.AddInParameter(cmd, "EmployeeMedicalInsurance", DbType.String, EmployeeMedicalInsurance);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmployeeMedicalInsurance", DbType.String, DBNull.Value);
                }

                this.db.ExecuteNonQuery(cmd);                
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
            return false;
        }

        /// <summary>
        /// Get list of ReferralForm for provided parameters.
        /// </summary>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        /// 
        public DataSet GetList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("ReferralFormGetList");
                ds = db.ExecuteDataSet(com);
            }
            catch(Exception ex)
            {
                //To Do: Handle Exception
            }
            return ds;
        }

        /// <summary>
        /// Deletes details of ReferralForm for provided ReferralFormId.
        /// </summary>
        /// <returns>True if Delete operation is successful; Else False.</returns>
        public bool Delete()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("ReferralFormDelete");
                this.db.AddInParameter(com, "ReferralFormID", DbType.Int32, this.ReferralFormID);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }
        #endregion
    }
}
