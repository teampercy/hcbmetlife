﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class Rehabilitation
    {
        #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        #endregion
         #region Constructor

        public Rehabilitation()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion
        #region Properties

        public int RehabilitationId
        { get; set; }

        public int HCBReference
        { get; set; }

        public int RehabilitationTypeId
        { get; set; }

        public int RehabilitationProviderId
        { get; set; }
        public string InstructedDate { get; set; }
        public string ReportedDate { get; set; }
        public decimal RehabilitationFee
        { get; set; }
       
        #endregion
        #region [Method for Rehabilitation]
        public DataSet GetListHCBRehabilitation(int HCBReference)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetListHCBRehabilitation";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Rehab";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public DataSet GetDetailHCBRehabilitation(int RehabilitationId)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetDetailHCBRehabilitation";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (RehabilitationId > 0)
                {
                    this.db.AddInParameter(cmd, "RehabilitationId", DbType.Int32, RehabilitationId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "RehabilitationId", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Rehab";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public int Save()
        {
            if (this.RehabilitationId == 0)
            {
                return this.Insert();

            }
            else
            {
                if (this.RehabilitationId > 0)
                {
                    this.Update();
                    return RehabilitationId;
                }
                else
                {
                    this.RehabilitationId = 0;
                    return RehabilitationId;
                }
            }
        }
        private int Insert()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "InsertHCBRehabilitation";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.HCBReference > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                }
                if (this.RehabilitationTypeId > 0)
                {

                    this.db.AddInParameter(cmd, "@RehabilitationTypeId", DbType.Int32, this.RehabilitationTypeId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabilitationTypeId", DbType.Int32, DBNull.Value);
                }
                if (this.RehabilitationProviderId > 0)
                {

                    this.db.AddInParameter(cmd, "@RehabilitationProviderId", DbType.Int32, this.RehabilitationProviderId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabilitationProviderId", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.InstructedDate))
                {
                    this.db.AddInParameter(cmd, "@InstructedDate", DbType.String, this.InstructedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@InstructedDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReportedDate))
                {
                    this.db.AddInParameter(cmd, "@ReportedDate", DbType.String, this.ReportedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReportedDate", DbType.String, DBNull.Value);
                }
                if (this.RehabilitationFee > 0)
                {
                    this.db.AddInParameter(cmd, "@RehabilitationFee", DbType.Decimal, this.RehabilitationFee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabilitationFee", DbType.Decimal, DBNull.Value);
                }
                
                cmd.ExecuteNonQuery();

                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        private int Update()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "UpdateHCBRehabilitation";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (this.RehabilitationId > 0)
                {

                    this.db.AddInParameter(cmd, "@RehabilitationId", DbType.Int32, this.RehabilitationId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabilitationId", DbType.Int32, DBNull.Value);
                }
                if (this.RehabilitationTypeId > 0)
                {

                    this.db.AddInParameter(cmd, "@RehabilitationTypeId", DbType.Int32, this.RehabilitationTypeId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabilitationTypeId", DbType.Int32, DBNull.Value);
                }
                if (this.RehabilitationProviderId > 0)
                {

                    this.db.AddInParameter(cmd, "@RehabilitationProviderId", DbType.Int32, this.RehabilitationProviderId);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabilitationProviderId", DbType.Int32, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.InstructedDate))
                {
                    this.db.AddInParameter(cmd, "@InstructedDate", DbType.String, this.InstructedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@InstructedDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.ReportedDate))
                {
                    this.db.AddInParameter(cmd, "@ReportedDate", DbType.String, this.ReportedDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@ReportedDate", DbType.String, DBNull.Value);
                }
                if (this.RehabilitationFee > 0)
                {
                    this.db.AddInParameter(cmd, "@RehabilitationFee", DbType.Decimal, this.RehabilitationFee);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@RehabilitationFee", DbType.Decimal, DBNull.Value);
                }

                cmd.ExecuteNonQuery();

                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        public DataSet DeleteHCBRehabilitation()
        {
            DataSet ds = new DataSet();

            try
            {

                DbCommand comm = this.db.GetStoredProcCommand("DeleteHCBRehabilitation");
                if (this.RehabilitationId > 0)
                {

                    this.db.AddInParameter(comm, "@RehabilitationId", DbType.Int32, this.RehabilitationId);
                }
                else
                {
                    this.db.AddInParameter(comm, "@RehabilitationId", DbType.Int32, DBNull.Value);
                }
               
                this.db.ExecuteNonQuery(comm);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion
    }
}
