﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Configuration;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBBLL
{
    public class ServiceRequired
    {

        #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());

        #endregion

        #region Constructor

        public ServiceRequired()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties

        public int ServiceId
        {
            get;
            set;
        }

        public string ServiceName
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public DataSet GetServices()
        {
            DataSet ds = new DataSet();

            try
            {    
                DbCommand com = this.db.GetStoredProcCommand("GetServiceRequired");
                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "Service";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
                
            }
            catch (Exception ex)
            {
                //conn.Close();
                throw;
            }

            return ds;
        }

        #endregion
    }
}
