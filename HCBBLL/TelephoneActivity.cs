﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace HCBBLL
{
    public class TelephoneActivity
    {
        #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        #endregion

        #region Constructor

        public TelephoneActivity()
        {
            this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
        }

        #endregion

        #region Properties

        public int HCBTelephoneID
        { get; set; }

        public int HCBReference
        { get; set; }


        public string CallDate
        { get; set; }

        public string CallTime
        { get; set; }

        public int TypeOfCall
        { get; set; }

        public string CallExpectRTWDateOptimum
        { get; set; }
        public string CallExpectRTWDateMaximum
        { get; set; }

        public string CallCaseMngtRecom
        { get; set; }

        public string CallNxtSchedule
        { get; set; }

        public decimal FeeCharged
        { get; set; }

        //public bool Call1Aborted
        //{ get; set; }

        //public string Call2Date
        //{ get; set; }

        //public string Call2Time
        //{ get; set; }

        //public string Call2ExpectRTWDate
        //{ get; set; }

        //public string Call2CasemngtRecom
        //{ get; set; }

        //public string Call2OriginalRTW
        //{ get; set; }

        //public string Call2EnvisagedDate
        //{ get; set; }

        //public string Call2NxtSchedule
        //{ get; set; }

        //public bool Call2Aborted
        //{ get; set; }

        //public string Call3Date
        //{ get; set; }

        //public string Call3Time
        //{ get; set; }

        //public string Call3ExpectRTWDate
        //{ get; set; }

        //public string Call3FurtherIntervention
        //{ get; set; }

        //public string Call3RTWDateAgreed
        //{ get; set; }

        //public string Call3CaseMngtInvolve
        //{ get; set; }

        //public string Call3RehabServicePurchase
        //{ get; set; }

        //public bool Call3Aborted
        //{ get; set; }


        //public double HomeVisitCurrency
        //{ get; set; }

        //public double Call1Fee
        //{ get; set; }

        //public double Call2Fee
        //{ get; set; }

        //public double Call3Fee
        //{ get; set; }

        #endregion

        #region Methods

        public int Save()
        {
            if (this.HCBTelephoneID == 0)
            {
                 //this.HCBTelephoneID = this.Insert();
                 return this.Insert();
               

            }
            else
            {
                if (this.HCBTelephoneID > 0)
                {
                    this.Update();
                    return HCBTelephoneID;
                }
                else
                {
                    this.HCBTelephoneID = 0;
                    return HCBTelephoneID;
                }
            }
        }
        private int Insert()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "CreateTelephoneAcitivity";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                this.db.AddOutParameter(cmd, "@HCBTelephoneID", DbType.Int32, 1024);
                if (this.HCBReference > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                }
                
                if (!string.IsNullOrEmpty(this.CallDate))
                {
                    this.db.AddInParameter(cmd, "@CallDate", DbType.String, this.CallDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallTime))
                {
                    this.db.AddInParameter(cmd, "@CallTime", DbType.String, this.CallTime);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallTime", DbType.String, DBNull.Value);
                }
                if (this.TypeOfCall > 0)
                {
                    this.db.AddInParameter(cmd, "@TypeofCall", DbType.Int16, this.TypeOfCall);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@TypeofCall", DbType.Int16, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallExpectRTWDateOptimum))
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateOptimum", DbType.String, this.CallExpectRTWDateOptimum);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateOptimum", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallExpectRTWDateMaximum))
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateMaximum", DbType.String, this.CallExpectRTWDateMaximum);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateMaximum", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallCaseMngtRecom))
                {

                    this.db.AddInParameter(cmd, "@CallCaseMngtRecom", DbType.String, this.CallCaseMngtRecom);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallCaseMngtRecom", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallNxtSchedule))
                {
                    this.db.AddInParameter(cmd, "@CallNxtSchedule", DbType.String, this.CallNxtSchedule);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallNxtSchedule", DbType.String, DBNull.Value);
                }
                if (this.FeeCharged > 0)
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, this.FeeCharged);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, DBNull.Value);
                }
                //this.db.AddInParameter(cmd, "@Call1Aborted", DbType.Boolean, this.Call1Aborted);


                
               // string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();
                id = Convert.ToInt32(this.db.GetParameterValue(cmd, "@HCBTelephoneID"));  
                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        private int Update()
        {

            DataSet ds = new DataSet();
            OleDbConnection conn = null;
            OleDbCommand cmd = null;
            int id = 0;
            try
            {


                //DbCommand cmd = this.db.GetStoredProcCommand("CreateHCBDuration");
                conn = DbConnection.OpenConnection();
                string queryClient = "UpdateTelephoneAcitivity";
                cmd = new OleDbCommand(queryClient, conn);
                cmd.CommandType = CommandType.StoredProcedure;

       
                if (this.HCBTelephoneID > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBTelephoneID", DbType.Int32, this.HCBTelephoneID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBTelephoneID", DbType.Int32, DBNull.Value);
                }
                if (this.HCBReference > 0)
                {

                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, this.HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@HCBReference", DbType.Int32, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.CallDate))
                {
                    this.db.AddInParameter(cmd, "@CallDate", DbType.String, this.CallDate);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallDate", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallTime))
                {
                    this.db.AddInParameter(cmd, "@CallTime", DbType.String, this.CallTime);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallTime", DbType.String, DBNull.Value);
                }
                if (this.TypeOfCall > 0)
                {
                    this.db.AddInParameter(cmd, "@TypeofCall", DbType.Int16, this.TypeOfCall);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@TypeofCall", DbType.Int16, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallExpectRTWDateOptimum))
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateOptimum", DbType.String, this.CallExpectRTWDateOptimum);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateOptimum", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallExpectRTWDateMaximum))
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateMaximum", DbType.String, this.CallExpectRTWDateMaximum);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallExpectRTWDateMaximum", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallCaseMngtRecom))
                {

                    this.db.AddInParameter(cmd, "@CallCaseMngtRecom", DbType.String, this.CallCaseMngtRecom);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallCaseMngtRecom", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.CallNxtSchedule))
                {
                    this.db.AddInParameter(cmd, "@CallNxtSchedule", DbType.String, this.CallNxtSchedule);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@CallNxtSchedule", DbType.String, DBNull.Value);
                }
                if (this.FeeCharged > 0)
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, this.FeeCharged);
                }
                else
                {
                    this.db.AddInParameter(cmd, "@FeeCharged", DbType.Decimal, DBNull.Value);
                }
                //this.db.AddInParameter(cmd, "@Call1Aborted", DbType.Boolean, this.Call1Aborted);



                // string query1 = "Select @@Identity";

                cmd.ExecuteNonQuery();
                //this.HCBTelephoneID = Convert.ToInt32(this.db.GetParameterValue(cmd, "@HCBTelephoneID"));
                //cmd.CommandText = query1;
                //cmd.CommandType = CommandType.Text;
                //id = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return 0;
            }


            return id;
        }
        public DataSet GetTelephoneActivity(int HCBReference)
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetListTelephoneAcitivity";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (HCBReference > 0)
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, HCBReference);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBReference", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Telephone";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public DataSet GetDetailTelephoneActivity()
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "GetDetailTelephoneAcitivity";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (this.HCBTelephoneID > 0)
                {
                    this.db.AddInParameter(cmd, "HCBTelephoneID", DbType.Int32, this.HCBTelephoneID);
                }
                else
                {
                    this.db.AddInParameter(cmd, "HCBTelephoneID", DbType.Int32, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Telephone";

                //if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                //    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public DataSet DeleteTelephoneAcitivity()
        {
            DataSet ds = new DataSet();

            try
            {

                DbCommand comm = this.db.GetStoredProcCommand("DeleteTelephoneAcitivity");
                this.db.AddInParameter(comm, "@HCBTelephoneID", DbType.Int32, this.HCBTelephoneID);
                this.db.ExecuteNonQuery(comm);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion
    }
}
