﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using System.Configuration;

using System.Web;

namespace HCBBLL
{
    public class Users
    {
        #region Variable Declaration

        private Database db;
        OleDbConnection conn = new OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HCB_Database_winology"].ToString());
        Exception exDll = new Exception(" Dll corrupt  ");
        #endregion

        #region Properties

        public int UserId
        {
            get;
            set;
        }

        public string EmailAdd
        {
            get;
            set;
        }

        public string UserType
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string PhoneNum
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public int AccountActive
        {
            get;
            set;
        }

        public int IncoId
        {
            get;
            set;
        }

        public int TermsAccepted
        {
            get;
            set;
        }

        public string password
        {
            get;
            set;
        }

        public int NominatedNurseId
        {
            get;
            set;
        }

        public int FaildAttempt
        {
            get;
            set;
        }

        public DateTime PasswordExpiryDate
        {
            get;
            set;
        }
        public string LastLogin
        {
            get;
            set;
        }
        public bool IsPasswordExpired
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public Users()
        {
            try
            {
                //if (DateTime.Now.Date < DateTime.Parse("14/06/2012"))
                //{
                this.db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
                //}
                //else
                //{
                //    this.db = null;
                //}
            }
            catch (Exception ex)
            {
                this.db = null;
            }
        }

        #endregion

        #region Methods
        public DataSet GetUsersGetDetail()
        {
            DataSet ds = new DataSet();

            try
            {
                string queryClient = "usp_UsersGetDetail";

                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                if (!string.IsNullOrEmpty(this.EmailAdd))
                {
                    this.db.AddInParameter(cmd, "EmailAdd", DbType.String, this.EmailAdd);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmailAdd", DbType.String, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(this.password))
                {
                    this.db.AddInParameter(cmd, "password", DbType.String, this.password);
                }
                else
                {
                    this.db.AddInParameter(cmd, "password", DbType.String, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Vist";

                if (ds != null & ds.Tables["Vist"].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw;
            }

            return ds;
        }
        public string Login()
        {
            string result = "Ok";
            try
            {
                if (!string.IsNullOrEmpty(this.EmailAdd) && !string.IsNullOrEmpty(this.password))
                {
                    //string query = "select Users.UserID, Users.EmailAddress, Users.Password, Users.UserType, Users.FullName, Users.TelephoneNumber, Users.Address, Users.AccountActive, Users.AccountActive, Users.IncoID, "
                    //+ "Users.TermsAccepted, Users.NominatedNurseID, Users.LastLoginDate, Users.FailedAttempt, Users.PasswordExpiryDate, IIf(StrComp( [Password] ,\"" + this.password + "\",0) = 0 ,1,0) AS IsValid from Users where EmailAddress = '" + this.EmailAdd
                    //    //' and Password = '" + this.password 
                    //+ "';";
                    //DataSet ds = new DataSet();

                    //DbCommand cmd = this.db.GetSqlStringCommand(query);
                    //cmd.CommandType = CommandType.Text;
                    //ds = this.db.ExecuteDataSet(cmd);
                    string query = string.Empty;
                    string queryClient = "usp_UsersGetDetail";

                    DbCommand cmd = this.db.GetStoredProcCommand(queryClient);


                    if (!string.IsNullOrEmpty(this.EmailAdd))
                    {
                        this.db.AddInParameter(cmd, "EmailAdd", DbType.String, this.EmailAdd);
                    }
                    else
                    {
                        this.db.AddInParameter(cmd, "EmailAdd", DbType.String, DBNull.Value);
                    }
                    if (!string.IsNullOrEmpty(this.password))
                    {
                        this.db.AddInParameter(cmd, "password", DbType.String, this.password);
                    }
                    else
                    {
                        this.db.AddInParameter(cmd, "password", DbType.String, DBNull.Value);
                    }

                    DataSet ds = new DataSet();
                    ds = this.db.ExecuteDataSet(cmd);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        ds.Tables[0].TableName = "UserInfo";
                        

                        if (ds.Tables["UserInfo"].Rows.Count > 0)
                        {
                            string pwd = Encryption.Decrypt(ds.Tables["UserInfo"].Rows[0]["Password"].ToString());
                            DataRow dr = ds.Tables["UserInfo"].Rows[0];
                            FaildAttempt = Convert.ToInt16(dr["FailedAttempt"]);
                            if (FaildAttempt < 5)
                            {
                                if (Convert.ToBoolean(dr["IsValid"]))
                                {
                                    try
                                    {
                                        this.AccountActive = Convert.ToInt32(dr["AccountActive"]);
                                    }
                                    catch (Exception ex)
                                    {
                                        this.AccountActive = 1;
                                    }

                                    try
                                    {
                                        this.Address = dr["Address"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        this.Address = "";
                                    }

                                    try
                                    {
                                        this.EmailAdd = dr["EmailAddress"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        this.EmailAdd = "";
                                    }

                                    try
                                    {
                                        this.IncoId = Convert.ToInt32(dr["IncoID"]);
                                    }
                                    catch (Exception ex)
                                    {
                                        this.IncoId = 0;
                                    }

                                    try
                                    {
                                        this.Name = dr["FullName"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        this.Name = "";
                                    }

                                    try
                                    {
                                        this.NominatedNurseId = Convert.ToInt32(dr["NominatedNurseID"]);
                                    }
                                    catch (Exception ex)
                                    {
                                        this.NominatedNurseId = 0;
                                    }

                                    try
                                    {
                                        this.password = dr["Password"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        this.password = "";
                                    }

                                    try
                                    {
                                        this.PhoneNum = dr["TelephoneNumber"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        this.PhoneNum = "";
                                    }

                                    try
                                    {
                                        this.TermsAccepted = Convert.ToInt32(dr["TermsAccepted"]);
                                    }
                                    catch (Exception ex)
                                    {
                                        this.TermsAccepted = 0;
                                    }

                                    try
                                    {
                                        this.UserId = Convert.ToInt32(dr["UserID"]);
                                    }
                                    catch (Exception ex)
                                    {
                                        this.UserId = 0;
                                    }

                                    try
                                    {
                                        this.UserType = dr["UserType"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        this.UserType = "";
                                    }

                                    try
                                    {
                                        this.PasswordExpiryDate = Convert.ToDateTime(string.IsNullOrEmpty(dr["PasswordExpiryDate"].ToString()) ? DateTime.Now.AddDays(-1).ToString() : dr["PasswordExpiryDate"].ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        this.PasswordExpiryDate = DateTime.MinValue;
                                    }
                                    this.IsPasswordExpired = Convert.ToBoolean(dr["IsPasswordExpired"]);
                                    try
                                    {
                                        this.LastLogin = string.IsNullOrEmpty(dr["LastLoginDate"].ToString()) ? "" : dr["LastLoginDate"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        this.LastLogin = "";
                                    }
                                    //if (FaildAttempt > 0)
                                    //{

                                    if (this.AccountActive == 1)
                                    {
                                        int mnth = GetAccessMonth(this.UserId);
                                        mnth = 1; //remove this
                                        if (mnth >= 6)
                                        {
                                            InActiveAccount(this.UserId);

                                            //result = "Your account is locked Please contact administrator";
                                            result = "Some Error Occured.";

                                            return result;
                                        }
                                        else
                                        {
                                            //query = "Update users set FailedAttempt = 0 ,LastLoginDate = now() where EmailAddress = '" + this.EmailAdd + "'";
                                            conn = DbConnection.OpenConnection();
                                            query = "usp_UsersFailedAttempUpdate";
                                            cmd = new OleDbCommand(query, conn);
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            if (!string.IsNullOrEmpty(this.EmailAdd))
                                            {
                                                this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, this.EmailAdd);
                                            }
                                            else
                                            {
                                                this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, DBNull.Value);
                                            }
                                            cmd.ExecuteNonQuery();

                                            //DbCommand com1 = this.db.GetSqlStringCommand(query);
                                            //com1.CommandType = CommandType.Text;
                                            //this.db.ExecuteNonQuery(com1);
                                        }
                                    }
                                    else if (this.AccountActive == 0)
                                    {
                                        //result = "Your account is locked Please contact administrator";
                                        result = "Your account is locked. Please contact administrator.";
                                        return result;
                                    }
                                }
                                else
                                {

                                    //query = "Update users set FailedAttempt = FailedAttempt +1 where EmailAddress = '" + this.EmailAdd + "'";
                                    //cmd = this.db.GetSqlStringCommand(query);
                                    //cmd.CommandType = CommandType.Text;
                                    //this.db.ExecuteNonQuery(cmd);

                                    conn = DbConnection.OpenConnection();
                                    query = "usp_UsersFailedLoginUpdate";
                                    cmd = new OleDbCommand(query, conn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    if (!string.IsNullOrEmpty(this.EmailAdd))
                                    {
                                        this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, this.EmailAdd);
                                    }
                                    else
                                    {
                                        this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, DBNull.Value);
                                    }
                                    cmd.ExecuteNonQuery();

                                    result = "Invalid Login.";
                                }
                            }
                            else
                            {
                                result = " Your account is locked. Please contact administrator";
                                //result = "Some Error Occured.";
                            }
                        }
                        else
                        {
                            //result = " Invalid user Name and Password";
                            result = " Please enter valid login credentials.";
                        }
                    }
                    else
                    {
                        //result = " Invalid user Name and Password";
                        result = " Please enter valid login credentials.";
                    }
                    return result;
                }

                result = "error";
                return result;
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name == "NullReferenceException")
                {
                    //result = exDll.Message;
                    result = "Some Error Occured.";
                }
                else
                {
                    //result = ex.Message;
                    result = "Some Error Occured.";
                    // conn.Close();
                }
                return result;
            }
        }
        //Gaurav 20-03-2014
        public DataSet GetClaimAssessor()
        {
            DataSet ds = null;

            try
            {
                
                //string queryAssess = "SELECT Users.UserID, Users.EmailAddress, Users.UserType, Users.FullName, Users.TelephoneNumber, Users.IncoID, Users.TermsAccepted FROM Users WHERE (((Users.[UserType])='Inco'));";

                //DbCommand com = this.db.GetSqlStringCommand(queryAssess);
                //com.CommandType = CommandType.Text;
                //ds = this.db.ExecuteDataSet(com);
               
               
               

                string query = "GetUsersByType";

                DbCommand cmd = this.db.GetStoredProcCommand(query);

                this.db.AddInParameter(cmd, "UserType", DbType.String,"Inco");

                ds = new DataSet();
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "Users";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {
                throw;
            }

            
            return ds;
        }

        //Gaurav 20-03-2014
        public DataSet GetTitles()
        {
            DataSet ds = null;

            try
            {
                
                string queryTitle = "GetTitle";
                
                DbCommand com = this.db.GetStoredProcCommand(queryTitle);

                ds = new DataSet();
                ds = this.db.ExecuteDataSet(com);
               
                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "Titles";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                throw;
            }

            return ds;
        }

        public static DataSet GetSClaimConsultant()
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("HCB_Database_winology");
            
                DbCommand dbcom = db.GetStoredProcCommand("GetClaimConsultant");
                ds = db.ExecuteDataSet(dbcom);
                ds.Tables[0].TableName = "Consultant";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

                return ds;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet GetClaimConsultant()
        {
            DataSet ds = new DataSet();

            try
            {
                
                string queryAssess = "GetClaimConsultant";
                
                DbCommand com = db.GetStoredProcCommand(queryAssess);
                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "Consultant";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {
                throw;
            }

            return ds;
        }

        public DataSet GetUsers()
        {
            DataSet ds = null;

            try
            {                
                string queryAssess = "GetAllUsers";
                
                DbCommand com = this.db.GetStoredProcCommand(queryAssess);
                ds = this.db.ExecuteDataSet(com);
                ds.Tables[0].TableName = "Users";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;
            }
            catch (Exception ex)
            {
                throw;
            }

            return ds;
        }

        //Gaurav 20-03-2014
        public DataSet GetIncoDet()
        {
            DataSet ds = null;

            try
            {
                
                string queryAssess = "GetAssessorDet";
                
                DbCommand com = this.db.GetStoredProcCommand(queryAssess);

                if (this.UserId > 0)
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, this.UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserId", DbType.Int32, 0);
                }

                ds = this.db.ExecuteDataSet(com);

                ds.Tables[0].TableName = "Assessor";

                if (ds != null & ds.Tables[0].Rows.Count < 1)
                    ds = null;

            }
            catch (Exception ex)
            {
                throw;
            }

            return ds;
        }

        public DataSet GetUserDet()
        {
            DataSet ds = new DataSet();

            try
            {
               
                string queryAssess = "GetUserFromID";
               
                DbCommand cmd = this.db.GetStoredProcCommand(queryAssess);
                this.db.AddInParameter(cmd, "UserId", DbType.String, this.UserId);
                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "UserDet";


                if (ds.Tables["UserDet"] != null && ds.Tables["UserDet"].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables["UserDet"].Rows[0];

                    try
                    {
                        this.AccountActive = Convert.ToInt32(dr["AccountActive"]);
                    }
                    catch (Exception ex)
                    {
                        this.AccountActive = 1;
                    }

                    try
                    {
                        this.Address = dr["Address"].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.Address = "";
                    }

                    try
                    {
                        this.City = dr["City"].ToString();
                    }
                    catch(Exception ex)
                    {
                        this.City = "";
                    }

                    try
                    {
                        this.EmailAdd = dr["EmailAddress"].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.EmailAdd = "";
                    }

                    try
                    {
                        this.IncoId = Convert.ToInt32(dr["IncoID"]);
                    }
                    catch (Exception ex)
                    {
                        this.IncoId = 0;
                    }

                    try
                    {
                        this.PhoneNum = dr["TelephoneNumber"].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.PhoneNum = "";
                    }

                    try
                    {
                        this.Name = dr["FullName"].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.Name = "";
                    }

                    try
                    {
                        this.NominatedNurseId = Convert.ToInt32(dr["NominatedNurseID"]);
                    }
                    catch (Exception ex)
                    {
                        this.NominatedNurseId = 0;
                    }

                    try
                    {
                        this.password = dr["Password"].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.password = "";
                    }

                    try
                    {
                        this.TermsAccepted = Convert.ToInt32(dr["TermsAccepted"]);
                    }
                    catch (Exception ex)
                    {
                        this.TermsAccepted = 0;
                    }

                    try
                    {
                        this.UserId = Convert.ToInt32(dr["UserID"]);
                    }
                    catch (Exception ex)
                    {
                        this.UserId = 0;
                    }

                    try
                    {
                        this.UserType = dr["UserType"].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.UserType = "";
                    }

                    try
                    {
                        this.PasswordExpiryDate = Convert.ToDateTime(string.IsNullOrEmpty(dr["PasswordExpiryDate"].ToString()) ? DateTime.Now.AddDays(-1).ToString() : dr["PasswordExpiryDate"].ToString());
                    }
                    catch (Exception ex)
                    {
                        this.PasswordExpiryDate = DateTime.MinValue;
                    }

                    try
                    {
                        this.LastLogin = string.IsNullOrEmpty(dr["LastLoginDate"].ToString()) ? "" : dr["LastLoginDate"].ToString();
                    }
                    catch (Exception ex)
                    { }
                }
            }
            catch (Exception ex)
            {
                ds = null;
            }

            return ds;
        }

        //Gaurav 20-03-2014
        private int GetAccessMonth(int uid)
        {
            int month = 0;
            OleDbConnection conn = null;

            try
            {
              //  string LastLogDate = string.Format("{0:MM/dd/yyyy-MM-dd}", this.LastLogin);
              DateTime dt=Convert.ToDateTime(this.LastLogin);
                string LastLogDate = string.Format("{0:u}", dt);
                int index=LastLogDate.IndexOf('Z');
                LastLogDate=LastLogDate.Remove(index,1);

               // LastLogDate="2014-02-20 15:40:08";

               // string MonthQuery = "select DateDiff(m,'" + LastLogDate + "',GETDATE()) from Users where UserID = " + this.UserId;
                string MonthQuery ="GetMonth";
                conn = DbConnection.OpenConnection();
                DbCommand comm = this.db.GetStoredProcCommand(MonthQuery);
                if(!String.IsNullOrEmpty(LastLogDate))
                {
                        this.db.AddInParameter(comm,"LastLogDate",DbType.String,LastLogDate);
                }
                else
                {
                       this.db.AddInParameter(comm, "LastLogDate",DbType.String,DBNull.Value);
                }
                if(UserId>0)
                {

                    this.db.AddInParameter(comm,"UserId",DbType.Int32,this.UserId);
                }
                else
                {
                    this.db.AddInParameter(comm, "UserId", DbType.Int32, DBNull.Value);
                }


                month = Convert.ToInt32(this.db.ExecuteScalar(comm));
            }
            catch (Exception ex)
            {
                DbConnection.CloseConnection(conn);
                return month;
            }

            return month;
        }

        //Gaurav 20-03-2014
        private bool InActiveAccount(int userid)
        {
            OleDbConnection conn = null;

            try
            {
                string MonthQuery = "UpdateUsersActivation";
                conn = DbConnection.OpenConnection();
                DbCommand comm = this.db.GetStoredProcCommand(MonthQuery);
                if(this.UserId>0)
                {
                    this.db.AddInParameter(comm,"UserId",DbType.Int32,this.UserId);

                }
                else
                {
                    this.db.AddInParameter(comm, "UserId", DbType.Int32,DBNull.Value);
                }
                this.db.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                DbConnection.CloseConnection(conn);
                return false;
            }

            return true;
        }

        public int Save()
        {
            int Uid = 0;
            if (this.UserId == 0)
            {
                Uid = this.Insert();
                return Uid;
            }
            else
            {
                if (this.UserId > 0)
                {
                    if (this.Update())
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    this.UserId = 0;
                    return 0;
                }
            }
        }

        /// <summary>
        /// Dont conver this function
        /// 
        /// </summary>
        /// <returns></returns>
        /// 

        //Gaurav 20-03-2014
        private int Insert()
        {
            int id = 0;
            try
            {
                conn.Open();
                
                string queryClient = "GetUserByMailId";
                DataSet ds = new DataSet();
                DbCommand cmd = this.db.GetStoredProcCommand(queryClient);
                if (!String.IsNullOrEmpty(this.EmailAdd))
                {
                    this.db.AddInParameter(cmd, "EmailAddress", DbType.String, this.EmailAdd);
                }
                else
                {
                    this.db.AddInParameter(cmd, "EmailAddress", DbType.String, DBNull.Value);
                }


                ds = this.db.ExecuteDataSet(cmd);
                ds.Tables[0].TableName = "UserDet";
                //Gaurav 20-03-2014
                //cmd1.CommandText = "GetUserByMailId";
                //cmd1.CommandType=CommandType.StoredProcedure;
                //cmd1.Connection = conn;
                //OleDbDataAdapter da = new OleDbDataAdapter();
                //da.SelectCommand = cmd1;
                //DataSet ds = new DataSet();
                //da.Fill(ds, "UserDet");
                if (ds!=null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    id = -1;
                }
                else
                {
                    OleDbCommand com = new OleDbCommand("CreateUser", conn);
                    com.CommandType = CommandType.StoredProcedure;
                    this.db.AddOutParameter(com, "UserID", DbType.Int32, 1024);
                    if (!String.IsNullOrEmpty(this.EmailAdd))
                    {
                        com.Parameters.Add(new OleDbParameter("EmailAddress", OleDbType.VarChar)).Value = this.EmailAdd;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("EmailAddress", OleDbType.VarChar)).Value = DBNull.Value;
                    }
                    if (!String.IsNullOrEmpty(this.password))
                    {
                        com.Parameters.Add(new OleDbParameter("Password", OleDbType.VarChar)).Value = this.password;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("Password", OleDbType.VarChar)).Value = DBNull.Value;
                    }
                    if (!String.IsNullOrEmpty(this.UserType))
                    {
                        com.Parameters.Add(new OleDbParameter("UserType", OleDbType.VarChar)).Value = this.UserType;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("UserType", OleDbType.VarChar)).Value = DBNull.Value;
                    }
                    if (!String.IsNullOrEmpty(this.Name))
                    {
                        com.Parameters.Add(new OleDbParameter("FullName", OleDbType.VarChar)).Value = this.Name;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("FullName", OleDbType.VarChar)).Value = DBNull.Value;
                    }
                    if (!String.IsNullOrEmpty(this.PhoneNum))
                    {
                        com.Parameters.Add(new OleDbParameter("TelephoneNumber", OleDbType.VarChar)).Value = this.PhoneNum;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("TelephoneNumber", OleDbType.VarChar)).Value = DBNull.Value;
                    }
                    if (!String.IsNullOrEmpty(this.Address))
                    {
                        com.Parameters.Add(new OleDbParameter("Address", OleDbType.VarChar)).Value = this.Address;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("Address", OleDbType.VarChar)).Value = DBNull.Value;
                    }
                    if (!String.IsNullOrEmpty(this.City))
                    {
                        com.Parameters.Add(new OleDbParameter("City", OleDbType.VarChar)).Value = this.City;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("City", OleDbType.VarChar)).Value = DBNull.Value;
                    }
                    if (this.AccountActive == 1)
                    {
                        com.Parameters.Add(new OleDbParameter("AccountActive", OleDbType.Integer)).Value = this.AccountActive;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("AccountActive", OleDbType.Integer)).Value = 0;
                    }
                    //if (this.IncoId > 0)
                    //{
                        com.Parameters.Add(new OleDbParameter("IncoID", OleDbType.Integer)).Value = this.IncoId;
                    //}
                    //else
                    //{
                    //    com.Parameters.Add(new OleDbParameter("IncoID", OleDbType.Integer)).Value = DBNull.Value;
                    //}
                    //if (this.NominatedNurseId > 0)
                    //{
                        com.Parameters.Add(new OleDbParameter("NominatedNurseID", OleDbType.Integer)).Value = this.NominatedNurseId;
                    //}
                    //else
                    //{
                    //    com.Parameters.Add(new OleDbParameter("NominatedNurseID", OleDbType.Integer)).Value = DBNull.Value;
                    //}
                    if (this.TermsAccepted >= 0)
                    {
                        com.Parameters.Add(new OleDbParameter("TermsAccepted", OleDbType.Integer)).Value = this.TermsAccepted;
                    }
                    else
                    {
                        com.Parameters.Add(new OleDbParameter("TermsAccepted", OleDbType.Integer)).Value = DBNull.Value;
                    }
                    if (this.PasswordExpiryDate != null)
                    {
                        this.db.AddInParameter(com, "PasswordExpiryDate", DbType.String, string.Format("{0: MM/dd/yyyy}", this.PasswordExpiryDate));
                    }
                    else
                    {
                        this.db.AddInParameter(com, "PasswordExpiryDate", DbType.String, DateTime.Now);
                    }
                    //com.Parameters.Add(new OleDbParameter("PasswordExpiryDate", OleDbType.VarChar)).Value = DateTime.Now.AddDays(-1);

                    //string query1 = "Select @@Identity";
                    com.ExecuteNonQuery();
                    id = Convert.ToInt32(this.db.GetParameterValue(com, "UserID")); 
                    //com.CommandText = query1;
                    //com.CommandType = CommandType.Text;
                    //id = (int)com.ExecuteScalar();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                // To Do: Handle Exception
                return 0;
            }

            //return this.UserId > 0; // Return whether ID was returned
            return id;
        }

        private bool Update()
        {
            try
            {
                
                DbCommand com = this.db.GetStoredProcCommand("UpdateOtherUser");
                if (this.UserId > 0)
                {
                    this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserId);
                }
                else
                {
                    this.db.AddInParameter(com, "UserID", DbType.Int32, DBNull.Value);
                }

                if (!String.IsNullOrEmpty(this.EmailAdd))
                {
                    this.db.AddInParameter(com, "EmailAddress", DbType.String, this.EmailAdd);
                }
                else
                {
                    this.db.AddInParameter(com, "EmailAddress", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.password))
                {
                    this.db.AddInParameter(com, "Password", DbType.String, this.password);
                }
                else
                {
                    this.db.AddInParameter(com, "Password", DbType.String, DBNull.Value);
                }
                //if (!String.IsNullOrEmpty(this.UserType))
                //{
                //    this.db.AddInParameter(com, "UserType", DbType.String, this.UserType);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "UserType", DbType.String, DBNull.Value);
                //}
                if (!String.IsNullOrEmpty(this.Name))
                {
                    this.db.AddInParameter(com, "FullName", DbType.String, this.Name);
                }
                else
                {
                    this.db.AddInParameter(com, "FullName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.PhoneNum))
                {
                    this.db.AddInParameter(com, "TelephoneNumber", DbType.String, this.PhoneNum);
                }
                else
                {
                    this.db.AddInParameter(com, "TelephoneNumber", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address))
                {
                    this.db.AddInParameter(com, "Address", DbType.String, this.Address);
                }
                else
                {
                    this.db.AddInParameter(com, "Address", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);                    
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);                    
                }

                if (this.NominatedNurseId > 0)
                {
                this.db.AddInParameter(com, "NominatedNurseID", DbType.Int32, this.NominatedNurseId);
                }
                else
                {
                    this.db.AddInParameter(com, "NominatedNurseID", DbType.Int32, DBNull.Value);
                }
                
                if (this.PasswordExpiryDate != null)
                {
                    this.db.AddInParameter(com, "PasswordExpiryDate", DbType.String, string.Format("{0: MM/dd/yyyy}",this.PasswordExpiryDate));
                }
                else
                {
                    this.db.AddInParameter(com, "PasswordExpiryDate", DbType.String, DateTime.Now);
                }
                if (this.IncoId > 0)
                {
                this.db.AddInParameter(com, "IncoID", DbType.Int32, this.IncoId);
                }
                else
                {
                    this.db.AddInParameter(com, "IncoID", DbType.Int32, DBNull.Value);
                }

                if (!string.IsNullOrEmpty(this.LastLogin))
                {
                    this.db.AddInParameter(com, "LastLoginDate", DbType.String, string.Format("{0: MM/dd/yyyy}",Convert.ToDateTime(this.LastLogin)));
                }
                else
                {
                    this.db.AddInParameter(com, "LastLoginDate", DbType.String, DBNull.Value);
                }

                if (this.AccountActive == 1)
                {
                    this.db.AddInParameter(com, "ActiveAccount", DbType.Int16, this.AccountActive);
                }
                else
                {
                    this.db.AddInParameter(com, "ActiveAccount", DbType.Int16, 0);
                }
                
                

                //if (this.TermsAccepted >= 0)
                //{
                //    this.db.AddInParameter(com, "TermsAccepted", DbType.String, this.TermsAccepted);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "TermsAccepted", DbType.String, DBNull.Value);
                //}

                //OleDbDataAdapter da = new OleDbDataAdapter();
                //da.UpdateCommand = com;


                //com.ExecuteNonQuery();

                //string query = "UpdateUsersLogin";

                this.db.ExecuteNonQuery(com);
                conn.Open();
                DbCommand com1 = this.db.GetStoredProcCommand("UpdateUsersLogin");
                com1.Parameters.Add(new OleDbParameter("UserId", this.UserId));
                //com.CommandType = CommandType.StoredProcedure;

                this.db.ExecuteNonQuery(com1);
                conn.Close();
                //conn.Close();
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                //conn.Close();
                return false;
            }

            return true;
        }

        public bool Delete()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("DeleteUser");
                this.db.AddInParameter(com, "UserId", DbType.Int32, this.UserId);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                conn.Close();
                return false;
            }

            return true;
        }
        //Added by Jaywanti
        /// <summary>
        /// Updates password expiry date
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public DataSet UpdatePasswordExpiyDate(string UserName, string Password, string newPassword)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("upUserUpdatePasswordExpiyDate");
                this.db.AddInParameter(com, "UserName", DbType.String, UserName);
                this.db.AddInParameter(com, "Password", DbType.String, Password);
                this.db.AddInParameter(com, "newPassword", DbType.String, newPassword);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        public DataSet usp_PasswordCheckAlreadyExist(int UserId, string newPassword)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_PasswordCheckAlreadyExist");
                this.db.AddInParameter(com, "UserId", DbType.String, UserId);
                this.db.AddInParameter(com, "newPassword", DbType.String, newPassword);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }
        
        public List<UserInformation> GetNurseLocations()
        {
            List<UserInformation> Nurses = new List<UserInformation>();
            DbCommand com = db.GetStoredProcCommand("usp_GetNursesLocations");
            using (DataSet dsNurseLocations = db.ExecuteDataSet(com))
            {
                if (dsNurseLocations != null && dsNurseLocations.Tables.Count > 0)
                {
                    using (DataTable dtNurseLocations = dsNurseLocations.Tables[0])
                    {
                        Nurses = (from DataRow dr in dtNurseLocations.Rows
                                  select new UserInformation()
                                  {
                                      Name = Convert.ToString(dr["FullName"]),
                                      Address = Convert.ToString(dr["Address"]),
                                      City = Convert.ToString(dr["City"])
                                  }).ToList();
                    }
                }
            }
            return Nurses;
        }

        #endregion
    }

    public class UserInformation
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
    }
}
